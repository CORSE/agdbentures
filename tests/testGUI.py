from enum import Enum
from threading import Thread
from config import Config
import tests.lib_test as T
import traceback        


class TestThread(Thread):
    def run(self):
        try:
            Thread.run(self)
        except:
            T.error("ERROR test thread doesn't exit correctly")
            print(traceback.format_exc())



class Reason(Enum):
    STARTING_GUI_TEST_RUNNER_THREAD="Starting gui test runner thread"
    STARTING_TEST_THREAD="Starting test thread"
    WINDOW_LOADED="Window loaded"
    VIEW_LOADER="View switch to loader"
    VIEW_LEVEL_CHOICE="View switch to level choice"
    VIEW_END_GAME="View switch to end game"
    VIEW_MAIN="View switch to main"
    POST_RESTART="The level has restarted"
    COMMAND_EXECUTED="The command has been executed"
    QUIT_LEVEL="The level has been quitted"  
    ARCADE_EXIT="Arcade exited"
    GUI_CHANGE="Gui changed"

class EventHandler:
    def setTestThread(thread):
        Config.test_thread = thread

    def getTestThread() -> Thread:
        return Config.test_thread
    
    def addReason(r: Reason):
        Config.reason_stack.append(r)
        Config.gui_event.clear()

    def wait(timeout=5):
        return Config.gui_event.wait(timeout)
        
    def notify(r: Reason):
        if (len(Config.reason_stack) > 0 and r == Config.reason_stack[-1]):
            Config.gui_event.set()
            Config.reason_stack.pop() 

class GuiTestRunner:
    def __init__(self, views: dict):
        self.views = views 
        test_thread = Thread(target=self.run_test)
        EventHandler.addReason(Reason.STARTING_GUI_TEST_RUNNER_THREAD)
        test_thread.start()
        EventHandler.wait()

        
    def run_test(self):
        import arcade
        lvl_failed = []  
        lvl_skipped = []  
        lvl_no_test = [] 
        EventHandler.notify(Reason.STARTING_GUI_TEST_RUNNER_THREAD)
        # Wait for the main view to be launched 
        EventHandler.addReason(Reason.WINDOW_LOADED)
        EventHandler.wait()
        # Click on PLAY
        EventHandler.addReason(Reason.VIEW_LOADER)
        self.views["main"].start_button.dispatch_event("on_click", arcade.gui.UIOnClickEvent)
        EventHandler.wait()
        # Click on TUTORIAL
        EventHandler.addReason(Reason.VIEW_LEVEL_CHOICE)
        self.views["loader"].button_map.get("tutorial").dispatch_event("on_click", arcade.gui.UIOnClickEvent)
        EventHandler.wait()
        levels: dict = self.views["level_choice"].button_map
        # Back to menu and start test all tutorials levels 
        EventHandler.addReason(Reason.VIEW_LOADER)
        self.views["level_choice"].button_back.dispatch_event("on_click", arcade.gui.UIOnClickEvent)
        EventHandler.wait()

        for level in levels.keys():
            prev_tests = T.num_tests
            prev_errors = T.num_errors
            prev_skipped = T.num_skipped


            print("==============Starting GUI tests on:", level, "==============")
            EventHandler.addReason(Reason.VIEW_LEVEL_CHOICE)
            self.views["loader"].button_map.get("tutorial").dispatch_event("on_click", arcade.gui.UIOnClickEvent)
            EventHandler.wait()

            EventHandler.addReason(Reason.STARTING_TEST_THREAD)
            levels[level].dispatch_event("on_click", arcade.gui.UIOnClickEvent)
            if not EventHandler.wait(None):
                print("No test found for", level) 
            elif EventHandler.getTestThread() is None or not EventHandler.getTestThread().is_alive():
                print("Error, the test thread must be running")
            else:
                print("Test found for", level) 
                EventHandler.getTestThread().join()
            

            if T.num_errors > prev_errors:
                lvl_failed.append(level)
            if T.num_skipped > prev_skipped:
                lvl_skipped.append(level)
            if T.num_tests == prev_tests:
                T.print_warn(f"No test run for {level}")
                lvl_no_test.append(level)
            EventHandler.setTestThread(None)
            # Return to menu; from the winning; defeat view or the game view
            EventHandler.addReason(Reason.QUIT_LEVEL)
            self.views['pause_game'].buttons["EXIT_LEVEL"].dispatch_event("on_click", arcade.gui.UIOnClickEvent)
            EventHandler.wait()
            
            
        EventHandler.addReason(Reason.VIEW_MAIN)
        self.views["loader"].button_back.dispatch_event("on_click", arcade.gui.UIOnClickEvent)
        EventHandler.wait()

        EventHandler.addReason(Reason.ARCADE_EXIT)
        self.views["main"].quit_button.dispatch_event("on_click", arcade.gui.UIOnClickEvent)
        EventHandler.wait()

        arcade.exit()
        arcade.get_window()._context = None
        T.info()    


        retcode = 0

        if len(lvl_skipped) > 0:
            T.print_warn("Skips in levels:")
            for name in lvl_skipped:
                T.print_warn("\t" + name)

        if len(lvl_no_test) > 0:
            T.print_warn("Levels without tests:")
            for name in lvl_no_test:
                T.print_warn("\t" + name)

        if len(lvl_failed) > 0:
            retcode = 1
            T.print_errinf("Failed levels:")
            for name in lvl_failed:
                T.print_errinf("\t" + name)

        exit(retcode)
