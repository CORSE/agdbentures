#!/usr/bin/env python3

import os
import sys
from pathlib import Path
from os.path import dirname, join
import logging
import argparse
import traceback
import queue


v = sys.version_info

if sys.version_info.major < 3 or sys.version_info.minor < 9:
    import platform

    print(
        f"Error: must be using Python 3.9 or higher, but your python version is {platform.python_version()}"
    )
    exit(1)
cdir = Path(os.path.dirname(__file__)).parent
sys.path.insert(
    0, os.path.join(cdir, f".venv/lib/python{v.major}.{v.minor}/site-packages")
)  # if using a python virtual env
sys.path.insert(0, os.path.join(cdir, "easytracker"))
sys.path.insert(0, os.path.join(cdir, "lib"))


from loader.loader_utils import find_levels, import_from_file, get_dynamic_level
from loader.loader import prepare_level
import tests.lib_test as T
from logs import log, set_loggers_level
from config import Config

parser = argparse.ArgumentParser(
    description="""Agdbentures test launcher.
Test levels integrated with Agdbentures.
"""
)
parser.add_argument(
    "--debug",
    "-d",
    type=int,
    choices=[0, 1, 2, 3],
    default=1,
    help="Set debug level (0 = crit, 1 = warn, 2 = info, 3 = debug)",
)
parser.add_argument(
    '-l', "--list", action="store_true", help="List levels instead of testing them"
)
parser.add_argument(
    '-a', "--all", action="store_true", help="Use all levels and not only the subset of categories"
)
parser.add_argument('levels', nargs='*', help="Test only levels on this list")
args = parser.parse_args()

if args.debug == 0:
    loglevel = logging.CRITICAL
elif args.debug == 1:
    loglevel = logging.WARNING
elif args.debug == 2:
    loglevel = logging.INFO
else:
    loglevel = logging.DEBUG

Config.DEBUG = args.debug

set_loggers_level(loglevel)

Config.test_mode()
Config.propagate()

lvl_failed = []   # record levels where some tests failed
lvl_skipped = []  # record levels where some tests were skipped
lvl_no_test = []  # record levels that have no tests


def test_level(level_name:str, filename: str, debug_lvl: int):
    """
    Loads a level, automatically finding metadata, then test it.

    :param filename: a .py file implementing the custom level class
    """
    level_in_queue: queue.Queue = (
        queue.Queue()
    )  # dummy queues for now, will probably not be used in test
    level_out_queue: queue.Queue = queue.Queue()

    full_level_name, custom_py = os.path.split(filename)
    assert custom_py.endswith('.py')

    try:
        cus_level_cls, metadata = prepare_level(level_name)
        if not cus_level_cls:
            raise ValueError("Could not create custom level class")
        if not metadata:
            raise ValueError("No metadata found for level")
    except Exception as e:
        T.error(f"Cannot prepare level {level_name}")
        raise e

    try:
        level_cls = get_dynamic_level(cus_level_cls, "test")
        controler = level_cls(metadata, level_in_queue, level_out_queue)
    except Exception as e:
        T.error(f"Cannot import level from {level_name}")
        raise e

    if debug_lvl > 1:
        print("Launching test in debug mode")
        controler.debug = True

    try:
        controler.test()
        controler.tracker.terminate()
    except Exception as e:
        T.error(f"Exception thrown when testing {level_name}")
        raise e


levs = os.path.join(cdir, "levels")
if args.all:
    lvls = find_levels()
else:
    lvls = find_levels(categories = ["tutorial","basic","medium"])


log.debug(f"Levels found: {lvls}")


prev_errors = 0

if args.list:
    print("List of levels:")
for categories in lvls:
    for name, file in sorted(lvls[categories]):
        prev_tests = T.num_tests
        prev_errors = T.num_errors
        prev_skipped = T.num_skipped

        if args.levels:
            to_test = False
            for l in args.levels:
                if l in name:
                    to_test = True
                    break

            if not to_test:
                continue  # skip

        if args.list:
            print(f"\t{name}")
            continue

        try:

            T.clear_cmds()
            cus_level_cls = import_from_file(file, "Level")
            ret = test_level(name, file, args.debug)

        except Exception as e:
            T.error(f"Exception thrown when testing {name}")
            T.print_errinf(e)
            traceback.print_exc()
            lvl_failed.append(name)
            continue


        if T.num_errors > prev_errors:
            lvl_failed.append(name)
        if T.num_skipped > prev_skipped:
            lvl_skipped.append(name)
        if T.num_tests == prev_tests:
            T.print_warn(f"No test run for {name}")
            lvl_no_test.append(name)


if args.list:
    exit(0)

T.info()

retcode = 0

if len(lvl_skipped) > 0:
    T.print_warn("Skips in levels:")
    for name in lvl_skipped:
        T.print_warn("\t" + name)

if len(lvl_no_test) > 0:
    T.print_warn("Levels without tests:")
    for name in lvl_no_test:
        T.print_warn("\t" + name)


known_failed = [
    ## No know failed at the moment, but input_command is still random.
    ## So we keep it here to avoid problems
    "basic/input_command",
    ## Command args seems also random, it used to work on my machine (Flo)
    ## but now it never passes tests (infinite loop), even on older commits.
    "basic/command_args",
    # "basic/holy_grail",
    # "medium/crepes",
    # "medium/local_struct",
    # "medium/near_exit",
    # "tests/entity_struct",
    # "tests/map_link",
]

for k in known_failed:
    if k not in lvl_failed:
        T.print_succ(f"GOOD NEWS: a 'known-to-fail' level has passed: {k}")
    else:
        T.print_warn(f"Known to fail: still failing {k}")
        lvl_failed.remove(k)

if len(lvl_failed) > 0:
    retcode = 1
    T.print_errinf("Failed levels:")
    for name in lvl_failed:
        T.print_errinf("\t" + name)

exit(retcode)
