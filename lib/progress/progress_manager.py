"""A class to track levels that are finished or not."""

import os
import json
from logs import log
from config import Config

PROGRESS_FILENAME = "progress.json"
PROGRESS_FILE = os.path.join(Config.ROOT_DIR, PROGRESS_FILENAME)

# Default levels for initialization
LEVELS = {
    "tutorial": [
        ["00_introduction"],
        ["01_first_bug"],
        ["02_refactoring"],
        ["03_bugs_hiding"],
        ["04_teleportation"],
        ["05_stairs"],
        ["06_key"],
    ],
    "basic": [
        ["simple_map"],
        ["password"],
        ["input_command"],
        ["holy_grail"],
        ["command_args"],
        ["keys"],
    ],
    "medium": [["pet_follow", "keys"], ["local_struct"], ["crepes"]],
}


class LevelProgression:
    """A single level with data describing the player's progress on it"""

    def __init__(
        self, name: str, done: bool = False, time_played: int = 0, restarts: int = 0
    ):
        self.name = name  # The name of the category's folder + the name of the level's folder. example: 'basic/password'
        self.done = done
        self.time_played = time_played
        self.restarts = restarts

    def to_dict(self) -> dict:
        return {
            "name": self.name,
            "done": self.done,
            "time_played": self.time_played,
            "restarts": self.restarts,
        }

    def is_done(self) -> bool:
        return self.done


class LevelLine:
    """A set of levels that can be done in parallel"""

    def __init__(self, levels: list[LevelProgression]):
        self.levels = levels

    def to_dict(self) -> list:
        return list(map(lambda lvl: lvl.to_dict(), self.levels))

    def is_done(self) -> bool:
        return all(map(lambda lvl: lvl.is_done(), self.levels))


class LevelCategory:
    """A set of LevelLines that are part of the same category"""

    def __init__(self, name: str, levels_lines: list[LevelLine] = []):
        self.name = name
        self.levels_lines = list(levels_lines)

    def add_levels(self, lvl: LevelLine):
        self.levels_lines.append(lvl)

    def to_dict(self) -> dict:
        return {
            "name": self.name,
            "levels": list(map(lambda lvl_grp: lvl_grp.to_dict(), self.levels_lines)),
        }

    def is_done(self) -> bool:
        return all(map(lambda lvl: lvl.is_done(), self.levels_lines))


class ProgressionManager:
    """The overall progress of the player (stored as a set of LevelCategory)"""

    def __init__(self):
        self.categories = []

        if os.path.isfile(PROGRESS_FILE):
            self.read_progression()
        else:
            log.info("Progress file not found, creating one")
            self.init_progression()
            self.write_progression()

    def add_category(self, cat: LevelCategory):
        self.categories.append(cat)

    def init_progression(self):
        """Uses LEVELS to create the default values for the progression"""
        log.info(f"Initializing the default progression")
        self.categories = []
        for cat_name in LEVELS.keys():
            cat = LevelCategory(cat_name)
            for lvl_list in LEVELS[cat_name]:
                cat.add_levels(
                    LevelLine(
                        list(
                            map(
                                lambda l: LevelProgression(os.path.join(cat_name, l)),
                                lvl_list,
                            )
                        )
                    )
                )
            self.add_category(cat)

    def to_dict(self) -> dict:
        return {"categories": list(map(lambda l: l.to_dict(), self.categories))}

    def write_progression(self):
        log.info(f"Saving progression")
        with open(PROGRESS_FILE, 'w+') as f:
            json.dump(self.to_dict(), f, indent=2)

    def read_progression(self):
        log.info(f"reading progression")
        try:
            with open(PROGRESS_FILE, 'r') as f:
                json_data = json.load(f)
        except Exception as e:
            print(e)
            log.error(f"Failed to load {PROGRESS_FILE}")
            return

        try:
            self.categories = list(map(json_to_LevelCategory, json_data["categories"]))
        except Exception as e:
            print(e)
            log.error(f"Failed to recover the data from {PROGRESS_FILE}.")

    def next_level(self) -> LevelProgression:
        """Returns the next level to be done"""
        log.info(f"Searching the next level")
        is_next = False
        for cat in self.categories:
            for lvl_line in cat.levels_lines:
                for lvl in lvl_line.levels:
                    if not lvl.is_done():
                        return lvl
        log.warning(f"Can't find next level")
        return None

    def set_done(self, level_name: str):
        lvl = self.find_level(level_name)
        if lvl != None:
            lvl.done = True
            self.write_progression()

    def reset_progress(self):
        self.init_progression()
        self.write_progression()

    def find_category(self, cat_name: str) -> LevelCategory:
        for cat in self.categories:
            if cat.name == cat_name:
                return cat
        log.warning(f"Can't find category {cat_name}")
        return None

    def find_level(self, level_name: str) -> LevelProgression:
        for cat in self.categories:
            for lvl_line in cat.levels_lines:
                for lvl in lvl_line.levels:
                    if lvl.name == level_name:
                        return lvl
        log.warning(f"Can't find level {level_name}")
        return None

    def change_time_played(self, level_name: str, n: int):
        """Adds n to the time already played"""
        lvl = self.find_level(level_name)
        if lvl != None:
            lvl.time_played += n
            self.write_progression()

    def add_restart(self, level_name: str):
        lvl = self.find_level(level_name)
        if lvl != None:
            lvl.restarts += 1
            self.write_progression()


def json_to_LevelProgression(json_data: dict) -> LevelProgression:
    return LevelProgression(
        json_data["name"],
        json_data["done"],
        json_data["time_played"],
        json_data["restarts"],
    )


def json_to_LevelLine(json_data: list) -> LevelLine:
    return LevelLine(list(map(json_to_LevelProgression, json_data)))


def json_to_LevelCategory(json_data: dict) -> LevelCategory:
    return LevelCategory(
        json_data["name"], list(map(json_to_LevelLine, json_data["levels"]))
    )

progress = ProgressionManager()