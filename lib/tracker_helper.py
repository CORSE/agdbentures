from logs import trk
import threading
import time
from easytracker import init_tracker


class TimeoutCallback(threading.Timer):
    """
    Launches a thread that wait for the specified amount of time before
    calling the specified function passed as argument.
    Used, e.g., to interrupt the inferior of a GDB tracker in an infinite loop.
    """

    def __init__(self, tracker, waiting_time: float, callback):
        # Starting in daemon mode so threads will exit when the rest of the
        # tests are over even if still sleeping.
        super().__init__(waiting_time, callback)
        self._daemonic = True
        self.tracker = tracker


class BreakPoint:
    def __init__(self,
                 tracker,
                 kind: str,  # possible values: start_func, end_func, watch
                 target:str, # function name, variable name, or filename (not implemented)
                 target_register: dict,  # a dictionnary to get target -> breakpoint assoc
                 breakpoints: dict,      # where to store the breakpoint num -> bp assoc
                 dict_break: dict,
                 callbacks: None|list = None):
        self.tracker = tracker
        self.kind = kind
        self.target = target
        self.target_register = target_register
        self.breakpoints = breakpoints
        self.callbacks = callbacks if callbacks else []
        self.our_breakpoint = dict_break
        if kind == 'watch' and len(breakpoints) >= 4:
             trk.warning(
                 f"Too many watched variables, not enough hardware watches available for {target}"
             )
             assert False

        self._bp_num = self._install_bp()
        assert target not in target_register
        assert self._bp_num not in breakpoints
        target_register[target] = self
        breakpoints[self._bp_num] = self

    def __str__(self):
        return f"{self.kind} #{self._bp_num} on {self.target} with {len(self.callbacks)} callbacks"

    def add_callback(self, cb):
        self.callbacks.append(cb)

    def delete_for_reinstall(self):
        """
        Warning, only to be used later with re_install!!!
        """
        self.tracker.delete_breakpoint(self._bp_num)
        trk.debug(f"Deleting br/watch for reinstall: {self}")

    def _install_bp(self):
        match self.kind:
            case 'start_func':
                new_num = self.tracker.break_before_func(self.target)
            case 'end_func':
                new_num = self.tracker.break_end_of_func(self.target)
            case 'watch':
                new_num = self.tracker.watch(self.target)
            case _:
                raise NotImplementedError
        self.our_breakpoint[
                new_num
        ] = (
            self.target,
            self.tracker.next_source_file if self.tracker.next_source_file else "",
            self.kind,
        )

        return new_num

    def re_install(self):
        new_num = self._install_bp()
        old_num = self._bp_num
        assert self.target_register[self.target] == self
        assert self.breakpoints[old_num] == self
        del self.breakpoints[old_num]
        self.breakpoints[new_num] = self
        self._bp_num = new_num
        trk.debug(f"Reinstalled breakpoint {self} (was #{old_num})")


class TrackerHelp:
    """
    Defines some utilities to ease usage of trackers in agdbentures
    """

    def __init__(self, source_level_dir, exec_name):

        self.source_level_dir = source_level_dir
        self.exec_name = exec_name
        self.init_tracker()

    def init_tracker(self):
        self.internal_bp = {}
        self.internal_wp = {}
        self.watched_variables = {}
        self.breaked_functions = {}
        self.breaked_leave_functions = {}
        self.magic_bp = None
        self.tracker = init_tracker("GDB", can_use_hw_watchpoints=True)
        self.tracker.send_direct_command(f"cd {self.source_level_dir}")
        self.our_breakpoint= {}
    def load_and_start_program(self):
        """
        Load the registered program file into gdb, then call `start`
        """
        self.tracker.load_program(self.exec_name)
        self.tracker.start()

    def reload_and_start_program(self):
        """
        Reload inferior. If tracker was not present, would need to recreate one, however,
        would also need to reinitialize the whole level again, so currently,
        raises an exception intsead.
        :return: True if the tracker is brand new (i.e., without all breaks and watches)
        """
        assert self.tracker  # Previous gdb instance should not be killed or have exited
        # No need to manually clear gdb cache with `directory` command.
        # It is now automatically done by easytracker.

        # Delete all internal breakpoints, then re-insert them, after program
        # has started
        # (Important! only loading the program with `file` is not enough)

        # export current list as the dictionary will be modified
        current_bps = list(self.internal_bp.keys())
        current_wps = list(self.internal_wp.keys())

        for bpnum in current_bps:
            self.internal_bp[bpnum].delete_for_reinstall()
        for bpnum in current_wps:
            self.internal_wp[bpnum].delete_for_reinstall()
        self.load_and_start_program()
        # trk.debug(f"GBBOUT: {self.tracker.get_gdb_console_output()}")
        for bpnum in current_bps:
            self.internal_bp[bpnum].re_install()
        for bpnum in current_wps:
            self.internal_wp[bpnum].re_install()


    def interrupt_inferior(self):
        """
        Send Ctrl-C to pause execution of inferior
        """
        self.tracker.interrupt()

    def interrupt_inferior_if_running(self):
        """
        Send Ctrl-C to pause execution of inferior
        """
        if self.tracker and self.tracker.is_running():
            self.interrupt_inferior()


    def _register_gen_breakpoint(self, kind, target, target_register, breakpoints, callback):
        if target in target_register:
            bp = target_register[target]
            trk.debug(f"Appending callback to breakpoint {bp} for {kind} {target}")
            bp.add_callback(callback)
        else:
            bp = BreakPoint(
                tracker = self.tracker,
                kind = kind,
                target = target,
                target_register = target_register,
                breakpoints = breakpoints,
                dict_break = self.our_breakpoint,
                callbacks = [callback],
            )
            trk.debug(f"Registered breakpoint {kind=} {target}: got br_no {bp._bp_num}")


    def register_breakpoint(self, function_name, callback):
        """
        Put a breakpoint on a function and register a callback to
        use whenever the breakpoint is hit, before continuing simulation
        the last gdb command received
        """
        self._register_gen_breakpoint(
            kind = 'start_func',
            target = function_name,
            target_register = self.breaked_functions,
            breakpoints = self.internal_bp,
            callback = callback,
        )

    def register_leave_function(self, function_name, callback):
        """
        Put a breakpoint when a function is just about to return and register a callback to
        use whenever the breakpoint is hit, before continuing simulating
        the last gdb command received
        """
        self._register_gen_breakpoint(
            kind = 'end_func',
            target = function_name,
            target_register = self.breaked_leave_functions,
            breakpoints = self.internal_bp,
            callback = callback,
        )

    def register_watch(self, variable_name, callback):
        """
        Put a watch on a variable and register a callback to
        use whenever the value of the variable changes.
        """
        self._register_gen_breakpoint(
            kind = 'watch',
            target = variable_name,
            target_register = self.watched_variables,
            breakpoints = self.internal_wp,
            callback = callback,
        )

    def tracker_in_function(self, fun_name):
        """
        Checks if the tracker is currently in function :param: fun_name
        """
        current = self.tracker.get_current_function_name()
        return current == fun_name

    def add_magic_breakpoint(self, x: int, y: int):
        """
        Create a "magic" break on forward function associated with a particular value of
        player_x and player_y
        """
        if not self.magic_bp:
            self.magic_bp = self.tracker.break_end_of_func("forward")
            trk.debug(
                f"Placing magic breakpoint {self.magic_bp} for coordinates {x}x{y}"
            )
        else:
            trk.debug(
                f"Changing magic breakpoint {self.magic_bp} for coordinates {x}x{y}"
            )

        # add_condition will change the condition if already existing
        ret = self.tracker.add_condition(
            self.magic_bp, f"player_x == {x} && player_y == {y}"
        )
        trk.debug(f"condition set : {ret}")

    def delete_magic_breakpoint(self):
        """
        Delete the breakpoint associated to a magic-bp
        """
        assert self.magic_bp
        self.tracker.delete_breakpoint(self.magic_bp)
        self.magic_bp = None
