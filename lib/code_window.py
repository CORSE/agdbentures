from subprocess import run, Popen, PIPE
import os
import re
import atexit
import shlex
import shutil

import graphic.constants as cst
from logs import log
from config import Config

# global variable since it simplifies code a lot


class GenServerWindow:
    window_alignment = 'right'

    def __init__(self, window):
        self._window_title = "GenServer"
        pass

    def launch(self):
        pass

    def resize_window(self, width, height):
        pass

    def move_window(self, xpos, ypos, winid=None):
        pass

    def snap_to_agdbentures(self):
        pass

    def show_next_line(self, base: str, source_file: str, line_number: int) -> None:
        pass

    def reload(self):
        """
        Force reloading a file (when we know it has been changed for instance)
        """
        pass

    def check_or_reopen(self):
        pass

    def terminate(self):
        pass

    def change_title(
        self,
    ):  # set the title as the code window (esp. vim) might have changed it
        pass


class NoServerWindow(GenServerWindow):
    def __init__(self, window=None):
        self._window_title = "NoServerWindow"


pos_re = re.compile(r'Position: (-?\d+),(-?\d+)')
geom_re = re.compile(r'Geometry: (\d+)x(\d+)')
wid_re = re.compile(r'WID: (\d+)\n')
dimen_re = re.compile(r'dimensions: +(\d+)x(\d+) pixels')
monitors_re = re.compile(r'\d+: +\S+ +(\d+)/\d+x(\d+)/\d+\+(\d+)\+\d+')


class ServerWindow(GenServerWindow):
    def __init__(self, agdb_window):
        super().__init__(agdb_window)
        self.agdb_window = agdb_window
        self.pid = None
        self._process = None
        self.source_file = None  # full (relative) path of the current source file
        self._launch_command = None
        self._file_command = None
        self._line_command = None
        self._window_id = None
        self._agdbwindow_id = None
        self._window_title = "ServerWindow"

        # get current screen geometry
        ## xrandr --listmonitors
        # or
        ## xdpyinfo | awk '/dimensions/ {print $2}'

        # x = run(["xdpyinfo"], capture_output=True)
        # m = dimen_re.search(x.stdout.decode())  # search dimensions output
        # if not m:
        # log.error(f"Cannot find screen dimensions")
        # self._screen_width, self._screen_height = 0, 0
        # else:
        # self._screen_width, self._screen_height = list(map(int, m.groups()))
        x = run(["xrandr", "--listmonitors"], capture_output=True)
        m = monitors_re.findall(x.stdout.decode())  # search dimensions output
        if not m:
            log.error(f"Cannot find screen dimensions")
            self._screen_width, self._screen_height = 0, 0
            self._screen_offset_x = 0
        else:
            # take last occurence
            self._screen_width, self._screen_height, self._screen_offset_x = list(
                map(int, m[-1])
            )

        log.debug(f"Screen dimensions : {self._screen_width, self._screen_height, self._screen_offset_x}")

        self.xdotool = shutil.which("xdotool")
        if not self.xdotool:
            log.warning("Cannot find xdotool in $PATH")
        else:
            log.debug(f"xdotool found: {self.xdotool}")

        atexit.register(self.terminate)

    def launch(self):
        assert self._launch_command
        # Need to use Popen as subprocess will continue to run (e.g., vim)
        self._process = Popen(shlex.split(self._launch_command), stdout=PIPE)
        self.pid = self._process.pid

    def check_or_reopen(self):
        if self._process is not None and self._process.poll() is None:
            return
        self.launch()

    def _get_window_id(self, option, name):
        if not self.xdotool:
            return
        x = run([self.xdotool, "search", option, name], capture_output=True)
        lst = x.stdout.decode().split('\n')
        if len(lst) == 1:
            log.error(f"Cannot find window id for {option, name}")
            return ""
        if len(lst) > 2:
            log.warning(f"Found more than one window for {option, name}: lst")
        return lst[-2]

    def get_code_window_id(self):
        """
        Get the code window id in case we don't have it yet.
        (e.g., for gvim it is not necessary as we ask it to return the window id at startup)
        """
        if self._window_id:
            return
        self.change_title()  # set the title as the code window (esp. vim) might have changed it
        self._window_id = self._get_window_id("--name", self._window_title)
        log.debug(f"Window id of code window: {self._window_id}")

    def get_agdb_window_id(self):
        if self._agdbwindow_id:
            return
        self._agdbwindow_id = self._get_window_id("--classname", cst.Win.TITLE)
        log.debug(f"Window id of agdbentures window: {self._agdbwindow_id}")

    def resize_window(self, width, height):
        if not self.xdotool:
            return
        run(["xdotool", "windowsize", self._window_id, str(width), str(height)])

    def move_window(self, xpos, ypos, winid=None):
        if not self.xdotool:
            return
        if winid is None:
            winid = self._window_id
        log.debug(f"Moving window {winid} to {xpos, ypos}")
        run([self.xdotool, "windowmove", winid, str(xpos), str(ypos)])

    def get_window_geometry(self, winid=None):
        if not self.xdotool:
            return 0, 0, 0, 0
        if winid is None:
            winid = self._window_id

        x = run([self.xdotool, "getwindowgeometry", winid], capture_output=True)
        m = pos_re.search(x.stdout.decode())
        if not m:
            log.error(
                f"xdotool cannot find position for {winid} window! (output was {x.stdout.decode()})"
            )
            return 0, 0, 0, 0

        xpos, ypos = list(map(int, m.groups()))
        log.debug(f"Position found for {winid} window: {xpos, ypos}")
        m = geom_re.search(x.stdout.decode())
        if not m:
            log.error(
                f"xdotool cannot find geometry for {winid} window! (output was {x.stdout.decode()})"
            )
            return 0, 0, 0, 0
            return
        width, height = list(map(int, m.groups()))
        log.debug(f"Geometry found for {winid} window: {width}x{height}")

        return xpos, ypos, width, height

    def snap_to_agdbentures(self):
        if not self.xdotool or Config.ARCADE_HEADLESS:
            return
        # make sure we have both windows ids
        self.get_code_window_id()
        self.get_agdb_window_id()

        # ax, ay, awidth, aheight = self.get_window_geometry(self._agdbwindow_id)
        log.debug("SCANNING FOR AGDBENTURES WINDOW DATA")
        ax, ay = self.agdb_window.get_location()
        log.debug(f"Position found for {self._agdbwindow_id} window : {ax, ay}")
        awidth = self.agdb_window.width
        aheight = self.agdb_window.height
        log.debug(f"Geometry found for {self._agdbwindow_id} window : {awidth}x{aheight}")

        log.debug("SCANNING FOR VIM CODE WINDOW DATA")
        cx, cy, cwidth, cheight = self.get_window_geometry()

        # snap code window on the left side of agdbentures window
        if abs(cy - ay) < 5 and abs(ax - cwidth - cx) < 5:
            # already snapped, with a 5 pixels tolerance
            return

        # new_cwidth = 400
        new_cwidth = cwidth  # we do not resize, but this might change in the future
        border_x = 5
        new_ypos = 0 + 20

        if (
            GenServerWindow.window_alignment == 'nothing'
            or self._screen_width == 0
            or self._screen_height == 0
        ):
            return
        if GenServerWindow.window_alignment == 'right':
            ## moving code window to the right edge of the screen, and agdb window on the left of it
            new_xpos = (
                self._screen_width + self._screen_offset_x - new_cwidth - border_x
            )
            self.move_window(new_xpos, new_ypos)
            self.move_window(
                new_xpos - cst.Win.INIT_W - border_x,
                new_ypos,
                winid=self._agdbwindow_id,
            )

        elif GenServerWindow.window_alignment == 'left':
            new_xpos = self._screen_offset_x + border_x
            ## moving code window to the left edge of the screen, and agdb window on the left of it
            self.move_window(new_xpos, new_ypos, winid=self._agdbwindow_id)
            self.move_window(new_xpos + cst.Win.INIT_W + border_x, new_ypos)

        # we do not resize, but this might change in the future
        self.resize_window(
            new_cwidth, aheight
        )  # resize to same height as arcade window
        log.debug(f"Resized {self._window_id} window : from {cwidth}x{cheight} to {new_cwidth}x{aheight}")

        # self.move_window(ax - new_cwidth//2, new_ypos)
        # self.move_window(ax + new_cwidth//2, new_ypos, winid=self._agdbwindow_id)

    def show_next_line(self, base: str, source_dir: str, line_number: int) -> None:
        """Highlight the next line in the remote window server."""
        if base == None:
            return
        source_file = os.path.join(source_dir, base)
        if source_file != self.source_file:
            # print ("CHANGING SOURCE FILE FROM", self.source_file, "TO", source_file)

            # Need to search for file either in the source directory, or in the
            # subdirectory 'engine'
            # This hack is a bit ugly, could we get the full path directly from gdb?
            if not os.path.exists(source_file):
                source_file = os.path.join(source_dir, 'engine', base)
                if not os.path.exists(source_file):
                    log.error(
                        f"Cannot show line in {source_file}, no such file exists!"
                    )
                    return

            if self._file_command:
                file_command = self._file_command.format(
                    command_base=self._command_base, source_file=source_file
                )
                self.source_file = source_file
                # log.debug(f"Running window file command '{file_command}'")
                # TODO get output error if needed
                p = run(shlex.split(file_command), capture_output=True)
                if p.returncode != 0:
                    log.error(f"Cannot make code window open file {source_file}")

        line_command = self._line_command.format(
            command_base=self._command_base,
            source_file=self.source_file,
            line_number=line_number,
        )

        try:
            p = run(shlex.split(line_command), check=True, capture_output=True)
            # log.debug(f"Return code window: {p}")
            if p.returncode != 0 or p.stderr != b'':
                log.error(f"Cannot make code window go to line {line_number}:")
                log.error(p.stderr.decode())

        except CalledProcessError as e:
            log.error(f"Error when telling code window go to line {line_number}: {e}")

    def terminate(self):
        run(["kill", str(self.pid)])


class VSCodeServerWindow(ServerWindow):
    def __init__(self, *args, **kwargs):
        self._launch_command = None
        super().__init__(*args, **kwargs)

        self._window_title = "vscode"
        self._command_base = "code -a " + Config.LEVEL_DIR
        self._line_command = "{command_base} -g {source_file}:{line_number}"


class VimServerWindow(ServerWindow):
    def __init__(self, *args, **kwargs):

        super().__init__(*args, **kwargs)
        self._window_title = "agdbcode"

        self._command_base = f"gview --servername {self._window_title}"

        # Note: did not manage to directly place gvim window where I want with
        # geometry (width)x(height)+xoff+yoff as gvim does not seem to care
        # about +xoff and +yoff offsets.
        # So we move the window afterwards
        self._launch_command = (
            self._command_base
            + f" -geometry {cst.Win.CODE_COLS}x{cst.Win.CODE_ROWS} --role agdbcode --echo-wid"
            + " --nofork -c 'source vim/vimrc'"
        )
        # " --nofork -u vim/vimrc"
        self._file_command = (
            "{command_base} --remote-expr 'execute(\"edit {source_file}\")'"
        )
        self._line_command = (
            "{command_base} --remote-expr 'HighlightLine({line_number})'"
        )

        self.launch()

    def _send_to_vim(self, cmd):
        command = f"{self._command_base} --remote-expr 'execute(\"{cmd}\")'"
        p = run(shlex.split(command))
        if p.returncode != 0:
            log.error(f"Cannot send {cmd} to vim window")


 

    def launch(self):
        super().launch()

        output = (
            self._process.stdout.readline()
        )  # read the line where gvim gives the WINID
        m = wid_re.search(output.decode())  # search WID in the line
        if not m:
            log.error(f"Cannot find window id for vim code window")
            return
        self._window_id = m.groups()[0]
        log.debug(f"Vim code window id: {self._window_id}")
        self.snap_to_agdbentures()

    def change_title(self):
        """
        If necessary, change the title of the gvim window (e.g., if we need to search by title with xdotool)
        """
        self._title_command = f"{self._command_base} --remote-expr 'execute(\"set titlestring={self._window_title}\")'"
        run(shlex.split(self._title_command))


    def reload(self):
        self._send_to_vim("edit")
