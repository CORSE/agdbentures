"""
Handles the loading of levels from actual files on disk.
-> make agdbenture level from source level directory
-> prepare level custom class and metadata
-> runs a level in test mode
"""

import os
import queue
from sys import argv
from signal import signal, SIGINT, SIGTERM
from os.path import dirname, splitext, exists, join, basename, isdir, isabs

import arcade

from logs import log
from utils import show_stack
from .loader_utils import import_from_file, get_dynamic_level, extract_metadata
import graphic.constants as cst

import subprocess
from config import Config


def make_level(level_name, test=False):
    """
    The test argument is used by the automatic testing of levels by tests/testLevels.py
    @return True if there was no problem.
    """
    PYTHON_COMMAND = "python3"

    assert not isabs(level_name), "Level name should not be an absolute path"


    log.info(f"making level {level_name}")
    compile_dir = os.path.join(Config.ROOT_DIR, Config.LEVEL_DIR, level_name)

    force = test or Config.MODE == 'test'

    # if the working directory already exists, no need to re-create the
    # level, unless we are in test mode
    if os.path.exists(compile_dir):
        if force:
            log.info(f"{compile_dir} already exists, but forcing to remake level")
        else:
            log.info(f"{compile_dir} already exists, not remaking it")
            return

    log.debug(f"creating directory {compile_dir}")
    if Config.MODE == 'test':
        modes = ["--test"]
    elif Config.MODE == 'dev':
        modes = ["--dev"]
    elif Config.MODE == 'answer':
        modes = ["--answer"]
    elif Config.MODE == 'normal':
        modes = [
            "--release"
        ]  # , "--answer"] does not seem useful to always also generate the answer
    else:
        log.error(f"Cannot make level, unknown mode '{Config.MODE}'")
        raise ValueError("Unknown make_level mode '{Config.MODE}'")

    success = True
    for mode in modes:
        log.debug(f"making level for mode {mode}")
        try:
            command = [
                PYTHON_COMMAND,
                join(Config.ROOT_DIR, "make_level.py"),
                "--no-compile",  # compilation will take place when starting the level
                mode,
                join(Config.ROOT_DIR, "levels", level_name),
                "--debug", str(Config.DEBUG),
            ]
            log.debug(f"Running command {command}")
            res = subprocess.run(
                command,
                capture_output=True,
                check=True,  # raises CalledProcessError if returs non-zero exit code
            )
            log.debug(res.stdout.decode())
            if res.stderr:
                log.debug("There where messages on stderr:")
                log.debug(res.stderr.decode())
        except subprocess.CalledProcessError as e:
            log.error(f"Command failed with return code {e.returncode}")
            if e.stdout:
                log.error(f"stdout is {e.stdout.decode()}")
            if e.stderr:
                log.error(f"stderr is {e.stderr.decode()}")
            else:
                log.error("Not output captured on stderr")
            raise e

def load_level(
    level_specific: type,
    level_type: str = "text",
    metadata: dict | None = None,
    level_in_queue=None,
    level_out_queue=None,
):
    """
    Dynamic level loader.

    :param level_specific: The class of the level.
    :param level_type: Visual interface that is either text, curses, arcade, or test.
    :param metadata: dict containing metadata for this level
    :param level_in_queue:  queue to send information to the level
    :param level_out_queue: queue where level can send information

    """
    raise NotImplementedError  # deprecated ???
    # Get dynamic class for this level, based on the level type.
    level_cls = get_dynamic_level(level_specific, level_type)
    if level_type == "arcade":
        raise NotImplementedError
        ## This part is deprecated, not used anymore to load the levels.
        ## Need to resurect it for pytest?
        from level.arcade_utils.loader import StandaloneLoader

        controler = StandaloneLoader(level_cls)
    else:
        controler = level_cls(metadata, level_in_queue, level_out_queue)
    return controler


def run_level(level_specific: type, filename: str, level_type: str = "text"):
    """
    Loads a level, automatically finding metadata, and runs it.
    """
    # Probably DEPRECATED
    raise NotImplementedError
    # metadata = get_metadata(filename)
    # controler = load_level(level_specific, level_type, metadata=metadata)
    # controler.run()


def prepare_level(full_level_name: str) -> tuple[type, dict]:
    """
    Takes the level name as argument, and prepares all metadata for this
    level, as well as create the level itself using make_level.

    :return: A couple (cls, metadata) of the custom level class for
    this level, as well as metadata for this level.
    """
    level_path = join(Config.ROOT_DIR, "levels", full_level_name)

    if not isdir(level_path):
        log.critical(f"Level directory does not exist: {level_path}")
        raise ValueError
        # TODO: maybe allow the program to continue?
        exit(1)

    # name without categories
    level_name = basename(full_level_name)

    # search the custom .py file for this level
    pyfile = join(level_path, level_name + ".py")
    if exists(pyfile):
        log.warning(f"Deprecated, '{pyfile}' found but 'level.py' is now preferred")
    else:
        lpyfile = join(level_path, "level.py")
        if not exists(lpyfile):
            raise FileNotFoundError(pyfile + " or " + lpyfile)
        pyfile = lpyfile

    log.debug(f"Custom python file for level: {pyfile}")

    # import Level class from python file
    level_cls = import_from_file(pyfile, "Level")

    # There should not be any metadata already set in class
    # (safety against old agdbenture code)
    assert not hasattr(level_cls, "metadata")

    mainfile = join(level_path, level_name + ".c")
    if not exists(mainfile):
        mainfile = join(level_path, "main.c")
    if not exists(mainfile):
        raise FileNotFoundError(mainfile)
    log.debug(f"Main source file {mainfile}")

    # load metada from .c or main.c file
    metadata = extract_metadata(mainfile)

    # make level will handle itself special cases
    # (existing directory, test mode, etc.)
    make_level(full_level_name)

    return level_cls, metadata
