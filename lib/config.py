import os.path
import yaml
from logs import log

from language import Lang, set_language


class Config:
    # Default language used
    LANGUAGE = Lang.EN

    ROOT_DIR = os.path.abspath(os.path.dirname(os.path.dirname(__file__)))

    MODE = 'normal'
    LEVEL_DIR = "working_directory"

    SPEED = 5
    LEFT = False
    WIN_MOVE = False

    # hide message_box in GUI, only keep prompt from the game
    SILENT = False

    CONSOLE = True
    SHOW_FPS = False  # Show a graph performance on the main window
    DEBUG = 1

    LEVEL = None

    @staticmethod
    def propagate():
        set_language(Config.LANGUAGE)

    @staticmethod
    def dev_mode():
        Config.MODE = 'dev'
        Config.LEVEL_DIR = "dev_directory"

    @staticmethod
    def test_mode():
        Config.MODE = 'test'
        Config.LEVEL_DIR = "test_directory"


    @staticmethod
    def answer_mode():
        Config.MODE = 'answer'
        Config.LEVEL_DIR = "answer_directory"

    @staticmethod
    def read_from_config_file(file = "config.yml") -> None:
        config = Config._read_file(file)
        if not config:
            return
        if 'language' in config:
            if config["language"] == "en":
                Config.LANGUAGE = Lang.EN
            elif config["language"] == "fr":
                Config.LANGUAGE = Lang.FR
            else:
                log.warning(f"Ignoring language value {config['language']} in config")
        if 'debug' in config:
            try:
                value = int(config["debug"])
                if value in [0, 1, 2, 3]:
                    Config.DEBUG = config["debug"]
                else:
                    log.warning(f"Ignoring debug value {value} in config (allowed: 0 to 3)")
            except:
                log.warning(f"Ignoring debug value {config['debug']} in config (integer expected)")

        if 'speed' in config:
            try:
                value = int(config["speed"])
                if 0 < value <= 100:
                    Config.SPEED = config["speed"]
                else:
                    log.warning(f"Ignoring speed value {value} in config (allowed: 0 to 100)")
            except:
                log.warning(f"Ignoring speed value {config['speed']} in config (integer expected)")

        if 'left' in config:
            Config.LEFT = (config['left'] == True)
        if 'win_move' in config:
            Config.WIN_MOVE = (config['win_move'] == True)
        if 'level' in config:
            Config.LEVEL = config["level"]
        if 'silent' in config:
            Config.SILENT = (config['silent'] == True)

    @staticmethod
    def _read_file(file):
        config_file = file
        try:
            with open(config_file, 'r') as f:
                try:
                    config = yaml.safe_load(f)
                    return config
                except yaml.YAMLError as ex:
                    log.error(f"Syntax error in yaml config file {config_file}")
                    return None
        except FileNotFoundError:
            log.info(f"No yaml configuration file found ({config_file}).")
            return None
