import logging
import os


class Colors:
    grey = "\x1b[38m"     #]
    yellow = "\x1b[33m"   #]
    red = "\x1b[31m"      #]
    brightred = "\x1b[31;1m"      #]
    blue = "\x1b[34m"     #]
    reset = "\x1b[0m"     #]


class ColoredFormatter(logging.Formatter):
    levels_colors = {
        logging.DEBUG: Colors.grey,
        logging.INFO: Colors.blue,
        logging.WARNING: Colors.yellow,
        logging.ERROR: Colors.red,
        logging.CRITICAL: Colors.brightred,
    }

    def __init__(self):
        super().__init__()
        format = "[%(levelname)s](%(name)s) %(message)s"
        self.formats = {}

        for lvl, color in self.levels_colors.items():
            self.formats[lvl] = color + format + Colors.reset

    def format(self, record):
        log_format = self.formats.get(record.levelno)
        formatter = logging.Formatter(log_format)
        return formatter.format(record)


class ColoredLogger(logging.Logger):

    def __init__(self, name):
        logging.Logger.__init__(self, name)

        formatter = ColoredFormatter()

        console = logging.StreamHandler()
        console.setFormatter(formatter)
        self.addHandler(console)


def get_loggers():
    return list(filter(lambda x: isinstance(x,ColoredLogger), globals().values()))

# /!\ using basicConfig will result in duplicated lines of log
def set_loggers_level(loglevel):
    """substitute for basicConfig"""
    logging.getLogger().setLevel(loglevel)
    for l in get_loggers():
        l.setLevel(loglevel)

if "AGDB_LOGS_NO_COLOR" not in os.environ or os.environ["AGDB_LOGS_NO_COLOR"] != "1":
    logging.setLoggerClass(ColoredLogger)

log = logging.getLogger("agdb")
cus = logging.getLogger("custom")
lvl = logging.getLogger("level")
chk = logging.getLogger("check")
trk = logging.getLogger("tracker")
tst = logging.getLogger("test")
mklog = logging.getLogger("mklevel")

# To activate timestamps + filename:line_number
# logging.basicConfig(format='%(asctime)s,%(msecs)03d %(levelname)-8s [%(filename)s:%(lineno)d] %(message)s',
    # datefmt='%Y-%m-%d:%H:%M:%S',
    # level=logging.DEBUG)
