"""

Arcade map informations

"""

import graphic.constants as cst

class Amap:

    def __init__(self, game_view, width=0, height=0, offset_x=0, offset_y=0):
        """
        """
        # TODO: offsets are now handled directly when loading the map, so we 
        # shouldn't need them anymore
        self.game_view = game_view
        self.width = width
        self.height = height
        self.offset_x = offset_x
        self.offset_y = offset_y

        self.scale: float = 1.0
        self.scaled_tile: float = cst.Tiles.TILE_SIZE

    def reset(self, width, height, off_x, off_y):
        self.change_scale(1.0)
        self.set_dimen(width, height)
        self.set_offset(off_x, off_y)

    def set_offset(self, off_x, off_y):
        self.offset_x = off_x
        self.offset_y = off_y

    def set_dimen(self, width, height):
        self.width = width
        self.height = height

    def change_scale(self, scale):

        self.scale = scale
        self.scaled_tile = cst.Tiles.TILE_SIZE * self.scale

    def __str__(self):
        return f"ArcadeMap(width {self.width}, height {self.height}, scale {self.scale}, scaled_tile {self.scaled_tile})"
