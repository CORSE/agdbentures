import arcade

from .title_screen import MainView
from .menu.category import CategoryView
from .menu.ingame import EndView, PauseView
from .game import GameView


def load_views():
    views = {}
    views["main"] = MainView()
    views["loader"] = CategoryView()
    views["game"] = GameView()
    views["end_game"] = EndView()
    views["pause_game"] = PauseView()
    return views
