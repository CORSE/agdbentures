"""Agdbentures' main view."""

import arcade
import arcade.gui
import arcade.key as KEY
import arcade.color as COLOR

from graphic.style import MenuButton, ConfirmationBox, MessageBox
from tests.testGUI import Reason, EventHandler
from .empty import EmptyView
from progress.progress_manager import progress
from language import message
from logs import log




# pylint: disable=unused-argument fixme
class MainView(EmptyView):
    """Starting, view of the GUI."""

    def __init__(self):
        super().__init__()
        
        self.v_box = arcade.gui.UIBoxLayout()

        # Game name
        text_agdb = arcade.gui.UITextArea(
            text="Agdbentures",
            font_size=24,
            font_name="Kenney Future",
            # font_name="square",
        )
        text_agdb.fit_content()
        self.v_box.add(text_agdb.with_space_around(bottom=100))
        self.start_button = MenuButton(text=message('PLAY'), width=200)
        self.start_button.on_click=self.on_click_play
        self.reset_button = MenuButton(text=message('RESET'), width=200)
        self.reset_button.on_click=self.on_click_reset
        self.quit_button = MenuButton(text=message('QUIT'), width=200, on_click=self.on_click_quit)
        self.quit_button.on_click=self.on_click_quit

        self.v_box.add(self.start_button.with_space_around(bottom=20))
        self.v_box.add(self.reset_button.with_space_around(bottom=20))
        self.v_box.add(self.quit_button.with_space_around(bottom=20))

        self.manager.add(
            arcade.gui.UIAnchorWidget(
                anchor_x="center_x", anchor_y="center_y", child=self.v_box
            )
        )

    def on_click_play(self, event):
        """Load callback function."""
        self.load_view("loader")
        EventHandler.notify(Reason.VIEW_LOADER)

    def on_click_reset(self, event):
        """Ask confirmation to reset all the level progression"""

        box = ConfirmationBox("Are you sure you want to reset progress?\n (all your data will be lost)",
                              self.do_reset_progress_if_ok)
        self.manager.add(box)

    def do_reset_progress_if_ok(self, event):
        if event == "Ok":
            progress.reset_progress()

    def set_editor_config(self, event):
        self.load_view("config_editor")

    def on_click_quit(self, event):
        """Quit callback function."""
        log.info("Quitting...")
        arcade.exit()
        EventHandler.notify(Reason.ARCADE_EXIT)

    def on_show_view(self):
        """View focus hook."""
        self.manager.enable()
        arcade.set_background_color(COLOR.DARK_BLUE_GRAY)

    def on_key_press(self, symbol, modifiers):
        if symbol == KEY.ESCAPE:
            self.on_click_quit(None)
        elif symbol == KEY.P:
            self.on_click_play(None)
        elif symbol == KEY.R:
            self.on_click_reset(None)
        elif symbol == KEY.Q:
            self.on_click_quit(None)



