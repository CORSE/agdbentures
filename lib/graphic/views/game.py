""" Game view. """

import os
import sys
import queue
import threading
import os.path
import time
from typing import Optional
from math import ceil
import subprocess
from threading import Event
from collections import deque


from pyglet.math import Vec2
import arcade
import arcade.gui
import arcade.key as KEY
import arcade.color as COLOR
from arcade.resources import add_resource_handle
from arcade.experimental.crt_filter import CRTFilter

from config import Config
from language import message
from logs import log
from tests.testGUI import Reason, EventHandler
from utils import show_threads
from level.objects import Object, LevelObjectsArea

from graphic.style import (
    MenuButton,
    DoneButton,
    UnLockedButton,
    BlinkButton,
    MessageBox,
    ErrorBox,
    ConfirmationBox,
    InputBox,
    GreedyLabelBox,
    OutputBox,
    draw_pause,
    draw_play,
    transparent_rect,
    LogBox,
)
import graphic.constants as cst
from .empty import EmptyView
import graphic.sprites.sp_objects as spo
from graphic.amap import Amap
from graphic.sprites.path import find_path, to_absolute, to_relative
from level.console import ConsoleInputThread
from level.action import Action
from progress.progress_manager import progress
from graphic.camera import AgdbCamera, CameraMode


# pylint: disable=fixme unused-argument too-many-instance-attributes too-many-branches
class GameView(EmptyView):
    """Game view class."""

    def __init__(self):
        super().__init__()
        self.key_pressed = None        
        self.waiting_for_input = False        
        self.vertical_padding = 10
        self.horizontal_padding = 20
        self.gui_buttons = arcade.gui.UIBoxLayout(vertical=True, align="left")
        self.back_button=None
        self.next_button=None
        self.step_button=None
        self.run_button=None
        self.time_last_ctrl_c: Float = 0.0  # to record two successive Ctrl-C
        self.level_cls: type
        self.metadata: dict
        self.level: LevelArcade = None
        self.tmx_maps = {}
        self.current_map = None
        self.tile_map: arcade.TileMap
        self.layers: dict[str, arcade.SpriteList]
        self.player: SpPlayer
        self.wop: NPC
        self.camera: AgdbCamera
        self.objects = None  # dictionnary for sprite objects
        self.characters = {}    # character objects
        self.has_started = False  # whether the inferior has started or not 
        self.executing = (
            False  # whether the inferior is executing (running or on pause) or not
        )
        self.on_pause = False  # whether the inferior is paused by gdb or running

        ##
        # Note : initially, is not started
        # Then is executing and has_started,
        # after program terminates, is not executing by still has_started
        self.code_window = None

        self.background_color = (80, 80, 110)
        # self.window.background_color = (120, 120, 150) # does not work, don't
        # know why

        # Loading resources
        self.resources_dir = os.path.join(Config.ROOT_DIR, "resources")
        add_resource_handle("characters", f"{self.resources_dir}/characters")
        add_resource_handle("tiles", f"{self.resources_dir}/tiles")
        add_resource_handle("maps", f"{self.resources_dir}/maps")
        add_resource_handle("effects", f"{self.resources_dir}/effects")
        add_resource_handle("fonts", f"{self.resources_dir}/fonts")
        add_resource_handle("window", f"{self.resources_dir}/window")
        add_resource_handle("buttons", f"{self.resources_dir}/buttons")

        arcade.load_font(":fonts:FuraCode.ttf")

        # queue for inter-thread communication.
        self.level_input_queue = queue.Queue()
        self.level_output_queue = queue.Queue()

        self.console_input_thread = ConsoleInputThread(self.level_input_queue)

        if Config.CONSOLE:
            self.console_input_thread.start()

        # Tiles and scales
        self.scale: float = 1.0
        self.scale_old: float = 1.0
        self.scaled_tile: float = cst.Tiles.TILE_SIZE

        # Cameras
        self.camera = AgdbCamera(self.window.width, self.window.height - (self.gui_buttons.height + self.vertical_padding * 2))

        self.camera_gui = arcade.Camera(self.window.width, self.window.height)
        # self.camera_gui.position = (Vec2(0, self.window.height * 2 / 3))

        # Text informations
        self.draw_text = False
        self.osd_message = None
        self.draw_help = False
        self.other_messages = []

        self.message_list = []
        self.box_queue = queue.Queue()
        self.message_box_close_hook = None

        self.other_actions_queue = deque()

        self.setup_gui()
        # A single box message for the whole game
        self.current_message_box: Optional[MessageBox] = None
        self.current_message_box_type = None

        # string to hold the messages of the inferior
        self.output_log = ""

        # Arcade map general information for objects
        self.amap = Amap(self)

        # Loading sprites for exit and breakpillar
        self.exit = spo.SpExit(
            ":effects:LightPillar/64x64/pipo-mapeffect013a-front.png",
            ":effects:LightPillar/64x64/pipo-mapeffect013a-back.png",
            amap = self.amap,
        )

        self.breaklight = spo.SpPillar(
            ":effects:LightPillar/64x64/pipo-mapeffect013b-front.png",
            ":effects:LightPillar/64x64/pipo-mapeffect013b-back.png",
            amap = self.amap,
        )

        self.teleport_in = spo.SpPillar(
            ":effects:LightPillar/64x64/pipo-mapeffect013c-front.png",
            ":effects:LightPillar/64x64/pipo-mapeffect013c-back.png",
            amap = self.amap,
        )

        self.teleport_out = spo.SpPillar(
            ":effects:LightPillar/64x64/pipo-mapeffect013c-front.png",
            ":effects:LightPillar/64x64/pipo-mapeffect013c-back.png",
            amap = self.amap,
        )

        self.backfrontobjects = [
            self.exit,
            self.breaklight,
            self.teleport_in,
            self.teleport_out
        ]

        # A sprite list for non moving sprites
        self.locked_sprites = arcade.SpriteList()

        if Config.SHOW_FPS:
            arcade.enable_timings()
            graph = arcade.PerfGraph(
                width=cst.Win.GRAPH_WIDTH, height=cst.Win.GRAPH_HEIGHT,
                graph_data="FPS",
                background_color=cst.Win.GRAPH_BACKGROUND_COLOR,
                axis_color=cst.Win.GRAPH_AXIS_COLOR,
                grid_color=cst.Win.GRAPH_GRID_COLOR
            )
            graph.center_x = cst.Win.GRAPH_WIDTH / 2   # bottom-left corner
            graph.center_y = cst.Win.GRAPH_HEIGHT / 2
            self.locked_sprites.append(graph)

        # Create a crt filter just for fun
        # Deactivated for now. If curious set the below flag to True
        self.crt_filter = CRTFilter(
            self.window.width,
            self.window.height,
            resolution_down_scale=6.0,
            hard_scan=-8.0,
            hard_pix=-3.0,
            display_warp=Vec2(1.0 / 32.0, 1.0 / 24.0),
            mask_dark=0.5,
            mask_light=1.5,
        )
        self.crt_filter_on = False

    def setup_gui(self):
        
        self.gui_first_l = arcade.gui.UIBoxLayout(vertical=False, align="left")
        self.gui_buttons = arcade.gui.UIBoxLayout(vertical=True, align="left")


        self.cmd_buttons = {}

        back_button = MenuButton(text="Menu")
        back_button.on_click = lambda _: self.load_view("pause_game")
        self.cmd_buttons["exit"] = back_button

        editor_open_button = BlinkButton(text="Edit")
        editor_open_button.on_click = self.open_editor
        self.cmd_buttons["edit"] = editor_open_button

        compile_button = MenuButton(text="Compile & Load")
        compile_button.on_click = self.compile_reload_level
        self.cmd_buttons["comp_reload"] = compile_button


        self.start_button = MenuButton(text="Restart")
        self.start_button.on_click = lambda _: self.trigger_restart_level()
        # start_button.on_click = lambda _: self.level.send_console_command("start")
        self.cmd_buttons["restart"] = self.start_button

        help_button = MenuButton(text="Help")
        help_button.on_click = lambda _: self.show_hide_help()
        self.cmd_buttons["help"] = help_button

        self.run_button = BlinkButton(text="Continue")
        self.run_button.on_click = lambda _: self.level.send_console_command("continue")
        self.cmd_buttons["continue"] = self.run_button

        interrupt_button = BlinkButton(text="Interrupt")
        interrupt_button.on_click = self.send_interrupt_inferior
        self.cmd_buttons["interrupt"] = interrupt_button

        self.next_button = BlinkButton(text="Next")
        self.next_button.on_click = lambda _: self.level.send_console_command("next")
        self.cmd_buttons["next"] = self.next_button

        self.step_button = BlinkButton(text="Step")
        self.step_button.on_click = lambda _: self.level.send_console_command("step")
        self.cmd_buttons["step"] = self.step_button


 

        self.manager.add(
            arcade.gui.UIAnchorWidget(
                anchor_x="left", anchor_y="top", child=self.gui_buttons
            )
        )

        # add a log box to the top right corner, in a new anchor widget so that
        # it is independent of the buttons (might overlap if bad resize)
        self.output_logbox = LogBox()
        self.manager.add(
            arcade.gui.UIAnchorWidget(
                anchor_x="right", anchor_y="bottom", child=self.output_logbox
            )
        )

    def adjust_buttons(self):
        """
        Deactivates some buttons if level does not yet allow them
        """

        self.gui_first_l.clear()
        self.gui_buttons.clear()
        self.gui_buttons.add(self.gui_first_l.with_space_around(top=self.vertical_padding, bottom=self.vertical_padding))

        if "available_commands" not in self.metadata:
            # activate all buttons
            self.metadata["available_commands"] = []
            for btnl in self.cmd_buttons:
                self.metadata["available_commands"].append(btnl)

        current_line = arcade.gui.UIBoxLayout(vertical=False, align="left")

        for cmd in self.metadata["available_commands"]:
            log.debug(f"Activating '{cmd}' buttons")
            if cmd == "edit":
                for edt in ["edit", "comp_reload"]:
                    self.gui_first_l.add(self.cmd_buttons[edt].with_space_around(left=self.horizontal_padding))
            elif cmd not in  ["exit", "edit", "comp_reload"]:
                b = self.cmd_buttons[cmd].with_space_around(left=self.horizontal_padding)
                if (current_line.width + b.width > self.window.width):
                    self.gui_buttons.add(current_line.with_space_around(bottom=self.vertical_padding))
                    current_line = arcade.gui.UIBoxLayout(vertical=False, align="left")
                current_line.add(b)

        self.gui_buttons.add(current_line)
        # align the menu button to the left by inserting a blanck space to push it at the left edge
        exit_button = self.cmd_buttons["exit"].with_space_around(left=self.horizontal_padding)
        while (self.gui_first_l.width <= self.window.width - exit_button.width - exit_button.width):
            self.gui_first_l.add(arcade.gui.UISpace(width=exit_button.width, height=0))
        
        
        self.gui_first_l.add(exit_button)  # always present

        for cmd in self.metadata["new_commands"] or []:
            log.debug(f"New command for level: {cmd}")
            self.cmd_buttons[cmd].start_blinking()

        for cmd in self.metadata["new_commands_during_level"] or []:
            log.debug(f"New command for level: {cmd}")
            self.cmd_buttons[cmd].start_blinking()


    def compile_reload_level(self, event, target='all', force=False):
        success = self.compile_level(target, force)
        self.waiting_for_input = False
        if success:
            self.trigger_restart_level()  # will also load the level
            self.code_window.reload()

        return success


    def compile_level(self, target='all', force=False):
        print("\n"+message('compiling/loading'))
        result = self.level.compile_source(capture_output=True, target=target, force=force)

        if result.returncode == 0:
            print(message('compilation/success'))
            return True

        # otherwise, show compilation errors
        msg = (
            message('compilation/error')
            + "\n"
            + result.stdout.decode()
            + "\n"
            + result.stderr.decode()
        )

        print(msg)
        self.box_append_queue('error', msg)
        return False

    # MOVED IN InGameView CLASS (MENU VIEW) #
    # def confirm_reset(self, event):
    #     """Ask confirmation to revert current level to original state"""
    #     box = ConfirmationBox(
    #         "Are you sure you want to reset this level?\n (all your modifications will be kept in a git branch)",
    #         self.do_reset_level_if_ok,
    #     )
    #     self.manager.add(box)
    # def do_reset_level_if_ok(self, event):
    #     # TODO: put in a different file, where player's code is handled
    #     if event != "Ok":
    #         return
    #     log.info(f"Resetting level {self.level.level_name}")
    #     subprocess.run(
    #         ["git", "commit", "-am", "'save before reset'"],
    #         cwd=self.level.source_level_dir,
    #     )
    #     subprocess.run(
    #         ["git", "reset", "--hard", "init"], cwd=self.level.source_level_dir
    #     )

    def open_editor(self, event):
        with open("config.editor") as f:
            command = f.readline()
        command = command.replace("<FOLDER>", Config.LEVEL_DIR)
        command = command.replace(
            "<FILE>", os.path.join(self.level.source_level_dir, "main.c")
        )
        log.debug(f"Opening in editor with command '{command}'")
        try:
            subprocess.run(command.split())
        except:
            log.error(f"Cannot run edit command '{command}'")


    def load_map_hook(self, map_name:str, tm: arcade.tilemap.tilemap.TileMap):
        """
        Hook to give to level class so it can give us back tmx objects after
        having loaded the map.
        """
        # Will be completed later, after level has finished being initialized,
        # by the `self.load_maps` method
        self.tmx_maps[map_name] = {
            "tile_map": tm,
            "scale": None,
        }


    def load_map(self, map_name, map_path: Optional[str] = None) -> None:
        """Load a map."""

        log.debug(f"Loading map {map_name}")

        assert map_name in self.tmx_maps

        tm = self.tmx_maps[map_name]["tile_map"]

        map_path = map_path or self.metadata["arcade_maps"][map_name]
        if not map_path.startswith(":maps:"):
            map_path = os.path.join(self.metadata["level_path"], map_path)

        # map_off_x, map_off_y = self.metadata["arcade_maps"].get(
            # map_name + "_offset", (0, 0)
        # )
        # tilemap_off = (
            # -map_off_x * cst.Tiles.TILE_SIZE,
            # -map_off_y * cst.Tiles.TILE_SIZE,
        # )

        # tm = arcade.load_tilemap(
            # map_path,
            # layer_options={"objects": {"custom_class": spo.SpObject},
                           # "characters": {"custom_class": spo.SpObject}
                          # },
            # offset=tilemap_off,
        # )  # type: ignore

        # Normally, directly handled during loading, so no need to keep this
        # information
        map_off_x = 0
        map_off_y = 0
        self.amap.reset(tm.width, tm.height, map_off_x, map_off_y)

        log.debug(f"Arcade metadata for level: {self.metadata['arcade_maps']}")
        log.debug(f"Objects metadata: {self.metadata['objects']}")
        log.debug(f"Offset of map: {map_off_x}x{map_off_y}")

        objects = {}

        # Initialize objects found the .tmx
        # This include characters, as they must have been added to objects upon
        # loading.
        for obj in tm.sprite_lists["objects"]:
            obj.set_amap(self.amap)
            if obj.coord_x < 0 or obj.coord_y < 0:
                obj.visible = False
            objects[obj.name] = obj

        coords = tm.object_lists["coordinates"]
        for c in coords.values():
            c.set_amap(self.amap)

        self.tmx_maps[map_name]["objects"] = objects
        self.tmx_maps[map_name]["coords"] = coords


    def load_maps(self):
        """

        """
        self.current_map = None

        for map_name in self.tmx_maps.keys():
            assert map_name in self.metadata["arcade_maps"]
            self.load_map(map_name)
        self.change_map("main")


    def change_map(self, map_name):
        """
        Setting currently displayed map to :param map_name:
        """
        log.debug(f"Changing to map {map_name}")

        if self.current_map:
            log.debug(f"Current map : {self.current_map}")
            if map_name == self.current_map:
                return

            # saving current scaling
            self.tmx_maps[self.current_map]["scale"] = self.scale

        self.current_map = map_name
        self.tile_map = self.tmx_maps[map_name]["tile_map"]
        self.objects = self.tmx_maps[map_name]["objects"]
        self.coords = self.tmx_maps[map_name]["coords"]

        log.debug(f"Objects in map:")
        for obj in self.objects.values():
            log.debug(f"\t{obj}")

        log.debug(f"Coordinates in map:")
        for c in self.coords.values():
            log.debug(f"\t{c}")

        arcade.set_background_color(self.tile_map.background_color)  # type: ignore
        self.amap.set_dimen(self.tile_map.width, self.tile_map.height)

        # Importing all layers from the tile map sprite lists
        try:
            self.layers = {
                layer: self.tile_map.sprite_lists[layer]
                for layer in [
                    "floor",
                    "decorations",
                    "decorations_top",
                    "walls",
                    "objects",
                ]
            }
            if "anim_objects" in self.tile_map.sprite_lists:
                self.layers["anim_objects"] = self.tile_map.sprite_lists["anim_objects"]

        except KeyError as e:
            log.error(f"Cannot load map {map_name}, missing layer {e.args[0]}")
            sys.exit(1)


        # for obj in self.objects.values():
        # log.debug(f"sp object: {obj}")
        # x, y = to_relative((obj.center_x, obj.center_y), self.amap)
        # log.debug(f"sp object rel location: {x}x{y}")
        # obj.coord_x = x
        # obj.coord_y = y

        self.scale = self.tmx_maps[map_name]["scale"]

        if not self.scale:
            # pretend that was a window resizing to recompute everything
            self.scale = 1.0
            self.on_resize(self.window.width, self.window.height)

    def rescale_map(self, scale) -> None:
        """Compute the scale and scale all the layers."""
        self.scale_old = self.scale
        self.scale = scale

        tile_relative_scale = self.scale / self.scale_old
        # for layer in self.layers.values():
        for lname, layer in self.layers.items():
            for tile in layer:
                if not isinstance(tile, spo.SpObject): # SpObjects have their own rescale that is called later in the game rescale
                    tile.rescale_relative_to_point((0, 0), tile_relative_scale)

    def compute_min_x_scale(self, win_width) -> float:
        """
        Minimum scale that does not show empty space on the left
        or right.
        Should not be used everywhere as it zooms too much.
        """
        map_width = int(self.tile_map.width - self.amap.offset_x)
        scale_x = float(win_width) / map_width / cst.Tiles.TILE_SIZE
        return scale_x

    def compute_scale(self, win_width, win_height) -> float:
        """Compute the sprites scale."""
        # win_width, win_height = self.window.get_size()

        # TODO get back to scaling later
        map_width = int(self.tile_map.width)
        map_height = int(self.tile_map.height)
        scale_x = float(win_width) / map_width / cst.Tiles.TILE_SIZE
        scale_y = (
            float(win_height - (self.gui_buttons.height + self.vertical_padding * 2)) / map_height / cst.Tiles.TILE_SIZE
        )

        scale_candidate = max(scale_x, scale_y)

        # Probably not useful anymore since min and max
        # scales are determined by width and height of
        # window compared to map.
        scale = max(
            cst.Tiles.MIN_AUTO_SCALE, min(cst.Tiles.MAX_AUTO_SCALE, scale_candidate)
        )
        # scale = max(scale, self.compute_min_x_scale(self.window.width))

        return scale

    def launch_level(self, code_window):
        """
        Launch a level already loaded in the window object from a file.
        - create a new object level
        - start its thread
        - prepare to receive instructions on the communication queue
        """
        self.key_pressed = None
        self.level_cls = self.window.level_cls
        self.metadata = self.window.metadata
        self.code_window = code_window

        # Change the skin to a 'bubble-like' object, to use agdbentures as a
        # "compute-it" simulator (cf. Toxicode company)
        if 'player_bubble' in self.metadata:
            self.player = spo.SpPlayerBubble(self.amap)
        else:
            player_skin = self.metadata["arcade_skins"]["player_main"]
            self.player = spo.SpPlayer(filename=player_skin, amap=self.amap)

        self.camera.player = self.player

        self.adjust_buttons()
        self.scale = None  # TODO: think what is necessary for scale

        # TODO: really need to recreate WOP? Should be the same for all levels
        # self.wop = spo.SpWop(self.metadata["arcade_skins"]["wop_main"], self.amap)
        # log.debug(f"created wop {self.wop}")
        # self.guard = spo.SpNPC(self.metadata["arcade_skins"]["guard"], self.amap)
        # log.debug(f"created guard {self.guard}")
        # self.knight = spo.SpNPC(self.metadata["arcade_skins"]["knight"], self.amap)
        # log.debug(f"created knight {self.knight}")

        # combine objects/coordinates of all maps to pass to the tracker
        sp_objs = {'exit': self.exit}
        sp_coords = {}

        for mp in self.tmx_maps.values():
            sp_objs |= mp["objects"]
            sp_coords |= mp["coords"]

        # Open or re-open code window if necessary
        if not Config.ARCADE_HEADLESS:
            code_window.check_or_reopen()
        ## Instantiate the level object, must be done before initializing maps
        # as objects will have the reference to the level_output_queue to send
        # payloads to the GUI

        # Clear the input queue
        while not self.level_input_queue.empty():
            item = self.level_input_queue.get()
            log.debug(f"clearing level input queue: {item}")

        # Clear old tmx maps
        self.tmx_maps.clear()

        # Instantiate the level object that will launch easytracker
        # We shutdown the previous level
        assert self.level is None  # will break soon...
        self.level = self.level_cls(
            self.metadata,
            code_window,
            self.level_input_queue,
            self.level_output_queue,
            input_thread=self.console_input_thread,
            load_map_hook=self.load_map_hook,
            # sp_objs,
            # sp_coords,
            window=self.window
        )
        self.console_input_thread.set_level(self.level)

        # Initializing WOP
        self.wop = spo.SpWop(filename=self.metadata["arcade_skins"]["wop_main"], amap=self.amap)
        log.debug(f"created wop {self.wop}")

        # Initializing maps
        self.load_maps()
        if 'map_width' in self.metadata:
            log.debug(
                f"Map width from metadata {self.metadata['map_width']} vs arcade {self.amap.width}"
            )
            assert not self.metadata['map_width'] or self.metadata['map_width'] <= self.amap.width
        else:
            self.metadata['map_width'] = self.amap.width
        if 'map_height' in self.metadata:
            log.debug(
                f"Map height from metadata {self.metadata['map_height']} vs arcade {self.amap.height}"
            )
            assert not self.metadata['map_height'] or self.metadata['map_height'] <= self.amap.height
        else:
            self.metadata['map_height'] = self.amap.height


        # Verify if level needs to be re-compiled
        # (e.g., if source more recent than executable)
        ret = self.compile_level()

        if not ret:
            log.error("Cannot launch level")
            return

        # Should not load here, will normally be handle in the level loop
        if not Config.ARCADE_HEADLESS:
            self.code_window.reload()

        self.set_reset_level()  # prepare the level with common code used also for restart

        # start the thread that will handle easytracker for this level
        self.level.start()

    def set_reset_level(self):
        """
        Initializes or re-initializes some objects for the level.
        Will be called then first starting a level, after the initial startup code,
        but also when 'restarting' a level currently being played.
        Should be called after a level has sent 'start' to gdb and already initialized some variables (e.g., player or exit position)
        """

        log.debug("Reseting level, hiding player, reseting camera, etc.")

        self.has_started = False
        self.executing = False
        self.on_pause = False

        self.draw_help = False

        # includes "exit"
        for o in self.backfrontobjects:
            o.reset()

        # Put the player in the correct visibility state
        self.player.set_reset()
        self.player.visible = self.level.player.visible
        self.player.set_relative_position(
            self.level.player.coord_x, self.level.player.coord_y
        )
        # exit should be a normal object now
        # self.exit.set_relative_position(
            # self.level.maps["main"].exit.coord_x, self.level.maps["main"].exit.coord_y
            # )
        # initial placement: show exit if not on map
        self.camera.reset()
        self.change_map("main")

        # Default: hide walls, the level.world is responsible for
        # showing them
        self.layers["walls"].visible = False

        # Hide NPCs
        # self.guard.visible = False

        for char in self.characters.values():
            char.set_reset()

        # clear output log
        self.output_logbox.reset()

        # Clear box queue
        self.message_list = []
        self.clear_box()  # remove any box that might still be open
        while not self.box_queue.empty():
            self.box_queue.get()
        self.message_box_close_hook = None
        self.other_actions_queue.clear()


    def post_start(self):
        """
        Must be called just after gdb has started the inferior, and the level has
        been filled with initial values from the inferior's memory.
        Currently, nothing to do as initial values e.g. for character
        are handled via passing *_update messages.
        """

        self.has_started = True
        self.executing = (
            True  # whether the inferior is executing (running or on pause) or not
        )


    def reset_level(self):
        """
        Reset a currently running level.
        Will be called when level 'decides' to do a restart
        """
        log.info(f"Resetting level to prepare restart...")

        self.executing = False
        self.set_reset_level()  # call common code that init/reinit a level

    def send_interrupt_inferior(self, event):
        self.level.interrupt_inferior()
        # self.level.interrupt_inferior_if_running()

    def trigger_restart_level(self):
        """
        When we want to restart a level: just send the information to the level
        class.
        """
        log.debug(f"Restarting level ('trigger_restart_level' from game_view)")
        self.level.interrupt_inferior_if_running()
        self.level.send_level_control("restart")

    def trigger_validation(self):
        """
        Trigger validation as if level was finished. For debugging purposes.
        """
        log.debug(f"Triggerring validation from game_view")
        if self.level.is_running:
            self.level.interrupt_inferior()
        self.level.send_level_control("validate")
        # self.level.restart()    # notify the level thread

    def on_show_view(self):
        """
        View focus hook.
        Does not restart the level when just showing, as we could not have a
        "pause" menu otherwise.
        """
        super().on_show_view()
        log.debug(f"Showing game view")

    def on_hide_view(self):
        super().on_hide_view()
        self.clear_box()

    ####
    ###
    ### Handling of boxes and messages
    ###
    ####
    def box_show_message_list(self, position=None):
        if len(self.message_list) == 1:
            self.box_message(self.message_list.pop(0), position=position)
            return
        self.box_message(
            self.message_list.pop(0), buttons=["more", "skip"], position=position
        )

    def box_message(self, message: str, error=False, buttons=["Ok"], position=None):
        """
        This method carry out the message box that can be use for the wop for
        example.
        If used to display an error to the user, set the :param error parameter to True

        # TODO: use position, currently, do not know how to do that, the
        # manager sets it on its own
        """
        if Config.SILENT:
            self.message_box_close_hook()
            return

        assert not self.current_message_box
        # if (
            # self.current_message_box
            # ):  # WARNING: the previous message box has been removed
        # self.manager.remove(self.current_message_box)

        mb = GreedyLabelBox(
            width=300,
            height=300,
            text=message,
            callback=self.close_message_box,
            buttons=buttons,
        )
        self.current_message_box = mb
        self.current_message_box_type = 'block'  # blocks interaction with gdb
        self.manager.add(self.current_message_box)
        # if not position:
            # mb.center_x = self.window.width / 2
        # mb.center_y = self.window.height / 2
        # mb.center_on_screen()
        # else:
            # log.debug(f"Message box at {position}")
        # mb.center = (position.coord_x, position.coord_y)
        # mb.center_on_screen()
        mb.center_on_screen()
        log.debug(f"Message box position {mb.center}")

    def clear_box(self):
        if not self.current_message_box:
            return
        self.manager.remove(self.current_message_box)
        self.current_message_box = None
        self.current_message_box_type = None

    def close_message_box(self, event) -> None:
        """Remove the box message if shown."""
        if not self.current_message_box:
            return
        log.debug(f"message box closed with event {event}")
        self.clear_box()

        run_hook = True
        if self.message_list:
            run_hook = False
            if event == "more":
                self.box_show_message_list()
            elif event.startswith("skip"):
                self.message_list = []
                run_hook = True
            elif event == "close":
                self.message_list = []
                run_hook = True
            else:
                log.error(f"Do not know how to handle event '{event}'")
                run_hook = True

        if run_hook and self.message_box_close_hook != None:
            self.message_box_close_hook()
            self.message_box_close_hook = None

    def input_box(self, prompt: str):
        assert not self.current_message_box
        # if (
            # self.current_message_box
            # ):  # WARNING: the previous message box has been removed
        # self.manager.remove(self.current_message_box) 
        it = InputBox(text=prompt, callback=self.close_input_box)
        self.current_message_box = it
        self.current_message_box_type = 'block'  # blocks interaction with gdb
        self.manager.add(it)  #  .with_border())
        self.key_pressed = None # force key release because it isn't captured when an inputbox is open 

    def close_input_box(self, answer):
        self.clear_box()
        self.waiting_for_input = False
        self.send_to_stdin(answer)
        self.level.prompt_ask = False

    def output_box(self, text: str):
        """
        Used to show some output from the inferior, e.g., strings shown using
        the 'message' function'
        """
        if Config.SILENT:
            return

        assert not self.current_message_box
        # if (
            # self.current_message_box
            # ):  # WARNING: the previous message box has been removed
        # self.manager.remove(self.current_message_box)
        lab = OutputBox(width=400, text=text)
        self.current_message_box = lab
        self.current_message_box_type = 'pass'  # any action will make it disappear
        self.manager.add(lab)  #  .with_border())

    def show_errors(self, message: str):
        assert not self.current_message_box
        # if (
            # self.current_message_box
            # ):  # WARNING: the previous message box has been removed
        # self.manager.remove(self.current_message_box)
        err = ErrorBox(message)
        self.manager.add(err)
        self.current_message_box = err
        self.current_message_box_type = 'block'

    def append_output_log(self, msg):
        """
        Add a new message to the output log box
        """
        self.output_logbox.append(msg)

    def box_queue_handle_next(self):
        if self.box_queue.empty():
            return

        if self.current_message_box is not None:
            if self.current_message_box_type == 'block':
                # log.debug("blocking box")
                return

            if self.current_message_box_type == 'pass':
                # log.debug("passing box")
                if self.current_message_box.active_box:
                    # log.debug("passing box is active")
                    return
                else:
                    self.clear_box()

        btype, msg, hook = self.box_queue.get()

        self.message_box_close_hook = hook

        if btype == 'talk':
            message_list = msg.split("\n\n\n")  # two consecutive empty lines
            self.message_list = msg.split("\n\n\n")  # two consecutive empty lines
            self.box_show_message_list(position=self.amap)

        elif btype == 'output':
            self.output_box(msg)

        elif btype == 'input':
            self.input_box(msg)

        elif btype == 'error':
            self.show_errors(msg)

    def box_append_queue(self, btype, msg, hook=None):
        """
        :param: hook can a be a function to be called when the box is closed
        """
        self.box_queue.put((btype, msg, hook))

    def append_input_box(self, prompt):
        self.box_append_queue('input', prompt)

    def append_output_box(self, message):
        self.box_append_queue('output', message)

    ###
    ### Handling resizing
    ###

    def rescale(self, scale):
        # TODO(flo): should not hard-code the list of objects to rescale
        self.rescale_map(scale)
        self.amap.change_scale(self.scale)

        self.player.rescale(self.scale)
        self.wop.rescale(self.scale)
        self.exit.rescale(self.scale)
        self.breaklight.rescale(self.scale)

        for obj in self.objects.values():
            obj.rescale(self.scale)
            log.debug(f"Rescaling {obj.name}")

        self.teleport_in.rescale(self.scale)
        self.teleport_out.rescale(self.scale)

        self.camera.resize(
            self.window.width, max(1, self.window.height - (self.gui_buttons.height + self.vertical_padding * 2))
        )
        self.camera_gui = arcade.Camera(self.window.width, self.window.height)
        # self.camera_gui.position = (Vec2(0, self.window.height * 2 / 3))

    def on_resize(self, width, height):
        """Window resize callback."""

        self.adjust_buttons() 
        new_scale = self.compute_scale(width, height)
        self.rescale(new_scale)

    ###
    ### Drawing methods, used for drawing frames
    ###

    def on_draw(self):
        """Render the screen."""
        if not self.has_started:
            return

        self.window.clear(self.background_color)

        if self.crt_filter_on:
            self.crt_filter.use()
            self.crt_filter.clear()

        self.camera.use()
        # Note: moving 'objects' such as NPC should also be on the 'object' layer
        for layer in ["floor", "decorations", "walls"]:
            if layer in self.layers:
                self.layers[layer].draw()

        # before objects/characters so they can be "inside"
        for o in self.backfrontobjects:
            o.draw_back()

        # Objects first so that panels, etc. are "behind" the player
        for layer in ["objects", "anim_objects"]:
            if layer in self.layers:
                self.layers[layer].draw()

        for obj in self.objects.values():
            if obj.health_bar_visible:
                obj.health_bar.draw()

        self.wop.draw()

        self.player.draw()
        if self.player.health_bar_visible:
            self.player.health_bar.draw()

        # after characters so they can be "inside"
        for o in self.backfrontobjects:
            o.draw_front()

        for layer in ["decorations_top"]:
            self.layers[layer].draw()

        for obj in self.objects.values():
            if hasattr(obj, 'foreground') and obj.foreground:
                obj.draw()

        if self.crt_filter_on:
            self.window.use()
            self.window.clear(self.background_color)
            self.crt_filter.draw()

        self.camera_gui.use()

        # Pause is independant of the camera location
        if self.on_pause:  # TODO: also check if executing?
            self.show_inferior_paused()
        else:
            self.show_inferior_running()

        self.print_osd_msg()
        self.print_other_msg()
        self.print_help_window()
        self.manager.draw()

        self.locked_sprites.draw()

    def show_inferior_paused(self):
        draw_pause(
            left=0,
            right=self.camera.viewport_width,
            top=self.camera.viewport_height,
            bottom=0,
        )

    def show_inferior_running(self):
        draw_play(
            left=0,
            top=self.camera.viewport_height,
        )

    ###
    ### Called at each tick. Will handle moving objects on the map.
    ###


    def on_update(self, delta_time):
        """Movement and game logic"""
        # First check if level thread is still running...
        if self.level and not self.level.is_alive():
            log.error(f"Level thread is not alive anymore!")
            # TODO: is that the cleanest way to do?
            # This way, we catches problems arising when the player
            # left the level in a state of non-compiling, but there
            # maybe other reasons for not having a level thread.
            self.window.views["end_game"].set_problem(
                "No inferior running\n\n"
                "If you just started the level maybe you left it in a \n"
                "state where the compilation failed?\n\n"
                f"Try going manually in {self.level.source_level_dir}\n"
                "and fix the problem or do a `git reset --hard init' there"
            )
            # TODO: do something like self.level.send_level_control('exit') 
            # maybe ?
            self.level = None
            self.load_view("end_game")
            return
            # raise RuntimeError("Level thread died unexpectedly")

        # regular colors when inferior is running
        # gray filter when inferior is stopped by gdb and in 'interactive' mode
        self.on_pause = False

        if self.metadata["interactive"]:
            # log.debug(f"Checking pause {self.level} and {self.level.is_running}").
            assert (
                self.level
            )  # why would there not be a level? mixup with level.tracker?
            if self.level and not self.level.is_running:
                self.on_pause = True

        # Update all regular sprites that are always present
        self.player.update(delta_time)
        self.wop.update(delta_time)
        self.exit.update(delta_time)
        self.breaklight.update(delta_time)
        self.teleport_in.update(delta_time)
        self.teleport_out.update(delta_time)

        if self.key_pressed != None and not self.player.in_action and not self.player.block_input and not self.level.prompt_ask:
            self.player.block_input = True
            if (self.key_pressed == KEY.RIGHT):
                self.send_to_stdin("RIGHT")
            elif (self.key_pressed == KEY.LEFT):
                self.send_to_stdin("LEFT")
            elif (self.key_pressed == KEY.UP):
                self.send_to_stdin("UP")
            elif (self.key_pressed == KEY.DOWN):
                self.send_to_stdin("DOWN")

        # Update animated sprites if any
        if "anim_objects" in self.layers:
            for sp in self.layers["anim_objects"]:
                sp.update_animation(delta_time)

        if (
            len(self.other_actions_queue) > 0
            and self.other_actions_queue[0]['action'].is_ready()
        ):
            self.start_next_action()

        for obj in self.objects.values():
            obj.update(delta_time)

        self.camera.update_state(self.tile_map, self.amap, delta_time)

        self.update_state()

        # Sometimes we are drawing text on screen
        # Here an ad-hoc hack to append dots during the validation process to
        # show the game is not frozen
        if self.draw_text:
            self.text_pause_time += delta_time
            if self.text_pause_time > 0.7:
                self.text_pause_time = 0
                self.draw_text_dots += "."

        # see if there is any box to show
        self.box_queue_handle_next()

        # Update collision: do we need to check for collision? Seems the role
        # of the inferior, not the gui...
        # TODO: use this
        # pylint: disable=unused-variable
        # hit_list = arcade.check_for_collision_with_list(
        # self.player, self.layers["walls"]
        # )

    def change_camera(self, payload):
        action = payload['action'].name
        if action == 'set_target':
            available_targets = self.objects | self.characters | {'wop': self.wop, 'player': self.player}
            self.camera.target = available_targets[payload['target']]
            self.camera.mode = CameraMode.TARGET_TRANSITION_ANIMATION

    def update_state(self):
        """
        Verify if there is any incomming packets to handle in the GUI.
        """
        while True:
            try:
                payload = self.level_output_queue.get_nowait()
            except queue.Empty:
                return
            log.debug(f"GUI received payload: {payload}")
            if payload is None:
                continue

            topic = payload["topic"]

            if topic == "quit":
                self.on_quit()
                return

            if topic == "first_start":
                self.post_start()

            elif topic == "pre_restart":
                self.reset_level()

            elif topic == "post_restart":
                self.post_start()

            elif topic == "reset":
                assert False

            elif topic == "cmd":
                self.gdb_command_update(payload["cmd"])
                # Camera follows closely player as soon as a first command
                # is entered
                self.center_to_player_speed = 1.0

            elif topic.endswith("_update"):
                who, _ = topic.split("_")
                self.object_update(who, payload)

            elif topic == "show_message":
                msg = payload["message"]
                self.append_output_log(msg)
                self.append_output_box(msg)

            elif topic == "ask_input":
                self.waiting_for_input = True
                msg = payload["prompt"]
                self.append_input_box(msg)

            elif topic == "inferior_exit":
                if ("action" in payload and type(payload["action"]) == Action and not payload["action"].is_ready()):
                    self.level_output_queue.put_nowait(payload)
                    return

                self.window.views["end_game"].source_dir = self.level.source_level_dir
                self.draw_text = False
                self.executing = False
                status= payload["action"].name
                if status == "validating":
                    self.start_loading_message(message('VALIDATING'))
                    return

                elif status == "success":
                    # unlock next levels
                    self.window.views["end_game"].set_victory()

                elif status == "failed":
                    self.camera.agdb_shake()
                    self.window.views["end_game"].set_defeat()
                elif status == "val_failed":
                    self.camera.agdb_shake()
                    self.window.views["end_game"].set_validation_failed(
                        payload["reasons"]
                    )
                else:
                    raise ValueError(f"Wrong payload status: {payload['status']}")

                # Show end game view
                self.load_view("end_game")
                EventHandler.notify(Reason.VIEW_END_GAME)

            elif topic == 'map_change':
                map_name = payload["map"]
                self.change_map(map_name)

            elif topic == 'sprites':
                self.append_action(payload)
            elif topic == 'gui_change':
                self.change_gui(payload)
            elif topic == 'bubble_player':
                self.change_bubble_player(payload)
            elif topic == 'camera':
                self.append_action(payload)
            else:
                raise ValueError("Payload topic not implemented: " + topic)

    def append_action(self, payload):
        self.other_actions_queue.append(payload)

    def start_next_action(self):
        assert len(self.other_actions_queue) > 0

        payload = self.other_actions_queue.popleft()
        action = payload["action"]
        topic = payload["topic"]
        if topic == "sprites":
            self.sprites_action(action, payload)
        elif topic == "camera":
            self.change_camera(payload)

        action.set_done()

    def change_gui(self, payload):
        action = payload["action"]
        if action == "show_button":
            cmd = payload["command"]
            if cmd not in self.metadata["available_commands"]:
                self.metadata["available_commands"].append(cmd)
                self.adjust_buttons()
        elif action == "unhide_button":
            cmd = payload["command"]
            self.metadata["hide_commands"].remove(cmd)
            self.adjust_buttons()
        elif action == "show_text":
            i = payload["index"]
            text = payload["text"]
            if i >= len(self.other_messages):
                while len(self.other_messages) < i:
                    self.other_messages.append("")
                self.other_messages.append(text)
            else:
                self.other_messages[i] = text
        elif action == "show_game":
            if self.window._current_view == self:
                return
            else:
                self.load_view("game")
        elif action == "change_health_bar":
            who, life = payload["who"], payload["life"]
            self.change_health_bar(who, life)
        else:
            raise NotImplementedError
        EventHandler.notify(Reason.GUI_CHANGE)

    def gdb_command_update(self, cmd):
        """
        A command was sent from level to gdb, see if there is anything to update
        in the GUI accordingly.
        """
        # New command received, clear the text
        # self.draw_text = False
        assert cmd != "q"  # should not pass through filters and arrive here...
        assert cmd != "quit"  # should not pass through filters and arrive here...
        assert cmd != "start"  # should not pass through filters and arrive here...
        log.debug(f"GUI received payload for gdb command {cmd}, but doing nothing")
        # if cmd == "start":
        # self.gui_restart_level()
        # return

        # self.remove_box("more") # if a box is present, send 'more' event

    def object_update(self, who, payload):
        is_player = False
        if who == "player":
            is_player = True
            obj = self.player
        elif who == "wop":
            obj = self.wop
        else:
            objname = payload["object_name"]

            if objname == "exit":
                obj = self.exit
            else:
                if objname in self.objects:
                    obj = self.objects[objname]
                elif objname in self.coords:
                    obj = self.coords[objname]
                else:
                    log.error(f"Cannot find object or coordinates {objname}")
                    log.error(
                        f"TODO: handle multi-map level objects/coordinates better"
                    )
                    return

        if "action" in payload:
            act = payload["action"].name
            if act == "fall" or act == "drown":
                obj.append_action({'action': Action('hit')})
                obj.append_action({'action': Action('disappear')})
            elif act == "change_skin":
                payload["skin"] = self.metadata["arcade_skins"][payload["skin"]]
                obj.append_action(payload)
            elif act == "change_color":
                obj.append_action(payload)
            elif act == "hit":
                obj.append_action(payload)
            elif act == "disappear":
                obj.append_action(payload)
            elif act == "talk":
                obj.append_action(payload)
            elif act == "turn":
                obj.append_action(payload)
            elif act == "set_reset":
                obj.set_reset()
            elif act == "move":
                pass
            elif act == "rescale":
                self.rescale(self.scale)
            elif act == "set_animation_frame":
                obj.append_action(payload)
            elif act == "play_animation":
                obj.append_action(payload)
            elif act == "set_angle":
                obj.append_action(payload)
            elif act == 'change/far_move':
                obj.append_action(payload)
            else:
                raise ValueError("Unknown action for payload: " + act)
            if act != "move":
                return

        # Only one change allowed per payload, otherwise there are problems
        # since the payloads would be aliases

        for ch in ['speed', 'far_move']:
            chkey = 'change/' + ch
            if chkey in payload:
                payload['action'] = Action(chkey)
                payload['value'] = payload[chkey]
                obj.append_action(payload)
                return

        if payload.get('talks', False):
            log.error("Must not use the 'talks' field anymore")
            return

        log.debug(f"... move action for {obj}")
        if 'action' not in payload:
            payload['action'] = Action('move')
        obj.append_action(payload)
        return

    def sprites_action(self, action, payload):
        if action.name == 'hide':
            layer = self.layers[payload['layer']]
            # search for corresponding sprites
            if "locations" not in payload:
                # apply to the whole layer
                layer.visible = False
                return
            for x, y in payload["locations"]:
                point = to_absolute(x, y, self.amap)
                sprs = arcade.get_sprites_at_point(point, layer)
                log.debug(f"Found sprites at {point}: {sprs}")
                for s in sprs:
                    s.visible = False
            return
        if action.name == 'show':
            layer = self.layers[payload['layer']]
            # search for corresponding sprites
            if "locations" not in payload:
                # apply to the whole layer
                layer.visible = True
                return
            for x, y in payload["locations"]:
                point = to_absolute(x, y, self.amap)
                sprs = arcade.get_sprites_at_point(point, layer)
                log.debug(f"Found sprites at {point}: {sprs}")
                if sprs:
                    for s in sprs:
                        s.visible = True
                else:
                    # create a default wall sprite
                    assert payload['layer'] == "walls"
                    wall = spo.create_default_wall(self.amap, x, y)
                    layer.append(wall)
            return

        if action.name == "create":
            if payload["type"] == "teleport_in":
                self.teleport_in.set_relative_position(payload["x"], payload["y"])
                self.teleport_in.reappear()
                return

            if payload["type"] == "teleport_out":
                self.teleport_out.set_relative_position(payload["x"], payload["y"])
                self.teleport_out.reappear()
                return
            assert payload["type"] == "wall"
            x = payload["x"]
            y = payload["y"]
            # First, check if there is an invisible wall sprite at this
            # location
            xpx, ypx = to_absolute(x, y, self.amap)
            sps = arcade.get_sprites_at_point((xpx, ypx), self.layers["walls"])

            if sps:
                log.debug(
                    f"Found {len(sps)} sprites at location {x, y} making the first visible"
                )
                sps[0].visible = True
                return

            wall = spo.create_default_wall(self.amap, x, y)
            self.layers["walls"].append(wall)
            return

        if action.name == 'hide_layer':
            layer = payload['layer']
            for sp in self.layers[layer]:
                log.debug(f"Hiding sprites {sp} from {layer}")
                sp.visible = False
            return

        if action.name == 'show_layer':
            layer = payload['layer']
            for sp in self.layers[layer]:
                log.debug(f"Hiding sprites {sp} from {layer}")
                sp.visible = True
            return

        raise NotImplementedError(f"Sprite action {payload['action']}")

    def change_bubble_player(self, payload):
        assert type(self.player) == spo.SpPlayerBubble
        if payload['action'] == 'next_color':
            self.player.next_color()
        elif payload['action'] == 'prev_color':
            self.player.prev_color()
        else:
            raise NotImplementedError(f"Bubble player action {payload['action']}")

    def start_loading_message(self, msg):
        self.text_pause_time = 0
        self.draw_text_dots = ""
        self.draw_text = True
        self.osd_message = msg

    def print_osd_msg(self):
        """
        This method prints a message directly on the interface.
        If the message is Victory the print is big, green, in the middle of the screen.
        If the message is Defeat the print is big, Red, in the middle of the screen.
        If the message is Validating the print is medium, white, in the middle of the screen, with blinking dots
        else : The message is white, small, at the top of the screen.
        """
        if not self.draw_text:
            return
        win_width, win_height = self.window.get_size()
        start_x = win_width / 3
        start_y = (win_height / 7) * 4.7
        color = COLOR.WHITE
        font_size = 40
        width = 80
        align = "left"
        anchor_x = "center"
        anchor_y = "center"
        msg = self.osd_message

        if self.osd_message == message('DEFEAT'):
            color = COLOR.RED
            align = "center"
        elif self.osd_message == message('VICTORY'):
            color = COLOR.GREEN
            align = "center"
        elif self.osd_message == message('VALIDATING'):
            color = COLOR.WHITE
            font_size = 20
            align = "center"
            anchor_x = "center"
            width = win_width * 9 / 10
            start_x = win_width / 2
            msg += self.draw_text_dots

        else:
            start_x = win_width / 9
            start_y = (win_height / 7) * 5
            font_size = 12
            width = 500
            anchor_x = "left"
            anchor_y = "bottom"

        arcade.draw_text(
            msg,
            start_x,
            start_y,
            color,
            font_size,
            width,
            align=align,
            font_name=cst.Fonts.FURACODE,
            anchor_x=anchor_x,
            anchor_y=anchor_y,
            multiline=True,
        )


    def print_help_window(self):
        if not self.draw_help:
            return

        # Transparent background for better readability
        arcade.draw_lrtb_rectangle_filled(
            0,
            self.window.width,
            self.window.height - (self.gui_buttons.height + self.vertical_padding * 2),
            0,
            cst.Win.HELP_BACKGROUND,
        )

        # Keyboard shortcuts
        start_x = 20
        start_y = self.window.height - (self.gui_buttons.height + self.vertical_padding * 2) - 20 * 2
        color = cst.Win.HELP_TEXT_COLOR
        font_size = cst.Win.HELP_TEXT_SIZE_CONTENT
        width = 500
        title_font_size = cst.Win.HELP_TEXT_SIZE_TITLE
        title = message('HELP_TITLE_KEYBOARD_SHORTCUTS')
        arcade.draw_text(
            title,
            start_x,
            start_y,
            color,
            title_font_size,
            width,
            font_name=cst.Win.HELP_TEXT_FONT,
            bold=cst.Win.HELP_TEXT_BOLD_TITLE,
            multiline=True,
        )

        start_y -= 30
        help_message = message('HELP_KEYBOARD_SHORTCUTS')
        arcade.draw_text(
            help_message,
            start_x,
            start_y,
            color,
            font_size,
            width,
            font_name=cst.Win.HELP_TEXT_FONT,
            bold=cst.Win.HELP_TEXT_BOLD_CONTENT,
            multiline=True,
        )

    # MOVED IN InGameView (MENU) #
    def show_hide_help(self):
        self.draw_help = not self.draw_help

    def print_other_msg(self):
        """
        This method prints a message directly on the interface.
        """

        if self.other_messages == []:
            return

        win_width, win_height = self.window.get_size()

        # The messages are printed one after the other, with a black background
        for index, msg in enumerate(self.other_messages):
            if msg == None or msg == "":
                continue

            # The base is the same for all messages
            color = COLOR.WHITE
            start_x = win_width * 0.2
            start_y = win_height * 0.55 - index * 27
            font_size = 18
            width = win_width * 5 / 10
            align = "center"
            anchor_x = "center"
            anchor_y = "center"

            # Draw a black rectangle behind the message
            arcade.draw_rectangle_filled(
                start_x,
                start_y,
                width * 0.55,
                font_size * 1.5,
                arcade.color.BLACK + (150,),
            )

            # Do specific things depending on the message
            if msg.startswith("Gold:"):
                # Get the gold value
                msg = int(msg.split(":")[1])

                coinIcon = arcade.Sprite("./resources/icons/coin.png")
                coinIcon.center_x = start_x - width * 0.2
                coinIcon.center_y = start_y
                coinIcon.width = 84
                coinIcon.height = 84
                if coinIcon not in self.locked_sprites:
                    self.locked_sprites.append(coinIcon)

                color = COLOR.GOLD

            elif msg.startswith("Weapon:"):
                # Get the weapon type
                msg = msg.split(":")[1]

                weaponIcon = arcade.Sprite("./resources/icons/sword.png")
                weaponIcon.center_x = start_x - width * 0.2
                weaponIcon.center_y = start_y
                if weaponIcon not in self.locked_sprites:
                    self.locked_sprites.append(weaponIcon)

                color = COLOR.RED

            elif msg.startswith("Armor:"):
                # Get the armor type
                msg = msg.split(":")[1]

                armorIcon = arcade.Sprite("./resources/icons/armor.png", 0.8)
                armorIcon.center_x = start_x - width * 0.2
                armorIcon.center_y = start_y
                if armorIcon not in self.locked_sprites:
                    self.locked_sprites.append(armorIcon)

                color = COLOR.RED

            # Draw the text
            arcade.draw_text(
                msg,
                start_x,
                start_y,
                color,
                font_size,
                width,
                align=align,
                font_name=cst.Fonts.FURACODE,
                anchor_x=anchor_x,
                anchor_y=anchor_y,
                multiline=True,
            )

    def change_health_bar(self, who, life):
        # Life bar
        bar_color = COLOR.FRENCH_LIME
        if who == "player":
            self.player.health_bar_visible = True
            self.player.health_bar.fullness = life / 100 if life > 0 else 0
            self.player.health_bar.full_color = bar_color
        else:
            if who in self.objects:
                self.objects[who].health_bar_visible = True
                self.objects[who].health_bar.fullness = life / 100 if life > 0 else 0
                self.objects[who].health_bar.full_color = bar_color

    #
    # Handling events.
    # Interaction methods, mouse, keypresses, and how to communicate with
    # inferior via it's stdin.
    #

    def on_mouse_press(self, px_x, px_y, button, modifier):
        low_x, low_y = self.camera.position  # bottom left corner of camera
        x, y = to_relative((low_x + px_x, low_y + px_y), self.amap)

        if self.current_message_box:
            self.current_message_box.show_all_message()
            return

        if button == arcade.MOUSE_BUTTON_RIGHT and "magic_break" in self.metadata:
            # check if click is on the map
            if x < 0 or x > self.amap.width or y < 0 or y > self.amap.height:
                return
            # arcade.MOUSE_BUTTON_RIGHT, arcade.MOUSE_BUTTON_MIDDLE
            if self.breaklight.is_at(x, y) and self.breaklight.visible:
                self.breaklight.disappear()
            else:
                self.breaklight.reappear()
                self.breaklight.set_relative_position(x, y)
                self.level.send_level_control(f"magic_breakpoint {x} {y}")
            return

    def on_mouse_drag(self, x, y, dx, dy, button, modifier):
        if button == arcade.MOUSE_BUTTON_LEFT:
            # dragging the map using the camera
            x, y = self.camera.center
            self.camera.mode = CameraMode.FREE
            self.camera.center_to_position(x - 2 * dx, y - dy, self.tile_map, self.amap)

    def on_mouse_scroll(self, px_x, px_y, scroll_x, scroll_y):
        """
        Used for zooming in and out.
        """
        if scroll_y == 0:
            return

        if scroll_y > 0:
            # zoom in
            scale = self.scale * 1.05
            scale = min(scale, cst.Tiles.MAX_SCALE)
            # scale = max(scale, self.compute_min_x_scale(self.window.width))
        else:
            # zoom out
            scale = self.scale / 1.05
            scale = max(scale, cst.Tiles.MIN_SCALE)
            # scale = max(scale, self.compute_min_x_scale(self.window.width))

        log.debug(f"Setting scale to {scale}")

        self.rescale(scale)

    def on_key_press(self, symbol, modifiers):
        """
        Called when the user presses a key.
        Note there is a a different behaviour when a box is open (e.g., wop's message)
        """
        # Keys meant only for gui
        if symbol == KEY.ESCAPE:
            if self.current_message_box:
                if self.current_message_box_type != 'block':
                    self.close_message_box('skip')
                else:
                    log.debug(
                        "Message box is present, not passing key Escape to inferior"
                    )
            elif self.draw_help:
                self.show_hide_help()
            else:
                self.load_view("pause_game")
            

            return

        # Handle all keys with 'Ctrl' modifier here
        # These are not handle by input boxes anymay

        if symbol == KEY.T and modifiers & KEY.MOD_CTRL:  # for fun, add a TV filter to the screen
            self.crt_filter_on = not self.crt_filter_on
            return

        if symbol == KEY.I and modifiers & KEY.MOD_CTRL:  # for 'i'nput
            # load first "*.in" file in current level directory, or
            # file 'default.in' in base directory

            save_speed = Config.SPEED
            Config.SPEED = 100
            file = os.path.join(self.level.source_level_dir, "default.in")
            if os.path.exists(file):
                self.load_input_file(file)
            else:
                self.load_input_file(os.path.join(Config.ROOT_DIR, "default.in"))
            Config.SPEED = save_speed
            return

        if symbol == KEY.M and modifiers & KEY.MOD_CTRL:  # for 'm'ake
            self.compile_reload_level(None)
            return

        if symbol == KEY.B and modifiers & KEY.MOD_CTRL:  # for 'b'ug
            log.info(f"Recompiling current level {self.level.level_name} in 'bug' mode")
            self.compile_reload_level(None, target='bug', force=True)
            return

        if symbol == KEY.A and modifiers & KEY.MOD_CTRL:  # for 'a'nswer
            log.info(f"Recompiling current level {self.level.level_name} in 'answer' mode")
            self.compile_reload_level(None, target='answer', force=True)
            return

        if symbol == KEY.F and modifiers & KEY.MOD_CTRL:  # for 'f'ix
            log.info(f"Recompiling current level {self.level.level_name} in one of the 'fix' modes")
            self.compile_reload_level(None, target='fix', force=True)
            return



        if symbol == KEY.R and modifiers & KEY.MOD_CTRL:  # restart level with Ctrl-R
            self.trigger_restart_level()
            return
        if (
            symbol == KEY.V and modifiers & KEY.MOD_CTRL
        ):  # trigger validation as if level was finished
            self.trigger_validation()
            return

        if symbol == KEY.SPACE:
            if self.current_message_box:
                self.current_message_box.show_all_message()
            return

        if self.current_message_box is not None:
            if self.current_message_box_type == 'block':
                # Do not continue interacting with level
                log.debug(
                    f"Message box is present, not passing key press {symbol} to inferior"
                )
                return
            else:
                self.close_message_box('more')

        if (
            modifiers & KEY.MOD_CTRL
            or modifiers & KEY.MOD_ALT
            or modifiers & KEY.MOD_SHIFT
        ):
            log.debug(f"Unknown key press {symbol} with modifier(s) {modifiers}")
            return

        if symbol == KEY.H:
            self.show_hide_help()
            return

        if symbol == KEY.P:  # only in 'bubble' mode (ComputeIt)
            if type(self.player) == spo.SpPlayerBubble:
                self.player.prev_color()
            else:
                log.debug(
                    "Cannot use 'P' key unless in special 'bubble' mode (ComputeIt)"
                )

        gdb_command = None
        inferior_command = None
        # Keys that interact with level
        if symbol == KEY.ENTER:
            gdb_command = ""
        elif symbol == KEY.C:
            gdb_command = "continue"
        elif symbol == KEY.F:
            gdb_command = "finish"
        elif symbol == KEY.N:
            gdb_command = "next"
        elif symbol == KEY.S:
            gdb_command = "step"
        elif symbol == KEY.T:
            inferior_command = "TAKE"
        elif symbol == KEY.A:
            inferior_command = "TOUCH"
        elif symbol == KEY.G:
            inferior_command = "GIVE"
        elif symbol == KEY.W:
            inferior_command = "WAIT"
        elif symbol == KEY.X:
            inferior_command = "EXIT"
        elif symbol == KEY.A:
            inferior_command = "COOK"  # TODO: make it into 'action', for now, cook to help debugging crepes level
        
        if self.player.in_action:
            return
        elif symbol == KEY.LEFT:
            direction = cst.Direction.LEFT
            self.key_pressed = KEY.LEFT
            inferior_command = "LEFT"
            return
        elif symbol == KEY.RIGHT:
            direction = cst.Direction.RIGHT
            self.key_pressed = KEY.RIGHT
            inferior_command = "RIGHT"
            return
        elif symbol == KEY.UP:
            direction = cst.Direction.UP
            self.key_pressed = KEY.UP
            inferior_command = "UP"
            return
        elif symbol == KEY.DOWN:
            direction = cst.Direction.DOWN
            self.key_pressed = KEY.DOWN
            inferior_command = "DOWN"
            return
        if self.level.is_recording:
            self.append_record(("input", dir_command))

        # Read a command and run it
        # if the inferior is running, put in on stdin
        # otherwise send command to gdb console
        
        if self.waiting_for_input or (self.current_message_box and self.current_message_box_type != "block"):
            return
        
        if gdb_command:
            if self.level.is_running:
                log.warn(message('sending/gdb/running').format(cmd=gdb_command))
            else:
                self.level.send_console_command(gdb_command)

        if inferior_command:
            if self.level.is_running:
                self.send_to_stdin(inferior_command)
            else:
                log.warn(message('sending/inf/pause').format(cmd=inferior_command))

    def on_key_release(self, symbol, modifiers):
        """
        Called when the user presses a key.
        Note there is a a different behaviour when a box is open (e.g., wop's message)
        """
        if symbol == self.key_pressed:
            self.key_pressed = None
            self.player.block_input = False

        
    def load_input_file(self, file):
        log.info(f"Using {file=} to send gdb commands and input")
        if not os.path.exists(file):
            log.error(f"Cannot find file {file} to use as input")
            return

        for l in open(file):
            if ">>>" in l:
                _, cmd = l.split(" >>> ")
            else:
                cmd = l
            log.debug(f"auto-command: {cmd}")
            if self.level.is_running:
                self.send_to_stdin(cmd)
            else:
                self.level.send_console_command(cmd)

    def on_quit(self) -> None:
        """Handle quit event."""
        self.console_input_thread.set_level(None)
        self.window.on_quit()

    def send_to_stdin(self, text):
        try:
            if self.level and self.level.tracker:
                # show the command in console as if it had been typed
                self.level.print_thread.show_inferior_stdin(text + '\n')
                log.debug(f"Sending string to stdin: {text.encode()}")
                self.level.tracker.send_to_inferior_stdin(text, append_newline=True)
        except:
            print("Error: trying to send a command but the level is closed")
