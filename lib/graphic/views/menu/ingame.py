"""
Views that are shown during or at the end of a level.
"""
import subprocess

import arcade
import arcade.gui
import arcade.key as KEY
import arcade.color as COLOR

from logs import log
from graphic.style import MenuButton, DoneButton, UnLockedButton, MessageBox, ConfirmationBox
from graphic.views.empty import EmptyView
import graphic.constants as cst
from language import message

from progress.progress_manager import progress
from tests.testGUI import EventHandler, Reason


# pylint: disable=fixme unused-argument
class GenericInGameView(EmptyView):
    """
    Views that allows to restart or quit the level.

    Scheme of the end view. Pause view is similar
    +-------------------------+
    |   Victory (or Defeat)   |
    |                         |
    |   Replay                |
    |   Next level            |
    |   Quit                  |
    +-------------------------+
    """

    def __init__(self, buttons_infos):
        """End view initialization."""
        super().__init__()
        self.buttons = {}
        self.can_go_next_level = True

        # Vertical layout
        self.v_box = arcade.gui.UIBoxLayout()

        self.source_dir = ""

        # Victory or Defeat message
        # self.result_lbl = arcade.gui.UITextArea(
        self.result_lbl = arcade.gui.UILabel(
            text="", font_size=24, font_name=cst.Fonts.KENNEY
        )
        self.top_pad = arcade.gui.UIWidget(height=-100)
        self.v_box.add(self.result_lbl.with_space_around(bottom=100))
        # pylint: disable=duplicate-code
        for label, callback in buttons_infos:
            button = MenuButton(text=message(label), width=200)
            button.on_click = callback
            self.buttons[label] = button
            self.v_box.add(button.with_space_around(bottom=20))
        self.details = arcade.gui.UITextArea(
            text="", font_size=16, width=850, height=400, font_name=cst.Fonts.FURACODE
        ).with_space_around(top=20)

        # self.v_box.add(self.details.with_space_around(top=20))

        # Managing the buttons
        self.manager.add(
            arcade.gui.UIAnchorWidget(
                anchor_x="center_x", anchor_y="center_y", child=self.v_box
            )
        )

    @property
    def result_text(self):
        """Get the result of a button."""
        return self._result_text

    @result_text.setter
    def result_text(self, value):
        """Set the content of the result button."""
        self.result_lbl.text = value
        self.result_lbl.fit_content()
        self._result_text = value

    @property
    def details_text(self):
        return self._details_text

    @details_text.setter
    def details_text(self, value):
        self.details.child.text = value
        self._details_text = value

    def on_show_view(self):
        super().on_show_view()
        arcade.set_background_color(COLOR.BLACK)  # a default color otherwise clear() might raise exception

    def on_update(self, delta_time):
        # continue updating game view so current animations can finish
        self.window.views["game"].on_update(delta_time)

    def on_draw(self):
        self.clear()
        # Draw the game behind with transparency
        self.window.views["game"].on_draw()
        arcade.draw_lrtb_rectangle_filled(
            left=0,
            bottom=0,
            right=self.window.width,
            top=self.window.height,
            color=self.bg_color + (200,),
        )
        self.manager.draw()

    def on_click_replay(self, event):
        """Replay callback function."""
        self.load_view("game")
        self.window.views["game"].trigger_restart_level()

    def on_click_edit(self, event):
        """Edit callback function."""
        with open("config.editor") as f:
            command = f.readline()
        command = command.replace("<FOLDER>", self.source_dir)
        try:
            subprocess.run(command.split())
        except:
            log.error(f"Cannot run edit command '{command}'")

    def on_click_confirm_reset(self, event):
        """Ask confirmation to revert current level to original state"""

        self.load_view("game") # switch to game view before asking for reinit

        box = ConfirmationBox(
            "Are you sure you want to reset this level?\n (all your modifications will be kept in a git branch)",
            self.do_reset_level_if_ok,
        )
        #self.manager.add(box)
        self.window.views["game"].manager.add(box)
    
    def do_reset_level_if_ok(self, event):
        if event != "Ok":
            return

        log.info(f"Resetting level {self.level.level_name}")
        subprocess.run(
            ["git", "commit", "-am", "'save before reset'"],
            cwd=self.level.source_level_dir,
        )
        subprocess.run(
            ["git", "reset", "--hard", "init"], cwd=self.level.source_level_dir
        )

    def show_hide_help(self):
        self.load_view("game") # switch to game view
        self.window.views["game"].draw_help = not self.window.views["game"].draw_help

    def on_click_quit(self, event):
        """Quit callback function."""
        self.window.on_quit()
        EventHandler.notify(Reason.QUIT_LEVEL)

    def on_click_next_level(self, event):
        if not self.can_go_next_level:
            return

        # nu = pm.next_undone()
        nl = progress.next_level()

        if not nl:
            # mb = MessageBox(width=300, 200, message_text = "There are no levels left !")
            mb = MessageBox(message_text="There are no levels left !")
            self.manager.add(mb)
            return

        level_name = nl.name
        log.info(f"Loading next level {level_name}")
        self.window.views["game"].on_quit()
        self.window.views["game"].window.start_level(level_name)


class EndView(GenericInGameView):
    def __init__(self):
        # Button creation
        buttons_infos = [
            ('REPLAY', self.on_click_replay),
            # ("Edit", self.on_click_edit),
            ('NEXT_LEVEL', self.on_click_next_level),
            ('MENU_RETURN', self.on_click_quit),
        ]
        super().__init__(buttons_infos)
        self.result_text = message('PAUSE')
        self.bg_color = COLOR.GRAY

    def add_details(self):
        if self.details not in self.v_box.children:
            self.v_box.add(self.details)
        if self.top_pad not in self.v_box.children:
            self.v_box.add(self.top_pad, index=0)

    def remove_details(self):
        self.details_text = ""
        for w in [self.details, self.top_pad]:
            if w in self.v_box.children:
                self.v_box.remove(w)

    def set_victory(self):
        self.result_text = message('VICTORY')
        self.bg_color = COLOR.GO_GREEN
        self.buttons['NEXT_LEVEL']._style["font_color"] = COLOR.WHITE
        self.can_go_next_level = True
        self.remove_details()

    def set_defeat(self):
        self.result_text = message('DEFEAT')
        self.bg_color = COLOR.ORANGE
        self.buttons['NEXT_LEVEL']._style["font_color"] = COLOR.GRAY
        self.can_go_next_level = False
        self.details_text = ""
        self.remove_details()

    def set_problem(self, reason:str):
        self.result_text = message('PROBLEM')
        self.bg_color = COLOR.RED
        self.buttons['NEXT_LEVEL']._style["font_color"] = COLOR.GRAY
        self.can_go_next_level = False
        self.add_details()
        self.details_text = reason

    def set_validation_failed(self, reasons):
        self.result_text = message('VALIDATION_FAILED')
        self.bg_color = COLOR.RED
        self.buttons['NEXT_LEVEL']._style["font_color"] = COLOR.GRAY
        self.can_go_next_level = False
        self.add_details()
        self.details_text = message('CHEATING')
        self.details_text += reasons

    def on_key_press(self, symbol, modifiers):
        if symbol == KEY.ESCAPE:
            self.on_click_quit(None)
        elif symbol == KEY.R:
            self.on_click_replay(None)
        elif symbol == KEY.N:
            if (
                modifiers & KEY.MOD_CTRL
            ):  # quick hack during development: Ctrl-N can always go to next level
                self.can_go_next_level = True
            self.on_click_next_level(None)


class PauseView(GenericInGameView):
    def __init__(self):
        # Button creation
        buttons_infos = [
            ('RETURN_LEVEL', lambda _: self.load_view("game")),
            ('HELP', lambda _: self.show_hide_help()),
            ('RESTART', self.on_click_replay),
            ('REINIT_LEVEL', self.on_click_confirm_reset),
            # ("Edit", self.on_click_edit),
            ('EXIT_LEVEL', self.on_click_quit),
        ]
        
        super().__init__(buttons_infos)
        self.result_text = message('PAUSE')
        self.bg_color = COLOR.GRAY

    def on_key_press(self, symbol, modifiers):
        if symbol == KEY.ESCAPE:
            self.load_view("game")
        elif symbol == KEY.R:
            self.on_click_replay(None)
        elif symbol == KEY.X or symbol == KEY.Q:
            self.on_click_quit(None)
