"""
Menu view used in main title to propose different choices.
Has a back button to return to the previous menu.
"""

import arcade.gui
import arcade.key as KEY

from graphic.style import MenuButton
from graphic.views.empty import EmptyView
from tests.testGUI import EventHandler, Reason


class MenuChoice(EmptyView):

    def __init__(self, parent: str = "main"):
        """
        Menu choice initialization.

        :param parent: the parent of the view.
        """
        super().__init__()

        self.parent = parent
        self.button_back = MenuButton(text="Back")
        self.button_back.on_click = self.on_click_back
        self.manager.add(
            arcade.gui.UIAnchorWidget(
                anchor_x="left",
                anchor_y="top",
                align_x=10,
                align_y=-10,
                child=self.button_back,
            )
        )

    # pylint: disable=unused-argument
    def on_click_back(self, event):
        """Back callback function."""
        self.load_view(self.parent)
        EventHandler.notify(Reason.VIEW_LOADER)

    def on_key_press(self, symbol, modifiers):
        if symbol == KEY.ESCAPE:
            self.on_click_back(None)


