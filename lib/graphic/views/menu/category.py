"""Level picker"""

import arcade
import arcade.gui
import arcade.key as KEY
import arcade.color as COLOR

from graphic.style import MenuButton, DoneButton, UnLockedButton
from config import Config
from tests.testGUI import Reason, EventHandler
from .choice import MenuChoice
from .level import LevelChoiceView

from progress.progress_manager import progress


# pylint: disable=unused-argument
class CategoryView(MenuChoice):
    """
    View to present the different level categories (tutorial, basic, etc.)
    """
    
    def __init__(self):
        super().__init__()
        self.button_map = {}
        self.category_box = arcade.gui.UIBoxLayout()
        self.shortkeys = {}
        unlocked = True
        for category in progress.categories:
            # check if the category is done
            done = category.is_done()
            key, button = self.create_category_button(category.name, done, unlocked)
            self.shortkeys[key] = button
            self.category_box.add(button.with_space_around(top=20))
            self.button_map[category.name] = button
            # if some of the levels are not done we don't unlock the next category
            unlocked = unlocked and done
        self.manager.add(
            arcade.gui.UIAnchorWidget(
                anchor_x="center_x", anchor_y="center_y", child=self.category_box
            )
        )

    def create_category_button(
        self, target: str, done: bool, unlocked: bool
    ) -> arcade.gui.UIFlatButton:
        """
        :param label: the button label
        :param target: category name
        :return: the button
        """
        locked = False
        if done:
            btncls = DoneButton
        elif unlocked:
            btncls = UnLockedButton
        else:
            # locked dark gray
            locked = True
            btncls = MenuButton

        button = btncls(
            text=target,
            width=200,
        )


        # add view switch callback to the category
        def view_switch(event):
            """Switch to the level choice view."""
            level_view = LevelChoiceView(target)
            self.window.show_view(level_view)
            self.window.views["level_choice"] = level_view
            EventHandler.notify(Reason.VIEW_LEVEL_CHOICE)

        # make the button clickable if unlocked level
        if not locked:
            button.on_click = view_switch

        key = ord(target[0].lower()) # get first letter of target and convert to a key
        return key, button

    def on_click_back(self, event):
        """Back callback function."""
        self.load_view("main")
        EventHandler.notify(Reason.VIEW_MAIN)

    def on_show_view(self):
        """View focus hook."""
        self.manager.children.clear()
        self.__init__()
        self.manager.enable()
        arcade.set_background_color(COLOR.DARK_BLUE_GRAY)

    def on_key_press(self, symbol, modifiers):
        if symbol == KEY.ESCAPE:
            self.on_click_back(None)
        else:
            if symbol in self.shortkeys:
                btn = self.shortkeys[symbol]
                btn.on_click(None)
