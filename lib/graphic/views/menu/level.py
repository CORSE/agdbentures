"""
Sublevel view

For levels that belong to one group (like tutorial)
"""

import os

from typing import Callable

import arcade
import arcade.gui
import arcade.key as KEY

from graphic.style import MenuButton, DoneButton, UnLockedButton
from .choice import MenuChoice

from progress.progress_manager import progress

import subprocess


def loader_button(level_name: str, win: arcade.Window) -> Callable:
    """Create a level button."""

    def callback(_):
        """Callback of the button."""
        # run the level
        win.start_level(level_name)

    return callback


# pylint: disable=fixme
class LevelChoiceView(MenuChoice):

    def __init__(self, target: str):
        """
        Propose all the levels in a given categore (tutorial, basic, etc.)

        :param target: The category that is accessible from this view.
        """
        super().__init__(parent="loader")


        self.vertical_box = arcade.gui.UIBoxLayout()
        self.button_map = {}
        category = progress.find_category(target)
        self.shortkeys = {}

        unlocked = True
        for level_line in category.levels_lines:
            horizontal_layout = arcade.gui.UIBoxLayout(vertical=False)
            for level in level_line.levels:
                key, button = self.create_level_button(
                    level.name, level.is_done(), unlocked
                )
                self.button_map[level.name] = button
                self.shortkeys[key] = button

                horizontal_layout.add(button.with_space_around(right=20))

            self.vertical_box.add(horizontal_layout.with_space_around(top=20))

            # if some of the levels are not done we don't unlock the next category
            unlocked = unlocked and level_line.is_done()

        self.manager.add(
            arcade.gui.UIAnchorWidget(
                anchor_x="center_x", anchor_y="center_y", child=self.vertical_box
            )
        )

    def create_level_button(self, level: str, done: bool, unlocked: bool, name=None):
        name = name or os.path.basename(level)

        locked = False
        if done:
            btncls = DoneButton
        elif unlocked:
            btncls = UnLockedButton
        else:
            # locked dark gray
            locked = True
            btncls = MenuButton

        button = btncls(
            text=name,
            width=250,
        )

        # make the button clickable if unlocked level
        if not locked:
            button.on_click = loader_button(level, self.window)

        if name[0] == '0':
            # For tutorial levels, do not use the leading 0 in 0?_...
            ch = name[1]
        else:
            ch = name[0]

        key = ord(ch.lower())  # get first letter of target and convert to a key
        return key, button

    def on_key_press(self, symbol, modifiers):
        if (
            modifiers & KEY.MOD_CTRL
            or modifiers & KEY.MOD_ALT
            or modifiers & KEY.MOD_SHIFT
            or symbol not in self.shortkeys
        ):
            super().on_key_press(symbol, modifiers)
            return

        btn = self.shortkeys[symbol]
        btn.on_click(None)
