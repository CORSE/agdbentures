"""
Empty view.
"""

import arcade
import arcade.gui
from pyglet.event import EVENT_HANDLED, EVENT_UNHANDLED
from logs import log

class DebugManager(arcade.gui.UIManager):
    """
    Class used to track what happens with events for debugging/understanding purposes
    """

    def on_event(self, event) -> bool:

        show=True
        if isinstance(event, arcade.gui.events.UIOnUpdateEvent):
            show=False
        if isinstance(event, arcade.gui.events.UIMouseMovementEvent):
            show=False

        if show:
            log.debug(f"Event in the manager {event}")
        layers = sorted(self.children.keys(), reverse=True)
        for layer in layers:
            for child in reversed(self.children[layer]):
                if child.dispatch_event("on_event", event):
                    log.debug(f"Dispatching event to {child}")
                    if show:
                        log.debug(f"Event consummed by {child}")
                    # child can consume an event by returning True
                    return EVENT_HANDLED
        return EVENT_UNHANDLED

class EmptyView(arcade.View):
    """Empty view containing the necessary attributes."""

    def __init__(self):
        super().__init__()

        self.manager = arcade.gui.UIManager()
        # self.manager = DebugManager()

    def load_view(self, view: str):
        """Load a new view."""
        new_view = self.window.views[view]
        self.window.show_view(new_view)

    def on_draw(self):
        """Update function."""
        self.clear()
        self.manager.draw()

    def on_show_view(self):
        """View focus hook."""
        self.manager.enable()

    def on_hide_view(self):
        """View unfocus hook."""
        self.manager.disable()
