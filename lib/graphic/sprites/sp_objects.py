"""Sprites implementation."""

from typing import Optional
from collections import deque
import inspect

import arcade
import pytiled_parser

from logs import log
import graphic.constants as cst
from graphic.amap import Amap
from .path import find_path, to_absolute, to_relative
from level.action import Action
from config import Config

import arcade

class HealthBar:
    def __init__(self, position, width, height, background_color, full_color, health):
        self._box_width: int = int(width)
        self._box_height: int = int(height)
        self._half_box_width: int = self._box_width // 2
        self._center_x: float = 0.0
        self._center_y: float = 0.0
        self._fullness: float = 0.0
        self.health: int = health
        self.full_color = full_color

        # Create the boxes needed to represent the indicator bar
        self._background_box: arcade.SpriteSolidColor = arcade.SpriteSolidColor(
            self._box_width + 2,
            self._box_height + 2,
            background_color,
        )
        self._full_box: arcade.SpriteSolidColor = arcade.SpriteSolidColor(
            self._box_width,
            self._box_height,
            self.full_color,
        )

        self.sprite_list = arcade.SpriteList()
        self.sprite_list.append(self._background_box)
        self.sprite_list.append(self._full_box)

        # Set the fullness and position of the bar
        self.fullness: float = self.health / 100
        self.position: tuple[float, float] = position

    def draw(self):
        self.sprite_list.draw()

    @property
    def fullness(self) -> float:
        return self._fullness

    @fullness.setter
    def fullness(self, value: float):
        if not (0.0 <= value <= 1.0):
            raise ValueError(
                f"Got {value}, but fullness must be between 0.0 and 1.0."
        )

        # Set the size of the bar
        self._fullness = value
        if value == 0.0:
            self._full_box.visible = False
            return
    
        # Set the full_box to be visible incase it wasn't then update the bar
        self._full_box.visible = True
        self._full_box.width = self._box_width * value
        self._full_box.left = self._center_x - (self._box_width // 2)

    @property
    def position(self) -> tuple[float, float]:
        return self._center_x, self._center_y

    @position.setter
    def position(self, value: tuple[float, float]):
        if value != self.position:
            self._center_x, self._center_y = value
            self._background_box.position = value
            self._full_box.position = value

            # Make sure full_box is to the left of the bar instead of the middle
            self._full_box.left = self._center_x - (self._box_width // 2)

    @property
    def visible(self) -> bool:
        return self._background_box.visible and self._full_box.visible
    
    @visible.setter
    def visible(self, value: bool):
        self._background_box.visible = value
        self._full_box.visible = value

class SpCoordinates:
    def __init__(self, name, amap=None):
        self.name = name
        self.amap = amap

        # NOTE: All these coordinates are using the coordinates of agdbentures
        #       (orthogonal axis centered in top left).
        #       But arcade uses cartesian coordinates. So we do some translation
        #       in all the arcade code.

        # Coordinates of the current relative (agdb) tile
        self.coord_x: int = None
        self.coord_y: int = None

        # coordinates of the sprite in pixels in arcade system
        self.center_x: float = 0
        self.center_y: float = 0

        # Coordinates of the absolute target tile if in movement
        self.target_x: int = self.coord_x
        self.target_y: int = self.coord_y

        # Coordinates of the previous tile in pixel
        self.prev_center_x: float = self.center_x
        self.prev_center_y: float = self.center_y

    def set_amap(self, amap: Amap | None):
        self.amap = amap

    def set_relative_position(self, pos_x, pos_y):
        """
        Set position of sprite on the relative coordinates
        """
        log.debug(f"Setting rel. position of {self} at {pos_x}x{pos_y}")
        if pos_x is None or pos_y is None:
            return
        self.coord_x = pos_x
        self.coord_y = pos_y
        self.target_x = None
        self.target_y = None
        px_x, px_y = to_absolute(
            pos_x, pos_y, self.amap, self.tl_offset_x, self.tl_offset_y
        )
        self.center_x = px_x
        self.center_y = px_y
        self.prev_center_x = px_x
        self.prev_center_y = px_y

    def __str__(self):
        return f"SpCoords({self.name} at {self.coord_x}x{self.coord_y})"


# pylint: disable=too-many-instance-attributes too-many-branches
class SpObject(arcade.Sprite, SpCoordinates):
    """Generic Sprite Object Class."""

    def __init__(self, *args, **kwargs):  # Note *args
        # kwargs['hit_box_algorithm'] = 'None'
        arcade.Sprite.__init__(self, *args, **kwargs)
        SpCoordinates.__init__(self, "GenericObject")

        self.health_bar_visible = False
        self.health_bar = HealthBar(
            (self.center_x, self.center_y + self.height / 2 + 10),
            int(self.width)*2,
            5,
            arcade.color.BLACK,
            arcade.color.RED,
            100,
        )
        self.block_input = False
        self.collision_radius = 0.0
        # offset to align sprite nicely, in %s of sprite size
        self.tl_offset_x: float = 0.0
        self.tl_offset_y: float = 0.0

        self.x_scale_factor: float = None
        self.y_scale_factor: float = None

        self.speed: float = 1.0

        self.visible = True
        self.action_queue = deque()
        # Add to this queue actions that need to be carried out by the object
        # in sequence

        self.set_reset()

    def __str__(self):
        return f"SpObject({self.name} at {self.coord_x}x{self.coord_y})"

    def set_relative_position(self, pos_x, pos_y, direct=None):
        super().set_relative_position(pos_x, pos_y)
        if direct:
            self.direction = direct
            self.idle_frame()

    def apply_scale_factor(self):
        if not self.x_scale_factor or not self.y_scale_factor:
            return

        self.clear_spatial_hashes()
        self._point_list_cache = None

        self._width *= self.x_scale_factor
        self._height *= self.y_scale_factor
        
        self.add_spatial_hashes()
        for sprite_list in self.sprite_lists:
            sprite_list.update_size(self)
    
    @arcade.Sprite.scale.setter
    def scale(self, new_value : float):
        """Overriding Arcade's setter to apply the scale factors to the width and height"""
        if new_value == self._scale:
            return

        arcade.Sprite.scale.fset(self, new_value)
        self.apply_scale_factor()
    
    @arcade.Sprite.width.setter
    def width(self, value):
        """Overriding Arcade's setter to catch the real size of the object as defined in the tmx"""
        arcade.Sprite.width.fset(self, value)
        if not self.x_scale_factor:
            self.x_scale_factor = self._width / self._scale / self.texture.width 
    
    @arcade.Sprite.height.setter
    def height(self, value):
        """Overriding Arcade's setter to catch the real size of the object as defined in the tmx"""
        arcade.Sprite.height.fset(self, value)
        if not self.y_scale_factor:
            self.y_scale_factor = self._height / self._scale / self.texture.height

    @arcade.Sprite.texture.setter
    def texture(self, value):
        arcade.Sprite.texture.fset(self,value)
        self.apply_scale_factor()

    def set_reset(self):
        self.moving: bool = False
        self.in_action: bool = False
        self.direction: cst.Direction = cst.Direction.RIGHT
        self.final_direction: Optional[cst.Direction] = None
        self.path: list[tuple[int, int]] = []

        # How to move object to a faraway distance (i.e., > 1 tile)
        # available values:
        # - teleport:  just change the position to destination
        # - crowflies: move the sprite in a straight-line
        # - pathfind:  find path avoiding walls
        self._far_move: str = 'crowflies'

        self.action_queue.clear()
        # function to call during update when performing this action
        # Takes one `delta_time` parameter which is a float (in seconds)
        self.action_hook = (
            None
        )
        self.end_action_hook = (
            None  # function to call after an action is finished (e.g., movement)
        )

        # Movement progression
        self.motion_progress: float = 0.0
        self.delta_x: float = 0.0
        self.delta_y: float = 0.0
        self.frame_rate_factor: float = 1.0  # higher means faster
        self.auto_dir = False
        self.color = arcade.color.WHITE

    @property
    def far_move(self):
        return self._far_move

    @far_move.setter
    def far_move(self, v):
        if v not in ['teleport', 'crowflies', 'pathfind']:
            raise ValueError(f"far_move cannot be set to {v}")
        self._far_move = v

    def is_at(self, pos_x, pos_y):
        """
        Return True if object is at given position
        """
        return self.coord_x == pos_x and self.coord_y == pos_y

    def get_position(self):
        return self.coord_x, self.coord_y

    # Default object has no frames
    def next_frame(self):
        pass

    def idle_frame(self):
        pass

    def append_action(self, payload):
        self.action_queue.append(payload)

    def update(self, delta_time: float):
        if self.in_action:
            assert self.action_hook != None
            self.action_hook(delta_time)
        elif len(self.action_queue) > 0 and self.action_queue[0]["action"].is_ready():
                self.start_next_action()

        if not self.visible:
            self.health_bar.visible = False
        elif self.health_bar_visible:
            if self.health_bar.fullness > 0:
                self.health_bar.visible = True

            self.health_bar.position = (self.center_x, self.center_y + self.height / 2 + 10)

    def delay_next_action(self):

        d = self.action_queue[0]["delay"]
        del self.action_queue[0]["delay"]  # to avoid recursion

        self.in_action = True
        self.waiting_time = d

        def wait_timeout(delta_time):
            self.waiting_time -= delta_time
            if self.waiting_time > 0:
                return
            self.in_action = False
            self.action_hook = None

        self.action_hook = wait_timeout


    def start_next_action(self):
        assert len(self.action_queue) > 0

        if 'delay' in self.action_queue[0]:
            self.delay_next_action()
            return

        payload = self.action_queue.popleft()
        action = payload['action']
        act_name = action.name
        log.debug(f"Next action {act_name} for {self}")

        if act_name == 'hit':
            self.start_hit(action)
        elif act_name == "change_skin":
            skin = payload["skin"]
            self.load_textures(skin)
            action.set_done()
        elif act_name == 'turn':
            num = payload['num']
            action = payload['action']
            self.start_turn(num, action)
        elif act_name == "change_color":
            color = payload['color']
            self.color = color
            action.set_done()
        elif act_name == 'disappear':
            self.disappear()
            action.set_done()
        elif act_name == 'reappear':
            self.reappear()
            action.set_done()
        elif act_name == 'talk':
            msg = payload['message']
            self.talks(msg, action)
        elif act_name == 'change/speed':
            self.frame_rate_factor = payload['value']
            action.set_done()
        elif act_name == 'change/far_move':
            self.far_move = payload['value']
            action.set_done()
        elif act_name == 'move':
            if self.handle_move_action(payload):
                action.set_done()
            else:
                assert self.end_action_hook is None # should not be already a hook
                log.debug(f"Setting hook action for {self}")
                self.end_action_hook = action.set_done
        elif act_name == 'rotate_90':
            self.rotate_90_animation()
            action.set_done()
        elif act_name == 'set_angle':
            self.angle = payload['value']
            action.set_done()
        elif act_name == 'set_animation_frame':
            assert(isinstance(self, SpTiledAnimatedObject))
            self.set_animation_frame(payload['value'])
            action.set_done()
        elif act_name == 'play_animation':
            assert(isinstance(self, SpTiledAnimatedObject))
            t = payload.get('times', 1)
            self.play_animation(times=t)
            self.end_action_hook = action.set_done
        else:
            log.error(f"Action {act_name} not found in action list")

    def start_turn(self, num, action):
        self.in_action = True
        self.num_turn = num
        self.action_hook = lambda _: self.turn_animation(action)

    def turn_animation(self, action):
        """
        Turn the object by 90 degrees num times.
        """
        if self.num_turn > 0:
            self.direction = cst.Direction((self.direction.value + 1) % 4)
            self.num_turn -= 1
            self.idle_frame()
            return

        self.in_action = False
        self.action_hook = None
        action.set_done()

    def start_hit(self, action):
        self.in_action = True
        self.action_hook = lambda _: self.hit_animation(action)
        self.hit_animation_red = True
        self.color = arcade.color.RED
        self.hit_animation_progress = 0.0
        self.hit_frame_count = 0

    def hit_animation(self, action):
        """
        Change the color tint if required.
        """
        self.hit_animation_progress += cst.Anim.HIT_SPEED * self.frame_rate_factor
        if self.hit_animation_progress < 1:
            self.hit_frame_count += 1
            if self.hit_frame_count > cst.Anim.HIT_UPDATE_RATE:
                self.hit_frame_count = 0
                self.hit_animation_red = not self.hit_animation_red
                if self.hit_animation_red:
                    self.color = arcade.color.RED
                else:
                    self.color = arcade.color.WHITE
            return

        # finish hit animation
        self.color = arcade.color.WHITE
        self.hit_animation_red = False
        self.in_action = False
        self.action_hook = None
        action.set_done()

    def update_position(self, delta_time):
        """
        Move the object if required.
        :param delta_time: ignored but necessary as used in action_hook
        """

        self.position = self.center_x, self.center_y

        # Too verbose to be kept. Uncomment if necessary.
        # log.debug(f"Current {self.name} px coordinates: {self.center_x, self.center_y}")
        # log.debug(f"Current motion_progress {self.motion_progress}")

        self.motion_progress += Config.SPEED * self.frame_rate_factor * self.speed

        if self.motion_progress < 1:
            self.next_frame()
            self.center_x = self.prev_center_x + self.delta_x * self.motion_progress
            self.center_y = self.prev_center_y + self.delta_y * self.motion_progress

        else:
            # We are at the current destination
            # Moving properly to the target tile

            self.center_x = self.prev_center_x + self.delta_x
            self.center_y = self.prev_center_y + self.delta_y
            # Updating states
            self.coord_x = self.target_x
            self.coord_y = self.target_y
            self.prev_center_x = self.center_x
            self.prev_center_y = self.center_y
            self.delta_x = 0
            self.delta_y = 0

            # any remaining movement on the path?
            if not self.path:
                self.stop_moving()
                self.auto_dir = False
            else:  # Moving to the next point of the path
                target_x, target_y = self.path.pop(0)
                abs_target = (target_x - self.coord_x, target_y - self.coord_y)
                self.move_forward(abs_target, self.auto_dir)

    def start_moving(self):
        self.in_action = True
        self.action_hook = self.update_position
        self.motion_progress = 0.0
        self.prev_center_x = self.center_x
        self.prev_center_y = self.center_y

    def stop_moving(self):
        self.block_input = False
        self.in_action = False
        self.action_hook = None

        if self.final_direction is not None:
            # Setting the final direction
            self.direction = self.final_direction
            self.final_direction = None
        # Setting the idle frame
        self.idle_frame()

        if self.end_action_hook is not None:
            self.end_action_hook()
            self.end_action_hook = None
            log.debug(f"Removing hook action for {self}")
        return

    def move_forward(self, abs_delta: Optional[tuple[int, int]] = None, auto_dir=False) -> bool:
        """
        Move by one tile according to the current direction.
        If the absolute delta is not given, guesses using the player's
        direction.

        :param abs_delta: the delta of the movement (in absolute tiles) +x is to the right, and +y down
        :param auto_dir:  change the direction of the sprite based on abs_delta

        :return True if the move is completed when this function returns, False if there is an ongoin animation
        """
        log.debug(
            f"Moving {self} forward by {abs_delta} from {self.center_x, self.center_y}"
        )
        if abs_delta is None:
            factor_x, factor_y = cst.DELTA[self.direction]
        else:  # Guessing the direction
            factor_x, factor_y = abs_delta
            # if factor_x and factor_y:
            # raise Exception(f"Illegal diagonal movement: {(factor_x, factor_y)}")

            if factor_x == 0 and factor_y == 0:  # No movement, let's skip it
                self.idle_frame()
                return True
            if auto_dir:
                if factor_x:
                    if factor_x < 0:
                        self.direction = cst.Direction.LEFT
                    else:
                        self.direction = cst.Direction.RIGHT
                elif factor_y:
                    if factor_y < 0:
                        self.direction = cst.Direction.UP
                    else:
                        self.direction = cst.Direction.DOWN

        self.delta_x = factor_x * self.amap.scaled_tile
        self.delta_y = (
            -factor_y * self.amap.scaled_tile
        )  # reverting for arcade coordinates

        self.target_x = self.coord_x + factor_x
        self.target_y = self.coord_y + factor_y
        log.debug(f"Target coordinates for {self}: {self.target_x}x{self.target_y}")
        log.debug(f"Initial px center for {self}: {self.center_x, self.center_y}")

        self.start_moving()
        return False

    def follow_path(
        self,
        path: list[tuple[int, int]],
        final_direction: Optional[cst.Direction] = None,
        auto_dir=False,
    ):
        """
        Make the object go to a given tile by following a path of tiles.

        :param path: List of point to follow.
        :param direction: The direction of the player at the end of the movement.
        """
        log.debug(f"Following path for object {self.name}: {path}")
        self.final_direction = final_direction
        self.delta_x = 0.0
        self.delta_y = 0.0
        self.target_x = self.coord_x
        self.target_y = self.coord_y
        self.path = path
        self.auto_dir = True
        self.start_moving()

    def handle_move_action(self, payload) -> bool :
        """
        Return 
        True if the move action is done
        False if the move action will continue after return (e.g., sprite will follow a path)
        """
        log.debug(f"Handling move action for {self} with {payload}")
        # now dealing with movement
        appearing = False
        if "visible" in payload:
            if not self.visible and payload['visible']:
                appearing = True  # object has just appeared
            self.visible = payload["visible"]

        if "direction" in payload:
            self.direction = cst.Direction(payload["direction"])

        # lastly, update coordinates if present in payload
        if "coord_x" in payload:
            assert "coord_y" in payload
        else:
            return True

        target_x, target_y = payload["coord_x"], payload["coord_y"]

        if target_x is None or target_y is None:
            return True # No movement to do

        if (
            self.coord_x is None
            or self.coord_x < 0
            or self.coord_y is None
            or self.coord_y < 0
            or appearing  # do not animate if was invisible before
            or "teleport" in payload
            or self.far_move == "teleport"
        ):
            self.set_relative_position(target_x, target_y, direct=self.direction)
            return True

        if type(target_x) == tuple and len(target_x) == 1:
            target_x = target_x[0]
        dx, dy = target_x - self.coord_x, target_y - self.coord_y

        # player always go for the straight-line
        if abs(dx) + abs(dy) <= 1 or self.far_move == "crowflies":
            # log.debug(f"Moving {self.name} only 1 square with move_forward")
            # print(f"move forward {dx, dy} for {self}")
            return self.move_forward((dx, dy))

        # Otherwise, move selfect / wop from current position to new position
        path = find_path(
            self, self.amap.game_view.layers["walls"], target_x, target_y, self.amap
        )
        log.debug(f"Moving {self.name} along path {path}")

        if not path:
            log.error(f"Cannot move {self.name} to destination!")
            return True

        self.follow_path(path, auto_dir=not self.name == 'Player')
        return False

    def hide(self):
        self.visible = False

    def show(self):
        self.visible = True

    def disappear(self):
        self.hide()
        ## TODO: really update this list, still used?
        # self.amap.game_view.disappeared_objects.append(self)

    def reappear(self):
        self.show()

    # TODO: change name to only "on_resize"?
    def rescale(self, scale):
        """Rescale the sprite."""
        factor = scale / self.scale
        self.scale = scale

        self.prev_center_x *= factor
        self.prev_center_y *= factor

        self.center_x *= factor
        self.center_y *= factor

    def talks(self, message, action):
        self.amap.game_view.box_append_queue('talk', message, hook=lambda: self.stop_talking(action))
        self.in_action = True
        self.action_hook = lambda _: None

    def stop_talking(self, action):
        self.in_action = False
        action.set_done()


class SpStillObject(SpObject):
    """
    Object with no animation. The sprite is a fixed image.
    """

    def __init__(
        self,
        sprites_sheet: str,
        amap: Amap,
        xoff: int,
        yoff: int,
    ):
        """
        Still Character initialization.

        :param sprites_sheet: filename of the sprite sheet
        :param map_height: Height of the map.
        :param map_width: Width of the map.
        :param xoff: x offset in the sprite sheet
        :param yoff: y offset in the sprite sheet
        """

        self.sprite_x_offset = xoff
        self.sprite_y_offset = yoff

        super().__init__()
        self.load_textures(sprites_sheet)

        self.name = "StillObject"
        self.set_amap(amap)
        self.scale = self.amap.scale
        self.tl_offset_y = (
            0.1  # for the exit sprite, currently the only "still character"
        )

    def load_textures(self, sprites_sheet):
        # loading only one still texture
        ts = cst.Tiles.TILE_SIZE
        self.texture = arcade.load_texture(
            sprites_sheet,
            x=self.sprite_x_offset * ts,
            y=self.sprite_y_offset * ts,
            width=ts,
            height=ts,
        )


def create_default_wall(amap, x, y):
    log.debug(f"Creating a wall sprite at location {x, y}")
    fn, shc, shr = cst.SPRITES["wall"]
    wall = SpStillObject(
        sprites_sheet=fn, amap=amap, xoff=shc, yoff=shr
    )
    wall.set_relative_position(x, y)
    wall.tl_offset_y = 0.0
    return wall



class SpChangingColorObject(SpStillObject):
    def __init__(self, sprites_sheet, amap, xoff, yoff):
        super().__init__(sprites_sheet, amap, xoff, yoff)

        self.textures = [self.texture]

        init_x_offset = xoff
        self.num_colors = 4
        for xoff in range(1, self.num_colors):
            self.sprite_x_offset = init_x_offset + xoff
            self.load_textures(sprites_sheet)
            self.textures.append(self.texture)

        self.current_color = 0
        self.texture = self.textures[self.current_color]

    def next_color(self):
        if self.current_color < self.num_colors - 1:
            self.current_color += 1
        self.texture = self.textures[self.current_color]

    def prev_color(self):
        if self.current_color > 0:
            self.current_color -= 1
        self.texture = self.textures[self.current_color]

    # def next_frame(self):
    # self.current_color += 1
    # self.current_color %= self.num_colors
    # self.texture = self.textures[self.current_color]


class SpAnimObject(SpObject):
    """
    Object/chars that have a sprite sheet of it's own for animation
    """

    def __init__(
        self, *args,
        # sprites_sheet: str, => deprecated, now uses 'filename' like arcade.Sprite
        amap: Amap|None = None, columns=3, count=12, sprite_scale=1,
        name: str = "GenCharacter",
         **kwargs
    ):

        # Do initialization as if it was a regular sprite
        super().__init__(*args, **kwargs)
        filename = kwargs['filename']
        image_y = kwargs['image_y'] if 'image_y' in kwargs else 0  # the row indicates the initial direction

        # Now load the rest of the textures to get animations
        self.columns = columns
        self.count = count
        self.sprite_scale = sprite_scale
        self.tl_offset_y: float = 0.4 # nicer align with tiles

        self.name = name
        self.set_amap(amap)

        self.load_textures(filename)

        self.direction = [cst.Direction.DOWN, cst.Direction.LEFT, cst.Direction.RIGHT, cst.Direction.UP][image_y // cst.Tiles.SPRITE_SIZE]

    def update(self, delta_time: int):
        self.frame_rate_factor = delta_time*60
        super().update(delta_time)

    def load_textures(self, sprites_sheet):
        self.textures = arcade.load_spritesheet(
            sprites_sheet,
            sprite_width=cst.Tiles.SPRITE_SIZE * self.sprite_scale,
            sprite_height=cst.Tiles.SPRITE_SIZE * self.sprite_scale,
            columns=self.columns,
            count=self.count,
            margin=0,
        )
        self.cur_texture: int = 0
        self.texture = self.textures[self.cur_texture]

        self.frame_count: int = 0

    def next_frame(self):
        """Update the texture if it is the right time."""
        self.frame_count += 1
        if self.frame_count <= cst.Anim.UPDATE_RATE / self.frame_rate_factor:
            return

        self.frame_count = 0
        self.cur_texture += 1

        # Going back to the first frame of the movement
        if self.cur_texture not in cst.SPRITE_FRAME[self.direction]:
            self.cur_texture = cst.SPRITE_FRAME[self.direction][0]

        self.texture = self.textures[self.cur_texture]

    def idle_frame(self):
        idle_index = cst.SPRITE_FRAME[self.direction][1]
        self.texture = self.textures[idle_index]

    def __str__(self):
        return (
            f"SpAnim({self.name} at {self.coord_x}x{self.coord_y} dir:{self.direction})"
        )

class SpTiledAnimatedObject(SpObject, arcade.AnimatedTimeBasedSprite):
    def __init__(self,**kwargs):
    # changing some values in the Arcade's caller because Arcade is a weirdo that doesn't do it's work properly
        caller_frame = inspect.currentframe().f_back.f_locals
        tile = caller_frame['tile']
        tileset = tile.tileset

        #if some of the tiles are not clearly defined in the .tmx we create them (should be Arcade's work)
        if tileset.tile_count != len(tileset.tiles):
            for id in range(tileset.tile_count):
                if id not in tileset.tiles:
                    tileset.tiles[id] = pytiled_parser.Tile(id=id)
        #setting the tileset of the tiles (required for Arcade to display)
        for t in tileset.tiles.values():
            t.tileset = tileset

        tile.animation = tileset.tiles[tile.id].animation


        SpObject.__init__(self,**kwargs)
        arcade.AnimatedTimeBasedSprite.__init__(self)

        self.check_animation_ended = False
        self.animation_counter = 0

    def update_animation(self, delta_time: float = 1 / 60):
        if self.check_animation_ended and self.cur_frame_idx == len(self.frames)-1:
            self.animation_counter -= 1
            if self.animation_counter == 0:
                self.end_animation()
            else:
                self.set_animation_frame(0)

        arcade.AnimatedTimeBasedSprite.update_animation(self, delta_time)

    def set_animation_frame(self, frame_id: int):
        self.time_counter = 0.0
        assert(frame_id < len(self.frames))
        self.cur_frame_idx = frame_id

    def play_animation(self, times=1):
        self.visible = True
        self.set_animation_frame(0)
        self.check_animation_ended = True
        self.animation_counter = times

    def end_animation(self):
        self.end_action_hook()
        self.check_animation_ended = False
        self.visible = False


class SpAnimOneSide(SpAnimObject):
    """
    Class for sprite animations that have a back and a front
    """

    def __init__(self, sprites_sheet: str, amap: Amap, columns, count, sprite_scale):
        self.spr_count = count
        super().__init__(filename=sprites_sheet, amap=amap, columns=columns, count=self.spr_count, sprite_scale=sprite_scale)
        self.name = "SpOneSide"

    def update(self, delta_time:int):
        super().update(delta_time)

        if self.amap.game_view.on_pause:
            return

        self.frame_count += 1
        if self.frame_count <= cst.Anim.UPDATE_RATE / self.frame_rate_factor:
            return

        self.frame_count = 0
        self.cur_texture += 1

        # Going back to the first frame of the movement
        if self.cur_texture >= self.spr_count:
            self.cur_texture = 0
        self.texture = self.textures[self.cur_texture]

    def idle_frame(self):
        pass


class SpAnimBackFront(SpAnimOneSide):
    def __init__(
        self,
        front_sprites_sheet: str,
        back_sprites_sheet,
        amap: Amap,
        columns,
        count,
        sprite_scale,
    ):
        self.back = SpAnimOneSide(
            back_sprites_sheet, amap, columns, count, sprite_scale
        )
        self.back.name = "SpAnimBack"
        super().__init__(front_sprites_sheet, amap, columns, count, sprite_scale)
        self.name = "SpAnimFront"

    def draw_front(self):
        self.draw()

    def draw_back(self):
        self.back.draw()

    def set_amap(self, amap):
        super().set_amap(amap)
        self.back.set_amap(amap)

    def rescale(self, scale):
        super().rescale(scale)
        self.back.rescale(scale)

    def set_relative_position(self, *args, **kwargs):
        super().set_relative_position(*args, **kwargs)
        self.back.set_relative_position(*args, **kwargs)

    def update(self, delta_time:int):
        super().update(delta_time)
        self.back.update(delta_time)

    # Do not call also update_position for back as this will already be the
    # case when calling self.back.update() in above update
    # def update_position(self):
    # super().update_position()
    # self.back.update_position()

    def move_forward(self, *args, **kwargs):
        ret = super().move_forward(*args, **kwargs)
        self.back.move_forward(*args, **kwargs)
        return ret

    def follow_path(self, path, final_direction=None, auto_dir=False):
        super().follow_path(path, final_direction, auto_dir)
        self.back.follow_path(path, final_direction, auto_dir)

    @property
    def visible(self) -> bool:
        return super().visible

    @visible.setter
    def visible(self, value: bool):
        # Note: super() does not give access of setter directly, hence
        # the need for this particular piece of code.
        super(SpAnimOneSide, self.__class__).visible.fset(self, value)
        self.back.visible = value

    # def hide(self):
        # super().hide()
        # self.back.hide()

    def show(self):
        super().show()
        self.back.show()


class SpPillar(SpAnimBackFront):
    def __init__(self, front_sprites_sheet: str, back_sprites_sheet, *, amap: Amap):
        super().__init__(
            front_sprites_sheet,
            back_sprites_sheet,
            amap=amap,
            columns=5,
            count=10,
            sprite_scale=2,
        )
        self.name = "PillarFront"
        self.back.name = "PillarBack"

    def reset(self):
        self.coord_x = None
        self.coord_y = None
        self.back.coord_x = None
        self.back.coord_y = None
        self.hide()

class SpExit(SpPillar):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.name = "Exit"
        self.back.name = "ExitBack"


class SpPlayer(SpAnimObject):
    """Sprite Player class."""

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.name = "Player"
        self.visible = False
        self.health_bar = HealthBar(
            (self.center_x, self.center_y + self.height / 2 + 10),
            self.width*2,
            5,
            arcade.color.BLACK,
            arcade.color.FRENCH_LIME,
            100,
        )

    def set_relative_position(self, *args, **kwargs):
        super().set_relative_position(*args, **kwargs)
        self.amap.game_view.camera.show_player()

    def move_forward(self, *args, **kwargs):
        ret = super().move_forward(*args, **kwargs)
        self.amap.game_view.camera.show_player()
        return ret

    def follow_path(self, *args, **kwargs):
        super().follow_path(*args, **kwargs)
        self.amap.game_view.camera.show_player()

    def disappear(self, *args, **kwargs):
        super().disappear(*args, **kwargs)
        self.amap.game_view.camera.show_player()

    def reappear(self, *args, **kwargs):
        super().reappear(*args, **kwargs)
        self.amap.game_view.camera.show_player()

    # def update(self):
        # log.critical(f"Updating player {self}")
        # super().update()


class SpPlayerBubble(SpChangingColorObject):
    """Sprite Player class."""

    def __init__(self, amap):
        super().__init__(
            ":buttons:Button.png",
            amap,
            3,
            2.5,
        )
        self.name = "Player"
        self.visible = False
        self.tl_offset_y = 0.1  # place a bit lower than regular player


class SpNPC(SpAnimObject):
    """Sprite NPC class."""

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.health_bar = HealthBar(
            (self.center_x, self.center_y + self.height / 2 + 10),
            self.width*2,
            5,
            arcade.color.BLACK,
            arcade.color.FRENCH_LIME,
            100,
        )

    def set_reset(self):
        super().set_reset()
        self.hide()


class SpWop(SpNPC):
    """Sprite NPC class."""

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.name = "WOP"
        # set_reset will be called by parent class Object

    def set_reset(self):
        super().set_reset()
        self.speed = 0.5  # old persons move slowly
        self.tl_offset_y = 0.5  # place a bit higher than player
        self.direction = cst.Direction.DOWN
        self.far_move = 'pathfind'
