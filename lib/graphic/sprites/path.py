"""
Path finding for sprites.
"""

import arcade
from logs import log

from graphic.amap import Amap

def to_absolute (
    coord_x: int,
    coord_y: int,
    amap: Amap,
    tl_offset_x: float = 0.0,
    tl_offset_y: float = 0.0,
    # map_width: int
) -> tuple[int, int]:
    """
    Translates coordinates in the agdbentures format ((0,0) as top left corner, in tiles) into
    coordinates is arcade format ((0,0) as bottom left corner, in pixels).
    Return the coordinates that are at the center of the tile.

    Warning: should not use the 'offset_*' from amap since this is now directly handled at map
    loading

    :param tl_offset_x  offset for precise tile placement, in % of the tile size
    :param tl_offset_y
    """
    return (
        (coord_x + 0.5 + tl_offset_x) * amap.scaled_tile,
        float(amap.height - (coord_y + 0.5) + tl_offset_y) * amap.scaled_tile
    )

def to_relative (
    coord_px: tuple[int, int],
    amap: Amap,
    tl_offset_x: float = 0.0,
    tl_offset_y: float = 0.0,
) -> tuple[int, int]:
    """Convert pixel coordinates to relative coordinates."""
    px_x, px_y = coord_px
    float_x = (px_x + amap.scaled_tile*tl_offset_x) / amap.scaled_tile
    float_y = (amap.height * amap.scaled_tile - (px_y + amap.scaled_tile*tl_offset_y)) / amap.scaled_tile

    # log.debug(f"To_relative {coord_px} => {float_x} x {float_y}")
    return int(float_x), int(float_y)


def _expand(pos: arcade.Point, grid_size: float):
    return int(pos[0] * grid_size), int(pos[1] * grid_size)

class OurAStarBarrierList(arcade.AStarBarrierList):
    """
    Redefinition because original AStarBarrierList does not use the
    center of the sprite but places sprite at the bottom left corner
    """
    def recalculate(self):
        """
        Recalculate blocking sprites.
        """
        # --- Iterate through the blocking sprites and find where we are blocked

        # Save original location
        original_pos = self.moving_sprite.position
        # Create a set of barriers
        self.barrier_list = set()
        how_many_previous = 0
        how_many = 0
        # Loop through the grid
        for cx in range(self.left, self.right + 1):
            for cy in range(self.bottom, self.top + 1):
                # Grid location
                cpos = cx+0.5, cy+0.5
                gpos = cx, cy
                # Pixel location
                pos = _expand(cpos, self.grid_size)

                # See if we'll have a collision if our sprite is at this location
                self.moving_sprite.position = pos
                how_many = len(arcade.check_for_collision_with_list(self.moving_sprite, self.blocking_sprites))
                if how_many > how_many_previous:
                    how_many_previous = how_many
                    self.barrier_list.add(gpos)

        # Restore original location
        self.moving_sprite.position = original_pos
        self.barrier_list = sorted(self.barrier_list)



# pylint: disable=too-many-arguments
def find_path(
    sprite: arcade.Sprite,
    blocking_sprites: arcade.SpriteList,
    target_x: int,
    target_y: int,
    amap: Amap,
) -> list[tuple[int, int]]:
    """
    Compute the path that a sprite has to follow.

    :param sprite: The sprite to compute the path.
    :param blocking_spites: Sprites that block the way.
    :param target_x: x component of the tile target.
    :param target_y: y component of the tile target.
    :param amap: the arcade map parameters
    :return: List of the coordinates of the tiles to follow.
    """
    barrier_list = OurAStarBarrierList(
        sprite,
        blocking_sprites,
        int(amap.scaled_tile),
        0,
        int(amap.width * amap.scaled_tile),
        0,
        int(amap.height * amap.scaled_tile),
    )
    # log.debug(f"Pathfinding: barrier_list {barrier_list}")
    # for b in barrier_list.barrier_list:
        # log.debug(f"\tbarrier {b}")


    target_px = to_absolute(target_x, target_y, amap, tl_offset_x=0.0, tl_offset_y=0.0)
    px,py = target_px
    log.debug(f"Pathfinding: to {target_x}x{target_y} pixels: {target_px} {px}x{py}")
    path = arcade.astar_calculate_path(
        # (sprite.center_x, sprite.center_y - amap.scaled_tile * 0.5),
        (sprite.center_x, sprite.center_y), # TODO: Florent : why *0.5 tilesize?
        target_px,
        barrier_list,
        diagonal_movement=False,
    )

    log.debug(f"Pathfinding: {sprite.center_x, sprite.center_y} {path}")
    if path is None:
        return []  # No path found.
    # Translating the path into absolute coordinates
    # Need to add offset as AStar returns bottom left corner of tiles
    path = list(map(lambda x : to_relative(x, amap, tl_offset_x=0.5, tl_offset_y=0.5), path[1:]))
    return path
