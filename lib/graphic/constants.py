"""Arcade constants."""

from enum import Enum
import arcade.color

# pylint: disable=too-few-public-methods


class Tiles:
    """Tiles constants."""

    SPRITE_SIZE = 32
    TILE_SIZE = 32
    MIN_SCALE = 0.25
    MAX_SCALE = 3
    MIN_AUTO_SCALE = 1.5
    MAX_AUTO_SCALE = 2.36 # just enough so many maps do not show black


class Anim:
    """Animation constants."""

    UPDATE_RATE = 5
    SPEED = 5 / 100
    HIT_UPDATE_RATE = 10
    HIT_SPEED = 5 / 300
    TEXT_UPDATE_DELAY = 0.02
    POST_TEXT_DELAY = 50  # time during which a box 'blocks' another box appearance


TILESETS = {
    "[Base]BaseChip_pipo.json": 1,
    "[A]Grass_pipo.json": 1065,
    "[A]Water_pipo.json": 1593,
}

BASESHEET = ":tiles:[Base]BaseChip_pipo.png"

SPRITES = {
    "exit": (
        BASESHEET,
        6,  # column in the sheet
        8,  # line in the sheet
    ),
    "wall": (
        BASESHEET,
        1,
        50,
    ),
}


# Direction = Enum("Direction", "DOWN LEFT RIGHT UP HERE")
class Direction(Enum):
    UP, DOWN, LEFT, RIGHT, HERE, UNKNOWN = range(6)


class Fonts:
    KENNEY = "Kenney Future"
    FURACODE = "FuraCode Nerd Font"
    BITSTREAM = "Bitstream Vera Serif"


SPRITE_FRAME = {
    Direction.DOWN: [0, 1, 2],
    Direction.LEFT: [3, 4, 5],
    Direction.RIGHT: [6, 7, 8],
    Direction.UP: [9, 10, 11],
    Direction.HERE: [0, 0, 0],
    Direction.UNKNOWN: [0, 0, 0],
}

DELTA = {
    Direction.UP: (0, -1),
    Direction.DOWN: (0, 1),
    Direction.LEFT: (-1, 0),
    Direction.RIGHT: (1, 0),
    Direction.HERE: (0, 0),
    Direction.UNKNOWN: (0, 0),
}


class Win:
    """Window constants."""

    # note: a button is 40px height
    # paddings between buttons are all at 20px

    MIN_W = 800
    MIN_H = 600
    MAX_W = 1920
    MAX_H = 1200
    INIT_W = 1024
    INIT_H = 768
    # CODE_COLS = 60  # number of columns in the code window
    CODE_COLS = 45  # number of columns in the code window
    CODE_ROWS = 38  # number of rows in the code window
    TITLE = "Agdbentures"

    GRAPH_WIDTH = 180
    GRAPH_HEIGHT = 3 * 20 + 20 + 15 # same as logbox
    GRAPH_MARGIN = 10 # actually not implemented
    GRAPH_AXIS_COLOR=arcade.color.BLACK
    GRAPH_GRID_COLOR=arcade.color.BLACK + (int(255 * 50/100),)
    GRAPH_BACKGROUND_COLOR=arcade.color.BLACK + (int(255 * 10/100),)

    LOGBOX_WIDTH = 300 + 20 # message box width + padding
    LOGBOX_HEIGHT = 3 * 20 + 20 # 3 lines + padding
    LOGBOX_TEXT_COLOR = arcade.color.GHOST_WHITE
    LOGBOX_TEXT_SIZE = 12
    LOGBOX_TEXT_FONT = Fonts.FURACODE

    HELP_BACKGROUND = arcade.color.BLACK + (255 * 80/100,)
    HELP_TEXT_COLOR = arcade.color.WHITE
    HELP_TEXT_SIZE_TITLE = 18
    HELP_TEXT_SIZE_CONTENT = 12
    HELP_TEXT_FONT = Fonts.FURACODE
    HELP_TEXT_BOLD_TITLE = True
    HELP_TEXT_BOLD_CONTENT = False

    GENERICBOX_WIDTH = 300
    GENERICBOX_HEIGHT = 250
    GENERICBOX_PADDING = 20
    GENERICBOX_TEXT_FONT = Fonts.BITSTREAM
    GENERICBOX_TEXT_COLOR = arcade.color.BLACK

    GENERICLABELBOX_WIDTH = 300
    GENERICLABELBOX_HEIGHT = 100
    GENERICLABELBOX_PADDING = 20
    GENERICLABELBOX_TOP_LABEL_FONT = Fonts.FURACODE
    GENERICLABELBOX_TOP_LABEL_SIZE = 12
    GENERICLABELBOX_TOP_LABEL_COLOR = arcade.color.DARK_RED
    GENERICLABELBOX_TOP_MESSAGE_FONT = Fonts.FURACODE
    GENERICLABELBOX_TOP_MESSAGE_SIZE = 14
    GENERICLABELBOX_TOP_MESSAGE_COLOR = arcade.color.BLACK
    GENERICLABELBOX_ENTRY_HEIGHT = 35
    GENERICLABELBOX_ENTRY_FONT = Fonts.FURACODE
    GENERICLABELBOX_ENTRY_SIZE = 20
    GENERICLABELBOX_ENTRY_COLOR = arcade.color.DARK_BLUE
    GENERICLABELBOX_ENTRY_BACKGROUND = arcade.color.DARK_GRAY
