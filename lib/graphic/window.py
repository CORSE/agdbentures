#!/usr/bin/env python3

"""
Arcade main window
"""

import os
import queue
from threading import Event
import time
from sys import argv
from signal import signal, Signals, SIGINT, SIGTERM
from os.path import dirname, splitext, exists, join, basename, isdir

import arcade
import arcade.key as KEY

from graphic.views.game import GameView
from logs import log
from tests.testGUI import Reason, EventHandler
from utils import show_threads
from config import Config
from loader.loader_utils import import_from_file, get_dynamic_level, extract_metadata
from loader.loader import prepare_level
from . import views
from . import constants as cst

# pylint: disable=no-member
class BaseArcadeWindow(arcade.Window):
    """Loader's base class."""

    def __init__(self, code_window_cls):
        super().__init__(
            cst.Win.INIT_W,
            cst.Win.INIT_H,
            cst.Win.TITLE,
            resizable=True,
            # update_rate=1/10  # impacts the refresh rate of the window, but
            # not the target FPS
        )  # type: ignore
        self.time_last_ctrl_c: Float = 0.0  # to record two successive Ctrl-C
        self.handle_signals()
        self.set_minimum_size(cst.Win.MIN_W, cst.Win.MIN_H)
        self.set_maximum_size(cst.Win.MAX_W, cst.Win.MAX_H)
        self.views = views.load_views()
        self.level_cls = None
        self.metadata = None
        self.code_window_cls = code_window_cls
        self.code_window = None  # do not yet instantiate a window to show code (will do when starting a level)

    def handle_signals(self):
        """
        Set handlers for the signals to pass to the tracker thread.
        """
        for sig in [SIGTERM, SIGINT]:
            signal(sig, self.handle_interrupt)


    def handle_interrupt(self, signum, frame):
        signame = Signals(signum).name
        if self.views["game"].level is not None:
            if self.views["game"].level.is_running:
                log.info(f"Received signal {signame} ({signum}), interrupting inferior")
                self.views["game"].level.interrupt_inferior()
                return
            elif self.views["game"].level.is_validating:
                log.warning(f"Received signal {signame} ({signum}), interrupting validation")
                self.views["game"].level.interrupt_validation()
                return
            elif self.views["game"].level.tracker:
                current_time = time.time()
                if self.time_last_ctrl_c + 1.0 > current_time:
                    pass
                    # really exit
                    # raise KeyboardInterrupt
                else:
                    self.time_last_ctrl_c = current_time
                    print(f"Will not Ctrl-C an inferior not running. To exit Agdbentures, hit Ctrl-C again.")
                    return

        # Otherwise, quit Agdbentures
        log.warning(f"Received signal {signame} ({signum})")
        self.kill_level_tracker()
        log.info(f"\texiting arcade")
        raise KeyboardInterrupt
        # arcade.exit()

    def start_level(self, full_level_name: str):
        """
        Load a level from a level name.
        The source for this level will be generated based on metadata on the
        'main.c' file present in the same directory.

        :param full_level_name: name of the level in the current level hierarchy, including category, e.g. "tutorial/00_introduction"
        """
        log.info(f"Loading level {full_level_name}")

        cus_level_cls, metadata = prepare_level(full_level_name)

        # prepare self with correct information, as this is where the
        # game view will search for the level class and metadata
        self.metadata = metadata
        self.level_cls = get_dynamic_level(cus_level_cls, "arcade")

        if self.code_window is None:
            # instantiate a window to show code, passing the main agdb window
            # as parameter
            self.code_window = self.code_window_cls(self)

        # Ask the "game" view to actually launch the level
        self.views["game"].launch_level(self.code_window)
        self.show_view(self.views["game"])
        
        

    def kill_level_tracker(self):
        level = self.views["game"].level
        if level is not None:
            log.info(f"\tkilling level tracker")
            level.terminate_tracker()

    def finish_input_thread(self):
        self.views["game"].console_input_thread.terminate()

    def on_quit(self) -> None:
        """Level quit callback."""
        level = self.views["game"].level
        if level is not None:
            level.send_level_control('exit')
            self.views["game"].level = None
        self.show_view(self.views["loader"])

    def on_key_press(self, symbol, modifiers):
        if symbol == KEY.C and modifiers & KEY.MOD_CTRL:
            self.handle_interrupt(SIGINT, None)


class MainWindow(BaseArcadeWindow):
    """Arcade game loader."""

    def run(self):
        self.show_view(self.views["main"])
        EventHandler.notify(Reason.WINDOW_LOADED)
        arcade.run()


class StandaloneLoader(BaseArcadeWindow):
    """Standalone loader for arcade."""

    def __init__(self, level_cls: type):
        """
        Standalone loader initialization.

        :param level_cls: the level class.
        :param metadata: the level metadata.
        """
        super().__init__()
        self.level_cls = level_cls
        self.metadata = level_cls.metadata

    def on_quit(self) -> None:
        arcade.exit()

    def run(self):
        """Run the loader."""
        self.launch_level(self.level_cls)
        arcade.run()


if __name__ == "__main__":
    window = MainWindow()
    window.run()
