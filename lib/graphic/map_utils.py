#!/usr/bin/env python3

"""
Map edit/creation tools.
"""

import sys
from os.path import dirname, basename
from collections import defaultdict
import xml.etree.ElementTree as ET
from typing import Optional

import arcade
import graphic.sprites.sp_objects as spo
import graphic.constants as cst
from level.objects import LevelObjectsArea
from logs import lvl


BG_TILE = 1
NOTHING = 0


# pylint: disable=too-many-locals
def create_map(
    map_dest: str,
    tilesets_path: Optional[str] = None,
    metadata: Optional[dict[str, str]] = None,
) -> None:
    """
    Create a xml according to some metadata.
    This allows you to create a template that you could edit easily with the
    graphic map editor 'tiled'.

    :param map_dest: The name of map create.
    :param tilesets_map: Path to the tilesets.
    :param metadata: The dictionnary that describes the level.
    """
    # Default arguments
    metadata = metadata or {}
    tilesets_path = tilesets_path or f"{dirname(__file__)}/resources/tiles"

    # Using defaultdict to avoid checks or KeyError
    map_info = defaultdict(lambda: None, metadata)

    # Reading variables from the metadata dictionnary
    width = map_info["map_width"] or "20"
    height = map_info["map_height"] or "10"
    tile_width = map_info["tile_width"] or "32"
    tile_height = map_info["tile_height"] or "32"
    background_color = map_info["background_color"] or "#000000"

    # Map root
    root = ET.Element(
        "map",
        version="1.8",
        tiledversion="1.8.4",
        orientation="orthogonal",
        renderorder="right-down",
        width=width,
        height=height,
        tilewidth=tile_width,
        tileheight=tile_height,
        infinite="0",
        backgroundcolor=background_color,
        nextlayerid="3",
        nextobjectid="1",
    )

    # Adding custom properties
    # Those properties can be accessed by reading the tile_map
    # attributes: `val = self.tile_map.properties["my_prop_name"]`
    properties_list = []  # [
    #    {"name": "player_x", "type": "int", "value": player_x},
    #    {"name": "player_y", "type": "int", "value": player_y},
    #    {"name": "player_direction", "type": "int", "value": player_direction},
    # ]
    properties = ET.SubElement(root, "properties")
    for prop in properties_list:
        name = prop["name"]
        var_type = prop["type"]
        value = prop["value"]
        ET.SubElement(
            properties, "property", name=name, type=var_type, value=str(value)
        )

    # Map info
    for tile_name, firstgid in cst.TILESETS.items():
        ET.SubElement(
            root,
            "tileset",
            firstgid=str(firstgid),
            source=f"{tilesets_path}/{tile_name}",
        )

    def add_layer(root, name, index, data_grid):
        """Create a layer."""
        # Scene layer
        scene_layer = ET.SubElement(
            root,
            "layer",
            id=str(index),
            name=name,
            width=str(width),
            height=str(height),
        )
        data = ET.SubElement(scene_layer, "data", encoding="csv")
        # Conversion of the data into a csv string
        data_txt_list = []
        for int_list in data_grid:
            str_map = map(str, int_list)
            str_line = ",".join(str_map)
            data_txt_list.append(str_line)
        data.text = "\n" + ",\n".join(data_txt_list)

    def add_object_layer(root, name, index):
        scene_layer = ET.SubElement(
            root,
            "objectgroup",
            id=str(index),
            name=name,
        )

    # Creation of the values of the grid
    floor = [[BG_TILE] * int(width) for _ in range(int(height))]
    decorations = [[NOTHING] * int(width) for _ in range(int(height))]
    decorations_top = [[NOTHING] * int(width) for _ in range(int(height))]
    walls = [[NOTHING] * int(width) for _ in range(int(height))]

    # Adding layer in stacking order
    layers = [
        ("floor", floor),
        ("decorations", decorations),
        ("decorations_top", decorations_top),
        ("walls", walls),
    ]
    for stack_index, (name, layer) in enumerate(layers, start=1):
        add_layer(root, name, stack_index, layer)

    add_object_layer(root,"objects", stack_index)

    # Writing with pretty print
    final_xml = ET.ElementTree(root)
    ET.indent(final_xml, space=" ")
    final_xml.write(map_dest, encoding="UTF-8", xml_declaration=True)


# TODO: refactor with to_relative from graphic/sprites/path ?
def to_relative_no_amap(
    coord_px: tuple[int, int],
    height: int  # height of map in tiles
) -> tuple[int, int]:
    """Convert pixel coordinates to relative coordinates."""
    px_x, px_y = coord_px

    ts = cst.Tiles.TILE_SIZE
    float_x = px_x / ts
    float_y = (height * ts - px_y) / ts

    # lvl.debug(f"To_relative {coord_px} => {float_x} x {float_y}")
    return int(float_x), int(float_y)



def load_tmx_map(map_path: str, offset: tuple[int, int], level_name: str):
    """
    Load a map in a .tmx file and retrieve objects from it.
    Pass the loaded tm object to the hook provided e.g., by the GUI (in arcade mode).
    (or discarded in test mode).
    """

    map_off_x, map_off_y = offset

    # load offset if there are any
    tilemap_off = (
        -map_off_x * cst.Tiles.TILE_SIZE,
        -map_off_y * cst.Tiles.TILE_SIZE,
    )

    tm = arcade.load_tilemap(
        map_path,
        layer_options={"objects": {"custom_class": spo.SpObject},
                       "characters": {"custom_class": spo.SpNPC},
                       "anim_objects": {"custom_class": spo.SpTiledAnimatedObject}
                      },
        offset=tilemap_off,
    )  # type: ignore

    assert tm.tile_height == cst.Tiles.TILE_SIZE, "Map has incorrect tile size"
    assert tm.tile_width == cst.Tiles.TILE_SIZE, "Map has incorrect tile size"

    level_area = None
    if 'levels' in tm.object_lists.keys():
        lvl_areas = list(filter(lambda x: x.name == level_name, tm.object_lists['levels']))
        if len(lvl_areas) > 1:
            lvl.error("Too many areas for this level")
        elif len(lvl_areas) == 1:
            lvl.debug("Found area for level")
            level_area = LevelObjectsArea(level_name, lvl_areas[0].shape)

    if 'objects' not in tm.sprite_lists:
        # create empty list
        tm.sprite_lists['objects'] = arcade.SpriteList()

    if 'characters' not in tm.sprite_lists:
        # create empty list
        tm.sprite_lists['characters'] = arcade.SpriteList()

    if 'anim_objects' not in tm.sprite_lists:
        # create empty list
        tm.sprite_lists['anim_objects'] = arcade.SpriteList()

    # coordinates
    if 'objects' not in tm.object_lists:
        # tm.object_lists['objects'] = arcade.SpriteList()
        tm.object_lists['objects'] = []
    # lvl.critical(f"Verify it exists? TODO {dir(tm)} et en particulier {tm.object_lists}")
    # assert tm.object_lists, f"Verify it exists? TODO {dir(tm)} et en particulier {tm.object_lists}"
    # assert "objects" in tm.object_lists
    # if tm.object_lists:
        # lvl.debug(
            # "Object layer: %s objects",
            # len(tm.object_lists["objects"]),
        # )
        # for obj in tm.object_lists["objects"]:

    # Remove objects outside the level_area
    if level_area:
        keep_obj = [obj for obj in tm.sprite_lists["objects"] if obj in level_area]
        keep_char = [obj for obj in tm.sprite_lists["characters"] if obj in level_area]
        keep_anim_obj = [obj for obj in tm.sprite_lists["anim_objects"] if obj in level_area]

        # Merge objects and characters into a single sprite_list
        tm.sprite_lists["objects"] = arcade.SpriteList()
        for sp_keep in [keep_obj, keep_char, keep_anim_obj]:
            tm.sprite_lists["objects"].extend(sp_keep)

        # coordinates are in a regular list, not a sprite list
        keep_coord = [obj for obj in tm.object_lists["objects"] if obj in level_area]
        tm.object_lists["objects"] = keep_coord
    else:
        # just append characters to the objects list
        for sp_list in ["characters", "anim_objects"]:
            tm.sprite_lists["objects"].extend(tm.sprite_lists[sp_list])


    lvl.debug(f"Sprite lists on this map: {tm.sprite_lists}")

    lvl.debug(
        "Object layer: %s sprites (including animated characters)",
        len(tm.sprite_lists["objects"]),
    )

    # Initialize objects found the .tmx
    for obj in tm.sprite_lists["objects"]:
        # moving attributes in the 'properties' field of the tiled editor 
        # directly in the object
        for p, v in obj.properties.items():
            setattr(obj, p, v)
            lvl.debug(f"\tsetting prop {p}:{v}")
        lvl.debug(f"sp object: {obj}")

        if not hasattr(obj, "name"):
            lvl.error(f"Some object on the map has no name!")
            raise ValueError(f"All map objects must have a name")

        x, y = to_relative_no_amap((obj.center_x, obj.center_y), tm.height)

        lvl.debug(f"\tsp object rel location: {x}x{y} from {obj.center_x}px {obj.center_y}py")
        obj.coord_x = x
        obj.coord_y = y

    # Get .tmx coordinates, referenced as "objects" by the tiled editor.
    coords = {}
    if tm.object_lists:
        lvl.debug(

            "Object layer: %s objects",
            len(tm.object_lists["objects"]),
        )
        for obj in tm.object_lists["objects"]:

            lvl.debug(f"coordinates: {obj}")
            coord = spo.SpCoordinates(obj.name)
            name = obj.name
            [cx, cy] = obj.shape
            coord.center_x = cx
            coord.center_y = cy
            lvl.debug(f"coords location: {cx}x{cy}")
            x, y = to_relative_no_amap((cx, cy), tm.height)
            lvl.debug(f"coords rel location: {x}x{y}")
            coord.coord_x = x
            coord.coord_y = y

            coords[name] = coord

    tm.object_lists["coordinates"] = coords

    return tm





if __name__ == "__main__":
    # Create an empty map

    def _main(
        map_dest: str,
        height: str,
        width: str,
    ):
        """Create an empty map using default configuration."""
        metadata = {"map_height": height, "map_width": width}
        create_map(map_dest, tilesets_path=None, metadata=metadata)
        print(f"map (dim {height}x{width}) successfully created to {map_dest}")

    def _help():
        """Show help message for the empty map generation."""
        print(
            f"Usage: {basename(sys.argv[0])} destination height width\n",
            "Create an empty map of size height by width to the given destination file\n",
            "destination: file to write the map in",
            "height: height of the wanted map",
            "width: width of the wanted map",
            sep="\n",
            file=sys.stderr,
        )

    if (
        len(sys.argv) != 3 + 1
        or not sys.argv[2].isnumeric()
        or not sys.argv[3].isnumeric()
    ):
        _help()
        sys.exit(1)
    else:
        _main(*sys.argv[1:])  # pylint: disable=no-value-for-parameter
