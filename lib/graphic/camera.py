import arcade
from pyglet.math import Vec2
from enum import Enum, auto

from logs import log
import graphic.sprites.sp_objects as spo
from graphic.amap import Amap
from graphic.sprites.path import to_absolute

class CameraMode(Enum):
    CENTER_TO_START = auto()
    CENTER_TO_EXIT = auto()
    CENTER_TO_OBJECT = auto()
    TARGET_TRANSITION_ANIMATION = auto()
    FREE = auto()

class AgdbCamera(arcade.Camera):

    def __init__(self, viewport_width: int, viewport_height: int):
        super().__init__(viewport_width, viewport_height)

        self.mode = CameraMode.FREE
        self.target: spo.SpObject = None
        self.center = (0, 0)
        self.pause_time = 0.0
        self.exit: spo.SpExit = None
        self.player: spo.SpPlayer

    def reset(self):
        self.mode = CameraMode.CENTER_TO_START
        # self.target = self.player
        # self.mode = CameraMode.CENTER_TO_OBJECT

    def show_exit(self, exit: spo.SpExit):
        self.exit = exit
        if not self.is_inside_camera(exit):
            self.mode = CameraMode.CENTER_TO_EXIT
            self.pause_time = 0.0
        else:
            self.mode = CameraMode.CENTER_TO_OBJECT
    
    def show_player(self):
        if self.mode == CameraMode.FREE and not self.is_inside_camera(self.player):
            log.debug("Player is outside camera view, showing player.")
            self.mode = CameraMode.CENTER_TO_OBJECT
            self.target = self.player

    def update_state(self, tile_map: arcade.TileMap, amap: Amap, delta_time: float):
        if self.mode == CameraMode.CENTER_TO_START:
            # Show left-most part of level, vertically centered
            x0, y0 = to_absolute(0, amap.height//2, amap)
            self.center_to_position(
                x0,
                y0,
                tile_map,
                amap,
                speed=0
            )
        elif self.mode == CameraMode.CENTER_TO_EXIT:
            ## do not center camera to player yet
            self.pause_time += delta_time
            if self.pause_time > 2:
                self.mode = CameraMode.CENTER_TO_OBJECT
                self.target = self.player
            else:
                self.center_to_position(
                    self.exit.center_x,
                    self.exit.center_y,
                    tile_map,
                    amap,
                    speed=0.1
                )
        elif self.mode == CameraMode.FREE:
            pass
        elif not self.target.visible:
            pass
        elif self.mode == CameraMode.CENTER_TO_OBJECT:
            self.center_to_object(
                tile_map,
                amap,
                speed=1.0
                )
        elif self.mode == CameraMode.TARGET_TRANSITION_ANIMATION:
            self.center_to_object(
                tile_map,
                amap,
                speed=0.1
            )
            if self.is_camera_on_goal():
                self.mode = CameraMode.CENTER_TO_OBJECT

    def center_to_position(self, px_x: int, px_y: int, tile_map: arcade.TileMap, amap: Amap, speed: float = 1.0):
        # log.debug(f"Centering camera from {self.center} to {px_x, px_y}")
        self.center = (px_x, px_y)

        vp_w = self.viewport_width
        vp_h = self.viewport_height

        # From given position in pixels calculate lower left corner from that
        screen_center_x = px_x - vp_w / 2
        screen_center_y = px_y - vp_h / 2

        # Set some limits on how far we scroll
        screen_center_x = max(screen_center_x, 0)
        screen_center_y = max(screen_center_y, 0)

        # Avoid black borders on the top
        map_height_px = amap.scaled_tile * tile_map.height
        if map_height_px >= vp_h:
            top_vp_center = map_height_px - vp_h
            screen_center_y = min(screen_center_y, top_vp_center)
        else:
            # map to small, center so there is the same amount of black
            # above and below
            too_much = vp_h - map_height_px
            screen_center_y = -too_much // 2

        # Avoid black borders on the right
        map_width_px = amap.scaled_tile * tile_map.width
        if map_width_px >= vp_w:
            top_vp_center = map_width_px - vp_w
            screen_center_x = min(screen_center_x, top_vp_center)
        else:
            # screen_center_x = 0
            too_much = vp_w - map_width_px
            screen_center_x = -too_much // 2

        # Here's our center, move to it
        pos_centered = screen_center_x, screen_center_y

        # move_to moves the camera so pos_centered is at the lower left corner
        self.move_to(pos_centered, speed=speed)  # type: ignore

    def center_to_object(self, tile_map: arcade.TileMap, amap: Amap, speed: float = 1.0):
        if (
            self.target.visible
            and self.target.center_x is not None
            and self.target.center_y is not None
            # self.player.center_x >= 0 and
            # self.player.center_y >= 0
        ):
            self.center_to_position(
                self.target.center_x, 
                self.target.center_y, 
                tile_map,
                amap,
                speed=speed
            )
        else:
            x0, y0 = to_absolute(0, 0, amap)
            self.center_to_position(x0, y0, tile_map, amap, speed=speed)

    def agdb_shake(self):
        self.shake(Vec2(25, 10), speed=5, damping=0.9)
    
    def is_inside_camera(self, obj: spo.SpObject):
        vp_w = self.viewport_width
        vp_h = self.viewport_height
        low_x, low_y = self.goal_position
        log.debug(
            f"Is target inside camera viewport? {vp_w}x{vp_h} {low_x}x{low_y} {obj}"
        )
        return (
            low_x < obj.center_x
            and obj.center_x < low_x + vp_w
            and low_y < obj.center_y
            and obj.center_y < low_y + vp_h
        )
    
    def is_camera_on_goal(self):
        distance = self.position.distance(self.goal_position)
        return distance < 10
