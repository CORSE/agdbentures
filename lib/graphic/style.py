from math import cos, pi, inf, ceil
import arcade
import arcade.gui
from pyglet.event import EVENT_HANDLED, EVENT_UNHANDLED
from arcade.gui.events import (
    UIEvent,
    UIOnUpdateEvent,
    UIMouseEvent,
    UIMousePressEvent,
    UIMouseReleaseEvent,
    UIMouseMovementEvent,
    UIMouseScrollEvent,
    UITextEvent,
    UIKeyPressEvent,
    UIKeyReleaseEvent,
)

from arcade.gui.mixins import UIWindowLikeMixin, UIMouseFilterMixin
from arcade.gui.widgets import (
    UILayout,
    UIAnchorWidget,
    UIPadding,
    UITextArea,
    UITexturePane,
    UIFlatButton,
    UIInputText,
    UILabel,
)
import arcade.key as KEY

import graphic.constants as cst
from logs import log
from language import message


class MenuButton(arcade.gui.UIFlatButton):
    """
    Regular button in the Agdbentures style
    """

    def __init__(self, *args, **kwargs):
        if "style" not in kwargs:
            kwargs["style"] = {}
        kwargs["style"]["font_name"] = cst.Fonts.KENNEY
        kwargs["style"]["font_size"] = 12
        if "width" not in kwargs:
            kwargs["width"] = 180
        super().__init__(*args, **kwargs)


class SkipButton(arcade.gui.UIFlatButton):
    """
    Skip button in the Agdbentures style that is meant to be more in the 'background'
    """

    def __init__(self, *args, **kwargs):
        if "style" not in kwargs:
            kwargs["style"] = {}
        kwargs["style"]["font_name"] = font_name = cst.Fonts.FURACODE
        kwargs["style"]["font_size"] = 12
        if "width" not in kwargs:
            kwargs["width"] = 180
        kwargs["style"]["font_color"] = arcade.color.DARK_BLUE
        kwargs["style"]["border_color"] = None
        kwargs["style"]["bg_color"] = None
        super().__init__(*args, **kwargs)


class UnLockedButton(MenuButton):
    """
    Button to display unlocked levels
    """

    def __init__(self, *args, **kwargs):
        if "style" not in kwargs:
            kwargs["style"] = {}
        kwargs["style"]["bg_color"] = (40, 40, 140)
        super().__init__(*args, **kwargs)


class DoneButton(MenuButton):
    """
    Button to display finished levels
    """

    def __init__(self, *args, **kwargs):
        if "style" not in kwargs:
            kwargs["style"] = {}
        kwargs["style"]["bg_color"] = (20, 100, 20)
        super().__init__(*args, **kwargs)


class BlinkButton(MenuButton):
    """
    Button to draw attention.
    Currently, does not blink, has just a different color
    """

    def __init__(self, *args, **kwargs):
        if "style" not in kwargs:
            kwargs["style"] = {}

        self.init_color = (20, 100, 120)
        kwargs["style"]["bg_color"] = self.init_color
        self.green = 100
        self.low_green = 100
        self.high_green = 220
        self.increment = 3
        self._is_blinking = False
        # self.color = (20, self.green, 20)
        super().__init__(*args, **kwargs)

    def start_blinking(self):
        self._is_blinking = True

    def stop_blinking(self):
        self._is_blinking = False
        self._style["bg_color"] = self.init_color

    ## does not work, color is not changed
    def on_update(self, delta_time):
        if self.pressed:
            self.stop_blinking()

        if not self._is_blinking:
            return

        self.green += self.increment * delta_time * 60
        if self.green >= self.high_green or self.green <= self.low_green:
            self.increment *= -1
        self._style["bg_color"] = (20, self.green, 120)


# TODO: find a way to place the message box where we want, and not just have an
# anchor widget placed by the manager
# TODO: refactor with generic label box
class GenericBox(UIMouseFilterMixin, UIAnchorWidget):
    """
    A simple dialog box that pops up a message with buttons to close.
    Copy from python arcade API UIMessageBox, but with our agdb style.
    """

    def __init__(
        self,
        *,
        width: float = cst.Win.GENERICBOX_WIDTH,
        height: float = cst.Win.GENERICBOX_HEIGHT,
        message_text: str,
        font_name: str = None,
        bg_texture: str = None,
        buttons=("Ok",),
        callback=None,
    ):
        """
        A simple dialog box that pops up a message with buttons to close.

        :param width: Width of the message box
        :param height: Height of the message box
        :param message_text:
        :param buttons: List of strings, which are shown as buttons
        :param callback: Callback function, will receive the text of the clicked button
        """

        space = cst.Win.GENERICBOX_PADDING

        if buttons:
            button_group = arcade.gui.UIBoxLayout(vertical=False)
            for button_text in buttons:
                if button_text == 'skip':
                    button = SkipButton(text="skip >>>", width=100)
                else:
                    button = MenuButton(text=button_text, width=100)
                button_group.add(button.with_space_around(left=space, right=space))
                button.on_click = self.on_ok
            bottom_height = button_group.height + space
        else:
            bottom_height = 0

        self._text_area = UITextArea(
            text=message_text,
            width=width - 2 * space,
            height=height - 2 * space - bottom_height,
            font_name=font_name or cst.Win.GENERICBOX_TEXT_FONT,
            text_color=cst.Win.GENERICBOX_TEXT_COLOR,
        )

        self._bg_tex = arcade.load_texture(bg_texture or ":window:grey_panel.png")

        self._callback = callback
        # self._text_area.fit_content()
        children = [
            UIAnchorWidget(
                child=self._text_area,
                anchor_x="center_x",
                anchor_y="top",
                align_x=0,
                align_y=-space,
            )
        ]
        if buttons:
            children.append(
                UIAnchorWidget(
                    child=button_group,
                    anchor_x="center_x",
                    anchor_y="bottom",
                    align_x=0,
                    align_y=space,
                )
            )
        group = UILayout(width=width, height=height, children=children).with_background(
            self._bg_tex
        )
        super().__init__(child=group)

    def show_all_message(self):
        # Used only for boxes with animated text
        pass

    def on_ok(self, event):
        self.parent.remove(self)
        if self._callback is not None:
            self._callback(event.source.text)


class MessageBox(GenericBox):
    pass


class ErrorBox(GenericBox):
    def __init__(self, message_text: str):
        super().__init__(width=600, height=400, message_text=message_text)


class ConfirmationBox(MessageBox):
    """
    Display a box asking if user really wants to do it.
    """

    def __init__(self, message: str, callback=None):
        super().__init__(
            width=300,
            height=200,
            callback=callback,
            message_text=message,
            buttons=("Ok", "Cancel"),
        )


class GreedyInputText(UIInputText):
    """
    An UIInputText widget that catches all key and mouses presse/releases events
    for itself and consumes them.
    """

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # TODO: try to have a visible caret. The following does not work
        # self.caret.color = (80, 20, 20)
        # self.caret.visible = True

    def on_event(self, event: UIEvent):
        # make the widget always active so text input anywhere on the window
        # gets input in the field
        self._active = True
        super().on_event(event)

        # Consume key presses/releases so they do not activate other widgets or
        # the main game area
        if (
            isinstance(event, UIMousePressEvent)
            or isinstance(event, UIMouseReleaseEvent)
            or isinstance(event, UIKeyPressEvent)
            or isinstance(event, UIKeyReleaseEvent)
        ):
            # log.debug(f"Consumed event in input area: {event}")
            return EVENT_HANDLED


class GenericLabelBox(UIMouseFilterMixin, UIAnchorWidget):
    def __init__(
        self,
        *,
        width=cst.Win.GENERICLABELBOX_WIDTH,
        height=cst.Win.GENERICLABELBOX_HEIGHT,
        label=None,  # if given a string, will add a label at top of the box
        text="",
        with_input=False,  # change to True to have an input field in the box
        callback=None,
        buttons=[],
        texture=None,
    ):
        self._callback = callback
        space = cst.Win.GENERICLABELBOX_PADDING

        self._full_text = text

        self._top_label = None
        if label is not None:
            self._top_label = UILabel(
                text=label,
                font_name=cst.Win.GENERICLABELBOX_TOP_LABEL_FONT,
                font_size=cst.Win.GENERICLABELBOX_TOP_LABEL_SIZE,
                text_color=cst.Win.GENERICLABELBOX_TOP_LABEL_COLOR,
            )

        self._top_message = UITextArea(
            text=self._full_text + "\xA0\n",  ## Reserve a bit more space than necessary
            width=width - 2 * space,
            font_name=cst.Win.GENERICLABELBOX_TOP_MESSAGE_FONT,
            font_size=cst.Win.GENERICLABELBOX_TOP_MESSAGE_SIZE,
            text_color=cst.Win.GENERICLABELBOX_TOP_MESSAGE_COLOR,
        )
        # Compute the height necessary for the whole text
        self._top_message.fit_content()
        cur_height = self._top_message.height

        if not with_input:
            self._entry_area = None
        else:
            # The entry where user input is stored
            self._entry_area = GreedyInputText(
                width=width - 2 * space,
                height=cst.Win.GENERICLABELBOX_ENTRY_HEIGHT,
                font_name=cst.Win.GENERICLABELBOX_ENTRY_FONT,
                font_size=cst.Win.GENERICLABELBOX_ENTRY_SIZE,
                text_color=cst.Win.GENERICLABELBOX_ENTRY_COLOR,
                archor_x="center_x",
                text="",
            )
            # Add padding only to we can change the background color
            self._entry_padding = UIPadding(
                child=self._entry_area, bg_color=arcade.color.DARK_GRAY
            )
            cur_height += self._entry_padding.height + 2 * space

        self._buttons_texts = buttons
        if buttons:
            button_group = arcade.gui.UIBoxLayout(vertical=False)
            for button_text in buttons:
                if button_text == 'skip':
                    button = SkipButton(text="skip >>>", width=100, height=20)
                else:
                    button = MenuButton(text=button_text, width=100, height=20)
                button_group.add(button.with_space_around(left=space, right=space))
                button.on_click = self.on_ok
            cur_height += button_group.height + space

        # same background as the message box but darker
        # self._bg_tex = arcade.load_texture(":window:grey_panel2.png")
        self._bg_tex = arcade.load_texture(texture or ":window:grey_panel.png")

        children = [
            UIAnchorWidget(
                child=self._top_message.with_space_around(),
                anchor_x="center_x",
                anchor_y="top",
                align_x=space / 2,
                align_y=-space,
            ),
        ]
        if with_input:
            children.append(
                UIAnchorWidget(
                    child=self._entry_padding.with_space_around(
                        bottom=space, right=space
                    ),
                    anchor_x="center_x",
                    anchor_y="bottom",
                    align_x=space / 2,
                    align_y=0,
                )
            )
        if buttons:
            children.append(
                UIAnchorWidget(
                    child=button_group,
                    anchor_x="center_x",
                    anchor_y="bottom",
                    align_x=0,
                    align_y=space,
                )
            )
        if label:
            children.append(
                UIAnchorWidget(
                    child=self._top_label.with_space_around(
                        left=space / 2, right=space / 2
                    ).with_background(self._bg_tex),
                    anchor_x="center_x",
                    anchor_y="top",
                    align_x=0,
                    align_y=space / 2,
                )
            )

        group = UILayout(
            width=width, height=cur_height + 2 * space, children=children
        ).with_background(self._bg_tex)
        super().__init__(child=group)

    def show_all_message(self):
        # Used only for boxes with animated text
        pass

    def close(self, event):
        log.debug(f"Removing label/input box from parent {self.parent}")
        self.parent.remove(self)
        if self._callback:
            if self._entry_area:
                self._callback(self._entry_area.text)
            else:
                self._callback(event)

    def on_ok(self, event):
        self.parent.remove(self)
        if self._callback is not None:
            self._callback(event.source.text)


class AnimGenericLabelBox(GenericLabelBox):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self._idx_text = 1
        self._words = self._full_text.split(' ')
        self.time_waited = 0.0

        self.active_box = True  # either animating text, or in follow-up delay
        self.anim_text = True  # currently animating text

        # Now erase text and put only the first char
        self._idx_word = -1
        self._text = ""
        self.prepare_next_word()
        self._previous_words = ""
        self._last_word = ""
        self._top_message.text = self._text

    def animation_finished(self):
        self.anim_text = False
        self.time_waited = 0

    def prepare_next_word(self):
        self._idx_word += 1
        if self._idx_word == len(self._words):
            # stop
            self._last_word = ""
            self.animation_finished()
            return
        self._last_word = ""
        word = self._words[self._idx_word]
        self._current_word = word
        self._anim_word = ["\xA0"] * len(
            word
        )  # list of non-breaking space, so that pyglet will not break word into multiple lines
        self._idx_char_in_word = -1

    def show_all_message(self):
        self._top_message.text = self._full_text
        self.animation_finished()

    def on_update(self, delta_time):
        if not self.active_box:
            return

        self.time_waited += delta_time

        if not self.anim_text:
            if self.time_waited >= cst.Anim.POST_TEXT_DELAY:
                self.active_box = False
            return

        if self.time_waited <= cst.Anim.TEXT_UPDATE_DELAY:
            return

        for i in range(ceil(self.time_waited/cst.Anim.TEXT_UPDATE_DELAY)):
            if not self.anim_text:
                return
            
            self._idx_char_in_word += 1
            if self._idx_char_in_word == len(self._current_word):
                self._previous_words += self._last_word + ' '
                self.prepare_next_word()
            else:
                self._anim_word[self._idx_char_in_word] = self._current_word[
                    self._idx_char_in_word
                ]
                self._last_word = ''.join(self._anim_word)
            self._top_message.text = self._previous_words + self._last_word

        self.time_waited = 0.0

class GreedyLabelBox(AnimGenericLabelBox):
    """
    Box that greedily consumes all keyboard / mouse events until it is closed
    """

    def on_event(self, event):
        if isinstance(event, UIKeyPressEvent):
            # log.debug(f"Key pressed: {event}")
            if event.symbol == KEY.ENTER:
                if len(self._buttons_texts) > 0:
                    self.close(self._buttons_texts[0])
                else:
                    self.close("UNKNOWN")
                return EVENT_HANDLED
            elif event.symbol == KEY.ESCAPE and not self._entry_area:
                if len(self._buttons_texts) > 0:
                    if len(self._buttons_texts) > 1:
                        self.close(self._buttons_texts[1])
                    else:
                        self.close(self._buttons_texts[0])
                else:
                    self.close("UNKNOWN")
                return EVENT_HANDLED

            return EVENT_UNHANDLED

        if isinstance(event, UIOnUpdateEvent):
            self.dispatch_event("on_update", event.dt)

        # pass event to children
        for child in self.children:
            if child.dispatch_event("on_event", event):
                return EVENT_HANDLED


class OutputBox(AnimGenericLabelBox):
    def __init__(self, *args, **kwargs):
        super().__init__(
            *args,
            texture=":window:grey_panel2.png",
            label=message('output/messagelabel'),
            **kwargs,
        )
        # add a label at the top


class InputBox(GreedyLabelBox):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, with_input=True, **kwargs)


# class LogBox(GenericBox):
# class LogBox(arcade.gui.UITextArea):
class LogBox(UITexturePane):
    def __init__(self):
        self.my_text = ""

        self.log_width = cst.Win.LOGBOX_WIDTH
        self.log_height = cst.Win.LOGBOX_HEIGHT
        self.log_hidden_height = 1
        self.appearing = False

        self._log_area = UITextArea(
            width=self.log_width,
            height=self.log_height,
            text_color=cst.Win.LOGBOX_TEXT_COLOR,
            font_size=cst.Win.LOGBOX_TEXT_SIZE,
            font_name=cst.Win.LOGBOX_TEXT_FONT,
            # font_name=cst.Fonts.BITSTREAM,
            scroll_speed=30.0,
            text="",
        )

        transp_rect = arcade.Texture.create_filled(
            name='log_bg',
            size=(10, 10),
            color=arcade.color.BLACK + (int(255 * 75/100),),
        )
        pad = 10

        super().__init__(
            child=self._log_area,
            tex=transp_rect,
            padding=(pad, pad, pad, pad),
        )

    def reset(self):
        # hide the box to the right of the visible screen
        # by tweaking the x adjustment variable
        self.parent.align_x = self.log_width + 30
        self.appearing = False
        self._log_area.text = ""

    def append(self, msg):
        # make the box appear as soon as there is content in it
        self.appearing = True

        # self._log_area.height = self.log_height
        self._log_area.text += msg + "\n"
        self._log_area.layout.view_y = -inf  # scroll to bottom

    def on_update(self, event):
        # make the box appear as soon as there is content in it
        if not self.appearing:
            return

        self.parent.align_x -= 10

        if self.parent.align_x <= 0:
            self.parent.align_x = 0
            self.appearing = False

    def on_event(self, event: UIEvent):
        """
        Inspired from arcade source for UITextArea, but returns HANDLED if scroll is happening.
        """
        if super().on_event(event):
            return EVENT_HANDLED

        if isinstance(event, UIMouseScrollEvent):
            if self.rect.collide_with_point(event.x, event.y):
                return EVENT_HANDLED

        return EVENT_UNHANDLED


def transparent_rect(left, right, top, bottom):
    arcade.draw_lrtb_rectangle_filled(
        left=left,
        right=right,
        top=top,
        bottom=bottom,
        color=arcade.color.BLACK + (80,),
    )


def draw_bar(l, r, t, b):
    arcade.draw_lrtb_rectangle_filled(
        left=l, right=r, top=t, bottom=b, color=arcade.color.WHITE
    )


def draw_pause(left, right, top, bottom):
    space = 20
    width = 10
    height = 40
    # draw a transparent white rectangle over the whole game area
    arcade.draw_lrtb_rectangle_filled(
        left=left,
        right=right,
        top=top,
        bottom=bottom,
        color=arcade.color.PASTEL_GRAY + (int(255*25/100),), # pastel grey with 25% opacity
    )
    # Crude "pause" symbol
    draw_bar(left + space, left + space + width, top - space, top - space - height)
    off = width + 2 * space / 3
    draw_bar(
        left + space + off,
        left + space + off + width,
        top - space,
        top - space - height,
    )


cosPi6 = cos(pi / 6)


def draw_play(left, top):
    tsize = 40
    space = 20
    arcade.draw_triangle_filled(
        left + space,
        top - space,
        left + space,
        top - space - tsize,
        left + space + tsize * cosPi6,
        top - space - tsize / 2,
        color=arcade.color.WHITE,
    )
