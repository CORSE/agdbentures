import threading
import traceback

def show_threads(print_fn):
    """
    Gives debug information about the current running threads.
    :param print_fn: the printing function to be used.
    """
    print_fn(f"Currently {threading.active_count()} threads running")
    for thread in threading.enumerate():
        print_fn(f"\t{thread.name}: {thread}")

def show_stack():
    """
    Show stack at a given point in code, useful during debug.
    """
    traceback.print_stack()

