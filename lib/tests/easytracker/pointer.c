#include <stdlib.h>

int player_x = 5;
int player_y = 3;
int exit_x = 6;
int exit_y = 3;
int player_direction = 1;

void forward(int *player_x) { (*player_x)++; }

int main()
{
    forward(&player_x);
    if (player_x == exit_x) {
        exit(EXIT_SUCCESS);
    } else {
        exit(EXIT_FAILURE);
    }
}
