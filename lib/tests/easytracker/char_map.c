#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char str_map[] = "\
+------------------------+\n\
|                        |\n\
|                        |\n\
|     >     @            |\n\
|                        |\n\
|                        |\n\
|                        |\n\
+------------------------+\n\
";

int player_x;
int player_y;

char *the_map = NULL;
int width;
int height;

void load_map(const char *the_str_map)
{
    int len = strlen(the_str_map);
    free(the_map);
    the_map = malloc(len * sizeof(char));
    width =
        strchr(str_map, '\n') - the_str_map + 1;  // keeping the '\n' in the row
    height = len / width;
    assert(len % width == 0);
    memcpy(the_map, the_str_map, len);  // No '\0' at the end of this one
    int row;
    int col;

    for (row = 0; row < height; row++) {
        for (col = 0; col < width; col++) {
            if (the_map[row * width + col] == '>') {
                player_y = row;
                player_x = col;
            }
        }
    }
}

void move_forward(void)
{
    the_map[player_y * width + player_x] = ' ';
    player_x++;
    the_map[player_y * width + player_x] = '>';
}

int main()
{
    load_map(str_map);
    move_forward();
    exit(EXIT_SUCCESS);
}
