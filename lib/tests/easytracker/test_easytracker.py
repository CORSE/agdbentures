from collections import defaultdict
import logging
import os
import sys
import pytest
import queue
import subprocess
from os.path import join, dirname, splitext
import tempfile

from easytracker import init_tracker, PauseReasonType
from level.console import ConsolePrintDummy
from level.level_test import LevelTest
from level.world import frames_then_global
from loader.loader_utils import import_from_file, get_dynamic_level
from logs import tst
import tests.lib_test as T


def assert_in_caplog(caplog, levelno, msg):
    # print("RECORDS IN CAPLOG")
    for record in caplog.records:
        # print(dir(record), record)
        # print(f"levelname {record.levelname}")
        # print(f"levelnumber {record.levelno}")
        # print(f"levelnumber Error {logging.ERROR}")
        if record.levelno == levelno:
            if msg in record.message:
                return
    # print("END RECORDS IN CAPLOG")
    assert False, f"Not found in logs: {msg}"


def assert_no_errors(caplog, levelnolimit):
    for record in caplog.records:
        # print(dir(record), record)
        # print(f"levelname {record.levelname}")
        # print(f"levelnumber {record.levelno}")
        # print(f"levelnumber Error {logging.ERROR}")
        if record.levelno >= levelnolimit:
            assert False, f"Log level too high {record.levelname} {record.message}"


class LevelUnitTest(LevelTest):
    def __init__(self, srcfile, flags=None, engine=None, debug=False):
        self.debug_mode = debug
        self.flags = flags

        # not an actual thread, will just discard commands sent and do
        # nothing the gdb's stdout
        self.print_thread = ConsolePrintDummy()

        exec_name, ext = splitext(srcfile)
        assert ext == ".c"
        if debug:  # not deleted on LevelUnitTest deletion
            self.temp_dir_name = tempfile.mkdtemp(prefix="agdb_unittest_")
        else:
            self.temp_dir = tempfile.TemporaryDirectory(prefix="agdb_unittest_")
            self.temp_dir_name = self.temp_dir.name

        self.metadata = defaultdict(
            lambda: None,
            {
                'level_name': f"Pytest easytracker {exec_name}",
                'level_path': join(dirname(__file__), srcfile),
                'exec_name': exec_name,
                'arcade_maps': {},
                'wops': {},
                # hacking the level_abc source_level_dir so it looks
                # there for the executable
                'source_level_dir': self.temp_dir_name,
                'engine_name': engine,
                'player_y': 3,
                'exit_x': 5,
                'exit_y': 3,
            },
        )

        self.compile()

        level_in_queue = queue.Queue()
        level_out_queue = queue.Queue()

        level = super().__init__(
            self.metadata,
            level_in_queue,
            level_out_queue,
        )

    def __del__(self):
        if not self.debug_mode:
            # TemporaryDirectory will be automatically deleted
            # no need to to cleanup() manually
            return

        print(f"TemporaryDirectory kept: {self.temp_dir_name}")
        logfile = join(self.temp_dir_name, "logs.txt")
        print("Showing contents of logs.txt file")
        with open(logfile, 'r') as file:
            contents = file.read()
            print("Contents of logfile:")
            print(contents)

    def compile(self, flags=None, defines=None):
        self.level_path = self.metadata['level_path']
        target_dir = self.metadata['source_level_dir']
        self.exec_name = join(target_dir, self.metadata['exec_name'])

        tst.info(f"Compiling {self.level_path} into {self.exec_name} {defines}")
        args = [
            "clang",
            "-g",
            self.level_path,
            "-o",
            self.exec_name,
        ]
        if flags:
            args.extend(flags)
        elif self.flags:
            args.extend(self.flags)
        else:
            args.extend(
                [
                    "-Wall",
                    "-Werror",
                    "-Wextra",
                ]
            )
        if defines:
            args.extend(defines)

        result = subprocess.run(
            args,
            capture_output=True,
            check=True,
        )

        tst.info(f"Compilation returned: {result.returncode}")
        if result.stdout:
            tst.info(f"Compilation stdout: {result.stdout.decode()}")
        if result.stderr:
            tst.warning(f"Compilation stderr: {result.stderr.decode()}")

        assert (
            result.returncode == 0
        ), "Failed compilation should be catched by 'check=True'"
        # tst.trace("test trace")
        # tst.debug("test debug")
        # tst.info("test info")
        # tst.warning("test warning")
        # tst.error("test error")
        # tst.critical("FATAL ERROR")

    def do_start(self):
        """
        Redefine the do_start method to only do what we want in tests
        """
        tst.debug("Performing do_start at the LevelUnitTest")
        if self.start_number == 0:
            self.load_and_start_program()
        else:
            self.reload_and_start_program()
        self.inferior_started = True
        self.start_number += 1

    def test(self):
        pass


def test_pointer(caplog):
    # caplog.set_level(logging.DEBUG)
    # caplog.set_level(logging.INFO)

    level = LevelUnitTest("pointer.c")

    # load in agdbentures

    # T.ccontinue(12)
    # level._gdb_next(3)
    level.do_start()

    # level._gdb_step()

    memory = level.tracker.get_program_memory(as_raw_python_objects=True)
    print(memory)
    coord_x = frames_then_global(memory, 'player_x')
    level._gdb_step()

    # this lines goes wild into infinite loop :-(  )
    memory = level.tracker.get_program_memory(as_raw_python_objects=True)
    print(memory)

    level._gdb_next()

    print("PAUSE GDB:", level.tracker.pause_reason)

    assert_no_errors(caplog, logging.WARNING)


def test_string(caplog):
    # caplog.set_level(logging.DEBUG)
    # caplog.set_level(logging.INFO)

    level = LevelUnitTest("string.c")

    # load in agdbentures

    # T.ccontinue(12)
    # level._gdb_next(3)
    level.do_start()
    level._gdb_next()
    level._gdb_step()

    # Retrieving string passed as char* to unknown location
    memory = level.tracker.get_program_memory(as_raw_python_objects=True)
    print(memory)
    msg = frames_then_global(memory, 'msg')
    assert msg == ('H',), "char* must not be scanned when retrieving memory"
    msg = level.tracker.get_variable_value_as_str('msg', 'char*')
    assert msg == "Hello world!"

    # Retrieving string passed as char* to malloc'd location
    level._gdb_finish()
    level._gdb_next()
    level._gdb_step()

    memory = level.tracker.get_program_memory(as_raw_python_objects=True)
    print(memory)
    msg = frames_then_global(memory, 'msg')
    assert msg == ['x'] * 20, "Array should be 20 long"

    # Retrieving string using 'print' from gdb
    msg = level.tracker.get_variable_value_as_str('msg', 'char*')
    assert msg.startswith("x' <repeats")
    assert_no_errors(caplog, logging.WARNING)


def test_missing_var(caplog):
    caplog.set_level(logging.DEBUG)

    level = LevelUnitTest("missing_var.c", engine="simple_map")
    level.do_start()
    level._gdb_next()
    level.update_all()

    assert_in_caplog(
        caplog,
        logging.WARNING,
        "Cannot find `player_x` in current map structure",
    )


def test_char_map(caplog):
    # caplog.set_level(logging.DEBUG)
    level = LevelUnitTest("char_map.c")  # , debug=True)
    level.do_start()
    level._gdb_next()

    memory = level.tracker.get_program_memory(as_raw_python_objects=True)
    r = level.tracker.get_variable_value_as_str('player_y', 'int')
    c = level.tracker.get_variable_value_as_str('player_x', 'int')
    w = level.tracker.get_variable_value_as_str('width', 'int')

    assert r == 3
    assert c == 6
    assert w == 27

    map = memory['global_variables']['the_map'].value

    print("Current map:", map)
    assert map[r * w + c] == '>', "starting player position"

    level._gdb_next()
    memory = level.tracker.get_program_memory(as_raw_python_objects=True)
    new_r = level.tracker.get_variable_value_as_str('player_y', 'int')
    new_c = level.tracker.get_variable_value_as_str('player_x', 'int')

    assert new_r == 3
    assert new_c == 7

    new_map = memory['global_variables']['the_map'].value

    assert len(map) == len(new_map), "map have same size"
    differences = []
    for i in range(len(new_map)):
        if map[i] != new_map[i]:
            differences.append((i, map[i], new_map[i]))

    assert len(differences) == 2
    assert differences[0] == (r * w + c, '>', ' ')
    assert differences[1] == (new_r * w + new_c, ' ', '>')

    assert_no_errors(caplog, logging.WARNING)


####
#
# The tests below use internal functions, breaks, etc. that are provided to
# levels.
#
####


def test_break_watch_internal(caplog):
    """
    Testing the behaviour with internal breaks on start and end of functions as
    well as watch.

    Important: we must use `parse_and_eval` here and not directly
    level._gdb_next and others as we are testing the agdbentures handling of
    internal breakpoints, with the automatic continuing to make it transparent
    to the user.
    """
    # caplog.set_level(logging.DEBUG)
    # caplog.set_level(logging.DEBUG, logger="easytracker")

    level = LevelUnitTest(
        "break-and-watch.c", flags=['-Wall', '-Wextra']
    )  # , debug=True)

    num_enter_myfunc = 0
    num_leave_myfunc = 0
    num_modify_playerx = 0

    def catch_myfunc():
        nonlocal num_enter_myfunc
        tst.info("Entering myfunc")
        num_enter_myfunc += 1

    def catch_leave_myfunc():
        nonlocal num_leave_myfunc, level
        assert level.tracker.get_current_function_name() == "myfunc"
        tst.info("Leaving myfunc")
        num_leave_myfunc += 1

    def catch_watch_player_x():
        nonlocal num_modify_playerx, level
        assert level.tracker.get_current_function_name() == "myfunc"
        tst.info("Player x modified")
        num_modify_playerx += 1

    level.do_start()
    level.register_breakpoint("myfunc", catch_myfunc)
    level.register_leave_function("myfunc", catch_leave_myfunc)
    level.register_watch("player_x", catch_watch_player_x)
    level.parse_and_eval("step")
    # Warning: is a breakpoint although it is an internal breakpoint
    assert level.tracker.pause_reason.type == PauseReasonType.BREAKPOINT
    assert level.tracker.get_current_function_name() == "myfunc"
    assert num_enter_myfunc == 1
    assert num_leave_myfunc == 0
    level.parse_and_eval("next")
    assert level.tracker.pause_reason.type == PauseReasonType.WATCHPOINT
    assert level.tracker.get_current_function_name() == "myfunc"
    assert num_leave_myfunc == 0
    print(level.tracker.get_gdb_console_output())
    assert num_modify_playerx == 1
    level.parse_and_eval("next")
    # Should be ENDSTEPPING_RANGE because, although there was a breakpoint, we
    # are in the same frame
    assert level.tracker.pause_reason.type == PauseReasonType.ENDSTEPPING_RANGE
    assert level.tracker.get_current_function_name() == "main"
    assert num_leave_myfunc == 1

    # Breaks should be persistent across restart

    level.do_start()
    level.parse_and_eval("continue")
    assert level.tracker.pause_reason.type == PauseReasonType.EXITED
    assert num_enter_myfunc == 2
    assert num_leave_myfunc == 2
    assert num_modify_playerx == 2

    # internal breaks and watches should be transparent across next if in 
    # called function

    level.do_start()
    level.parse_and_eval("next")
    assert level.tracker.pause_reason.type == PauseReasonType.FUNCTION_FINISHED
    assert num_enter_myfunc == 3
    assert num_leave_myfunc == 3
    assert num_modify_playerx == 3

    assert_no_errors(caplog, logging.WARNING)


def test_teleport(caplog):
    """
    Testing the automatic delete and restore feature for internal breakpoints.
    It is particularly important for break_end_of_func because if the
    function's size changes after recompilation, then gdb will try to re-insert
    the initial breakpoint into a different place, sometimes "corrupting" the
    instructions (e.g., inserting it's 0xcc into a data place instead of the
    opcode).
    """
    # caplog.set_level(logging.DEBUG)
    # caplog.set_level(logging.DEBUG, logger="easytracker") # need also to deactivate colors in logs.py

    level = LevelUnitTest("teleport.c", flags=['-Wall', '-Wextra'])  # , debug=True)
    assert_in_caplog(
        caplog,
        logging.WARNING,
        "left operand of comma operator has no effect",
    )

    tr = level.tracker

    def catch_end_teleport():
        tst.info("Break at end of teleport")

    level.do_start()
    level.register_leave_function("teleport", catch_end_teleport)
    level.parse_and_eval("next 5")

    bug_y = tr.get_variable_value_as_str('player_y', 'int')
    bug_x = tr.get_variable_value_as_str('player_x', 'int')

    tst.info(f"Values: {bug_y=} and {bug_x=}")

    assert bug_y == 6, "player_y should not change"
    assert bug_x == 12, "player_x should take value of tp_out_y"

    level.parse_and_eval("next")
    level.parse_and_eval("continue")

    assert not tr.is_running()
    pr = tr.pause_reason
    assert pr.type == PauseReasonType.EXITED
    # return code: failed
    assert pr.args[0] == 1

    level.compile(defines=["-D", "ANSWER"])
    tst.debug(tr.get_gdb_console_output())

    level.do_start()
    tst.debug(tr.get_gdb_console_output())

    level.parse_and_eval("next 4")
    tst.debug(tr.get_gdb_console_output())
    assert tr.pause_reason.type == PauseReasonType.ENDSTEPPING_RANGE

    level.parse_and_eval("step")
    tst.debug(tr.get_gdb_console_output())
    assert tr.pause_reason.type == PauseReasonType.ENDSTEPPING_RANGE
    level.parse_and_eval("step")
    tst.debug(tr.get_gdb_console_output())
    assert tr.pause_reason.type == PauseReasonType.ENDSTEPPING_RANGE
    level.parse_and_eval("step")
    tst.debug(tr.get_gdb_console_output())
    assert tr.pause_reason.type == PauseReasonType.ENDSTEPPING_RANGE, "Previous breakpoint has broken the instruction"

    ans_y = level.tracker.get_variable_value_as_str('player_y', 'int')
    ans_x = level.tracker.get_variable_value_as_str('player_x', 'int')

    tst.info(f"Values: {ans_y=} and {ans_x=}")
    assert ans_y == 12
    assert ans_x == 9

    level.parse_and_eval("continue")

    assert not level.tracker.is_running()
    pr = level.tracker.pause_reason
    assert pr.type == PauseReasonType.EXITED
    # return code: success
    assert pr.args[0] == 0

    assert_no_errors(caplog, logging.ERROR)
