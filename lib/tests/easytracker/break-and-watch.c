#include <stdlib.h>

/*
 * Tests the behaviour of internal breakpoints when arriving on break point.
 * with:
 * - continue
 * - next
 * - step
 */

int player_x = 3;
int player_y;

void myfunc(void) {
    player_x++;
}

int main(void)
{
    myfunc();
    exit(EXIT_SUCCESS);
}
