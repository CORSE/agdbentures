#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int player_x;

void message(const char msg[]) { printf("The message: %s\n", msg); }

int main(void)
{
    char *buffer = malloc(20 * sizeof(char));
    message("Hello world!");

    memset(buffer, 'x', 24); // too much
    message(buffer);

    memset(buffer+12, 'y', 12); // too much
    message(buffer);

    exit(EXIT_SUCCESS);
}
