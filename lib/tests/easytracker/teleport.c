#include <stdlib.h>

int player_x;
int player_y;

int tp_out_x;
int tp_out_y;

void teleport(void) {
#ifdef ANSWER
    player_x = tp_out_x;
    player_y = tp_out_y;
#else
    player_y, player_x = tp_out_y, tp_out_x;
#endif
}

int main(void)
{
    player_x = 3;
    player_y = 6;
    tp_out_x = 9;
    tp_out_y = 12;

    teleport();

    if (player_x == tp_out_x && player_y == tp_out_y) {
        exit(EXIT_SUCCESS);
    } else {
        exit(EXIT_FAILURE);
    }
}
