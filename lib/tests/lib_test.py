"""
Library to do testing on levels
"""

import sys
import os
import time
import traceback
from enum import Enum, unique
from termcolor import colored, cprint
from queue import Queue

from logs import tst

##
#
# Printing routines
#
##


class color:
    success = "\033[32m"     # closing ]
    failed = "\033[31;1m"    # closing ]
    info = "\033[33m"        # closing ]
    default = "\033[0m"      # closing ]

cols = 60

def alignLR(left, right):
    dots = "." * (cols - len(left) - len(right) - 2)
    return left + " " + dots + " " + right


print_error = lambda x: cprint(x, "red", attrs=["bold"])
print_errinf = lambda x: cprint(x, "red")
print_warn = lambda x: cprint(x, "yellow")
print_succ = lambda x: cprint(x, "green")
print_info = lambda x: cprint(x, "yellow", attrs=["dark"])


test_warn = print_warn
test_succ = print_succ
test_info = print_info

##
#
# Variables to hold statistics
#
##

global num_tests, num_passed, num_errors, num_skipped
num_tests = 0
num_passed = 0
num_errors = 0
num_skipped = 0

### Commands available in "Test" method in exercice class

# This queue saves commands that will be used when testing a scenario.
# All calls to T.<command> will append a command to the queue.
# Commands will be executed from the queue whenver a self.run() is triggered.
#
global _current_commands
_current_commands = Queue()


### Commands available in "Test" method in exercice class
# Test commands are generated based on this Enum class.
# For each element, a corresponding fonction is created that
# adds the command to the _current_commands queue.
#
# They can be called in the `test` method of levels, e.g. as follows:
#   import tests.lib_test as T
#   T.reset()
#   T.expect_error()
#   T.expect_string("my string")
#
# Special action can be set for each command in the `apply_command`
# of the LevelTest class in the lib/level/level_test.py file.
#
###
@unique
class _Cmd(Enum):
    # Victory means the inferior exited with exit status 0
    expect_victory = 1
    # Defeat means the inferior exited with exit status 1
    expect_defeat = 2

    # TODO: Is reset used ???
    reset = 3
    # Sleeps for the given number of seconds (float argument)
    sleep = 4
    # Segfault need to appear
    expect_segfault = 5
    # Infinite loop are actually "loops that take more time than expected"
    expect_infinite_loop = 6
    # This is actually the generale case (no infinite loop), but still give the
    # option to write more readable tests
    unexpect_infinite_loop = 7
    # There should be a gdb error at this point (TODO: verify which error?)
    expect_error = 8
    # There should NOT be a gdb error at this point
    unexpect_error = 9
    # Last output from inferior should be this string
    expect_string = 10
    # Last output from inferior should contain all the strings in the list
    expect_sub_strings = 11
    # Last output from inferior should NOT be this string
    unexpect_string = 12
    # Last output from inferior should NOT contain...
    unexpect_sub_strings = 13

    # Same as the above functions, but checks againts the last
    # message from agdb (could be the WOD or information)
    expect_agdb_string = 14
    expect_agdb_sub_strings = 15
    unexpect_agdb_string = 16
    unexpect_agdb_sub_strings = 17

    # Send strings to the inferior's stdin
    send_input = 18
    up = 19
    down = 20
    left = 21
    right = 22

    # level has send a payload to GUI,
    # keyword arguments configure what to look for.
    # See function `verify_payload_queue` below for more information.
    payload = 23

    # stop testing from here ?NOT IMPLEMENTED?
    stop = 25
    # skip the rest of the commands ?NOT IMPLEMENTED?
    skip = 26

    # Hook to attach a custom function, e.g.
    # T.hook(self.verify_this_stuff)
    hook = 27
    # ?NOT IMPLEMENTED? TODO
    start_loop = 28
    end_loop = 29

    ## inspection
    # Check that a variable has a particular value
    variable = 30

    # command that does nothing, used to catch errors
    dummy = 39

    ## Common gdb commands that manipulate execution

    # Generic gdb command to manipulate execution
    # argument will be the command to send to gdb
    # Warning: this is probably never used and not tested.
    # The code to handle this command is probably not existant, but one
    # can imagine it can be completed and should be used e.g. as follows:
    # T.command("continue")
    command = 40

    # These commands will create functions that send to gdb the name without the leading 'c'
    # e.g.: T.crun() will send "run" to gdb
    crun = 41
    cnext = 42
    cstep = 43
    ccontinue = 44


# List of commands that manipulate gdb execution.
# These are all commands that appear after the `command` element of the above
# Enum.
# This list if filled later by the for loop that iterate over all the enum
# elements
gdb_exec_commands = []


# Define a function for each command available.
# When called, these functions add to the queue the corresponding command.
fn_template = """
def {name}(*args, **kwargs):
    global _current_commands
    _current_commands.put((_Cmd.{name}, "{cmdstr}", args, kwargs))"""


for cname, cmd in _Cmd.__members__.items():
    # print ("Command:", cname, cval)
    if cname[0] == "c" and cname != "command":
        cmdstr = cname[1:]
    else:
        cmdstr = cname
    exec(fn_template.format(name=cname, cmdstr=cmdstr))
    if cmd.value >= _Cmd.command.value:
        gdb_exec_commands.append(cmd)

def has_cmd():
    """ Check if there is a command available on the queue. """
    global _current_commands
    return not _current_commands.empty()


def read_cmd():
    """ Return the next available command on the queue. """
    global _current_commands

    if _current_commands.empty():
        raise ValueError("no items on command list")

    fullcmd = _current_commands.get()
    return fullcmd

def clear_cmds():
    """Clear the command queue, in case a level did not empty it."""
    global _current_commands
    while not _current_commands.empty():
        _current_commands.get_nowait()


def run(self) -> None:
    """
    Run in test mode. Seems DEPRECATED.
    See the "run" method in the LevelTest class instead.
    """
    raise Exception("Should not call this function, maybe? It looks deprecated")

    self.prepare_tracker()
    pause_type = self.tracker.pause_reason.type

    while pause_type != PauseReasonType.EXITED:
        # Fetch variables
        self.update_state()

        # Print inferior's output
        self.print_stdout()

        # Update the code visualizator
        self.show_next_line()

        # Show the map
        self.show_map()
        self.print_wop_msg()

        # Read a command and run it
        cmd = self.read_cmd()
        self.parse_and_eval(cmd)
        pause_type = self.tracker.pause_reason.type

    self.print_stdout()

    code = self.tracker.exit_code
    print(f"Inferior exited with {code} as exit code.")


def error(msg):
    """ Record a test as an error. """
    global num_tests, num_errors
    num_tests += 1
    num_errors += 1
    print_error("Test failed: " + msg)
    ## uncomment if necessary to know who called the error function
    # traceback.print_stack()

def skipped(msg):
    """ Record a test as skipped. """
    global num_tests, num_skipped
    num_tests += 1
    num_skipped += 1
    print_warn("Test skipped: " + str(msg))

def passed(msg):
    """ Record a test as passed. """
    global num_tests, num_passed
    num_tests += 1
    num_passed += 1
    print_succ("OK " + msg)

def assert_eq(a, b, errmsg=""):
    if a == b:
        passed(f"Passed: {a} equals {b}")
    else:
        error(f"Error: {a} should equal {b} {errmsg}")

def verify_expect_string(st: str, args: [str], stream: str, should_be_equal: True):
    """
    Verify that a string is equal to the expected string.
    """
    if st == args[0]:
        if should_be_equal:
            passed("Found: " + ", ".join(args) + " in " + stream)
        else:
            error("Should not be: " + ", ".join(args) + " in " + stream)
            print_errinf("Last message was: '" + st + "'")
    else:
        if should_be_equal:
            error("Expected string: " + ", ".join(args) + " in " + stream)
            print_errinf("Last message was: '" + st + "'")
        else:
            passed(
                "String: "
                + ", ".join(args)
                + " not present in "
                + stream
                + " as expected"
            )
            print_info("Last message was: '" + st + "'")


def check_expect_sub_strings(st, args):
    for arg in args:
        if arg not in st:
            return False
    return True


def verify_expect_sub_strings(
    st: str, args: [str], stream: str, should_be_present: True
):
    if check_expect_sub_strings(st, args):
        if should_be_present:
            passed("Found: " + ", ".join(args) + " in " + stream)
        else:
            error("Should not find: " + ", ".join(args) + " in " + stream)
            print_errinf("Last message was: '" + st + "'")
    else:
        if should_be_present:
            error("Expected sub string: " + ", ".join(args) + " in " + stream)
            print_errinf("Last message was: '" + st + "'")
        else:
            passed(
                "Sub strings: "
                + ", ".join(args)
                + " not present in "
                + stream
                + " as expected"
            )
            print_info("Last message was: '" + st + "'")

def verify_payload_queue(queue, kwargs):
    """
    Check if a payload is on the queue.
    Warning: removes items from the queue until it finds a matching payload.
    """
    tst.debug(f"Looking for payload with contents {kwargs}")
    while not queue.empty():
        payload = queue.get()
        tst.debug(f"Testing payload {payload}")
        if 'topic' in kwargs:
            if 'topic' not in payload:
                continue
            else:
                top = payload['topic']
                tkind = kwargs['topic']
                if top != tkind:
                    continue

        if 'action' in kwargs:
            if 'action' not in payload:
                continue
                # error(f"Last payload has no action: {payload}")
            else:
                act = payload['action']
                akind = kwargs['action']
                if act.name != akind:
                    continue
        if 'message_substr' in kwargs:
            if 'message' not in payload:
                continue
            else:
                if kwargs['message_substr'] not in payload['message']:
                    continue

        passed(f"Found matching payload for {kwargs}")
        return

    error(f"Could not find matching payload {kwargs}")


def call_hook (*args):
    fun = args[0]
    return fun(*(args[1:]))

def warning(msg):
    print_warn("Warning: "+msg)

def info():
    ratio_passed = f"{num_passed}/{num_tests}"

    if num_passed == num_tests:
        print_succ(alignLR("All tests successfully passed.", ratio_passed))
        return True

    if num_errors > 1:
        pr = print_error
        exit_code = 1
    else:
        pr = print_warn
        exit_code = 0
    pr(
        alignLR(
            "{tst} tests: {pas} passed, {err} error(s) and {skip} skipped.".format(
                tst=num_tests, pas=num_passed, err=num_errors, skip=num_skipped
            ),
            ratio_passed,
        )
    )
    return False
