#!/usr/bin/env bash
# run all the tests

path="$(realpath "$0")"
dir="$(dirname "$path")"

for test_dir in "$dir"/*; do
  # run `make run` inside the directory
  if [[ -d $test_dir ]]; then
    make run -C "$test_dir"

    # Stop if the test failed
    if [[ $? != 0 ]]; then
      echo Test "$(basename "$test_dir")" failed.
      exit $?
    fi
  fi
done

echo Tests ok.

# vim:set ts=8 sts=2 sw=2 et:
