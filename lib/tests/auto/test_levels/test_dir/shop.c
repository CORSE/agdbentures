#include "shop.h"

const int weapon_price[WEAPONS_N] = {0, 1, 2, 5};
const int armor_price[ARMORS_N] = {0, 2, 4, 8};

int buy_generic(const int price[], int num_prices)
{
    int idx=num_prices-1;
    while (idx > 0 && price[idx] > gold) { idx--; };
    gold = gold - price[idx];
    if (!idx) {
        printf ("You do not have enough gold for that.\n");
    } else {
        printf ("You buy something for %d gold.\n",price[idx]);
        printf ("You now have %d gold.\n",gold);
    }
    return idx;
}

weapon_type buy_weapon(void)
{
    printf("You try to buy a weapon...\n");
    int widx  = buy_generic (weapon_price, WEAPONS_N);
    return (weapon_type)widx;
}

armor_type buy_armor(void)
{
    printf("You try to buy armor...\n");
    int aidx  = buy_generic (armor_price, ARMORS_N);
    return (armor_type)aidx;
}



