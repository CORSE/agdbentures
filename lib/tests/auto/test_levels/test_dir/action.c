/**
 * Very simple program representing a character moving in 8 directions. Can't pass through walls,
 * and can pick "items" by walking on it.
 * 
 * Concepts used: Variable locality, reference passing, remainder operator
 * 
 * Possible bugs:
 *  - not understanding the % operator
 *  - passing a negative int while the parameter is an unsigned. I think in that case it underflows and is MAX_UNSIGNED - n
 *  - confusing x and y, because this program does level[y][x]
 * 
 * Game design related tip:
 *   it is common in fighting games to represent the directional inputs using numbers from 1 to 9.
 *   Just think on how they are disposed on a numeric keypad:
 *    ↖  ↑  ↗
 *     7 8 9
 *   ← 4 5 6 →
 *     1 2 3
 *    ↙  ↓  ↘
*/

#include "action.h"

/***********************************/
/*   Constants, types, variables   */
/***********************************/

/***********************************/
/*      Methods and functions      */
/***********************************/

void turn_right (void)
{
    int d;
    switch (player_direction) {
        case 8: d=6; break;
        case 4: d=8; break;
        case 2: d=4; break;
        case 6: d=2; break;
        default: assert(false);
    }
    player_direction = d;
}

void turn_left (void)
{
    int d;
    switch (player_direction) {
        case 8: d=4; break;
        case 4: d=2; break;
        case 2: d=6; break;
        case 6: d=8; break;
        default: assert(false);
    }
    player_direction = d;
}

void coord_in_dir_n (int y, int x, int dir, int *cy, int *cx, int n)
{
    assert (dir >= 1 && dir <= 9);

    // warning: (0, 0) is top left

    // vertical movement
    if (dir >= 7) {
        // 7, 8, 9
        *cy = y - n;
        /* BUGLINE: forgetting that y = 0 is at the top
         * y = player_y + 1;
         */
    } else if (dir <= 3) {
        // 1, 2, 3
        *cy = y + n;
    } else {
        *cy = y;
    }

    if (dir % 3 == 0) {
        // 3, 6, 9
        *cx = x + n;
        /* BUGLINE: thinking x is also inverted because y is
         * x = pox_x - 1;
         */
    } else if (dir % 3 == 1) {
        // 1, 4, 7
        *cx = x - n;
    } else {
        *cx = x;
    }
}


void coord_in_dir (int y, int x, int dir, int *cy, int *cx)
{
    coord_in_dir_n (y, x, dir, cy, cx, 1);
}



/* Auxiliary functions to help compute coordinates relative to the player */

void player_in_dir(int *cy, int *cx)
{
    coord_in_dir (player_y, player_x, player_direction, cy, cx);
}

void player_in_dir_n(int *cy, int *cx, int n)
{
    coord_in_dir_n (player_y, player_x, player_direction, cy, cx, n);
}

void forward (int player_direction)
{
    int y, x;
    coord_in_dir (player_y, player_x, player_direction, &y, &x);

    // in this version there is no screen transition
    // screen edges act as obstaces
    int col = collision(y, x);
    if (col < 0) {
        printf ("You cannot walk outside of the map!\n");
        return;
    } else if (col > 0) {
        /* printf ("You bump into something!\n"); */
        printf ("You bump into something at (%d, %d)!\n", x, y);
        return ;
    }

    if (is_water(y, x)) {
        printf ("You drown!\n");
        level_failed();
    }

    char *mon = is_monster(y, x);
    if (mon) {
        printf ("You walk into a monster %c!\n", *mon);
        receive_damage(mon);
        return ;
    }

    player_x = x;
    player_y = y;
    // we verify here if we walk on an item
    pick_item(y, x);
}

void up(void)
{
    player_direction = DIR_UP;
    forward(player_direction);
}
void down(void)
{
    player_direction = DIR_DOWN;
    forward(player_direction);
}
void left(void)
{
    player_direction = DIR_LEFT;
    forward(player_direction);
}
void right(void)
{
    player_direction = DIR_RIGHT;
    forward(player_direction);
}

void forward_n (int player_direction, unsigned num_steps)
{
    for (unsigned int i=0; i<num_steps; i++)
        forward(player_direction);
}

int collision(int y, int x) {
    map* m = current_map();
    if (x < 0 || x > m->width - 1 || y < 0 || y > m->height -1) {
        /* BUGLINE: off by one
         * if (x < 0 || x > SCREEN_WIDTH || y < 0 || y > SCREEN_HEIGHT) {
         */
        return -1;
    }

    // complex but allows modifiying the possible obstacles quickly. Maybe strlen(OBSTACLES) could be a literal defined in move.h
    /* BUGIDEA:
     * adding a char at the end of OBSTACLES, hence erasing the \0, making 
     * strlen fail...
     */
    if (is_obstacle(y, x)) {
        return 1;
    }

    return 0;
}


void pick_item(int y, int x)
{
    char *it = is_item(y, x);
    if (!it) {
        return;
    }

    if (*it != '$') {
        printf("You found item '%c' but cannot pick it.\n", *it);
        return ;
    }

    gold++;
    printf("You found a gold coin. You now have %d gold.\n", gold);
    /* now remove it */
    *it = ' ';
}


void talk(void)
{
    int y, x;
    /* try talking at distances 1 and 2 in front of player */
    for (int dist = 1; dist<=2; dist++) {
        player_in_dir_n(&y, &x, dist); /* coordinates at distance dist */
        char *mon = is_monster(y, x);
        if (mon) {
            talk_monster(mon);
            return;
        }
    }
    printf("There is no one to talk to here.\n");
}



void fight(equipment eq)
{
    int y, x;
    player_in_dir(&y, &x);

    char *mon = is_monster(y, x);
    if (!mon) {
        printf("There is no monster to fight here.\n");
        return;
    }

    fight_monster(mon, eq);
}
