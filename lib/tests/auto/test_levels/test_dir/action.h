#ifndef ACTION_H
#define ACTION_H

#include "agdbentures.h"


/**
 * pos_x : current x position of the character
 * pos_y : current y position of the character
 * dir : Defines the direction in which the player moves. 
 * Must be in [1..9], where 5 is no movement, 1 is down-left, 9 is top-right... Do you follow?
 * If dir is not in [1..9], it is treated as 5, so no movement occurs.
 */

/* Modify the direction the playing is currently facing. */
void turn_right (void);
void turn_left (void);


/**
 * Computes the coordinates from (x,y) when moving 1 square in direction 'dir'
 * and stores the results in pointers (*cx, *cy).
 */
void coord_in_dir (int y, int x, int dir, int *cy, int *cx);

/**
 * Idem but when moving 'n' squares in direction 'dir'.
 */
void coord_in_dir_n (int y, int x, int dir, int *cy, int *cx, int n);

/**
 * Move the character 1 step forward, on 'n' steps forward.
 */
void forward (int);
void forward_n (int, unsigned int);


void up(void);
void down(void);
void left(void);
void right(void);



/**
 * Verifies if the player can move to tile (x, y).
 * Returns 0  if (x, y) is accessible
 * Returns 1  if (x, y) is no accessible because it's occupied by an obstacle
 * Returns -1 if (x, y) is outside the screen. In that case it's up to the game to decide
 * wether the player will transition to another screen or can't move anymore.
 */
int collision(int x, int y);

/**
 * Verifies if an item is at tile (x, y), and adds it to the inventory if possible.
 * Has no effect if (x, y) doesn't contain an item.
 */
void pick_item(int x, int y);

/**
 * Tries to talk to someone in the direction the player is facing, up to 
 * 2 squares away.
 */
void talk(void);

/**
 * Fight with a monster directly in front of the player, using the player's 
 * equipment (armor & weapon).
 */
void fight(equipment eq);

#endif//ACTION_H
