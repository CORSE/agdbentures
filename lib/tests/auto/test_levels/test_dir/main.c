#include <stdlib.h>

#include "agdbentures.h"

#define CIRCULAR_BUFFER_SIZE 20
#define FOLLOW_DISTANCE \
  5  // The distance at which the pet will follow the character

// Circular buffer definition to store the command history
typedef struct {
  command * internal[CIRCULAR_BUFFER_SIZE];
  int read_index;  // The next command to read
} circular_buffer;

command right_command = {"RIGHT", 5, 0, NULL};
circular_buffer command_buffer;

void buffer_write(circular_buffer * buffer, command * to_write) {
  buffer->internal[(buffer->read_index + FOLLOW_DISTANCE) % CIRCULAR_BUFFER_SIZE] = to_write;
}

command * buffer_read(circular_buffer * buffer) {
  command * command_read = buffer->internal[buffer->read_index % CIRCULAR_BUFFER_SIZE];
  buffer->read_index += 1;
  return command_read;
}

void init_buffer(circular_buffer *buffer) {
  for (int i = 0; i < FOLLOW_DISTANCE; i++) {
    buffer->internal[i] = duplicate_command(&right_command);
  }
  buffer->read_index = 0;
}

void pet_follow_action(void) {
  // reads the next command to do
  command * command_read = buffer_read(&command_buffer);

  // moves the pet accordingly
  printf("%s\n", command_read->command_buffer);
  printf("%d\n", command_buffer.read_index);

  // free the command once used
  // free_command(command_read);
}

int main() {
  init_map_stack();

  // Init map and entities
  map * m = create_empty_map("main", 20, 8);
  place_player(m, 4, 8, UNKNOWN);
  place_entity(m, 4, 3, 0, NPC, NULL, 'P');

  init_buffer(&command_buffer);

  // register pet follow event
  add_event(pet_follow_action);

  command * command;
  while (!exit_main_loop) {
    command = get_next_command();

    apply_input(command);
    // write the command in the buffer for the pet to follow
    buffer_write(&command_buffer, duplicate_command(command));

    if (!exit_main_loop) {
        apply_events();
    }

    free_command(command);
}

  return 0;
}
