import os
import sys

# add parent directory to path
current_level_dir = os.path.dirname(__file__)
project_dir = os.path.dirname(os.path.dirname(current_level_dir))
sys.path.append(os.path.join(project_dir, "lib"))

from level.level import level

filename = os.path.join(current_level_dir, "main.c")
level_obj = level(filename, "text")
