#ifndef COMMON_H
#define COMMON_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <stdbool.h>


/********************/
/* Helping function */
/********************/
#define max(a,b) (a>b?a:b)


/*************************************/
/* Variables referring to the player */
/*************************************/

#define DIR_UP   8
#define DIR_DOWN 2
#define DIR_LEFT 4
#define DIR_RIGHT 6

// x position of the player
extern int player_x;

// y position of the player
extern int player_y;

// the direction the player is facing
extern int player_direction;

// The number of hit/health points
extern int health_points;

// How much gold the player currently has
extern int gold;


/**
 * A structure to hold the equipement of a player.
 */
typedef struct {
    int weapon;
    int armor;
} equipment;


/**
 * A function to call when the level is failed prematurely. E.g. if the player 
 * drowns or its health points drop to zero.
 */
void level_failed(void);

#endif//COMMON_H
