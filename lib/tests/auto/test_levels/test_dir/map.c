#include "map.h"

static map_stack mstack;

void init_map_stack(void)
{
    mstack.length = 0;
}

map* current_map (void)
{
    return &mstack.maps[mstack.length-1];
}


void load_map(char * str_map) {
    map *m;

    /*BUGIDEA : forgetting to save player position */
    if (mstack.length > 0) {
        m = current_map();
        m->player_x = player_x;
        m->player_y = player_y;
    }

    mstack.length++;

    m = current_map();
    m->exit_x = -1;
    m->exit_y = -1;
    m->player_x = -1;
    m->player_y = -1;

    /* initialize the map */
    /* width is the num of characters in the first line */
    char *ch = strchr (str_map, '\n');
    if (! ch) {
        fprintf (stderr, "Error: cannot find line return in map description!");
        exit(EXIT_FAILURE);
    }
    m->width = ch - str_map;

    /* counting the number of '\n' in the map to get the height
     * note that here, ch is already at the first '\n', so the height
     * is at least 1, set the first time entering the loop;
     * */
    m->height = 0;
    do {
        m->height++;
        ch = strchr (ch+1, '\n');
    } while (ch);

    printf ("Size of map: %dx%d\n", m->width, m->height);
    /* allocating enough space to hold the map : an array of strings,
     * each string representing a line in the map */
    m->floor = (char**)malloc(m->height * sizeof(char*));

    /* now making all lines point to existing locations in the whole 'map' 
     * string */
    ch = str_map; /* line 0 starts at the first character, but +1 will be added in the loop */
    for (int line = 0; line<m->height; line++) {
        m->floor[line] = ch ;
        ch = strchr (ch, '\n'); /* search for next line */
        *ch = '\0';             /* just for security    */
        ch++;
    }

    /* finally, search for the start/end positions */
    ch = str_map;
    for (int y=0; y<m->height; y++) {
        for (int x=0; x<m->width; x++) {
            switch (*ch) {
                case '@': m->player_x = x;
                          m->player_y = y;
                          *ch = ' ';       /* replace player with normal floor */
                          break;
                case '>': m->exit_x = x;
                          m->exit_y = y;
                          break;
                case 'D': m->door_x = x;
                          m->door_y = y;
                          break;
 
                default:  ; /* nothing to do otherwise */
            }
            ch++;
        }
        assert (*ch == '\0');
        ch++;
    }

    player_x = m->player_x;
    player_y = m->player_y;
    /* assert (player_x != -1 && player_y != -1); */
}


void pop_from_stack (void)
{
    map *m = current_map();

    free(m->floor);

    mstack.length--;
    m = current_map();

    /* place player at saved position on previous map */
    player_x = m->player_x;
    player_y = m->player_y+1;
}

int coord_idx(int y, int x)
{
    map *m = current_map();
    return y * m->width + x;
}

int player_idx(void)
{
    return coord_idx (player_y, player_x);
}





/**
 * Generic version that checks a position.
 */
char* check_position(const char* chlist, int y, int x)
{
    map *m = current_map();
    char *ch = &(m->floor[y][x]);

    if (strchr(chlist, *ch)) {
        return ch;
    } else {
        return NULL;
    }
}

bool is_obstacle(int y, int x)
{
    return check_position(WALLS, y, x) != NULL;
    /* BUGIDEA: having a == instead of != here */
}


char* is_monster(int y, int x)
{
    return check_position(MONSTERS, y, x);
}


char* is_item(int y, int x)
{
    return check_position(ITEMS, y, x);
}


bool is_water(int y, int x)
{
    return check_position("~", y, x) != NULL;
}

void verify_exit(void)
{
    map *m = current_map();
    if (player_x == m->exit_x && player_y == m->exit_y) {
        printf("VICTOIRE!\n");
        exit (EXIT_SUCCESS);
    } else {
        printf("DEFAITE!\n");
        exit (EXIT_FAILURE);
    }
}

