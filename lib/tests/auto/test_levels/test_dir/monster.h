#ifndef MONSTER_H
#define MONSTER_H

#include "common.h"

/**
 * Talk to a 'monster', described by a char. A pointer to the char is passed.
 */
void talk_monster(char* mon);

/**
 * Fight a monster, described by a char. A pointer to the char is passed.
 */
void fight_monster(char* mon, equipment);

/**
 * Directly receive damage from a monster.
 */
void receive_damage(char* mon);



#endif//MONSTER_H
