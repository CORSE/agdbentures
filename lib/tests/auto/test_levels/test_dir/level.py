#!/usr/bin/env python3

import curses
from easytracker import init_tracker, PauseReasonType
from easytracker.utils import variable_dict_to_struct

DEBUG=0

def debug():
    while tracker.get_pause_reason().type != PauseReasonType.EXITED:
        tracker.resume()

        frame = tracker.get_current_frame(as_raw_python_objects=True)
        variables = tracker.get_global_variables(as_raw_python_objects=True)

        variables = variable_dict_to_struct(variables)

        weapon, armor = get_equipment(frame)
        gold = get_gold(variables)
        map = get_map(variables)
        p_x, p_y = get_player_pos(variables)

        print(f"weapon {weapon} armor {armor} gold {gold}")
        print(f"player {p_x} {p_y}")
        print(map)

        input()

def log(message):
    if DEBUG:
        print(message)

def main(stdscr):
    curses.curs_set(False)

    # Clear screen
    stdscr.clear()

    pad = curses.newpad(100, 100)
    while tracker.get_pause_reason().type != PauseReasonType.EXITED:
        tracker.resume()

        frame = tracker.get_current_frame(as_raw_python_objects=True)
        variables = tracker.get_global_variables(as_raw_python_objects=True)

        variables = variable_dict_to_struct(variables)

        weapon, armor = get_equipment(frame)
        gold = get_gold(variables)
        map = get_map(variables)
        p_x, p_y = get_player_pos(variables)

        #if map is not None:
        #    print_map(map)
        #print(x, y)

        # display map
        y, x = 0, 0
        for c in map:
            if c == '\x00':
                y+=1
                x = 0
            else:
                pad.addch(y,x, str(c))
                x += 1

        # display player
        if p_y > 0 and p_x > 0:
            pad.addch(p_y, p_x, '@')

        # display armor
        pad.addstr(25,0, f"armor {armor}")
        # display weapon
        pad.addstr(26,0, f"weapon {weapon}")
        # display gold
        pad.addstr(27,0, f"gold {gold}")

        # Displays a section of the pad in the middle of the screen.
        # (0,0) : coordinate of upper-left corner of pad area to display.
        # (5,5) : coordinate of upper-left corner of window area to be filled
        #         with pad content.
        # (20, 75) : coordinate of lower-right corner of window area to be
        #          : filled with pad content.
        pad.refresh( 0,0, 5,5, 40,75)

        stdscr.getkey()

        pad.clear()

def get_equipment(frame):
    while frame is not None:
        variables = variable_dict_to_struct(frame.variables)
        try:
            my_eq = variables.eq
        except AttributeError:
            try:
                my_eq = variables.my_eq
            except AttributeError:
                frame = frame.parent
                continue

        # dereference if we have a pointer
        if isinstance(my_eq, tuple):
            my_eq = my_eq[0]
        return (my_eq.weapon, my_eq.armor)
    return (None, None)

def get_gold(glob_vars):
    try:
        return glob_vars.gold
    except:
        return None

def get_player_pos(glob_vars):
    try:
        player_x = glob_vars.player_x
        player_y = glob_vars.player_y
    except:
        return (None, None)

    return (player_x, player_y)

def get_map(glob_vars):
    try:
        maps_struct = glob_vars.mstack
        shop_map = glob_vars.str_shop
        main_map = glob_vars.str_map
    except:
        return None

    lenght = maps_struct.length
    log(f"lenght {lenght} {type(lenght)}")
    log(f"{main_map}")
    log(f"{shop_map}")
    if lenght == 0:
        return None
    #map = maps_struct.maps[lenght-1]

    if lenght == 1:
        return main_map
    if lenght == 2:
        return shop_map
    return None

def print_map(strm):
    #return
    for c in strm:
        if c == '\x00':
            print("")
        else:
            print(str(c), end="")

tracker = init_tracker("GDB")
tracker.send_direct_command("set inferior-tty /dev/pts/13")
tracker.load_program("local_struct")
tracker.start(in_file="in.txt")
# tracker.start("in.txt")
# tracker.start()

tracker.watch("player_x")
tracker.watch("player_y")

if DEBUG:
    debug()
else:
    curses.wrapper(main)
