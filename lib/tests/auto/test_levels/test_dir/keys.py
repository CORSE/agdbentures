#!/usr/bin/env python3

from easytracker import init_tracker, PauseReasonType, UNDEFINED_VARIABLE
from easytracker.utils import unwrap_memory_graph
import subprocess

import sys
import os
sys.path.append(os.path.dirname(os.path.dirname(os.path.dirname(__file__))))

from verif_lib import check_level, VerificationError

player_x=0
player_y=0
key_count=0

prev_cmd=""

def check_level_function(tracker):
    tracker.load_program("keys")
    tracker.start()

    # check that exit_x and exit_y are never modified
    tracker.watch("exit_x")
    tracker.watch("exit_y")

    # check that player_x and player_y only move on by one
    tracker.watch("player_x")
    tracker.watch("player_y")

    g = unwrap_memory_graph(tracker.get_global_variables(), recursive=True)
    last_x, last_y = g.player_x, g.player_y

    pause_reason = tracker.resume()
    while pause_reason.type != PauseReasonType.EXITED:
        g = unwrap_memory_graph(tracker.get_global_variables(), recursive=True)

        if g.exit_x != 27:
            raise VerificationError("exit_x was modified")
        if g.exit_y != 5:
            raise VerificationError("exit_y was modified")

        x, y = g.player_x, g.player_y
        if abs(x - last_x) > 1 or abs(y - last_y) > 1:
            raise VerificationError("player moved to fast")
        last_x, last_y = x, y

        pause_reason = tracker.resume()

#check_level("05_key", check_level_function)

def show_next_line():
    # Update line number
    with subprocess.Popen(
        f"vim --servername tuto --remote-expr "
        f"'HighlightLine({tracker.get_next_lineno()})'",
        shell=True,
        stdout=subprocess.DEVNULL,
    ) as process:
        process.wait()


def wait_user_input(tracker):
    """Waits for user input to continue the tracked program."""
    print(tracker.get_stdout())
    cmd = input("Enter command: > ")
    if cmd == "":
        cmd = wait_user_input.prev_cmd
    if cmd == "next" or cmd == "n" :
        tracker.next()
    elif cmd == "step" or cmd == "s" :
        tracker.step()
    elif cmd == "supercowpowers":
        print("You now have super cow powers!")
        wait_user_input.allow_direct_commands=True
    else:
        print("Does not understand command", cmd)
        if wait_user_input.allow_direct_commands:
            print("Sending it directly to gdb...")
            tracker.send_direct_command(cmd)

    wait_user_input.prev_cmd = cmd
    print("Stop reason:", tracker.get_pause_reason().type)

wait_user_input.prev_cmd = None
wait_user_input.allow_direct_commands = False


# keys_set = False


def getset_globs(g):
    global player_x,player_y,key_count,themap,keys_set
    player_x = g.player_x
    player_y = g.player_y
    key_count= g.key_count

    for k in range(2):
        x=g.key_x[k]
        y=g.key_y[k]

tracker = init_tracker("GDB")
tracker.load_program("05_key")

tracker.start()


map_width = 29
map_height = 9

top_line = ['+'] + ['-']*map_width + ['+']
themap = []

themap.append(top_line)
for h in range(map_height):
    l = ['|'] + [' ']*map_width + ['|']
    themap.append(l)
themap.append(top_line)


def show_map(g):
    print("You have " + str(key_count) + " key(s)")
    g = unwrap_memory_graph(tracker.get_global_variables(), recursive=True)

    to_erase = []

    themap[player_y][player_x] = '@'
    to_erase.append((player_y, player_x))

    for k in range(2):
        x=g.key_x[k]
        y=g.key_y[k]
        if x > 0 and y > 0:
            themap[y][x] = 'k'
            to_erase.append((y,x))

    for l in themap:
        print (''.join(l))

    for (y,x) in to_erase:
        themap[y][x] = ' '


g = unwrap_memory_graph(tracker.get_global_variables(), recursive=True)

for x in range(g.wall_width):
    x+=g.wall_x
    for y in range(map_height):
        y+=1
        themap[y][x] = 'D' if y == g.door_y else '#'
themap[g.exit_y][g.exit_x] = '>'

# tracker.watch('key_count')

while tracker.get_pause_reason().type != PauseReasonType.EXITED:
    variables = unwrap_memory_graph(tracker.get_global_variables(), recursive=True)
    getset_globs(variables)

    show_map(variables)

    print (tracker.get_console_output())
    show_next_line()
    wait_user_input(tracker)
