#include "monster.h"


void talk_monster(char *mon)
{
    assert(mon);
    if (*mon != 'S') {
        printf("You can only talk to Shopkeepers.\n");
        return;
    }
    printf("Welcome to my humble shop traveler.\nDo you want to buy something?\n");
    return;
}

void reduce_health(int dmg)
{
    health_points = max(0, health_points-dmg);
    if (health_points == 0) {
        printf("You die.\n");
        level_failed();
    }
    printf("Your health points are now %d.\n", health_points);
}

void receive_damage(char *mon)
{
    assert(mon);
    if (*mon != 'T') {
        printf("You receive damage from a '%c' but don't know how to do.\n", *mon);
        exit(1);
    }

    int dmg = 4;
    printf("You receive %d damage.\n", dmg); /* BUGIDEA: forgetting the 'dmg' argument */
    reduce_health (dmg);
}

/* returns if monster is defeated, otherwise level_failed will be called */
void fight_monster(char *mon, equipment eq)
{
    int mon_health_points, armor, weapon;
    char *name;
    int dmg;

    assert(mon);
    if (*mon == 'T') {
        mon_health_points = 25;
        armor = 1;
        weapon = 4;
        name = "Troll";
    } else if (*mon == 'S') {
        mon_health_points = 9999;
        armor = 99;
        weapon = 99;
        name = "Shopkeeper";
    } else {
        printf("You want to fight a '%c' but don't know how to do.\n", *mon);
        return;
    }

    /* fight until player or monster dies */
    while (mon_health_points > 0) {
        /* attack */

        dmg = max(0, eq.weapon - armor);
        printf ("You inflict %d damage to the %s.\n", dmg, name);
        mon_health_points -= dmg;
        /* BUGIDEA: forgetting to remove damage to the monster */


        if (mon_health_points > 0) {
            /* retaliates */
            dmg = max(0, weapon - eq.armor);
            printf ("The %s inflicts %d damage to you.\n", name, dmg);
            reduce_health (dmg);
        }
    }
    /* monster dies */
    printf ("The %s dies.\n", name);

    /* remove the monster from the map */
    *mon = ' ';
    /* BUGIDEA: forget to remove monster from the map */
}


