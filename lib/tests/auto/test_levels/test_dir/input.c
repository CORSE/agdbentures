#include "input.h"

command wait_command(void)
{
    char cmd[50];
    if (fgets(cmd, 50, stdin) == NULL) {
        return CMD_EOF;
    }

    cmd[strcspn(cmd, "\n")] = '\0'; /* remove trailing '\n' */

    if (!strcmp(cmd, "")) {
    }
    else if (!strcmp(cmd, "up")) {
        up();
    } else if (!strcmp(cmd, "down")) {
        down();
    } else if (!strcmp(cmd, "right")) {
        right();
    } else if (!strcmp(cmd, "left")) {
        left();
    } else if (!strcmp(cmd, "talk")) {
        talk();
    } else if (!strcmp(cmd, "buy")) {
        return CMD_BUY;
    } else if (!strcmp(cmd, "fight")) {
        return CMD_FIGHT;
    } else {
        fprintf(stderr, "Unknown command '%s'!\n", cmd);
    }
    return CMD_NONE;
}
