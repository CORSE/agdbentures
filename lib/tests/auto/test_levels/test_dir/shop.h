#ifndef SHOP_H
#define SHOP_H

#include "common.h"


/**
 * Different types of weapons
 */
typedef enum {
    NO_WEAPON,
    DAGGER,
    SHORT_SWORD,
    LONG_SWORD,
    WEAPONS_N
} weapon_type;

/**
 * Different types of armor
 */
typedef enum {
    NO_ARMOR,
    LEATHER_JACKET,
    CHAINMAIL,
    FULLPLATE,
    ARMORS_N
} armor_type;


/**
 * Buy the most expensive weapon possible.
 */
weapon_type buy_weapon(void);

/**
 * Buy the most expensive armor possible.
 */
armor_type buy_armor(void);


#endif//SHOP_H
