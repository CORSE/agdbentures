#ifndef INPUT_H
#define INPUT_H

#include <stdio.h>
#include "action.h"

typedef enum {
    CMD_NONE,
    CMD_BUY,
    CMD_FIGHT,
    CMD_EOF /* end of commands */
} command;

command wait_command(void);

#endif//INPUT_H
