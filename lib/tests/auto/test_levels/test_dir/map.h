#ifndef MAP_H
#define MAP_H

#include "agdbentures.h"

/**
 * Possible symbols found on the map
 */
#define WALLS "+|-"   /* Unpassable items, walls and corners */
#define ITEMS "*$[/)" /* gem/rock, gold, armor, wand, weapon */
#define MONSTERS "ST" /* Shopkeeper, Troll */

typedef struct {
  char **floor; /* matrix for the floor: a array of strings of the same size */
  int width;    /* size of map : width of a single line of the above matrix */
  int height;   /*               total number of lines  */
  int player_x; /* position of player */
  int player_y;
  int exit_x;
  int exit_y; /* position of the exit if it exists */
  int door_x;
  int door_y; /* position of the door if it exists */
} map;

/**
 *  Stack of maps: when we enter a map, it is pushed above the previous one.
 *  When returning from a map, we pop the current map and go back to the stored
 *  position.
 */
#define MAX_MAPS 256
typedef struct {
  map maps[MAX_MAPS];
  int length;
} map_stack;

/**
 * Initialize the map stack, to be called before any call to load_map.
 */
void init_map_stack(void);

/**
 * Use 'str_map' as a map for the player, where lines are separated by
 * newlines.
 */
void load_map(char *str_map);

/**
 * Leave current map and return to the previous one.
 */
void pop_from_stack(void);

/**
 * Return the current map, i.e., the map at the top of the map stack.
 */
map *current_map(void);

/**
 * Check if it is possible to move to position (x,y).
 */
bool is_obstacle(int y, int x);

/**
 * Check if there is an item at position (x,y).
 * Returns a pointer to the item if there is one, NULL otherwise.
 */
char *is_item(int y, int x);

/**
 * Similar fonction, but check for a monster.
 */
char *is_monster(int y, int x);

/**
 * Idem, but for water.
 */
bool is_water(int y, int x);

/**
 * Check if player is at the exit position, which is a win condition.
 * Exits the program with return code 0 if position is correct, and
 * 1 otherwise.
 */
void verify_exit(void);

#endif // MAP_H
