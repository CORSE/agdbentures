#include "common.h"

// x position of the character
int player_x = -1;

// y position of the character
int player_y = -1;

// Initial direction: facing East
int player_direction = 6;

// nomber of hit/health points
int health_points = 20;

int gold = 0;

void level_failed(void)
{
    printf("You lose!\n");
    exit(1);
}
