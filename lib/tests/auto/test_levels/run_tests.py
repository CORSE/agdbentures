import os
import subprocess

BUG_MODE = "--defines"
FIX_MODE = "--udefines"

def dirnames(p, n=1):
    if n <= 0:
        return p
    return dirnames(os.path.dirname(p), n-1)

def call_make_level(path, mode):
    subprocess.call([MAKE_LEVEL_SCRIPT, path, "--dest-dir", TEST_DIR, mode, "BUG"])

AGDBENTURE_DIR = dirnames(__file__, 5)
LEVELS_DIR = os.path.join(AGDBENTURE_DIR, "levels")
TEST_DIR = os.path.join(dirnames(__file__), "test_dir")

MAKE_LEVEL_SCRIPT = os.path.join(AGDBENTURE_DIR, "make_level.py")

print(AGDBENTURE_DIR)
print(LEVELS_DIR)
print(TEST_DIR)

# read level list
with open("level_list.txt") as f:
    level_paths = [p.strip("\n") for p in f.readlines()]

# test all levels
for level_path in level_paths:
    src_path = os.path.join(LEVELS_DIR, level_path)
    for mode in [BUG_MODE, FIX_MODE]:
        # apply make level
        call_make_level(src_path, mode)

        # compile level
        subprocess.call(["make", "-C", "test_dir"])

        # run and check output
        # delete the test directory
