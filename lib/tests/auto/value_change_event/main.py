#!/usr/bin/env python3

"""
Value change event test.

We connect a custom function to the eventlist.
This function will log the new values of the coordinates of the
player in a list.
At the end of the script, we check that the values are correct.
"""

from os.path import dirname

from level.level_abc import AbstractLevel
from level.loader import load_level
from level.utils import generate_dict


class Level(AbstractLevel):
    """Callback test"""

    metadata = generate_dict(f"{dirname(__file__)}/test.c")

    def setup(self):
        """Setup to test the value change event."""
        self.event_list.connect("value_change:player_x", event_callback)


# List that stores and the values of the variable that we watch
VALUES = []


def event_callback(new_value: int) -> None:
    """
    Append the new value to the global list
    when a value_change event is received.
    """
    VALUES.append(new_value)


def check() -> None:
    """Check the result."""
    expected = list(range(10))

    # Checking the list
    print(f"Received: {VALUES}, expected {expected}")
    assert VALUES == list(range(10))


if __name__ == "__main__":
    load_level(Level, level_type="text")
    check()
