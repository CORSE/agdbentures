#include <stdio.h>

/*
 * Creates a global variable.
 * And increments it 10 times.
 */

/* @AGDB
 * exec_name: test
 * map_width: 20
 * map_height: 10
 * available_commands: next
 * alias: player_y 5
 * alias: player_direction 3
 * alias: right 3
 * alias: exit_y 5
 * alias: exit_x 9
 */

int player_x = 0;

int main() {
    for (int i = 0; i < 10; ++i)
        player_x = i;

    return 0;
}
