"""Common code used to test the 3 frontend tools."""

from os.path import dirname

from level.level_abc import AbstractLevel
from loader.loader_utils import extract_metadata


class Level(AbstractLevel):
    """Test level."""


    def test(self):
        metadata = extract_metadata(f"{dirname(__file__)}/test.c")
        """dummy"""
