#!/usr/bin/env python3

"""Strip the annoted file."""

from loader.loader_utils import strip_comments

if __name__ == "__main__":
    strip_comments("test.c", "test_clean.c")

# vim: set ft=python ts=4 sts=4 sw=4 et :
