#!/usr/bin/env python3

import pytest
from language import Lang
from config import Config
import tempfile

def init_tmp(speed, debug, language, level, left, win_move, silent):
    tmp = tempfile.NamedTemporaryFile(mode='w+t', dir=tempfile.gettempdir(), prefix="hey", suffix=".yml")
    line = [f"speed: {speed}\n",
            f"debug: {debug}\n",
            f"language: {language}\n",
            f"level: {level}\n",
            f"left: {left}\n",
            f"win_move: {win_move}\n"
            f"silent: {silent}\n"]
    tmp.writelines(line)
    tmp.seek(0)
    Config.read_from_config_file(tmp.name)
    tmp.close()


@pytest.mark.usefixtures("caplog")
def test_config_file(caplog):
    
    # Test valeur vides
    init_tmp("","","","","","", "")
    assert Config.SPEED == 5
    assert Config.DEBUG == 1
    assert Config.LANGUAGE == Lang.EN
    assert Config.WIN_MOVE == False
    assert Config.LEFT == False
    assert Config.LEVEL == None
    assert Config.SILENT == False
    assert 'Ignoring speed value None in config (integer expected)' in caplog.text
    assert 'Ignoring language value None in config' in caplog.text
    assert 'Ignoring debug value None in config (integer expected)' in caplog.text

    # test valeurs fausses
    init_tmp("101","essai","essai","","essai","essai", "hey")
    assert Config.SPEED == 5
    assert Config.DEBUG == 1
    assert Config.LANGUAGE == Lang.EN
    assert Config.WIN_MOVE == False
    assert Config.LEFT == False
    assert Config.LEVEL == None
    assert Config.SILENT == False
    assert 'Ignoring speed value 101 in config (allowed: 0 to 100)' in caplog.text
    assert 'Ignoring language value essai in config' in caplog.text
    assert 'Ignoring debug value essai in config (integer expected)' in caplog.text


    #test valeurs correctes
    init_tmp("99","2","fr","tutorial/00_introduction","True","True", "True")
    assert Config.SPEED == 99
    assert Config.DEBUG == 2
    assert Config.LANGUAGE == Lang.FR
    assert Config.WIN_MOVE == True
    assert Config.LEFT == True
    assert Config.LEVEL == "tutorial/00_introduction"
    assert Config.SILENT == True

    # test fichier non trouvé
    with caplog.at_level('INFO', logger='agdb'):
        tmp = tempfile.NamedTemporaryFile(mode='w+t', dir=tempfile.gettempdir(), prefix="hey", suffix=".yml")
        Config.read_from_config_file(tmp.name+"a")
        assert f'No yaml configuration file found ({tmp.name+"a"}).' in caplog.text

    # test fichier n'a pas une configuration valide

    with caplog.at_level('ERROR', logger='agdb'):
        tmp = tempfile.NamedTemporaryFile(mode='w+t', dir=tempfile.gettempdir(), prefix="hey", suffix=".yml")
        line = [f"speed: \n",
                f"debug: \n",
                f"language: \n",
                f"level: \n",
                f"left: \n",
                f"win_move\n",
                f"silent: \n"] # il manque le ':' à cette ligne d'ou l'erreur de format
        tmp.writelines(line)
        tmp.seek(0)
        Config.read_from_config_file(tmp.name)
        tmp.close()
        assert f'Syntax error in yaml config file {tmp.name}' in caplog.text
