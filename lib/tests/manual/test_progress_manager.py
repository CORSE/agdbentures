import pytest
import os
import progress.progress_manager
import tempfile


def test_write_file():
    # Using a new temporary file to hold the progression
    td = tempfile.gettempdir()
    tf = os.path.join(td,"progress_manager_test.json")

    progress.progress_manager.PROGRESS_FILE = tf

    # blank progression
    pm = progress.progress_manager.ProgressionManager()
    assert os.path.exists(tf)
    pm.reset_progress() # tf may exist and contain old data if the last test failed

    pm.change_time_played("tutorial/00_introduction", 42)
    pm.add_restart("tutorial/01_first_bug")
    pm.set_done("tutorial/00_introduction")
    pm.set_done("tutorial/01_first_bug")
    del pm

    # verify we can read back the same information

    pm = progress.progress_manager.ProgressionManager()

    assert pm.find_level("tutorial/00_introduction").name == "tutorial/00_introduction"
    assert pm.find_category("tutorial").name == "tutorial"
    assert pm.find_level("tutorial/00_introduction").time_played == 42
    assert pm.find_level("tutorial/01_first_bug").restarts == 1
    assert pm.find_level("tutorial/00_introduction").is_done()
    assert not pm.find_level("tutorial/02_refactoring").is_done()
    assert pm.next_level().name == "tutorial/02_refactoring"

    #Check if reset works
    pm.reset_progress()
    pm.write_progression()
    pm.read_progression()

    assert not pm.find_level("tutorial/00_introduction").is_done()
    assert pm.find_level("tutorial/00_introduction").time_played == 0
    assert pm.find_level("tutorial/01_first_bug").restarts == 0

    os.unlink(tf)
