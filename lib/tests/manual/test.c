#include <stdio.h>

/** @AGDB
 *
 * level_name: test_name
 * exec_name: test
 * level_number: 1
 *
 * available_commands: next step edit
 *
 * map_height: 7
 * map_width: 15
 *
 * BUG: Here describe the bug
 * tag: #OffByOne
 *
 * HINT1: first tip
 * HINT2: second tip
 **/

/* @AGDB
 * Block comment test
 */

// @AGDB Inline comment test
/* Do not strip this */ //
// Skip this also
enum direction {
    UP    = 0,
    DOWN  = 1,
    LEFT  = 2,
    RIGHT = 3
};

/** @AGDB
 * Here we define the variables that represents the player
 *
 * alias: player_x player.x
 * alias: player_y player.y
 * alias: player_direction player.d
 */

struct {
	int x, y, d;
} player;

/* @AGDB
 * Map goal
 * tag: #FixedExit
 *
 * WOP: message_condition    player_x == 5 and player_y == 3
 * WOP: visibility_conditions player_x == 5 and player_y == 3
 * WOP: labels some_label
 * WOP: message Hi, stay still. You are half way to the end!
 * WOP: message :-)
 * WOP: position 7 4
 */
int exit_x = 10,
    exit_y = 3;

// @AGDB watch: exit_x

/* @AGDB
 * Creating an useless object
 * OBJ: var_x 2
 * OBJ: var_y 2
 * OBJ: char_rep ~
 */
int main(void) {
	player.x  = 3;
	player.y  = 3;
	player.d  = RIGHT;

    // Forward 10 times
    for (int i = 0; i < 10; ++i)
        player.x += 1;

    // Check
    if (player.x != exit_x || player.y != exit_y)
        return 1;

    return 0;
}
