#include <stdio.h>


/* Do not strip this */ //
// Skip this also
enum direction {
    UP    = 0,
    DOWN  = 1,
    LEFT  = 2,
    RIGHT = 3
};

int player_x  = 3,
    player_y  = 3,
    player_direction = RIGHT;

int exit_x = 10,
    exit_y = 3;

int main(void) {
    // Forward 10 times
    for (int i = 0; i < 10; ++i)
        player_x += 1;

    // Check
    if (player_x != exit_x || player_y != exit_y)
        return 1;

    return 0;
}
