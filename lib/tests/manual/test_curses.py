#!/usr/bin/env python3

""" Basic cursed level test """

import pytest
from test_common import Level
from loader.loader import load_level

@pytest.mark.skip(reason="Not supported at the moment, need to rework the tests")
def test_level():
    game = load_level(Level, level_type="curses")
    game.run()
