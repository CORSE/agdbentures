from enum import Enum

class Lang(Enum):
    default, EN, FR = range(3)

    def __str__(self):
        if self.value == Lang.EN:
            return "en"
        elif self.value == Lang.FR:
            return "fr"
        else:
            return "default"

_language = Lang.default

def set_language(l):
    global _language
    _language = l

_message_data = {
    Lang.default: {},
    Lang.EN: {},
    Lang.FR: {}
}

def _add_message(key, en=None, fr=None):
    if not en:
        en = key
    if not fr:
        fr = key
    _message_data[Lang.default][key] = en
    _message_data[Lang.EN][key] = en
    _message_data[Lang.FR][key] = fr

_add_message('output/messagelabel', 'Message', 'Message')

_add_message('PLAY', fr='Jouer')
_add_message('RESET', 'Reset progress', 'Effacer les données')
_add_message('QUIT', fr='Quitter')

_add_message('REPLAY', 'Replay', 'Rejouer')
_add_message('NEXT_LEVEL', 'Next level', 'Niveau suivant')
_add_message('MENU_RETURN', 'Menu return', 'Retour menu')


_add_message('RETURN_LEVEL', 'Return', 'Retour')
_add_message('PAUSE')
_add_message('RESTART', 'Restart', 'Recommencer')
_add_message('REINIT_LEVEL', "Reinit", "Réinitialiser")
_add_message('HELP', 'Help', 'Aide')
_add_message('EXIT_LEVEL', 'Exit', 'Quitter')

_add_message('VICTORY','VICTORY','VICTOIRE')
_add_message('DEFEAT','DEFEAT','DEFAITE')
_add_message('PROBLEM','PROBLEM','PROBLEME')
_add_message('VALIDATING','Validating level','Validation du niveau')
_add_message('VALIDATION_FAILED','VALIDATION FAILED','ECHEC DE LA VALIDATION')

_add_message('CND_INITIAL','At start of program','Au début du programme')
_add_message('CND_EQUAL','must be equal to','doit être égal à')
_add_message('CND_GENERIC','must be {cnd} to','doit être {cnd} à')

_add_message('CHEATING',
        """
You have completed the level, but seem to have either 
purposefully or inadvertently avoided the actual difficulties.

Please fix the following problems:
""",
        """
Vous avez terminé le niveau, cependant, il semble que vous ayez
contourné les difficultés au lieu de les corriger.

Voici les problèmes à corriger :
""")

_add_message('compiling/loading',"Recompiling and loading the level...","Recompilation et chargement du niveau...")
_add_message('compilation/error',"Compilation error:","Erreur de compilation:")
_add_message('compilation/success',"Compilation successful.","Compilation réussie.")

_add_message('sending/gdb/running',
             "Cannot send gdb command '{cmd}' when program is running.",
             "Ne peut envoyer la commande gdb '{cmd}' quand le programme est en cours d'exécution.",
             )

_add_message('sending/inf/pause',
             "Cannot send command '{cmd}' to program when on pause.",
             "Ne peut envoyer la commande '{cmd}' au programme quand il est en pause.",
             )

_add_message('HELP_TITLE_KEYBOARD_SHORTCUTS',
             "Keyboard shortcuts",
             "Raccourcis clavier",
             )

_add_message('HELP_KEYBOARD_SHORTCUTS', 
             """\
Ctrl+M: Compile level
N: next  C: continue  S: step Ctrl+C: interrupt

Esc: Pause
H: Show/Hide help
Ctrl+R: Restart

Ctrl+I: Load inputs
SPACE: Show all message instantly
Ctrl+V: Trigger validation
Ctrl+F: Enable/Disable filter

T: Take, G: Give, X: Exit, A: Cook  W: Wait
←: Left, →: Right, ↑: Up, ↓: Down""",
             """\
Ctrl+M: Compiler le niveau
N: next  C: continue  S: step  Ctrl+C: Interrompre

Esc: Pause
H: Afficher/Cacher l'aide
Ctrl+R: Recommencer le niveau

Ctrl+I: Charger les saisies
SPACE: Afficher tous les messages instantanément
Ctrl+V: Passer le niveau
Ctrl+F: Activer/Désactiver le filtre

T: Prendre, G: Donner, X: Sortir, A: Cuisiner  W: Attendre
←: Gauche, →: Droite, ↑: Haut, ↓: Bas""",
)

def message(key):
    """
    Look for the asked string (by key, i.e., a string identifier) in the
    language corresponding to the current configuration.
    """
    global _language
    return _message_data[_language][key]

def lmessage(dic):
    """
    Choose correct message in dictionary :param: dic based on current language
    """
    return dic[_language]

