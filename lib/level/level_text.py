"""Text level implementation."""

from easytracker import PauseReasonType
from level.level_abc import AbstractLevel


class LevelText(AbstractLevel):
    """Level using text output."""

    def __init__(
        self,
        metadata,
        level_input_queue,
        level_output_queue,
    ):
        super().__init__(
            metadata, level_input_queue, level_output_queue
        )
        self.custom_setup()

    def show_map(self) -> None:
        """Print a ascii map."""
        # Fetching the map
        ascii_map = self.map.get_ascii_map()

        # Adding the WOP
        for wop_x, wop_y in self.available_wop():
            ascii_map[wop_y][wop_x] = "W"

        # Printing the middle lines
        for line in ascii_map:
            print("".join(line))

    def print_wop_msg(self) -> None:
        """Print the WOP message."""
        for wop_id, message_list in self.get_wop_message().items():
            wop_message = "\n".join(message_list)
            print(f"WOP{wop_id} says:\n{wop_message}")

    def print_stdout(self) -> None:
        """Prints the inferior stdout."""
        stdout = self.tracker.get_inferior_stdout()
        if stdout:
            print(f"stdout:\n{stdout}")

    def run(self) -> None:
        """Run in a text interface."""
        self.prepare_tracker()
        pause_type = self.tracker.pause_reason.type

        while pause_type != PauseReasonType.EXITED:
            # Fetch variables
            self.update_state()

            # Print inferior's output
            self.print_stdout()

            # Update the code visualizator
            self.show_next_line()

            # Show the map
            self.show_map()
            self.print_wop_msg()

            # Read a command and run it
            cmd = self.read_cmd()
            self.parse_and_eval(cmd)
            pause_type = self.tracker.pause_reason.type

        self.print_stdout()

        code = self.tracker.exit_code
        print(f"Inferior exited with {code} as exit code.")
