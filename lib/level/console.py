from abc import ABC, abstractmethod
import queue
import threading
import sys
import datetime
from os.path import join
import select
import atexit
import code
import readline


from config import Config

CONSOLE_LOG = 0

PROMPT = "(gdb) "

def console_log(message, prefix):
    if CONSOLE_LOG:
        with open(".console_logs.txt", "a") as f:
            now = datetime.datetime.now()
            print(f"{str(now)} {prefix} {message.rstrip()}", file=f)


class HistoryConsole(code.InteractiveConsole):
    def __init__(self, locals=None, filename="<console>",
                 histfile=join(Config.ROOT_DIR, ".console-history")):
        code.InteractiveConsole.__init__(self, locals, filename)
        self.init_history(histfile)

    def init_history(self, histfile):
        readline.parse_and_bind("tab: complete")
        if hasattr(readline, "read_history_file"):
            try:
                readline.read_history_file(histfile)
            except FileNotFoundError:
                pass
            atexit.register(self.save_history, histfile)

    def save_history(self, histfile):
        readline.set_history_length(1000)
        readline.write_history_file(histfile)

    def write(self, data):
        """Write a string.
        The base implementation writes to sys.stderr; a subclass may
        replace this with a different implementation.
        We choose stdout here, and add a fush.
        """
        sys.stdout.write(data)
        sys.stdout.flush()

class ConsoleInputThread(threading.Thread):
    def __init__(self, input_queue: queue.Queue):
        super().__init__(name="Console input thread", daemon=True)
        self.level = None
        self.keep_going = True
        self.input_queue = input_queue
        self.current_line: int|None = None
        self.prompt_condition = threading.Condition()

        # To remember if we have set the prompt
        self.waiting_for_input = False
        self.hc = HistoryConsole()

    def set_level(self, level):
        self.level = level

    def terminate(self):
        self.keep_going = False

    def trigger_prompt(self):
        if self.waiting_for_input:
            self.hc.write(PROMPT)
        else:
            with self.prompt_condition:
                self.prompt_condition.notify()


    def run(self):
        while self.keep_going:
            try:
                # verify if there is something to read on stdin
                # required to exit gracefully when main window is closed for 
                # instance (via the `quit' button)
                # => seems not useful anymore
                # if select.select([sys.stdin, ], [], [], 5.0)[0]:

                with self.prompt_condition:
                    self.prompt_condition.wait()

                self.waiting_for_input = True
                line = self.hc.raw_input(PROMPT)
                self.waiting_for_input = False

                console_log(line, " console <<<")
                # else:
                    # continue
            except EOFError:
                if self.level.tracker and self.level.tracker.is_running():
                    print("Not sending EOF to inferior")
                    # sending eof to inferior
                    # self.level.tracker.close_inferior_stdin()
                    continue
                    # line = "" # <-- does NOT work
                else:
                    # quitting gdb (and current level)
                    line = "quit"
            if self.keep_going:
                if not self.level:
                    print(
                        "No level started, use the GUI to start a level before using the console."
                    )
                    continue

                if self.level.tracker and self.level.tracker.is_running():
                    # inferior is running and possibly expecting input
                    self.level.tracker.send_to_inferior_stdin(line)
                else:
                    # inferior is stopped, we execute gdb commands
                    self.input_queue.put_nowait({"type": "gdb", "command": line})


class AbstractConsolePrint(ABC):
    """
    Abstract console print class that emulates the behaviour of a gdb output console.
    """

    @abstractmethod
    def send_error(self, smg):
        """
        Show error on console.
        """

    @abstractmethod
    def show_gdb_command(self, cmd):
        """
        Show gdb command on console.
        """

    @abstractmethod
    def send_command(self, cmd):
        """
        Send command to the console to modify it's behavior
        (e.g., stop/resume printing gdb commands).
        """

    @abstractmethod
    def set_finish_true(self):
        """TODO"""

    @abstractmethod
    def set_finish_false(self):
        """TODO"""



class ConsolePrintDummy(AbstractConsolePrint):
    """
    Dummy class that mimick the behaviour of ConsolePrintThread
    but actually does nothing.
    Used for unit testing.
    """

    def show_gdb_command(self, cmd):
        pass

    def send_error(self, smg):
        pass

    def send_command(self, cmd):
        pass

    def set_finish_true(self):
        pass

    def set_finish_false(self):
        pass


class ConsolePrintThread(AbstractConsolePrint, threading.Thread):
    """
    Thread to print to console gdb messages and inferior stdout
    """

    def __init__(self, input_thread, list_break):  # easytracker_output_queue: queue.Queue):
        """
        :param input_thread: the input thread to communicate prompt triggers
        :param list_break: ListBreakpoints class to access internal breakpoints and hide them

        """
        threading.Thread.__init__(self, name="Console print thread", daemon=True)
        self.input_thread = input_thread
        self.output_queue = None
        self.do_print_output = True
        self.list_break = list_break
        # set ListBreakpoints class to access break placed by us
        # use in level_abc when we send finish to gdb
        self.our_finish : bool = False
        self.we_delete_all = False

    def set_easytracker_output_queue(self, qu: queue.Queue):
        """
        Connect print thread to easytracker output queue, to read stdout.
        """
        self.output_queue = qu

    def pause_printing(self):
        self.do_print_output = False

    def resume_printing(self):
        self.do_print_output = True

    def print_message(self, color, logstr, message):
        if not Config.ARCADE_HEADLESS:
            print(f"{color}{message}\033[00m", end='')  # closing]
            console_log(message, logstr)
            sys.stdout.flush()

    def send_command(self, cmd):
        # used by level to send command to the print thread
        # we use gdb output queue to send command to ourself
        self.output_queue.put_nowait(('command', cmd))

    def show_gdb_command(self, cmd):
        # used by level to display a command sent to gdb by the GUI
        # we use gdb output queue to send command to ourself
        self.output_queue.put_nowait(('gdb_cmd', cmd))

    def show_inferior_stdin(self, cmd):
        # used by level to display a string sent to the inferior stdin by the GUI
        # we use gdb output queue to send command to ourself
        self.output_queue.put_nowait(('inf_stdin', cmd))

    def send_error(self, msg):
        # we use gdb output queue to put an error message in console
        self.output_queue.put_nowait(('ERROR', msg))

    def reinitialisation(self) -> None:
        """
        We execute this code each time level is start or restart.
        We use this in level_arcade on restart function
        """
        self.init_done = False
        self.now = False
        self.nb_restart += 1
        self.our_finish = False
        if self.nb_restart == 1:
            self.nb_restart += 1

    def set_finish_true(self) -> None:
        """
        This method is call in level_abc because it's the only file
        where we can send finish to gdb
        Change the value of finish attribut in the console
        value of our_finish means :
        True -> We send "finish" in console,
            we don't display message
        False -> Player send "finish" in console,
            he can see all gdb say's
        """
        self.our_finish = True

    def set_finish_false(self) -> None:
        """
        This method is call in level_abc because it's the only file
        where we can send finish to gdb

        Change the value of finish attribut in the console
        value of our_finish means :
        True -> We send "finish" in console,
            we don't display message
        False -> Player send "finish" in console,
            he can see all gdb say's
        """
        self.our_finish = False

    def run(self):
        self.nb_restart = -1
        self.reinitialisation()
        while True:
            output = self.output_queue.get()
            if output is None:  # stdout of inferior is closed
                break
            otype, message = output
            logstr = '(unknown)'

            # will now print differently depending on the type of output

            if otype == 'command':
                # change behaviour of printing thread
                if message == 'pause':
                    self.pause_printing()
                    continue

                elif message == 'resume':
                    self.resume_printing()
                    continue

                elif message == 'last-and-resume':
                    # resume from pause, printing the last message (that was 
                    # not printed)
                    self.print_message(last_color, last_logstr, last_message)
                    last_color = None
                    last_color = None
                    self.resume_printing()
                    continue

                elif message == 'prompt':
                    self.input_thread.trigger_prompt()
                    continue
                    # message = PROMPT
                    # logstr = 'prompt'

                else:
                    print(f"ERROR: console printing command {message}")

            elif otype == 'gdb_cmd':
                color = "\033[93m"  # yellow    closing]
                logstr = ">>>"
            elif otype == 'inf_stdin':
                color = "\033[00m"  # white (reset)  closing]
                logstr = ">>>"
            elif otype == 'ERROR':
                color = "\033[91m"  # red     closing]
                logstr = "ERROR"
            elif otype == 'GDB':
                color = "\033[95m"  # violet  closing]
                logstr = "GDB"
            elif otype == 'stdout':
                color = "\033[92m"  # green   closing]
                logstr = "<<<"
            else:
                print("WARNING: unknown output source")
                color = "\033[91m"  # red     closing]


            if self.do_print_output:

                if self.now:
                    # we use this because breakpoint placed by us
                    # appear in the dictionary at this moment in the execution
                    # of the program, just before player can do something
                    self.now = False
                    self.init_done = True
                    self.list_break.create_real_list_break(self.nb_restart)

                # all initialisation where we respond and player can't are skipped
                if "answered Y" in message and not "Temporary breakpoint" in message:
                    continue

                # we display initialisation if it's not a question from gdb
                if not self.init_done:
                    self.print_message(color, logstr, message)

                # now we analyze message because maybe the output is not due to player
                else:
                    message = self.analyze_message(message)
                    if message != "":
                        self.print_message(color, logstr, message)

                # We are on main breakpoint, so in the beginning of the program
                if ("Temporary breakpoint" in message and not self.init_done ):
                    # at this point easytracker will have all breakpoint
                    # placed by us in the level.py of current level
                    # in the next message, so we store this information
                    self.now = True


            else:
                last_color = color
                last_logstr = logstr
                last_message = message

            # TODO: protect the output (escape \n for instance)
            # 'repr' does not work as it also protects the \t at beginning and
            # final \n...
            # => probably need to do that from easytracker
            # message = repr(output[1])
            # print(f"Message from inferior: {message.encode()}")



    def analyze_info_break(self, message : str) -> str:
        """
        This function see what gdb produce when user wrote :
        info [break or watch ]
        and analyze the result in order to remove all informations
        weird for player like breakpoints placed by us
        Args:
            message (str): The original message from gdb
        Returns:
             str: A string of what we really want to print in the console
        """
        tmp_message = message.split("\n")

        # catch if info break or not
        if tmp_message[1].split()[1] != "breakpoint":
            # we only use break by default in agdbentures so we can return the real message
            return message


        # We are on the first line with name of different columns
        message = tmp_message[0] + '\n'

        flag = False
        # last line is just " " we don't want this
        for line in tmp_message[1:-1]:

            # id of a break
            if '0' <= line[0] <= '9':
                num = int(line.split()[0])
                if self.list_break.not_our_breakpoint(num):
                    flag = True
                    message += line + "\n"

            # If program already passed
            # on a breakpoint then there's more line for
            # this breakpoint with more information
            # like how many times we hit this line
            else:
                if flag:
                    message += line + "\n"
                    flag = False

        # there's no breakpoints from user
        if len(message.split("\n")) == 2:
            message = f"No breakpoints.\n"

        return message


    def analyze_finish(self, message : str) -> str:
        """
        This function see what gdb produce when wrote :
        fin or finish
        because player and us can send this command to gdb

        if we sent it then we only display to player the line in the code
        he expected to see (the next one of the last were print in console)

        else we send all gdb message

        Args:
            message (str): The original message from gdb
        Returns:
             str: A string of what we really want to print in the console
        """

        # This attribut is set in level_abc, because it's the only file
        # where we sent fin to gdb
        if self.our_finish:
            try:
                # if it's the last fin to return to current line
                # then we display the current line to player
                line_number = int(message.split("\n")[-2].split()[0])
                if line_number == self.current_line + 1:
                    return message.split("\n")[-2] + '\n'
            except:
                pass
            return ""
        return message

    def analyze_breakpoint(self, message : str) -> str:
        """
        This function see what gdb produce when a Breakpoint is hit
        If breakpoint is one of ours we don't display information
        If it's a player's one then we show tho him all message
        Args:
            message (str): The original message from gdb
        Returns:
             str: A string of what we really want to print in the console
        """

        # Usual format of gdb place the name of the function
        # at this position when there's a breakpoint
        good_index_id = message.split().index("Breakpoint")

        # number of breakpoint is placed after the word "Breakpoint" in gdb
        # sentence but there's a ',' without space, so we remove this
        # character in order to convert to int
        id_func = int(message.split()[good_index_id + 1][:-1])

        if self.list_break.not_our_breakpoint(id_func):
            return message

        # we don't print the message because it's not
        # player's breakpoint
        return ""

    def analyze_delete(self, message: str) -> str:
        """
        @param: message = the original message from gdb we need to display to the console
        @return : str = if message contain information not pertinent for the player we remove them.
        We try to delete all number if we send "d" command, so we hide to player all message saying "no breakpoint"
        """
        if self.list_break.we_delete_all:
            # if we are on the last breakpoint then we pass in normal mode because command send by
            # level_abc to delete all is done
            self.list_break.delete_finish(int(message.split()[3][:-1]))
            return ""
        return message


    def analyze_message(self, message : str) -> str:
        """
        @param: message = the original message from gdb we need to display to the console
        @return : str = if message contain information not pertinent for the player we remove them.
        We keep only break from the player in the display and hide to him break we placed for the level.
        """
        # display a Breakpoint during execution
        if "Breakpoint" in message:
            return self.analyze_breakpoint(message)

        elif "No breakpoint number" in message:
            return self.analyze_delete(message)

        # if user wrote : info break or info watch
        elif "Num     Type" in message:
            return self.analyze_info_break(message)

        # if "fin" or "finish" is sent to gdb to leave a function
        elif "Run till exit from " in message:
            return self.analyze_finish(message)

        # basic case maybe add some command before this default
        else:
            try:
                # current line is used when we do a finish
                self.current_line = int(message.split()[0])
            except:
                pass
            return message
