"""Arcade level implementation."""
import sys
from threading import Thread, Event
import threading
import time

import arcade

from easytracker import PauseReasonType

from config import Config
from graphic.constants import Direction
from lib.graphic.views.game import GameView
from progress.progress_manager import progress
from level.level_abc import AbstractLevel
from logs import lvl
from level.action import Action
from level.objects import Object, Coordinates
from level.console import ConsolePrintThread
from level.list_breakpoints import ListBreakpoints
import tests.lib_test as T
from tests.testGUI import Reason, EventHandler, TestThread
from tracker_helper import TrackerHelp


# pylint: disable=fixme
class LevelArcade(AbstractLevel, Thread):
    """
    Level handling when there is an arcade thread running.

    param: sp_objects: a sprite list representing objects
    param: sp_coords: a list representing map locations
    """
    prompt_ask = False
    def __init__(
        self,
        metadata: dict,
        code_window,
        level_input_queue,
        level_output_queue,
        load_map_hook,
        input_thread,  # to be able to pass it to the console print thread
        # sp_objects=[],
        # sp_coords=[],
        window=None
    ):
        # Launch a thread that will handle printing in the console
        # (commands, stdout from inferior, etc.)
        # Only for arcade level, as e.g. LevelTest need to directly handle gdb and inferior output
        # TODO: use only one thread for all agdbenture instead of restarting 
        # one for each level
        self.list_break = ListBreakpoints()

        self.input_thread = input_thread
        print_thread = ConsolePrintThread(
            self.input_thread,
            self.list_break,
        )

        self.print_thread = print_thread

        AbstractLevel.__init__(
            self,
            metadata,
            level_input_queue,
            level_output_queue,
            code_window=code_window,
            print_console=print_thread,
            load_map_hook=load_map_hook,
            window=None
        )

        assert self.tracker
        print_thread.set_easytracker_output_queue(self.tracker.stdout_queue)
        self.list_break.set_list_all_breakpoint(self.our_breakpoint)
        print_thread.start()

        Thread.__init__(self, name=f"Arcade level thread {metadata['level_title']}", daemon=True)

        # Launch a thread that will handle printing in the console
        # (commands, stdout from inferior, etc.)
        # Only for arcade level, as e.g. LevelTest need to directly handle gdb and inferior output
        self.test_thread = None

        self.window = window


    def send_to_gui(self, payload):
        self.send_out_queue(payload)

    def send_topic_to_gui(self, topic):
        out_payload = {'topic': topic}
        self.send_to_gui(out_payload)

    def send_cmd_to_gui(self, cmd):
        out_payload = {'topic': 'cmd', 'cmd': cmd}
        self.send_to_gui(out_payload)

    def execute_agdbentures_control(self, payload):
        # the payload can be more elaborated in the future
        cmd = payload['command']

        if cmd == "exit":
            self.terminate_tracker()
            return 'exit'

        if cmd == "restart":
            return 'restart'

        if cmd == "validate":
            return 'validate'

        # we start interactive mode if requested
        if cmd == 'interactive_mode':
            self.interactive_mode = True
            self.interactive_bp = self.tracker.break_before_func("getline")
            self.send_console_command("run")

        elif cmd == 'stop_interactive_mode':
            if self.interactive_mode:
                self.interactive_mode = False
                self.tracker.delete_breakpoint(self.interactive_bp)

        elif cmd.startswith("magic_breakpoint"):
            _, x, y = cmd.split()
            self.add_magic_breakpoint(x,y)

    def execute_gdb_command(self, payload):
        cmd = payload["command"]

        ret = self.parse_and_eval(cmd)
        match ret:
            case 'restart':
                # gdb command was 'start', we catch it and do not send to GUI
                return ret
            case 'gdb_exit':
                return None
            case None:
                pass
            case _:
                raise ValueError(f"parse_and_eval return unknown string {ret}")

        out_payload = {"topic": "cmd", "cmd": cmd}
        self.send_to_gui(out_payload)

        # we do mode-specific commands
        if self.interactive_mode:
            self.send_console_command("continue")
        # after command evaluation we update the level internal state

        self.print_prompt()

        if self.tracker.pause_reason.type == PauseReasonType.EXITED:
            # we are already out of the program and cannot update anymore
            return

        self.update_all()




    def catch_message(self):
        assert self.tracker.get_current_function_name() == "message"
        message = self.tracker.get_variable_value_as_str('msg', "char*")
        lvl.debug(f"Got message to display: {message.encode()}")

        self.update_state()  # show the current state
        payload = {"topic": "show_message", "message": message}
        self.send_to_gui(payload)

        lvl.debug(f"Message catched: {message}")

    def catch_exit(self):
        lvl.debug(f"Exit funtion catched!")
        self.update_state()

    def catch_prompt(self):
        assert self.tracker.get_current_function_name() == "prompt"
        self.prompt_ask = True
        message = self.tracker.get_variable_value_as_str('msg', "char*")
        lvl.debug(f"Got message to display for prompt: {message.encode()}")
 
        # frame = self.tracker.get_program_memory(as_raw_python_objects=True)["stack"][-1]
        # lvl.debug(f"current frame: {frame}")
        # assert frame.name == "prompt"

        # message = frame.variables["msg"].value[:-1]  # char list
        # convert to string
        # message = "".join(message)

        self.update_state()  # show the current state

        payload = {"topic": "ask_input", "prompt": message}
        self.send_to_gui(payload)

    def catch_get_next_command(self):
        self.update_state()  # show the current state so player knows what to do


    def pre_first_start(self):
        """
        Called just before the first 'start' is sent to gdb.
        """
        super().pre_first_start()

    def pre_restart(self):
        """
        Called just before the subseQuent 'start' are sent to gdb.
        """
        super().pre_restart()
        self.print_thread.reinitialisation()
        self.list_break.restart()
        self.send_topic_to_gui('pre_restart')

    def post_first_start(self):
        """
        Called just after the first 'start' is sent to gdb.
        """
        super().post_first_start()
        self.send_topic_to_gui('first_start')
        self.payload_before_end = []
        self.custom_first_start()
        if (Config.ARCADE_HEADLESS):
            thread = TestThread(target=self.testGUI)
            thread.start()
            EventHandler.setTestThread(thread)
            EventHandler.notify(Reason.STARTING_TEST_THREAD)


    def post_restart(self):
        """
        Called just after the subsecquent 'start' are sent to gdb.
        """
        super().post_restart()
        self.payload_before_end = []
        self.send_topic_to_gui('post_restart')
        self.custom_restart()
        EventHandler.notify(Reason.POST_RESTART)

    def restart(self):
        """
        Redefine this method in a level to set special code to be executed
        after the first start for this level.
        """
        pass

    def testGUI(self):
        """
        Redefine this method in a level to set special code to be executed
        as a test in headless mode for this level.
        """
        pass

    def arcade_custom_restart(self):
        """
        Redefine this method in a level to set special code to be executed each
        time the program is *restarted* for this level (i.e., all 'start' but
        for the first one).
        """
        super().arcade_custom_restart()



    def update_time_played(self, start_time_level):
        time_played = time.time() - start_time_level
        try:
            progress.change_time_played(self.level_name, int(time_played))
        except IndexError:
            lvl.error("Error while trying to save time. Time not saved.")

    def update_number_restarts(self):
        try:
            progress.add_restart(self.level_name)
        except IndexError:
            lvl.error("Error while trying to add a restart. Not added.")



    def in_level_loop(self):
        """
        Logic to apply when level is running, i.e., while inferior is executed by gdb.
        """
        # We do not initialize to self.tracker.pause_reason as it would be
        # a BREAKPOINT (temporary breakpoint at start of program)
        pause_type = PauseReasonType.START

        self.print_prompt()
        lvl.debug("Starting main debug loop")

        exit_code = None

        do_restart_level = False
        do_exit_level = False

        while pause_type is not PauseReasonType.EXITED: #, PauseReasonType.SIGNAL]:
            lvl.debug(f"Inferior paused for reason {self.tracker.pause_reason}")

            if pause_type == PauseReasonType.SIGNAL:
                if 'SIGSEGV' in self.tracker.pause_reason.args:
                    exit_code = 139
                    lvl.info(f"Inferior got a segmentation fault")
                    self.send_print_thread_error("SEGMENTATION FAULT\n")
                    # TODO: better error message: notify GUI
                    break
                if 'SIGINT' in self.tracker.pause_reason.args:
                    # regular interruption
                    lvl.info(f"Inferior interrupted")
                else:
                    lvl.error(f"Inferior received an unknown signal")
                    self.send_print_thread_error(f"SIGNAL ERROR {self.tracker.pause_reason}\n")
                    exit_code = 255
                    break

            payload = self.in_queue.get()
            lvl.debug(f"Payload received: {payload}")

            if payload is None:
                # if the payload is None we terminate the level
                assert False, "Should not use None to terminate tracker anymore"
                # Now, a payload of type 'control' and command 'exit' must 
                # be sent

            payload_type = payload["type"]
            if payload_type == "control":
                ret = self.execute_agdbentures_control(payload)
            elif payload_type == "gdb":
                ret = self.execute_gdb_command(payload)
            else:
                lvl.error(f"Unknown payload type {payload_type} in {payload}")

            if ret == 'exit':
                do_exit_level = True
                exit_code = -1
                break
            elif ret == 'restart':
                do_restart_level = True
                exit_code = -1
                break
            elif ret == 'validate': # early launch validation, pretend inferior exited successfully
                exit_code = 0
                break



            # We do not allow asking gdb to quit directly
            # => must exit the level if we want to do so
            assert ret != 'gdb_exit' # gdb was asked to quit
                # exit_code = 1
                # break


            pause_type = self.tracker.pause_reason.type
            # do not notify the test thread if the level is finished;
            # it will be notified whe nthe end screen has been loaded
            if (pause_type is not PauseReasonType.EXITED):
                EventHandler.notify(Reason.COMMAND_EXECUTED)

        if exit_code is None:
            assert pause_type is PauseReasonType.EXITED, f"Wrong pause reason {pause_type}"
            exit_code = self.tracker.exit_code
        return do_exit_level, do_restart_level, exit_code

    def set_dependencies(self, payload):
        self.payload_before_end.append(payload)

    def get_dependencies(self):
        return self.payload_before_end

    def run(self):
        """
        This method is called when the level thread is started.
        It contains all the interaction with gdb on one side, and the GUI on 
        the other side.
        """

        # Register some breakpoints on particular functions
        self.register_breakpoint(function_name="exit", callback=self.catch_exit)
        self.register_breakpoint(function_name="message", callback=self.catch_message)
        self.register_breakpoint(function_name="prompt", callback=self.catch_prompt)
        self.register_breakpoint(
            function_name="get_next_command", callback=self.catch_get_next_command
        )

        while True:
            # Check if the window view is on game_view, can happen if we are on 
            # the exit screen and start is typed in the console instead of 
            # clicking the 'replay' button
            self.send_out_queue({
                'topic': "gui_change",
                'action': "show_game",
            })


            do_restart_level = False

            self.do_start()
            start_time_level = time.time()

            # The main loop playing the level
            do_exit_level, do_restart_level, exit_code = self.in_level_loop()
            self.update_time_played(start_time_level)

            if do_exit_level:
                lvl.debug(f"Exiting level thread {self}")
                return

            if do_restart_level:
                # inferior has not exited, user has asked to restart
                # the level, either by using the GUI or by typing 'start'
                # in the console
                self.update_number_restarts()
                continue


            # If we arrive there, the inferior has exited, but maybe also the 
            # tracker!
            # Need to know if the level is validated or not

            lvl.info(f"Inferior exited with {exit_code} as exit code.")
            self.inferior_started = False

            assert exit_code >= 0
            dependencies = self.get_dependencies()
            if exit_code != 0:
                # failed
                payload = {'topic': "inferior_exit", 'action': Action('failed',dependencies)}
            else:
                # level says it's ok but we still need to validate it
                # send first a message saying we are trying to validate the level
                payload = {'topic': "inferior_exit", 'action': Action('validating',dependencies)}
                self.send_to_gui(payload)

                # Now run validation code
                val,reasons = self.validate_level()

                if val:
                    payload = {'topic': "inferior_exit", 'action': Action('success')}
                    self.unlock_next_levels()
                else:
                    payload = {'topic': "inferior_exit", 'action': Action('val_failed'),
                               'reasons': reasons}

            self.send_to_gui(payload)


            # self.end_level_loop() => TODO move rest into its own method?
            # Waiting loop to know whether we restart the level or terminate it
            while True:
                # print("In END OF LEVEL LOOP")
                payload = self.in_queue.get()
                if payload is None:
                    self.terminate_tracker()
                    return
                payload_type = payload["type"]

                # Two ways of restarting, either via the 'start' in the console
                # or by being notified by the GUI
                if payload_type == "gdb":
                    if payload["command"] == "start":
                        # get out of this loop, and back to the "restart"
                        # command above
                        break
                    print(
                        "Program has ended. Click 'Replay' in the interface to restart it, or type 'start' here."
                    )
                    lvl.debug(f"Getting payload in thread {self}")

                    # TODO: allow some commands?
                    # if self.tracker:
                        # ret = self.execute_gdb_command(payload)
                        # print(f"Return value: {ret}")

                elif payload_type == 'control':
                    if payload["command"] == "exit":
                        lvl.debug(f"Exiting from waiting loop after level run in {self}")
                        return

                    if payload['command'] == 'restart':
                        self.update_number_restarts()
                        break
                    lvl.debug(f"Control payload received while level is finished: {payload}")
                else:
                    lvl.debug(f"Payload received while level is finished: {payload}")
                self.print_prompt()


    def get_named_object_position(self, object_name:str) -> tuple:
        """
        Get the (x,y) position of a named object
        """
        obj = self.world.get_outside_object(object_name)
        return (obj.coord_x, obj.coord_y)

    def wait_gui_changed(self):
        """
        Wait for the trigger of a GUI chnage payload
        """
        EventHandler.addReason(Reason.GUI_CHANGE)
        EventHandler.wait(3)



    def verify_named_object_position(self, object_name:str, x:int=None, y:int=None, direction:Direction=None, shouldNotBeHere:bool=False) -> None:
        """
        A method to run test on a named object position and direction
        """
        obj = None
        if object_name == "player":
            obj = self.world.player
        elif object_name in self.world.outside_objects:
            obj = self.world.get_outside_object(object_name)
        elif object_name in self.world.get_objects():
            obj = self.world.get_object(object_name)
        else:
            T.error(f"Impossible to locate {object_name} in the list of named objects")
            return
        valid_x = x is None or (not shouldNotBeHere and x == obj.coord_x) or (shouldNotBeHere and x != obj.coord_x)
        valid_y = y is None or (not shouldNotBeHere and y == obj.coord_y) or (shouldNotBeHere and y != obj.coord_y)
        valid_direction = direction is None or direction == obj.direction
        message_end = "but shouldn't" if shouldNotBeHere else f"instead of "
        if not valid_x:
            T.error(f"{object_name} x coordinate in GUI: {obj.coord_x}"+ message_end + "" if shouldNotBeHere else {x})
        if not valid_y:
            T.error(f"{object_name} y coordinate in GUI: {obj.coord_y}"+ message_end + "" if shouldNotBeHere else {y})
        if not valid_direction:
            T.error(f"{object_name} direction in GUI: {obj.direction} instead of {direction}")
        if valid_direction and valid_x and valid_y:
            T.passed(f"Valid {object_name} position and direction in the GUI")

    def verify_player_position(self, x:int=None, y:int=None, direction:Direction=None):
        """
        A method to run test on the player position.
        The argument are optionnal, and considerate as always true if not specified
        """
        self.verify_named_object_position("player", x, y, direction)

    def verify_player_is_in_camera_range(self):
        """
        Test if the player is inside the camera
        """
        # TODO fix it : the camera isn"t load when the test script start
        return
        gw: GameView = self.window.views["game"]
        if gw.is_inside_camera(gw.player):
            T.passed(f"The player is visible in the camera, as excepted")
        else:
            T.error("The player isn't visible in the camera")

    def click_next(self, mustEndLevel=False):
        """
        Simulate a click on the next button in the GUI, and wait until the element is threated
        """

        if mustEndLevel:
            EventHandler.addReason(Reason.VIEW_END_GAME)
        else:
            EventHandler.addReason(Reason.COMMAND_EXECUTED)
        gw: GameView = self.window.views["game"]
        gw.next_button.dispatch_event("on_click", arcade.gui.UIOnClickEvent)
        # Wait for the end of the next routine before continuing test script
        if not EventHandler.wait(5):
            T.error("Timeout on a click next")

    def click_restart(self):
        """
        Simulate a click on the next button in the GUI, and wait until the element is threated
        """
        EventHandler.addReason(Reason.POST_RESTART)
        gw: GameView = self.window.views["game"]
        gw.start_button.dispatch_event("on_click", arcade.gui.UIOnClickEvent)
        # Wait for the end of the next routine before continuing test script
        if not EventHandler.wait(10):
            T.error("Timeout on a click restart")

    def click_continue(self, mustEndLevel=False):
        """
        Simulate a click on the continue button in the GUI, and wait until the element is threated
        """
        # Force to consume the flag
        # Force to consume the flag
        if mustEndLevel:
            EventHandler.addReason(Reason.VIEW_END_GAME)
        else:
            EventHandler.addReason(Reason.COMMAND_EXECUTED)
        gw: GameView = self.window.views["game"]
        gw.run_button.dispatch_event("on_click", arcade.gui.UIOnClickEvent)
        # Wait for the end of the next routine before continuing test script
        if not EventHandler.wait(5):
            T.error("Timeout on a click continue")

    def click_step(self, mustEndLevel=False):
        """
        Simulate a click on the step button in the GUI, and wait until the element is threated
        """
        # Force to consume the flag
        # Force to consume the flag
        if mustEndLevel:
            EventHandler.addReason(Reason.VIEW_END_GAME)
        else:
            EventHandler.addReason(Reason.COMMAND_EXECUTED)
        gw: GameView = self.window.views["game"]
        gw.step_button.dispatch_event("on_click", arcade.gui.UIOnClickEvent)
        # Wait for the end of the next routine before continuing test script
        if not EventHandler.wait(5):
            T.error("Timeout on a click continue")

    def verify_victory(self):
        """
        Verify if we have reach the victory screen
        """
        match (self.window.views["end_game"].result_text):
            case "VICTORY" | "VICTOIRE":
                T.passed(f"GUI Victory view loaded as expected")
            case _:
                T.error(f"GUI loaded {self.window.views['end_game'].result_text} instead of victory")

    def verify_defeat(self):
        """
        Verify if we have reach the defeat screen
        """
        match (self.window.views["end_game"].result_text):
            case "DEFEAT" | "DEFAITE":
                T.passed(f"GUI Defeat view loaded as expected")
            case _:
                T.error(f"GUI loaded {self.window.views['end_game'].result_text} instead of victory")


    def recompile_bug(self):
        super().recompile_bug()
        # Ensure the level is well reset and synchronize the test thread
        self.click_restart()

    def recompile_answer(self):
        super().recompile_answer()
        # Ensure the level is well reset and synchronize the test thread
        self.click_restart()

    def verify_if_command_available(self, cmd: str, mustBeAvailable:bool=True):
        """
        Verify if a command button is visible in the GUI
        """
        gw: GameView = self.window.views["game"]
        if (cmd in gw.metadata["available_commands"]) == mustBeAvailable:
            message  = "" if mustBeAvailable else "not "
            T.passed(f"{cmd} {message}available as expected")
        else:
            message  = "should" if mustBeAvailable else "shouldn't"
            T.error(f"{cmd} {message} be available")
