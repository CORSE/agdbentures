"""Map objects."""

from typing import Any, Union, Optional
from queue import Queue

from logs import lvl
from level.action import Action

import graphic.constants as cst
from config import Config
from language import Lang

# Default direction
# _DIRECTION_DICT = {"up": 0, "down": 1, "left": 2, "right": 3}
_DIRECTION_DICT = {
    "up": cst.Direction.UP,
    "down": cst.Direction.DOWN,
    "left": cst.Direction.LEFT,
    "right": cst.Direction.RIGHT,
    "here": cst.Direction.HERE,
    0: cst.Direction.UP,
    1: cst.Direction.DOWN,
    2: cst.Direction.LEFT,
    3: cst.Direction.RIGHT,
    4: cst.Direction.HERE,
    "DIR_UNKNOWN": cst.Direction.HERE,
    "DIR_RIGHT": cst.Direction.RIGHT,
    "DIR_LEFT": cst.Direction.LEFT,
    "DIR_UP": cst.Direction.UP,
    "DIR_DOWN": cst.Direction.DOWN,
    "DIR_HERE": cst.Direction.HERE,
    None: cst.Direction.RIGHT,
}




class Coordinates:
    def __init__(
        self,
        *,
        char_rep: str = "!",
        coord_x: Union[int, None] = None,
        coord_y: Union[int, None] = None,
        name: Optional[str] = None,
    ):
        """
        Coordinates to have easier access to particular points.
        """
        self._char_rep = char_rep
        self.name = name
        self.coord_x: Union[int, None] = coord_x
        self.coord_y: Union[int, None] = coord_y

    def __str__(self):
        return f"Coords({self.name} at {self.coord_x}x{self.coord_y})"


class Object:
    def __init__(
        self,
        *,
        char_rep: str = "o",
        init_x: Union[int, None] = None,
        init_y: Union[int, None] = None,
        var_x: Union[str, None] = None,  # variable to watch in .c code
        var_y: Union[str, None] = None,
        var_dir: Union[str, None] = None,
        name: Optional[str] = None,
        map: Optional[str] = None,
        **kwargs,
    ):
        """
        Object initialization.
        """
        self._char_rep = char_rep
        self.name = name
        self.out_queue: Optional[Queue] = None

        # dynamic properties
        self.initial_x: Union[int, None] = init_x  # used also when reset
        self.initial_y: Union[int, None] = init_y  # used also when reset
        self.var_x = var_x  # the inferior's variable to update coord_x
        self.var_y = var_y  # the inferior's variable to update coord_y
        self.var_dir = var_dir  # the inferior's variable to update direction
        self.map = map
        self.initial_direction = cst.Direction.HERE
        self.visible: bool = True
        self.has_changed: bool = False  # whether to send update or not
        self._topic = "object_update"

        for name, value in kwargs.items():
            lvl.debug(f"Setting attribute of {self.name} {name}:{value}")
            self.__setattr__(name, value)

        self.set_reset()

    def set_reset(self):
        self.coord_x: Union[int, None] = self.initial_x
        self.coord_y: Union[int, None] = self.initial_y
        self.direction: Union[int, None] = self.initial_direction

    def __str__(self):
        return f"Object({self.name} at {self.coord_x}x{self.coord_y})"

    def __repr__(self):
        """String representation."""
        attr = {key: value for key, value in self.__dict__.items() if key != "out_queue"}
        return f"Object({attr})"

    def payload(self):
        return {
            "topic": self._topic,
            "object_name": self.name,
            "coord_x": self.coord_x,
            "coord_y": self.coord_y,
            "direction": self.direction,
            "visible": self.visible,
        }

    def message(self, msg_label):
        try:
            return self.messages[Config.LANGUAGE][msg_label]
        except KeyError:
            lvl.debug(f"Message '{msg_label}' not found in language {Config.LANGUAGE}") # TODO: how to convert enum to string to display?
            return self.messages[Lang.default][msg_label]

    def talk(self, msg_label: str):
        payload = self.payload()
        payload['action'] = Action('talk')
        payload['message'] = self.message(msg_label)
        self.send_update(payload)

    def talks(self, msg_label: str):
        # for backward compatibility
        self.talk(msg_label)

    def notify_update(self, memory=None):
        """
        Sends an update payload if the `has_changed` flag is active.
        Then calls the post_update function.
        """
        if not self.has_changed:
            return
        lvl.debug(f"Updated: {self}")
        payload = self.payload()
        self.send_update(payload)
        self.has_changed = False
        self.post_update(self, memory)

    def update(self, memory: dict, run_hook: bool = True) -> None:
        """
        Update object property.
        Need to overide set_position
        """
        raise NotImplementedError("Self update of objects is deprecated, the world should do it")


    def set_out_queue(self, queue: Queue|None = None) -> None:
        """Set a communication queue for post update hook."""
        self.out_queue = queue

    def send_update(self, payload: dict|None = None) -> None:
        """
        Send update informations to hook functions.

        The argument given to the hooks is a communication payload.
        """
        pl = payload or self.payload()
        if self.out_queue is not None:
            self.out_queue.put_nowait(pl)
        else:
            lvl.warning("Cannot send payload")

    def set_empty(self):
        self.visible = False
        self.coord_x = None
        self.coord_y = None
        self.direction = cst.Direction.RIGHT

    @property
    def char_rep(self) -> str:
        """Get the character representation of the player."""
        return self._char_rep

    # def visibility_condition(self):
    # return True

    def set_position(self, new_x, new_y):
        """
        Set object position, setting `has_changed` flag if necessary.
        """
        old_x = self.coord_x
        old_y = self.coord_y
        changed = False
        if new_x is not None and new_x != old_x:
            changed = True
            self.coord_x = new_x
        if new_y is not None and new_y != old_y:
            changed = True
            self.coord_y = new_y
        if changed:
            lvl.debug(f"Position changed for {self} (was: {old_x}x{old_y})")
            # by default, also make object visible if it has
            # both coordinates
            self.visible = self.coord_x is not None and self.coord_y is not None
            self.has_changed = True

    def set_direction(self, new_dir_idx):
        """
        Set object direction, setting `has_changed` flag if necessary.
        """
        old_dir = self.direction
        if new_dir_idx is None or new_dir_idx not in _DIRECTION_DICT:
            return
        new_dir = _DIRECTION_DICT[new_dir_idx]
        self.direction = new_dir
        self.has_changed = True

    def is_at(self, x, y):
        return self.coord_x == x and self.coord_y == y

    def is_on(self, obj, side=cst.Direction.HERE):
        """
        Test if object in on another object, or close to an object in given direction.
        """
        lvl.debug(f"test if {self} is on {obj} (side {side})")
        if obj.coord_x is None or obj.coord_y is None:
            return False
        dx, dy = cst.DELTA[side]
        return self.is_at(obj.coord_x+dx, obj.coord_y+dy)

    def is_above(self, obj):
        return self.is_on(obj, side=cst.Direction.UP)

    def is_on_left(self, obj):
        return self.is_on(obj, side=cst.Direction.LEFT)

    def is_on_right(self, obj):
        return self.is_on(obj, side=cst.Direction.RIGHT)

    def is_below(self, obj):
        return self.is_on(obj, side=cst.Direction.DOWN)


    def place_at(self, x, y):
        if x != self.coord_x or y != self.coord_y:
            self.has_changed = True
        self.coord_x = x
        self.coord_y = y

    def place_on(self, obj, side=cst.Direction.HERE):
        """
        Place object at same position as another object, or close to an object in given direction.
        """
        dx, dy = cst.DELTA[side]
        self.place_at(obj.coord_x + dx, obj.coord_y + dy)

    def post_update(self, object, memory):
        """
        Redefine this function in a level to get special effects for this particular object.
        """
        pass

    def reset(self):
        """
        Redefine this function to run custom code for this object when the level restarts.
        """
        pass

class Player(Object):
    """Player class."""

    def __init__(self):
        super().__init__(name="player")
        self._topic = "player_update"

    def set_initial_y(self, y:int):
        """
        Set initial_y as well as coord_y
        """
        self.initial_y = y
        self.coord_y = y

    def set_initial_direction(self, dir):
        self.initial_direction = dir
        self.direction = dir

    def set_reset(self):
        super().set_reset()
        self.visible = self.coord_x is not None and self.coord_y is not None

    @property
    def char_rep(self) -> str:
        """Get the character representation of the player."""
        up_value = cst.Direction.UP
        down_value = int(self.dir_mapping["down"])
        left_value = int(self.dir_mapping["left"])
        right_value = int(self.dir_mapping["right"])

        if self.direction == cst.Direction.UP:
            return "^"
        if self.direction == cst.Direction.DOWN:
            return "v"
        if self.direction == cst.Direction.LEFT:
            return "<"
        if self.direction == cst.Direction.RIGHT:
            return ">"
        return "?"  # Unknown direction


class OutsideObject(Object):
    """
    Objects added by agdbentures but are not actually in the inferior.
    """
    def update(self):
        pass


class Wop(OutsideObject):
    """Wise old person class."""

    def __init__(
        self,
        wop: dict,
    ):
        """
        Wop initialization.

        :param variables_env: The environment to use to evaluate the conditions.
        :param name: The name/label of the WOP.
        :param wop: Dictionnary that describes the wop.
        """
        super().__init__(name=wop["name"], init_x=int(wop["x"]), init_y=int(wop["y"]))
        self._topic = "wop_update"

        # Object initialization
        # TODO Change WOP visibility from arguments
        lvl.debug(f"wop visible? {wop['visible']}")
        self.visible = wop["visible"]
        self.direction = _DIRECTION_DICT[wop["direction"]]

        # Infos
        self.messages = wop["messages"]
        # self.triggered = False

        # List of message labels the wop already used
        self.has_talked = []

        # TODO: does this really work now? need to rethink this probably
        self.has_changed = True  # to send first payload so WOP is placed


    def talk(self, msg_label: str):
        """
        Override the method so the wop only talks each message once.
        """
        if msg_label in self.has_talked:
            return
        self.has_talked.append(msg_label)
        super().talk(msg_label)


    # def update(self, globs, frames, run_hook: bool = True) -> None:
    # """
    # Update object property.
    #
    # :param var: The variable that has been updated.
    # :param new_value: The updated value.
    # :param run_hook: Tells if then hook (if existing) should be called on update.
    # """
    # if self.triggered:
    # return  # TODO handle non oneshot wop
    #
    # lvl.debug(f"WOP update: {var} ← {new_value} hook:{run_hook}")
    #
    # old_talks = self.talks
    ## Post-update hook that updates the message condition
    # self.talks = self.visible and self.trigger_condition(self)
    #
    # self.has_changed = self.has_changed or old_talks != self.talks
    #
    # if self.talks:
    # self.triggered = True
    #
    # if run_hook and self.has_changed:
    # self.send_update(self.payload())
    # self.has_changed = False

    def trigger_condition(self, wop_obj):
        return False


class LevelObjectsArea:
    """class to create an area on a tmx maps so that all the objects inside it belongs to a specific level"""

    def __init__(self, level_name:str, shape:list[list[int]]):
        self.level_name = level_name

        self.top_left_x, self.top_left_y = shape[0]
        self.bot_right_x, self.top_left_y = shape[2]

    def __contains__(self, obj):
        if hasattr(obj, "center_x"): # for SpObject
            x, y = obj.center_x, obj.center_y
        elif hasattr(obj, "shape"): # for TiledObject
            x, y = obj.shape
        return (x > self.top_left_x and y > self.top_left_y
            and x < self.bot_right_x and y < self.bot_right_x )
