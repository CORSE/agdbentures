"""Abstract level implementation."""

from abc import ABC, abstractmethod
from typing import Optional, Any, Callable
import subprocess
import sys
import os
from os import chdir
from os.path import join
import logging
import time
import tests.lib_test as T

from logs import lvl
from utils import show_threads
from easytracker import init_tracker, PauseReasonType
from level.console import AbstractConsolePrint, ConsolePrintDummy
from level.world import get_world_cls
from level.objects import Player, Object, Wop, Coordinates, LevelObjectsArea
from progress.progress_manager import progress
from tracker_helper import TrackerHelp
from verif_lib import LevelChecker
from code_window import GenServerWindow, NoServerWindow
from config import Config
from level.engine import Engine
# TODO: use lighter loading method of .tmx for non arcade versions
# import arcade
# import graphic.sprites.sp_objects as spo
import graphic.constants as cst
from graphic.map_utils import load_tmx_map

# pylint: disable=too-many-instance-attributes fixme
class AbstractLevel(ABC, TrackerHelp):
    """
    Abstract level class that allows to use different configurations.

    :param level_input_queue: a queue to receive info from whover has started us, e.g., a GUI or a command line
    :param level_output_queue: a queue to send info with whoever has started us, e.g., a GUI
    :param code_window: a code window object to show the current line of the inferior
    :param print_console: an object able to simulate gdb console output, or a dummy object when testing
    """


    def __init__(
        self,
        metadata: dict,
        level_input_queue,
        level_output_queue,
        code_window: GenServerWindow | None = None,
        print_console: AbstractConsolePrint | None = None,
        load_map_hook: Callable | None = None,  # a function to call when we load a tmx map
        window=None
    ):
        """
        Initialize the level

        :param metadata: the dictionnary that describes the level
        """
        # Communication queues
        # queue for output communication, e.g., with GUI
        self.out_queue = level_output_queue
        # queue for getting GDB command or inferior input
        self.in_queue = level_input_queue
        self.window = window
        self.code_window = code_window or NoServerWindow()
        self.print_console = print_console or ConsolePrintDummy()
        self.load_map_hook = load_map_hook

        self.metadata = metadata
        # Level infos
        self.level_number = metadata["level_number"]
        self.level_title = metadata["level_title"]
        self.level_name = metadata["level_name"]
        self.level_path = metadata["level_path"]
        self.source_level_dir = metadata["source_level_dir"] or os.path.join(Config.ROOT_DIR, Config.LEVEL_DIR, self.level_name)
        self.exec_name = metadata["exec_name"]

        self.world: None|WorldABC = None

        # Available commands
        self.available_commands = metadata["available_commands"]
        # Bugs/hints description
        self.bug = metadata["bug"]
        self.hints = metadata["hints"]

        # number of times we have started or restarted the level
        self.start_number = 0

        # compile the level sources if required (files newer than executable)
        # TODO(flo): should not be here. Either the tests will compile, or
        # arcade must handle it so it can catch errors even when starting up.
        # self.compile_source()

        # Tracker
        # Contains internal breakpoints used by level (e.g., pause on 'message' function)
        TrackerHelp.__init__(
            self,
            self.source_level_dir,
            self.exec_name,
        )

        # Maps and connection to the inferior's variable
        self.load_world()
        Engine.set_current_engine(
            Engine,
            metadata["engine_name"] if "engine_name" in metadata.keys()
            else None
        )
        player_mode = Engine.player_mode

        # Everything that also need to be reset for a restart
        self.set_reset()

        # If there are any level specific initialization
        self.custom_setup()


    def set_reset(self):
        """
        Common code that must be called when we start/restart a level
        Initializes or re-initializes internal data structures necessary to
        the well-functioning of the level.
        """
        # Internal infos
        self._last_cmdline = "next"  # TODO: change to None?
        self._internal_function_name = (
            None  # used for internal breakpoints, to know where to return to
        )
        self._internal_line_number = None

        # When recording, keep all interaction with player in a list
        self._recording = False
        self._record = []

        # checker: initially none, gets created on the fly when validating the level.
        # kept in the current objects so it can be used in custom level.py
        # files to know whether a check has already been performed (and failed)
        self.checker = None

        # Initially False, will be set to True as soon as we sent 'start' to gdb.
        self.inferior_started = False

        # True if we are handling internal brwatch. Important to simulate
        # the inferior running as in gdb, i.e., either it is actually running
        # or we are handling internal breakpoints or watchpoints, which from
        # the user point-of-view should not make a difference.
        self.is_handling_internal_brwatch = False

        # if a magic breakpoint was placed, remove it
        # TODO: should actually replace magic bp after restart, showing it
        # in the GUI.
        # This is probably already handled in the TrackerHelp
        # (removing and re-installing), but need to notify the GUI.
        if self.magic_bp:
            self.delete_magic_breakpoint()

        # Management of interactive levels, where user can navigate the
        # player character.
        self.interactive_bp = None
        self.interactive_mode = False

        # Reset of particular objects
        self.world.set_reset()


#####
##
## Utility methods
##
#####

    def show_next_line(self):
        """
        Show in the code window the next line that will be executed by gdb.
        To mimick gdb's behavior.
        """
        source_file = self.tracker.next_source_file
        line_number = self.tracker.next_lineno
        self.code_window.show_next_line(
            source_file, self.source_level_dir, line_number
        )


    def send_print_thread_command(self, cmd):
        """
        Send a command to modify the console print behavior.
        """
        self.print_console.send_command(cmd)


    def send_print_thread_error(self, msg):
        """
        Show error in the print console.
        """
        self.print_console.send_error(msg)


    def userinfo(self, msg):
        """
        Just output informations from agdbentures (warning, errors, messages to player)
        """
        print(msg)



#####
##
## Properties on current level
##
#####

    @property
    def is_running(self):
        """
        When we are handling an internal breakpoint, we consider the level is
        still running as from the user perspective, this should be the case
        (internal breaks and watches should be transparents).
        """
        r = self.tracker and (self.tracker.is_running() or self.is_handling_internal_brwatch)
        return r

    @property
    def is_validating(self):
        return self.checker and self.checker.tracker and self.checker.tracker.is_running()

    @property
    def is_recording(self):
        return self._recording

#####
##
## Map loading from .tmx files
##
#####

    def load_map(self, map_name):
        map_path = join(self.level_path, self.metadata["arcade_maps"][map_name])
        map_off_x, map_off_y = self.metadata["arcade_maps"].get(
            map_name + "_offset", (0, 0)
        )

        tm = load_tmx_map(map_path, (map_off_x, map_off_y), self.level_name)
        if self.load_map_hook:
            self.load_map_hook(map_name, tm)

        self.world.add_map(
            name=map_name,
            width=tm.width,
            height=tm.height
        )
        assert 'map_width' not in self.metadata, "Deprecated"
        assert 'map_height' not in self.metadata, "Deprecated"

        # Will get objects and characters
        for o in tm.sprite_lists["objects"]:
            obj = Object(init_x=o.coord_x, init_y=o.coord_y,
                         **(o.properties | self.metadata['objects'][o.name]))

            if hasattr(o, "direction"):  # only for characters
                obj.direction = o.direction

            if hasattr(o, "name"):  # for characters, not in properties anymore
                obj.name = o.name

            obj.set_out_queue(self.out_queue)
            # TODO: may add to outside_objects? How to make the difference?
            # maybe in a different layer in the .tmx?
            self.world.add_object(map_name, obj)

        for coord in tm.object_lists["coordinates"].values():
            self.world.add_coordinates(map_name, coord)

    def load_world(self):
        """
        Load all maps mentionned in metadata.
        Those are tmx files.
        """
        world_cls = get_world_cls(self.metadata)
        self.world = world_cls(level=self, out_queue=self.out_queue)
        self.current_map = "main"

        if not self.metadata["arcade_maps"]:
            # only for unit tests
            self.world.add_map(name="main",width=1, height=1)
        else:
            # regular levels always have .tmx maps
            for map_name in self.metadata["arcade_maps"]:
                if not map_name.endswith("_offset"):
                    self.load_map(map_name)

        # Add the player to the world
        if 'player_mode' in self.metadata:
            lvl.warning(f"Deprecated: player_mode is not used anymore ({self.metadata['player_mode']})")

        self.player = Player()
        if "player_y" in self.metadata:
            self.player.set_initial_y(int(self.metadata["player_y"]))
            self.player.set_initial_direction(cst.Direction.RIGHT)

        self.player.set_out_queue(self.out_queue)
        self.world.add_player(self.player)

        # Handling of the exit spot
        if "exit_x" in self.metadata:
            init_x = int(self.metadata["exit_x"])
        else:
            init_x = None

        if "exit_y" in self.metadata:
            init_y = int(self.metadata["exit_y"])
        else:
            init_y = None

        # Adding the exit to the map
        exit_obj = Object(
            init_x=init_x,
            init_y=init_y,
            char_rep="@",
            name="exit",
        )
        exit_obj.set_out_queue(self.out_queue)
        self.world.add_exit(exit_obj)

        # lvl.debug(f"Init {exit_obj.name}: {init_x} and {init_y}, watching {var_x} and {var_y}")
        # lvl.debug(f"Init {exit_obj.name}: var_x in object {exit_obj.var_x}")
        # lvl.debug(f"Init {exit_obj.name}: var_y in object {exit_obj.var_y}")

        # Create a WOP
        for wop_dict in self.metadata["wops"].values():
            lvl.debug(f"Adding wop {wop_dict}")
            wop = Wop(wop_dict)
            self.world.add_outside_object(wop)
            wop.set_out_queue(self.out_queue)





#####
##
## Methods to update the state of a level, reflecting the inferior's state
##
#####

    def get_inferior_memory(self):
        return self.tracker.get_program_memory(as_raw_python_objects=True)

    def update_state(self) -> None:
        """Run events for updated variables"""
        if (not self.inferior_started
        or self.tracker._pause_reason.type == PauseReasonType.SIGNAL and 'SIGINT' in self.tracker._pause_reason.args):
            return
        try:
            memory = self.get_inferior_memory()
            if memory is None:
                lvl.error(f"Cannot update state of level, memory is None")
                return False
        except ValueError as e:
            lvl.warning(f"Cannot update state of level: {e}")
            return False

        # verify if there was a map change since last time
        # self.check_map_change(memory)

        # lvl.debug(f"Globals variables: {globs}")
        # lvl.debug(f"Frames: {frames}")
        # update all objects on the current map, as well as the floor (entities)
        self.world.update(memory)


    def update_all(self):
        lvl.debug("Running update_all for level")

        # Fetch and update variables if required
        self.update_state()

        # Update the code view
        self.show_next_line()


#####
##
## Management surrounding the level: compilation, other levels.
##
#####

    def compile_source(self, force=False, target='all', capture_output=False):
        lvl.info(f"Compiling program in {self.source_level_dir}")
        cmd = ["make", "-C", self.source_level_dir, target]
        if lvl.getEffectiveLevel() > logging.DEBUG:
            cmd.append("--quiet")
        if force:
            cmd.append("--always-make")
        result = subprocess.run(
            cmd,
            capture_output=capture_output
        )
        lvl.debug(f"Compilation results: {result}")
        return result

    def unlock_next_levels(self):
        """
        Called whenever we consider this level valid, hence unlocking potential
        next levels.
        """
        lvl.info(f"Success on level {self.level_name}, unlocking levels...")
        # mark the level as success
        progress.set_done(self.level_name)

#####
##
## Validation of a level
##
#####

    def pre_validation(self):
        """
        Redefine for a particular level
        """
        pass

    def post_validation(self):
        """
        Redefine for a particular level
        """
        pass

    def validate_level(self, timeout: float|None = None) -> (bool, str | None):

        """
        Do some general and specific checkings for current level.
        """
        lvl.info(f"Validating level")

        # Freezing temporarily current tracker to avoid using it by error during validation
        tracker_freeze = self.tracker
        assert tracker_freeze
        self.tracker = None
        valid = False
        reasons = None

        if timeout:
            self.checker = LevelChecker(self, timeout=timeout)
        else:
            self.checker = LevelChecker(self)
        if self.checker.result():
            lvl.info("Level validated")
            valid = True
        else:
            valid = False
            lvl.info("Level failed validation for the following reasons:")
            reasons = self.checker.get_reasons()
            lvl.info(reasons)
            lvl.debug(f"Check statistics: {self.checker.get_stats()}")

        # Restoring tracker, if we want to start again
        self.tracker = tracker_freeze
        assert self.tracker
        return valid, reasons

#####
##
## Start / restart management.
##
## The first 4 methods will be overriden by specialization class of the AbstractLevel class.
## The fifth method can be overriden by the level-specific class.
##
#####

    def pre_first_start(self):
        """
        Called just before the first 'start' is sent to gdb.
        """
        pass

    def pre_restart(self):
        """
        Called just before the subsequent 'start' are sent to gdb.
        """
        pass

    def post_first_start(self):
        """
        Called just after the first 'start' is sent to gdb.
        """
        pass

    def post_restart(self):
        """
        Called just after the subsequent 'start' are sent to gdb.
        """
        pass

    def custom_setup(self) -> None:
        """
        Set the level. Needs to be defined in the level-specific .py file
        """
        pass

    def custom_first_start(self):
        """
        Redefine this method in a level to set special code to be executed
        after the first start for this level.

        LEGACY: call the old `arcade_custom_first_start` if not redefined so old levels still
        work.
        """
        self.arcade_custom_first_start()

    def arcade_custom_first_start(self):
        pass

    def custom_restart(self):
        """
        Redefine this method in a level to set special code to be executed each
        time the program is *restarted* for this level (i.e., all 'start' but
        for the first one).

        LEGACY: call the old `arcade_custom_restart` if not redefined so old levels still
        work.
        """
        self.arcade_custom_restart()

    def arcade_custom_restart(self):
        pass



    def do_start(self):
        """
        Start the level, by starting the inferior.
        Can be called either at first start or at a restart.
        (Note: cannot be named 'start' at it interferes with the Thread class)

        There are two cases:
            - initial start: need to initialize everything
            - restart with tracker alive
        """

        lvl.debug("Performing level 'set/reset' then start")


        if self.start_number == 0:
            self.pre_first_start()
            lvl.debug("Performing first start")
            self.load_and_start_program()
        else:
            self.pre_restart()  # before set_reset, as the GUI must be notified first
            self.set_reset()    # not necessary for first start as was already called by init
            lvl.debug("Performing a restart")
            self.reload_and_start_program()

        self.inferior_started = True
        self.start_number += 1

        # update the local variables, line in code viewer, then notify
        self.update_all()
        
        if self.start_number == 1:
            self.post_first_start()
        else:
            self.post_restart()


    def restart(self):
        """
        Called to trigger a level restart.
        """
        assert False
        # Not the correct way to do anymore
        # self.send_console_command("start", prompt=True)  # make gdb start/restart the program


#####
##
## GDB execution handling / simulating
##
#####

    def handle_breakpoint(self, cont=False) -> bool:
        """
        Handle stopping on a breakpoint during a gdb command.

        :param cont is set to True if the breakpoint happened during a 'continue'-like
        command:
           * continue  => just return and let _gdb_continue method loop work
           * step      => can happen with e.g. 'step 50': let _gdb_step method loop do the work

        :return value is True if we must really stop there (e.g. we are on a user breakpoint)

        """
        pause_reason = self.tracker.pause_reason

        # Check if the breakpoint is user defined
        br_no = int(pause_reason.args[0])  # type: ignore

        if br_no in self.internal_bp:
            bp = self.internal_bp[br_no]
        elif br_no in self.internal_wp:
            bp = self.internal_wp[br_no]
        else:
            lvl.debug(f"Breakpoint number {br_no} is not internal")
            self.is_handling_internal_brwatch = False
            return True

        self.is_handling_internal_brwatch = True

        # This is an internal breakpoint, need to "finish" the current gdb
        # command
        lvl.debug(f"Handling internal bp {br_no} mode {cont=} ({bp})")
        # lvl.debug(f"Internal breakpoint, calling callback functions")
        # lvl.debug(f"BREAKPOINTS: {self.internal_bp}")
        # lvl.debug(f"My BP: {self.internal_bp[br_no]}")
        for cb in bp.callbacks:
            cb()

        # Now it will depend on the type of the last gdb command used
        if cont:
            # continue will happen inside the _gdb_continue method
            # step also
            self.is_handling_internal_brwatch = False
            return False

        # Otherwise, we must go up in frames until we reach a user bp or the
        # function where command was called.
        # With next, it will effectively place the PC on the next line
        assert self._internal_function_name
        lvl.debug(
            f"START going up the stack to find {self._internal_function_name} (currently at {self.tracker.get_current_function_name()})"
        )

        while True:
            # we can already by at the correct position, e.g., if the next line
            # had the breakpoint
            if self.tracker.get_current_function_name() == self._internal_function_name:  # type: ignore
                # breakpoint()
                lvl.debug(
                    f"Finished going up the stack: found {self._internal_function_name}"
                )
                # If on a function that stores the result in a variable, one
                # start step is necessary to "finish" the current line
                if self._internal_line_number == self.tracker.next_lineno:
                    self.tracker.step()
                self.is_handling_internal_brwatch = False
                self.print_console.set_finish_false()
                return False  # back to the initial frame

            lvl.debug(
                f"Finishing {self.tracker.get_current_function_name()} to go up the stack to find {self._internal_function_name}"
            )
            self.print_console.set_finish_true()
            pause_reason = self.tracker.send_direct_command("finish")

            if pause_reason.type is PauseReasonType.FUNCTION_FINISHED:
                # that was expected :-) next round!
                continue

            # need to handle other wanted or not stops
            if pause_reason.type is PauseReasonType.EXITED:
                # somehow the program terminated
                self.is_handling_internal_brwatch = False
                return True  # should not try to contine after program exited...

            if pause_reason.type is PauseReasonType.BREAKPOINT or \
               pause_reason.type is PauseReasonType.WATCHPOINT:
                # recursively handle the bp:
                # - if user breakpoint, will return True
                # - if internal breakpoint, will have gone up the stack back to
                # original point and returned False
                return self.handle_breakpoint(cont)

            if pause_reason.type is PauseReasonType.CALL:
                raise NotImplementedError
                self.handle_track(finish=False)

            if pause_reason.type is PauseReasonType.SIGNAL:
                # can be an interrupt, e.g. if asking for recompilation
                if 'SIGINT' in self.tracker._pause_reason.args:
                    return True  # must stop anyway

            # TODO: debug just to know the pause reason when fin
            lvl.error(f"Pause reason {pause_reason} not handled")
            raise NotImplementedError


    def _gdb_next(self, count: Optional[int] = None):
        """
        Emulate the next gdb command.
        We have to use a loop and not pass 'count' to gdb, as we need to handle
        internal bp, and there is no way of knowing how many 'next' gdb has executed before
        stopping on a bp.
        """
        count = count or 1
        if count > 1:
            # hide all the commands sent to gdb in the user console
            self.send_print_thread_command('pause')
        for _ in range(count):
            pause_reason = self.tracker.next()
            # self.update_state()
            if (
                pause_reason.type is PauseReasonType.EXITED
                or pause_reason.type is PauseReasonType.SIGNAL
            ):
                self._internal_function_name = None
                self._internal_line_number = None
                break
            elif pause_reason.type is PauseReasonType.CALL:
                raise NotImplementedError
                self.handle_track()  # Internal breakpoint
            elif pause_reason.type is PauseReasonType.BREAKPOINT or \
                pause_reason.type is PauseReasonType.WATCHPOINT:
                ret = self.handle_breakpoint()
                if ret:
                    break
            elif pause_reason.type is PauseReasonType.WATCHPOINT:
                ret = self.handle_breakpoint()
                if ret:
                    break
            elif pause_reason.type is PauseReasonType.ENDSTEPPING_RANGE:
                # normal reason: 'next' is finished
                continue
            elif pause_reason.type is PauseReasonType.ERROR:
                lvl.error(f"Error from gdb resulting from _gdb_next")
                raise ValueError("GDB error")

            elif pause_reason.type is PauseReasonType.UNKNOWN:
                lvl.error(f"Pause reason {pause_reason} badly handled in _gdb_next")
                raise NotImplementedError
            else:
                lvl.error(f"Pause reason {pause_reason} not handled")
                raise NotImplementedError
                # break  # Breakpoint while handling CALL or WATCHPOINT

        if count > 1:
            self.send_print_thread_command('last-and-resume')

    def _gdb_continue(self, timeout: float|None = None):
        """Emulate the continue command.
        TODO add ignore_count parameter
        """
        while True:
            lvl.debug(f"Emulating continue: looping on `resume`")
            pause_reason = self.tracker.resume()
            if pause_reason.type is PauseReasonType.BREAKPOINT or \
               pause_reason.type is PauseReasonType.WATCHPOINT:
                lvl.debug(f"Got a breakpoint, will check if need to really stop")
                ret = self.handle_breakpoint(cont=True)
                lvl.debug(f"Back from handle breakpt")
                if ret:
                    lvl.debug(f"Must stop now")
                    break
            elif pause_reason.type is PauseReasonType.CALL:
                raise NotImplementedError
                self.handle_track()
            else:
                lvl.debug(f"Must stop because pause reason is {pause_reason.type}")
                break

    def _gdb_until(self, timeout: float|None = None):
        """
        Emulate the continue command.
        TODO think about this, it is more difficult than it seems...
        => probably need to store argument (line number?, what if not given?)
           to be able to re-do the until if on an internal break
        """
        raise NotImplementedError


    def _gdb_interrupt(self):
        self.interrupt_inferior()

    def _gdb_break(
        self,
        loc_str: Optional[str] = None,
        line_no: Optional[int] = None,
    ) -> None:
        """Emulate the break command."""
        if line_no is not None:
            self.tracker.break_before_line(line_no)
        elif loc_str is not None:
            self.tracker.break_before_func(loc_str)
        else:
            current_line = self.tracker.next_lineno
            if current_line is not None:
                self.tracker.break_before_line(current_line)

    def _gdb_step(self, count: Optional[int] = None):
        """Emulate gdb commands."""
        count = count or 1
        if count > 1:
            self.send_print_thread_command('pause')
        for _ in range(count):
            pause_reason = self.tracker.step()
            if pause_reason.type is PauseReasonType.WATCHPOINT:
                raise NotImplementedError
                self.handle_watch()
            elif pause_reason.type is PauseReasonType.CALL:
                raise NotImplementedError
                self.handle_track()
            elif pause_reason.type is PauseReasonType.BREAKPOINT:
                ret = self.handle_breakpoint(cont=True)
                if ret:
                    break
            else:
                break
        if count > 1:
            self.send_print_thread_command('last-and-resume')

    def _gdb_delete(self, numbers : list[str]) -> None:
        """
        @params : numbers= command receive from the game in the expected format :
        We are on a delete, so we will parse the rest of line to see break number

        This function analyze command player send to gdb to see if
        the breakpoint he wants to delete is one of us.
        If it's one of us, we do nothing else we delete him breakpoint
        """
        # actual number of the biggest id of breakpoint in gdb
        borne = self.tracker._next_bp_num + 1
        for number in numbers:
            try :
                if (
                        int(number) not in
                        self.list_break.get_ids_of_our_breakpoints() and
                        int(number) < borne
                ):
                    self.tracker.send_direct_command(f"d {number}")
                else:
                    self.print_console.output_queue.put_nowait(('GDB', f"No breakpoint number {number}\n"))
            except:
                lvl.debug("delete take number in argument with space separation")


    def _gdb_finish(self):
        self.tracker.send_direct_command("finish")

    def _on_quit(self):
        """
        Quit handler. Called when asking gdb to quit or exit in the console.
        """
        lvl.warning("Exiting gdb...")
        self.stop_level()


#####
##
## Process input commands from the command line or the GUI
##
#####

    def add_short_cmds(self) -> None:
        """
        Extend the available commands with the gdb aliases.
        TODO: use this or similar instead of hard-code in parse_and_eval.
        """
        for command in self.available_commands:
            if command == "next":
                cmds = ["n"]
            elif command == "step":
                cmds = ["s"]
            elif command == "break":
                cmds = ["brea", "brea", "bre", "bre", "b"]
            elif command == "run":
                cmds = ["r"]
            elif command == "continue":
                cmds = ["c", "fg"]
            elif command == "condition":
                cmds = ["cond"]
            else:
                continue
            self.available_commands.extend(cmds)


    # pylint: disable=too-many-branches
    def parse_and_eval(self, cmdline: str) -> None | str:
        """
        Parse a command and call the appropriate function.

        :param cmd: The command to run.
        :return: None if there is nothing else to do, or a string if subsequent
        actions are required
        """
        def not_implemented(cmd: str) -> None:
            """Verbose error if not implemented."""
            self.userinfo(f"Arguments for the '{cmd}' command are not handled yet")

        def usage(cmd: str, args: str) -> None:
            """Print usage of command that takes one single integer."""
            self.userinfo(f"Usage: {cmd} {args}")

        if not cmdline or cmdline.isspace():
            if self._last_cmdline is None:
                return  # Nothing to do
            cmdline = self._last_cmdline  # Using the last command

        lvl.debug(f"Parse and eval: {cmdline}")
        # Saving function name
        self._internal_function_name = self.tracker.get_current_function_name()  # type: ignore
        self._internal_line_number = self.tracker.next_lineno
        # Cleaning up the command
        cmdline = cmdline.strip()

        # Saving the command unless the command is to restart
        if cmdline != "start":
            self._last_cmdline = cmdline
        if self.is_recording:
            self.append_record(["gdb", cmdline])

        # Parsing argument and conversion to integer
        cmd, *args = cmdline.split(maxsplit=1)
        arg_str = None
        arg_int = None
        if args and len(args) == 1:
            arg_str = args[0]
            if arg_str.isnumeric():
                arg_int = int(arg_str)
                # if cmd not in self.available_commands and
                # cmd not in {"quit", "q", "exit"}:
        if False:

            self.userinfo(f"{cmd} is not a valid command.")
            return

        lcmd = len(cmd)

        if cmd in ["run"]:
            self.inferior_started = True

        if cmd == "start":
            # Do not forward to gdb, notify the level thread that we have to
            # restart the level
            return 'restart'

        elif cmd in ["n", "next"]:
            if arg_str is not None and arg_int is None:
                usage(cmd, "[count_integer]")
                return
            self._gdb_next(arg_int)

        elif cmd in ["s", "step"]:
            if arg_str is not None and arg_int is None:
                usage(cmd, "[count_integer]")
                return
            self._gdb_step(arg_int)

        # elif cmd in ["r", "run"]:
        #    if arg_str is not None or arg_int is not None:
        #        not_implemented(cmd)
        #        return
        #    self._gdb_continue()

        elif cmd in ["c", "cont", "continue"]:
            self._gdb_continue()

        elif cmd in ["b", "break"]:
            self._gdb_break(arg_str, arg_int)

        elif lcmd >= 3 and "finish".startswith(cmd):
            self._gdb_finish()

        elif cmd == "interrupt":
            self._gdb_interrupt()

        elif cmd in ["q", "qui", "quit", "exi", "exit"]:
            lvl.warning("Not allowed to quit gdb directly")
            # self._on_quit()
            return "gdb_exit"

        elif cmd in ["d", "del", "delete"]:
            if args:
                self._gdb_delete(args[0].split())
            else:
                self.list_break.change_delete(self.tracker._next_bp_num + 1)
                init_break = self.list_break.get_ids_of_our_breakpoints()
                for number in range(1, self.tracker._next_bp_num + 2):
                    if number not in init_break:
                        self.tracker.send_direct_command(f"d {number}")


        else:
            lvl.info(f"Command {cmd} not implemented yet.")
            try:
                self.tracker.send_direct_command(cmdline)
            except RuntimeError:
                print("Command does not exist!")
        # TODO add other commands (condition, ...)
        # and extend argument of existing commands (run, continue)


    def print_prompt(self):
        self.send_print_thread_command('prompt')


    def interrupt_validation(self):
        """
        Used e.g., on an infinite loop during validation.
        TODO: use a timer to automatically send interrupt.
        """
        assert self.checker.tracker
        self.checker.tracker.interrupt()


    def send_out_queue(self, payload):
        """
        Send message to whoever has started us
        """
        self.out_queue.put_nowait(payload)

    def send_to_gui(self, payload):
        """
        Alias during transition toward GUI-free stuff in abc
        """
        self.send_out_queue(payload)

    ## Putting commands in our own queue
    def send_console_command(self, command: str, cmdtype="gdb", prompt=False):
        lvl.debug(f"Putting gdb command in level_in queue {command}")
        if prompt:
            self.print_prompt()
        self.in_queue.put_nowait({"type": cmdtype, "command": command})
        self.print_console.show_gdb_command(command + '\n')

    ## Putting commands in our own queue
    def send_level_control(self, control_command: str):
        lvl.debug(f"Putting level command in level_in queue {control_command}")
        self.in_queue.put_nowait({"type": "control", "command": control_command})


#####
##
## Management of user interraction recording.
##
#####

    def start_record(self):
        self._recording = True
        self.send_console_command("start")

    def append_record(self, rec):
        self._record.append(rec)

    def save_record(self):
        if not self._recording:
            raise RuntimeError("No record file")
        self._recording = False
        with open(self.record_file(), "w") as f:
            for source, command in self._record:
                print(f"{source}:{command}", file=f)

        # empty current record
        self._record.clear()

    def play_record(self):
        if not os.path.exists(self._record_file()):
            raise RuntimeError("No record file")

        self._recording = False

        with open(self.record_file()) as f:
            commands = f.readlines()

        # self.send_console_command("start")

        for command in commands:
            source, text = command.strip("\n").split(":")
            # print("(gdb) ", end="")
            # TODO nice loop to wait for GDB to run before sending to stdin
            if source == "gdb":
                self.send_console_command(text)
            elif source == "input":
                self.send_console_command(text)
            else:
                print(f"Unknown source in record {source}")

            # print(self.level.tracker.get_global_variables(True))

    def record_file(self):
        return os.path.join(
            self.source_level_dir,
            "record.in",
        )


#####
##
## WOP utilities
##
#####

    def available_wop(self) -> set[tuple[int, int]]:
        """Return a list of WOP coordinates."""
        assert False, "Deprecated function?"
        coordinates = set()

        for wop in self.wops:
            if wop.visible:
                coordinates.add((wop.coord_x, wop.coord_y))

        return coordinates

    def get_wop_message(self) -> dict[str, list[str]]:
        """
        Return a dictionnary of messages, with the WOP
        name as key and its message as value (as a list of lines).
        """
        message_dict = {}

        assert False, "Deprecated function?"
        for wop in self.wops:
            if wop.visible and wop.talks:
                wop.triggered = True
                message_dict[wop.name] = wop.message_list
                wop.talks = False

        return message_dict


#####
##
## Methods to stop / terminate / delete level or tracker.
##
#####

    def __del__(self):
        self.stop_level()

    def stop_level(self):
        """
        Terminate the tracker
        TODO: investigate, useless to send None to the GUI. Can we send something else?
        """
        # self.out_queue.put_nowait({'topic': "quit"})
        lvl.info("Stopping level...")
        self.out_queue.put_nowait(None)
        self.terminate_tracker()

    def terminate_tracker(self):
        """
        Safe function to call when we want to terminate the tracker.
        Ensures the tracker exists before terminating it, and sets the tracker to None.
        """
        if not self.tracker:
            return
        self.tracker.terminate()
        self.tracker = None


    @abstractmethod
    def test(self):
        """ Test the level. Needs to be overriden by level-specific class. """

    @abstractmethod
    def run(self):
        """ Run the level. Needs to be overriden by specialization class. (test, arcade) """

    def recompile(self, mode="all"):
        T.print_info(f"Recompiling in {mode} mode")
        T.clear_cmds()  # ensure old commands are not lying around
        res = self.compile_source(force=True, target=mode)
        if res.returncode != 0:
            T.error("Compilation failed")
            # Skip rest of tests for this run
            self.skipping = True
            self.compile_ok = False
        else:
            self.compile_ok = True
        # else:
            # TODO: fix easytracker which cannot do a correct reload program
            # sleep(0.3)
            # self.reload_program()
            # Don't know if it is really necessary but otherwise results where 
            # not always consistent in different test runs
            TrackerHelp.__init__(
                self,
                self.source_level_dir,
                self.exec_name
            )
            # self.print_thread.set_easytracker_output_queue(self.tracker.stdout_queue)
            # self.load_program()
            #
            ## Do start will be called in the `run` method
            # self.do_start()

    def recompile_bug(self):
        self.recompile(mode="bug")

    def recompile_answer(self):
        self.recompile(mode="answer")

    def recompile_fix(self, fix_name):
        self.recompile(mode="fix_"+fix_name)
