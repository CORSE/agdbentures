#!/usr/bin/env python3

# pylint: disable=too-many-locals fixme

"""
Conversion and parsing utils.

# C commment parsing.

C like block comments (/* ... */) or inline comments (// ...) that
starts with the tag @AGDB are recognized.

Key are case insensitive.
Keys that are not defined here will still
be added to the dictionnary of metadata (as strings).

- Mandatory strings:
    - exec_name (name of the binary that will be loaded)
    e.g: // @AGDB exec_name: main
    - map_width
    e.g: // @AGDB map_width: 8
    - map_height
    e.g: // @AGDB map_height: 10

- Mandatory lists:
    - available_commands (list of available gdb commands)
    e.g: // @AGDB available_commands: next step edit

- Non-mandatory variable declaration.

  It tells which variable to track in order to draw the game.
  It also can tell the value of some constants like the orientations.

  Syntax: `alias: key value`

  The keys are:
    - player_mode (global_variables, simple_map, map_stack)
    - player_direction (default value: player_direction)
    - map_array (default value: none)
    - player_x  (default value: player_x)
    - player_y  (default value: player_y)
    - exit_x    (default value: exit_x)
    - exit_y    (default value: exit_y)
    - up        (default value: 0)
    - down      (default value: 1)
    - left      (default value: 2)
    - right     (default value: 3)
    e.g: /* @AGDB
          * Variables alias here
          * alias: player_x my_struc.var_x
          * Constants here
          * alias: up   1
          * alias: down 2
          */

- Non-mandatory strings:
    - level_name
    e.g: // @AGDB TAG tag1 tag2
    - level_number
    - hintXXX (XXX to be filled)
    - bug

- Non-mandatory lists:
    - tag
    e.g: // @AGDB TAG tag1 tag2
    - tags
    e.g: // @AGDB TAGS tag1 tag2
    - watch (declare the variables to watch)
    e.g: // @AGDB watch: mstack my_var
    - track (watch the value of a variable at the end of a function)
    e.g: // @AGDB track: my_fun:my_var my_other_fun:my_other_var

- WOP (wise old women/men)
    Here, xxx represents the WOP's identifier.

    - message condition:
        WOPxxx: message_condition COND
        e.g: - // @AGDB WOPxxx: message_condition  player_x == 5 and player_y
             - // @AGDB WOPxxx: message_condition  near
    - visibility condition:
        WOPxxx: visibility_condition COND
        e.g: - // @AGB WOPxxx: visibility_condition  player_x == 5 and player_y
             - // @AGB WOPxxx: visibility_condition  near
    - messages:
        object_name: message MESSAGE_TAG
            This is the message contents, that will end 
            after the current line.
            EndOfMessage
        e.g: /* @AGDB
              * WOPxxx: message First line here
              * WOPxxx: message Second line here
              * WOPxxx: message and so on...
              */
    - localized messages: append the language to form a 'messageXX' tag
        e.g.:
              OJBpanel: messageFR msgtag Message en francais
              OJBpanel: messageEN msgtag English message

    - position
        WOPxxx: position X_pos Y_pos
        e.g: - // @AGDB WOPxxx: position 8 5

- OBJ
    Custom object that belongs to the map. xxx represents its identifier.

    - var_x: The variables or a constant that defines its x position.
    - var_y: The variables or a constant that defines its y position.
    - [var_dir]: The variables or a constant that defines its direction.
    - char_rep: Its character representation.
    - [visibility_condition]

    e.g:
    /* @AGDB
     * OBJdoor: var_x door_x
     * OBJdoor: var_y 5
     * OBJdoor: char_rep D
     * The following line is useless
     * OBJdoor: visibility_condition True
     */

- arcade_maps and arcade_skins
    Describes the arcade assets

    e.g:
    /* @AGDB
     * arcade_skins: player_normal path_to_the_skin
     * arcade_skins: player_fire path_to_the_other_skin
     * arcade_maps: main <path_to_the_map>
     * arcade_maps: main_offset <x_offset> <y_offset>
     * arcade_maps: other_map <path_to_the_other_map>
     */
"""

import json
import re
import subprocess
from collections import defaultdict
from typing import Optional
from os.path import dirname, split, relpath
from utils import lvl
from verif_lib import VerifyCondition, VerifyMustCall, VerifyChangeInFunction
import graphic.constants as cst
from language import Lang
from config import Config


def get_defines(filename):
    """
    Extract all the ifdef preprocessing directive name
    from a file.

    :param filename: The file to analyse.
    :return: List of defined tokens. Tokens are sorted and unique.
    """
    with subprocess.Popen(
        f"unifdef -s {filename}", shell=True, text=True, stdout=subprocess.PIPE
    ) as process:
        out, _ = process.communicate()

    tokens = out.split("\n")

    # remove duplicates by converting to a set then back
    return sorted(list(set(tokens)))


def apply_unifdef(
    in_file: str,
    out_file: str,
    defines: Optional[list[str]],
    udefines: Optional[list[str]],
) -> None:
    """
    Applies unifef command with D arguments with the defines list and U
    arguments for the udefines one #ifdef in the defines list will chose the
    then branch and in udefines the else branch. If we call this, we can force
    the removing of AGDB comments because we prepare a usable version
    """
    # Default values
    defines = defines or []
    udefines = udefines or []

    # removes @AGDB comment
    strip_comments(in_file, out_file)

    # -m: apply unifdef in place
    # -x 2: exits with status 0 whether the file was modified or not
    args = ["unifdef", "-m", "-x", "2"]
    for define in defines:
        assert define != ""
        args.append(f"-D{define}")
    for udefine in udefines:
        assert udefine != ""
        args.append(f"-U{udefine}")
    args.append(out_file)
    subprocess.run(args, check=True)


def strip_comments(file_in, file_out=None, language="FR"):
    """
    Read all the C comments that are that with @AGDB and removes them.
    Keep only comments in the current language.

    For instance:
    /* @AGDB
     * This will be match.
     */
    // @AGDB This will also match

    /* This won't match */
    // Neither this one

    This comment will be kept only when generating for french audience.
    /*FR ... */

    And this one when for an english audience:
    /*EN ... */

    If the sole comment on an inline comment, will keep/remove the line according to the language.
    Example:
        printf("Hello world!"); //EN
        printf("Bonjour tout le monde !"); //FR


    :param file_in: the file to parse
    :param file_out: the file the write the uncomment text in
    :return: list of comments (given line by line)
    """
    # Match everything between '/* @AGDB' and '*/'
    # in multiline ([\s\S]+)
    # /[*]+ [*]+/ are the escaped tags
    # [\S\s]*? captures everything in a non greedy way
    # So it will stop at the first ending tag */
    # We don't use DOTALL because it will mess up the next regex
    block_comment_re = r"^\s*/[*]+\s*@AGDB([\S\s]*?)[*]+/[ \t]*\n"

    # Match everything after '// @AGDB'
    # In a greedy way (.*) till the first EOL
    inline_comment_re = r"//\s*@AGDB\s(.*)"

    # Union of the two regexes
    comment_re = re.compile(f"{inline_comment_re}|{block_comment_re}", re.MULTILINE)

    # Regexp for language comments
    bl_st = r"/[*]"
    bl_end = r"\s+([\S\s]*?)[*]+/[ \t]*"

    block_lang_comment_keep_re = re.compile(
        bl_st + language, re.MULTILINE
    )  # replace with only '/*'
    inline_lang_comment_keep_re = re.compile(
        r"//" + language, re.MULTILINE
    )  # replace with only '//'
    inline_lang_line_keep_re = re.compile(
        r"[ \t]*//" + language + '$', re.MULTILINE
    )  # remove

    # These will need to be removed
    block_lang_comment_discard_re = "[ \t]*" + bl_st + r"[A-Z]{2}" + bl_end
    inline_lang_comment_discard_re = r"[ \t]*//[A-Z]{2} (.*)\n"
    inline_lang_line_discard_re = r"^.*//[A-Z]{2}\n"

    # Union of the languages subject to removal
    lang_remove_re = re.compile(
        f"{inline_lang_comment_discard_re}|{block_lang_comment_discard_re}|{inline_lang_line_discard_re}",
        re.MULTILINE,
    )

    # extra line returns
    line_returns = re.compile(r"\n{2,}")  # 2 or more

    with open(file_in, "r", encoding="utf-8") as file:
        file_txt = file.read()
        raw_matches = comment_re.findall(file_txt)

    # Comment-free and language processed file
    if file_out is not None:
        uncommented_file = comment_re.sub("", file_txt)

        # Now proceed to modify language if necessary

        # keep only block comments of the correct language
        keep = block_lang_comment_keep_re.sub("/*", uncommented_file)
        keep = inline_lang_line_keep_re.sub("", keep)
        keep = inline_lang_comment_keep_re.sub("//", keep)

        # now discard blocks/inline comments of other language, lines of other
        # languages, and tags of correct language
        ok_lang = lang_remove_re.sub("", keep)

        final_text = line_returns.sub("\n\n", ok_lang)

        with open(file_out, "w+", encoding="utf-8") as file:
            file.write(final_text)

    # Remove empty line and leading ' *'
    comments = []
    for inline, multi in raw_matches:
        raw_lines = []
        if inline:
            raw_lines.extend(inline.split("\n"))
        if multi:
            raw_lines.extend(multi.split("\n"))
        for line in raw_lines:
            if line.startswith(" *"):
                line = line[2:]
            line = line.strip()
            comments.append(line)  # keep empty lines for messages

    return comments


# pylint: disable=too-many-branches too-many-statements
def extract_metadata(source_file):
    """
    Generate a dictionnary that describes the file given in argument.

    :param source_file: the C file to convert
    :return: the wanted dictionnary
    """

    def str_to_list(string: str) -> list[str]:
        """
        Convert a string into a list of string. An empty strings gives an empty list
        Example:
        "next n step s" -> ["next", "n", "step", "s"]
        """
        stripped_string = string.strip()
        if stripped_string == "":
            return []
        return stripped_string.split(" ")

    def add_message(odict: dict, msg_ident: str, message_lines: [str]) -> None:
        """
        Add a message to a wop or object
        :param odict: the localized or default dictionnary to insert the message to
        """
        odict[msg_ident] = "\n".join(message_lines)

    def add_obj_info(obj_dict: dict, name: str, info: str) -> None:
        """
        Add informations to a dictionnary of either wops or objects.

        :param obj_dict: The object dictionnary. Each entry correspond
                      to a new object.
        :param name: Name of the object to update, with the 'OBJ' prefix removed.
                     For the WOP, just "wop" (need to update later if multiple wops)
        :param info: The info to add in a raw format (string).
        """
        # Getting the object index
        assert name
        # If object is missing in dictionnary, will be created thank to the
        # default dict object

        # Updating the object infos
        try:
            [key, val] = info.split(maxsplit=1)
        except ValueError:
            key = info
            val = ""

        # if key in ["var_x", "var_y", "var_dir", "char_rep", "visibility_condition"]:
        # obj_dict[obj_index][key] = val

        if key == "position":
            # set absolute position of object
            # to track position based on an inferior's variable, use 'var_x' and 'var_y'
            obj_x, obj_y = val.split()
            obj_dict[name]["x"] = int(obj_x)
            obj_dict[name]["y"] = int(obj_y)

        elif key == "message":
            # should not be handled here anymore
            assert False
        # elif key.startswith("message"):
        # if "message" not in obj_dict[obj_index]:
        # obj_dict[obj_index]["message"] = val
        # else:
        # obj_dict[obj_index]["message"] += "\n" + val

        else:
            lvl.debug(f"obj/wop info for {name}: {key}:{val}")
            obj_dict[name][key] = val

    def parse_verify(inst):
        if inst.startswith("cond "):
            _, condition = inst.split(' ', 1)
            return VerifyCondition(condition)
        if inst.startswith("change_in_function "):
            _, var_name, fun_name = inst.split(' ')
            return VerifyChangeInFunction(var_name, fun_name)
        if inst.startswith("must_call "):
            _, fun_name = inst.split(' ')
            return VerifyMustCall(fun_name)

        raise ValueError(f"Cannot parse verify instruction '{inst}'")

    comments = strip_comments(source_file)
    metadata = {
        "exec_name": "main",
        "alias": {
            "player_x": "player_x",
            "player_y": "player_y",
            "player_direction": "player_direction",
            "exit_x": "exit_x",
            "exit_y": "exit_y",
            "map_array": "none",
            # not putting default dictionary for direction anymore
            # since otherwise it would break with cst.Direction.b
            # "up": "0",
            # "down": "1",
            # "left": "2",
            # "right": "3",
        },
        "player_mode": "global_variables",
        "tags": [],
        "hints": [],
        "watch": [],
        "tracked_functions": defaultdict(list),
        "BUG_TAGS": [],
        "arcade_skins": {
            "player_main": ":characters:Male/Male 02-2.png",
            "wop_main": ":characters:Male/Male 12-1.png",
            # "wop_main": ":characters:Female/Female 19-1.png",
            "guard": ":characters:Soldier/Soldier 01-1.png",
            # "guard": ":characters:Enemy/Enemy 05-1.png",
            "cook": ":characters:Female/Female 14-1.png",
            "enemy": ":characters:Enemy/Enemy 15-1.png",
        },
        "arcade_sprites": {  # this is the old sprite that was used in earlier versions
            "exit": (
                ":tiles:[Base]BaseChip_pipo.png",
                6,
                8,
            )  # 6 and 8 are the position in the sheet
        },
        "arcade_maps": {"main": ":maps:default_map.tmx", "main_offset": (0, 0)},
        ## Checks for level validation
        # custom checks for level
        'verify': {'gen': [], 'init': [], 'always': [], 'exit': []},
        # general checks to disable
        'no_verify': {'gen': [], 'init': [], 'always': [], 'exit': []},
    }

    def add_verify(head, tail):
        if head == 'verify':
            when = 'gen'
        else:
            _, when = head.split('_', 1)
        metadata['verify'][when].append(parse_verify(tail))

    def add_no_verify(head, tail):
        if head == 'no_verify':
            when = 'gen'
        else:
            _, _, when = head.split('_', 2)
        metadata['no_verify'][when].append(parse_verify(tail))

    in_message = False  # flag to know whether we are currently within a message
    dict_message = None
    ident_message = None
    current_message_list = []  # list of paragraphs of a message
    current_message = []  # lines of current message

    def new_message_dict():
        d = dict()
        for l in Lang:
            d[l] = {}
        return d

    # Adding default values.
    # I do it at the end because the type checker screams when
    # I overwrite a None value to something else
    metadata["alias"] = defaultdict(lambda: None, metadata["alias"])
    # metadata["wops"] = defaultdict(lambda: {})
    metadata["wops"] = defaultdict(
        lambda: {
            "visibility_condition": "always",
            # "triggered": False,
            "visible": False,
            "messages": new_message_dict(),
            "name": "wop",
            "x": 0,
            "y": 0,
            "direction": "down",
        }
    )

    # print("wops meta", metadata["wops"])
    # print("wops meta", metadata["wops"]['wop'])

    metadata["objects"] = defaultdict(lambda: {"messages": new_message_dict()})
    metadata["coordinates"] = defaultdict(lambda: None)

    for comment in comments:
        # Special check if we are currently in an object/wop message
        if in_message:
            if comment == "EndOfMessage":
                in_message = False
                msg = " ".join(current_message_lines)
                current_message_list.append(msg)
                add_message(dict_message, ident_message, current_message_list)
                continue
            if comment == "":
                msg = " ".join(current_message_lines)
                current_message_list.append(msg)
                current_message_lines = []
            else:
                current_message_lines.append(comment)
            continue

        if not comment:
            continue  # empty line

        if comment[0] == '#':
            continue  # commented out line

        if ":" not in comment:
            continue  # Not a metadata

        # Metadata is in the form of "key: value"
        [head, tail] = comment.split(":", 1)
        head = head.strip()
        tail = tail.strip()

        # Integer values
        if head in ['level_number', 'map_height', 'map_width']:
            metadata[head] = int(tail)
        # List values
        elif head in [
            'available_commands',
            'new_commands',
            'new_commands_during_level',
            'Tags',
            'coordinates',
            'hide_commands',
        ]:
            metadata[head] = str_to_list(tail)
        elif head == 'objects':
            # first way of declaring objects: in a list, without any attributes
            for objname in str_to_list(tail):
                metadata['objects'][objname] = {}
        # Aliased variables
        elif head == 'alias':
            key, val = tail.split(maxsplit=1)
            metadata['alias'][key] = val
        # Variables to watch
        elif head == 'watch':
            metadata['watch'] += str_to_list(tail)
        # Variables to track
        elif head == "track":
            for key in str_to_list(tail):
                func, var = key.split(":", 1)
                metadata['tracked_functions'][func].append(var)
        # Tags
        elif head in ['TAGS', 'TAG']:
            metadata["tags"] += str_to_list(tail)
        # engine_tags, engine_antitags
        elif head.endswith('tags'):
            if head not in metadata:
                metadata[head] = []
            metadata[head] += str_to_list(tail)
        # Raw hints
        elif head.startswith("HINT"):
            metadata["hints"].append(tail)
        # WOP
        elif head.startswith("WOP") or head.startswith("OBJ"):
            ident = head[3:]
            is_wop = head[0] == 'W'

            if is_wop and not ident:
                ident = "wop"

            if tail.startswith("message"):
                try:
                    key, ident_message = tail.split()
                except ValueError as e:
                    lvl.error(f"Too many parts while trying to parse message: {tail}") 
                    raise e

                strlang = key[7:]  # get last part, EN or FR, if any

                if not strlang:
                    lang = Lang.default
                else:
                    lang = Lang[strlang]

                if is_wop:
                    dict_message = metadata["wops"][ident]['messages'][lang]
                else:
                    dict_message = metadata["objects"][ident]['messages'][lang]
                in_message = True
                current_message_lines = []
                current_message_list = []
            else:
                if is_wop:
                    add_obj_info(metadata['wops'], ident, tail)
                else:
                    add_obj_info(metadata['objects'], ident, tail)

        # ifdefs tags
        elif head == "BUG_TAGS":
            metadata[head] += str_to_list(tail)
        # arcade assets
        elif head in ["arcade_skins", "arcade_maps"]:
            key, value = tail.split(maxsplit=1)
            if key.endswith("offset"):
                strx, stry = value.split(maxsplit=1)
                metadata[head][key] = (int(strx), int(stry))
            else:
                metadata[head][key] = value
        elif head.startswith('verify'):
            add_verify(head, tail)
        elif head.startswith('no_verify'):
            add_no_verify(head, tail)
        # String keys and string values
        else:
            metadata[head] = tail

    level_path = dirname(source_file)
    rel_path = relpath(level_path, Config.ROOT_DIR)
    assert rel_path.startswith("levels/")
    level_name = relpath(rel_path, "levels")

    assert level_name
    metadata["level_path"] = level_path
    metadata["level_name"] = level_name

    if not metadata["exec_name"]:
        lvl.error("The 'exec_name' metadata field cannot be empty")
        exit(1)

    return defaultdict(lambda: None, metadata)


def dict_to_json(metadata, json_filename, sort_keys=True, indent=2):
    """
    Dump a dictionnary into a json file.

    :param metadata: the dictionnary to dump
    :param json_filename: the name of the wanted json file
    :param sort_keys: tells if the json file will be sorted by keys
    :param indent: the indent of the json file
    :return: the name of the json file
    """
    # Using the right extension
    filename = json_filename
    if not filename.endswith(".json"):
        filename += ".json"

    with open(filename, "w+", encoding="utf-8") as dest_file:
        json.dump(metadata, dest_file, sort_keys=sort_keys, indent=indent)

    return filename


def json_to_dict(json_filename):
    """
    Convert a json file into a dictionnary.

    :param json_filename: the name of the json file
    :return: the dictionnary that describes the json file
    """
    with open(json_filename, "r+", encoding="utf-8") as file:
        json_obj = json.load(file)

    return json_obj
