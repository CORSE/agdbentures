"""
Model of the "world" of the inferior.
This is where we know how to update the state of the level from the inferior's memory.
"""

from abc import ABC, abstractmethod
from queue import Queue
from copy import deepcopy
from typing import Optional, Union
from collections import defaultdict
from enum import IntEnum, IntFlag, StrEnum, auto

from logs import lvl
from level.objects import Object, Player, Wop, Coordinates
from level.action import Action


class EntityChar(StrEnum):
    """
    Base chars before entity structs
    """
    EMPTY = ' '
    GROUND = '.'
    WALL = '#'
    WATER = '~'
    DOOR = 'D'
    PLAYER = '>'
    EXIT = '@'
    PATH = '+'


def floor_to_char(f):
    return [
        EntityChar.EMPTY,
        EntityChar.GROUND,
        EntityChar.WALL,
        EntityChar.WATER,
        EntityChar.DOOR,
        EntityChar.EXIT,
        EntityChar.PATH,
        'w',
        'g',
        'h',
    ][f]

def char_to_floor(c):
    try:
        return {
            EntityChar.EMPTY: Floor.F_EMPTY,
            EntityChar.GROUND: Floor.F_GROUND,
            EntityChar.WALL: Floor.F_WALL,
            EntityChar.WATER: Floor.F_WATER,
            EntityChar.DOOR: Floor.F_DOOR,
            EntityChar.EXIT: Floor.F_TARGET,
            EntityChar.PATH: Floor.F_GROUND,  # special case for Tutorial, to check stairs
            'w': Floor.AREA_WHITE,
            'g': Floor.AREA_GRAY,
            'h': Floor.AREA_HASHED,
            EntityChar.PLAYER: Floor.F_EMPTY,  # player is handled differently
        }[c]
    except KeyError:
        lvl.warning(f"Unknown character '{c}' for floor")
        return Floor.F_EMPTY

class Floor(IntEnum):
    F_EMPTY = 0
    F_GROUND = auto()
    F_WALL = auto()
    F_WATER = auto()
    F_DOOR = auto()
    F_TARGET = auto()
    AREA_WHITE = auto()
    AREA_GRAY = auto()
    AREA_HASHED = auto()
    FLOORTYPE_N = auto()

    __str__ = floor_to_char

class Category(IntEnum):
    C_NONE = 0
    C_MONSTER = auto()
    C_NPC = auto()
    C_ITEM = auto()
    C_PLAYER = auto()
    C_FLAG = auto()
    C_EXIT = auto()

class Property(IntFlag):
    """
    These are powers of 2, we can do bitwise operations on them.
    """
    ACTIVABLE = auto()
    PICKABLE = auto()
    PUSHABLE = auto()
    TRAP = auto()
    OBSTACLE = auto()

# _TILES_DICT = {
    # "EMPTY": " ",
    # "WATER": "~",
    # "DOOR": "D",
    # "WALL": "#",
    # "PATH": "+",  # a particular path to follow (useful for validation)
# }


###############
#
# Utility functions to search memory
#
###############


def valid_pointer(varptr):
    """
    Pointer returned by easytracker is valid and points to meaningful data
    """
    return varptr is not None and varptr != (None,)


def valid_uni_pointer(varptr):
    """
    Is valid pointer and points to only one item (not an array):
    """
    return varptr is not None and varptr != (None,) and len(varptr) == 1


def find_main_frame(frames):
    if frames[0].name != "main":
        print("Warning stack with not a main frame")
    return frames[0]


def find_var_in_context(context, variable):
    access = None

    if type(variable) == tuple:
        # array / structure access
        name, access = variable
    else:
        name = variable
    if name not in context:
        return None

    if access is None:
        return context[name].value

    contents = context[name].value

    if not valid_pointer(contents):
        return None

    if type(access) == int:
        # array access
        return contents[access]
    if type(access) == str:
        try:
            if access.startswith('->'):
                # field access in a ptr to struct
                contents = contents[0]
                access = '.' + access[2:]
            res = eval("contents" + access)
            return res
        except IndexError:
            lvl.warning(f"Index error when trying to get {variable}")
            return None
        except AttributeError:
            lvl.warning(f"Field not found when trying to get {variable}")
            return None

    raise NotImplementedError(f"Access problem: {variable}")


def find_var_in_frame(frame, name):
    return find_var_in_context(frame.variables, name)


def find_var_in_frames(frames, name):
    for f in reversed(frames):
        var = find_var_in_frame(f, name)
        if var is not None:
            return var
    return None


def find_in_global(memory, name):
    globs = memory["global_variables"]
    return find_var_in_context(globs, name)


def frames_then_global(memory, name):
    frames = memory["stack"]
    var = find_var_in_frames(frames, name)
    if var is not None:
        return var
    return find_in_global(memory, name)


def get_top_map(frames):
    # find a frame with "game" variable (gameinstance* type)
    game = find_var_in_frames(frames, "game")[0]
    # if the game is not initialized we do nothing
    if game is None:
        return None

    # fetch top map
    map_stack = game.map_stack[0]
    return map_stack.maps[map_stack.length - 1][0]


def find_map_in_game(frames):
    game = find_var_in_frames(frames, "game")[0]
    if game is not None:
        return game.map[0]

    return None


def find_map(frames):
    return find_var_in_frames(frames, "current_map")[0]


def find_char(map, char):
    """
    Ugly find in linearized map. Will need to be updated when easytracker gives
    us a real string to work with, instead of a list of values retrieved from
    gdb's console.
    """
    look_for = "'" + char + "'"
    if map is not None:
        for idx, cont in enumerate(map.tiles):
            if look_for in cont:
                return (idx % map.width), (idx // map.width)

    # old code for 2D array
    # if map is not None:
    # for y, line in enumerate(map.tiles):
    # for x, val in enumerate(line):
    # if char in val:
    # return x, y
    return None, None


def map_linear_to_matrix(map):
    lvl.warning("deprecated, try to use `new_floor_from_current_map` instead")
    width, height = map.width, map.height
    map.tiles = [map.tiles[i : i + width] for i in range(0, width * height, width)]



def get_coords_from_variables(memory, var_x, var_y):
    coord_x = frames_then_global(memory, var_x) if var_x else None
    coord_y = frames_then_global(memory, var_y) if var_y else None
    return coord_x, coord_y


def get_value_from_variable(memory, var):
    return frames_then_global(memory, var)


def update_object_pos(obj, memory, var_x, var_y):
    coord_x, coord_y = get_coords_from_variables(memory, var_x, var_y)
    obj.set_position(coord_x, coord_y)
    obj.notify_update(memory)


def update_object_pos_and_dir(obj, memory, var_x, var_y, var_dir):
    coord_x, coord_y = get_coords_from_variables(memory, var_x, var_y)
    direction_idx = get_value_from_variable(memory, var_dir) if var_dir else None
    obj.set_position(coord_x, coord_y)
    obj.set_direction(direction_idx)
    obj.notify_update(memory)


class Map:
    """
    A map in the world
    During tutorial, there is no map so it is just used to hold the objects.
    With engines, we will store here the state of the map.
    """

    def __init__(
        self,
        name: str,
        width: int = 1,
        height: int = 1,
        # tiles_dict: Optional[dict[str, str]] = None,
        # fallback_tile: str = ' ',
    ):
        """
        Map initialization.

        :param width: The map width.
        :param height: The map height.
        :param tiles_dict: The dictionnary that give the string mapping
        of the enums.
        :param fallback_tile: Tile to use if the value is not defined.
        """
        self.name = name
        self.init_width = width
        self.init_height = height
        self.current_map: str | None = None  # The name of the current map
        self.tiles: None | list(list(Floor)) = None  # Will be a 2D array
        # if tiles_dict is None:
            # self.tilesdict = _TILES_DICT
        # else:
            # self.tilesdict = tiles_dict
        # self.fallback_tile = fallback_tile
        self.unnamed_objects: set[Object] = set()
        self.named_objects: dict[str, Object] = {}
        self.coordinates: dict[str, Object] = {}
        # We do not call set_reset now, as it will be
        # done by the initial set_reset of the world
        # self.set_reset()

    def __repr__(self):
        return (
            f"Map({self.name}, {self.width}x{self.height}, tiles:\n"
            + '|\n'.join([''.join(map(str, l)) for l in self.tiles])
            + "|\n)"
        )

    def set_reset(self):
        self.set_size(self.init_width, self.init_height)
        for o in self.unnamed_objects:
            o.set_reset()
        for o in self.named_objects.values():
            o.set_reset()

    def set_size(self, width: int, height: int):
        if width is None:
            width = 0
        if height is None:
            height = 0
        self.width = width
        self.height = height
        self.tiles = [[Floor.F_EMPTY] * self.width for _ in range(self.height)]

    def set_floor(self, strmap):
        """
        set floor tiles from a given 2D array of char
        """
        for r in range(min(self.height, len(strmap))):
            for c in range(min(self.width, len(strmap[0]))):
                self.tiles[r][c] = char_to_floor(strmap[r][c])

    def enum_to_char(self, enum: str) -> str:
        """
        Convert an enum to a char (tile of the map).
        Override this method to have a custom tiles

        :param enum: The enum to convert.
        """
        raise NotImplementedError
        if len(enum) == 1:  # It is not an enum
            return enum  # Nothing to do
        # Maybe using defaultdict?
        if enum in self.tilesdict:
            return self.tilesdict[enum]
        return self.fallback_tile

    def get_ascii_map(self) -> list[list[str]]:
        """
        Return a list that represents the map.

        :return: The ascii map with its objects.
        """
        ascii_map = deepcopy(self.tiles)

        # Adding unnamed_objects
        for obj in self.unnamed_objects:
            if obj.visible:
                ascii_map[obj.coord_y][obj.coord_x] = obj.char_rep  # type: ignore

        # Adding named objects
        for name, obj in self.named_objects.items():
            if obj.visible and name not in ["player", "exit"]:
                ascii_map[obj.coord_y][obj.coord_x] = obj.char_rep  # type: ignore

        # Adding the player and the exit
        for obj in [self.exit, self.player]:
            if obj and obj.visible:
                ascii_map[obj.coord_y][obj.coord_x] = obj.char_rep  # type: ignore

        return ascii_map


# pylint: disable=too-many-instance-attributes
class WorldABC:
    """
    World class.
    Is intended to be a reconstruction of the world of the level
    based on the inferior's data structure, e.g., the map, the player position, etc.
    This is the abstract class, from which worlds corresponding to each engine
    will derive.

    During tutorial, there is no map so it is just used to hold the objects.
    With engines, we will store here the state of the map, and issue updates
    when there are differences at the next easytracker pause.
    """

    def __init__(
        self,
        level,  # AbstractLevel, not putting type because of circular dependencies
        out_queue: Queue | None = None,
    ):
        """
        World initialization.

        :param level: the level that instanciated this World, so we have access to the tracker
        :param out_queue: the queue where to put update information.
        """
        self.level = level
        self.out_queue = out_queue
        self.current_map: str | None = "main"  # The name of the current map
        self.player: Union[Player, None] = None
        self.outside_objects: dict[str, Object] = {}  # objects added by agdbentures. They are not found in the inferior.
        self.maps: Dict[Map] = {}
        self.memory = None

        # By default, walls are hidden and shown only when level requests it
        self.show_walls_reset = False

    def __repr__(self):
        return f"{self.__class__.__name__}(level={self.level}, current_map={self.current_map}, player={self.player}, maps={self.maps})"

    ##############
    #
    # Functions to inspect the underlying world, i.e., the inferior's memory.
    #
    ##############

    def set_memory(self, memory):
        self.memory = memory

    def get_variable(self, var: str):
        return frames_then_global(self.memory, var)

    def get_main_variable(self, var: str):
        frames = self.memory["stack"]
        return find_var_in_frame(find_main_frame(frames), "i")

    def get_pointer_address(self, var: str):
        o = self.level.tracker.get_variable_value(var, as_raw_python_objects=False)
        return o.value.content.address

    #############
    #
    # Fonctions that handles map, objects, and updates
    #
    #############

    def send_update(self, payload: dict | None = None) -> None:
        """
        Send update informations to hook functions.

        The argument given to the hooks is a communication payload.
        """

        if self.out_queue is not None:
            self.out_queue.put_nowait(payload)
        else:
            lvl.warning("Cannot send payload")

    def add_map(self, name: str, width: int, height: int) -> None:
        self.maps[name] = Map(name=name, width=width, height=height)

    def add_object(self, map_name: str, obj: Object) -> None:
        """
        Add an object to the world.

        :param obj: The object to add.
        """
        if obj.name is None:
            self.maps[map_name].unnamed_objects.add(obj)
        else:
            self.maps[map_name].named_objects[obj.name] = obj

    def get_objects(self):
        """
        Get the list of the named objects on the current map
        """
        return self.maps[self.current_map].named_objects

    def get_object(self, name: str):
        return self.maps[self.current_map].named_objects[name]

    def get_player(self) -> Player:
        return self.player

    def get_wop(self):
        return self.outside_objects["wop"]

    def get_outside_object(self, name: str):
        return self.outside_objects[name]

    def get_coordinates(self, name: str):
        return self.maps[self.current_map].coordinates[name]

    def add_coordinates(self, map_name: str, coord: Coordinates) -> None:
        """
        Adds coordinates corresponding to a position on the map.
        """
        assert coord.name
        self.maps[map_name].coordinates[coord.name] = coord

    def add_player(self, player: Player) -> None:
        """Add a player to the world."""
        self.player = player

    def add_exit(self, exit_obj: Object) -> None:
        """Add an exit to the world."""
        # we do not know where the object is, generally
        self.exit = exit_obj

    def add_outside_object(self, obj):
        self.outside_objects[obj.name] = obj

    def set_reset(self):
        """
        Setting or resetting particular things, e.g. player, objects...
        """
        self.player.set_reset()
        for m in self.maps.values():
            m.set_reset()

    @abstractmethod
    def update_map(self) -> None:
        """
        Update the map from inferior's memory
        The way to do it depends on the engine.
        So we need a different class for each engine.
        """

    @abstractmethod
    def update_objects(self):
        """
        How to update the objects on the current map.
        Might be empty if update_map above handles everything.
        """

    def update(self, memory: dict):
        """
        Update the current world: the map as well as all objects
        which should be on the map.
        Can be redefined for particular engines.
        """
        self.memory = memory
        self.update_map()
        self.update_objects()


class WorldTutorial(WorldABC):
    """
    How to update the world in the tutorial levels.
    """

    def __init__(self, level, out_queue):
        super().__init__(level, out_queue)
        self.show_walls_reset = True

    def add_player(self, player: Player) -> None:
        """Add a player to the world."""
        super().add_player(player)
        player.var_x = "player_x"
        player.var_y = "player_y"
        player.var_dir = "player_direction"
        self.add_object("main", player)

    def add_exit(self, exit_obj):
        """In the tutorial, the exit is always on the main map"""
        super().add_exit(exit_obj)
        exit_obj.var_x = "exit_x"
        exit_obj.var_y = "exit_y"
        self.add_object("main", exit_obj)

    def update_object(self, obj) -> None:
        """
        Will update objects only if their var_x and var_y is not None.
        This is also useful for later engines, where sometimes objects only appear
        on the map, but sometimes we also have a dedicated structure for them.
        """
        if obj.var_dir is not None:
            update_object_pos_and_dir(
                obj, self.memory, obj.var_x, obj.var_y, obj.var_dir
            )
        else:
            update_object_pos(obj, self.memory, obj.var_x, obj.var_y)

    def update_objects(self) -> None:
        lvl.debug(f"Updating level objects for {self.current_map=} ")
        for obj in self.maps[self.current_map].unnamed_objects:
            self.update_object(obj)
        for obj in self.maps[self.current_map].named_objects.values():
            self.update_object(obj)

        # Some objects are added to the world by agdbentures
        # These objects are allowed to have an update function
        # Wod is not actually updated from inferior's memory.
        # Maybe there are some triggers here???
        for obj in self.outside_objects.values():
            obj.update()

    def update_map(self) -> None:
        """
        Nothing to do in the tutorial
        """
        pass

    def set_reset(self) -> None:
        """
        Walls are shown by default in the tutorial.
        """
        super().set_reset()
        if self.show_walls_reset:
            pl = {"topic": 'sprites', 'action': Action('show'), 'layer': 'walls'}
            self.send_update(pl)


class WorldSimpleMap(WorldTutorial):
    """
    World for the simple_map engine

    Note: we keep the update object of the tutorial for objects
    which have their own var_x and var_y
    """

    def __init__(self, level, out_queue):
        super().__init__(level, out_queue)
        self.show_walls_reset = True

        # fields in the current_map structure
        self.expected_cm_fields = [
            'player_x',
            'player_y',
            'player_direction',
            'height',
            'width',
            'tiles',
        ]

    def add_exit(self, exit_obj):
        """
        In simple_map and later, the exit is determined by the position
        of the '@' in the map.
        Do not link exit to any "exit_x" or "exit_y" variable.
        """
        WorldABC.add_exit(self, exit_obj)
        # self.add_object("main", exit_obj)

    def update_map(self) -> None:
        """
        Update the map and objects from inferior's memory
        """
        memory = self.memory
        cmptr = frames_then_global(memory, "current_map")

        if not valid_uni_pointer(cmptr):
            # Not yet initialized, do not update
            return

        # getting the actual map, i.e., the first value pointed to by map ptr
        cm = cmptr[0]
        self.update_map_from_current_map(cm)


    def new_floor_from_current_map(self, cm):

        def gdb_char_to_floor(st):
            try:
                _, ch, _ = st.split("'")
                return char_to_floor(ch)
            except ValueError:
                lvl.warning(f"Cannot decode gdb's output for char {st}")
                return Floor.F_EMPTY

        if not cm.tiles:
            lvl.error("Cannot update floor, current map tiles not found")
            return None

        width, height = cm.width, cm.height
        if len(cm.tiles) > width*height:
            lvl.warning("Too much information in map string")
        tiles = [list(map(gdb_char_to_floor, cm.tiles[i : i + width])) for i in range(0, width * height, width)]
        return tiles


    def update_map_from_current_map(self, current_map) -> None:
        """
        Simple Map has the current map in a structure containing:
            - width
            - height
            - tiles a pointer to a linearized string
        """
        cm = current_map

        for field in self.expected_cm_fields:
            if not hasattr(cm, field):
                lvl.warning(f"Cannot find `{field}` in current map structure")
                setattr(cm, field, None)

        ## First, update player position & direction
        self.update_player(cm)

        self.remove_entities = defaultdict(lambda: [])
        self.add_entities = defaultdict(lambda: [])

        map = self.maps[self.current_map]

        if cm.height != map.height or cm.width != map.width:
            if cm.height != map.height:
                lvl.warning(f"Map height changed from {map.height} to {cm.height}")
            if cm.width != map.width:
                lvl.warning(f"Map width changed from {map.width} to {cm.width}")

            for r in range(map.height):
                for c in range(map.width):
                    if map.tiles[r][c] != Floor.F_EMPTY:
                        self.remove_entities[map.tiles[r][c]].append((c, r))

            # Clearing current map, and re-reading everything
            map.set_size(cm.width, cm.height)

        new_tiles = self.new_floor_from_current_map(cm)
        if new_tiles is None:
            lvl.error("Cannot update map state")
            return
        self.update_floor(new_tiles)
        self.send_floor_update()


    def update_floor(self, new_tiles):

        map = self.maps[self.current_map]
        for r in range(map.height):
            for c in range(map.width):
                newt = new_tiles[r][c]
                oldt = map.tiles[r][c]
                if oldt != newt:
                    lvl.debug(
                        f"Character changed from {oldt} to {newt} at {c}x{r}"
                    )
                    if oldt != Floor.F_EMPTY:
                        self.remove_entities[oldt].append((c, r))
                    if newt != Floor.F_EMPTY:
                        self.add_entities[newt].append((c, r))
                    map.tiles[r][c] = newt


    def send_floor_update(self):
        # Now handle update of entities
        # remove first, as if the width/height have changed, we must
        # first clear all walls, then maybe add some walls
        if Floor.F_WALL in self.remove_entities:
            pl = {
                "topic": 'sprites',
                'action': Action('hide'),
                'layer': 'walls',
                'locations': self.remove_entities[Floor.F_WALL],
            }
            self.send_update(pl)

        if Floor.F_WALL in self.add_entities:
            pl = {
                "topic": 'sprites',
                'action': Action('show'),
                'layer': 'walls',
                'locations': self.add_entities[Floor.F_WALL],
            }
            self.send_update(pl)

        if Floor.F_TARGET in self.add_entities:
            assert len(self.add_entities[Floor.F_TARGET]) == 1
            assert len(self.remove_entities[Floor.F_TARGET]) <= 1

            map = self.maps[self.current_map]
            if 'exit' not in map.named_objects:
                ex = self.exit
                self.add_object(map.name, ex)
            else:
                ex = map.named_objects['exit']

            ex.place_at(*(self.add_entities[Floor.F_TARGET][0]))


    def update_player(self, cm):
        """
        Update player position/dir from current_map
        """
        x = cm.player_x
        y = cm.player_y
        self.player.set_position(x, y)
        d = cm.player_direction
        self.player.set_direction(d)
        self.player.notify_update(self.memory)



class WorldMapStack(WorldSimpleMap):
    """
    In map_stack, there is no "current_map" variable anymore, we need to find
    the current map at the top of the map stack in the game_instance `game` variable.
    """

    def __init__(self, level, out_queue):
        super().__init__(level, out_queue)
        self.expected_cm_fields.append('name')

    def update_map(self) -> None:
        memory = self.memory
        game_instance_ptr = frames_then_global(memory, "game")

        if not valid_uni_pointer(game_instance_ptr):
            return

        # getting the game instance, i.e., the first value pointed to by game ptr
        game = game_instance_ptr[0]

        if not valid_uni_pointer(game.map_stack):
            return
        ms = game.map_stack[0]
        if not valid_pointer(ms.maps):
            return

        top = ms.length

        if top >= len(ms.maps):
            return

        cmptr = ms.maps[top - 1]
        if not valid_uni_pointer(cmptr):
            return
        self.update_map_from_current_map(cmptr[0])


class WorldEntityStruct(WorldMapStack):
    """
    Map is now separated into
    - floor for immovable tiles
    - tiles for entities (objects/chars/etc.)
    """

    def __init__(self, level, out_queue):
        super().__init__(level, out_queue)
        self.expected_cm_fields.append('entities')

    def tile_to_floor(self, t):
        return Floor[t]

    def new_floor_from_current_map(self, cm):
        width, height = cm.width, cm.height
        if len(cm.tiles) > width*height:
            lvl.warning("Too much information in map string")

        try:
            tiles = [list(map(self.tile_to_floor, row)) for row in cm.tiles]
            return tiles
        except KeyError as e:
            lvl.error(f"Unknown floor type in current map: {e.args[0]}")
            return None

class WorldCustomEntity(WorldEntityStruct):
    """
    Entites have categories.
    """
    pass


class WorldEntityProperties(WorldCustomEntity):
    """
    Tiles in floor is not just a floor_type enum but have
    a structure.
    """
    def tile_to_floor(self, t):
        return Floor[t.floor]


class WorldMapGraph(WorldEntityProperties):
    """
    In map_graph, we still have the stack, but also a current_map pointer
    in the game_instance pointed by `game`.
    """

    def update_map(self) -> None:
        memory = self.memory
        game_instance_ptr = frames_then_global(memory, "game")

        if not valid_uni_pointer(game_instance_ptr):
            return
        game = game_instance_ptr[0]

        if not valid_uni_pointer(game.current_map):
            return
        cm = game.current_map[0]
        self.update_map_from_current_map(cm)


def get_world_cls(metadata):
    """
    Return the "correct" world class depending on the metadata for a level.
    Will select from different 'engine' versions.
    """
    assert 'engine_name' in metadata

    match metadata['engine_name']:
        case None | "none":
            return WorldTutorial
        case (
            "simple_map"
            | "level_end"
            | "input_command"
            | "levers"
            # | "config_file"  # disabled in engine-evolution.org
            | "read_map_str"
            | "command_args"
        ):
            return WorldSimpleMap

        case "map_stack":
            return WorldMapStack

        case (
            "entity_struct"
            | "inventory_array"
            | "custom_entity"
        ):
            return WorldEntityStruct

        case (
            "entity_properties"
            | "map_entity_stacking"
        ):
            return WorldEntityProperties

        case "map_graph" | "events" | "event_args":
            return WorldMapGraph

        case _:
            lvl.error(
                f"TODO: think of map changes for engine ({self.metadata['engine_name']})"
            )
            raise NotImplementedError
