

class Engine:
    current_engine: str = None
    player_mode: str = None

    def set_current_engine(self, engine_name):
        self.current_engine = engine_name
        if not engine_name or engine_name == "none":
            self.player_mode = "global_variables"
            return
        engine_version_map = [
            "simple_map",
            "input_command",
        ]
        engine_version_game = [
            "level_end",
            "levers",
            "read_map_str",
            "command_args",
        ]
        engine_version_stack = [
            "map_stack",
            "entity_struct",
            "inventory_array",
            "custom_entity",
            "entity_properties",
            "map_entity_stacking",
            "map_graph",
            "events",
            "read_map_tmx",
            "event_args",
        ]
        if engine_name in engine_version_game:
            self.player_mode = "simple_game"
        elif engine_name in engine_version_map:
            self.player_mode = "simple_map"
        elif engine_name in engine_version_stack:
            self.player_mode = "map_stack"
        else:
            print(f"la variable {engine_name=} n'est ps implémentée")
            raise NotImplementedError