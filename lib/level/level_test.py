"""
Level class specialization to test levels.
"""

from os import system
import threading
from time import sleep
from subprocess import call
import traceback

from easytracker import PauseReasonType
from tracker_helper import TrackerHelp, TimeoutCallback
from level.level_text import LevelText
import tests.lib_test as T
from logs import tst


class LevelTest(LevelText):
    """Level for testing, based on the LevelText (not Arcade) class."""

    error = None # whether the last gdb command resulted in an error or not
    last_stdout: str = ""
    last_agdb_message: str = ""

    def post_first_start(self):
        """
        Called just after the first 'start' is sent to gdb.
        """
        super().post_first_start()
        self.custom_first_start()

    def reset(self):
        T.clear_cmds()
        super().set_reset()

    def print_stdout(self) -> None:
        """Prints the inferior stdout."""
        self.last_stdout = self.tracker.get_gdb_console_output()
        if self.last_stdout:
            tst.info(f"stdout:\n{self.last_stdout}")

    def print_wop_msg(self) -> None:
        """Print the WOP message."""
        tst.warning("Should not call `print_wop_msg` anymore")

        for wop_id, message_list in self.get_wop_message().items():
            wop_message = "\n".join(message_list)
            tst.debug(f"WOP{wop_id} says:\n{wop_message}")
            self.last_agdb_message = wop_message

    def info(self, msg) -> None:
        """Override info system to keep messages."""
        self.last_agdb_message = msg
        tst.info(f"Info message: {msg}")

    def read_cmd(self):
        return T.read_cmd()

    def level_compiled(self):
        if not hasattr(self, "compile_ok"):
            T.error("Level has not called any of the `self.recompile*` variants")
            return
        return self.compile_ok

    def run(self) -> None:
        """Run in test mode."""
        if not self.level_compiled():
            return

        T.print_info(f"Testing level {self.level_name}")

        # Install a default timeout of 5 seconds
        timer = TimeoutCallback(
            self.tracker, 5.0, self.interrupt_inferior_if_running
        )
        timer.start()



        self.skipping = False
        self.signal_handled = False
        self.do_start()

        if not self.tracker:
            T.error("Cannot start tracker")
            return

        self.print_stdout()

        self.pause_type = PauseReasonType.START # mimick level_arcade

        while self.pause_type != PauseReasonType.EXITED:
            # Fetch variables
            try:
                # Update all state
                # Note that show_next_line will send to NoServerWindow, i.e., 
                # will be ignored.
                # It the future, maybe send to mock ServerWindow to to checks?
                self.update_all()
            except Exception as e:
                T.error("Cannot update status in tracker")
                T.print_error(e)
                print(traceback.format_exc())
                return

            # Print inferior's output
            self.print_stdout()

            # Show the map
            ## TODO: maybe reactivate later
            # self.show_map()

            ## Useless to call this function now
            # self.print_wop_msg()

            # Run next command in test
            if T.has_cmd():
                self.apply_command(in_loop=True)
            else:
                break

            self.pause_type = self.tracker.pause_reason.type

        # By security, disable global timeout, otherwise, might interrupt
        # subsequent runs if they use the same tracker...
        timer.cancel()

        self.print_stdout()

        # apply all remaining commands
        while T.has_cmd():
            self.apply_command(in_loop=False)

        # apply a dummy command to check last command did not launch an error
        self.apply_command(in_loop=False, dummy=True)

    def apply_command(self, in_loop, dummy=False):
        if dummy:
            # Nothing to do, this is not a real command
            cmd = T._Cmd.dummy
            cmdstr = "dummy"
            args = []
            kwargs = {}
        else:
            cmd, cmdstr, args, kwargs = self.read_cmd()
        tst.info(f"Next command: {cmdstr} {args} {kwargs} {cmd} {in_loop=}")
        tst.info(f"Next line_no/source: {self.tracker.next_source_file}:{self.tracker.next_lineno}")

        if self.skipping:
            if dummy:
                return
            T.skipped(cmdstr)
            return

        # First thing to do is check if program was interrupted, and if so,
        # if that was expected (e.g., in an infinite loop)
        if self.pause_type == PauseReasonType.SIGNAL and not self.signal_handled:
            self.signal_handled = True
            signal = self.tracker.pause_reason.args
            if 'SIGINT' not in signal:
                if cmd == T._Cmd.expect_segfault and self.tracker.pause_reason.type == 3:
                    T.passed("Segfault found as expected")
                else:
                    T.error(f"Inferior received signal {signal} ({self.tracker.pause_reason})")
                self.skipping = True
                return

            if cmd == T._Cmd.expect_infinite_loop:
                T.passed(f"Infinite loop interrupted with {self.tracker.pause_reason}")
                # Not skipping the rest of the tests now, because
                # we set the `self.signal_handled` flag so other tests are
                # possible after this one.
                # self.skipping = True
            elif cmd == T._Cmd.unexpect_infinite_loop:
                T.error(f"Inferior timeout while explicitely no infinite loop were expected")
            else:
                T.error(f"Inferior timeout (unexpected infinite loop?)")
                self.skipping = True
            return

        # If program was not interrupted, check for the infinite loop tests
        if cmd == T._Cmd.expect_infinite_loop:
            # Inferior did not receive SIGINT signal
            T.error(f"Should have been an infinite loop but tracker stopped for another reason: {self.tracker.pause_reason}")
            return

        if cmd == T._Cmd.unexpect_infinite_loop:
            # Inferior did not receive SIGINT signal, as expected
            T.passed(f"No infinite loop found as expected")
            return

        # Check if command shoud be sent to gdb via the level interface
        if cmd in T.gdb_exec_commands:
            if not in_loop:
                # We have a gdb command that manipulate execution (such as 
                # `next`), but the execution in finished.
                T.error(f"Test exited but command found: {cmd} ({cmdstr})")
                return
            if cmd == T._Cmd.command:
                cmdstr = " ".join(map(str, args))
            else:
                if args:
                    cmdstr += " " + " ".join(map(str, args))
            try:
                # reinitialize signal handling
                self.signal_handled = False
                self.error = None
                for opt in kwargs:
                    if opt == 'timeout':
                        check_loop = TimeoutCallback(
                            self.tracker, kwargs['timeout'], self.interrupt_inferior_if_running
                        )
                        check_loop.start()
                        continue
                    raise NotImplementedError(f"Unkown option '{opt}' for command {cmdstr}")
                self.parse_and_eval(cmdstr)
            except Exception as e:
                tst.critical(f"Test raised error {e} when trying command {cmd}")
                tst.critical(traceback.format_exc())
                self.error = e
            return

        if cmd == T._Cmd.expect_error or cmd == T._Cmd.unexpect_error:
            if cmd == T._Cmd.expect_error:
                if self.error is not None:
                    T.passed("Error found as expected")
                else:
                    T.error("Should have made an error but error not found")
            else:
                if self.error is not None:
                    T.error("Error found but it should not have")
                else:
                    T.passed("No error found, as expected")
            return

        # Regular commands. First check if previous command has resulted in an 
        # error
        if self.error is not None:
            T.error(f"Unexpected error {self.error}")

        # Check for defeat or victory
        if cmd == T._Cmd.expect_defeat or cmd == T._Cmd.expect_victory:
            if cmd == T._Cmd.expect_defeat:
                code_expect = 1
                name_expect = "Defeat"
            else:
                code_expect = 0
                name_expect = "Victory"

            if in_loop:
                T.error(f"{name_expect} expected but level is not finished.")
            else:
                code = self.tracker.exit_code
                if code == code_expect:
                    T.passed(f"{name_expect} found as expected")
                else:
                    T.error(f"{name_expect} expected, but exit code is {code}")

        elif cmd == T._Cmd.expect_sub_strings:
            T.verify_expect_sub_strings(
                self.last_stdout, args, "stdout", should_be_present=True
            )

        elif cmd == T._Cmd.unexpect_sub_strings:
            T.verify_expect_sub_strings(
                self.last_stdout, args, "stdout", should_be_present=False
            )

        elif cmd == T._Cmd.expect_agdb_string:
            T.verify_expect_string(
                self.last_agdb_message, args, "agdbout", should_be_equal=True
            )

        elif cmd == T._Cmd.unexpect_agdb_string:
            T.verify_expect_string(
                self.last_agdb_message, args, "agdbout", should_be_equal=False
            )

        elif cmd == T._Cmd.expect_agdb_sub_strings:
            T.verify_expect_sub_strings(
                self.last_agdb_message, args, "agdbout", should_be_present=True
            )

        elif cmd == T._Cmd.unexpect_agdb_sub_strings:
            T.verify_expect_sub_strings(
                self.last_agdb_message, args, "agdbout", should_be_present=False
            )

        elif cmd == T._Cmd.send_input:
            self.tracker.send_to_inferior_stdin(args[0])

        elif cmd in [T._Cmd.left, T._Cmd.up, T._Cmd.right, T._Cmd.down]:
            try:
                count_loop = int(args[0])
            except :
                count_loop = 1
            if cmd == T._Cmd.left:
                word = "LEFT"
            if cmd == T._Cmd.right:
                word = "RIGHT"
            if cmd == T._Cmd.up:
                word = "UP"
            if cmd == T._Cmd.down:
                word = "DOWN"
            for _ in range (count_loop):
                self.tracker.send_to_inferior_stdin(word)

        elif cmd == T._Cmd.payload:
            if self.out_queue.empty():
                T.error("No payload available on the level output queue.")
            else:
                # while not self.out_queue.empty():
                    # pl = self.out_queue.get()
                # T.verify_last_payload(pl, kwargs)
                T.verify_payload_queue(self.out_queue, kwargs)

        elif cmd == T._Cmd.variable:
            name, exp_val = args

            val = self.tracker.get_variable_value_as_str(name, "int")
            if val == exp_val:
                T.passed(f"Variable {name} is {exp_val}")
            else:
                T.error(f"Variable {name} should be {exp_val} but is {val}")

        elif cmd == T._Cmd.hook:
            T.call_hook(*args)

        elif cmd == T._Cmd.skip:
            if len(args) >= 1:
                msg = "".join(args)
            else:
                msg = "skipping for unknown reason"
            # skipping the rest of this test
            T.skipped(msg)
            self.skipping = True

        elif cmd == T._Cmd.sleep:
            sleep(args[0])

        elif cmd == T._Cmd.dummy:
            pass

        else:
            T.error("Test command not implemented " + cmdstr)

    def check_validation(self, should_validate=True, substr_reasons=None) -> None:
        if not self.level_compiled():
            return

        val, reasons = self.validate_level(timeout=0.5)

        if should_validate:
            if val:
                T.passed("Level validated as expected")
            else:
                T.error(f"Level did not validate but was expected to.\nReasons: {reasons}")
        else:
            if val:
                T.error("Level validated but should not have")
            else:
                if not substr_reasons:
                    T.passed(f"Level did not validate as expected")
                elif substr_reasons in reasons:
                    T.passed(f"Level did not validate as expected, {substr_reasons} found in reasons")
                else:
                    T.error(f"Level did not validate as expected, but {substr_reasons} not found in reasons")


    def test_scenario(self, scenario):
        self.recompile_bug()
        scenario()
        T.expect_defeat()
        self.run()

        self.recompile_answer()
        scenario()
        T.expect_victory()
        self.run()
