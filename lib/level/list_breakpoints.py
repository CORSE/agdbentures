class ListBreakpoints:
    """
    Manage breakpoints set by the application in gdb.

    Attributes:
        previous_break (dict): Previous set of breakpoints.
        our_breakpoints (list): List of breakpoints set by the application.
        list_all_break (dict): Dictionary of all breakpoints in the game.
        gap (int): Difference between the current gdb breakpoint index and our internal index.
        delete_over (int): Maximum ID of breakpoints known for deletion.
        we_delete_all (bool): Flag to indicate if all breakpoints are being deleted.
        breakpoints_set (bool): Flag to ensure breakpoints are set before deletion.
    """
    
    def __init__(self):
        self.previous_break: dict = {}
        self.our_breakpoints: list = []
        self.list_all_break: dict = {}
        self.gap: int = 0
        self.delete_over: int = 0
        self.we_delete_all: bool = False
        self.breakpoints_set: bool = False

    def set_list_all_breakpoint(self, list_all_break: dict) -> None:
        """
        Set the dictionary of all breakpoints placed in the game.
        
        Args:
            list_all_break (dict): Dictionary of all breakpoints.
        """
        self.list_all_break = list_all_break

    @staticmethod
    def get_id_init(breakpoint: int) -> int:
        """
        Get the initial ID of a breakpoint.

        Args:
            breakpoint (int): Breakpoint identifier.
        
        Returns:
            int: The initial ID of the breakpoint.
        """
        return breakpoint

    @staticmethod
    def get_name_init(breakpoint: tuple) -> str:
        """
        Get the initial name of a breakpoint.

        Args:
            breakpoint (tuple): Breakpoint data.
        
        Returns:
            str: The name of the breakpoint.
        """
        return breakpoint[0]

    def get_ids_of_our_breakpoints(self) -> list[int]:
        """
        Get the IDs of all breakpoints set by the application.

        Returns:
            list[int]: List of breakpoint IDs.
        """
        return [id for id, _ in self.our_breakpoints]

    def change_delete(self, over: int | None) -> None:
        """
        Change the delete state for breakpoints.

        Args:
            over (int | None): Maximum ID of breakpoints known for deletion.
        """
        self.delete_over = over if over is not None else 0
        self.we_delete_all = not self.we_delete_all

    def delete_finish(self, actual: int) -> None:
        """
        Finish the delete process if the current breakpoint matches the delete_over limit.

        Args:
            actual (int): Current breakpoint ID.
        """
        if actual == self.delete_over:
            self.change_delete(None)

    def not_our_breakpoint(self, num: int) -> bool:
        """
        Check if a breakpoint is set by the player or the application.

        Args:
            num (int): Breakpoint ID.
        
        Returns:
            bool: True if the breakpoint is set by the player, False if set by the application.
        """
        return all(num != id for id, _ in self.our_breakpoints)

    def restart(self) -> None:
        """
        Restart the level, keeping only active breakpoints in gdb.
        Erase all previous breakpoints from the previous instance of the level.
        """
        if not self.breakpoints_set:  # Used if we restart without running any command
            self.create_real_list_break(self.gap)
        gap = 0
        for elem in self.previous_break:
            id = elem[0]
            if id > 5:  # id = 5 => Temporary breakpoint placed by gdb during start
                gap = -self.gap
            if (id + gap) in self.list_all_break:
                del self.list_all_break[id + gap]
        self.breakpoints_set = False

    def create_real_list_break(self, gap: int) -> None:
        """
        Create the real list of breakpoints, adjusting for gdb's indexing.

        Args:
            gap (int): The gap between our internal indexing and gdb's indexing.
        """
        self.our_breakpoints = []
        flag = False
        list_keys = list(self.list_all_break.keys())
        for index, key in enumerate(list_keys):
            name_fun = self.get_name_init(self.list_all_break[key])
            id_fun = self.get_id_init(key)

            # Do this to avoid storing previous breakpoints placed by the player in our list
            if (not flag and name_fun == "exit" and
                    self.get_name_init(self.list_all_break[list_keys[index + 1]]) == "message" and
                    self.get_name_init(self.list_all_break[list_keys[index + 2]]) == "prompt"):
                name_fun = "__GI_exit"
                flag = True
            if id_fun == 5:
                gap += 1

            if flag:
                self.our_breakpoints.append([id_fun + gap, name_fun])

        self.previous_break = self.our_breakpoints.copy()
        self.gap = gap
        self.breakpoints_set = True
