"""Curses level implementation."""

from typing import Callable
import curses
import curses.textpad

from easytracker import PauseReasonType
from level.level_abc import AbstractLevel


# pylint: disable=fixme
class LevelCurses(AbstractLevel):
    """Level using curses."""

    def __init__(self):
        super().__init__(self.metadata)
        self.setup()

    # pylint: disable=too-many-locals
    def show_map(self, stdscr: curses.window, pad: curses.window) -> None:
        """Update the cursed map according the variables."""
        pad.clear()
        pad.box()

        # Fetching the map
        list_map = self.map.get_ascii_map()
        for i, line in enumerate(list_map, 1):
            line_str = "".join(line)
            pad.addstr(i, 1, line_str)

        # Adding the WOP
        for wop_x, wop_y in self.available_wop():
            pad.addch(wop_y + 1, wop_x + 1, "W")

        # Update and refresh
        lines, cols = stdscr.getmaxyx()
        smincol = int(cols / 2 - self.map.width / 2 - 1)
        sminrow = int(lines / 2 - self.map.height / 2 - 1)
        smaxcol = smincol + self.map.width + 2
        smaxrow = sminrow + self.map.height + 2
        stdscr.clear()
        stdscr.refresh()
        pad.refresh(0, 0, sminrow, smincol, smaxrow, smaxcol)

    @staticmethod
    def box_msg(
        stdscr: curses.window, message_list: list[str], title: str = ""
    ) -> None:
        """
        Show a message in a box.

        :param stdscr: The window to display the message in.
        :param messages: The message to display, given as a list of lines.
        :param title: The title of the box.
        """
        # Pad init
        max_msg_len = max(map(len, message_list)) + 2
        pad_height = len(message_list) + 2
        pad_width = max(max_msg_len, len(title) + 2)
        pad = curses.newpad(pad_height, pad_width)
        pad.box()

        # Add the title of the box
        if title:
            pad.addstr(0, 1, title)

        # Add the content
        for line, message in enumerate(message_list, start=1):
            pad.addstr(line, 1, message)

        # Center the pad
        lines, cols = stdscr.getmaxyx()
        smincol = int(cols / 2 - pad_width / 2)
        sminrow = int(lines / 2 - pad_height / 2)
        smaxcol = smincol + pad_width + 2
        smaxrow = sminrow + pad_height + 2
        pad.refresh(0, 0, sminrow, smincol, smaxrow, smaxcol)

        # Wait for confirmation
        stdscr.getkey()

    def read_cmd_curses(
        self,
        stdscr: curses.window,
        input_win: curses.window,
        resize_func: Callable[[], None],
    ) -> str:
        """
        Read a command while handling resizes.

        :param stdscr: The main window to write in.
        :param input_win: The input window.
        :param resize_func: The function to call in a resize event.
        :return: The raw input.
        """
        # Show a prompt
        lines, _ = stdscr.getmaxyx()
        stdscr.addstr(lines - 1, 0, self.prompt)
        stdscr.refresh()

        # New Input Box:
        cmd_pad = curses.textpad.Textbox(input_win)

        # Show the cursor
        curses.curs_set(True)

        def on_resize() -> None:
            """Update the windows on resize."""
            # Update the map
            resize_func()
            # Update the input window's coordinates
            lines, cols = stdscr.getmaxyx()
            input_win.resize(1, cols - len(self.prompt))
            input_win.mvwin(lines - 1, len(self.prompt))
            # Show the prompt
            stdscr.addstr(lines - 1, 0, self.prompt)
            stdscr.refresh()

        def validator(char: int) -> int:
            """
            Textpad validator.
            By default, the `edit` method of the Textbox instance
            terminates the input when Control-G is received.
            We want it to end with the enter key.
            """
            if char == curses.KEY_RESIZE:
                on_resize()
            elif char == ord("\n"):
                char = 7  # End the command (Control-G)
            return char

        # Read the command
        input_win.clear()
        cmd = cmd_pad.edit(validator)

        # Hide the cursor
        curses.curs_set(False)

        return cmd

    def print_wop_msg(self, stdscr: curses.window) -> None:
        """
        Print the WOP message.

        :param stdscr: The main window.
        """
        for wop_id, wop_message in self.get_wop_message().items():
            self.box_msg(stdscr, wop_message, title=f"WOP{wop_id}")

    def run(self) -> None:
        """Run in a curses interface."""

        def runner(stdscr: curses.window):
            """Runner that will be called by the curses wrapper."""
            self.prepare_tracker()
            pause_type = self.tracker.pause_reason.type

            # Prepare curses
            lines, cols = stdscr.getmaxyx()
            pad = curses.newpad(self.map.height + 2, self.map.width + 2)
            input_win = curses.newwin(
                1, cols - len(self.prompt), lines - 1, len(self.prompt)
            )
            curses.curs_set(False)
            stdscr.clear()

            while pause_type != PauseReasonType.EXITED:
                # Fetch variables
                self.update_state()

                # Printing the output
                # TODO add a dedicated pad for the stdout
                stdout = self.tracker.get_inferior_stdout()
                if stdout:
                    self.box_msg(stdscr, stdout.split("\n"), title="stdout")

                # Update the code visualizator
                self.show_next_line()

                # Show the map
                self.show_map(stdscr, pad)
                self.print_wop_msg(stdscr)

                # Read a command and run it
                cmd = self.read_cmd_curses(
                    stdscr, input_win, resize_func=lambda: self.show_map(stdscr, pad)
                )
                self.parse_and_eval(cmd)
                pause_type = self.tracker.pause_reason.type

            if self.tracker.exit_code == 0:
                self.box_msg(stdscr, ["VICTOIRE"])
            else:
                self.box_msg(stdscr, ["DEFAITE"])

        # Call the runner
        curses.wrapper(runner)
