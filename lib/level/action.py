from typing import Any, Union, Optional
from queue import Queue
from logs import lvl

import graphic.constants as cst
from config import Config
from language import Lang


class Action:

    def __init__(
        self,
        name: str,  # action to do
        dependencies: list = [],  # list of actions that must be done before this one
        payload: Optional[dict] = None,  # payload to send to the gui
        done: bool = False,  # whether the action has been done
        callback: callable = None, # function to call after an Action is done
    ):

        self.name = name
        self.done = done
        self.payload = payload
        self.dependencies = dependencies
        self.callback = callback

    def __str__(self):
        dependencies = [str(a) + ", " for a in self.dependencies]
        return f"Action({self.name}, payload={self.payload}, dependencies={dependencies}, done={self.done}, callback={self.callback.__name__ if self.callback else None})"

    def __repr__(self):
        return self.__str__()

    def is_done(self) -> bool:
        return self.done

    def is_ready(self) -> bool:
        return all([action.is_done() for action in self.dependencies])

    def set_done(self):
        self.done = True
        if self.callback:
            self.callback()

