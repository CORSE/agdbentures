"""Verification module so we can write some simple checks for levels"""

from typing import Callable

from logs import chk
from easytracker import init_tracker, PauseReasonType
from tracker_helper import TrackerHelp, TimeoutCallback
from language import message
from level.engine import Engine

class VerifyCondition:
    def __init__(self, cond):
        var_a, cnd, var_b = cond.split(" ")
        self.var_a = var_a
        self.condition = cnd
        try:
            self.var_b = int(var_b)
        except ValueError:
            self.var_b = var_b

    def __eq__(self, obj):
        if not isinstance(obj, VerifyCondition):
            return False
        return (
            self.var_a == obj.var_a
            and self.condition == obj.condition
            and self.var_b == obj.var_b
        )

    def get(self):
        if self.condition == '==':
            condstr = message('CND_EQUAL')
        else:
            condstr = message('CND_GENERIC').format(cnd=self.condition)

        return f"{self.var_a} {condstr} {self.var_b}"

    def eval(self, val_a, val_b=None):
        if not val_b:
            assert type(self.var_b) == int
            val_b = self.var_b
        return eval(f"{val_a} {self.condition} {val_b}")

    def __str__(self):
        return f"Condition({self.get()})"


class VerifyChangeInFunction:
    def __init__(self, var_name, fun_name):
        self.var_name = var_name
        self.fun_name = fun_name

    def __eq__(self, obj):
        chk.debug(f"Comparing {self} and {obj}")
        if not isinstance(obj, VerifyChangeInFunction):
            return False
        return self.var_name == obj.var_name and self.fun_name == obj.fun_name

    def __str__(self):
        return f"Change({self.var_name} in {self.fun_name})"


class VerifyMustCall:
    def __init__(self, fun_name):
        self.fun_name = fun_name

    def __eq__(self, obj):
        if not isinstance(obj, VerifyMustCall):
            return False
        return self.fun_name == obj.fun_name

    def __str__(self):
        return f"MustCall({self.fun_name})"


class LevelChecker(TrackerHelp):
    """
    Class to validate a level.
    By default, a timeout of 6 seconds is applied.
    """
    def __init__(self, level, timeout=6.0):
        self.level = level
        self.level.checker = self  # so pre_validation has access to checker
        self.metadata = self.level.metadata
        self._result = True  # by default, consider the validation successful
        self.reasons_failed = []
        self.stats = {}
        self.stats["in_function"] = {}
        self.stats["count_calls"] = {}
        self._inputs = []  # inputs to send to inferior during execution
        self.timeout = timeout

        chk.info(f"Starting checker for {level}")

        super().__init__(self.level.source_level_dir, self.metadata["exec_name"])
        self.load_and_start_program()
        # First run some custom code for this level
        # self.check()
        self.level.pre_validation()

        ## TODO: need to decide how to describe input to send to inferior
        # self.tracker.send_to_inferior_stdin("Swordfish")
        #
        must_calls = [VerifyMustCall("verify_exit")]

        init_conditions = []
        always_conditions = []

        exit_conditions = []
        in_functions = []
        if self.metadata["test"] and self.metadata["test"] == "true":
            return
        if Engine.player_mode == 'global_variables':
            exit_conditions.append(VerifyCondition("player_x == exit_x"))
            exit_conditions.append(VerifyCondition("player_y == exit_y"))

            in_functions.append(VerifyChangeInFunction("player_x", "forward"))
            in_functions.append(VerifyChangeInFunction("player_y", "forward"))

        elif (
            Engine.player_mode in ['simple_map', 'map_stack', 'simple_game']
        ):
            exit_conditions.append(VerifyCondition("map->player_x == exit_x"))
            exit_conditions.append(VerifyCondition("map->player_y == exit_y"))
            in_functions.append(VerifyChangeInFunction("map->player_x", "move_player"))
            in_functions.append(VerifyChangeInFunction("map->player_y", "move_player"))
        else:
            raise NotImplementedError(f"Player mode not implemented for engine: {Engine.current_engine}")

        for v in self.metadata["no_verify"]["gen"]:
            chk.debug(f"No verify: {v}")
            if isinstance(v, VerifyChangeInFunction):
                in_functions.remove(v)
            elif isinstance(v, VerifyMustCall):
                must_calls.remove(v)
            else:
                raise ValueError(f"Cannot remove verify of this instance {v}")

        for v in self.metadata["no_verify"]["exit"]:
            chk.debug(f"No verify: {v}")
            if isinstance(v, VerifyCondition):
                chk.debug(f"Removing from exit conditions")
                exit_conditions.remove(v)
            else:
                raise ValueError(f"Cannot remove verify of this instance {v}")

        for v in self.metadata["no_verify"]["init"]:
            raise NotImplementedError(f"Cannot remove verify of this instance {v}")
        for v in self.metadata["no_verify"]["always"]:
            raise NotImplementedError(f"Cannot remove verify of this instance {v}")

        for when in self.metadata["verify"]:
            for v in self.metadata["verify"][when]:
                if when == "gen":
                    raise NotImplementedError
                chk.debug(f"Verify {when}: {v}")
                if isinstance(v, VerifyChangeInFunction):
                    assert when == "always"
                    in_functions.append(v)
                elif isinstance(v, VerifyMustCall):
                    assert when == "always"
                    must_calls.append(v)
                elif isinstance(v, VerifyCondition):
                    if when == "always":
                        always_conditions.append(v)
                    elif when == "init":
                        init_conditions.append(v)
                    elif when == "exit":
                        exit_conditions.append(v)
                    else:
                        raise ValueError(f"When condition '{when}' not valid")
                else:
                    raise NotImplementedError

        for v in in_functions:
            self.check_var_changes_in_function(v.var_name, v.fun_name)

        for f in must_calls:
            self.count_calls(f.fun_name)

        for c in init_conditions + always_conditions:
            if not self.eval_condition(c):
                self.failed(message('CND_INITIAL') + ', ' + c.get())

        chk.debug("Init + always conditions OK")

        for c in always_conditions:
            assert type(c.var_b) is int  # TODO: handle variable if necessary
            chk.debug(f"Watching variable {c.var_a} (must not change)")
            self.register_watch(
                variable_name=c.var_a, callback=lambda: self.check_condition_at_watch(c)
            )

        chk.debug("Always conditions OK")

        self.register_breakpoint(
            function_name="verify_exit",
            callback=lambda: self.check_exit_conditions(exit_conditions),
        )
        chk.debug("Exit conditions OK")

        # Install a timeout for validation, max 6 seconds
        timer = TimeoutCallback(self.tracker, self.timeout, self.interrupt_inferior_if_running)
        timer.start()

        # send all inputs to inferior
        # for i in self._inputs:
        # self.tracker.send_to_inferior_stdin(i, append_newline=True)
        inf_in = '\n'.join(self._inputs)
        self.tracker.send_to_inferior_stdin(inf_in, append_newline=True)

        chk.debug("Sent all inputs")

        queue = self.tracker.stdout_queue
        # Show gdb's output from starting the inferior
        while not queue.empty():
            chk.debug(queue.get())
        self.tracker.resume()
        pause_type = self.tracker.pause_reason.type

        while pause_type != PauseReasonType.EXITED:
            chk.debug(f"Inferior paused for reason {self.tracker.pause_reason}")

            # Show current output queue from gdb
            while not queue.empty():
                chk.debug(queue.get())

            if pause_type == PauseReasonType.WATCHPOINT:
                chk.debug("Pause reason : WATCHPOINT")
                wp_no = self.tracker.pause_reason.args[0]
                assert wp_no in self.internal_wp
                bp = self.internal_wp[wp_no]
                for cb in bp.callbacks:
                    cb()
            elif pause_type == PauseReasonType.BREAKPOINT:
                chk.debug("Pause reason : BREAKPOINT")
                bp_no = self.tracker.pause_reason.args[0]
                assert bp_no in self.internal_bp
                bp = self.internal_bp[bp_no]
                for cb in bp.callbacks:
                    cb()
            elif pause_type == PauseReasonType.ERROR:
                self.reasons_failed = []  # all other reasons are irrelevant
                chk.error(
                    f"ERROR RECEIVED FROM GDB, please contact the developpers of Agdbentures"
                )
                raise ValueError("GDB error received")

            elif pause_type == PauseReasonType.SIGNAL:
                sig = self.tracker.pause_reason.args
                if 'SIGINT' in sig:
                    chk.debug("SIGINT received during validation")
                    self.failed(f"Validation interrupted")
                else:
                    self.failed(f"Signal {sig} received {self.tracker.pause_reason}")
                    chk.error(f"Signal type not implemented for CHECK: {pause_type}")
                break
            else:
                self.failed(f"UNEXPECTED PAUSE REASON: {self.tracker.pause_reason} (please contact the developpers)")
                chk.error(f"Pause type not implemented for CHECK: {pause_type}")
                break

            self.tracker.resume()
            pause_type = self.tracker.pause_reason.type

        timer.cancel()

        chk.debug("Pause loop OK")

        code = self.tracker.exit_code

        chk.debug("Inferior output during validation:")
        while not queue.empty():
            chk.debug(queue.get())

        self.tracker.terminate()

        for f in must_calls:
            if self.stats["count_calls"][f.fun_name] == 0:
                self.failed(f"{f.fun_name} must be called but was not")

        self.level.post_validation()

        chk.info(f"Finished validating level, return value {code}")


    def send_command(self, command : str) -> None:
        """
        @params: command = a command we want to send to gdb
        Send a command to gdb during validation of level
        """
        self.tracker.send_direct_command(command)
        chk.debug(f"send {command} to gdb")
    def append_inputs(self, inputs):
        self._inputs += inputs

    def check_var_changes_in_function(self, var_name, fun_name):
        self.stats["in_function"][var_name] = {}
        self.stats["in_function"][var_name]["num_hits"] = 0
        self.stats["in_function"][var_name]["num_fails"] = 0
        self.stats["in_function"][var_name]["fun_name"] = fun_name

        chk.debug(f"Registering check that {var_name} changes only in {fun_name}")
        self.register_watch(
            variable_name=var_name,
            callback=lambda: self.check_in_function(var_name, fun_name),
        )

    def check_in_function(self, var_name, fun_name):
        chk.debug(f"Checking {var_name} changes in {fun_name}")
        self.stats["in_function"][var_name]["num_hits"] += 1
        if self.tracker_in_function(fun_name):
            return
        self.stats["in_function"][var_name]["num_fails"] += 1
        self.failed()  # will only add one reason at the end

    def count_calls(self, fun_name):
        self.stats["count_calls"][fun_name] = 0
        chk.debug(f"Setting counting of calls to {fun_name}")
        self.register_breakpoint(
            function_name=fun_name, callback=lambda: self.increase_call_count(fun_name)
        )

    def increase_call_count(self, fun_name):
        self.stats["count_calls"][fun_name] += 1

    def check_condition_at_watch(self, cond):
        chk.debug(f"Watch triggered for {cond}, will evaluate condition")
        chk.debug(f"Watchpoint: {self.tracker.pause_reason}")
        args = self.tracker.pause_reason.args
        if cond.eval(args[3]):
            chk.debug("Condition evaluation OK")
            return
        self.failed(
            f"Condition failed when {args[1]} value changed from {args[2]} to {args[3]}: {cond.get()}"
        )

    def eval_condition(self, cond):
        try:
            val_a = self.tracker.get_variable_value_as_str(cond.var_a, "int")
        except AttributeError as e:
            chk.error(f"Cannot get value for variable {cond.var_a}")
            raise e

        if type(cond.var_b) is str:
            try:
                val_b = self.tracker.get_variable_value_as_str(cond.var_b, "int")
            except AttributeError as e:
                chk.error(f"Cannot get value for variable {cond.var_b}")
                raise e
        else:
            val_b = cond.var_b

        return cond.eval(val_a, val_b)

    def check_exit_conditions(self, conds):
        chk.debug("Checking exit conditions")
        for c in conds:
            chk.debug(f"Evaluating {c}")
            if self.eval_condition(c):
                chk.debug("Exit condition OK")
                continue
            self.failed(f"Condition at exit failed: {c.get()}")

    def failed(self, reason=None):
        self._result = False
        if reason:
            self.reasons_failed.append(reason)

    def result(self):
        return self._result

    def get_stats(self):
        return self.stats

    def get_reasons(self) -> str:
        reasons = [""] + self.reasons_failed
        for var_name in self.stats["in_function"]:
            if self.stats["in_function"][var_name]["num_fails"] > 0:
                fun_name = self.stats["in_function"][var_name]["fun_name"]
                reasons.append(
                    f"{var_name} must only change in function \"{fun_name}\""
                )

        return "\n - ".join(reasons)

    #####
    ##### Warning: get_variable and eval_condition
    ##### come from an old version of agdbentures
    ##### and are included here for future reference
    #####
    def OLD_get_variable(self, var: str):
        """
        Fetch the value of a variable.

        :param var: The variable to fetch.
        """
        # Translation into the inferior variable name
        target = self.alias[var]

        return value(target, self.variables)

    def OLD_eval_condition(self, condition: str):
        """
        Evaluates a condition while making sure that we use
        the inferior variable names for the information like
        player_x or player_x.

        :param condition: The condition to evaluate.
        :return: The boolean value of the condition.
        """
        # Expand the expression
        resolved_condition = expand_expr(condition, self.alias)

        return value(resolved_condition, self.variables)
