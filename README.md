# Agdbentures

## Setup

### Dependencies

- Python3.10 or above is required. Please check your Python3 version.
- gcc
- gdb version 10.1 and higher
- gtk Vim with server mode enabled
- GNU make
- unifdef version 2.10 and higher (check with `unifdef -V`)
- termcolor (python3-termcolor)

Under Ubuntu/Debian, this can be achieved using:
```console
apt install gcc python3 build-essential unifdef vim-gtk3
```

Optional: `xdotool` to allow Agdbentures to modify the window placement.
```console
apt install xdotool
```

Optional: Visual Studio Code (VScode) to edit levels, althouh any editor can do
the job (modify the `config.editor` file accordingly).

See the [Debian install page](https://wiki.debian.org/VisualStudioCode)


### Getting easytracker
Agdbentures depends on the Easytracker library to communicate with GDB.
Easytracker is kept in Agdbentures as a git submodule.
To get it:

```console
git submodule init
git submodule update
```

or

```console
git submodule update --init
```


(Later, when updating, need only to checkout the last version using:
```console
git pull
git submodule update
```
)

### Installing easytracker

After the submodule is checked-out, run the following commands to install
the various Python libraries Easytracker depends on, then build the various
libraries necessary for Agdbentures.

```console
cd easytracker
pip3 install --user -r requirements.txt
make
cd ..
```

Note: it may be necessary to install the requirements in a python venv (Virtual
Environment) depending on your distribution and whether your python packages
are managed by the distribution or by pip, for instance, in the agdbentures
root directory:

```console
python3 -m venv .venv
source .venv/bin/activate
pip3 install -r requirements.txt
```


### Python PATH
(SHOULD NOT BE NECESSARY ANYMORE)

Add agdbentures and easytracker to your python path:

```console
export PYTHONPATH=$PYTHONPATH:$HOME/.../agdbentures/easytracker:$HOME/.../agdbentures/lib
```


### Installing dependencies

Agdbentures depends on the python Arcade library.
Note: Arcade depends on other libraries, such as Pillow, which needs some 
header files that are usually installed on systems. Verify the header files of 
`libjpeg` are installed using:

```console
apt show libjpeg-dev
```

If not, install them using:

```console
apt install libjpeg-dev
 ```

Agdbentures uses other python modules that are usually easily availaible in the 
package manager of your distribution. If not, they can be installed in the 
python virtual environment of Agdbentures.

```console
apt install python3-termcolor python3-yaml
```

You can now install the requirements of Agdbentures in a virtual environment 
for Agdbentures (this time in the `agdbentures` directory, not `easytracker`'s submodule).

```console
python3 -m venv .venv
source .venv/bin/activate
pip3 install -r requirements.txt
```

If necessary (if the packages where not available in your system's package 
manager), install the following python modules in the `venv`.
```console
pip3 install termcolor yaml
```



#### Note for MAC users
It may be necessary to install `easy_install`

```console
pip3 install --user easy_install
```


## Running Agdbentures


### Generating the engines
Mandatory to do prior to launching agdbentures.

```console
python3 make_engine.py
```

### Running Agdbentures
Normally, at this step, all should work correctly:

```console
./agdbentures
```


# Developping in Agdbentures

## Dependencies

To test Agdbentures, you will need `pytest`, `pytest-env` and `criterion`.

```console
apt install libcriterion3 python3-pytest python3-pytest-env
```

or, in the venv (only for pytest):

```console
pip install pytest pytest-env
```



## Getting ssh access

If you cloned Agdbentures using HTTPS urls, it is more cumbersome to work on 
the project. You will have to enter your username and password to push 
updates.
Change the urls in your `.git/config`, replacing:

    url = https://gitlab.inria.fr/CORSE/agdbentures.git

by

    url = git@gitlab.inria.fr:CORSE/agdbentures.git


## Working on levels

### Generating a level
Level generation is done automatically from Agdbentures. If necessary, it is
possible to manually do it using the following command, for example:

```console
python3 make_level.py levels/local_struct
```

### Creating a level

> TODO: explain the general way to do it.

For now, look at an existing level, e.g., `levels/tutorial/00_intro` 


### Tips when working on a level

When designing and testing a level, you often need relaunch the same level 
after have worked on the `.c` files, and also switching a lot between the 'bug' 
and the 'answer' version.

In the regular mode of Agdbentures (`release`), this requires to manually 
regenerates the level using `make_level.py`.

In order to simplify this process, it is recommended to start Agdbenture with 
either the `--dev` or the `--test` flags, which make Agdbentures search its 
files in a different hierarchy than the `working_directory` one.

- With the `--dev` flag, it is in the `dev_directory`, where `.c` files are 
  hard links to the original level files.
  This mode is adapted when in the process of modifying the `.c` files.
  WARNING: in this mode, language is not applied to filter the `.c` files,
  for instance, all `printf` will be kept in all languages.

- With the `--test` flag, it is in the `test_directory`, where `.c` file are 
  stripped from `@agdb` comments but still keep the `#ifdef`/`#ifndef` tags.
  This mode is adapted when testing levels that should already work, e.g., when
  there is a change in the graphical engine and one wants to test that levels
  still behave ok.
  If the original `.c` level files are changed, it is sufficient to exit the
  level and re-enter it to update the code.


In both modes, the graphical window provides "secrets" keybindings to quickly 
recompile the code:
- Ctrl-b: recompile in "bug" mode
- Ctrl-a: recompile in "answer" mode

For `fix_*` versions, one need to manully go in the corresponding directory, 
e.g., `dev_directory/tutorial/01_first_bug` and recompile as follows:
```console
make -B fix_move_exit
```

### Kill Agdbentures when things go awry and C-c does not work
```console
pkill -e -9 -f ./agdbentures
```


### Generating a map
If you need a map for your level:
of width 20 and height 10 in file test.tmx

```console
python3 ./lib/graphic/map_utils.py test.tmx 10 20
```

Then edit the tmx file with 'tiled'
and add the following line to the /* @AGDB tag */ of the .c file:

```
 * arcade_maps: main test.tmx
```



## Notes

The level list and progress is hardcoded in the `__init__` function of
ProgressManager class in `lib/progress/progress_manager.py`




# Troubleshooting issues

## Graphics problems with Windows Subsystem for Linux (WSL)


Need to install opengl drivers.
```console
apt install mesa-utils
```

See
https://github.com/microsoft/WSL/issues/2855

Maybe need to set the DISPLAY environment variable:
```console
export DISPLAY="`grep nameserver /etc/resolv.conf | sed 's/nameserver //'`:0"
```


