#!/usr/bin/env bash
set -euo pipefail

dir="$(dirname "$0")"

registry="registry.gitlab.inria.fr"
repository="$registry/corse/agdbentures"
image="agdbentures"
version="$(cat "$dir"/VERSION)"

cd "$dir"
echo "Login to remote repository: registry.gitlab.inria.fr"
docker login "$registry"
echo "Checking not published: $repository/$image:$version"
! docker manifest inspect "$repository/$image:$version" >/dev/null 2>&1 || \
    { echo "ERROR: version already exists on registry $repository: $image:$version" >&2; exit 1; }
echo "Tagging: $repository/$image:$version"
docker tag "$image:latest" "$repository/$image:$version"
echo "Pushing: $repository/$image:$version"
docker push "$repository/$image:$version"
echo "Tagging: $repository/$image:latest"
docker tag "$image:latest" "$repository/$image:latest"
echo "Pushing: $repository/$image:latest"
docker push "$repository/$image:latest"

