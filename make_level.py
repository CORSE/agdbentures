#!/usr/bin/env python3
"""
Script that will prepare the working directory for the student.
We should test the levels in this directory and not in the source directory

Usage: python make_level.py levels/<path_to_level>/<level_directory>
    or python make_level.py levels/<path_to_level>/<some_level>.c
See "python make_level.py -h" for a list of arguments  

Examples:
python make_level.py levels/medium/local_struct --defines BUG --clear
python make_level.py levels/medium/local_struct --dev


//!\\ LIMITATION:
Cannot work with subdirectories every engine files has to be at the same level.
This is the same for level files.

1- It reads all the metadata in the main.c file
2- Then it applies the engine and level tags to engine and level file and place them in the destination directory
3- Creates a Makefile

Agdbentures will run the level.py file in the source directory
The working directory is printed on standard output at the end of the script so caller script can get it

You can copy and fill this at the begining of your level file:
/* @AGDB
 * level_name: MANDATORY
 * engine_name: MANDATORY
 * exec_name: OPTIONAL (will be "main" if absent)
 */

 engine tags and antitags are -Dsym and -Usym parameters passed to unifdef in addition to the parameter tags
"""

import logging
import os
import sys
import shutil
import argparse
from subprocess import run, PIPE, CalledProcessError

from string import Template

v = sys.version_info
# Make sure to insert first in the search path the current lib and easytracker
# directory
cdir = os.path.dirname(__file__)
sys.path.insert(
    0, os.path.join(cdir, f".venv/lib/python{v.major}.{v.minor}/site-packages")
)  # if using a python virtual env
sys.path.insert(0, os.path.join(cdir, "lib"))
sys.path.insert(0, os.path.join(cdir, "easytracker"))

from loader.loader_utils import extract_metadata, apply_unifdef, get_defines, strip_comments
from logs import mklog

AGDBENTURES_DIR = os.path.dirname(__file__)
ENGINES_DIRS = os.path.join(AGDBENTURES_DIR, "engine_versions")
DEV_DIR = os.path.join(AGDBENTURES_DIR, "dev_directory")
TEST_DIR = os.path.join(AGDBENTURES_DIR, "test_directory")
ANSWER_DIR = os.path.join(AGDBENTURES_DIR, "answer_directory")
WORKING_DIR = os.path.join(AGDBENTURES_DIR, "working_directory")

# Argument parsing
parser = argparse.ArgumentParser()
parser.add_argument(
    "source",
    help="The path to either a single .c file, or a level source directory containing a main.c",
)
parser.add_argument(
    "--dest-dir",
    action="store",
    help="The path to the destination directory if this is not the default one",
)
parser.add_argument(
    "--clear",
    action="store_true",
    default=False,
    help="Remove the destination directory before operation",
)
parser.add_argument(
    "-r",
    "--release",
    action="store_true",
    help="""Option to toggle release mode.
Prepares files to be used in-game in the working directory.""",
)
parser.add_argument(
    "-a",
    "--answer",
    action="store_true",
    help="""Option to toggle answer mode.
Used alone, creates the solution in an answer/ hierarchy.
Used in conjunction with --release, generates solution files in the working/ hierarchy to be used in-game (mainly for dev. purposes)""",
)
parser.add_argument(
    "--test",
    action="store_true",
    help="""Option to toggle test mode: preparing levels for testing.
Removes the @agdb comments but keep the #ifdef/#ifndef related to bugs.
Warning: implies --clear""",
)
parser.add_argument(
    "-f",
    "--fix",
    nargs="?",
    default=None,
    const="",
    help="""Creates a 'fix' version, using one of the 'false' fix solutions, i.e., a solution that technically finishes the level but by circumventing the problem(s) instead of really fixing them.
Must be used in conjunction with --release.
Argument must be empty or a string 'foo' that will be used to create define 'FIX_FOO'. Special argument 'help' can be used to display the list of available fixes for this level.""",
)
parser.add_argument(
    "--dev",
    action="store_true",
    help="""Option to toggle development mode and use hard links.
Useful during the development of a level to be able to compile and work in the
dev directory while having the changes in the source files be reflected in the level/ hierarchy.""",
)
parser.add_argument(
    "--defines",
    nargs="+",
    default=[],
    help="""A list of #define for unifdef with --release.
Include 'BUG' to get the student version (default).""",
)
parser.add_argument(
    "--udefines",
    nargs="+",
    default=[],
    help="""A list of #define that are manually not defined, used with --release.
Include 'BUG' to get the correct solution (Undersand: the bug is not defined, default with --answer).""",
)
parser.add_argument(
    "--debug",
    "-d",
    type=int,
    choices=[0, 1, 2, 3],
    default=2,
    help="Set debug level (0 = crit, 1 = warn, 2 = info, 3 = debug)",
)
parser.add_argument(
    "--no-compile",
    action="store_true",
    help="""Disable trying to compile using `make` in the target directory after creating the files.""",
)


args = parser.parse_args()

if args.debug == 0:  # Nothing except critical messages
    loglevel = logging.CRITICAL
elif args.debug == 1:  # Only warning and errors
    loglevel = logging.WARNING
elif args.debug == 2:
    loglevel = logging.INFO
else:
    loglevel = logging.DEBUG

mklog.setLevel(loglevel)

# In test mode, should always clear first the directory to ensure we use the 
# latest version
if args.test:
    args.clear = True

if (args.defines != [] or args.udefines != []) and not args.release:
    print("Error: --defines and --udefines can only be used with --release")
    exit(1)


defines = args.defines
udefines = args.udefines

if args.dest_dir is not None and args.clear:
    print("--dest-dir and --clear cannot be used together to avoid mistakes !")
    sys.exit(1)

if args.fix is not None:
    if not args.release:
        print("Error: --fix can only be used with --release")
        sys.exit(1)
    elif args.answer:
        print("Cannot have both --fix and --answer at the same time")
        sys.exit(1)


def create_dir(dir_path: str, clear=False, git=False):
    """Creates the given directory if it doesn't exist if clear is True, deletes the directory first"""
    if clear:
        try:
            shutil.rmtree(dir_path)
            mklog.info(f"Deleted {dir_path}")
        except FileNotFoundError:
            mklog.warning(
                f"Asked to delete {dir_path} but the directory does not exist"
            )

    if not os.path.exists(dir_path):
        os.makedirs(dir_path)
        mklog.info(f"Created {dir_path}")
        if git:
            run(["git", "init", dir_path, "-b", os.path.basename(dir_path)])
            mklog.info("Init git working dir")


# Check if source is a directory or a file
if os.path.isfile(args.source):
    SOURCE_LEVEL_DIR = None
    MAIN_SOURCE_FILE = args.source
elif os.path.isdir(args.source):
    SOURCE_LEVEL_DIR = args.source
    MAIN_SOURCE_FILE = os.path.join(SOURCE_LEVEL_DIR, "main.c")
    if not os.path.isfile(MAIN_SOURCE_FILE):
        print(f"Error: cannot find {MAIN_SOURCE_FILE} in {SOURCE_LEVEL_DIR}")
        exit(1)
else:
    print(f"Error: {args.source} is not a valid directory or .c file")
    exit(1)


mklog.info(f"Source file: {MAIN_SOURCE_FILE} in {SOURCE_LEVEL_DIR}")


# 1 Reads the metadata from the main.c file
metadata = extract_metadata(MAIN_SOURCE_FILE)
mklog.debug("Metadata:")
mklog.debug(str(metadata))

LEVEL_NAME = metadata["level_name"]

# 1bis retrieve #ifdef flags
mklog.info("Retrieving #ifdef flags from source")
defs = get_defines(MAIN_SOURCE_FILE)
# will activate all BUG_* by default and deactivate FIX_* by default

bug_list = ["BUG"]
fix_list = []
debug_list = []
ignored_list = []
for d in defs:
    if d.startswith("BUG_"):
        bug_list.append(d)
    elif d.startswith("FIX_"):
        fix_list.append(d)
    elif d.startswith("_"):
        debug_list.append(d)
    else:
        ignored_list.append(d)

mklog.debug(f"Defines for bugs: {bug_list}")
mklog.debug(f"Defines for fixes: {fix_list}")
mklog.debug(f"Defines used for debug: {debug_list}")
mklog.debug(f"Ignored defines: {ignored_list}")

if args.fix == 'help':
    mklog.info("List of available fixes for this level:")
    for f in fix_list:
        print(f)
    exit(0)

# 1ter set variables and defs/udefs based on options
with_git = False
if args.dev:
    WORKING_DIR = DEV_DIR
    defines = bug_list
    udefines = fix_list
elif args.test:
    WORKING_DIR = TEST_DIR
    defines = bug_list + debug_list
    udefines = fix_list
elif args.release:
    WORKING_DIR = WORKING_DIR
    with_git = True
    if defines == [] and udefines == []:
        udefines = debug_list  # never show debug parts in release environment
        if args.answer:  # both options: generate answer in working dir
            udefines += bug_list + fix_list
        elif args.fix is not None:  # use one of the FIX_ defines
            if args.fix != "":
                a_fix = "FIX_" + args.fix.upper()
                assert a_fix in fix_list
            else:
                a_fix = fix_list[0]

            while a_fix in fix_list:
                fix_list.remove(a_fix)

            defines += bug_list + [a_fix]
            udefines += fix_list
        else:
            defines += bug_list
            udefines += fix_list
elif args.answer:
    WORKING_DIR = ANSWER_DIR
    defines = []
    udefines = bug_list + fix_list + debug_list
else:
    print(f"Error: must choose between at least --dev --release --answer or --test")
    exit(1)


# 2a Create destination directory
create_dir(WORKING_DIR, git=with_git)


# 2b Apply tags to engine and level files
if args.dest_dir is None:
    DEST_DIR = os.path.join(
        WORKING_DIR,
        LEVEL_NAME
        # metadata["level_name"].lower().replace(" ", "_")
    )
else:
    DEST_DIR = args.dest_dir
DEST_ENGINE_DIR = os.path.join(DEST_DIR, "engine")
create_dir(DEST_DIR, clear=args.clear, git=with_git)
if metadata["engine_name"] != "none":
    create_dir(DEST_ENGINE_DIR)


def copy_with_unifdef(
    in_file: str, out_file: str, defines: list[str] = [], udefines: list[str] = []
):
    """Copy the file to destination directory by applying unifdef"""
    # outfile is basename of the in_file in the dest directory
    apply_unifdef(in_file, out_file, defines, udefines)

def copy_removing_comments(
    in_file: str, out_file: str
):
    """Copy the file to destination directory, removing comments and keeping only one language"""
    # outfile is basename of the in_file in the dest directory
    strip_comments(in_file, out_file)



def process_file(
    src_file: str, defines: list[str] = [], udefines: list[str] = [], engine=False
):
    mklog.info(f"Processing {src_file}")
    if not os.path.exists(src_file):
        mklog.warning(f"{src_file} does not exist!")
        return

    dest_dir = DEST_DIR
    if engine:
        dest_dir = DEST_ENGINE_DIR
    out_file = os.path.abspath(os.path.join(dest_dir, os.path.basename(src_file)))

    if args.dev and (not engine and "engine_bug" in metadata):
        # in development mode, just symlink the directories and files
        # symlinking everything (no hard links) so it still works after a git 
        # update that modifies the files
        if not os.path.exists(out_file):
            abs_src_file = os.path.abspath(src_file)
            os.symlink(abs_src_file, out_file)
            mklog.info(f"symliking {out_file} to {abs_src_file}")
        else:
            if not os.path.islink(out_file):
                mklog.error(f"File should be a symlink but is not: {out_file} (from {src_file})")
                exit(1)
            mklog.info(f"already present: {src_file}")

    elif args.test:
        # in test mode, we must still remove @agdb clutter, and generate the 
        # correct version w.r.t. the language (will only keep one version of 
        # each 'printf')
        if os.path.isfile(src_file):
            ## Old version, was simply copying the file to the destination directory
            # shutil.copy(src_file, dest_dir)
            # mklog.info(f"Copied {src_file}")
            copy_removing_comments(src_file, out_file)
            mklog.info(f"Removed comments and applied language to {src_file}")
        else:
            mklog.warning(f"Cannot find file {src_file}")

    else:
        # if the file a .c or .h apply unifdef
        if src_file.endswith(".c") or src_file.endswith(".h"):
            copy_with_unifdef(src_file, out_file, defines, udefines)
            mklog.info(f"Applied unifdef to {src_file}")

        elif os.path.isfile(src_file):
            # simple copy to the destination directory
            shutil.copy(src_file, dest_dir)
            mklog.info(f"Copied {src_file}")
        else:
            mklog.warning(f"Cannot find file {src_file}")


def process_directory(
    src_dir: str, defines: list[str] = [], udefines: list[str] = [], engine=False
):
    """Applies copy_with_unifdef to all the files in a directory.
    In development mode, creates hard links for files and don't apply unifdef"""
    for file_path in os.listdir(src_dir):
        mklog.debug(f"Checking if need to process {file_path}")

        # skip files that starts with _, hidden files, vim backup files and
        # other files we want to hide
        filename = os.path.basename(file_path)
        if (
            filename.startswith("_")
            or filename.startswith(".")
            or filename.endswith("~")
            or filename in ["tests.c", "logs.txt"]
            or filename.endswith(".tmx")
            or filename.endswith(".py")
        ):
            continue
        fpath = os.path.join(src_dir, file_path)
        process_file(fpath, defines, udefines, engine)


# === copy engine files

if metadata["engine_name"] != "none":
    try:
        dirs = os.listdir(ENGINES_DIRS)
    except FileNotFoundError as e:
        run(['python3',AGDBENTURES_DIR+"/make_engine.py"])
        dirs = os.listdir(ENGINES_DIRS)
    SOURCE_ENGINE_DIR = ""
    for dir in dirs:
        if metadata["engine_name"] in dir:
            SOURCE_ENGINE_DIR = os.path.join(ENGINES_DIRS, dir)
    if SOURCE_ENGINE_DIR == "":
        run(['python3',AGDBENTURES_DIR + "/make_engine.py", "--which", metadata["engine_name"]])
        dirs = os.listdir(ENGINES_DIRS)
        for dir in dirs:
            if metadata["engine_name"] in dir:
                SOURCE_ENGINE_DIR = os.path.join(ENGINES_DIRS, dir)

    mklog.info(f"Source engine dir {SOURCE_ENGINE_DIR}")

    process_directory(SOURCE_ENGINE_DIR, defines, udefines, engine=True)

    mklog.info(
        f"Prepared engine files with #defines {defines} and negation {udefines}"
    )

# if engine bug mode, apply bug patch
if metadata["engine_bug"] is not None and not args.answer:
    PATCHFILE = os.path.join(
        AGDBENTURES_DIR, "engines", "bug_patchs", metadata["engine_name"]
    )
    mklog.info(f"Applying bug patch {PATCHFILE} to engine dir {DEST_ENGINE_DIR}")

    if os.stat(PATCHFILE).st_size == 0:
        mklog.error(f"Cannot apply empty bug patch {PATCHFILE}!")
        mklog.error(f"Either create a real bug patch, or remove the `engine_bug` line in main.c")
        exit(1)

    command = ["patch", "-d", DEST_ENGINE_DIR, "-i", PATCHFILE, "--force"]
    if args.dev or args.test:
        command.append("-D BUG")
    mklog.debug(f"Patch command: {command}")

    try:
        res = run(
            command,
            capture_output=True,
            check=True,  # raises CalledProcessError if returs non-zero exit code
        )
        mklog.debug(f"patch stdout: {res.stdout.decode().strip()}")
        if res.stderr:
            mklog.warning("There where messages on stderr:")
            mklog.warning(f"patch stderr: {res.stderr.decode()}")
    except CalledProcessError as e:
        mklog.error(f"Command failed with return code {e.returncode}")
        if e.stdout:
            mklog.error(f"stdout is {e.stdout.decode()}")
        if e.stderr:
            mklog.error(f"stderr is {e.stderr.decode()}")
        else:
            mklog.error("No output captured on stderr")
        raise e



# === copy level files
if SOURCE_LEVEL_DIR:
    process_directory(SOURCE_LEVEL_DIR, defines, udefines)
else:
    process_file(MAIN_SOURCE_FILE, defines, udefines)
    base, _ = os.path.splitext(MAIN_SOURCE_FILE)
    PYTHON_FILE = base + ".py"
    TMX_FILE = base + ".tmx"
    process_file(PYTHON_FILE, defines, udefines)
    process_file(TMX_FILE, defines, udefines)


mklog.info(f"Prepared level files with #defines {defines} and negation {udefines}")


# 3 Creates the Makefile

# template Makefile
# $ in the Makefile are escaped as $$
# /!\ TABS HAVE TO BE HARD TABS AND NOT SPACES
base_template_string = """
MAINPROG := $exec_name
SRCS = $$(wildcard *.c)
OBJS = $$(SRCS:.c=.o)

CC := gcc
CFLAGS := -O0 -g$warnings $bugflags_devtest

all: $$(MAINPROG)

$$(MAINPROG): $$(OBJS)
$compile_engine\t$$(CC) $$(CFLAGS) $$(OBJS) $link_engine -o $$@

.c.o: $git_call
	$$(CC) $$(CFLAGS) -c $$< -o $$@
$git_rule

clean:
	rm *.o

clear: clean
	rm $exec_name

.PHONY: all clean clear commit $phony_devtest
"""

devtest_head = "BUGFLAGS := $bug_defines\n"
devtest_first_tail = ""
devtest_tail = """
bug:
	make all BUGFLAGS="$bug_defines"

answer:
	make all BUGFLAGS="$answer_defines"
"""

# apply template
if args.dev or args.test:
    # generate rules for fix_* defines
    phony_devtest = "bug"
    for f in udefines:
        rule = f.lower()

        if devtest_first_tail == "":
            devtest_first_tail = f"""
fix: {rule}

"""

        devtest_tail = (
            f"""
{rule}:
	make all BUGFLAGS="-D{f} $bug_defines"
        """
            + devtest_tail
        )
        phony_devtest += " " + rule

    template_string = devtest_head + base_template_string + devtest_first_tail + devtest_tail
    bug_defines = " -D".join([""] + defines + debug_list)
    answer_defines = " -D".join([""] + debug_list)
    warnings = " -Wall -Wextra"
    bugflags_devtest = "$(BUGFLAGS)"  # only one $ as it is in substituted string
else:
    template_string = base_template_string
    bug_defines = ""
    answer_defines = ""
    warnings = ""
    phony_devtest = ""
    bugflags_devtest = ""

if with_git:
    git_rule = "commit:\n\t-git commit -am 'debugging' > /dev/null"
    git_call = "\n\t@$(MAKE) commit 2>&1 > /dev/null"
else:
    git_rule = ""
    git_call = ""

if metadata["engine_name"] != "none":
    link_engine = "engine/engine.a"
    compile_engine = "\tmake -C engine CFLAGS=\"$(CFLAGS)\"\n"
else:
    link_engine = ""
    compile_engine = ""

template_makefile = Template(template_string)
makefile_string = template_makefile.substitute(
    exec_name=metadata["exec_name"],
    warnings=warnings,
    git_rule=git_rule,
    git_call=git_call,
    bug_defines=bug_defines,
    answer_defines=answer_defines,
    link_engine=link_engine,
    compile_engine=compile_engine,
    phony_devtest=phony_devtest,
    bugflags_devtest=bugflags_devtest,
)

# write it into Makefile
make_path = os.path.join(DEST_DIR, "Makefile")
if os.path.exists(make_path):
    mklog.info("Updating " + make_path)
else:
    mklog.info("Creating " + make_path)
with open(make_path, "w") as makefile:
    print(makefile_string, file=makefile)


# making the initial commit
if with_git:
    mklog.info("Creating git repository for level...")
    for gitargs in [
        ["add", "*.c"],
        ["add", "*.h"],
        ["add", "engine/"],
        ["commit", "-m", "initial commit"],
        ["tag", "init"],
    ]:
        p = run(["git"] + gitargs, cwd=DEST_DIR, stdout=PIPE, stderr=PIPE)
        mklog.debug("git messages: " + p.stdout.decode())
        mklog.debug("git errors: " + p.stderr.decode())

print(f"Level created in {DEST_DIR}")

if args.no_compile:
    exit(0)

mklog.info(f"Testing level compilation")

result = run(
    ["make", "--always-make"],
    cwd=DEST_DIR,
    capture_output=True,
)
if result.returncode == 0:
    print("Compilation sucessful")
    exit(0)

# otherwise, show compilation errors
msg = (
    "Error(s) during compilation!\n"
    + result.stdout.decode()
    + "\n"
    + result.stderr.decode()
)

print(msg)
