#!/usr/bin/bash
#
# Very basic way of generating bug patches.
# Beware not to overwrite existing patches (git is your friend)
#

BUG_DIR="bugged_engine_versions"
REF_DIR="engine_versions"

if [ ! -d $BUG_DIR ] ; then
    echo "$BUG_DIR does not exist, creating it"
    mkdir -p $BUG_DIR
    echo "Now, copy existing engine version into it, make modifications, then re-run $0"
    exit 0
fi

#rm "$BUG_DIR" -rf
#cp "$REF_DIR" "$BUG_DIR" -r

for engine_dir in "$BUG_DIR"/*/; do
    ## Check if the glob gets expanded to existing files.
    ## If not, f here will be exactly the pattern above
    ## and the exists test will evaluate to false.
    [ -d "$engine_dir" ] || (echo "no bugdir found" ; break)

    ## This is all we needed to know, so we can break after the first iteration

    BASE_ENGINE_DIR=$(basename $engine_dir)
    echo "Generating bug patch for ${BASE_ENGINE_DIR}"
    PATCH_FILE_NAME=${BASE_ENGINE_DIR#*_}
    PATCH_FILE="engines/bug_patchs/$PATCH_FILE_NAME"

    if diff -ur $REF_DIR/$BASE_ENGINE_DIR $BUG_DIR/$BASE_ENGINE_DIR > $PATCH_FILE ; then
        echo "Warning: there is no difference between your versions"
        continue
    fi

    if [ ! -s $PATCH_FILE ] ; then
        echo "ERROR: patch is empty, but diff returned non-zero!"
    fi
done
