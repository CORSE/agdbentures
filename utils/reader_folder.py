from asyncore import read
"""
Removes @AGDB comments and reads @TAGS from all files frm a folder.
The user should not write code on the same line as a comment like this:
code++; // @AGDB
code++; /* @AGDB */ code++;
This leads to poor indentation in the output (the program stays the same)
The user should not put several @AGDB comments on the same line like this:
/* @AGDB */ // @AGDB
Only the first will be deleted (why would you write that anyway?)
"""

import os
import sys
import reader_file

if __name__ == "__main__":
    if (len(sys.argv) < 2):
        print("Usage:", sys.argv[0], "<folder> [exercise]*")
        exit(1)
    
    dir = sys.argv[1]

    if (not os.path.isdir(dir)):
        print(dir, "is not a directory.")
        exit(1)

    for e in os.listdir(dir):
        if (os.path.isfile(dir + "/" + e) and (e.endswith(".c") or e.endswith(".h"))):
            a = [dir + "/" + e]
            reader_file.main([dir + "/" + e].extend(sys.argv[2:]))