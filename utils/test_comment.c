#include <stdio.h>
#include <stdlib.h>

// @AGDB This is not an exercise
// @TAGS baba is test
int main(int argc, char const *argv[]) {

    for (unsigned i = 0; i < argc; i++) {
        printf("%s ", argv[i]); // @AGDB comment at the end of a line
    }
    int a = 1;
    // @AGDB indented comment
    // comment to keep
    printf("\n");
    /* @AGDB comment on
    several lines */
    a++;
    /* @AGDB multiline but on one line */ a++;

    /* @AGDB */
    return EXIT_SUCCESS;
    // @TAGS and keke
}
