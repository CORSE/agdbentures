#include <stdio.h>
// https://linux.die.net/man/1/unifdef

// @TAGS call-by-reference typing
/* @AGDB deux exercices imbriqués */
#ifdef AGDB_pointeur
// increments a
void plusplus(int a) {
	#ifdef AGDB_typage
	a = a + '1';
	#else
	a = a + 1;
	#endif
#else
// increments a given its pointer
void plusplus(int * a) {
	#ifdef AGDB_typage
	*a = *a + '1';
	#else
	*a = *a + 1;
	#endif
#endif
}

int main(int argc, char const *argv[]) {

	// @AGDB Exercice illustrant le typage de C
	float x = 1.0;
	#ifdef AGDB_typage
	printf("%d\n", x);
	#else
	printf("%f\n", x);
	#endif

	int y = 10;
	#ifdef AGDB_pointeur
	// @AGDB imprime "10"
	plusplus(y);
	printf("%d\n", y);
	#else
	// @AGDB imprime "11"
	plusplus(&y);
	printf("%d\n", y);
	#endif
}
