"""
Removes @AGDB comments and reads @TAGS from a single file.
The user should not write code on the same line as a comment like this:
code++; // @AGDB
code++; /* @AGDB */ code++;
This leads to poor indentation in the output (the program stays the same)
The user should not put several @AGDB comments on the same line like this:
/* @AGDB */ // @AGDB
Only the first will be deleted (why would you write that anyway?)

After that, calls unifdef so the file is ready with the exercises you want
All #define tags for exercices must start with AGDB_ to not confuse with other #define to keep.
"""

"""unifdef
unifdef -D <defined> -U <undefined> -o <output> <input>
"""

import os
import sys

def main(argv):
    filename = argv[0]
    if (not os.path.isfile(filename)):
        print(filename, "is not a file.")
        exit(1)

    tags = []
    exercises = []
    
    with open(filename) as f:
        try:
            out_dir = os.mkdir("tmp_out")
        except FileExistsError:
            pass

        with open("tmp_out/"+os.path.split(filename)[-1], "w") as o:
            c = False
            for line in f:
                # list all ifdef used for exercises
                if line.lstrip()[:12] == "#ifdef AGDB_" and exercises.count(line.lstrip()[12:-1]) == 0:
                    exercises.append(line.lstrip()[12:-1])

                # if we are in a multi-line comment
                if (c):
                    k = line.find("*/")
                    if (k != -1):
                        line = line[k+len("*/"):].strip() + "\n"
                        if (len(line.strip()) == 0):
                            line = ""
                        c = False
                    else:
                        line = ""
                else:
                    # search for a new multi-line comments
                    j = line.find("/* @AGDB")
                    if (j != -1):
                        k = line.find("*/")
                        if (k != -1):
                            line = line[:j].rstrip() + " " + line[k+len("*/"):].lstrip()
                            c = False
                        else:
                            c = True
                            line = line[:j]

                    # search for an AGDB comment in the line
                    i = line.find("// @AGDB")

                    # removes AGDB comment if found
                    if (i != -1):
                        line = line[:i] + "\n"

                    # searchs for TAGS
                    l = line.find("// @TAGS")

                    if (l != -1):
                        tags += line[l+len("// @TAGS")+1:-1].split(" ")
                        line = ""

                    # if there was no code before the comment,
                    # remove the line entirely
                    if (len(line.strip()) == 0 and (i != -1 or j != -1)):
                        line = ""
                    
                print(line, end="", file=o)
    print("Tags found:", tags)
    print("Exercises found:", exercises)

    # Now it's time to apply unifdef
    # the names given as arg will be defined, the names found but not passed will be undefined
    ex_to_keep = []
    ex_to_remove = []
    for e in exercises:
        try:
            i = argv.index(e, 1)
        except ValueError:
            ex_to_remove.append(e)
            continue
        ex_to_keep.append(e)
        
    print("Now applying unifdef. You asked to keep the exercises:", ex_to_keep)
    print("The following exercises will be removed:", ex_to_remove)

if __name__ == "__main__":
    if (len(sys.argv) < 2):
        print("Usage:", sys.argv[0], "<filename> [exercise*]")
        exit(1)
    main(sys.argv)
