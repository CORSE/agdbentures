all: easytracker engine_versions

easytracker:
	make -C easytracker/easytracker/gdb

engine_versions:
	python make_engine.py

tests: pytest testengines testlevels

pytest:
	@echo Unit testing with pytest
	pytest -rxXs

pytest-info:
	@echo Unit testing with pytest, showing all info
	pytest -rA

testengines:
	@echo Testing engines
	@./make_engine.py --test
	make -C engine_tests

testlevels:
	@echo Testing levels
	./tests/testLevels.py

testgui:
	@echo Testing GUI
	./agdbentures --headless --test --silent

.PHONY: all easytracker tests pytest testengines leveltest
