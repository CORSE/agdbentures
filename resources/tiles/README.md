How to make multi-tile objects?
-------------------------------

Tiled does not allow directly to create multi-tile objects from a tileset.
The workaround is the following. Supposing you want to create an object of 
height 2 from the BaseChip_pipo tileset:
1- create a new very small map of 1x2 tiles (32 pixel squares)
2- paste the two tiles into your map as basic tiles
3- File -> export as image and save the image as a .png
4- Move the file in the `objects/` directory
5- Add the object in the "objects" collection of image in Tiled
   (a collection of objects is a special tileset)
6- You can now use this object in your maps!
