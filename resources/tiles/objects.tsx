<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.8" tiledversion="1.8.2" name="objects" tilewidth="32" tileheight="64" tilecount="3" columns="0">
 <grid orientation="orthogonal" width="1" height="1"/>
 <tile id="1">
  <image width="32" height="64" source="objects/lamp-off.png"/>
  <animation>
   <frame tileid="1" duration="500"/>
   <frame tileid="2" duration="500"/>
  </animation>
 </tile>
 <tile id="3">
  <image width="32" height="64" source="objects/lamp-off.png"/>
 </tile>
 <tile id="2">
  <image width="32" height="64" source="objects/lamp-on.png"/>
 </tile>
</tileset>
