[idées de bug](./NOTES.org)

# ChangeLog de dev

## Refactorisation Map -> World (juillet 2024)
(message du mattermost)

- il n'y a plus de level.map
- il y a à la place un level.world
- le world, c'est le point d'entrée pour récupérer les infos sur l'état en 
  cours de l'inférieur
- ce qui peut être fait automatiquement l'est, et est en fonction de l'engine 
  (par exemple player_x et player_y vont être cherchés au bon endroit, soit var 
  globales, soit current_map->player_* par exemple)
- la carte courante current_map est mise à jour aussi en scannant les 
  caractères de la string de l'inférieur. Pour l'instant seuls les murs sont 
  mis à jours (ça marche même si width et height changent, voir le FIX_ de 
  input_command)
- par contre j'ai pas encore bien réussi à gérer le exit selon les niveaux
- ce qui n'est pas mis à jour automatiquement, on peut récupérer en demandant 
  à world la valeur de variable mais plus en demandant directement au tracker 
  => utiliser par exemple self.world.get_main_variable("toto") => il manque 
  forcément des fonctions, je rajouterai au fur et à mesure
- la manière de mettre à jour les objets est plus puissante. Si un objet a des 
  champs "var_x" et "var_y" non None, automatiquement on va chercher dans 
  l'inférieur les valeurs => on peut soit mettre juste une variable (ex: 
  "objet_x") => on peut aussi chercher dans les structs et tableaux (ex dans 
  avoid monsters: skeleton.var_x = ("monsters_data", f"->info[{m}].x")

Au final, le code custom de pas mal de niveau devrait se simplifier pour tout ce qui est mise à jour.
Pour l'instant, j'ai pas merge dans devel pour pas tout péter, donc j'ai fait une branche experimental.
Recommandation : si vous bosser sur un niveau, essayer de merge avec experimental pour voir si vous arrivez à faire tourner le niveau, et sinon le corriger pour que ça rentre dans le nouveau framework.

TODO : le changement de map automatique en fonction du nom de la carte, gérer mieux l'exit, gérer d'autres symboles sur la map (avec création à la volée, par exemple si des 'G' arrivent, il faut faire des gardes à la demande).



# Création d'un niveau


### Structure d'un niveau

Un niveau se compose de :

* `main.c` ([structure](#le-main))
* `level.py` ([structure](#levelpy))
* `map.tmx` ([comment créer d'une carte](#carte))
* un `engine` (dans la majorité des cas) ([utilisation](#engine))

### Fonctionnement d'un niveau

Les niveaux dans Agdbentures se trouvent tous dans le répertoire `levels`.

Cependant, le jeu est censé avoir différentes versions d'un code (une bugué et une solution). Il y aura donc une copie du code lors du déploiement de votre niveau. La copie pourra se faire dans plusieurs destinations différentes (au choix), décrites dans `./agdbentures --help`. (answer, dev et test)

De cette manière, l'utilisateur n'aura jamais accès au fichiers d'origine de votre niveau.

## Engine

Très vite dans l'avancée du jeu, les niveaux commencent à utiliser des engines (moteur de jeu). Ils permettent la séparation dans différents fichiers des actions du joueur et des interaction avec la carte. Ces engines sont voués à évoluer. Dès lors, ils ne se trouveront plus dans votre répertoire de travail mais dans `engine_versions/`.

Pour intégrer un engine, inscrivez-le dans les métadonnées du niveau.
([plus d'informations sur les métadonnées](#utilisation-des-métadonnées-))
Ainsi, au lancement du jeu, la partie de l'engine utile pour ce niveau sera copiée dans le dossier du joueur (`working_directory`).

Pour en savoir plus sur le fonctionnement interne des engines se réferer à [la documentation](./engines/API.md).

> **À DÉPLACER**
>
> En réalité, l'engine source se trouve dans le répertoire `engine` et toutes les copies que nous avons se font via des `#ifdef`.


## main.c

Le fichier main va contenir beaucoup d'information utile au jeu via ses [métadonnées](#utilisation-des-métadonnées-).

En règle générale, **votre code devra inclure `engine/agdbentures.h`.**\
Cela va permettre de récupérer l'engine pour votre jeu. Même s'il n'est pas présent au moment du développement, l'engine sera tout de même là au final car il est copié lors du déploiement.

L'objectif d'un niveau étant de faire découvrir des bugs au étudiants, il faut évidemment en glisser au moins un dans votre propre `main.c`. Cependant nous voulons avoir deux versions, une « fonctionnelle » et l'autre bugué.\
Pour ce faire nous utilisons cette structure:
```c
// ...
#ifdef BUG
 // code bugué ici 
#else
 // code servant de solution 
#endif
// ...
```

Si vous voulez rajouter des lignes uniquement dans certaines circonstances vous pouvez aussi faire cela :
```c
#ifdef BUG
// code bugé
#endif
```
ou bien si vous voulez ne rien avoir dans le code bugué :
```c
#ifndef BUG
// solution ici
#endif
```

Vous pouvez retrouver des cas d'utilisation de cette structure dans plusieurs niveaux, par exemple : 

- [grosse structure avec if else et end](./levels/medium/local_struct/main.c)
- [petite structure avec juste en ifndef](./levels/tutorial/01_first_bug/main.c)

Vous pouvez aussi créer des bugs dans l'engine du jeu pour certains niveaux, auquel cas vous le spécifierez dans les métadonnées pour rendre l'utilisation future de votre niveau plus simple.
Un exemple de niveau avec bug sur l'engine est [command_args](./levels/basic/command_args).



## Carte

Le jeu utilise des cartes en `.tmx`.

Le plus simple reste d'aller voir un exemple comme la carte du niveau [local_struct](./levels/medium/local_struct/main.tmx).

- Les lignes commençant par `tileset` permettent de récupérer les sprites utilisés dans notre jeu et seront communes à tous les niveaux (tant que rien ne sera rajouté dans les ressources).
- Les champs `width` et `height` sont ceux qui permettent de choisir la taille de votre carte. Il faut que ceux définis tout en haut de votre fichier soient cohérent avec ceux de chaque layer.
- Une layer est une couche du niveau ayant certaines propriétés. Vous retrouverez surtout les champs `floor` , `decorations`, `decorations_top` et `walls`. Chacune de ces layers va être une matrice de taille `width` × `height` contenant des numéros. Ces numéros correspondent à l'ID d'un sprite.

Pour construire une carte, nous vous recommandons d'utiliser tiled. (installation :)
```bash
apt-get install tiled
```

Dans le logiciel, vous pourrez observer tout les objets à votre disposition pour la création de votre niveau, ainsi que leur id correspondant si vous souhaitez construire la carte directement via le fichier `.tmx`



## level.py

Le fichier level.py va avoir plusieurs rôles dans l'utilisation d'un niveau.

* tester la bonne conception du niveau, voir [Test](#utilisation-du-module-de-test)
* Valider la solution proposée par un joueur, voir [Validation](#utilisation-du-module-de-validation)
* Discuter avec l'interface graphique, voir [Interface graphique](#lien-avec-linterface-graphique)\


Sa structure ne change(ra) pas. Le fichier commence par spécifier comment il s'exécute puis inclus les différentes ressources qui sont nécessaires au niveau.
```python
#!/usr/bin/env python3

from level.level_abc import AbstractLevel
from loader.loader import load_level, run_level, test_level
from level.objects import find_var_in_frame, get_top_map
from utils import lvl
import graphic.constants as cst
```

Ensuite, pour instancier votre niveau :
```python
class Level(AbstractLevel):
	
```

L'instance se structure autour des fonctions suivantes :

```python
def arcade_custom_first_start(self):
	"""cette fonction est appelée quand le joueur clique sur 'restart'"""
	
def arcade_custom_restart(self):
	"""cette fonction est appelée quand le joueur va lancer le niveau"""
	
def pre_validation(self):
	"""cette fonction sert à vérifier que le joueur n'a pas triché.
	Elle place des repères avant l’exécution de son code"""
	
def post_validation(self):
	"""cette fonction sert aussi à vérifier que le joueur n'a pas triché.
	Elle vérifie la valeur des points de contrôle (après modifications du joueur).
	Si celles-ci ne sont pas les valeurs attendues,
	elle envoie au joueur que son code n'est pas la solution attendue."""
	
def test(self):
	"""cette fonction sert (en interne) à vérifier qu'un niveau (que l'on développe)
	a bien le comportement attendu en mode BUG et en mode solution"""
```



Maintenant, vous allez pouvoir définir d'autres fonctions spécifiques à votre niveau. 

En règle générale vous utiliserez, pour les appeler, l'une de ces fonctions :
```python
self.register_breakpoint("foo", callback)
# va lancer callback() de level.py si la fonction 'foo' est appelée dans le code C.
	
self.register_leave_function("foo", callback)
# va appeler callback à la sortie de la fonction 'foo' dans le code C
```

Aussi, pour récupérer la valeur d'une variable du code C :
```python
chk = self.checker.tracker
chk.get_variable_value_as_str('x',"type")
# renvoie la valeur de la variable 'x' du code C en la typant avec 'type'
```
Par exemple, pour récupérer l'entier `var_entier` et la stocker dans une variable `my_var`, on fait :
```python
ma_var = chk.get_variable_value_as_str('var_entier',"int")
```

La fonction suivante permet d'appeler une fonction `callback()` dès que la valeur d'une variable `x` du code C est modifiée :
```python
self.checker.register_watch('{x}', callback)
```



## Utilisation du module de validation

Les tests fait lors de la validation du niveau par le joueur se font en deux parties.

Pour être appelées, ces fonctions attendent que le joueur ait atteint la sortie du niveau. À ce moment s'enclenche la validation.

### `pre_validation`

C'est là qu'on initialise les fonctions qui serviront à vérifier que le joueur modifie son `main.c` de la manière attendue.

Au début de la phase de validation nous allons donc initialiser des valeurs dans `pre_validation` ainsi que donner les différents inputs nécessaires au bon fonctionnement du niveau.

Pour ce faire nous allons toujours utiliser la même fonction à placer dans votre `pre_validation` pour éviter une boucle infinie :
```python
self.checker.append_inputs() :
	"""cette fonction va stocker des inputs et les envoyer au jeu
	pendant la validation et quand celui-ci attend une entrée."""
```

Par exemple, dans un niveau qui nous demanderait d'aller à droite, puis en haut, puis à gauche, de taper `BUY` et de descendre, nous ferions :
```python
self.checker.append_inputs(
    [
        'RIGHT',
        'UP',
        'LEFT',
        'BUY',
        'DOWN',
    ]
)
```


### `post_validation`

Ici on vérifie que tout s'est bien passé dans `pre_validation`.

Sinon, on fait remonter l'erreur afin d'annuler la validation du niveau (en cas de comportements « non désiré »)

Pour ce faire nous allons fonctionner de cette manière :

```python
if valeur_actuelle != valeur_voulue:
    self.checker.failed("{message au joueur expliquant le refus de sa solution}")
```

Si la fonction se termine sans messages d'erreur, on peut considérer que la solution du joueur est valide. On lui permet alors d'accéder au niveau suivant.


## Utilisation du module de test

Les niveaux doivent être testés pour s'assurer de leur bon fonctionnement lors de leur mise en place dans le jeu.

Il existe un fichier permettant de tester automatiquement les niveaux.
Le fonctionnement de ce fichier consiste à lancer votre niveau puis exécuter les actions définies dans le la fonction de test de votre `level.py`.

Cette fonction test aura toujours la même structure :
```python
def test(self):
    import tests.lib_test as T
    
    # si le test vérifie les actions censés engendrer une défaite :
    self.recompile_bug()
    # émuler des actions
    T.expect_defeat()
    
    # si les actions sont censés valider le niveau :
    self.recompile_answer()
    # émuler des actions
    T.expect_victory()
    
    # dans tout les cas :
    self.run()
    
```

Plusieurs tests peuvent ainsi s'enchaîner en utilisant cette structure.

Maintenant vient la question de comment envoyer des commandes.
Pour le savoir rendez vous [dans `lib_test.py`](./lib/tests/lib_test.py) ou vous trouverez une classe `_Cmd`.

Dans cette classe sont définies plusieurs instruction qui pourront être envoyées directement à GDB.

Lorsque vous effectuerez une action du type `T.foo()`, cette commande va directement être envoyé dans une fonction, créée dynamiquement, permettant de construire la commande voulue. Celle-ci sera ensuite interprétée dans le fichier [`level_test.py`](./lib/level/level_test.py), dans la fonction run.

Ainsi, les commandes sont stockées jusqu'à ce que `self.run()` soit appelé. L'appel de `self.run()` entraînera leur exécution, tout en respectant l'ordre dans lequel elles ont été stockées.\
Il faut noter que l'envoi d'inputs avec la commande `T.send_input("ma_chaine")`, doivent se faire avant `T.continue`. Sinon, vous serez dans la boucle sans pouvoir envoyer les commandes car celles-ci ne sont plus lues.

L'exécution des tests se fait comme suit :
```bash
# dans agdbentures/
./tests/testLevels.py
```
Note : `./tests/tesLevels.py` dispose d'une aide si besoin. (appelable avec l'option `-h`)

## Communication avec l'interface graphique

Pour interagir avec l'interface graphique depuis votre fichier `level.py`, il existe des payloads, dont l'usage principal sera dans le fichier [sp_objects.py](./lib/graphic/sprites/sp_objects.py).

Leur structure générale consiste à avoir :

- un topic 
- une action
- un objet (sur lequel faire cette action)
- ses paramètres (si cette action a besoin de plus d'informations)

Par exemple, pour cacher un élément de la layer `décoration_top` :

```python
payload = {
    "topic": "sprites",
    "action": "hide",
    "layer": "decorations_top",
    "locations": [
        ({position en x}, {position en  y}),
    ],
}
self.send_to_gui(payload)
```

Pour retrouver plus d’exemples, consulter des `level.py` de niveaux déjà existants.


## Utilisation des métadonnées

Les métadonnées sont essentielles au fonctionnement du jeu.
Elles vont préciser :

- le nom du niveau
- le mode de jeu
- les commandes utilisables dans l'interface graphique
- le nom de l'engine utilisé
- la ou les cartes utilisées pour ce niveau
- les messages à envoyées au joueur par le « wise old man »
- les coordonnées de certains objets ainsi que certains de leurs attributs comme le caractère qui les représente
- des vérifications automatiques (potentiellement)

Pour voir comment toutes ces choses sont initialisées il est recommandé de consulter les niveaux déjà existants.
Vous avez un bon exemple [dans le niveau crêpe](./levels/medium/crepes/main.c).\

Les métadonnées sont initialisées grâce aux tags suivants :
```c
/* @AGDB
 * 
 * métadonnées
 *
 */
```

Nous vous recommandons aussi d'y introduire un description de votre bug et une explication de l'objectif général du niveau pour qu'il soit compréhensible par un futur développeur.

Mettre les commentaires dans les métadonnées permet aussi que seuls les développeurs d'Agdbentures les voient. Le joueur ne sera jamais informé de leur existence.

