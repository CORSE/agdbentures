#include "debug.h"
#ifndef MAP_H
#define MAP_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#ifdef READ_MAP_STR
#include <assert.h>
#endif

#ifndef ENTITY_STRUCT
#define WALL '#'
#define DOOR 'D'
#define PLAYER '>'
#define EXIT '@'
#define EMPTY ' '
#define WATER '~'
#else
#include "entity.h"
#endif

#ifdef MAP_STACK
#define MAX_MAPS 10

#endif
// La liste des 4 directions, UNKNOWN sert de valeur d'initialisation
typedef enum Direction {
  DIR_UP,
  DIR_DOWN,
  DIR_LEFT,
  DIR_RIGHT,
  DIR_UNKNOWN
} direction;
#ifdef MAP_GRAPH

struct map_s;

typedef struct map_link_s {
  int from_x;
  int from_y;

  struct map_s *destination;
  int dest_x;
  int dest_y;

  struct map_link_s *next;
} map_link;
#endif

#ifndef ENTITY_STRUCT
// Les entités sont des caractères sur la carte
typedef char entity;
#endif

typedef struct map_s {
#ifdef MAP_STACK
  char *name;  // nom unique: "main", "shop", etc.
#endif
  // Les dimensions de la carte
  int width;
  int height;
#ifndef ENTITY_STRUCT
  // La carte est linéarisée en un simple tableau unidimensionel
  entity *tiles;
#else
  // Séparation de la carte entre le "sol" (inclus les murs)
  // et les "objets" (entités).
  // Ce sont maintenant des tableaux 2D.
  tile **tiles;  // sol immobile
  entity **entities; // enemis, NPCs, objets, interrupteurs...
#endif
  // Les informations du joueur
  int player_x;
  int player_y;
  direction player_direction;
#ifdef MAP_GRAPH
  // Liens vers d'autres cartes
  struct map_link_s *map_link_list;
#endif
} map;

extern int exit_x;  // Position de la sortie
extern int exit_y;  // Ce sont des variables globales définies dans main.c
#ifdef MAP_STACK

typedef struct {
  map *maps[MAX_MAPS];
  int length;
} map_stack;

map_stack *init_map_stack(void);
#ifdef MAP_GRAPH
void free_map_stack(map_stack *ms, map * current);
#else
void free_map_stack(map_stack *ms);
#endif
map *init_map(const char *name, int height, int width);
#else

// Cree une carte vide avec la dimension demandee
map *init_map(int height, int width);
#endif
// Libere la carte donnee
void free_map(map *map);
#ifdef MAP_STACK

// Retourne la carte sur le haut de la pile de carte (la carte "actuelle").
map *current_map(map_stack *ms);

// Ajoute la carte sur le haut de la pile de carte, elle devient la nouvelle
// carte "active".
void push_on_stack(map_stack *ms, map *m);
// Enleve la carte du haut de la pile de carte, la carte n'est pas detruite.
map* pop_from_stack(map_stack *ms);

// Cree une nouvelle carte en fonction de la chaine de caractere donnee.
// La fonction place aussi le joueur
map *load_map(const char *name, const char *data);
#ifdef MAP_GRAPH

void add_link_to_map(map *m, int y, int x, map *dest_m, int dest_y, int dest_x);
void add_bidirectional_link_to_map(map *m, int y, int x, map *dest_m,
                                   int dest_y, int dest_x);
#endif
#else
#ifdef READ_MAP_STR
// Cree une nouvelle carte en fonction de la chaine de caractere donnee.
// La fonction place aussi le joueur
map *load_map(const char *data);
#endif
#endif
#ifdef ENTITY_STRUCT
#ifdef ENTITY_PROPERTIES

int parse_tile(char symbol, tile *tile);
#else
int parse_floor(char symbol, tile *floor);
#endif

entity parse_entity(char symbol);
#endif

// Fonctions pour le placement/suppression d'entite et du joueur.
// La fonction de placement du joueur ne gere pas la direction
// Il faut la gerer dans le programme principal.
void place_player(map *map, int y, int x);
void place_entity(map *map, entity entity, int y, int x);
entity get_entity(map *map, int y, int x);
#ifdef ENTITY_STRUCT
tile get_tile(map* map, int y, int x);
#endif
entity remove_entity(map *map, int y, int x);

#ifdef MAP_ENTITY_STACKING
entity find_category_in_stack(map *map, entity_category category, int y,
                               int x);
entity find_property_in_stack(map *map, entity_property property, int y,
                               int x);
#endif
/**
 * Affiche une version ASCII de la carte \p map.
 */
void show_map(map *map);

#ifndef EVENTS  // With events, declared in prefab_events.h
void verify_exit(map* map);
#endif
#endif // MAP_H
