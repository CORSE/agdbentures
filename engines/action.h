#include "debug.h"
#ifndef ACTION_H
#define ACTION_H

#include <stdbool.h>
#include "map.h"

#ifdef LEVERS
#define DEACTIVATED 0
#define ACTIVATED 1

#endif
// Fonctions de communications entree/sortie
void message(char msg[]);
void prompt(char msg[], char* dest, int max_len);

/**
 * Fonctions qui déplacent le joueur
 *
 * @return 1 s'il y a eu déplacement, 0 sinon.
 */
int player_up(map *map);
int player_down(map *map);
int player_left(map *map);
int player_right(map *map);

// Fonctions pour deplacer l'entite depuis la position donnee
void up(map *map, int y_start, int x_start);
void down(map *map, int y_start, int x_start);
void left(map *map, int y_start, int x_start);
void right(map *map, int y_start, int x_start);
#ifdef COMMAND_ARGS

/**
 * Variants of the function with multiple steps
 *
 * @return the number of moves that took place.
 */
int player_up_n(map *map, unsigned n);
int player_down_n(map *map, unsigned n);
int player_left_n(map *map, unsigned n);
int player_right_n(map *map, unsigned n);
#endif

// Deplace l'entite depuis la position donnee vers la position demandee.
// Verifie que la case d'arrivee est libre et dans la carte.
// Retourne false si finalement rien n'a bouge, true si l'entite a bouge.
bool move_entity(map *map, int fy, int fx, int ty, int x);
/**
 * Variante de la fonction précédente qui s'applique directement au joueur
 * On donne en plus la direction finale du joueur.
 * Le personnage est orienté vers \p dir_dest même le déplacement 
 * échoue.
 */
bool move_player(map *map, int y_dest, int x_dest, direction dir_dest);

#ifdef ENTITY_STRUCT
bool is_obstacle(map *m, int y, int x);
#else
// Retourne si l'entite donnee est un obstacle
bool is_obstacle(entity e);
#endif

// Fonctions internes qui s'occupent des verifications de deplacement
// pour les fonctions move_entity et move_player
// Retourne 1 si le deplacement est possible 0 sinon
int can_player_move_to(map *map, int y_dest, int x_dest);
int can_move_to(map *map, int y_start, int x_start, int y_dest, int x_dest);

// Fonction de deplacement d'entite sans verifications
void move_without_check(map *map, int y_start, int x_start, int y_dest,
                        int x_dest);

// Retourne la direction associee a la chaine de caractere en entree
direction direction_from_string(char *dir_string);

// Calcule la coordonnee a une distance de 1 depuis la position donnee si on
// regarde dans la direction demandee
void look_forward(int pos_y, int pos_x, direction direction, int *dest_y,
                  int *dest_x);
// Meme chose mais a une distance de n
void look_forward_n(int pos_y, int pos_x, direction direction, int n,
                    int *dest_y, int *dest_x);

// Variantes des deux fonctions precedantes mais qui s'applique directement au
// joueur
void player_look_forward_n(map *map, int n, int *dest_y, int *dest_x);
void player_look_forward(map *map, int *dest_y, int *dest_x);

#endif // ACTION_H
