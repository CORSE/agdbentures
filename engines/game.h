#include "debug.h"
#ifdef LEVEL_END
#ifndef GAME_H
#define GAME_H

#include <stdbool.h>

#include "map.h"


#ifdef INVENTORY_ARRAY
#define MAX_INVENTORY_SIZE 10

#endif
#ifdef EVENTS
struct event_s;
#endif
// Structure globale qui garde une instance du moteur qu'on partage entre les
// fonctions
typedef struct game_s {
#ifdef MAP_STACK
  map_stack *map_stack; // La pile de cartes est stockee dans le moteur.
#else
  map *map; // La carte est directement stockee et initialisee a vide dans le
            // moteur.
#endif
#ifdef MAP_GRAPH
  map *current_map;
#endif
#ifdef INVENTORY_ARRAY
  entity inventory[MAX_INVENTORY_SIZE];
#endif

#ifdef EVENTS
  struct event_s *event_list;

#endif
  bool exit_main_loop; // Un booleen qu'on peut utiliser dans une boucle qui
                       // accepte des commandes
  int level_exit_code; // Le code de retour a renvoyer par le programme
} game_instance;

#ifdef MAP_STACK
// Initialisation de la structure
game_instance *init_game();
#else
// Initialisation de la structure qui cree une carte vide de la taille demandee
game_instance *init_game(int height, int width);
#endif
void free_game(game_instance *game);

// Fonction d'affichage simple d'un message dans la console
void say(char *message);

// Fonctions pour terminer un niveau, le programme ne se termine pas tout de
// suite mais fait une derniere iteration si on utilise le booleen
// exit_main_loop.
void level_failed(game_instance *game, int exit_code);
void level_success(game_instance *game);

#ifdef MAP_GRAPH
map *take_map_link(game_instance *game, map *m);

#endif
#endif // GAME_H
#endif
