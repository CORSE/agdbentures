#include "debug.h"
#ifdef INPUT_COMMAND
#ifndef READ_INPUT_H
#define READ_INPUT_H

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "action.h"

#define INPUT_FAILURE 2  // Exit code when an error occurs at reading

// Structure d'une commande qui contient une chaine de caractere et la taille de
// la chaine
typedef struct {
#ifdef COMMAND_ARGS
  char *command_buffer;  // Le buffer contient la commande et les arguments tous
                         // separes par des \0
#else
  char *command_buffer;  // Chaine de caractere terminee par \0
#endif
  size_t buffer_size;  // La taille du buffer pour la chaine

#ifdef COMMAND_ARGS
  int n_args;   // Le nombre d'arguments (0 aucun argument)
  char **args;  // Un tableau de pointeurs contenant les arguments
#endif
} command;

// Attends une commande sur l'entree standard et alloue une nouvelle structure.
command *get_next_command(void);

void free_command(command *c);

/**FR
 * Fonction de base qui va appliquer les commandes UP, DOWN, LEFT, RIGHT avec
 * les comportements par defaut du moteur.
 * L'utilisateur peut reecrire lui meme sa fonction qui applique les commandes.
 * La fonction retourne 1 si elle a reconnu la commande 0 sinon.
 *
 * @return 1 ou plus si la commande a été appliquée avec succès, 0 sinon.
 * Dans le cas d'une commande multiple (e.g., "LEFT 4"), la valeur renvoyée est 
 * le nombre d'applications réussies.
 */
/**EN
 * Tries to apply command on a given map.
 *
 * @return 1 if the command was successfully applied, 0 otherwise.
 */
int apply_input(map *map, const command *c);

// Returns the tile from the map m where the player is trying to go using the command c
entity next_tile(map *m, const command *c);

#endif // READ_INPUT_H
#endif
