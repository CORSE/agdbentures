#include "debug.h"
#ifdef ENTITY_STRUCT
#ifndef ENTITY_H
#define ENTITY_H

#include <stdbool.h>
#include <stdlib.h>

#define WALL '#'
#define PLAYER '>'
#define EXIT '@'
#define EMPTY ' '

#ifdef CUSTOM_ENTITY

typedef enum EntityCategory {
  C_NONE = 0,
  C_MONSTER = 1,  // generic monster 'M'
  C_NPC = 2,      // generic NPC 'A'
  C_ITEM = 3,     // generic item 'I'
  C_PLAYER = 4,
  C_FLAG = 5,     // pinpoint special location
  C_EXIT = 6,     // exit the map or whole level
} entity_category;

#endif

#ifdef ENTITY_PROPERTIES
typedef enum EntityProperty {
  ACTIVABLE = 1,      // switch to activate 'O' or 'X'
  PICKABLE = 2,
  PUSHABLE = 1 << 3,  // (obstacle/can't walk on it) a block that can be pushed
  TRAP = 1 << 4,      // an invisible trap '_'
  OBSTACLE = 1 << 5,  // something blocking movement, e.g., a wall
} entity_property;

#endif
struct entity_s {
  int id;  // identifier to display and use in events
#ifdef CUSTOM_ENTITY
  entity_category category;  // used to group in events, like monster, NPC...
  void *stats;  // any property the entity may have (life points, state...)
#endif
#ifdef ENTITY_PROPERTIES
  entity_property properties;
#endif
  char display_symbol;  // The symbol of the Entity on an ASCII map
#ifdef INVENTORY_ARRAY
    #ifndef ENTITY_PROPERTIES
  bool is_pickable;
    #endif
#endif
#ifdef MAP_ENTITY_STACKING
  struct entity_s *next;
#endif
};
// Une entité est un pointeur vers la structure entity_s
typedef struct entity_s* entity;

typedef enum {
  F_EMPTY,    // unknown/unreachable
  F_GROUND,      // standard floor ' '
  F_WALL,     // (obstacle) '-' or '|' or '+'
  F_WATER,      // can be used in event to drown the player '~'
  F_DOOR,       // (obstacle) '[' or ']'
  F_TARGET,     // can be used in events 'T'
  AREA_WHITE,    // For special levels: color of floor
  AREA_GRAY,     // For special levels: color of floor
  AREA_HASHED,   // For special levels: color of floor
  FLOORTYPE_N   // last one to know how many types there are
} floor_type;

#ifndef ENTITY_PROPERTIES
typedef floor_type tile;  // Une tuile de sol
#else
typedef struct {
  floor_type floor;            // identifier to display the tile
  entity_property properties;  // whether the player can move on it or not
  char display_symbol;
} tile;
#endif

#ifdef ENTITY_PROPERTIES
entity create_entity(char symbol, entity_property properties, entity_category category, void *stats);
#else
    #ifdef CUSTOM_ENTITY
entity create_entity(char symbol, entity_category category, void *stats);
    #else
entity create_entity(char symbol);
    #endif
#endif

#ifdef ENTITY_PROPERTIES
bool has_property(entity_property e, entity_property property);
void add_property(entity_property *e, entity_property property);
void remove_property(entity_property *e, entity_property property);
#endif

#ifdef ENTITY_PROPERTIES
tile create_tile(floor_type f, entity_property properties, char display_symbol);
#endif

#ifdef ENTITY_PROPERTIES
tile create_wall();
#endif

#endif // ENTITY_H
#endif
