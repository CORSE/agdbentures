#include "debug.h"
#include "action.h"
#include <string.h>

void message(char msg[]) {
    puts(msg);
    fflush(stdout);
}
void prompt(char msg[], char* dest, int max_len) {
    printf("%s", msg);
    fflush(stdout);
    fgets(dest, max_len, stdin);
}

int player_up(map *map) {
    return move_player(map, map->player_y - 1, map->player_x, DIR_UP);
}
int player_down(map *map) {
    return move_player(map, map->player_y + 1, map->player_x, DIR_DOWN);
}
int player_left(map *map) {
    return move_player(map, map->player_y, map->player_x - 1, DIR_LEFT);
}
int player_right(map *map) {    
    return move_player(map, map->player_y, map->player_x + 1, DIR_RIGHT);
}

#ifdef COMMAND_ARGS
int player_up_n(map *map, unsigned n) {
    int count = 0;
    for (unsigned i = 0; i < n; i++) {
        count += player_up(map);
    }
    return count;
}
int player_down_n(map *map, unsigned n) {
    int count = 0;
    for (unsigned i = 0; i < n; i++) {
        count += player_down(map);
    }
    return count;
}
int player_left_n(map *map, unsigned n) {
    int count = 0;
    for (unsigned i = 0; i < n; i++) {
        count += player_left(map);
    }
    return count;
}
int player_right_n(map *map, unsigned n) {
    int count = 0;
    for (unsigned i = 0; i < n; i++) {
        count += player_right(map);
    }
    return count;
}
#endif

void up(map *map, int y_start, int x_start) {
    move_entity(map, y_start, x_start, y_start-1, x_start);
}

void down(map *map, int y_start, int x_start) {
    move_entity(map, y_start, x_start, y_start+1, x_start);
}

void left(map *map, int y_start, int x_start){
    move_entity(map, y_start, x_start, y_start, x_start-1);
}

void right(map *map, int y_start, int x_start) {
    move_entity(map, y_start, x_start, y_start, x_start+1);
}

bool move_entity(map *map, int fy, int fx, int ty, int tx) {
    // Si il n'y a rien a l'emplacement de depart ou que le deplacement est impossible la fonction ne fait rien
    entity entity_from = get_entity(map, fy, fx);
#ifdef ENTITY_STRUCT
    if (entity_from != NULL && can_move_to(map, fy, fx, ty, tx)) {
#else
    if (entity_from != EMPTY && can_move_to(map, fy, fx, ty, tx)) {
#endif
#ifndef EVENTS
  #ifdef ENTITY_STRUCT
    #ifndef ENTITY_PROPERTIES
        if ((get_tile(map, ty, tx)) == F_WATER && map->player_x == fx && map->player_y == fy ){
    #else
        if ((get_tile(map, ty, tx)).floor == F_WATER && map->player_x == fx && map->player_y == fy ){
    #endif
  #else
        if (get_entity(map, ty, tx) == WATER && map->player_x == fx && map->player_y == fy ){
  #endif
           message ("Vous vous noyez !");
           free_map(map);
           exit (EXIT_FAILURE);
        }
#endif
        move_without_check(map, fy, fx, ty, tx);

        return true;
    }

    return false;
}

bool move_player(map *map, int y_dest, int x_dest, direction dir_dest) {
    map->player_direction = dir_dest;
    if (move_entity(map, map->player_y, map->player_x, y_dest, x_dest)) {
        // On modifie la position du joueur apres le deplacement
        map->player_x = x_dest;
        map->player_y = y_dest;
        return true;
    } else {
        return false;
    }
}

#ifndef ENTITY_STRUCT
bool is_obstacle(entity e) {
    // Liste des caracteres qui representent des portions de murs
    // Les portes (door 'D') sont aussi des murs ainsi que l'eau '~'
    return e == WALL || e == '+' || e == '-' || e == '|' || e == DOOR || e == 'V';
}
#else
bool is_obstacle(map* m, int y, int x) {
    // Map boundaries are obstacle
    if ((y >= m->height || y < 0) || (x >= m->width || x < 0)) {
        return true;
    }
#ifndef ENTITY_PROPERTIES
    return get_tile(m, y, x) == F_WALL;
#else
    entity e = get_entity(m, y, x);
#ifndef MAP_ENTITY_STACKING
    if (has_property(e->properties, OBSTACLE)){
        return true;
    }
#else
    while (e) {
        if (has_property(e->properties, OBSTACLE)){
            return true;
        }
        e = e->next;
    }
#endif
    return has_property(get_tile(m, y, x).properties, OBSTACLE);
#endif
}
#endif

int can_move_to(map *map, int from_y, int from_x, int dest_y, int dest_x)
{
    // y en dehors de la carte
    if (dest_y >= map->height || dest_y < 0) {
        return 0;
    }
    // x en dehors de la carte
    if (dest_x >= map->width || dest_x < 0) {
        return 0;
    }
    // La destination est trop loin
    if (abs(dest_y - from_y) + abs(dest_x - from_x) != 1) {
        return 0;
    }
    
    // y en dehors de la carte
    if (dest_y >= map->height || dest_y < 0) {
        return 0;
    }
    // x en dehors de la carte
    if (dest_x >= map->width || dest_x < 0) {
        return 0;
    }

    // hit a wall
#ifndef ENTITY_STRUCT
    return !is_obstacle(get_entity(map, dest_y, dest_x));
#else
    return !is_obstacle(map, dest_y, dest_x);
#endif
}

void move_without_check(map *map, int y_start, int x_start, int y_dest, int x_dest) {
    // Pour deplacer on supprime l'entite de la case de depart et on la copie
    // sur la case d'arrivee
    entity previous_entity = remove_entity(map, y_start, x_start);
    place_entity(map, previous_entity, y_dest, x_dest);
}

direction direction_from_string(char *dir_string) {
    // On cherche la chaine de caractere correspondante
    if(strcmp(dir_string, "LEFT") == 0) {
        return DIR_LEFT;
    } else if (strcmp(dir_string, "RIGHT") == 0) {
        return DIR_RIGHT;
    } else if (strcmp(dir_string, "UP") == 0) {
        return DIR_UP;
    } else if (strcmp(dir_string, "DOWN") == 0) {
        return DIR_DOWN;
    } else {
        return DIR_UNKNOWN;
    }
}

void look_forward_n(int pos_y, int pos_x, direction direction, int n, int *dest_y, int *dest_x) {
    *dest_x = pos_x;
    *dest_y = pos_y;
    switch (direction) {
        case DIR_LEFT:
            *dest_x -= n;
        break;
        case DIR_RIGHT:
            *dest_x += n;
        break;
        case DIR_UP:
            *dest_y -= n;
        break;
        case DIR_DOWN:
            *dest_y += n;
        break;
        default:
            printf("Direction inconnue impossible de regarder.\n");
    }
}

void look_forward(int pos_y, int pos_x, direction direction, int *dest_y, int *dest_x) {
    look_forward_n(pos_y, pos_x, direction, 1, dest_y, dest_x);
}

void player_look_forward_n(map *map, int n, int *dest_y, int *dest_x) {
    look_forward_n(map->player_y, map->player_x, map->player_direction, n, dest_y, dest_x);

}

void player_look_forward(map *map, int *dest_y, int *dest_x) {
    player_look_forward_n(map, 1, dest_y, dest_x);
}
