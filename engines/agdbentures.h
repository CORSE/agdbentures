#ifndef AGDBENTURES_H
#define AGDBENTURES_H

#include "action.h"
#ifdef LEVEL_END
#include "game.h"
#endif
#ifdef ENTITY_STRUCT
#include "entity.h"
#endif
#ifdef INVENTORY_ARRAY
#include "inventory.h"
#endif
#ifdef EVENTS
#include "events.h"
#include "prefab_events.h"
#endif
#include "map.h"
#ifdef INPUT_COMMAND
#include "read_input.h"
#endif

#endif // AGDBENTURES_H
