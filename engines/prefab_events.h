#include "debug.h"
#ifdef EVENTS
#ifndef PREFAB_EVENTS_H
#define PREFAB_EVENTS_H

#include "game.h"

// Vérifie si le joueur se trouve sur la case de victoire
void verify_exit(game_instance *game);

// Le joueur se noie s'il est sur une case d'eau
void drown(game_instance *game);

#endif // PREFAB_EVENTS_H
#endif