#include "debug.h"
#ifdef INPUT_COMMAND
#include "read_input.h"
#include <errno.h>
#include <stdio.h>

// Fonction interne qui cree la structure de la commande a partie du buffer
// recupere depuis stdin
command *build_command(char *input_buffer, size_t buffer_size);
#ifdef COMMAND_ARGS
// Fonction interne qui place les pointeurs des arguments aux bons endroit dans
// le buffer de la commande.
void place_args_pointer(command *com);
#endif

command *get_next_command(void)
{
    size_t command_length;
    // On reutilise le buffer qui contient la commande.
    char *line = NULL;
    int linesize;

    printf("Entrez la prochaine commande:\n");
    linesize = getline(&line, &command_length, stdin);
    if (feof(stdin)) {
        // EOF received, exit gracefully
        printf("EOF received, exiting!\n");
        free(line);
        exit(INPUT_FAILURE);
    }
    if (linesize != -1) {
        if (linesize == 0)
            printf("Hum, empty line???\n");
        return build_command(line, linesize);
    } else {
        perror("Failed to read line");
        free(line);
        exit(INPUT_FAILURE);
    }
}

void free_command(command *com)
{
    free(com->command_buffer);
#ifdef COMMAND_ARGS
    if (com->args != NULL)
        free(com->args);
#endif
    free(com);
}

int apply_input(map *map, const command *c)
{
    if (!strcmp(c->command_buffer, "UP")) {
        return player_up(map);
    } else if (!strcmp(c->command_buffer, "DOWN")) {
        return player_down(map);
    } else if (!strcmp(c->command_buffer, "LEFT")) {
        return player_left(map);
    } else if (!strcmp(c->command_buffer, "RIGHT")) {
        return player_right(map);
#ifdef COMMAND_ARGS
    } else if (!strcmp(c->command_buffer, "UP_N")) {
        return player_up_n(map, atoi(c->args[0]));
    } else if (!strcmp(c->command_buffer, "DOWN_N")) {
        return player_down_n(map, atoi(c->args[0]));
    } else if (!strcmp(c->command_buffer, "LEFT_N")) {
        return player_left_n(map, atoi(c->args[0]));
    } else if (!strcmp(c->command_buffer, "RIGHT_N")) {
        return player_right_n(map, atoi(c->args[0]));
#endif
    } else {
        return -1;
    }
}

entity next_tile(map *m, const command *c){
    int x = m->player_x;
    int y = m->player_y;

    char* cb = c->command_buffer;
    if(!strcmp(cb, "UP")){
        y--;
    }else if(!strcmp(cb, "DOWN")){
        y++;
    }else if(!strcmp(cb, "LEFT")){
        x--;
    }else if(!strcmp(cb, "RIGHT")){
        x++;
    }else{
        return '\0';
    }

    return get_entity(m, y, x);
}

command *build_command(char *input_buffer, size_t buffer_size)
{
    // On alloue la structure pour la commande
    command *com = malloc(sizeof(command));

    // On alloue un buffer de la meme taille que l'entree
    com->command_buffer = malloc(sizeof(char) * buffer_size);
    com->buffer_size = buffer_size;
#ifndef COMMAND_ARGS
    memcpy(com->command_buffer, input_buffer, buffer_size);
#endif
    com->command_buffer[buffer_size - 1] = '\0';
#ifdef COMMAND_ARGS

    // Copie le buffer d'entree et compte les argument et remplace les espaces
    // par des \0 dans le buffer de destination
    com->n_args = 0;
    for (size_t current_char_index = 0; current_char_index < buffer_size;
         current_char_index++) {
        if (input_buffer[current_char_index] == ' ') {
            com->n_args++;
            com->command_buffer[current_char_index] = '\0';
        } else if (input_buffer[current_char_index] != '\n') {
#ifdef NEVER
            // TODO remove this copy in BUG version
#endif
            com->command_buffer[current_char_index] =
                input_buffer[current_char_index];
        }
    }

    // On place les pointeurs pour les arguments de la commande
    place_args_pointer(com);
#endif
    free(input_buffer);
    return com;
}

#ifdef COMMAND_ARGS
void place_args_pointer(command *com)
{
    // On alloue le tableau de pointeur pour les arguments.
    // Si il n'y a pas d'arguments il n'y a rien a allouer.
    if (com->n_args > 0) {
        com->args = malloc(sizeof(char *) * com->n_args);
    } else {
        com->args = NULL;
    }

    // On parcourt le buffer et on place le pointeur pour chaque argument apres
    // les \0 (la fin de l'argument d'avant)
    int n_args = 0;
    for (size_t current_char_index = 0; n_args < com->n_args;
         current_char_index++) {
        if (com->command_buffer[current_char_index] == '\0') {
            com->args[n_args] = com->command_buffer + current_char_index + 1;
            n_args++;
        }
    }
}
#endif
#endif
