#include "debug.h"
#ifdef INVENTORY_ARRAY
#ifndef INVENTORY_H
#define INVENTORY_H

#include <stdbool.h>

#include "game.h"

// pick the item at the given position
entity pick_item(game_instance *game, int y, int x);

// drop the given item at the given position
int drop_item(game_instance *game, entity e, int y, int x);

// gets the number of items in the inventory
int inventory_size(game_instance *game);

void clear_inventory(game_instance *game);

// if the item is already in the inventory it is not added
int add_item_in_inventory(game_instance *game, entity e);
// check if the entity is in the inventory with pointer equality not entity
// equality
bool has_item_in_inventory(game_instance *game, entity e);
int remove_item_from_inventory(game_instance *game, entity e);

#endif // INVENTORY_H
#endif
