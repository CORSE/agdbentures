#include "debug.h"
#ifdef INVENTORY_ARRAY
#include "entity.h"
#include "inventory.h"
#include "map.h"


#ifdef ENTITY_PROPERTIES
// Prend le premier objet qui a la propriété PICKABLE
entity pick_item(game_instance *game, int y, int x) {

    map* m = current_map(game->map_stack);
    entity e = get_entity(m,y,x);
    #ifdef MAP_ENTITY_STACKING
    entity before = NULL;
    while (e)
    {
        if (has_property(e->properties,PICKABLE)){
            if (before){
                before->next = e->next;
            }else{
                remove_entity(m, y, x);  // enlève la première
            }
            e->next = NULL;
            add_item_in_inventory(game, e);
            return e;
        }
        before = e;
        e = e->next;
    }
    #else
    if (has_property(e->properties,PICKABLE)){
        add_item_in_inventory(game, e);
        return e;
    }
    #endif
    return NULL;
}
#else
entity pick_item(game_instance *game, int y, int x) {
    map* m = current_map(game->map_stack);
    entity e = get_entity(m,y,x);
    if (e->is_pickable){
            add_item_in_inventory(game, e);
            return e;
    }
    return NULL;
}
#endif

int drop_item(game_instance *game, entity e, int y, int x) {
    // if we successfully removed the item from the inventory drop it
    int success_remove = remove_item_from_inventory(game, e);
    if (success_remove == 0) {
        place_entity(current_map(game->map_stack), e, y, x);
    }

    return success_remove;
}

int inventory_size(game_instance *game) {
    int count = 0;
    for(int i = 0; i < MAX_INVENTORY_SIZE; i++) {
        if (game->inventory[i] != NULL) {
            count++;
        }
    }
    return count;
}

int add_item_in_inventory(game_instance *game, entity e) {
    if (has_item_in_inventory(game, e)) {
        return 1;
    }
    for(int i = 0; i < MAX_INVENTORY_SIZE; i++) {
        if (game->inventory[i] == NULL) {
            game->inventory[i] = e;
            return 0;
        }
    }
    return 1;
}

bool has_item_in_inventory(game_instance *game, entity e) {
    for(int i = 0; i < MAX_INVENTORY_SIZE; i++) {
        if (game->inventory[i] == e) {
            return true;
        }
    }
    return false;
}

int remove_item_from_inventory(game_instance *game, entity e) {
    for(int i = 0; i < MAX_INVENTORY_SIZE; i++) {
        if (game->inventory[i] == e) {
            game->inventory[i] = NULL;
            return 0;
        }
    }
    return 1;
}

void clear_inventory(game_instance *game) {
    for(int i = 0; i < MAX_INVENTORY_SIZE; i++) {
        game->inventory[i] = NULL;
    }
}
#endif
