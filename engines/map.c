#include "debug.h"
#include "map.h"
#include <stdio.h>
#include <stdlib.h>

// Creation et initialisation de la carte
#ifdef MAP_STACK
map *current_map(map_stack* ms)
{
    if (ms->length <= 0)
        return NULL;
    return ms->maps[ms->length-1];
}

map_stack* init_map_stack(void)
{
    map_stack* mstack = malloc(sizeof(map_stack));
    mstack->length = 0;
    for(int i = 0; i < MAX_MAPS; i++) {
        mstack->maps[i] = NULL;
    }
    return mstack;
}
#ifdef MAP_GRAPH
void free_map_stack(map_stack *ms, map * current)
{
    map * to_free;
    while (ms->length > 0) {
        to_free = pop_from_stack(ms);
        if (to_free != current){
            free_map(to_free);
        }
    }
    free(ms);
}
#else

void free_map_stack(map_stack *ms)
{
    while (ms->length > 0) {
        free_map(pop_from_stack(ms));
    }
    free(ms);
}
#endif
#endif
#ifdef MAP_STACK
void push_on_stack(map_stack *ms, map *m)
{
    if (ms->length >= MAX_MAPS - 1) {
        printf("load_map: can't load a new map, the maximum number has been reached.\n");
        return;
    }

    ms->maps[ms->length] = m;
    ms->length++;
}

map *init_map(const char *name, int height, int width)
#else
map *init_map(int height, int width)
#endif
{
    map* new_map = malloc(sizeof(map));
#ifdef MAP_STACK
    new_map->name = (char *) malloc(sizeof(char) * (strlen(name) + 1));
    strcpy(new_map->name, name);
#endif
    new_map->width = width;
    new_map->height = height;

    // Pour l'instant on met des valeurs par defaut avant que le joueur ne soit place
    new_map->player_x = -1;
    new_map->player_y = -1;
    new_map->player_direction = DIR_UNKNOWN;

#ifdef ENTITY_STRUCT
    new_map->entities = (entity **) malloc(new_map->height * sizeof(entity*));
    new_map->tiles = (tile **) malloc(new_map->height * sizeof(tile*));
#else
    // On alloue les lignes
    new_map->tiles = (entity *) malloc(new_map->height * new_map->width );
#endif
    // Pour chaque ligne on alloue les colonnes et on les remplit avec du vide.
    for(int y = 0; y < new_map->height; y++) {
#ifdef ENTITY_STRUCT
        new_map->entities[y] = (entity *) calloc(new_map->width , sizeof(entity));
        new_map->tiles[y] = (tile *) malloc(new_map->width * sizeof(tile));  
#else
#endif
        for(int x = 0; x < new_map->width; x++) {
#ifndef ENTITY_STRUCT
            new_map->tiles[y * new_map->width + x] = EMPTY;
#else
#ifdef ENTITY_PROPERTIES
            tile f = {F_EMPTY, 0, '_'};
            new_map->tiles[y][x] = f;
            new_map->entities[y][x] = NULL;

#else
            new_map->tiles[y][x] = F_EMPTY;
#endif
#endif
        }
    }
#ifdef MAP_GRAPH
    new_map->map_link_list = NULL;
#endif
    return new_map;
}
#ifdef MAP_GRAPH

void free_link(map_link* ml) {
    if (ml->next != NULL) {
        free_link(ml->next);
    }
    free(ml);
}
#endif

void free_map(map* m) {
if (!m) {return;}
#ifdef MAP_STACK
    free(m->name);
    m->name = NULL;
#endif
#ifdef ENTITY_STRUCT
    for (int y = 0; y < m->height; y++){
        for (int x = 0; x < m->width; x++){
            entity to_remove = remove_entity(m,y,x);
            while (to_remove){
                free(to_remove);
                to_remove = remove_entity(m,y,x);
            }
            free(m->entities[y][x]);
        }
        free(m->entities[y]);
    }
    free(m->entities);
    m->entities = NULL;
    for (int y = 0; y < m->height; y++){
        free(m->tiles[y]);
    }
    free(m->tiles);
    m->tiles = NULL;
#else
    free(m->tiles);
    m->tiles = NULL;
#endif
#ifdef MAP_GRAPH
    if (m->map_link_list){
        free_link(m->map_link_list);
    }
#endif
    free(m);
}
#ifdef READ_MAP_STR

#ifndef MAP_STACK
map* load_map(const char *data)
#else
map *load_map(const char *name, const char *data)
#endif
{
    // Trouve la taille de la carte dans la chaine d'entree
    const char *ch = strchr(data, '\n');
    if (!ch) {
        fprintf (stderr, "Error: cannot find line return in map description!");
        exit(EXIT_FAILURE);
    }
    int width = ch - data;
    int height = 0;
    while (ch) {
        ch = strchr(ch+1, '\n');
        height++;
    }

    // On cree une carte vide avec les bonnes dimensions
#ifndef MAP_STACK
    map *m = init_map(height, width);
#else
    map *m = init_map(name, height, width);
#endif
    ch = data;
#ifdef ENTITY_STRUCT
    tile current_floor;
#endif
    for (int y=0; y < m->height; y++) {
        for (int x=0; x < m->width; x++) {
#ifndef ENTITY_STRUCT
            // On copie le caractere dans la carte au bon endroit.
            if (*ch == PLAYER) {
                place_player(m, y, x);
            } else {
                m->tiles[width*y + x] = *ch;
            }
#else
  #ifndef ENTITY_PROPERTIES
            if (parse_floor(*ch, &current_floor) == 0) {
  #else
            if (parse_tile(*ch, &current_floor) == 0) {
  #endif
                m->tiles[y][x] = current_floor;
            } else {
  #ifndef ENTITY_PROPERTIES
                m->tiles[y][x] = F_GROUND;
  #else
                m->tiles[y][x].floor = F_GROUND;
  #endif
                m->entities[y][x] = parse_entity(*ch);
  #ifndef ENTITY_PROPERTIES
                if (m->entities[y][x]->display_symbol == PLAYER){
  #else
                if (m->entities[y][x]->category == C_PLAYER){
  #endif
                   m->player_y = y;
                   m->player_x = x;
                   m->player_direction = DIR_RIGHT;
                }
            }
#endif
            ch++;
        }
        assert(*ch == '\n');
        ch++;
    }
    return m;
}
#endif

#ifdef MAP_STACK
map* pop_from_stack(map_stack *ms) {
    if (ms->length > 0) {
        map *m = ms->maps[ms->length-1];
        ms->length--;
        ms->maps[ms->length] = NULL;
        return m;
    } else {
        printf("pop_from_stack: Can't unload map: map stack is already empty.\n");
    }
    return NULL;
}
#endif

#ifdef ENTITY_STRUCT
#ifdef ENTITY_PROPERTIES
int parse_tile(char symbol, tile *tile) {
#else
int parse_floor(char symbol, tile *tile) {
#endif
#ifdef ENTITY_PROPERTIES
    tile->display_symbol = symbol;
#endif
    switch (symbol) {
        case '+':
        case '|':
        case '-':
        case '#':
#ifdef ENTITY_PROPERTIES
            tile->floor = F_WALL;
            tile->properties = OBSTACLE;
#else
            *tile = F_WALL;
#endif
            return 0;
        case '~':
#ifdef ENTITY_PROPERTIES
            tile->floor = F_WATER;
            tile->properties = TRAP;
#else
            *tile = F_WATER;
#endif
            return 0;
        case ']':
        case '[':
        case 'D':
#ifdef ENTITY_PROPERTIES
            tile->floor = F_DOOR;
            tile->properties = OBSTACLE | ACTIVABLE;
#else
            *tile = F_DOOR;
#endif
            return 0;
        case 'T':
#ifdef ENTITY_PROPERTIES
            tile->floor = F_TARGET;
            tile->properties = 0;
#else
            *tile = F_TARGET;
#endif
            return 0;
        case '.':
#ifdef ENTITY_PROPERTIES
            tile->floor = AREA_WHITE;
            tile->properties = 0;
#else
            *tile = AREA_WHITE;
#endif
            return 0;
        case 'o':
#ifdef ENTITY_PROPERTIES
            tile->floor = AREA_GRAY;
            tile->properties = 0;
#else
            *tile = AREA_GRAY;
#endif
            return 0;
        case 'x':
#ifdef ENTITY_PROPERTIES
            tile->floor = AREA_HASHED;
            tile->properties = 0;
#else
            *tile = AREA_HASHED;
#endif
            return 0;

        case ' ':
#ifdef ENTITY_PROPERTIES
            tile->floor = F_GROUND;
            tile->properties = 0;
#else
            *tile = F_GROUND;
#endif
            return 0;
        default:
            return 1;
    }
}

entity parse_entity(char symbol) {
  #ifndef CUSTOM_ENTITY
    return create_entity(symbol);
  #else
    #ifdef ENTITY_PROPERTIES
    entity_property properties = 0;
    #endif
    entity_category category = C_NONE;
    switch (symbol) {
    #ifdef ENTITY_PROPERTIES
        case 'X':  // interrupteur ou équivalent
            properties = ACTIVABLE;
            break;
    #endif
        case EXIT:
            category = C_EXIT;
            break;
        case PLAYER:
            category = C_PLAYER;
            break;
        // TODO complete with other entities
    }
    #ifndef ENTITY_PROPERTIES
    return create_entity(symbol, category, NULL);
    #else
    return create_entity(symbol, properties, category, NULL);
    #endif
  #endif
}
#endif
#ifdef MAP_GRAPH

void add_link_to_map(map *m, int y, int x, map *dest_m, int dest_y, int dest_x) {
    map_link* new_link = malloc(sizeof(map_link));

    new_link->destination = dest_m;
    new_link->dest_x = dest_x;
    new_link->dest_y = dest_y;
    new_link->from_x = x;
    new_link->from_y = y;
    new_link->next = m->map_link_list;

    m->map_link_list = new_link;
}

void add_bidirectional_link_to_map(map *m, int y, int x, map *dest_m,
                                   int dest_y, int dest_x) {
    add_link_to_map(m, y, x, dest_m, dest_y, dest_x);
    add_link_to_map(dest_m, dest_y, dest_x, m, y, x);
}
#endif

void place_entity(map* map, entity entity, int y, int x) {
#ifndef ENTITY_STRUCT
    map->tiles[map->width * y + x] = entity;
#else
  #ifdef MAP_ENTITY_STACKING
    entity->next = remove_entity(map,y,x);
  #endif
    map->entities[y][x] = entity;
#endif
}

void place_player(map* map, int y, int x) {
    map->player_x = x;
    map->player_y = y;
#ifndef ENTITY_STRUCT
    place_entity(map, PLAYER, y, x);
#else
  #ifndef CUSTOM_ENTITY
    entity player = create_entity(PLAYER);
  #else
    #ifndef ENTITY_PROPERTIES
    entity player = create_entity(PLAYER, C_PLAYER, NULL);
    #else
    entity player = create_entity(PLAYER, 0, C_PLAYER, NULL);
    #endif
  #endif
    place_entity(map, player, y, x);
#endif
}

entity get_entity(map* map, int y, int x)
{
#ifndef ENTITY_STRUCT
    return map->tiles[map->width * y + x];
#else
    return map->entities[y][x];
#endif
}

#ifdef ENTITY_STRUCT
tile get_tile(map* map, int y, int x)
{
    return map->tiles[y][x];
}
#endif

entity remove_entity(map* map, int y, int x)
{
    entity removed_entity = get_entity(map, y, x);
    // On remplace l'ancienne entité par une case vide.
#ifndef ENTITY_STRUCT
    place_entity(map, EMPTY, y, x);
#else
  #ifndef MAP_ENTITY_STACKING
    place_entity(map, NULL, y, x);
  #else
    // Seule l'entité en début de liste est enlevée
    if (removed_entity){
        map->entities[y][x] = removed_entity->next;
    } else{
        map->entities[y][x] = NULL;
    }
  #endif
#endif
    return removed_entity;
}

#ifdef MAP_ENTITY_STACKING
entity find_category_in_stack(map* map, entity_category category, int y, int x){
    entity tmp = map->entities[y][x];
    while (tmp)
    {
        if (tmp->category == category){
            return tmp;
        }
        tmp = tmp->next;

    }
    return NULL;
} 

entity find_property_in_stack(map *map, entity_property property, int y, int x) {
    entity tmp = map->entities[y][x];
    while (tmp)
    {
        if (has_property(tmp->properties, property)){
            return tmp;
        }
        tmp = tmp->next;

    }
    return NULL;          
}
#endif

void show_map(map *map) {
    for (int y = 0; y < map->height; y++) {
        for (int x = 0; x < map->width; x++) {
#ifdef ENTITY_STRUCT
            if (map->entities[y][x] != NULL) {
                printf("%c", map->entities[y][x]->display_symbol);
  #ifdef ENTITY_PROPERTIES
            } else if (map->tiles[y][x].floor != F_EMPTY){
                printf("%c", map->tiles[y][x].display_symbol);
  #endif
            } else {
                printf(" ");
            }
#else
            printf("%c", map->tiles[map->width * y + x]);
#endif
        }
        printf("\n");
    }
}

#ifndef EVENTS  // With events, declared in prefab_events.h
void verify_exit(map* map)
{
    if (map->player_x != exit_x || map->player_y != exit_y) {
        printf("DEFAITE!\nYou haven't reached the exit\n");
        exit (EXIT_FAILURE);
    } else {
        printf("VICTOIRE!\nYou've reached the exit\n");
        exit (EXIT_SUCCESS);
    }
}
#endif
