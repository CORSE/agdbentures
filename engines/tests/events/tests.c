

#include <criterion/criterion.h>
#include <criterion/internal/assert.h>
#include <criterion/internal/test.h>

#include "agdbentures.h"

int exit_x, exit_y;

void salut(game_instance *game){
    player_up(game->current_map);
}

void salut2(game_instance *game){
    player_down(game->current_map);
    player_down(game->current_map);
    player_down(game->current_map);
}

Test(events, test_event) {

    game_instance *game = init_game();
    game->current_map = init_map("map", 5, 6);
    place_player(game->current_map, 1,1);
    add_event(game, salut);
    cr_assert_eq(game->current_map->player_y, 1);
    cr_assert_eq(game->current_map->player_x, 1);
    apply_events(game);
    cr_assert_eq(game->current_map->player_y, 0);
    cr_assert_eq(game->current_map->player_x, 1);
    cr_assert_eq(game->event_list->next, NULL);
    remove_event(game, (salut) );
    cr_assert_eq(game->event_list, NULL);
    add_event(game, (salut2));
    add_event(game, (salut));
    apply_events(game);
    cr_assert_eq(game->current_map->player_y, 2);
    cr_assert_eq(game->current_map->player_x, 1);
    remove_all_events(game);
    cr_assert_eq(game->event_list, NULL);
    free_game(game);
}
