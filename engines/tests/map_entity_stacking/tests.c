#include <criterion/criterion.h>
#include <criterion/internal/assert.h>
#include <criterion/internal/test.h>

#include "agdbentures.h"

int exit_x, exit_y;

Test(map_entity_stacking, simple_stacking) {
    int the_void = 100;
    map *m = init_map("map", 5, 6);
    entity e = create_entity('E', PICKABLE, C_MONSTER , &the_void);
    entity f = create_entity('F', PICKABLE, C_MONSTER , &the_void);
    place_entity(m, e, 2, 1);
    cr_assert_eq(e->next, NULL);
    cr_assert_eq(get_entity(m,2,1)->display_symbol, 'E');
    place_entity(m, f, 2, 1);
    cr_assert_eq(get_entity(m,2,1)->display_symbol, 'F');
    cr_assert_eq(get_entity(m,2,1)->next->display_symbol, 'E');
    entity removed = remove_entity(m, 2, 1);
    cr_assert_eq(get_entity(m,2,1)->display_symbol, 'E');
    cr_assert_eq(get_entity(m,2,1)->next, NULL);
    cr_assert_eq(removed->display_symbol, 'F');
    free(f);
    free_map(m);
}


Test(map_entity_stacking, move_entity) {
    int the_void = 100;
    map *m = init_map("map", 5, 6);
    entity e = create_entity('E', PICKABLE, C_MONSTER , &the_void);
    entity f = create_entity('F', PICKABLE, C_MONSTER , &the_void);
    entity g = create_entity('G', PICKABLE, C_MONSTER , &the_void);

    place_entity(m, e, 2, 1);
    place_entity(m, f, 2, 1);
    place_entity(m, g, 2, 2);

    move_entity(m, 2, 1, 2, 2);
    cr_assert_eq(get_entity(m,2,1)->display_symbol, 'E');
    cr_assert_eq(get_entity(m,2,2)->display_symbol, 'F');
    cr_assert_eq(get_entity(m,2,2)->next->display_symbol, 'G');
    free_map(m);
}


Test(map_entity_stacking, find_attributs) {
    int the_void = 100;
    map *m = init_map("map", 5, 6);
    entity e = create_entity('E', PICKABLE, C_MONSTER , &the_void);
    entity f = create_entity('F', PUSHABLE, C_ITEM , &the_void);
    place_entity(m, e, 2, 1);
    place_entity(m, f, 2, 1);

//  find_property

    cr_assert_eq(find_property_in_stack(m,PICKABLE,2,1), e);
    cr_assert_eq(find_property_in_stack(m,PICKABLE,2,2), NULL);
    cr_assert_eq(find_property_in_stack(m,PUSHABLE,2,1), f);
    cr_assert_eq(find_property_in_stack(m,PICKABLE,2,1), e);
    remove_entity(m, 2, 1);
    cr_assert_eq(find_property_in_stack(m,PUSHABLE,2,1), NULL);
    cr_assert_eq(find_property_in_stack(m,PICKABLE,2,1), e);
    place_entity(m, f, 2, 1);

//  find_category


    cr_assert_eq(find_category_in_stack(m,C_MONSTER,2,1), e);
    cr_assert_eq(find_category_in_stack(m,C_MONSTER,2,2), NULL);
    cr_assert_eq(find_category_in_stack(m,C_ITEM,2,1), f);
    cr_assert_eq(find_category_in_stack(m,C_MONSTER,2,1), e);
    remove_entity(m, 2, 1);
    cr_assert_eq(find_category_in_stack(m,C_ITEM,2,1), NULL);
    cr_assert_eq(find_category_in_stack(m,C_MONSTER,2,1), e);
    free_map(m);
    free(f);
}

