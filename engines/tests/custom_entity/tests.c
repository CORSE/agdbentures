#include <criterion/criterion.h>
#include <criterion/internal/assert.h>
#include <criterion/internal/test.h>

#include "agdbentures.h"

int exit_x, exit_y;

Test(custom_entity, test_create) {
    int the_void = 100;
    entity e = create_entity('#', C_MONSTER , &the_void);
    cr_assert_eq(e->id , -1);
    cr_assert_eq(e->category, C_MONSTER);
    cr_assert_eq(e->stats, &the_void);
    cr_assert_eq(e->display_symbol, '#');
    free(e);
}
