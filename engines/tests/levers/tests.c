#include <criterion/criterion.h>
#include <criterion/internal/assert.h>
#include <criterion/internal/test.h>

#include "agdbentures.h"

int exit_x;
int exit_y;

Test(generic, level)
{
    int lever_A;
    int lever_B;

    lever_A = ACTIVATED;
    lever_B = DEACTIVATED;

    // Not many things to try in this version...
    cr_assert(lever_A);
    cr_assert(!lever_B);
    exit(0);
}
