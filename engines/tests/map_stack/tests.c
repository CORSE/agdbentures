#include <criterion/criterion.h>
#include <criterion/internal/assert.h>
#include <criterion/internal/test.h>

#include "agdbentures.h"

int exit_x;
int exit_y;

Test(map_stack, load_unload_empty) {
    game_instance* game = init_game();

    // check empty
    cr_assert_eq(game->map_stack->length, 0);
    cr_assert_eq(current_map(game->map_stack), NULL);

    // add a map
    map* m1 = init_map("map1", 3, 3);
    push_on_stack(game->map_stack, m1);
    cr_assert_eq(game->map_stack->length, 1);
    cr_assert_eq(current_map(game->map_stack), m1);


    // add another one
    map* m2 = init_map("map2", 3, 3);
    push_on_stack(game->map_stack, m2);
    cr_assert_eq(game->map_stack->length, 2);
    cr_assert_eq(current_map(game->map_stack), m2);

    // remove the second one (it is a stack)
    map* first_unload = pop_from_stack(game->map_stack);
    cr_assert_eq(game->map_stack->length, 1);
    cr_assert_eq(current_map(game->map_stack), m1);
    cr_assert_eq(first_unload, m2);

    // remove the first one
    map* second_unload = pop_from_stack(game->map_stack);
    cr_assert_eq(game->map_stack->length, 0);
    cr_assert_eq(current_map(game->map_stack), NULL);
    cr_assert_eq(second_unload, m1);
    free_map(m1);
    free_map(m2);
    free_game(game);
}
