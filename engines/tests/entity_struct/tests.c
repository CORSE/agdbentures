#include <criterion/criterion.h>
#include <criterion/internal/assert.h>
#include <criterion/internal/test.h>

#include "agdbentures.h"

int exit_x, exit_y;


Test(entity_struct, gestion_of_entity) {
    map* m = init_map("map1", 3, 3);
    entity e = create_entity('#');
    place_entity(m,e,1,1);
    place_player(m,2,2);

    // test on gestion of entity

    cr_assert_eq( get_entity(m,1,1), e );
    cr_assert_eq( get_entity(m,2,2)->display_symbol,'>');
    cr_assert_eq(e->display_symbol, '#');
    cr_assert_eq(e->id, -1);
    cr_assert_eq( remove_entity(m,1,1), e );
    free(e);
    cr_assert_eq( get_entity(m,1,1), NULL);
    free_map(m);
}

Test(entity_struct, gestion_of_floor) {
    map* m = init_map("map2", 3, 3);

    // test on floor
    cr_assert_eq(parse_floor('P',&(m->tiles[0][0])),1);
    cr_assert_eq(parse_floor('D',&(m->tiles[0][0])),0);
    cr_assert_eq(m->tiles[0][0],F_DOOR);
    free_map(m);
}


const char *str_map3 =
"\
 +----+--+\n\
++  X  ~~|\n\
|      ##+\n\
| >      @\n\
|      ##+\n\
++     ~~|\n\
 +----+--+\n\
";

Test(entity_struct, player_placement) {
    map* m = load_map("map3", str_map3);

    // Test floor
    cr_assert(m->tiles[0][0] == F_GROUND);  // TODO: should be F_EMPTY here
    cr_assert(m->tiles[1][1] == F_WALL);
    cr_assert(m->tiles[1][2] == F_GROUND);
    cr_assert(m->tiles[1][4] == F_GROUND, "Unknown symbol should force ground tile");
    cr_assert(m->tiles[1][7] == F_WATER);
    cr_assert(m->tiles[1][8] == F_WATER);
    cr_assert(m->tiles[1][9] == F_WALL);
    /* cr_assert(m->tiles[1][10] == F_EMPTY, "out of bound"); */ // don't know how to test this

    // Test entities
    cr_assert(m->entities[1][4]->display_symbol == 'X', "Unknown entity should be 'X'");
    cr_assert(m->entities[3][2]->display_symbol == PLAYER);
    cr_assert(m->entities[3][9]->display_symbol == EXIT);

    // Test player position
    cr_assert(m->player_x == 2);
    cr_assert(m->player_y == 3);
    free_map(m);
}
