#include <criterion/criterion.h>
#include <criterion/internal/assert.h>
#include <criterion/internal/test.h>
#include <criterion/redirect.h>
#include <stdio.h>

#include "agdbentures.h"

int exit_x;
int exit_y;

Test(level_end, test_success)
{
    cr_redirect_stdout();

    game_instance *game = init_game(0, 0);

    cr_assert_eq(game->exit_main_loop, false);
    level_success(game);

    fflush(stdout);
    cr_assert_stdout_eq_str("VICTOIRE!\n");

    cr_assert_eq(game->exit_main_loop, true);
    cr_assert_eq(game->level_exit_code, 0);
    free_game(game);
}

Test(level_end, test_failure)
{
    cr_redirect_stdout();

    game_instance *game = init_game(0, 0);

    cr_assert_eq(game->exit_main_loop, false);
    level_failed(game, 1);

    fflush(stdout);
    cr_assert_stdout_eq_str("DEFAITE!\n");

    cr_assert_eq(game->exit_main_loop, true);
    cr_assert_eq(game->level_exit_code, 1);
    free_game(game);
    
}
