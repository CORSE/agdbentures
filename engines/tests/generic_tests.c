#include <criterion/criterion.h>
#include <criterion/internal/assert.h>
#include <criterion/internal/test.h>

#include "agdbentures.h"

int exit_x;
int exit_y;

Test(generic, missing)
{   printf("\033[31;01m");
    printf("****************************************************************************************\n");
    printf ("************************Warning: no test for this engine version************************\n");
    printf("****************************************************************************************\n");
    printf("\033[00m");
    cr_assert(true);
}
