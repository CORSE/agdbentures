#include <criterion/criterion.h>
#include <criterion/internal/assert.h>
#include <criterion/internal/test.h>

#include "agdbentures.h"

int exit_x, exit_y;

Test(inventory, test_add) {
    game_instance* game = init_game();
    struct entity_s entity_a = { .id = 0, .display_symbol = 'a'};
    struct entity_s entity_b = {1, 'b'};
    int result;

    cr_assert_eq(has_item_in_inventory(game, &entity_a), false);
    cr_assert_eq(has_item_in_inventory(game, &entity_b), false);

    result = add_item_in_inventory(game, &entity_a);
    cr_assert_eq(result, 0);
    cr_assert_eq(has_item_in_inventory(game, &entity_a), true);
    cr_assert_eq(has_item_in_inventory(game, &entity_b), false);

    result = add_item_in_inventory(game, &entity_b);
    cr_assert_eq(result, 0);
    cr_assert_eq(has_item_in_inventory(game, &entity_a), true);
    cr_assert_eq(has_item_in_inventory(game, &entity_b), true);

    // should fail so result is 1
    result = add_item_in_inventory(game, &entity_a);
    cr_assert_eq(result, 1);
    cr_assert_eq(has_item_in_inventory(game, &entity_a), true);
    cr_assert_eq(has_item_in_inventory(game, &entity_b), true);
    free_game(game);
}

Test(inventory, test_remove) {
    game_instance* game = init_game();

    struct entity_s entity_a = {0, 'a'};
    struct entity_s entity_b = {1, 'b'};
    int result;

    cr_assert_eq(has_item_in_inventory(game, &entity_a), false);
    cr_assert_eq(has_item_in_inventory(game, &entity_b), false);

    // insertion is already tested
    add_item_in_inventory(game, &entity_a);
    add_item_in_inventory(game, &entity_b);

    result = remove_item_from_inventory(game, &entity_b);
    cr_assert_eq(result, 0);
    cr_assert_eq(has_item_in_inventory(game, &entity_a), true);
    cr_assert_eq(has_item_in_inventory(game, &entity_b), false);

    // should do nothing and return 1 because b is already removed
    result = remove_item_from_inventory(game, &entity_b);
    cr_assert_eq(result, 1);
    cr_assert_eq(has_item_in_inventory(game, &entity_a), true);
    cr_assert_eq(has_item_in_inventory(game, &entity_b), false);

    clear_inventory(game);
    cr_assert_eq(has_item_in_inventory(game, &entity_a), false);
    cr_assert_eq(has_item_in_inventory(game, &entity_b), false);
    free_game(game);
}
