

#include <criterion/criterion.h>
#include <criterion/internal/assert.h>
#include <criterion/internal/test.h>

#include "agdbentures.h"

int exit_x, exit_y;

Test(map_graph, test_on_link) {
    map *m = init_map("map", 5, 6);
    map *m2 = init_map("map", 5, 6);
    add_link_to_map(m,3,2,m2,3,2);
    add_bidirectional_link_to_map(m,2,3,m2,2,3);
    map_link *link = m->map_link_list;
    cr_assert_eq(link->from_x, 3);
    cr_assert_eq(link->from_y, 2);
    cr_assert_eq(link->dest_x, 3);
    cr_assert_eq(link->dest_y, 2);
    cr_assert_eq(link->destination, m2);
    link = link->next;
    cr_assert_eq(link->from_x, 2);
    cr_assert_eq(link->from_y, 3);
    cr_assert_eq(link->dest_x, 2);
    cr_assert_eq(link->dest_y, 3);
    cr_assert_eq(link->destination, m2);
    cr_assert_eq(link->next, NULL);
    link = m2->map_link_list;
    cr_assert_eq(link->from_x, 3);
    cr_assert_eq(link->from_y, 2);
    cr_assert_eq(link->dest_x, 3);
    cr_assert_eq(link->dest_y, 2);
    cr_assert_eq(link->destination, m);
    cr_assert_eq(link->next, NULL);
    free_map(m);
    free_map(m2);
}

Test(map_graph, player_move) {
    game_instance* game = init_game();
    map *m = init_map("map", 10, 10);
    map *m2 = init_map("map", 10, 10);
    add_bidirectional_link_to_map(m,2,3,m2,8,9);
    push_on_stack(game->map_stack,m);
    push_on_stack(game->map_stack,m2);
    place_player(current_map(game->map_stack), 8,8);
    cr_assert_eq(current_map(game->map_stack), m2);
    player_right(current_map(game->map_stack));
    cr_assert_eq(current_map(game->map_stack)->player_x,9);
    cr_assert_eq(current_map(game->map_stack)->player_y,8);
    take_map_link(game, current_map(game->map_stack));
    cr_assert_eq(game->current_map, m);
    cr_assert_eq(game->current_map->player_x,3);
    cr_assert_eq(game->current_map->player_y,2);
    take_map_link(game, game->current_map);
    cr_assert_eq(game->current_map, m2);
    cr_assert_eq(current_map(game->map_stack)->player_x,9);
    cr_assert_eq(current_map(game->map_stack)->player_y,8);

    free_game(game);
}
