#include <criterion/criterion.h>
#include <criterion/internal/assert.h>
#include <criterion/internal/test.h>

#include "agdbentures.h"

int exit_x, exit_y;

Test(read_map_str, read_empty)
{
    const char *str_map = "\
         \n\
         \n\
         \n";

    map *m = load_map(str_map);

    cr_assert_eq(m->width, 9);
    cr_assert_eq(m->height, 3);

    for (int y = 0; y < m->height; y++) {
        for (int x = 0; x < m->width; x++) {
            cr_assert_eq(m->tiles[y * 9 + x], ' ');
        }
    }
    free_map(m);
}

Test(read_map_str, read_map)
{
    const char *str_map = "\
XO  \n\
ABC \n\
 A  \n";

    map *m = load_map(str_map);

    cr_assert_eq(m->width, 4);
    cr_assert_eq(m->height, 3);
    cr_assert_eq(m->tiles[0 * 4 + 0], 'X');
    cr_assert_eq(m->tiles[0 * 4 + 1], 'O');
    cr_assert_eq(m->tiles[0 * 4 + 2], ' ');
    cr_assert_eq(m->tiles[0 * 4 + 3], ' ');
    cr_assert_eq(m->tiles[1 * 4 + 0], 'A');
    cr_assert_eq(m->tiles[1 * 4 + 1], 'B');
    cr_assert_eq(m->tiles[1 * 4 + 2], 'C');
    cr_assert_eq(m->tiles[1 * 4 + 3], ' ');
    cr_assert_eq(m->tiles[2 * 4 + 0], ' ');
    cr_assert_eq(m->tiles[2 * 4 + 1], 'A');
    cr_assert_eq(m->tiles[2 * 4 + 2], ' ');
    cr_assert_eq(m->tiles[2 * 4 + 3], ' ');
    free_map(m);
}
