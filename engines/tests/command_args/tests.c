#include <criterion/criterion.h>
#include <criterion/internal/assert.h>
#include <criterion/internal/test.h>
#include <criterion/redirect.h>
#include <stdio.h>

#include "agdbentures.h"

int exit_x;
int exit_y;

Test(command_args, read_command)
{
    FILE *f_stdin = cr_get_redirected_stdin();

    fprintf(f_stdin, "UP 5\n");
    fprintf(f_stdin, "DOWN 21 22\n");

    command *c = get_next_command();
    cr_assert_eq(c->buffer_size, 5);
    cr_assert_str_eq(c->command_buffer, "UP");
    cr_assert_eq(c->n_args, 1);
    cr_assert_str_eq(c->args[0], "5");
    cr_assert_eq(atoi(c->args[0]), 5);

    command *c2 = get_next_command();
    cr_assert_eq(c2->buffer_size, 11);
    cr_assert_str_eq(c2->command_buffer, "DOWN");
    cr_assert_eq(c2->n_args, 2);
    cr_assert_str_eq(c2->args[0], "21");
    cr_assert_eq(atoi(c2->args[0]), 21);
    cr_assert_str_eq(c2->args[1], "22");
    cr_assert_eq(atoi(c2->args[1]), 22);

    // check that old buffer is not destroyed
    cr_assert_str_eq(c->command_buffer, "UP");
    free_command(c);
    free_command(c2);
}

Test(command_args, apply_input)
{
    FILE *f_stdin = cr_get_redirected_stdin();
    command *c;
    fprintf(f_stdin, "LEFT_N 2\n");
    fprintf(f_stdin, "DOWN_N 2\n");
    fprintf(f_stdin, "WRONG 2\n");
    fprintf(f_stdin, "RIGHT_N 2\n");
    fprintf(f_stdin, "UP_N 2\n");

    map *m = init_map(6, 6);
    place_player(m, 3, 3);

    int res;
    // left
    c = get_next_command();
    res = apply_input(m, c );
    cr_assert_eq(res, 2);
    cr_assert_eq(m->player_x, 1);
    cr_assert_eq(m->player_y, 3);
    free_command(c);

    // down
    c = get_next_command();
    res = apply_input(m, c );
    cr_assert_eq(res, 2);
    cr_assert_eq(m->player_x, 1);
    cr_assert_eq(m->player_y, 5);
    free_command(c);

    // wrong
    c = get_next_command();
    res = apply_input(m, c );
    cr_assert_eq(res, -1);
    cr_assert_eq(m->player_x, 1);
    cr_assert_eq(m->player_y, 5);
    free_command(c);

    // right
    c = get_next_command();
    res = apply_input(m, c );
    cr_assert_eq(res, 2);
    cr_assert_eq(m->player_x, 3);
    cr_assert_eq(m->player_y, 5);
    free_command(c);

    // up
    c = get_next_command();
    res = apply_input(m, c );
    cr_assert_eq(res, 2);
    cr_assert_eq(m->player_x, 3);
    cr_assert_eq(m->player_y, 3);
    free_command(c);
    free_map(m);
}
