#include <criterion/criterion.h>
#include <criterion/internal/assert.h>
#include <criterion/internal/test.h>

#include "agdbentures.h"

int exit_x, exit_y;

Test(entity_properties, test_properties)
{
    int the_void = 100;
    entity e = create_entity('#', PICKABLE, C_MONSTER, &the_void);
    cr_assert_eq(has_property(e->properties, PICKABLE), true);
    cr_assert_eq(has_property(e->properties, ACTIVABLE), false);

    add_property(&(e->properties), ACTIVABLE);

    cr_assert_eq(has_property(e->properties, PICKABLE), true);
    cr_assert_eq(has_property(e->properties, ACTIVABLE), true);

    remove_property(&(e->properties), ACTIVABLE);

    cr_assert_eq(has_property(e->properties, ACTIVABLE), false);
    cr_assert_eq(has_property(e->properties, PICKABLE), true);

    remove_property(&(e->properties), PICKABLE);

    cr_assert_eq(has_property(e->properties, PICKABLE), false);

    cr_assert_eq(e->category, C_MONSTER);
    cr_assert_eq(e->stats, &the_void);
    cr_assert_eq(e->display_symbol, '#');
    free(e);
}

Test(entity_properties, test_tile)
{

    // test on tile
    tile tmp = create_tile(F_GROUND, TRAP, ' ');
    cr_assert_eq(tmp.floor, F_GROUND);
    cr_assert_eq(tmp.properties, TRAP);
    cr_assert_eq(tmp.display_symbol, ' ');
}
