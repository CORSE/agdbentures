#include <criterion/criterion.h>
#include <criterion/new/assert.h>
#include <criterion/redirect.h>
#include <fcntl.h>
#include <signal.h>
#include <stdio.h>
#include <unistd.h>

#include "agdbentures.h"

int exit_x;
int exit_y;

Test(read_input, read_command)
{
    FILE *f_stdin = cr_get_redirected_stdin();

    fprintf(f_stdin, "UP\n");
    fprintf(f_stdin, "DOWN\n");

    command *c = get_next_command();
    cr_assert_eq(c->buffer_size, 3);
    cr_assert_str_eq(c->command_buffer, "UP");
    command *c2 = get_next_command();
    cr_assert_eq(c2->buffer_size, 5);
    cr_assert_str_eq(c2->command_buffer, "DOWN");

    // check that old buffer is not destroyed
    cr_assert_str_eq(c->command_buffer, "UP");
    free_command(c);
    free_command(c2);

}

Test(read_input, apply_input)
{
    FILE *f_stdin = cr_get_redirected_stdin();

    fprintf(f_stdin, "LEFT\n");
    fprintf(f_stdin, "DOWN\n");
    fprintf(f_stdin, "WRONG\n");
    fprintf(f_stdin, "RIGHT\n");
    fprintf(f_stdin, "UP\n");

    map *m = init_map(5, 6);
    place_player(m, 3, 3);

    int res;
    // left
    command *c = get_next_command();
    res = apply_input(m, c);
    cr_assert_eq(res, 1);
    cr_assert_eq(m->player_x, 2);
    cr_assert_eq(m->player_y, 3);
    free_command(c);

    // down
    c = get_next_command();
    res = apply_input(m, c);
    cr_assert_eq(res, 1);
    cr_assert_eq(m->player_x, 2);
    cr_assert_eq(m->player_y, 4);
    free_command(c);

    // wrong
    c = get_next_command();
    res = apply_input(m, c);
    cr_assert_eq(res, -1);
    cr_assert_eq(m->player_x, 2);
    cr_assert_eq(m->player_y, 4);
    free_command(c);

    // right
    c = get_next_command();
    res = apply_input(m, c);
    cr_assert_eq(res, 1);
    cr_assert_eq(m->player_x, 3);
    cr_assert_eq(m->player_y, 4);
    free_command(c);

    // up
    c = get_next_command();
    res = apply_input(m, c);
    cr_assert_eq(res, 1);
    cr_assert_eq(m->player_x, 3);
    cr_assert_eq(m->player_y, 3);
    free_command(c);
    free_map(m);
    fclose(f_stdin);
}
Test(read_input, handle_problems, .exit_code = 2)
{
    FILE *f_stdin = cr_get_redirected_stdin();
    fprintf(f_stdin, "toto\n");
    fprintf(f_stdin, "\n");

    map *m = init_map(5, 6);
    place_player(m, 3, 3);

    int res;
    command *c = get_next_command();
    res = apply_input(m, c);

    cr_expect(res == -1, "unknown command");
    free_command(c);
    c = get_next_command();
    res = apply_input(m, c);
    cr_expect(res == -1, "empty command");
    free_command(c);
    // Next one should make read fail with unavailable resource
    fclose(f_stdin);
    c = get_next_command();
    res = apply_input(m, c);
    free_command(c);
    free_map(m);
    cr_fail("Should not continue after reading from a closed stdin");
}

Test(read_input, handle_EOF, .exit_code = 2)
{
    FILE *f_stdin = cr_get_redirected_stdin();
    int fd = open("/dev/null", O_RDONLY);
    dup2(fd, fileno(f_stdin));
    close(fd);

    map *m = init_map(5, 6);

    command *c = get_next_command();
    apply_input(m, c);
    cr_fail("Should not continue after reading when received EOF");
    free_command(c);
    free_map(m);
    fclose(f_stdin);
}
