#include <criterion/criterion.h>
#include <criterion/internal/assert.h>
#include <criterion/internal/test.h>

#include "agdbentures.h"

int exit_x;
int exit_y;

Test(simple_map, create_empty_map)
{
    map *m = init_map(5, 6);

    cr_assert(m->width == 6);
    cr_assert(m->height == 5);

    for (int y = 0; y < 5; y++) {
        for (int x = 0; x < 6; x++) {
            cr_assert(m->tiles[y*6 + x] == EMPTY);
        }
    }
    cr_assert(m->player_direction == DIR_UNKNOWN);
    free_map(m);

}

Test(simple_map, place_entity)
{
    map *m = init_map(5, 6);

    place_player(m, 2, 1);
    cr_assert(m->player_x == 1);
    cr_assert(m->player_y == 2);

    place_entity(m, 'X', 3, 5);
    cr_assert(m->tiles[3*6 + 5] == 'X');
    free_map(m);
}

Test(simple_map, remove_entity)
{
    map *m = init_map(5, 6);

    place_entity(m, 'X', 3, 5);
    remove_entity(m, 3, 5);
    cr_assert(m->tiles[3*6 + 5] == EMPTY);
    free_map(m);

}

Test(simple_map, move_entity)
{
    map *m = init_map(5, 6);

    place_entity(m, WALL, 2, 1);
    cr_assert(m->tiles[2*6 + 1] == WALL);

    left(m, 2, 1);
    cr_assert(m->tiles[2*6 + 0] == WALL);

    down(m, 2, 0);
    cr_assert(m->tiles[3*6 + 0] == WALL);

    right(m, 3, 0);
    cr_assert(m->tiles[3*6 + 1] == WALL);

    up(m, 3, 1);
    cr_assert(m->tiles[2*6 + 1] == WALL);

    left(m, 2, 1);
    left(m, 2, 1);
    cr_assert(m->tiles[2*6 + 0] == WALL);
    free_map(m);

}

Test(simple_map, move_player)
{
    map *m = init_map(5, 6);

    place_player(m, 2, 1);

    cr_assert(m->player_x == 1);
    cr_assert(m->player_y == 2);
    cr_assert(m->player_direction == DIR_UNKNOWN);

    player_left(m);
    cr_assert(m->player_x == 0);
    cr_assert(m->player_y == 2);
    cr_assert(m->player_direction == DIR_LEFT);

    player_down(m);
    cr_assert(m->player_x == 0);
    cr_assert(m->player_y == 3);
    cr_assert(m->player_direction == DIR_DOWN);

    player_right(m);
    cr_assert(m->player_x == 1);
    cr_assert(m->player_y == 3);
    cr_assert(m->player_direction == DIR_RIGHT);

    player_up(m);
    cr_assert(m->player_x == 1);
    cr_assert(m->player_y == 2);
    cr_assert(m->player_direction == DIR_UP);

    player_left(m);
    player_left(m);
    cr_assert(m->player_x == 0);
    cr_assert(m->player_y == 2);
    cr_assert(m->player_direction == DIR_LEFT);

    exit_x = 0;
    exit_y = 2;
    verify_exit(m);
    free_map(m);

}

Test(simple_map, not_on_exit, .exit_code = EXIT_FAILURE)
{
    map *m = init_map(5, 6);

    exit_y = 2;
    exit_x = 5;

    place_player(m, exit_y, 1);
    place_entity(m, WALL, exit_y, 3);

    player_right(m);
    player_right(m);
    player_right(m);
    cr_expect(is_obstacle(get_entity(m, exit_y, 3)));

    place_entity(m, DOOR, m->player_y+1, m->player_x);
    cr_assert(player_down(m) == 0);

    // Player should not be on exit, but stuck behing wall;
    // verify_exit should fail, i.e., exit on EXIT_FAILURE;
    verify_exit(m);
    free_map(m);

}

Test(simple_map, player_move_limits)
{
    map *m = init_map(2, 2);
    place_player(m, 0, 0);
    cr_assert(!can_move_to(m, 0, 0, 1, 1), "no diagonal movement");
    cr_assert(player_left(m) == 0, "should not move out of the map");
    cr_assert(player_up(m) == 0, "should not move out of the map");
    cr_assert(player_right(m) == 1, "one move right allowed");
    cr_assert(player_right(m) == 0, "no more move to the right");
    cr_assert(player_down(m) == 1, "one move down allowed");
    cr_assert(player_down(m) == 0, "no more move down");
    cr_assert(player_left(m) == 1, "one move left allowed");
    cr_assert(player_left(m) == 0, "no more move to the left");
    free_map(m);

}
