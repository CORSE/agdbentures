#include "debug.h"
#ifdef EVENTS
#ifndef EVENTS_H
#define EVENTS_H

#include "stdlib.h"

#include "game.h"

typedef struct event_s {
#ifdef EVENT_ARGS
  void (*event)(game_instance *game, void *);
  void *params;
#else
  void (*event)(game_instance *game);
#endif
  struct event_s *next;
} event_struct;



#ifdef EVENT_ARGS
event_struct* create_event(void (* event_to_add) (game_instance *game, void*), void* params);
#else
event_struct* create_event(void (* event_to_add) (game_instance *game));
#endif

#ifdef EVENT_ARGS
void add_event(game_instance *game,
               void (*event_to_add)(game_instance *game, void *), void *params);
void remove_event(game_instance *game,
                  void (*event_to_remove)(game_instance *game, void *));
#else
void add_event(game_instance *game, void (*event_to_add)(game_instance *game));
void remove_event(game_instance *game,
                  void (*event_to_remove)(game_instance *game));
#endif
#ifdef EVENT_ARGS
void change_params(game_instance *game,
                   void (*event_to_change)(game_instance *game, void *),
                   void *params);
#endif
void remove_all_events(game_instance *game);

/**
 * Applies all triggers and events in sequence. Due to this, the event ordering
 * matters.
 */
void apply_events(game_instance *game);

#endif // EVENTS_H
#endif
