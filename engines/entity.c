#include "debug.h"
#ifdef ENTITY_STRUCT
#include "entity.h"

#ifdef ENTITY_PROPERTIES
tile create_tile(floor_type f, entity_property properties, char display_symbol) {
    tile t = {f, properties, display_symbol};
    return t;
}
#endif

#ifdef ENTITY_PROPERTIES
entity create_entity(char symbol, entity_property properties, entity_category category, void *stats) {
    #else
    #ifdef CUSTOM_ENTITY
entity create_entity(char symbol, entity_category category, void *stats) {
    #else
entity create_entity(char symbol) {
    #endif
#endif
    if (symbol == ' ') {
        return NULL;
    }
    entity e = malloc(sizeof(struct entity_s));
    e->id = -1;
#ifdef CUSTOM_ENTITY
    e->category = category;
    e->stats = stats;
#endif
#ifdef ENTITY_PROPERTIES
    e->properties = properties;
#endif
    e->display_symbol = symbol;

    return e;
}

#ifdef ENTITY_PROPERTIES
tile create_wall() {
    return create_tile(F_WALL, OBSTACLE, '#');
}
#endif


#ifdef ENTITY_PROPERTIES
bool has_property(entity_property e, entity_property property) {
    return e & property;
}
#endif

#ifdef ENTITY_PROPERTIES
void add_property(entity_property* e, entity_property property){
    *e = (*e) | property;
}
#endif

#ifdef ENTITY_PROPERTIES
void remove_property(entity_property* e, entity_property property){
    *e = (*e) & ~property;
}
#endif

#endif
