#include "debug.h"
#ifdef LEVEL_END
#include <stdio.h>

#include "map.h"
#include "game.h"
#ifdef EVENTS
#include "events.h"
#endif

#ifdef MAP_STACK
game_instance *init_game() {
#else
game_instance *init_game(int height, int width) {
#endif
    game_instance* game = malloc(sizeof(game_instance));
#ifdef MAP_STACK

    game->map_stack = init_map_stack();
#else

    game->map = init_map(height, width);
#endif
#ifdef MAP_GRAPH

    game->current_map = NULL;
#endif
#ifdef EVENTS

    game->event_list = NULL;
#endif
#ifdef INVENTORY_ARRAY

    for(int i = 0; i < MAX_INVENTORY_SIZE; i++) {
        game->inventory[i] = NULL;
    }
#endif

    game->exit_main_loop = false;
    game->level_exit_code = -1;

    return game;
}

void free_game(game_instance *game) {
#ifdef MAP_STACK
#ifdef MAP_GRAPH
    free_map_stack(game->map_stack, game->current_map);
#else
    free_map_stack(game->map_stack);
#endif
#else
    free_map(game->map);
#endif
#ifdef MAP_GRAPH
    free_map(game->current_map);
#endif
#ifdef EVENTS
    remove_all_events(game);
#else
#endif
    free(game);
}

void say(char *message) {
    printf("%s\n", message);
}

void level_failed(game_instance *game, int exit_code) {
    printf("DEFAITE!\n");
    game->exit_main_loop = true;
    game->level_exit_code = exit_code;
}

void level_success(game_instance *game) {
    printf("VICTOIRE!\n");
    game->exit_main_loop = true;
    game->level_exit_code = 0;
}

#ifdef MAP_GRAPH
map *take_map_link(game_instance *game, map *m) {
    map_link* link = m->map_link_list;
    while(link != NULL) {
        // if we found the link to take
        if(m->player_x == link->from_x && m->player_y == link->from_y) {
            map* dest_map = link->destination;
            dest_map->player_x = link->dest_x;
            dest_map->player_y = link->dest_y;
            game->current_map = dest_map;
            place_player(dest_map, link->dest_y, link->dest_x);
            return dest_map;
        }

        link = link->next;
    }

    return NULL;
}
#endif


#endif