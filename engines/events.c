#include "debug.h"
#ifdef NEVER
/**
 * The event manager. Loads, watches and triggers events.
 * Called by the main program at each frame (main loop).
 * 
 * Concepts used:
 *  - pointers to functions
 *  - side-effecting functions
 *  - communication with the main function (to stop the main loop)
 * 
 * Possible bugs:
 *  - creating a trigger function which changes the state of the game
 *  - calling two events in the wrong order because they were loaded in reverse
 * 
 * Game design related tip:
 *  Simple games may not use events at all. More complete game engines have several complex
 *  events loops. For example, have a look at how Unity handles the execution sequence:
 *  <https://docs.unity3d.com/Manual/ExecutionOrder.html>
 *  Of course, if you create a game with Unity you will probably not use all of them.
*/
#endif
#ifdef EVENTS
#include "events.h"

#ifdef EVENT_ARGS
event_struct* create_event(void (* event_to_add) (game_instance *game, void*), void* params) {
#else
event_struct* create_event(void (* event_to_add) (game_instance *game)) {
#endif
    event_struct *new_event;
    new_event = malloc(sizeof(event_struct));
    new_event->event = event_to_add;
#ifdef EVENT_ARGS
    new_event->params = params;
#endif
    new_event->next = NULL;

    return new_event;
}

#ifdef EVENT_ARGS
void add_event(game_instance* game, void (*event_to_add)(game_instance *game, void *), void *params) {
#else
void add_event(game_instance *game, void (* event_to_add) (game_instance *game)) {
#endif
    if (!game->event_list) {
#ifdef EVENT_ARGS
        game->event_list = create_event(event_to_add, params);
#else
        game->event_list = create_event(event_to_add);
#endif
        return;
    }

    event_struct * cur = game->event_list;

    while (cur->next){
        cur = cur->next;
    }

#ifdef EVENT_ARGS
    cur->next = create_event(event_to_add, params);   
#else
    cur->next = create_event(event_to_add);
#endif
}

#ifdef EVENT_ARGS
void remove_event(game_instance* game, void (* event_to_remove) (game_instance *game, void*)) {
#else
void remove_event(game_instance *game, void (* event_to_remove) (game_instance *game)) {
#endif
    if (!game->event_list)
        return;

    event_struct * cur = game->event_list;
    event_struct * pred = NULL;

    while (cur->event != event_to_remove && cur) {
        pred = cur;
        cur = cur->next;
    }
    
    if (!cur)
        return;
    
    if (!pred) {
        game->event_list = cur->next;
    } else {
        pred->next = cur->next;
    }

    free(cur);
}

void remove_all_events(game_instance* game) {
    event_struct * to_free;

    while (game->event_list) {
        to_free = game->event_list;
        game->event_list = to_free->next;
        free(to_free);
    }
}

#ifdef EVENT_ARGS
void change_params(game_instance* game, void (*event_to_change)(game_instance *game, void *), void *params) {
    event_struct * cur = game->event_list;
    while (cur != NULL) {
        if (cur->event == event_to_change) {
            cur->params = params;
            return;
        }
    }
}
#endif

void apply_events(game_instance* game) {
    event_struct * cur = game->event_list;
    while (cur != NULL) {
#ifdef EVENT_ARGS
        cur->event(game, cur->params);
#else
        cur->event(game);
#endif
        cur = cur->next;
    }    
}
#endif