# Game engine API

The goal of this small library is to provide a simple framework for creating 2D games.  
The map is a 2D grid, with tiles (floor, obstacles...) and entities (boxes to push, monsters, items...).
Inputs can be prompted or read from a file, and a simplistic events manager is given.  
The game can then be played in a console, with ASCII art. To create an actual graphic interface we will probably use EasyTracker.


## Including the library and common stuff
(Implemented)

Use `#include "agdbentures/agdbentures.h"`.

The *bool exit_main_loop* is defined as static and global variable.
Use it in your main loop. It will be modified by some functions of the library.

Two basic functions for success and defeat are defined: `level_success(void)` and
`void level_failure(void)`. They set *exit_main_loop* as true and display a message.  
Feel free to write your own if you need something more complex.


## Game object

The game object have the following properties.
All are not include in all engine version in order to simplify level perception and conception

```c
typedef struct game_s {
  map_stack *map_stack; // La pile de cartes est stockee dans le moteur.
  map *current_map;
  entity *inventory[MAX_INVENTORY_SIZE];
  struct event_s *event_list;
  bool exit_main_loop; // Un booleen qu'on peut utiliser dans une boucle qui
                       // accepte des commandes
  int level_exit_code; // Le code de retour a renvoyer par le programme
} game_instance;
```

Map stack is describe in the next paragraph, it's the stack who contains all different map of the level. [Map stack](#managing-map)\

Current map is the map where player is for the moment \
Properties of inventory is describe later in this document, It's an array of pointer on entity. [Inventory](#inventory)\
Event_list is a linked list where each cell can contain a pointer on a fucntion with his parameter in order to make interaction during game [Events](#managing-events)\
Exit_main_loop is used for the loop of the level, usually we we used it like this [Main Loop](#main-loop)\


## Managing map

(Implemented)

> **TODO:**
> 
> map array instead of stack (can go to any map of the array, unload any, get id...)

You have access to the following types and definitions: \
Note that it can be a bit different depending on the engine you work
```c
typedef enum Direction {
    UP,
    DOWN,
    LEFT,
    RIGHT,
    UNKNOWN
} direction;

typedef struct {
    char * name;       // shop, overworld, dungeon_Nth_level...
    tile ** floor;      // immobile floor
    entity *** entities; // ennemies, NPCs, items, switches... anything used by events
    int width;         // map size
    int height;
    int player_x;      // player position & direction
    int player_y;
    int player_direction;
} map;

typedef struct {
    int category;  // identifier to display the tile
    bool obstacle; // whether the player can move on it or not
} tile;

```

For pEdaGoGIcaL PUrpOSeS, the maps are managed using a stack.
This means it's super cool for creating a donjon, but terribly bad
if you have any cyclic path in your world.

At one position of the map, there can be 1 tile, 1 entity and the player.  
2 tiles can't be at the same place, same for 2 entities.

To see more about entity got to [Entity](#entity)

### `void init_map_stack(void)`
At the beginning, you have to initialize the map stack, by calling `init_map_stack(void)` once.  
Don't call other map functions before that.  

### `map * load_map(const char * name, const char * data)`
Allocates memory for a map, which becomes the current map.  
See [Map Data](#map-data) for details.

### `void pop_from_stack(void)`
Frees the allocated memory of a map, and the previous map
in the stack becomes the current map. Returns NULL if the unloaded
map was the last in the stack.  
Also frees the memory for *floor* and *entities* (and *entities->stats*).

### `map * current_map(void)`
Returns the current map, which is on top of the stack.  
Returns NULL if there is no map in the stack.  

### `void free_map_stack(void)`
Call `pop_from_stack(void)` for each map.    
After that the map stack is the same as after calling `ini_map_stack(void)` at the beginning.

### `int coord_idx(int y, int x)`
Utilitary function, returns the number *i* such that *current_map()->floor[i]*
and *current_map()->entities[i]* correspond to the coordinates (*x*, *y*).

### `int player_idx(void)`
Same but using *current_map()->player_x* and *current_map()->player_y*.


## Managing inputs

(Implemented)

The following type is defined:
```c
typedef struct Command {
    char * command_buffer; // the name of the command
    unsigned n_args;       // a number of optional arguments (can be 0)
    char ** args;          // the arguments, as many as n_args (NULL if n_args is 0)
} command;
```

### `bool init_inputs_file(const char * filename)`

Call this once if you want to read the inputs from a file. If you don't call it,
the inputs will be prompted on the stdin.  
If *filename* is valid, the inputs will be read in this file (one per line, end with a blank line).  
So each input in an input file must be written like `<input> [args]\n`.  
Example of an input file (notice the blank line at the end):
```
UP
RIGHT_N 5
UP
ATTACK

```
Returns true on success, false on failure (if you gave a non-empty *filename* but the file could not be opened). \
Returns false and fails if you try to open a new file without closing the current opened file.

### `void close_inputs_file(void)`
Closes and frees the current file opened with `init_inputs(const char * filename)`.

### `command * get_next_command(void)`
Allocates memory for the next user input and returns it.  
Blocking until the player gives an input, because the game engine is simplistic and turn-based. \
Returns `NULL` if inputs are being read from a file but `EOF` has been reached.

### `void free_input(command * i)`
Frees the allocated memory of an input. To avoid memory leaks, you should call this once for each call to `get_next_command(void)`.

### `command * get_and_free_input(command * i)`
For your convenience, this function does exactly the same as `get_next_command(void)` but in addition it will free *i* if this parameter is not `NULL`.\
Setting *i* as NULL does the same exact thing is calling `get_next_command(void)`.

If you are reading inputs from a file, you have to call `close_inputs(void)` to close the file you opened. \
You also have to manage the case where `get_new_input(void)` returns `NULL`, indicating `EOF` has been reached.


## Managing events

(Implemented)

```c
typedef struct event_s {
  void (*event)(game_instance *game, void *);
  void *params;
  struct event_s *next;
} event;
```
Event is here in purpose to create some interaction during execution stored in the game directly\


### `void add_event(void (* event) (void))`
Adds an event to the end of the list. *event* must be a pointer to a function `void event(void)`.  
If *event* is already in the list, nothing happens.

### `void remove_event(void (* event) (void))`
Removes the given *event* from the list.
No effect if *event* is not in the list.

### `void remove_all_events(void)`
Removes all events from the list. Please call this at the end of
your program to free all the memory allocated to events.

### `void apply_events(void)`
Calls all *event* functions added by `add_event(void (* event) (void))`,
in a fixed order: the events added **last** are called **first**.

Inside a event function, you **cannot** call any of these 4 functions, because
you are already in the event execution loop and this will create problems, either by accessing freed memory, or by creating infinite recursion.


## Functions given for convenience

> **TODO:**
> 
> monsters and NPC bases 
> (Implemented except monsters and NPC)

**`void up(void)`**  
**`void down(void)`**  
**`void left(void)`**  
**`void right(void)`**  
Moves the player in the given direction and change its orientation.  
Displays a message if the player can't move because of an obstacle.

**`void up_n(unsigned n)`**  
**`void down_n(unsigned n)`**  
**`void left_n(unsigned n)`**  
**`void right_n(unsigned n)`**  
Moves *n* times in a given direction.

**`void turn_left(void)`**  
**`void turn_right(void)`**  
**`void forward(void)`**  
**`void forward_n(unsigned n)`**  
Legacy functions to have the player direction change by a quarter of a turn,
or to have the player move forward, depending of the current direction faced.

All these functions will modify the values *player_x*, *player_y* and *player_direction*
of the current map.  
Functions `*_n(unsigned n)` will call `apply_events(void)` n - 1 times,
so the main loop stays clean (see [Main Loop](#main-loop)).

Talking, attacking, interacting depend of your game. Code them yourself.


## Inventory
The inventory is store in the game object and is unique

### `void clear_inventory(game_instance *game)`
This function set NULL value to all the inventory, size of inventory is define in game.h

### `int remove_item_from_inventory(game_instance *game, entity *e)`
This function search in the inventory if the item is present.
If not then it will return 1 else if item is found the item will be delete from the inventory and function return 0

### `bool has_item_in_inventory(game_instance *game, entity *e)`

Like previous function, return 0 if item found and 1 else but item will stay in inventory

### `int add_item_in_inventory(game_instance *game, entity *e)`

This function search the first empty emplacement in the inventory and put the item inside
then return  0.
If inventory is full then function don't store the item and return 1

### `int inventory_size(game_instance *game)`
return the number of item stored at the moment in the inventory.

### `int drop_item(game_instance *game, entity *e, int y, int x)`

This function search if we have the entity in our inventory and drop it on the map in position (x,y)
if we have. It return 0 if we have th entity and 1 else


### `entity *pick_item(game_instance *game, int y, int x)`

This function search item on position (x,y) and if entity is pickable at this place we take it and put it in the inventory



## Main Loop

The (Holy) Main Loop should look like this:
```c
// before: all initializations

while (!exit_main_loop) {
    command = get_next_command();

    apply_input(command);

    if (!exit_main_loop)
        apply_events();

    free_input(command);
}

// after: all frees
```
You have to write `apply_input(input * command)` yourself if you have some exotic commands. but it's quite easy. you can see a good example in [input_manager.c](../levels/harder/interrupteurs/input_manager.c).

Here we have the assumption that the game is turn-based, so nothing happens as long as the player doesn't give any input. For real-time games, the call to `get_next_command(void)` will generally be at the end, and a timer determines when the next frame occurs, whether there has been an input or not. But this game engine is simplistic and turn-based, so `get_next_command(void)` is blocking.

Don't forget to free all the allocated memory at the end, notably by calling `remove_all_events(void)` and `free_map_stack(void)`.


## Entity

```c
typedef enum EntityProperty {
  ACTIVABLE = 1, // switch to activate 'O' or 'X'
  PICKABLE = 2,
  PUSHABLE =
      1 << 3, // (obstacle/can't walk on it) a block that can be pushed like in
  TRAP = 1 << 4, // an invisible trap '_',
  OBSTACLE = 1 << 5,
} entity_property;

typedef enum EntityCategory {
  C_NONE = 0,
  C_MONSTER = 1,  // generic monster 'M'
  C_NPC = 2,      // generic NPC 'A'
  C_ITEM = 3,     // generic item 'I'
  C_PLAYER = 4,
  C_FLAG = 5,     // pinpoint special location
  C_EXIT = 6,     // exit the map or whole level
} entity_category;

typedef struct entity_s {
  int id; // identifier to display and use in events
  entity_category category; // used to group in events, like monster, NPC...
  void *stats; // any property the entity may have (life points, state...)
  entity_property properties;
  char display_symbol; // The symbol of the Entity on an ASCII map
  struct entity_s *next;
} entity;

```
- `Entity` is an object or a person on the map with certain properties
- `id` is used to know on which entity we are and it need to be unique
- `stat` is not necessary in all level, it can be used to add more properties to entity.
- `properties` is used to know how the player can interact with the entity
- `display_symbol` is use to link the `struct` to the map via the `str_map` see [Map data](#map-data)
- `next` is here due to the usage of linked chain but it's not for all levels

## Map data

The data you give to the map creator `load_map(const char * name, const char * data)` is a string. The function will deduce the width and height, along with the player position and orientation.

The following categories are defined for tiles and entities. For entities, you have to set the stats yourself, they are NULL by default.

```c
typedef enum {
  F_EMPTY,    // unknown/unreachable
  F_GROUND,      // standard floor ' '
  F_WALL,     // (obstacle) '-' or '|' or '+'
  F_WATER,      // can be used in event to drown the player '~'
  F_DOOR,       // (obstacle) '[' or ']'
  F_TARGET,     // can be used in events 'T'
  AREA_WHITE,    // For special levels: color of floor
  AREA_GRAY,     // For special levels: color of floor
  AREA_HASHED,   // For special levels: color of floor
  FLOORTYPE_N   // last one to know how many types there are
} floor_type;

// example of a map data string
const char *str_map =
"\
+-----+--+\n\
|X   X|~~|\n\
|     +--+\n\
|>    [  @\n\
|     +--+\n\
|X   X|~~|\n\
+-----+--+\n\
";
```

When the function sees a character for an entity, it will create it and set the ground beneath as `F_GROUND`. It's up to you to update the ground if you need. One such example is [custom_map.c](../levels/harder/interrupteurs/custom_map.c), where the ground below `SWI` is set as `F_WALL`.
If an unknown character is met, the ground will be `EMPTY`. You can then change it if you wanted to add your own stuff.  If there is no entity on a tile, the entity is `NULL`.

For the player, it will be placed at the position of a character `>`, `<`, `^` or `v` and its direction depends of the character (`^` for up...).
If no character for the player is seen, its position is set as (-1, -1) and you have to set it yourself.
If several characters are met (you shouldn't do that), the last seen is taken into account.

--------------------------------------------------------------------------------

--------------------------------------------------------------------------------

## IDÉES DE BUGS (dans les niveaux)

- [/] `simple_map` malloc(sizeof(entity)) au lieu de `sizeof(entity*)`
  TODO: ce bug a probablement disparu suite au retour à un tableau 2d linéarisé 
  pour la map

- [ ] bug copy paste `player_move_right m->player_x` en premier argument de 
  `can_move_to` au lieu de `player_y`

- [x] `read_map_str` oublier de `read_map`
- [x] `read_map_str` inverser width, height

- [ ] error allocation floor
- [ ] dont init floor with empty everywhere

- [ ] `map_stack` avec un seul load map et on partage tout avec `push_on_stack`

- [ ] inventory sur place ramasse le player donc il faut faire specialement si c'est la position du joueur
