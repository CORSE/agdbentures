#include "debug.h"
#ifdef EVENTS
#include "prefab_events.h"

// verify if the player is on the exit
void verify_exit(game_instance* game) {
    map * map = current_map(game->map_stack);
    if (find_category_in_stack(map, C_EXIT, map->player_y, map->player_x)) {
        printf("You reached the exit!\n");
        level_success(game);
    }
}

// verify if the player is on water
void drown(game_instance* game) {
    map * m = current_map(game->map_stack);
    if ((get_tile(m, m->player_y, m->player_x)).floor == F_WATER) {
        printf("You drown!\n");
        exit(EXIT_FAILURE);
    }
}

#endif
