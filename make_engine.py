#!/usr/bin/env python3
"""
Script that generates the different engine versions by using unifdef on the
engine sources, a activating a different set of preprocessor variables for each version.
"""

import os
import sys
from pathlib import Path
from shutil import rmtree, copyfile
import subprocess
import logging
import argparse
from string import Template

v = sys.version_info
# Make sure to insert first in the search path the current lib and easytracker directory
cdir = os.path.dirname(__file__)
sys.path.insert(
    0, os.path.join(cdir, f".venv/lib/python{v.major}.{v.minor}/site-packages")
)  # if using a python virtual env
sys.path.insert(0, os.path.join(cdir, "lib"))
sys.path.insert(0, os.path.join(cdir, "easytracker"))

from loader.loader_utils import apply_unifdef

parser = argparse.ArgumentParser()
parser.add_argument(
    "--debug",
    "-d",
    type=int,
    choices=[0, 1, 2, 3],
    default=2,
    help="Set debug level (0 = crit, 1 = warn, 2 = info, 3 = debug)",
)

parser.add_argument(
    "--all",
    "-a",
    action="store_true",
    help="Generates engines for all features, even those under work",
)

parser.add_argument(
    "--which",
    "-w",
    type = str,
    default = None,
    help="Generates engines for features chosen",
)


parser.add_argument(
    "--list",
    "-l",
    action="store_true",
    help="Only list the engines that would be generated",
)

parser.add_argument(
    "--test",
    "-t",
    action="store_true",
    help="Generate engine with test files",
)


args = parser.parse_args()

if args.debug == 0:
    loglevel = logging.CRITICAL
elif args.debug == 1:
    loglevel = logging.WARNING
elif args.debug == 2:
    loglevel = logging.INFO
else:
    loglevel = logging.DEBUG

logging.basicConfig(level=loglevel)


TEST_DIR = os.path.join(cdir, "engines", "tests")

if args.test:
    VERSION_DIR = os.path.join(cdir, "engine_tests")
else:
    VERSION_DIR = os.path.join(cdir, "engine_versions")

logging.info(f"Deleting exist versions in {VERSION_DIR}")
rmtree(VERSION_DIR, ignore_errors=True)


makefile_template = """SRCS = $$(wildcard *.c)
OBJS = $$(SRCS:.c=.o)
CC := gcc
CFLAGS := -O0 -g -Wall

all: $$(OBJS)
	ar cr engine.a $$(OBJS)

.c.o:
	$$(CC) $$(CFLAGS) -c $$< -o $$@
$test_rule

clean:
	rm *.o
	rm engine.a
	rm test

.PHONY: all clean $phony_test
"""

test_rule = """
test: all
	$(CC) $(CFLAG) $(OBJS) -o test -lcriterion
	./test
"""

tmplt = Template(makefile_template)
if args.test:
    makefile_contents = tmplt.substitute(test_rule=test_rule, phony_test="test")
else:
    makefile_contents = tmplt.substitute(test_rule="", phony_test="")


fevol = os.path.join(cdir, "engine_evolution.org")
logging.info(f"Reading engine evolutions from {fevol}")

# Save the first feature that is NOT to be generated into a version
do_feature_stop = False
feature_stop = None

with open(fevol) as f:
    feature_list = []

    for l in f:
        # Toggle take features at second main section unless --all is asked
        # We detect this by considering there is at least one
        # feature working in the first section
        if l.startswith('* '):
            if feature_list and not args.all:
                do_feature_stop = True

        if l.startswith('** '):
            if do_feature_stop:
                feature_stop = l.strip()[3:]
                do_feature_stop = False
            feature_list.append(l.strip()[3:])

if args.list:
    print("Engines:")
    for f in feature_list:
        print(f"\t{f}")
    exit(0)

logging.info(f"Feature list: {feature_list}")

flag_compile = True
for i, feature in enumerate(feature_list):
    if feature == feature_stop or not flag_compile:
        break
    if args.which and ( feature == args.which or feature == args.which.upper() ):
        flag_compile = False
    logging.info(f"Creating engine for feature {feature}")
    # create dest dir
    engine_dir = os.path.join(VERSION_DIR, f"{i:02d}_{feature.lower()}")
    os.makedirs(engine_dir, exist_ok=True)

    # apply unifdef
    defines = feature_list.copy()
    udefines = ["NEVER"]

    # Add all defines leading up to the current feature
    for _ in range(len(feature_list) - i - 1):
        udefines.append(defines.pop())

    logging.debug(f"Will use defines {defines} and udefines {udefines}")

    for file_name in os.listdir("engines"):
        file_path = os.path.join("engines", file_name)
        if (
            os.path.isfile(file_path)
            and os.path.basename(file_path)[0] != '.'  # do not process hidden files
            and file_name not in ["API.md", "debug.h"]
        ):
            out_file = os.path.join(engine_dir, file_name)
            logging.debug(f"Generating file {out_file}")
            apply_unifdef(file_path, out_file, defines, udefines)

            # remove first line if #include "debug.h"
            output = subprocess.run(["head", out_file, "-n", "1"], capture_output=True)
            # logging.debug(f"Output of head command: {output.stdout}")
            if output.stdout.decode() == '#include "debug.h"\n':
                os.system(f"sed '1d' {out_file} > tmpfile; mv tmpfile {out_file}")

            # if the file is empty, delete it
            size = os.path.getsize(out_file)
            # this is to remove files that contains only #include "debug.h"
            if size == 0 and file_name != "config.h":
                logging.debug(f"\tDeleting empty file")
                os.remove(out_file)

    # put makefile
    with open(os.path.join(engine_dir, "Makefile"), "w") as f:
        print(makefile_contents, file=f)

    # Put test file as a symlink
    if args.test:
        tstfile = os.path.join(TEST_DIR, feature.lower(), "tests.c")
        if os.path.exists(tstfile):
            os.symlink(tstfile, os.path.join(engine_dir, "tests.c"))
        else:
            logging.warning(
                f"Could not symlink test file feature {feature}, using generic test"
            )
            os.symlink(
                os.path.join(TEST_DIR, "generic_tests.c"),
                os.path.join(engine_dir, "tests.c"),
            )

# place global makefile for tests
global_makefile_str = """SUBDIRS := $(wildcard */)

all:
	@for d in $(SUBDIRS); do \\
		echo "Testing engine version $$d"; \\
		make -C $$d test || exit 1; \\
	done

.PHONY: all
"""

if args.test:
    with open(os.path.join(VERSION_DIR, "Makefile"), "w") as f:
        print(global_makefile_str, file=f)
