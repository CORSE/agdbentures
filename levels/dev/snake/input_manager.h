#ifndef INPUT_MANAGER_H
#define INPUT_MANAGER_H

#include "engine/agdbentures.h"

// examine la commande c et appelle les fonctions d'action ou de déplacement
// correspondantes
void custom_apply_input(game_instance *game, command *c);

#endif // INPUT_MANAGER_H