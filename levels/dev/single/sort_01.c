void swap(int tab[], int i, int j) {
    int temp = tab[i];
    tab[i] = tab[j];
    tab[j] = temp;
}

void sort(int tab[], int size) {
    int biggest_0 = 0;
    int smallest_1 = size;

    int i = 0;
    while (biggest_0 < smallest_1) {
        if (tab[i] == 0) {
            biggest_0++;
            i++;
        } else {
            smallest_1--;
            swap(tab, i, size);
        }
    }
}

int main() {
    int tab[5] = {0, 1, 1, 0, 0};

    sort(tab);

    return 0;
}
