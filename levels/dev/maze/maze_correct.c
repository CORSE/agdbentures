#include "maze.h"

void place_vertical_wall(square_t **walls, int x, int y, int height) {
    printf("place wall v x:%d y:%d l:%d\n", x, y, height);
    // JAI ECRIT CA LOL CA MARCHE QUE QUAND CEST 0 JPP for(int j=y; j<height; j++) {
    for(int j=0; j<height; j++) {
        //walls[x][y] = 1; INVERSION ROW-COLUMN MAJOR
        walls[x][y+j].vertical_wall = 1;
    }
}

void place_horizontal_wall(square_t **walls, int x, int y, int width) {
    printf("place wall h x:%d y:%d l:%d\n", x, y, width);
    // PAREIL QUE AU DESSUS MAIS MDR
    for(int i=0; i<width; i++) {
        //walls[i][y] = 1; INVERSION VERTICAL HORIZONTAL
        //walls[x][y] = 1; INVERSION ROW-COLUMN MAJOR
        walls[x+i][y].horizontal_wall = 1;
    }
}

void place_horizontal_door(square_t **walls, int x, int y, int width) {
    int door_pos = rand() % width;

    //walls[x+door_pos][y] = 0; INVERSION VERTICAL HORIZONTAL
    //walls[y+door_pos][x] = 0; INVERSION ROW-COLUMN MAJOR
    walls[x+door_pos][y].horizontal_wall = 0;
}

void place_vertical_door(square_t **walls, int x, int y, int height) {
    int door_pos = rand() % height;

    //walls[x][y+door_pos] = 0; INVERSION VERTICAL HORIZONTAL LINVERSE EN FAIT
    walls[x][y+door_pos].vertical_wall = 0;
}

void divide_chamber(square_t **walls, int x, int y, int width, int height) {
    // Stop when we have a corridor
    printf("DIVIDE x:%d y:%d width:%d height:%d\n", x, y, width, height);
    if (width == 1 || height == 1) {
        return;
    }

    // Choose side to divide
    if (width >= height) {
        // Place vertical wall
        int vertical_wall = x + 1 + rand() % (width-1);
        printf("v: %d x: %d y: %d\n", vertical_wall, x, y);
        place_vertical_wall(walls, vertical_wall, y, height);
        place_vertical_door(walls, vertical_wall, y, height);

        // divide 1 0 3 3
        // v wall 2 0 3

        // 1 0 1 3
        divide_chamber(walls, x, y, vertical_wall-x, height);
        // 2 0 1 3
        // PLUS X
        divide_chamber(walls, vertical_wall, y, x+width-vertical_wall, height);
    } else {
        // Place horizontal wall
        int horizontal_wall = y + 1 + rand() % (height-1);
        printf("h: %d x: %d y: %d\n", horizontal_wall, x, y);
        place_horizontal_wall(walls, x, horizontal_wall, width);
        place_horizontal_door(walls, x, horizontal_wall, width);
        
        divide_chamber(walls, x, y, width, horizontal_wall-y);
        // PLUS Y
        divide_chamber(walls, x, horizontal_wall, width, y+height-horizontal_wall);
    }
}

maze_t generate_maze(int width, int height) {
    // Walls allocation, at the beginning all walls are empty
    maze_t maze;
    maze.width = width+1;
    maze.height = height+1;
    maze.walls = malloc(sizeof(square_t*) * maze.width);
    square_t** walls = maze.walls;
    for(int i=0; i < maze.width; i++) { // WIDTH AU LIEU DE MAZE.WIDTH
        maze.walls[i] = calloc(maze.height, sizeof(square_t));
    }

    // Enclose the maze
    // ICI JAVAIS FAIT UN BUG DE FOR NESTED WALLS[i][j] = 1
    place_horizontal_wall(walls, 0, 0, width);
    place_horizontal_wall(walls, 0, height, width); // WIDTH AU LIEU DE HEIGHT
    place_vertical_wall(walls, 0, 0, height);
    place_vertical_wall(walls, width, 0, height);

    // Recursively divide the chambers
    divide_chamber(walls, 0, 0, maze.width-1, maze.height-1);

    // Place the two exits on the two horizontal walls
    place_horizontal_door(walls, 0, 0, width);
    place_horizontal_door(walls, 0, height, width);
    
    return maze;
}

void display_maze_internal(square_t** walls, int width, int height, char* filename) {
    FILE* svg = svg_file(filename, width*20 + 20, height*20 +20);

    for(int j=0; j < height; j++) {
        for(int i=0; i < width; i++) {
            if (walls[i][j].horizontal_wall) {
                print_line(svg, 20+20*i, 20+20*j, 20*i+40, 20+20*j);
            }
            if (walls[i][j].vertical_wall) {
                print_line(svg, 20+20*i, 20+20*j, 20+20*i, 20*j+40);
            }
        }
    }

    end_svg(svg);
}

void display_maze(maze_t maze, char* filename) {
    display_maze_internal(maze.walls, maze.width, maze.height, filename);
}
