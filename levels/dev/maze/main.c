#include <stdio.h>

#include "maze.h"

int main() {
    srand(0);
    //srand((unsigned) time(&t));

    maze_t maze = generate_maze(5, 4);

    display_maze(maze, "out.svg");

    return 0;
}
