#ifndef SVG_H
#define SVG_H

#include <stdio.h>

FILE* svg_file(char* filename, int width, int height);
void print_line(FILE* svg_file, int xs, int ys, int xe, int ye);
void end_svg(FILE* svg_file);

#endif
