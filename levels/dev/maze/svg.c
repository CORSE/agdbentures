#include "svg.h"

FILE* svg_file(char* filename, int width, int height) {
    FILE* svg = fopen(filename, "w");

    fprintf(svg, "<svg width=\"%d\" height=\"%d\">\n", width, height);
    fprintf(svg, "  <rect width=\"%d\" height=\"%d\" style=\"fill:white;stroke-width:0\" />\n", width, height);

    return svg;
}

void print_line(FILE* svg_file, int xs, int ys, int xe, int ye) {
    fprintf(svg_file, "  <line x1=\"%d\" y1=\"%d\" x2=\"%d\" y2=\"%d\" style=\"stroke:rgb(255,0,0);stroke-width:2\" />\n", xs, ys, xe, ye);
}

void end_svg(FILE* svg_file) {
    fprintf(svg_file, "</svg>");
    fclose(svg_file);
}
