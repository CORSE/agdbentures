#ifndef MAZE_H
#define MAZE_H

#include <stdlib.h>
#include <stdio.h>

#include "svg.h"

typedef struct {
    int horizontal_wall;
    int vertical_wall;
} square_t;

typedef struct {
    int width;
    int height;
    square_t **walls; // an 2d array of walls in a column major way
} maze_t;

// Allocate a new generate maze from the given size
maze_t generate_maze(int width, int height);

void raw_display(square_t** walls, int width, int height);
void display_maze_internal(square_t** walls, int width, int height, char* filename);
void display_maze(maze_t maze, char* filename);

#endif
