/* @AGDB
 * level_title: monte
 * engine_name: none
 * exec_name: monte
 * engine_tags:
 * engine_antitags:
 */

#include <stdlib.h>

#define NUMBER_OF_MOVES 10

// The truth is obtained from a compiled file that has to be linked
int predict_end_position(int start_position, int * move_sequence);

int predict_end_position(int start_position, int * move_sequence) {
  int end_position = start_position;

  for(int i = 0; i < NUMBER_OF_MOVES; i++) {
    // if the coin will be moved
    if (end_position != move_sequence[i]) {
      end_position = 3 - end_position - move_sequence[i];
    }
  }

  return end_position;
}


// TODO move this function to another file to be precompiled or obfuscated

int * generate_move_sequence() {
  int * move_sequence = malloc(sizeof(int) * NUMBER_OF_MOVES);

  for(int i = 0; i < NUMBER_OF_MOVES; i++) {
    move_sequence[i] = rand() % 3;
  }

  return move_sequence;
}

int main() {
  // init random seed
  time_t t;
  srand((unsigned) time(&t));

  // init coin position
  int coin_position = rand() % 3;

  // generate sequence of moves
  // a move is encoded as a single integer which is the immobile card
  int * move_sequence = generate_move_sequence();

  // guess end position
  int end_position = coin_position;

  for(int i = 0; i < NUMBER_OF_MOVES; i++) {
    // if the coin will be moved
    if (end_position != move_sequence[i]) {
      // move it to the last available position (not the immobile one and not the actual one)
      /*
      if (end_position == 0) {
        end_position = 3 - move_sequence[i];
      } else if (end_position == 1) {
        end_position = 2 - move_sequence[i];
      } else {
        end_position = 1 - move_sequence[i];
      }
      */
      end_position = 3 - end_position - move_sequence[i];
    }
  }

  // compare and finish level
  if (end_position == predict_end_position(coin_position, move_sequence)) {
    return EXIT_SUCCESS;
  } else {
    return EXIT_FAILURE;
  }
}