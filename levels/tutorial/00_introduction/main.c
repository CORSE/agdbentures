/* @AGDB
 *
 * First level when entering the game. To bug in that level, it should just 
 * work...
 *
 * GUI only 'next' button for level 00.
 *
 * Upon succes, directly launch level 01 which is exactly the same, but with
 * the WOM (wise old man) or WOW midway to the exits, that talks to the player.
 *
 * level_title:   0-0 Introduction
 * exec_name:    00_introduction
 * level_number: 0
 * engine_name:  none
 *
 * available_commands: next
 * new_commands: next
 * hide_commands: restart
 *
 * no_verify: change_in_function player_x forward
 * no_verify: change_in_function player_y forward
 * no_verify: must_call verify_exit
 * no_verify_exit: cond player_y == exit_y
 *
 * arcade_maps: main ../tutorial.tmx
 * arcade_maps: main_offset 0 0
 **/
#include <stdlib.h>
#include <stdio.h>

/** @AGDB
 * Player status
 * player_y: 4
 *
 * Map goal
 * exit_x: 9
 * exit_y: 4
 *
 * Display panel, already placed on the tmx map.
 *
 * OBJpanel: strname panneau
 * OBJpanel: messageFR intro
 * Bienvenue jeune aventurier·e !
 * Pour valider un niveau, il faut se
 * trouver sur le pilier de lumière
 * quand le *programme se termine*.
 *
 *
 *
 * Tant que le triangle blanc est visible en haut à gauche, c'est que le 
 * programme tourne.
 *
 *
 * Bonne chance !
 * EndOfMessage
 *
 * OBJpanel: messageEN intro
 *
 * Greetings young adventurer!
 * To successfully pass a level,
 * you must be on the light pillar
 * when the *program ends*.
 *
 *
 *
 * As long as the white triangle on the top left is visible, the program is 
 * running.
 *
 *
 * Good luck!
 * EndOfMessage
 *
 */

int player_x = 4; /*FR Position du personnage *//*EN Character position */
int exit_x   = 9; /*FR Position de la sortie *//*EN Exit position */

int main(void)
{
    /*FR 5 deplacements a faire pour atteindre la sortie */
    /*EN 5 moves to reach the exit */
    player_x++;
    player_x++;
    player_x++;
    player_x++;
    player_x++;

    /*FR Verification de la position finale */
    /*EN Check if the end position is correct */
    if (player_x != exit_x) {
        printf("DEFAITE !\n"); //FR
        printf("DEFEAT!\n"); //EN
        exit (EXIT_FAILURE);
    } else {
        printf("VICTOIRE !\n"); //FR
        printf("VICTORY!\n"); //EN
        exit (EXIT_SUCCESS);
    }
}
