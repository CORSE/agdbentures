#!/usr/bin/env python3

from threading import Thread
from level.level_abc import AbstractLevel
from logs import tst
from config import Config

class Level(AbstractLevel):
    """00_intro"""

    def custom_first_start(self):

        player = self.world.get_player()
        panel = self.world.get_object("panel")
        self.world.exit.visible = True
        self.world.exit.send_update()
        def player_panel_update(player, *args):
            if player.is_below(panel):
                panel.talk('intro')

        player.post_update = player_panel_update

    def arcade_custom_restart(self):
        self.custom_first_start()
        
    def test(self):
        import tests.lib_test as T
        assert "panel" in self.world.maps["main"].named_objects

        self.recompile_bug()

        T.variable("player_x", 4)
        for i in range(2):
            T.cnext()
            T.variable("player_x", 5 + i)

        T.payload(action='talk', message_substr='Greetings')

        for i in range(3):
            T.cnext()
            T.variable("player_x", 7 + i)


        T.cnext()
        T.cnext()
        T.expect_sub_strings("VICTOIRE")
        T.cnext()
        T.expect_victory()
        self.run()

    def testGUI(self):
        self.recompile_answer()
        self.verify_player_position(4)
        for i in range(5):
            self.click_next()
            self.verify_player_position(5 + i)
        #   self.verify_player_is_in_camera_range()
        self.click_restart()
        self.verify_player_position(4)
        self.click_continue(True)
        self.verify_victory()

        import tests.lib_test as T
        T.info()

if __name__ == "__main__":
    pass
    # run_level(Level, __file__, level_type="text")
    # run_level(Level, __file__, level_type="arcade")
    # test_level(Level, __file__)
