/* @AGDB
 *
 * BUG: player doesn't get teleported when stepping on the teleport tile
 * Tag: 
 *
 * GUI : only 'next/step/edit' buttons + click-break
 *
 * level_title:   Teleportation
 * exec_name:    04_teleportation
 * level_number: 4
 * engine_name:  none
 *
 * available_commands: next step edit
 *
 * The player has to cross a river by means of two teleporters connected to each 
 * other. Walking over the first teleporter is sufficient to get teleported to the
 * second one.
 *
 * no_verify: change_in_function player_x forward
 * no_verify: change_in_function player_y forward
 * verify_init: cond player_x == 3
 * verify_init: cond player_y == 2
 * # verify_always: cond exit_x == 17
 * # verify_always: cond exit_y == 2
 * # verify_always: cond tp_in_x == 9
 * # verify_always: cond tp_in_y == 7
 * verify_always: cond tp_out_x == 15
 * verify_always: cond tp_out_y == 4
 * verify_always: cond river_x == 12
 * verify_always: cond river_width == 2
 * verify_always: must_call teleport
 * verify_always: must_call verify_exit
 *
 *
 *
 * arcade_maps: main ../tutorial.tmx
 * arcade_maps: main_offset 23 0
 *
 *
 *
 * WOP: messageFR congrats
 * Merveilleux, ça fonctionne !
 * EndOfMessage
 * WOP: messageEN congrats
 * Wonderful, it works!
 * EndOfMessage
 *
 *
 * WOP: messageFR warning
 * On dirait qui'il y a un problème avec le téléporteur...
 * EndOfMessage
 * WOP: messageEN warning
 * It seems there is a problem with the teleporter...
 * EndOfMessage
 *
 *
 * WOP: messageFR activate_tp
 * Tiens ! Qu'est-ce qui se passe ici ?
 * J'étais certain que le pont était encore là ce matin.
 * Bon... Il y a dû avoir une crue soudaine.
 *
 *
 * Laisse-moi t'aider à traverser la rivière.
 * Je vais créer deux téléporteurs,
 * il te suffit de marcher dessus pour être téléporté de l'autre côté !
 * EndOfMessage 
 * WOP: messageEN activate_tp
 * Huh? What happened here?
 * The bridge was still intact this morning, I could swear it!
 * It must have been a sudden flood.
 *
 *
 *
 * Let me help you cross the river.
 * I will create two teleporters,
 * just step on one and you'll be teleported to the other side!
 * EndOfMessage
 *
 *
 * OBJenemy: messageFR threat
 * ......... !
 *
 * Tu n'as rien à faire ici, repars d'où tu viens si tu tiens à ta vie.
 * EndOfMessage
 * OBJenemy: messageEN threat
 * ......... !
 *
 * You don't belong here, go back if you want to go on living.
 * EndOfMessage
 * 
 * OBJenemy: messageFR destroy
 * J'ai cassé le pont ! Tu ne pourras jamais passer !
 * EndOfMessage
 * OBJenemy: messageEN destroy
 * I broke the bridge! You'll never get across!
 * EndOfMessage
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

enum direction {
    UP,
    DOWN,
    LEFT,
    RIGHT
};

/* Variables pour le joueur */
int player_x=3, player_y=2;         /* coordonnées */
int player_direction = RIGHT;   /* direction du joueur */

// Coordonnées de la sortie
int exit_x = 17;
int exit_y = 2;

// Coordonnées du téléporteur 1
int tp_in_x = 9;
int tp_in_y = 7;

// Coordonnées du téléporteur 2
int tp_out_x = 15;
int tp_out_y = 4;

int river_x=12; /* Position de la rivière */
int river_width=2; /* Largeur de la rivière */

void message(char msg[])
{
    puts(msg);
    fflush(stdout);
}

int in_water(void)
{
    message ("Vous vous noyez.");
    return EXIT_FAILURE;
}

void teleport(void) {
#ifndef BUG
    player_x = tp_out_x;
    player_y = tp_out_y;
#else
    player_y, player_x = tp_out_y, tp_out_x;
#endif
}

void forward(void)
{
    if (player_direction == UP) {
        player_y--;
    } else if (player_direction == DOWN) {
        player_y++;
    } else if (player_direction == LEFT) {
        player_x--;
    } else if (player_direction == RIGHT) {
        player_x++;
    } else {
        fprintf(stderr, "Erreur: direction inconnue!\n");
    }

    if (player_x == tp_in_x && player_y == tp_in_y) {
        teleport();
        return;
    }

    if (player_x >= river_x && player_x < river_x + river_width) {
        int ret = in_water();
        exit(ret);
    }
}

void verify_exit(void)
{
    if (player_x != exit_x || player_y != exit_y) {
        printf("DEFAITE!\n");
        exit (EXIT_FAILURE);
    } else {
        printf("VICTOIRE!\n");
        exit (EXIT_SUCCESS);
    }
}

void turn_left (void)
{
    if (player_direction == UP) {
        player_direction = LEFT;
    } else if (player_direction == DOWN) {
        player_direction = RIGHT;
    } else if (player_direction == LEFT) {
        player_direction = DOWN;
    } else if (player_direction == RIGHT) {
        player_direction = UP;
    } else {
        fprintf(stderr, "Erreur: direction inconnue!\n");
    }
}

void turn_right (void)
{
    if (player_direction == UP) {
        player_direction = RIGHT;
    } else if (player_direction == DOWN) {
        player_direction = LEFT;
    } else if (player_direction == LEFT) {
        player_direction = UP;
    } else if (player_direction == RIGHT) {
        player_direction = DOWN;
    } else {
        fprintf(stderr, "Erreur: direction inconnue!\n");
    }
}

int main(void) {
    forward();
#ifndef BUG
    forward();
    forward();
    forward();
    forward();
    forward();
    turn_right();
    forward();
    forward();
    forward();
    forward();
    forward();
    turn_left();
    forward();
    forward();
    turn_left();
    forward();
    forward();
#else
    forward();
    forward();
    forward();
    forward();
    forward();
    forward();
    forward();
    forward();
    forward();
    forward();
    forward();
    forward();
    forward();
    forward();
#endif
    verify_exit();
}
