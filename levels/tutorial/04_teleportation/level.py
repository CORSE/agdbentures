#!/usr/bin/env python3

from level.level_abc import AbstractLevel
from level.action import Action
import arcade
import graphic.constants as cst
from logs import lvl, log

class Level(AbstractLevel):
    """04_teleportation"""
    def arcade_custom_restart(self):
        self.restart = True

        self.enemy.visible = False
        self.enemy_spawned = False

        self.thunder.visible = False
        self.thunder.send_update()
        # During a restart, if the bridge has been destroyed, we destroy it again
        if self.bridge_destroyed:
            self.destroy_bridge()

        # If the wop has been spawned, we check if it has finished its actions
        if self.wop_spawned:
            # If it has finished, we activate the tp
            if self.wop_finished:
                self.activate_tp()

            # If it has not finished, we reset it and spawn it again
            else:
                payload = {
                    "topic": "wop_update",
                    "action": Action("set_reset"),
                }
                self.send_to_gui(payload)
                self.spawn_wop()

        # If the wop has not been spawned, we reset it to clear its actions
        else:
            payload = {
                "topic": "wop_update",
                "action": Action("set_reset"),
            }
            self.send_to_gui(payload)

    def destroy_bridge(self):
        # To destroy the bridge, we hide the sprites that compose it
        if not self.restart:
            action_thunder = Action("play_animation", [self.actions["enemy/talk/0"]])
            payload_thunder = {
                "topic": "object_update",
                "object_name": "thunderstrike",
                "action": action_thunder
            }

            self.thunder.send_update(payload_thunder)

            first_action_fire = None
            for n in range(4):
                action_fire = Action("play_animation", [action_thunder])
                if not first_action_fire:
                    first_action_fire = action_fire
                payload_fire = {
                    "topic": "object_update",
                    "object_name": f"fire{n}",
                    "action": action_fire,
                    "delay": n * 0.45,
                    "times": 3 - (n//2)
                }

                self.fires[n].send_update(payload_fire)

        self.actions['break_bridge'] = Action("hide", [first_action_fire] if not self.restart else [])
        payload_bridge = {
            "topic": "sprites",
            "action": self.actions['break_bridge'],
            "layer": "decorations_top",
            "locations": [(12,1), (13,1), (12,2), (13,2), (12,3), (13,3)],
        }

        self.send_to_gui(payload_bridge)

    def activate_tp(self):

        pl = {
            "topic": "camera",
            "action": Action("set_target",[self.actions["wop/talk/0"]]),
            "target": "player"
        }
        self.send_to_gui(pl)

        # Pillars sprites are created and placed on the tp locations
        payload = {
            "topic": "sprites",
            "action": Action("create", [self.actions["wop/talk/0"]] if not self.restart else []),
            "type": "teleport_in",
            "x": self.tp_in.coord_x,
            "y": self.tp_in.coord_y,
        }
        self.send_to_gui(payload)

        payload = {
            "topic": "sprites",
            "action": Action("create", [self.actions["wop/talk/0"]] if not self.restart else []),
            "type": "teleport_out",
            "x": self.tp_out.coord_x,
            "y": self.tp_out.coord_y,
        }
        self.send_to_gui(payload)

    def spawn_enemy(self):
        enemy = self.world.get_object("enemy")
        self.enemy_spawned = True

        enemy_init = self.world.get_coordinates("enemy_init")
        enemy_target = self.world.get_coordinates("enemy_target")

        # Spawn enemy
        enemy.place_on(enemy_init)
        enemy.visible = True

        pl = {
            "topic": "camera",
            "action": Action("set_target"),
            "target": "enemy"
        }
        self.send_to_gui(pl)

        for key, val in [('change/speed', 0.8), ('change/far_move', 'pathfind')]:
            pl = enemy.payload()
            pl[key] = val
            enemy.send_update(pl)

        pl = enemy.payload()
        self.actions["enemy/move/0"] = Action("move")
        pl["action"] = self.actions["enemy/move/0"]
        enemy.send_update(pl)

        # Move enemy
        enemy.place_on(enemy_target)
        self.actions["enemy/move/1"] = Action("move")
        pl = enemy.payload()
        pl["action"] = self.actions["enemy/move/1"]
        enemy.send_update(pl)

        enemy.direction = cst.Direction.LEFT
        enemy.send_update()

        # Finally register a talk for when it has finished walking
        self.actions["enemy/talk/0"] = Action("talk")
        payload = {
            "topic": "object_update",
            "object_name": "enemy",
            "action": self.actions["enemy/talk/0"],
            "message": enemy.message('threat'),
        }
        enemy.send_update(payload)

        self.destroy_bridge()

        # Register a new talk for when it has finished talking
        self.actions["enemy/talk/1"] = Action("talk", [self.actions['break_bridge']])
        payload = {
            "topic": "object_update",
            "object_name": "enemy",
            "action": self.actions["enemy/talk/1"],
            "message": enemy.message('destroy'),
        }
        enemy.send_update(payload)

        # The enemy goes back to its initial position
        enemy.direction = cst.Direction.DOWN
        enemy.send_update()

        enemy.place_on(enemy_init)
        pl = enemy.payload()
        self.actions["enemy/move/2"] = Action("move")
        pl["action"] = self.actions["enemy/move/2"]
        enemy.send_update(pl)

        enemy.visible = False
        enemy.send_update()

        self.spawn_wop()

    def spawn_wop(self):
        wop = self.wop
        wop_init = self.world.get_coordinates("wop_tp_init")
        wop_tp = self.world.get_coordinates("wop_tp")

        # Spawn wop
        pl = wop.payload()
        pl["change/speed"] = 0.8
        wop.send_update(pl)

        wop.visible = True
        wop.place_on(wop_init)
        pl = wop.payload()
        pl["teleport"] = True
        pl["action"] = Action("move", [self.actions["enemy/move/2"]] if not self.restart else [])
        wop.send_update(pl)

        pl = {
            "topic": "camera",
            "action": Action("set_target",[self.actions["enemy/move/2"]]),
            "target": "wop"
        }
        self.send_to_gui(pl)

        # Move wop to wop_tp
        wop.place_on(wop_tp)
        pl = wop.payload()
        pl["action"] = Action("move")
        wop.send_update(pl)

        # Finally register a talk for when it has finished walking
        self.actions["wop/talk/0"] = Action("talk")
        payload = {
            "topic": "wop_update",
            "action": self.actions["wop/talk/0"],
            "message": wop.message('activate_tp'),
        }
        wop.send_update(payload)

        # Activate the tp when the wop has finished talking
        self.activate_tp()

    def arcade_custom_first_start(self):
        self.restart = False

        tp_in = self.world.get_object("tp_in")
        tp_out = self.world.get_object("tp_out")
        self.wop = self.world.get_wop()
        wop = self.wop
        self.enemy = self.world.get_object("enemy")
        self.enemy.visible = False
        self.thunder = self.world.get_object("thunderstrike")
        self.thunder.visible = False
        self.thunder.send_update()

        self.fires = [self.world.get_object(f"fire{n}") for n in range(4)]
        for n in range(4):
            self.fires[n].visible = False
            self.fires[n].send_update()

        wop_congrats = self.world.get_coordinates("wop_congrats")

        self.bridge_destroyed = False # If the bridge has been destroyed
        self.wop_spawned = False # If the wop has been spawned
        self.enemy_spawned = False # If the enemy has been spawned
        self.wop_finished = False # When the wop has finished its actions
        self.wop_congrats = False # When the wop has congratulated the player after successful tp
        self.tp_in = tp_in # tp_in object
        self.tp_out = tp_out # tp_out object
        self.actions = {} # Actions that will be used to trigger events

        def player_custom_update(player, *args):
            if "enemy/talk/1" in self.actions and self.actions["enemy/talk/1"].is_done():
                if not self.bridge_destroyed:
                    self.bridge_destroyed = True
                if not self.wop_spawned:
                    self.wop_spawned = True

            if "wop/talk/0" in self.actions and self.actions["wop/talk/0"].is_done():
                if not self.wop_finished:
                    self.wop_finished = True

            if player.coord_x == 5 and not self.bridge_destroyed and not self.enemy_spawned:
                self.spawn_enemy()

            if not self.wop_congrats and player.is_on(wop_congrats):
                self.wop_congrats = True
                wop.talks('congrats')

        self.player.post_update = player_custom_update

        def drown_player():
            payload = {
                "topic": "player_update",
                "action": Action("drown")
            }
            self.player.send_update(payload)

        def hide_player():
            self.player.visible = False
            pl = self.player.payload()
            self.player.send_update(pl)

        def visible_player():
            self.player.visible = True
            pl = self.player.payload()
            self.player.send_update(pl)

        def catch_preteleportation():
            lvl.debug(f"Teleport called, will teleport the player")
            # first, finish movement
            self.update_all()
            self.player.send_update()

            # make the character turn on itself
            pl = {
                "topic": "player_update",
                "action": Action("turn"),
                "num": 8,
            }
            self.send_to_gui(pl)
            # hide when moving to the other location
            hide_player()

        def catch_teleportation():
            self.update_all()
            lvl.debug(f"Teleporting player {self.player}")
            self.actions["player/turn/0"] = Action("turn")
            pl = {
                "topic": "player_update",
                "action": self.actions["player/turn/0"],
                "num": 8,
            }
            self.send_to_gui(pl)
            visible_player()

            if self.player.coord_x != tp_out.coord_x or self.player.coord_y != tp_out.coord_y:
                payload = {
                    "topic": "wop_update",
                    "action": Action("talk", [self.actions["player/turn/0"]]),
                    "message": wop.message('warning'),
                }
                wop.send_update(payload)

        self.register_breakpoint("teleport", catch_preteleportation)
        self.register_leave_function("teleport", catch_teleportation)
        self.register_breakpoint("in_water", drown_player)

    def check_on_tp_in(self):
        """checks if the player has stepped on the first teleporter"""
        chk = self.checker.tracker
        plx = chk.get_variable_value_as_str('player_x', "int")
        ply = chk.get_variable_value_as_str('player_y', "int")
        tp_in_x = chk.get_variable_value_as_str('tp_in_x', "int")
        tp_in_y = chk.get_variable_value_as_str('tp_in_y', "int")

        lvl.debug(f"Checking if player really is on TP_in {plx, ply} vs {tp_in_x, tp_in_y}")
        if plx == tp_in_x and ply == tp_in_y:
            self.checker.is_on_tp_in = True

    def check_on_tp_out(self):
        """checks if the player is indeed teleported to the second teleporter"""
        chk = self.checker.tracker
        plx = chk.get_variable_value_as_str('player_x', "int")
        ply = chk.get_variable_value_as_str('player_y', "int")
        tp_out_x = chk.get_variable_value_as_str('tp_out_x', "int")
        tp_out_y = chk.get_variable_value_as_str('tp_out_y', "int")

        lvl.debug(f"Checking if player really is teleported to tp_out {plx, ply} vs {tp_out_x, tp_out_y}")
        if plx == tp_out_x and ply == tp_out_y:
            self.checker.is_on_tp_out = True
            self.checker.has_been_teleported = True


    def pre_validation(self):
        self.checker.has_been_teleported = False
        self.checker.is_on_tp_in = False
        self.checker.is_on_tp_out = False
        self.checker.register_breakpoint('teleport', self.check_on_tp_in)
        self.checker.register_leave_function('teleport', self.check_on_tp_out)


    def post_validation(self):
        if not self.checker.has_been_teleported:
           self.checker.failed("Player hasn't been teleported")
        if not self.checker.is_on_tp_in:
            self.checker.failed("Player is not on TP_in")
        if not self.checker.is_on_tp_out:
            self.checker.failed("Player is not on TP_out")

    def test(self):
        import tests.lib_test as T

        ## There was an old bug, there, which seems to have disappeared now,
        ## probably because we start a new tracker at each compilation now.
        # Bug segfault after restart with bug...
        # Still occurs when doing this sequence manually
        self.recompile_answer()
        self.run()
        self.recompile_bug()
        T.cnext()
        T.unexpect_error()
        self.run()

        # Bug player_y is 0 instead of 1 after teleport
        # when doing this sequence manually
        self.recompile_bug()
        for _ in range(9):
            T.cnext()
        T.expect_defeat()
        self.run()

        self.recompile_answer()
        # self.restart()
        for _ in range(11):
            T.cnext()

        # check teleport
        T.variable("player_x", 9)
        T.variable("player_y", 6)
        T.cnext()
        T.variable("player_x", 15)
        T.variable("player_y", 4)

        for _ in range(7):
            T.cnext()
        T.expect_victory()
        self.run()

    def testGUI(self):
        self.verify_player_position(3, 2)
        self.verify_named_object_position("tp_in", 9, 7)
        self.verify_named_object_position("tp_out", 15, 4)
        self.verify_named_object_position("wop", 0, 0)
        for i in range(8):
            self.click_next()
            self.verify_player_position(4+i, 2)
        self.verify_named_object_position("wop", 17, 1)
        self.click_next(True)
        self.verify_defeat()

        self.recompile_answer()
        for i in range(6):
            self.click_next()
            self.verify_player_position(4+i, 2, cst.Direction.RIGHT)
        self.click_next()
        self.verify_player_position(9, 2, cst.Direction.DOWN)
        for i in range(4):
            self.click_next()
            self.verify_player_position(9, 3+i, cst.Direction.DOWN)
        self.click_next()
        self.verify_player_position(15, 4, cst.Direction.DOWN)
        self.click_next()
        self.verify_player_position(15, 4, cst.Direction.RIGHT)
        for i in range(2):
            self.click_next()
            self.verify_player_position(16+i, 4, cst.Direction.RIGHT)
        self.click_next()
        self.verify_player_position(17, 4, cst.Direction.UP)
        for i in range(2):
            self.click_next()
            self.verify_player_position(17, 3-i, cst.Direction.UP)
        self.click_next(True)
        self.verify_victory()
        import tests.lib_test as T
        T.info()

if __name__ == "__main__":
    test_level(Level, __file__)
