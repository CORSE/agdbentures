#!/usr/bin/env python3

from graphic.constants import Direction
from level.level_abc import AbstractLevel
from level.action import Action


class Level(AbstractLevel):
    """03_second_bug"""

    # TODO: do we need this mechanism? maybe for testing?
    # if so, rename back to `setup` and fix the map mechanism accordingly in
    # level/map.py
    def TODO_custom_setup(self):
        """Set a custom map."""
        str_map = [
            "~~~~~~~~~~~~",
            "~~~~~~~~~~~~",
            "~~~~~~~~~~~~",
            "~~~~~~      ",
            "~~~~~~ ~~~~~",
            "       ~~~~~",
            "~~~~~~~~~~~~",
            "~~~~~~~~~~~~",
            "~~~~~~~~~~~~",
        ]
        self.map.update_floor(str_map)


    def arcade_custom_first_start(self):

        player = self.world.get_player()
        trap = self.world.get_object("trap")
        water_trap = self.world.get_object("water_trap")

        wod = self.world.get_wop()
        wod.visible = True
        wod_init = self.world.get_coordinates("wop_traps")
        wod.place_on(wod_init)
        wod.send_update()

        def player_custom_update(slf, *args):
            if player.is_below(wod):
                wod.talks("intro")

            for tr in [trap, water_trap]:
                if tr.coord_x == player.coord_x and tr.coord_y == player.coord_y:
                    payload = {
                        "topic": "object_update",
                        "object_name": tr.name,
                        "action": Action("disappear")
                    }
                    slf.send_update(payload)
                    payload = {
                        "topic": "player_update",
                        "action": Action("fall") if tr is trap else Action("drown")
                    }
                    slf.send_update(payload)

        player.post_update = player_custom_update





    def test(self):
        import tests.lib_test as T

        def scenario():
            for _ in range(12):
                T.cnext()

        self.test_scenario(scenario)


    def testGUI(self):
        self.recompile_bug()
        self.verify_named_object_position("wop", 5, 3)
        self.verify_player_position(4, 4, Direction.RIGHT)
        for i in range(4):
            self.click_next()
            self.verify_player_position(5+i, 4, Direction.RIGHT)
        for i in range(3):
            self.click_next()
            self.verify_player_position(8, 4+i, Direction.UP)
        for i in range(4):
            self.click_next()
            self.verify_player_position(8-i, 6, Direction.LEFT)
        self.click_next(True)
        self.verify_defeat()

        self.recompile_answer()
        self.verify_named_object_position("wop", 5, 3)
        self.verify_player_position(4, 4, Direction.RIGHT)
        for i in range(4):
            self.click_next()
            self.verify_player_position(5+i, 4, Direction.RIGHT)
        for i in range(3):
            self.click_next()
            self.verify_player_position(8, 4-i, Direction.UP)
        for i in range(4):
            self.click_next()
            self.verify_player_position(8+i, 2, Direction.RIGHT)
        self.click_next(True)
        self.verify_victory()
        import tests.lib_test as T
        T.info()

if __name__ == "__main__":
    # run_level(Level, __file__, level_type="text")
    test_level(Level, __file__)
