/* @AGDB
 *
 * First program with a real bug.
 *
 * GUI only 'next/step/edit' buttons
 *
 * level_title:   0-3 Bugs Hiding
 * exec_name:    03_bugs_hiding
 * engine_name:  none
 *
 * available_commands: next step edit
 *
 * arcade_maps: main ../tutorial.tmx
 * arcade_maps: main_offset 15 0
 *
 * Map goal
 *
 * verify_init: cond player_x == 4
 * verify_init: cond player_y == 4
 * verify_always: cond exit_x == 11
 * verify_always: cond exit_y == 2
 *
 *
 * Trap, already placed on the tmx map
 * The bush disappears when player is on top of it, making player fall in 
 * a hole.
 *
 * objects: trap water_trap
 *
 * coordinates: wop_traps
 *
 * Wise person
 * WOP: message intro
 * Rapidement, j'ai eu envie que mes personnages puissent aussi se déplacer 
 * suivant y et pas seulement x.
 *
 *
 * J'avais pensé un système basique avec une direction et des fonctions pour 
 * tourner le joueur à gauche ou à droite.
 *
 *
 *
 * Évidemment, c'est assez limité comme système, mais ça fonctionnait pas si 
 * mal à l'époque. 
 *
 *
 * Mis à part que je ne savais pas encore comment factoriser du code 
 * : résultat, beaucoup de copié-collé et des bugs en perspective...
 * EndOfMessage
 *
 *
 */
#include <stdlib.h>
#include <stdio.h>

enum direction {
    UP,
    DOWN,
    LEFT,
    RIGHT
};


/* Position du joueur */
int player_x = 4, player_y = 4;
int player_direction = RIGHT;   /* direction pour le deplacement */

/* Coordonnees de la sortie */
int exit_x = 11;
int exit_y = 2;


void turn_left (void)
{
    if (player_direction == UP) {
        player_direction = LEFT;
    } else if (player_direction == DOWN) {
        player_direction = RIGHT;
    } else if (player_direction == LEFT) {
        player_direction = DOWN;
    } else if (player_direction == RIGHT) {
        player_direction = UP;
    } else {
        fprintf(stderr, "Error: unknown direction!\n");
    }
}

void turn_right (void)
{
    if (player_direction == UP) {
#ifdef BUG
        player_direction = LEFT;
#else
        player_direction = RIGHT;
#endif
    } else if (player_direction == DOWN) {
#ifdef BUG
        player_direction = RIGHT;
#else
        player_direction = LEFT;
#endif
    } else if (player_direction == LEFT) {
        player_direction = UP;
    } else if (player_direction == RIGHT) {
        player_direction = DOWN;
    } else {
        fprintf(stderr, "Erreur: direction inconnue!\n");
    }
}


void forward(void)
{
    if (player_direction == UP) {
#ifdef BUG
        player_y++;
#else
        player_y--;
#endif
    } else if (player_direction == DOWN) {
#ifdef BUG
        player_y--;
#else
        player_y++;
#endif
    } else if (player_direction == LEFT) {
        player_x--;
    } else if (player_direction == RIGHT) {
        player_x++;
    } else {
        fprintf(stderr, "Erreur: direction inconnue!\n");
    }
}


void verify_exit(void)
{
    if (player_x != exit_x || player_y != exit_y) {
        printf("DEFAITE!\n");
        exit (EXIT_FAILURE);
    } else {
        printf("VICTOIRE!\n");
        exit (EXIT_SUCCESS);
    }
}

int main(void)
{
    forward();
    forward();
    forward();
    forward();
    turn_left();
    forward();
    forward();
    turn_right();
    forward();
    forward();
    forward();

    verify_exit();
}
