/* @AGDB
 *
 * BUG: cannot pick up the key on the path
 * Tag:
 *
 * GUI: only 'next/step/edit' buttons + click-break
 *
 * level_title:   0-6 Key
 * exec_name:    06_key
 * level_number: 6
 * engine_name:  none
 *
 * available_commands: next step edit continue
 * magic_break: True
 *
 * A vertical wall bars the passage to the exit. There is one locked door on
 * the wall. A keys are generated at a random position on the same line.
 * Just walking over the key should be sufficient to pick it up and open the
 * door.
 *
 * verify_init: cond player_x == 3
 * verify_init: cond player_y == 2
 * verify_always: cond exit_x == 28
 *
 *
 *
 * arcade_maps: main ../tutorial.tmx
 * arcade_maps: main_offset 65 0
 *
 * coordinates: wop_key
 *
 * WOP: message intro
 * Bravo tu arrives au dernier niveau de ce tutoriel.
 *
 * Pour l'occasion, je t'ai concocté un coctail de mes premiers bugs les plus 
 * "amusants" :-)
 * 
 *
 * Pour en venir à bout, utilise tout ce que je t'ai offert jusqu'à présent :
 * next, step, continue, print... et les arrêts magiques !
 * EndOfMessage
 *

 *
 * OBJpanel_castle: strname panneau
 * OBJpanel_castle: message intro
 * JOURNÉE PORTES OUVERTES
 *
 *
 * VENEZ VISITER LE CHATEAU
 *
 *
 * Merci de suivre les barrières de sécurité.
 *
 *
 *
 * PS : nous avons égaré les clefs, si vous les
 * retrouvez, merci de nous les rapporter.
 * EndOfMessage
 *
 */

#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <assert.h>
#include <time.h>

enum direction {
    UP,
    DOWN,
    LEFT,
    RIGHT
};

/* Variables pour le joueur */
int player_x=3, player_y=2;         /* coordonnées */
int player_direction = RIGHT;   /* direction du joueur */
bool has_key = false;   /* Est ce qu'on possede la clef ? */

/* Coordonnees de la sortie */
int exit_x=28;
int exit_y= 2;

/* Position du mur horizontal */
int wall_x=23; /* Le mur commence à cette position x jusqu'au bout de la carte */
int wall_y=6;
int wall_width=2;  /* Épaisseur du mur */
int door_x=28;     /* Partie gauche de la porte */
int door_width=2;  /* Largeur de porte (donc 2 portes) */

/* Coordonnées pour la clef */
int key_x = -1;     /* la clef n'est pas présente initialement */
int key_y = -1;


void message(char msg[])
{
    puts(msg);
    fflush(stdout);
}

void turn_left (void)
{
    if (player_direction == UP) {
        player_direction = LEFT;
    } else if (player_direction == DOWN) {
        player_direction = RIGHT;
    } else if (player_direction == LEFT) {
        player_direction = DOWN;
    } else if (player_direction == RIGHT) {
        player_direction = UP;
    } else {
        fprintf(stderr, "Erreur: direction inconnue!\n");
    }
}

void turn_right (void)
{
    if (player_direction == UP) {
        player_direction = RIGHT;
    } else if (player_direction == DOWN) {
        player_direction = LEFT;
    } else if (player_direction == LEFT) {
        player_direction = UP;
    } else if (player_direction == RIGHT) {
        player_direction = DOWN;
    } else {
        fprintf(stderr, "Erreur: direction inconnue!\n");
    }
}

void turn_around(void)
{
    if (player_direction == UP) {
        player_direction = DOWN;
    } else if (player_direction == DOWN) {
        player_direction = UP;
    } else if (player_direction == LEFT) {
        player_direction = RIGHT;
    } else if (player_direction == RIGHT) {
        player_direction = LEFT;
    } else {
        fprintf(stderr, "Erreur: direction inconnue!\n");
    }
}

bool try_open_door(void)
{
#ifdef FIX_MAGIC_KEY
    message ("MAGIC KEY!!!");
    return true;
#endif
    // On regarde si on possede la clef avec la variable globale
    if (!has_key) {
        message ("Vous n'avez pas la clef !");
        return false;
    } else {
        message ("Vous ouvrez la porte avec votre clef.");
        return true;
    }
}

/**
 * Verifie s'il y a une clef a la position du joueur.
   La recupere si cest le cas.
 */
void check_key(void)
{
    if (player_x == key_x && player_y == key_y) {
        message("Vous avez trouve une clef !");
        /* @AGDB
        * BUGREMOVE
        * forgot to change the boolean value
	*/
#ifndef BUG
        has_key = true;
#endif
        key_x = -1; /* Faire disparaitre la clef */
        key_y = -1;
    }
}


/* Renvoie 'true' si le joueur est devant le mur */
bool near_wall(void)
{
    if (player_y-1 == wall_y) {
        return true;
    } else {
        return false;
    }
}

void forward(void)
{
    if (player_direction == UP) {
        if (near_wall()) {
            if (player_x < door_x || player_x > door_x + door_width -1) {
                message ("Vous vous cognez contre un mur.");
            } else {
                if (try_open_door()) {
                    player_y--;
                }
            }
        } else {
            player_y--;
        }
    } else if (player_direction == DOWN) {
        player_y++;
    } else if (player_direction == LEFT) {
        player_x--;
    } else if (player_direction == RIGHT) {
        player_x++;
    } else {
        fprintf(stderr, "Erreur: direction inconnue!\n");
    }
    check_key();
}

void forward_n(int steps)
{
    for (int i=0; i<steps; i++) {
        forward();
    }
}

/**
 * Génère une position aléatoire pour la clef perdue.
 */
void key_random_position(void)
{
    srand(time(NULL)); /* Graine pour le générateur de nombres aléatoires */;

    key_x = 9+rand() % 13;  /* Position x : entre 9 et 21 */
    key_y = 2*(rand() % 5); /* Position y : nombre pair entre 0 et 8 */

}

void verify_exit(void)
{
    if (player_x != exit_x || player_y != exit_y) {
        printf("DEFAITE!\n");
        exit (EXIT_FAILURE);
    } else {
        printf("VICTOIRE!\n");
        exit (EXIT_SUCCESS);
    }
}

int main(void)
{
    key_random_position(); /*FR Choisit une position aleatoire pour la clef *//*EN Choose a random plocation for the key */

#ifdef FIX_NO_DOOR
    forward_n(exit_x - player_x);
    verify_exit();
#endif

#ifdef FIX_NO_DOOR_PRETEND
    forward_n(exit_x - player_x);
    try_open_door();
    verify_exit();
#endif

#ifdef FIX_HAS_KEY
    has_key=true;
    turn_right();
    forward_n(6);
    turn_left();
    forward_n(exit_x - player_x);
    turn_left();
    forward_n(player_y - exit_y);

    verify_exit();
#endif
#ifdef FIX_MAGIC_KEY
    turn_right();
    forward_n(6);
    turn_left();
    forward_n(exit_x - player_x);
    turn_left();
    forward_n(player_y - exit_y);
    verify_exit();
#endif


    forward();
    forward();
    forward();
    forward();
    turn_left();
    forward();
    forward();
    turn_right();
    forward();
    forward();

    bool to_the_right = true;
    int len_path = 12;

    for (int i=0; i<5; i++) {
        forward_n(len_path);
#ifndef BUG
        if (i==4) break; // sortir de la boucle !
#endif
        if (to_the_right) {
            turn_right();
            forward_n(2);
            turn_right();
        } else {
            turn_left();
            forward_n(2);
#ifdef BUG
            turn_right();
#else
            turn_left();
#endif
        }
        to_the_right=!to_the_right;
    }
#ifdef BUG
    forward_n(4);
#else
    forward_n(7);
#endif
    turn_left();
    forward_n(6);

    verify_exit();
}
