#!/usr/bin/env python3

from level.level_abc import AbstractLevel
from level.action import Action
from logs import lvl


class Level(AbstractLevel):
    """06_key"""

    def arcade_custom_first_start(self):

        player = self.world.get_player()
        panel = self.world.get_object("panel_castle")
        wop = self.world.get_wop()
        wop.visible = True

        wop_init = self.world.get_coordinates("wop_key")
        wop.place_on(wop_init)
        wop.send_update()

        def catch_open_door():
            self.update_all()  # update player position

            lvl.debug(f"In try open door with player {player}")
            globs = self.tracker.get_program_memory(as_raw_python_objects=True)[
                "global_variables"
            ]
            key = globs["has_key"].value

            if key:
                payload = {
                    "topic": "sprites",
                    "action": Action("hide"),
                    "layer": "walls",
                    "locations": [
                        (player.coord_x, player.coord_y - 1),
                        (player.coord_x, player.coord_y - 2),
                    ],
                }
                self.send_to_gui(payload)

        self.register_breakpoint(
            function_name="try_open_door", callback=catch_open_door
        )

        def player_custom_update(player, *args):
            if player.is_below(wop_init):
                wop.talks("intro")

            if player.is_below(panel):
                pl = panel.payload()
                pl["action"] = Action('talk')
                pl["message"] = panel.message('intro')
                panel.send_update(pl)

        player.post_update = player_custom_update

    def check_on_key(self):
        chk = self.checker.tracker
        plx = chk.get_variable_value_as_str('player_x', "int")
        ply = chk.get_variable_value_as_str('player_y', "int")
        keyx = chk.get_variable_value_as_str('key_x', "int")
        keyy = chk.get_variable_value_as_str('key_y', "int")

        lvl.debug(f"Checking if player really is on key {plx, ply} vs {keyx, keyy}")
        if plx != keyx or ply != keyy:
            self.checker.failed("Player is not on key when has_key changes")

    def check_open_door(self):
        chk = self.checker.tracker
        plx = chk.get_variable_value_as_str('player_x', "int")
        ply = chk.get_variable_value_as_str('player_y', "int")
        doorx = chk.get_variable_value_as_str('door_x', "int")
        doory = chk.get_variable_value_as_str('wall_y', "int")
        lvl.debug(f"Checking if player really is on door {plx, ply} vs {doorx, doory}")
        if plx != doorx and plx != doorx + 1 or ply != doory and ply != doory + 1:
            self.checker.failed("Player is not on door when opening it")

        has_key = chk.get_variable_value_as_str('has_key', "int")
        if not has_key:
            self.checker.failed("Player opens the door but does not have the key")
        else:
            self.checker.has_opened_door = True

    def pre_validation(self):
        # We need to check that has_key is changed to true only
        # if player is on key, and that player passes through the door
        self.checker.has_opened_door = False
        self.checker.register_watch('has_key', self.check_on_key)
        self.checker.register_breakpoint('try_open_door', self.check_open_door)
        pass

    def post_validation(self):
        if not self.checker.has_opened_door:
            self.checker.failed("Player has not opened the door")
        pass

    def custom_setup(self):
        """Set a custom map."""
        pass
        ## TODO: is that useful??
        # str_map = (
        # "                     |    "
        # "                     |    "
        # "                     |    "
        # "                     |    "
        # "                     |    "
        # )
        # char_map = [tile for tile in str_map]
        # self.map.update_floor(char_map)

    def test(self):
        import tests.lib_test as T

        self.recompile_bug()
        T.ccontinue()
        T.expect_defeat()
        self.run()

        self.recompile_answer()
        T.ccontinue()
        T.expect_victory()
        self.run()

    def testGUI(self):
        import tests.lib_test as T 
        self.recompile_bug()
        self.verify_named_object_position("key", -1, -1)
        self.click_next()
        self.verify_named_object_position("key", -1, -1, None, True)
        self.click_continue(True)
        self.verify_defeat()

        self.recompile_answer()
        self.click_continue(True)
        self.verify_victory()
        T.info()
        

