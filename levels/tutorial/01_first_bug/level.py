#!/usr/bin/env python3

from threading import Thread
from level.level_abc import AbstractLevel
from level.action import Action
from config import Config
from logs import lvl


class Level(AbstractLevel):
    """01_first_bug"""

    def arcade_custom_first_start(self):
        self.arcade_custom_restart()

    def arcade_custom_restart(self):
        wod = self.world.get_wop()

        if self.start_number == 1:

            entry = self.world.get_coordinates("wop_start")
            goto = self.world.get_coordinates("wop_target")

            wod.place_on(entry)
            wod.visible = True
            pl = wod.payload()
            pl["teleport"] = True
            wod.send_update(pl)

            # Then move wod to player
            wod.place_on(goto)
            wod.send_update(wod.payload())

            # Finally register a talk for when it has finished walking
            pl = wod.payload()
            pl["action"] = Action("talk")
            pl["message"] = wod.message('intro')
            wod.send_update(pl)

            # Then move wod to player
            wod.place_at(wod.coord_x+3, wod.coord_y)
            wod.send_update()



        if self.start_number == 2:
            pl = {
                "topic": "gui_change",
                "action": "unhide_button",
                "command": "restart",
            }
            wod.send_update(pl)

            # if self.checker is None:
            wod.talks("second")

            pl = {
                "topic": "gui_change",
                "action": "show_button",
                "command": "edit",
            }
            wod.send_update(pl)

        elif self.start_number == 3:
            wod.talks("PS")
        else:
            pass


    def check(self):
        # Nothing particular to verify here.
        # Regular checks of player_x and exit_x are configured in the main.c file.
        pass

    def test(self):
        import tests.lib_test as T

        # T.cnext()
        # TODO: cannot check wop messages anymore
        # is there a way around?
        T.warning("TODO: cannot check wop messages anymore, find a work-around?")
        # T.expect_agdb_sub_strings("edit the code")

        self.recompile_bug()
        for i in range(4):
            T.cnext()
            T.variable("player_x", 5 + i)

        for i in range(2):
            T.cnext()

        # T.expect_victory()
        T.expect_sub_strings("DEFAITE")
        T.cnext()
        T.expect_defeat()
        self.run()
        self.check_validation()

        self.recompile_answer()
        for i in range(4):
            T.cnext()
            T.variable("player_x", 5 + i)

        for i in range(3):
            T.cnext()
        T.expect_sub_strings("VICTOIRE")
        T.cnext()
        T.expect_victory()
        self.run()
        self.check_validation()

        self.recompile_fix("player_start")
        for _ in range(7):
            T.cnext()
        T.expect_victory()
        self.run()
        self.check_validation(
            should_validate=False,
            substr_reasons="player_x must be equal to 4"
        )

        self.recompile_fix("move_exit")
        for _ in range(7):
            T.cnext()
        T.expect_victory()
        self.run()
        self.check_validation(
            should_validate=False,
            substr_reasons="exit_x must be equal to 9"
        )

    def testGUI(self):
        self.recompile_bug()
        self.verify_named_object_position("wop", 8, 3)
        self.verify_player_position(4)
        for i in range(4):
            self.click_next()
            self.verify_player_position(5+i)
        for i in range(2):
            self.click_next()
        self.verify_named_object_position("wop", 8, 3)

        self.click_next(True)

        self.verify_defeat()
        self.recompile_answer()
        self.verify_player_position(4)
        for i in range(5):
            self.click_next()
            self.verify_player_position(5+i)
        for i in range(2):
            self.click_next()
        self.click_next(True)
        self.verify_victory()
        import tests.lib_test as T
        T.info()
        
if __name__ == "__main__":
    # run_level(Level, __file__, level_type="text")
    # run_level(Level, __file__, level_type="arcade")
    test_level(Level, __file__)
