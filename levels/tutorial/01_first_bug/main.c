/* @AGDB
 *
 * Second level after entering the game.
 *
 * GUI only 'next' and 'edit' buttons.
 *
 * The WOP encourages the player to check what happens when there is a bug.
 * When the level fails, restart and the WOP now encourages the player to
 * 'cheat' by e.g., modifying the exit position.
 * => still fails with message warning the user not to bypass bugs.
 * => restarts with WOP explaining the philosophy of agdbentures: teaching to
 * debug and that trying to bypass bugs will lessen the learning.
 * => player must revert back to original program (special button?) to finish
 * the level.
 *
 *
 * level_title:   0-1 First Bug
 * exec_name:    01_first_bug
 * level_number: 1
 * engine_name:  none
 *
 * available_commands: next
 * hide_commands: restart
 * new_commands_during_level: edit
 *
 *
 * arcade_maps: main ../tutorial.tmx
 * arcade_maps: main_offset 5 0
 **/
#include <stdlib.h>
#include <stdio.h>

/** @AGDB
 * Player status
 * player_y: 4
 *
 * Map goal
 * exit_y: 4
 *
 * Verifications for level validation
 * verify_init: cond player_x == 4
 * verify_always: cond exit_x == 9
 * no_verify: change_in_function player_x forward
 * no_verify: change_in_function player_y forward
 * no_verify: must_call verify_exit
 * no_verify_exit: cond player_y == exit_y
 *
 * coordinates to load from tmx
 * coordinates: wop_enter wop_start wop_target
 *
 * Wise person
 * WOP: visible False
 * WOP: messageFR intro
 * Bonjour, je suis une vieille personne sage et
 * le créateur de ce jeu. Malheureusement, il y a
 * des bugs que je ne parviens pas à corriger...
 *
 *
 *
 * J'ai besoin de ton aide, mais avant de t'attaquer
 * aux bugs difficiles, tu devras d'abord prouver ta
 * compétence en corrigeant des bugs plus simples qui
 * étaient présents dans les premières versions du jeu
 * il y a quelques années.
 *
 *
 *
 * Continue ce programme jusqu'au bout, et a l'écran de défaite, appuie sur le 
 * bouton 'Replay'.
 * EndOfMessage
 *
 * WOP: messageEN intro
 * Hello, I am a wise old person and the designer of this
 * game. However, there are difficult bugs I am not able
 * to find in the latest version.
 *
 *
 *
 * I would like to request your help, but before I challenge
 * you to my difficult bugs, you must first prove your worth
 * by correcting bugs that were present in the first stages
 * of the game I was developping years ago.
 *
 *
 *
 * Continue this program to the end. On the 'defeat' screen,
 * hit the 'Replay' button.
 * EndOfMessage
 *
 * WOP: messageFR second
 * Tu peux maintenant éditer le code du programme
 * qui correspond à ce niveau grâce au bouton
 * 'open in editor'.
 *
 *
 * Une fois les changements effectués, il faut recompiler et charger le nouveau 
 * programme avec le bouton 'Compile & Load'.
 *
 *
 *
 * Le bouton 'Restart' permet de recommencer au début du programme.
 *
 *
 * Si tu t'es perdu en modifiant le programme, tu peux revenir à la version 
 * initiale du niveau avec le bouton 'Reset'.
 * EndOfMessage
 *
 * WOP: messageEN second
 * You can now edit the code corresponding to this level
 * by clicking the 'open in editor' button.
 *
 *
 * After you have made changes, you must recompile and reload the new
 * program using the 'Compile & Load' button.
 *
 *
 *
 * The 'Restart' button allows you to start again from the beginning of
 * the program.
 *
 *
 * If you are lost when modifying the code, you can get back to the initial
 * version by hitting the 'Reset' button.
 * EndOfMessage
 *
 * WOP: messageFR PS
 * PS : si tu ne te souviens plus de mes explications, il faut quitter le 
 * niveau ('Menu' → 'Exit level') puis le recommencer.
 * EndOfMessage
 *
 * WOP: messageEN PS
 * PS: if you forgot my explanations, you must quit the level
 * ('Menu' → 'Exit level') then restart it.
 * EndOfMessage
 *
 * WOP: message implicit-rules-DEPRECATED
 *
 * Ancien "faux indice":
 * Pour te guider dans ton premier bug, voici un indice:
 * La position de la sortie est déterminée par la
 * variable 'exit_x' dans le code.
 *
 *
 * Essaye de modifier sa valeur pour valider ce niveau.
 
 * Héhé, je vois que tu es tombé dans mon piège...
 *
 *
 *
 * Modifier la position de départ, ou celle de la sortie, c'est un peu de la 
 * triche. Le niveau doit donc passer des tests avant d'être validé. Si 
 * celle-ci échoue, lit bien les raisons pour comprendre ce qui était attendu.
 *
 *
 *
 * Note : il est toujours possible de "contourner" la validation de chaque 
 * niveau, mais le jeu perd alors tout son intérêt et tu ne progresseras pas 
 * beaucoup en debug...
 * EndOfMessage
 *
 * WOP: position 6 3
 * WOP: direction down
 *
 * English version
 * WO message Hello, I am a wise old person and the designer of this
 * WO message game. However, there are difficult bugs I am not able
 * WO message to find in the latest version.
 * WO message
 * WO message I would like to request your help, but before I challenge
 * WO message you to my difficult bugs, you must first prove your worth
 * WO message by correcting bugs that were present in the first stages
 * WO message of the game I was developping years ago.
 * WO message 
 * WO message You can now edit the code using the 'edit' button above.
 * WO message To help you with your first bug, here is a hint:
 * WO message the exit position is determined using the exit_x variable.
 * WO message Try to change the value so that this level works.
 *
 */

#ifdef FIX_PLAYER_START // wrong way to solve the level
int player_x = 5;
#else
int player_x = 4;
#endif
#ifdef FIX_MOVE_EXIT // another wrong way to solve the level
int exit_x   = 8;
#else
int exit_x   = 9;
#endif

int main(void)
{
    player_x++;
    player_x++;
    player_x++;
    player_x++;
#ifndef BUG
    player_x++;
#endif

    if (player_x != exit_x) {
        printf("DEFAITE !\n"); //FR
        printf("DEFEAT!\n"); //EN
        exit (EXIT_FAILURE);
    } else {
        printf("VICTOIRE !\n"); //FR
        printf("VICTORY!\n"); //EN
        exit (EXIT_SUCCESS);
    }
}
