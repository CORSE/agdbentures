#!/usr/bin/env python3

from level.level_abc import AbstractLevel

import graphic.constants as cst
from logs import lvl
from level.world import Floor


class Level(AbstractLevel):
    """05_stairs"""

    def arcade_custom_first_start(self):
        wop = self.world.get_wop()
        wop.visible = True
        wop.place_on(self.world.get_coordinates("wop_stairs_start"))
        wop.send_update()

        player = self.world.get_player()
        talks = self.world.get_coordinates("wop_stairs_talks")
        trigger = self.world.get_coordinates("wop_too_long")

        wop.last_talk = 0

        def player_custom_update(player, m):
            lvl.debug(f"{trigger} and {player}")
            if wop.last_talk == 0 and player.is_on(talks):
                wop.last_talk = 1
                wop.talk('intro')

            elif wop.last_talk <= 1 and ( player.is_on(trigger) or  player.coord_x > trigger.coord_x ):

                wop.last_talk = 2

                wop.place_on(player, side=cst.Direction.LEFT)
                wop.visible = True
                pl = wop.payload()
                wop.send_update(pl)

                wop.talk('toolong')

                pl = {
                    "topic": "gui_change",
                    "action": "show_button",
                    "command": "continue",
                }
                wop.send_update(pl)  # will not contain 'wop' information

                pl = wop.payload()
                pl['change/speed'] = 2
                wop.send_update(pl)

                # place WOD near the exit
                ex = self.world.get_object('exit')
                wop.place_at(ex.coord_x - 1, ex.coord_y - 1)
                wop.send_update()

                wop.direction = cst.Direction.DOWN
                wop.send_update()

            elif wop.last_talk == 2 and player.is_below(wop):
                wop.last_talk += 1
                wop.talk('finally')

        player.post_update = player_custom_update

    def check_on_path(self):
        chk = self.checker.tracker
        plx = chk.get_variable_value_as_str('player_x', "int")
        ply = chk.get_variable_value_as_str('player_y', "int")
        tiles = self.world.maps['main'].tiles
        lvl.debug(
            f"Checking {plx, ply} on path: {tiles[ply][plx]} vs '+'"
        )
        if tiles[ply][plx] != Floor.F_GROUND:
            self.checker.failed("Player strayed out of the path.")

    def pre_validation(self):
        self.checker.register_watch('player_x', self.check_on_path)
        self.checker.register_watch('player_y', self.check_on_path)

        """Set a custom map."""
        str_map = [
            "                                    ",
            "                                    ",
            "   +++    ++    ++    ++    +++     ",
            "     ++   +++   +++   +++   +       ",
            "      ++  + ++  + ++  + ++  +       ",
            "       ++ +  ++ +  ++ +  ++ +       ",
            "        +++   +++   +++   +++       ",
            "         ++    ++    ++    ++       ",
            "                                    ",
        ]
        self.world.maps['main'].set_floor(str_map)

    def test(self):
        import tests.lib_test as T

        self.recompile_bug()
        T.ccontinue()
        T.expect_defeat()
        self.run()

        self.recompile_answer()
        T.ccontinue()
        T.expect_victory()
        self.run()
        self.check_validation(should_validate=True)

        self.recompile_fix("straight")
        T.ccontinue()
        T.expect_victory()
        self.run()
        self.check_validation(
            should_validate=False, substr_reasons="Player strayed out of the path."
        )

    def testGUI(self):
        from tests.testGUI import Reason, EventHandler
        import tests.lib_test as T 
        self.recompile_bug()
        self.verify_if_command_available("continue", False)
        EventHandler.addReason(Reason.GUI_CHANGE)
        while not self.world.player.is_on(self.world.get_coordinates("wop_too_long")):
            self.click_next()
        self.wait_gui_changed()
        self.verify_if_command_available("continue", True)
        self.click_continue(True)
        self.verify_defeat()


        self.recompile_answer()
        self.verify_if_command_available("continue", True)
        self.click_continue(True)
        self.verify_victory()
        T.info()



