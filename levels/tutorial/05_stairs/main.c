/* @AGDB
 *
 * BUG: one iteration of the loop too many
 * Tag: #offbyone
 *
 * Now a level with a loop. Introduces softly the notion of "breaks" without
 * giving real access to the break command. ("it's nice to be able to exectute
 * up to a particular point").
 *
 * GUI: only 'next/step/edit' buttons
 * Click on WOP executes code until player triggers the message.
 * Click on map executes code until player arrives at that point.
 *
 * Map: add a WOP after some steps in the "staircase".
 *
 * Trail is:
 * @-┐     ┌-┐     ┌-┐     ┌-┐     ┌->
 *   └┐    | └┐    | └┐    | └┐    |
 *    └┐   |  └┐   |  └┐   |  └┐   |
 *     └┐  |   └┐  |   └┐  |   └┐  |
 *      └┐ |    └┐ |    └┐ |    └┐ |
 *       └-┘     └-┘     └-┘     └-┘
 *
 * level_title:   0-5 Stairs
 * exec_name:    05_stairs
 * level_number: 5
 * engine_name:  none
 *
 * available_commands: next step edit
 * new_commands_during_level: continue
 * magic_break: True
 *
 * verify_init: cond player_x == 3
 * verify_init: cond player_y == 2
 * verify_always: cond exit_x == 30
 * verify_always: cond exit_y == 2
 *
 *
 * arcade_maps: main ../tutorial.tmx
 * arcade_maps: main_offset 38 0
 *
 *
 * coordinates: wop_stairs_start wop_stairs_talks wop_too_long
 *
 * WOP: message intro
 * Ne quitte pas le chemin !
 * EndOfMessage
 *
 * WOP: message toolong
 * C'est long hein ?
 *
 * Je vais te donner deux nouveaux pouvoirs:
 *
 *
 * - la commande 'continue' pour exécuter la suite du programme en entier
 *
 *
 *
 * - les "arrêts magiques" pour stopper le programme quand ton personnage passe
 *   dessus : tu peux maintenant cliquer sur le chemin avec le bouton droit de
 *   la souris pour placer un "arrêt magique".
 *
 *
 *
 * Fais un test : clique quelque part sur le chemin puis sur le bouton
 * 'continue'.
 *
 *
 * Attention : si l'arrêt magique se trouve à un endroit où le personnage ne
 * passera jamais, le programme continuera jusqu'au bout !
 *
 *
 *
 * PS : Tu peux zoomer et dezoomer avec la molette de la souris.
 *
 * Rejoins-moi vite !
 * EndOfMessage
 *
 * WOP: message finally
 * Ah ! Te voilà enfin.
 * Reste bien sur le chemin...
 * EndOfMessage
 **
 **/
#include <stdio.h>
#include <stdlib.h>

enum direction { UP, DOWN, LEFT, RIGHT };

/* Variables pour le joueur */
int player_x = 3, player_y = 2; /* coordonnées */
int player_direction = RIGHT;   /* direction du joueur */

/* Coordonnées de la sortie */
int exit_x = 30, exit_y = 2;

void turn_left(void)
{
    if (player_direction == UP) {
        player_direction = LEFT;
    } else if (player_direction == DOWN) {
        player_direction = RIGHT;
    } else if (player_direction == LEFT) {
        player_direction = DOWN;
    } else if (player_direction == RIGHT) {
        player_direction = UP;
    } else {
        fprintf(stderr, "Erreur: direction inconnue!\n");
    }
}

void turn_right(void)
{
    if (player_direction == UP) {
        player_direction = RIGHT;
    } else if (player_direction == DOWN) {
        player_direction = LEFT;
    } else if (player_direction == LEFT) {
        player_direction = UP;
    } else if (player_direction == RIGHT) {
        player_direction = DOWN;
    } else {
        fprintf(stderr, "Erreur: direction inconnue!\n");
    }
}

void forward(void)
{
    if (player_direction == UP) {
        player_y--;
    } else if (player_direction == DOWN) {
        player_y++;
    } else if (player_direction == LEFT) {
        player_x--;
    } else if (player_direction == RIGHT) {
        player_x++;
    } else {
        fprintf(stderr, "Erreur: direction inconnue!\n");
    }
}

void verify_exit(void)
{
    if (player_x != exit_x || player_y != exit_y) {
        printf("DEFAITE!\n");
        exit(EXIT_FAILURE);
    } else {
        printf("VICTOIRE!\n");
        exit(EXIT_SUCCESS);
    }
}

int main(void)
{
    forward();
#ifdef FIX_STRAIGHT  // Trying to cheat by going straight to the exit
    for (int i = player_x; i < exit_x; i++)
        forward();
    verify_exit();
#endif
#ifdef BUG
    for (int i = 0; i <= 4; i++) {
#else
    for (int i = 0; i <= 3; i++) {
#endif
        /* On descend comme sur des escaliers */
        /* À chaque itération la position change x en x+1 et y en y+1 */
        for (int j = 0; j <= 4; j++) {
            forward();
            turn_right();
            forward();
            turn_left();
        }
        forward();
        turn_left();

        /* Maintenant on remonte en haut de la carte */
        for (int j = 0; j <= 4; j++) {
            forward();
        }
        turn_right();
    }
    forward();
    forward();
    verify_exit();
}
