/* @AGDB
 *
 * BUG: missing {} and misleading indentation.
 *
 * HINT1: did you see the 'verify_exit' function has changed?
 * HINT2: braces are important
 *
 * Copy of the intro but with a bug.
 * Comes right after the introduction with a WOP explaing this is a test, the
 * exact same program has been used and the player should correct the program.
 *
 * GUI: 'next/edit' buttons, introduction of 'step'
 *
 * level_title:   0-2 Refactoring
 * exec_name:    02_refactoring
 * level_number: 2
 * engine_name:  none
 *
 * available_commands: next edit step
 * new_commands_during_level: step
 *
 * arcade_maps: main ../tutorial.tmx
 * arcade_maps: main_offset 10 0
 *
 **/
#include <stdlib.h>
#include <stdio.h>

/** @AGDB
 * Player status
 * player_y: 4
 *
 * Map goal
 * exit_y: 4
 *
 * verify_init: cond player_x == 4
 * verify_always: cond exit_x == 9
 * no_verify: change_in_function player_y forward
 * no_verify_exit: cond player_y == exit_y
 *
 *
 * coordinates: wop_refactor
 *
 * Wise person
 * WOP: message intro
 * Je me souviens avec émotion de ce premier bug...
 *
 *
 * J'avais fait mon premier 'refactoring' et mis les
 * déplacements ainsi que les conditions de sorties
 * dans une fonction, et cela ne fonctionnait plus !
 *
 *
 *
 * Sauras-tu retrouver l'erreur plus rapidement que
 * moi à l'époque ?
 *
 *
 * Je te donne une nouvelle commande : 'step' permet de rentrer dans les 
 * fonctions pendant l'exécution.
 *
 *
 *
 * Tu peux observer la valeur de variables avec la fonction 'print'
 * dans la console de debug. Par exemple, pour voir la valeur de 'x',
 * tape 'print x' dans la console.
 *
 *
 *
 * Il est aussi possible de mettre des 'printf' judicieusement placés dans le 
 * code pour avoir une trace des valeurs, par exemple:
 * printf("Valeur de x: %d\n", x);
 * Ces messages sont également affichés dans la console.
 * EndOfMessage
 *
 *
 *
 * English version
 * WO message I put the exit conditions in a function as it will often be 
 * WO message called in a level, however now it doesn't work anymore :-(
 * WO message Please help me.
 *
 */

int player_x = 4;
int exit_x = 9;

/*FR Gere le deplacement en avant du personnage */
/*EN Handle moving forward the character */
#ifdef BUG
void forward(int player_x)
#else
void forward(void)
#endif
{
    player_x++;
}

void verify_exit(void)
{
#ifdef FIX_NO_VERIFY
    exit (EXIT_SUCCESS);
#endif
    /*FR Si le joueur n'est pas a la bonne position,
      afficher le message de defaite et quitter le programme. */
    /*EN If the character is not on the correct spot,
      show the failure message and exit the program. */
    if (player_x != exit_x) {
        printf("DEFAITE !\n"); //FR
        printf("DEFEAT!\n"); //EN
        exit (EXIT_FAILURE);
    }
    else {
        printf("VICTOIRE !\n"); //FR
        printf("VICTORY!\n"); //EN
        exit (EXIT_SUCCESS);
    }
}


int main(void)
{
#ifdef BUG
    forward(player_x);
    forward(player_x);
    printf("Position du joueur: %d\n", player_x); //FR
    printf("Player position: %d\n", player_x); //EN
    forward(player_x);
    forward(player_x);
    forward(player_x);
#else
    forward();
    forward();
    forward();
    forward();
    forward();
#endif

    verify_exit();
}
