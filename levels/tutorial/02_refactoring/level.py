#!/usr/bin/env python3

from threading import Thread
from level.level_abc import AbstractLevel
import graphic.constants as cst
from config import Config
from logs import lvl


class Level(AbstractLevel):
    """02_first_bug"""

    def arcade_custom_first_start(self):
        wod = self.world.get_wop()
        wod.visible = True
        trigger = self.world.get_coordinates("wop_refactor")
        lvl.debug(f"Trigger for wod: {trigger}")
        wod.place_on(trigger, side=cst.Direction.UP)
        wod.send_update()
        player = self.world.get_player()
        wod.talks('intro')

        # def player_custom_update(player, m):
            # if player.is_on(trigger):
                # wod.talks("intro")
#
                # pl = {
                    # "topic": "gui_change",
                    # "action": "show_button",
                    # "command": "step",
                # }
                # wod.send_update(pl)
#
        # player.post_update = player_custom_update

    def test(self):
        import tests.lib_test as T

        self.recompile_bug()
        for _ in range(7):
            T.cnext()
        T.expect_sub_strings("DEFAITE")
        T.expect_defeat()
        self.run()

        self.recompile_answer()
        for _ in range(6):
            T.cnext()
        T.expect_sub_strings("VICTOIRE")
        T.expect_victory()
        self.run()

        self.recompile_fix('no_verify')
        for _ in range(7):
            T.cnext()
        T.expect_victory()
        self.run()
        self.check_validation(
            should_validate=False,
            substr_reasons="player_x must be equal to exit_x"
        )

    def testGUI(self):
        self.recompile_bug()
        self.verify_named_object_position("wop", 6, 3)
        self.verify_player_position(4)
        for i in range(2): # Verify step by step the bug
            self.click_step()
            self.click_step()
            self.verify_player_position(5)
            self.click_step()
            self.verify_player_position(4)
        self.click_next()
        for i in range(2):
            self.click_next()
            self.verify_player_position(4)
        self.click_continue(True)
        self.verify_defeat()

        self.recompile_answer()
        self.verify_named_object_position("wop", 6, 3)
        self.verify_player_position(4)
        self.click_next()
        self.verify_player_position(5)
        self.click_continue(True)
        self.verify_victory()
        import tests.lib_test as T
        T.info()


if __name__ == "__main__":
    # run_level(Level, __file__, level_type="text")
    test_level(Level, __file__)
