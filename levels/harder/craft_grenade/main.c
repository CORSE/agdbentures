/* @AGDB
 * level_title: Craft grenade
 * engine_name: event_args
 * available_commands : next step continue interrupt
 * exec_name: craft_grenade
 * engine_tags:
 * engine_antitags:
 * arcade_maps: main craft_grenade.tmx
 * player_mode: map_stack
 * no_verify: change_in_function map->player_y move_player
 * no_verify: change_in_function map->player_x move_player
 * verify_always: must_call destroy_tile
 * verify_always: must_call move_grenade
 * There's two bugs in this level : infinite loop because we forgot
 *  to decrease value in throw_grenade and grenade is here bug game
 *  say we have no grenade because id is not save in verify_grenade_npc
 */

#include <stdlib.h>
#include <stdio.h>


#include "events_map.h"
#include "custom_map.h"
#include "input_manager.h"
#include "engine/agdbentures.h"

int exit_x = 13;
int exit_y = 5;

int execute_loop(game_instance* game){
  
  map * m = current_map(game->map_stack);
  command * com ;

  while (!game->exit_main_loop) {
    com = get_next_command();
    c_apply_inputs(game, com);

    /* since the events may be applied if the input is FORWARD_N, 
    we need to check to avoid triggering the same event twice  */
    if (!game->exit_main_loop){
      show_map(m);
      apply_events(game);
    }
    // plus aucun input mais on a pas encore gagné ou perdu -> défaite

    if (com == NULL && !game->exit_main_loop){
      level_failed(game,game->level_exit_code);
      free_command(com);
      return EXIT_FAILURE;
      }
    free_command(com);
    }
  if (game->level_exit_code == -3){
    exit(EXIT_FAILURE);
  }
  return EXIT_SUCCESS;
}

int main(){

  game_instance* game = init_game();
  map* m = load_map("grenade_map",str_map1);
  place_grenade_parts(m);
  push_on_stack(game->map_stack,m);

  add_event(game, verify_player,NULL);
  add_event(game, my_verify_exit,NULL);
  add_event(game, verify_item,NULL);
  add_event(game, verify_grenade_npc,NULL);

  show_map(m);

  execute_loop(game);

  verify_exit(game);

}