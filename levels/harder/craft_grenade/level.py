#!/usr/bin/env python3

from level.level_abc import AbstractLevel
from logs import lvl
import graphic.constants as cst
import time
from level.action import Action
from datetime import datetime

class Level(AbstractLevel):
    """craft_grenade"""
    grenade_parts = []
    position_actu_grenade = []
    grenade_location = []
    my_map = None
    my_memory = None
    nb_restart = 1
    list_move = []
    count = 0

    def arcade_custom_restart(self):
        self.grenade_parts = []
        self.nb_restart += 1
        self.arcade_custom_first_start()

    def arcade_custom_first_start(self):
        self.count = 0
        self.my_map = None
        for i in range(3):
            self.grenade_parts.append(self.world.get_object(f"grenade_part_{i}"))
            self.grenade_parts[i].visible = False
            self.grenade_parts[i].send_update()
        self.position_actu_grenade = []
        self.grenade_location = []
        #self.exit = self.maps["main"].named_objects["exit"]
        self.grenade_finish = self.world.get_object("grenade-finish")
        self.grenade_finish.visible = False
        self.grenade_finish.send_update()
        self.grenade_craft = self.world.get_object("grenade")
        self.grenade_craft.visible = False
        self.grenade_craft.send_update()
        self.first_call_hide = True
        self.player.visible = True
        self.register_leave_function("destroy_tile", self.remove_wall)
        self.register_breakpoint("place_entity", self.check_location_grenade)
        self.register_breakpoint("throw_grenade", self.grenade_visible)
        self.player.post_update = self.post_update


    def move_grenade(self):

        x = (self.tracker.get_variable_value("grenade_x", as_raw_python_objects=True).value)
        y = (self.tracker.get_variable_value("grenade_y", as_raw_python_objects=True).value)
        self.grenade_finish.visible = True
        self.grenade_finish.place_at(x,y)
        self.grenade_finish.send_update()

    def grenade_visible(self):
        self.register_breakpoint("move_grenade", self.move_grenade)
    def post_update(self, player, memory):
        pos_player = [player.coord_x,player.coord_y]
        for i, pos_grenade in enumerate(self.grenade_location):
            if i != 3:
                if pos_player == pos_grenade:
                    self.grenade_parts[i].visible = False
                    self.grenade_parts[i].send_update()
    def check_location_grenade(self):
        if len(self.grenade_location) == 4:
            return
        if self.count % self.nb_restart != 0:
            self.count += 1
            return
        self.count += 1
        x = (self.tracker.get_variable_value("part_pos_x", as_raw_python_objects=True).value)
        y = (self.tracker.get_variable_value("part_pos_y", as_raw_python_objects=True).value)
        self.grenade_location.append([x,y])
        if len(self.grenade_location) < 4:
            self.grenade_parts[len(self.grenade_location) -1].place_at(x,y)
            self.grenade_parts[len(self.grenade_location) -1].visible = True
            self.grenade_parts[len(self.grenade_location) -1 ].send_update()
        else :
            self.grenade_craft.place_at(x,y)
            self.grenade_craft.visible = True
            self.grenade_craft.send_update()
        return

    def remove_wall(self):
        self.grenade_finish.visible = False
        self.grenade_finish.send_update()
        x = self.tracker.get_variable_value("x", as_raw_python_objects=True).value
        y = self.tracker.get_variable_value("y", as_raw_python_objects=True).value
        if x == self.grenade_craft.coord_x and y == self.grenade_craft.coord_y:
            self.grenade_craft.visible = False
            self.grenade_craft.send_update()
        if x == self.player.coord_x and y == self.player.coord_y:
            self.player.visible = False
            self.player.send_update()

#        if x == self.exit.coord_x and y == self.exit.coord_y:
#            payload = {
#                "topic" : "exit_update",
#                "object_name": "exit",
#                "action" : Action("disappear")
#            }
#            self.send_to_gui(payload)
        payload = {
            "topic":"sprites",
            "action": Action("hide"),
            "layer" : "walls",
            "locations": [
                (x,y)
            ],
        }
        self.send_to_gui(payload)


    def find_path(self):

        liste_move = [
        "RIGHT",
        "DOWN",
        "DOWN",
        "DOWN",
        "LEFT",
        "RIGHT",
        "RIGHT",
        "RIGHT",
        "RIGHT",
        "RIGHT",
        "LEFT",
        "LEFT",
        "LEFT",
        "LEFT",
        "UP",
        "UP",
        "UP",
        "UP",
        "RIGHT",
        "RIGHT",
        "t",
        "RIGHT",
        "RIGHT",
        "RIGHT",
        "RIGHT",
        "RIGHT",
        "RIGHT",
        "RIGHT",
        "RIGHT",
        "RIGHT",
        "UP",
        "RIGHT"
        ]
        return liste_move

    def check_on_inventory(self):
        if self.good_id:
            return
        grenade = self.checker.tracker.get_variable_value_as_str('game->inventory[0]->id',"int")
        if grenade == 14:
            self.good_id = True


    def change_seed(self):
        self.checker.send_command("next")
        self.checker.send_command("next")
        self.checker.send_command("set variable seed = 1")
        self.checker.send_command("continue")

    def pre_validation(self):
        self.good_id = False
        self.checker.register_leave_function("add_item_in_inventory", self.check_on_inventory)
        self.checker.append_inputs(
            self.find_path()
        )
        self.checker.register_breakpoint("place_grenade_parts", self.change_seed)

    def post_validation(self):
        if not self.good_id:
            self.checker.failed("It seems you don't have grenade in your inventory")
        if not self.player.visible:
            self.checker.failed("You die !")



    def solve_level(self, T, die = False):
        T.command("next")
        T.command("next")
        T.command("step")
        T.command("next")
        T.command("next")
        T.command("set variable seed = 1")
        T.right()
        T.down(3)
        T.left()
        T.right(5)
        T.left(4)
        T.up(4)
        T.right(2)
        if die:
            T.right(5)
        T.send_input("t")
        T.right(10)

    def test(self):
        import tests.lib_test as T
        self.recompile_bug()
        self.solve_level(T, die=True)
        T.ccontinue()
        T.expect_infinite_loop()
        self.run()
        self.recompile_answer()
        self.solve_level(T, die=True)
        T.ccontinue()
        T.expect_defeat()
        self.run()
        self.recompile_answer()
        self.solve_level(T, die=False)
        T.ccontinue()
        T.expect_victory()
        self.run()
if __name__ == "__main__":
    # run_level(Level, __file__, level_type="text")
    # run_level(Level, __file__, level_type="arcade")
    test_level(Level, __file__)

