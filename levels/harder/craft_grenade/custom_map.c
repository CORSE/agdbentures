#include "custom_map.h"
#include "time.h"

const char *str_map1 = "\
##############\n\
#         #  #\n\
#         #  #\n\
#         #  #\n\
#         #  #\n\
#>        #  @\n\
#         #  #\n\
#         #  #\n\
#         #  #\n\
#         #  #\n\
##############\n\
";

void place_grenade_parts(map *m)
{
    int seed = time(NULL);
    int part_pos_x;
    int part_pos_y;
    int remaining_parts = 3;
    int parts[3] = {FIRST_PART, SECOND_PART, THIRD_PART};
    srand(seed);

    while (remaining_parts > -1) {
        part_pos_x = (rand() % 9) + 1;
        part_pos_y = (rand() % 9) + 1;
        if (!m->tiles[part_pos_y][part_pos_x]) {
            if (remaining_parts) {
                entity e = create_entity('I', PICKABLE, C_ITEM, NULL);
                e->id = parts[remaining_parts - 1];
                place_entity(m, e, part_pos_y, part_pos_x);
            } else {
                place_entity(m, create_entity('A', OBSTACLE, C_NPC, NULL), part_pos_y,
                             part_pos_x);
            }
            remaining_parts--;
        }
    }
}
