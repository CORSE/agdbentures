#include "events_map.h"
#include "custom_map.h"
#include "engine/agdbentures.h"
#include "input_manager.h"
#include "stdbool.h"

void grenade_explanations()
{
    printf("To throw the grenade enter THROW or t.\n\
The grenade will be thrown in the direction the player is looking and will explode 4 tiles away\n\
the explosion radius is %d and is in the shape of a square\n\
",
           GRENADE_RADIUS);
}

void my_verify_exit(game_instance *game, void *r)
{

    map *m = current_map(game->map_stack);

    if (find_category_in_stack(m, C_EXIT m->player_y, m->player_x)) {
        printf("You reached the exit !\n");
        level_success(game);
    }
}

void verify_player(game_instance *game, void *r)
{
    map *m = current_map(game->map_stack);

    if (!find_category_in_stack(m, C_PLAYER, m->player_y, m->player_x)) {
        printf("you have just died by the explosion of your own grenade ...\n");
        level_failed(game, -3);
    }
}

void verify_grenade_npc(game_instance *game, void *r)
{
    map *m = current_map(game->map_stack);
    int dest_x, dest_y;
    switch (m->player_direction){
        case DIR_UP:
            dest_x = m->player_x;
            dest_y = m->player_y - 1;
            break;
        case DIR_DOWN:
            dest_x = m->player_x;
            dest_y = m->player_y + 1;
            break;
        case DIR_LEFT:
            dest_x = m->player_x - 1;
            dest_y = m->player_y;
            break;
        case DIR_RIGHT:
            dest_x = m->player_x + 1;
            dest_y = m->player_y;
            break;
    }
    if (find_category_in_stack(m, C_NPC, dest_y, dest_x)) {
            clear_inventory(game);
            entity gre = create_entity('I', 0, C_ITEM, NULL);
#ifndef BUG
            gre->id = GRENADE;
#endif
            add_item_in_inventory(game, gre);
            grenade_explanations();
        } else {
            printf(
                "You don't have the required pieces to craft the grenade ^^\n");
        }
    }
}

void verify_item(game_instance *game, void *r)
{
    map *m = current_map(game->map_stack);

    if (find_category_in_stack(m, C_ITEM, m->player_y, m->player_x)) {
        printf("You gathered one part of the grenade !\n");
        pick_item(game, m->player_y, m->player_x);
    }
}
