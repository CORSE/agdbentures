#include "engine/agdbentures.h"
#include "custom_map.h"

entity find_item_id_in_inventory(game_instance* game, int id) {
    for(int i = 0; i < MAX_INVENTORY_SIZE; i++) {
        if (game->inventory[i] && game->inventory[i]->id == id) {
            return game->inventory[i];
        }
    }
    return NULL;
}

void destroy_tile(map* m, int y, int x){
    if ( y >= m->height || y <0 || x <0 || x >= m->width ){
        return;
    }
    entity tmp;
    while (m->tiles[y][x] != NULL) {
        if (m->tiles[y][x]->stats)
            free(m->tiles[y][x]->stats);
                
        tmp = m->tiles[y][x];
        m->tiles[y][x]=m->tiles[y][x]->next;
        free(tmp);
    }
    m->floor[y][x].category = GRASS;
    m->floor[y][x].display_symbol = ' ';
    remove_property(&m->floor[y][x].properties, OBSTACLE);
}

void boom(map* m, int y, int x){
    for (int i = y-(GRENADE_RADIUS-1); i < y+GRENADE_RADIUS; i++){
        for (int j = x-(GRENADE_RADIUS-1); j < x+GRENADE_RADIUS; j++){
            destroy_tile(m,i,j);
        }
    }
}

int throw_grenade(game_instance* game){
    int dest_x,dest_y;
    map* m = current_map(game->map_stack);
    switch (m->player_direction){
        case DIR_UP:
            dest_x = m->player_x;
            dest_y = m->player_y - 1;
            break;
        case DIR_DOWN:
            dest_x = m->player_x;
            dest_y = m->player_y + 1;
            break;
        case DIR_LEFT:
            dest_x = m->player_x - 1;
            dest_y = m->player_y;
            break;
        case DIR_RIGHT:
            dest_x = m->player_x + 1;
            dest_y = m->player_y;
            break;
    }
    int grenade_y,grenade_x;
    if (is_obstacle(m,dest_y,dest_x)){
        grenade_y = m->player_y;
        grenade_x = m->player_x;
    } else{
        grenade_y = dest_y;
        grenade_x = dest_x;
    }
    direction grenade_dir = m->player_direction;
    int remaining_steps = GRENADE_RANGE;

    drop_item(game,find_item_id_in_inventory(game, GRENADE), grenade_y,grenade_x);
    show_map(m);
    int new_y, new_x;
    look_forward(grenade_y, grenade_x, grenade_dir, &new_y,&new_x);
    while (remaining_steps && !is_obstacle(m,new_y,new_x))
    {   
        move_grenade(m,grenade_y,grenade_x,new_y, new_x);
#ifdef BUG
#else
        remaining_steps--;
#endif
        grenade_y = new_y; grenade_x = new_x;
        show_map(m);
        look_forward(grenade_y, grenade_x, grenade_dir, &new_y,&new_x);
    }
    look_forward(grenade_y, grenade_x, grenade_dir, &new_y,&new_x);
    while (remaining_steps && (m->player_y!=grenade_y || m->player_x!=grenade_x))
    {
        move_grenade(m,grenade_y,grenade_x,new_y,new_x);
#ifdef BUG
#else
        remaining_steps--;
#endif
        grenade_y = new_y; grenade_x = new_x;
        show_map(m);
        look_forward(grenade_y, grenade_x, grenade_dir, &new_y,&new_x);
    }
    boom(m, grenade_y,grenade_x);
    return 1;
}

int c_apply_inputs(game_instance* game, command * c){
    if (!strcmp(c->command_buffer, "THROW") || !strcmp(c->command_buffer, "t")) {
        if (find_item_id_in_inventory(game, GRENADE)){
            return throw_grenade(game);
        }
            printf("You don't have any grenade to throw !\n");
            return 0;
    }
    return apply_input(current_map(game->map_stack), c);
}

void move_grenade(map *m, int grenade_y, int grenade_x, int new_y, int new_x){
    move_entity(m,grenade_y,grenade_x,new_y,new_x);
}
