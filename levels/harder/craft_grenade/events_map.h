#ifndef EVENTS_MAP_H
#define EVENTS_MAP_H

#include "engine/agdbentures.h"

// Vérifie si le joueur se trouve sur la case de victoire
void my_verify_exit(game_instance* game, void* r);

void verify_player(game_instance* game, void* r);

void verify_item(game_instance* game, void* r);

void verify_grenade_npc(game_instance* game, void* r);

#endif // EVENTS_MAP_H
