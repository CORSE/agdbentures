#ifndef CUSTOM_MAP_H
#define CUSTOM_MAP_H

#include "engine/agdbentures.h"

#define FIRST_PART 11
#define SECOND_PART 12
#define THIRD_PART 13
#define GRENADE 14
#define GRENADE_RADIUS 3
#define GRENADE_RANGE 5

extern int exit_x;
extern int exit_y;



extern const char *str_map1;

void place_grenade_parts(map* m);

#endif // CUSTOM_MAP_H
