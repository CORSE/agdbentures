#ifndef INPUT_MANAGER_H
#define INPUT_MANAGER_H

#include "engine/agdbentures.h"

int c_apply_inputs(game_instance* game, command *c);

entity* find_item_id_in_inventory(game_instance* game, int id);

void move_grenade(map *m, int grenade_y, int grenade_x, int new_y, int new_x);

#endif // INPUT_MANAGER_H
