#ifndef INPUT_MANAGER_H
#define INPUT_MANAGER_H

#include "../../engines/fullgame/agdbentures.h"
#include "custom_map.h"
#include "hanoi.h"

// examine la commande c et appelle les fonctions d'action ou de déplacement correspondantes
void pick(void);
void drop (void);
void solve(void);
void apply_disks_movements(movements * mv);
void c_apply_input(command * c);


#endif // INPUT_MANAGER_H