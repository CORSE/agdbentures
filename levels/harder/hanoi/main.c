/** @AGDB
 * level_title: Tours de Hanoi
 * program_name: hanoi
 * engine_name: fullgame
 * available_commands: next step continue
 * map_height: 7
 * map_width: 10
 *
 * alias: map_array mstack.maps[mstack.length - 1].floor
 * alias: exit_x    mstack.maps[mstack.length - 1].exit_x
 * alias: exit_y    mstack.maps[mstack.length - 1].exit_y
 * alias: left  4
 * alias: right 6
 * alias: up    8
 * alias: down  2
 *
 * watch: player_direction player_x player_y
 * track: load_map:mstack load_map:player_x load_map:player_y
 * 
 * BUG: 
 * tag: 
 * 
 * HINT1: What is the value of current_map()->entities for switches?
 */ 

/**
 * Entry point of the game. Manages the main loop, and apart from
 * that only calls functions.
 * 
 * Initialization:
 *  1) load the map and possible items
 *  2) load all events
 *  3) initialize the player's position and data
 *  4) start the main loop
 * 
 * Main loop:
 *  1) apply the player's input (move or action)
 *  2) observe triggers, apply each resolution function or applying triggers
 *  3) read the next input (file) or wait for the next frame and buffer an input during the wait (real time)
 * 
 * Endgame:
 *  1) receive signal from level_success or level_failure
 *  2) free all allocated memory
 *  3) exit properly
 * 
 * Game design related tip:
 *   If you want to create a game, note all your ideas, they may serve later.
 *   Video game is an art as cinema or literature: play lots of games to get a solid culture.
 *   But beside game design, several fields exist in video games creation:
 *      - level design
 *      - storytelling and writing
 *      - music and sound effets
 *      - worldbuilding
 *      - ... and many others!
*/
#include "../../engines/fullgame/agdbentures.h"
#include "events_map.h"
#include "input_manager.h"
#include "custom_map.h"
#include "hanoi.h"

extern Hanoi * H;
extern int room_solved;

int execute_loop(bool manual_mode){
    command * com = get_next_command();
    exit_main_loop = false;
    // The Holy Main Loop
    while (!exit_main_loop) {
        c_apply_input(com);

        /* since the events may be applied if the input is FORWARD_N, 
           we need to check to avoid triggering the same event twice  */
           
        if (!exit_main_loop)
            apply_events();
        
        if (manual_mode)
            show_map();

        if (!exit_main_loop)
            com = get_and_free_input(com);

        // plus aucun input mais on a pas encore gagné ou perdu -> défaite
        if (!manual_mode && com == NULL && !exit_main_loop){
            level_failed();
            return 1;
        }
    }
    return 0;
}


int main(int argc, char ** argv) {
    bool manual_mode = true;

    //TODO FLO: mode "non manuel" doit être géré par agdbentures
    if (argc > 1)
        manual_mode = false;

    if (manual_mode) {
        printf("Manual mode. Available commands: UP, DOWN, LEFT, RIGHT and *_N variants, PICK, DROP.\nUse Ctrl+C to quit.\n");
    } else {
        if (!init_inputs_file(argv[1])) {
            printf("Error while reading the inputs!\n");
            return EXIT_FAILURE;
        }
    }
    init_map_stack();
    
    

    //TODO FLO: ajouter commentaires explicatifs
    add_event((void *)verify_exit, NULL);
    add_event((void *)verify_stack, NULL);
    
    // first room 
    init_map(load_map("a first room", str_map1));
    if (manual_mode) {
        show_map();
    }
    if (execute_loop(manual_mode)){
        remove_all_events();
        free_map_stack();
        close_inputs_file();
        return 0;
    }

    //2nd room
    init_map(load_map("a second room", str_map2));
    if (manual_mode) {
        show_map();
    }
    if (execute_loop(manual_mode)){
        remove_all_events();
        free_map_stack();
        close_inputs_file();
        return 0;
    }
    
    //3rd room
    init_map(load_map("a third room", str_map3));
    printf("You can now use the SOLVE command in front of the first stack to solve the probleme automatically.\n");
    if (manual_mode) {
        show_map();
    }
    H = initialize_hanoi(4,8,1);
    if (execute_loop(manual_mode)){
        remove_all_events();
        free_map_stack();
        close_inputs_file();
        return 0;
    }


    remove_all_events();
    free_map_stack();
    close_inputs_file();
    
    return EXIT_SUCCESS;
}
