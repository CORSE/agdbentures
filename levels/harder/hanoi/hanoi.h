#ifndef HANOI_H
#define HANOI_H

#include <stdlib.h>
#include <stdio.h>

#define NB_DISKS_MAX 20
#define NB_STACKS_MAX 3
#define NB_MAX_MOVE 1048576 //TODO Flo: pourquoi ce nombre ?

#include "custom_map.h"

// the stack are always horizontal and adjacent. The first stack is above the last and is oriented from left to right.
typedef struct {
    int nb_disks[3];    // nb_disks[i] -> number of disks on stack i
    int pos_x1;
    int pos_y1;        // map positions of the first stack
    int nb_disks_total; // number of disks in total
} Hanoi;

typedef struct {
    int move [NB_MAX_MOVE][2];
    int len;
} movements;

movements * initialize_movements(); 
void add_movement(movements * m, int start, int end);
void show_movements(movements * m);

Hanoi * initialize_hanoi(int n, int x, int y);
void hanoi_tower_rec(int n, int start, int aux, int end, movements * m);

movements * solve_hanoi(void); // return the movements the player have to make to solve the problem.

#endif // HANOI_H
