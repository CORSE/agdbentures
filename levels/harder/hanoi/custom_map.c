#include "custom_map.h"

extern Hanoi * H;

const char *str_map1 =
"\
+--+---+\n\
|>   1[@\n\
|    =-+\n\
|    =-+\n\
+---+--+\n\
";


const char *str_map2 =
"\
+---+---+\n\
|>   12[@\n\
|    ==-+\n\
|    ==-+\n\
+---+-+-+\n\
";

const char *str_map3 =
"\
+----+----+\n\
|>   1234[@\n\
|    ====-+\n\
|    ====-+\n\
+----+----+\n\
";

//initialize the maps and the hanoi structures.
void init_map(map * m) {
    int max = 0;
    int x_pos1, y_pos1;
    for (int y = 0; y < m->height; y++) {
        for (int x = 0; x < m->width; x++) {
            int i = coord_idx(y, x);
            if (m->entities[i] && m->entities[i]->category == DISK) {
                m->floor[i]->obstacle = true;
                if (m->entities[i]->stats[0]==1){
                    x_pos1 = x, y_pos1 = y;
                }
                if (m->entities[i]->stats[0] > max){
                    max = m->entities[i]->stats[0];
                }
            }
        }
    }
    H = initialize_hanoi(max, x_pos1 + max - 1, y_pos1);
    printf("You enter %s.\n", m->name);
}