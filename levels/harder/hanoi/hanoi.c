#include "hanoi.h"

Hanoi * H;
extern int room_solved;

movements * initialize_movements(){
    movements * m = (movements *)malloc(sizeof(movements));
    m->len = 0;
    return m;
}

void add_movement(movements * m, int start, int end){
    int n  = m->len;
    if (n >= NB_MAX_MOVE){
        printf("You can not add another movement.\n");
        exit(1);
    }
    m->move[n][0] = start;
    m->move[n][1] = end;
    m->len++;
}

void show_movements(movements * m){
    int n = m->len;
    for (int i = 0; i<n; i++){
        printf("%d -> %d \n", m->move[i][0], m->move[i][1]);
    }
    printf("%d\n",m->len);
}

// n is the number of disks for the problem, x1 is the x position of the beginning if the first stack an y1 is the y position of the beginning if the first stack 
Hanoi * initialize_hanoi(int n, int x1, int y1){
    room_solved = 0;
    Hanoi * H = (Hanoi *) malloc(sizeof(Hanoi));
    H->nb_disks_total = n;
    H->nb_disks[0] = n;
    H->nb_disks[1] = 0;
    H->nb_disks[2] = 0;
    H->pos_x1 = x1;
    H->pos_y1 = y1;
    
    return H;
}

// get the movements the player needs to make to solve hanoi structure
void hanoi_tower_rec(int n, int start, int aux, int end, movements * m){
    if (n > 0){
        #ifdef index_error
        hanoi_tower_rec(n-1, aux, start, end, m);
        #else
        hanoi_tower_rec(n-1, start, end, aux, m);
        #endif
        add_movement(m, start, end);
        hanoi_tower_rec(n-1, aux, start, end, m);
    }   
}

movements * solve_hanoi(void){
    movements * m = initialize_movements();
    if (H->nb_disks[0]!=H->nb_disks_total){
        printf("You already started solving the problem, put the stack as it was when you entered the room to get help.\n");
        return m;
    }
    hanoi_tower_rec(H->nb_disks_total, 0, 1, 2, m);
    return m;
}
