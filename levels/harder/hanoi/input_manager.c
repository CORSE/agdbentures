#include "input_manager.h"

extern Hanoi * H;
int disk_in_pocket = 0;

//move player in front of the stack where they need to pick or drop.
void player_go_to(int stack_num){
    map * m = current_map();
    int pos_to_y = H->pos_y1 + stack_num;
    int pos_to_x = H->pos_x1-H->nb_disks_total;
    if(m->player_x == pos_to_x && m->player_y == pos_to_y){
        return;
    }
    move_entity(m->player_y, m->player_x, pos_to_y, pos_to_x);
    m->player_y = pos_to_y;
    m->player_x = pos_to_x;
}

void apply_disks_movements(movements * mv){
    for (int i = 0; i < mv->len; i++){
        player_go_to(mv->move[i][0]);
        pick();
        player_go_to(mv->move[i][1]);
        drop();
    }
}


// if the player is in front of the first stack : calculate the movements the player needs to make to solve the problem and apply them.
void solve(){
    map * m = current_map();
    int pos_stack_x = H->pos_x1 - H->nb_disks_total; // first stack's position on the map
    int pos_stack_y = H->pos_y1;
    if(m->player_x == pos_stack_x && m->player_y == pos_stack_y){ //player in front of the first stack
        movements * mv = solve_hanoi();
        apply_disks_movements(mv);
        //free_move(mv);
    }
    else{
        printf("You're not in front of the first stack.\n");
    }
}

// pick the last disk from the stack in front of the player.
void pick(void){
    #ifndef verify_disk_in_pocket
    if (disk_in_pocket != 0){
        printf("You already have a disk, drop it before picking another one.\n");
        return;
    }
    #endif

    map * m = current_map();
    int i;
    int x = H->pos_x1 - H->nb_disks_total; // first stack's position on the map
    int y = H->pos_y1;
    int stack_num;
    // we verify if the player is on a line where a stack is
    if (m->player_y == y){
        stack_num = 0;
    } else if(m->player_y == y+1){
        stack_num = 1;
    } else if(m->player_y == y+2){
        stack_num = 2;
    } else {
        printf("You can't pick anything here : you're not in front of a stack.\n");
        return;
    }    
    if(m->player_x == x){
        #ifdef verify_disk_on_stack  
        i = coord_idx(m->player_y, H->pos_x1 - H->nb_disks[stack_num] + 1); // the last disk x's position is 'number of disks on the stack' away from the beginning if the stack.
        disk_in_pocket = m->entities[i]->stats[0];
        m->entities[i]->category = STACK;
        H->nb_disks[stack_num]--;
        m->floor[i]->obstacle = false;   
        #else
        if (H->nb_disks[stack_num] == 0){
            printf("There isn't any disk on this stack.\n");
            return;
        } else{
            i = coord_idx(m->player_y, H->pos_x1 - H->nb_disks[stack_num]+1);// the last disk x's position is 'number of disks on the stack' away from the beginning if the stack.
            disk_in_pocket = m->entities[i]->stats[0];
            m->entities[i]->category = STACK;
            H->nb_disks[stack_num]--;
            m->floor[i]->obstacle = false;   
        }
        #endif
    }
    else{
        printf("You can't pick anything here : you're not in front of a stack.\n");
    }

}

// BUG : oubli de regarder si pas de disque avant de récup l'ancien
// drop the disk in the pocket on the stack in front of the player if allowed.
void drop(void){
    map * m = current_map();
    if (disk_in_pocket == 0 ){  //the user didn't pick a disk before trying to drop one.
        printf("You don't have a disk to drop.\n");
        return;
    }
    int i;
    int x = H->pos_x1 -  H->nb_disks_total; 
    int y = H->pos_y1;
    int stack_num;
    // we verify if the player is on a line where a stack is
    if (m->player_y == y){
        stack_num = 0;
    } else if(m->player_y == y+1){
        stack_num = 1;
    } else if(m->player_y == y+2){
        stack_num = 2;
    } else {
        printf("You can't drop anything here : you're not in front of a stack.\n");
        return;
    }    
    if(m->player_x == x){ //same for column
        #ifdef verify_disk_on_stack
        i = coord_idx(m->player_y, H->pos_x1 - H->nb_disks[stack_num]+1);
        int last_disk = m->entities[i]->stats[0];
        if (disk_in_pocket > last_disk || H->nb_disks[stack_num] == H->nb_disks_total){
            printf("You can't move a bigger disk on top of a smaller one.\n");
            return;
        }
        #else
        int nb_disks_tower = H->nb_disks[stack_num];
        if (nb_disks_tower > 0){
        
            i = coord_idx(m->player_y, H->pos_x1 - H->nb_disks[stack_num]+1);
            int last_disk = m->entities[i]->stats[0];
            if (disk_in_pocket > last_disk || H->nb_disks[stack_num] == H->nb_disks_total){
                printf("You can't move a bigger disk on top of a smaller one.\n");
                return;
            }
        }
        #endif
        H->nb_disks[stack_num]++;
        i = coord_idx(m->player_y, H->pos_x1-H->nb_disks[stack_num]+1);
        m->entities[i]->category = DISK;
        m->entities[i]->stats = (int *)malloc(sizeof(int));
        m->entities[i]->stats[0] = disk_in_pocket;
        m->floor[i]->obstacle = true;
        #ifndef update_disk_drop
        disk_in_pocket = 0;
        #endif
        
    } else{
        printf("You can't drop anything here : you're not in front of a stack.\n");
    }

}

void c_apply_input(command * c) {
    map * m = current_map();
    if (!strcmp(c->command_buffer, "PICK")) {
        pick();
    } else if (!strcmp(c->command_buffer, "DROP")) {
        drop();
    } else if (!strcmp(c->command_buffer, "SOLVE") && !strcmp(m->name, "a third room")){
        //TODO Flo: utiliser des identifiants de maps entiers ?
        solve();
    } else {
        apply_input(c, m->player_y, m->player_x);
    }
}
