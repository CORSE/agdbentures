#include "events_map.h"

extern Hanoi * H;
int room_solved  = 0; 

//verify if the player reached the exit (@)
void verify_exit(void) {
    map * m = current_map();
    int i = player_idx();
    if (m->entities[i] && find_in_stack(m->player_y, m->player_x, FLAG)) {
        printf("You reached the exit!\n");
        level_success();
    }
}

//verify if the third stack has all the disks (problem solved)
void verify_stack() {
    if (room_solved) { // if the room is already solved : no need to rewrite the message.
        return;
    }

    if (H->nb_disks[2] == H->nb_disks_total){
        printf("You solved the problem and the door opens.\n");
        room_solved = 1;
        map * m = current_map();
        //TODO Flo: sauvegarder la position de la porte ?
        for (int i = 0; i < m->width * m->height; i++) { // open the door
            if (m->entities[i] && m->entities[i]->category == DOOR) {
                m->entities[i]=NULL;
                free(m->entities[i]);
                m->floor[i]->category = GRASS;
                m->floor[i]->obstacle = false;
            }
        }
    }
}
