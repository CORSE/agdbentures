#ifndef CUSTOM_MAP_H
#define CUSTOM_MAP_H

#include "../../engines/fullgame/agdbentures.h"
#include "hanoi.h"

#define N_STACKS 3

extern const char * str_map1;
extern const char * str_map2;
extern const char * str_map3;


// initializes the stacks
void init_map(map * m);

#endif // CUSTOM_MAP_H
