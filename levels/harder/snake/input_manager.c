#include "input_manager.h"

void custom_apply_input(game_instance* game, command * c) {
    map * m = current_map(game->map_stack);
    direction dir = m->player_direction;

    if (!strncmp(c->command_buffer, "U", 1)) {
        dir = DIR_UP;
    } else if (!strncmp(c->command_buffer, "D", 1)) {
        dir = DIR_DOWN;
    } else if (!strncmp(c->command_buffer, "L", 1)) {
        dir = DIR_LEFT;
    } else if (!strncmp(c->command_buffer, "R", 1)) {
        dir = DIR_RIGHT;
    } else {
        printf("Unknown input command. Continuing in the same direction\n");
    }
    m->player_direction = dir;

    int y, x;
    player_look_forward(m, &y, &x);

    // there is no wall in this game so collision will be 0 or -1
    int dest_y, dest_x;
    if (!can_move_to(m, m->player_y, m->player_x, y, x)) {
        switch (dir) {
        case DIR_UP:
            dest_y = m->height - 1;
            m->player_y = dest_y;
            move_entity(m, m->player_y, m->player_x, dest_y, m->player_x);
            break;
        case DIR_DOWN:
            m->player_y = 0;
            move_entity(m, m->player_y, m->player_x, 0, m->player_x);
            break;
        case DIR_LEFT:
            dest_x = m->width - 1;
            m->player_x = dest_x;
            move_entity(m, m->player_y, m->player_x, m->player_y, dest_x);
            break;
        case DIR_RIGHT:
            m->player_x = 0;
            move_entity(m, m->player_y, m->player_x, m->player_y, 0);
            break;        
        default:
            printf("apply_input: invalid direction\n");
            break;
        }
    } else {
        switch (dir) {
        case DIR_UP:
            player_up(m);
            break;
        case DIR_DOWN:
            player_down(m);
            break;
        case DIR_LEFT:
            player_left(m);
            break;
        case DIR_RIGHT:
            player_right(m);
            break;        
        default:
            printf("apply_input: invalid direction\n");
            break;
        }
    }
}
