#ifndef EVENTS_MAP_H
#define EVENTS_MAP_H

#include "engine/agdbentures.h"

typedef struct Snake {
  entity *body;
  struct Snake *next;
} snake;

snake *head;

/**
 * moves the snake and makes it longer if it ate an apple.
 * Have it crash and game over if it ate itself.
 */
void update_snake(game_instance *game);

/**
 * Creates an apple at a random location outside of the snake's body.
 */
void spawn_apple(game_instance *game);

#endif // EVENTS_MAP_H
