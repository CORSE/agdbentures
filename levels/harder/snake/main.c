/** @AGDB
 * level_title: Snake
 * exec_name: snake
 * engine_name: events
 * level_number: 1109
 * available_commands: next step continue
 * map_height: 10
 * map_width: 10
 * 
 * BUG: We forgot to reset no_apple to false in spawn_apple, so apples spawn at each step.
 * tag: snake simple chained_list
 * 
 * HINT1: How is the spawning of apples supposed to be prevented?
 */ 

/**
 * Entry point of the game. Manages the main loop, and apart from
 * that only calls functions.
 * 
 * Game design related tip:
 *   Snake is a weird game you can't win, yet many people like it, maybe
 *   because it is simplistic and allows you to pass time
 *   on your brand new Nokia phone, or TI82.
*/

#include "engine/agdbentures.h"
#include "events_map.h"
#include "input_manager.h"

const char * str_map =
"\
          \n\
     ^    \n\
     A    \n\
     A    \n\
     A    \n\
          \n\
          \n\
          \n\
          \n\
          \n\
";


/**
 * Generates a snake in the form of a linked list
 */
void create_snake(game_instance* game) {
    snake * cur;
    snake * pred = NULL;
    map * m = current_map(game->map_stack);

    for (int y = 0; y < m->height; y++) {
        for(int x = 0; x < m->width; x++) {
            if (m->tiles[y][x] && m->tiles[y][x]->category == C_NPC) {
                cur = malloc(sizeof(snake));
                if (pred)
                    pred->next = cur;
                else
                    head = cur;
                cur->body = m->tiles[y][x];
                cur->next = NULL;
                pred = cur;
            }
        }
    }

    // actual head is where the player is
    entity* head_e = malloc(sizeof(entity));
    head_e->category = C_NPC;
    head_e->id = pred->body->id + 1;
    m->tiles[m->player_y][m->player_x] = head_e;

    cur = malloc(sizeof(snake));
    cur->next = head;
    cur->body = head;
    head = cur;
}


/**
 * Frees the body of a snake starting from its parameter
 */
void free_snake(snake * s) {
    if (s->next)
        free_snake(s->next);
    free(s);
}


int main(int argc, char ** argv) {
    printf("Manual mode. Available commands: UP, DOWN, LEFT, RIGHT and *_N variants.\nUse Ctrl+C to quit.\n");
   
    game_instance* game = init_game();
    map* m = load_map("torus", str_map);
    push_on_stack(game->map_stack, m);
    create_snake(game);
    srand(0xC0FF33); // random seed
    spawn_apple(game);
    add_event(game, update_snake);
    add_event(game, spawn_apple);

    show_map();

    command * com = get_next_command();

    // The Holy Main Loop
    while (!game->exit_main_loop) {
        custom_apply_input(game, com);

        /* since the events may be applied if the input is FORWARD_N, 
           we need to check to avoid triggering the same event twice*/
        if (!game->exit_main_loop) {
            apply_events(game);
            com = get_and_free_input(com);
        }

        show_map();
    }

    free_snake(head);
    free_command(com);
    remove_all_events(game);
    free_map_stack(game->map_stack);
    
    return EXIT_SUCCESS;
}
