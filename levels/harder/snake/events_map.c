#include "events_map.h"

bool no_apple = true;
int apple_id = 0;

void update_snake(game_instance *game) {
    map * m = current_map(game->map_stack);
    
    int y = m->player_y;
    int x = m->player_x;
    // snake ate itself, bad
    if (m->tiles[y][x]
            && m->tiles[y][x]->category == C_NPC
            && m->tiles[y][x]->id != head->body->id) {
        printf("You chomped yourself!\n");
        level_failed(game, 1);
        return;
    }

    // snake ate apple, good
    if (m->tiles[y][x]
            && m->tiles[y][x]->category == C_ITEM) {
        m->tiles[y][x]->category = C_NPC;
        snake * new_head = malloc(sizeof(snake));
        new_head->body = m->tiles[y][x];
        new_head->next = head;
        head = new_head;
        no_apple = true;
        return;
    }

    // snake moved
    snake * pred = NULL;
    snake * tail = head;
    while (tail->next) {
        pred = tail;
        tail = tail->next;
    }
    tail->next = head;
    pred->next = NULL;
    head = tail;
    
    for(int yp = 0; yp < m->height; yp++) {
        for(int xp = 0; xp < m->width; xp++) {
            if (m->tiles[yp][xp]
            && m->tiles[yp][xp]->id != head->body->id) {
                m->tiles[yp][xp] = NULL;
                m->tiles[y][x] = head->body;
                return;
            }
        }
    }
}


void random_apple(map* m, int* ya, int* xa) {
    *ya = rand() % m->height;
    *xa = rand() % m->width;
}

void spawn_apple(game_instance *game) {
    if (!no_apple)
        return;
    
    map * m = current_map(game->map_stack);
    int ya, xa;
    random_apple(m, &ya, &xa);

    while (m->tiles[ya][xa]) {
        random_apple(m, &ya, &xa);
    }
    
    m->tiles[ya][xa] = malloc(sizeof(entity));
    m->tiles[ya][xa]->category = C_ITEM;
    #ifdef BUG2
    m->tiles[ya][xa]->id = apple_id++;
    #else
    m->tiles[ya][xa]->id = apple_id++ + (m->width * m->height);
    #endif
    #ifndef BUG
    no_apple = false;
    #endif
}
