#include "custom_map.h"
#include <cstdlib>
#include <stdio.h>

extern int finish;


const char *str_map =
"\
ssssssssssss@\n\
sssssssssssss\n\
sssssssssssss\n\
sssssssssssss\n\
sssssssssssss\n\
sssssssssssss\n\
>ssssssssssss\n\
";

extern int end_reached;


// look for value in the array list and return 1 if NOT found.
int value_not_in_list(int value, int * list, int len){
    for (int i = 0; i<len; i++){
        if(list[i] == value){
            return 0;
        }
    }
    return 1;
}


//use in the end to show the map with the bomb.
void show_map_bombs(game_instance* game){
    map * m = current_map(game->map_stack);

    for (int y = 0; y < m->height; y++) {
        for (int x = 0; x < m->width; x++) {
            entity * e = m->tiles[y][x];
            // the entities are displayed in front of tiles
            if (e){
                if (e->category == PLAYER) {
                    switch (m->player_direction) {
                        case DIR_UP:
                            printf("^");
                            break;
                        case DIR_DOWN:
                            printf("v");
                            break;
                        case DIR_LEFT:
                            printf("<");
                            break;
                        case DIR_RIGHT:
                            printf(">");
                            break;
                        default:
                            printf("?");
                            break;
                    }
                } else if (e->category == C_EXIT) {
                    printf("@");
                } else if (has_property(e->properties, ACTIVABLE)) {
                    if (*((int*) e->stats) == ACTIVATED)
                        printf("O");
                    else
                        printf("X");
                } else if (e->category == WALL) {
                    printf("#");
                } else if (e->category == DOOR){
                    printf("[");
                } else { // unknown
                    printf("!");
                    printf("\nUNKNOWN\n");
                }  
            // tiles are displayed last
            } else {
                tile t = m->floor[y][x];
                if (t.category == WATER)
                    printf("~");
                else if (t.category == GRASS)
                    printf(" ");
                
                else if (t.category == TARGET)
                    printf("T");
                else
                    printf("§"); // empty or unknown
            }
            printf(" ");
        }
        printf("\n");
    }
}


//return the starting and ending indexes to browse all the tiles next to (y,x), and return the number of neighbours.
void return_position_8(int * i_start, int * i_end, int * j_start, int * j_end , int y, int x, int * nb_neighbours){
    map * m = current_map();
    if (y == m->height-1){
        if (x == m->width-1){   // bottom right corner -> 3 neighbours
            *i_start = -1; *i_end = 0;
            *j_start = -1; *j_end = 0;
            *nb_neighbours = 3;
        } else if (x == 0){     // bottom left corner -> 3 neighbours
            *i_start = -1; *i_end = 0;
            *j_start = 0; *j_end = 1;
            *nb_neighbours = 3;
        } else {                // bottom line -> 5 neighbours
            *i_start = -1; *i_end = 0;
            *j_start = -1; *j_end = 1;
            *nb_neighbours = 5;
        }

    } else if (y == 0){
        if (x == m->width-1){   // top right corner ->3 neighbours
            *i_start = 0; *i_end = 1;
            *j_start = -1; *j_end = 0;
            *nb_neighbours = 3;
        } else if (x == 0){     // top left corner-> 3 voisins
            *i_start = 0; *i_end = 1;
            *j_start = 0; *j_end = 1;
            *nb_neighbours = 3;
        } else {                // top line -> 5 neighbours
            *i_start = 0; *i_end = 1;
            *j_start = -1; *j_end = 1;
            *nb_neighbours = 5;
        }
        
    } else {
        if (x == m->width-1){   // right column -> 5 neighbours
            *i_start = -1; *i_end = 1;
            *j_start = -1; *j_end = 0;            
            *nb_neighbours = 5;
        } else if (x == 0){     // left column -> 5 neighbours
            *i_start = -1; *i_end = 1;
            *j_start = 0; *j_end = 1;
            *nb_neighbours = 5;
        } else {                // in the middle -> 8 neighbours
            #ifdef index_error
            *i_start = -2; *i_end = 2;
            *j_start = -2; *j_end = 2;
            #else
            *i_start = -1; *i_end = 1;
            *j_start = -1; *j_end = 1;
            #endif
            *nb_neighbours = 8;
        }
    }
}

// initialize the list of indexes next to player but without the tiles in the corner.
int zone_next_to_player(map * m, int * zones){
    int x = m->player_x;
    int y = m->player_y;
    int i_start, i_end, j_start, j_end, nb;
    return_position_8(&i_start, &i_end, &j_start, &j_end, y, x, &nb);
    int len = 0;
    for (int i=i_start; i<=i_end; i++){
        for (int j=j_start; j<=j_end; j++){
            if (j != 0 || i != 0){
                int k = coord_idx(y+i,x+j);
                if (!(k>= m->height*m->width)&& !(k<0)){
                    zones[len] = k;
                    len++;
                }
            }
        }
    }
    return nb;
}

// pick random indexes and put bombs there.
void choose_index(int nb_bombs, map * m) {
    #ifndef srand_while
    srand(time(NULL));
    #endif
    int x, y, index;
    int * picked = (int *) malloc(sizeof(int)* nb_bombs);
    int * zones = (int *) malloc(sizeof(int)*8);
    int nb_neighbours = zone_next_to_player(m, zones);
    int i = 0;
    if (nb_bombs > m->width*m->height - 2 - nb_neighbours){ // -2 for the player and the flag and -number of tiles next to player.
        nb_bombs = m->width*m->height - 2 - nb_neighbours;
    }
    while (i != nb_bombs){
        #ifdef srand_while
        srand(time(NULL));
        #endif
        x = rand() % m->width;
        y = rand() % m->height;
        index = coord_idx(y, x);
        if (m->entities[index] && m->entities[index]->category == SAND){
            if (value_not_in_list(index, picked, i) && value_not_in_list(index, zones, nb_neighbours)){
                picked[i] = index;
                m->entities[index]->stats[1] = 1;
                i++;
            }
        }
    }
}

// return true if it finds a way between player and flag.
int way_exists_rec(int i_start, int i_end, int j_start, int j_end, map * m, int y, int x, int * visited, int * len){
    for (int i=i_start; i<=i_end; i++){
        for (int j=j_start; j<=j_end; j++){
            if (i==0 || j==0){
                int k = coord_idx(y+i,x+j);
                if (value_not_in_list(k, visited, *len)){
                    if (!(k>= m->height*m->width)&& !(k<0) && m->entities[k] && m->entities[k]->category == FLAG){
                        visited[*len] = k;
                        *len += 1;
                        return 1;
                    } else if (!(k>= m->height*m->width)&& !(k<0) && m->entities[k] && m->entities[k]->category == SAND && m->entities[k]->stats[1] == 0){
                        visited[*len] = k;
                        *len += 1;
                        int ok = way_exists(m, y+i, x+j, visited, len);
                        if (ok)
                            return 1;
                    }
                }
            }
        }
    }
    return 0;
}

int way_exists(map * m, int y, int x, int * visited, int * len){
    int i_start, i_end, j_start, j_end, nb;
    return_position_8(&i_start, &i_end, &j_start, &j_end, y, x, &nb);
    return way_exists_rec(i_start,i_end,j_start,j_end,m,y,x, visited, len);    
}

// clear the bombs from the map
void clear_map(map * m){
    for (int i = 0; i < m->height; i++){
        for (int j = 0; j < m->width; j++){
            int k = coord_idx(i,j);
            if (m->entities[k] && m->entities[k]->category == SAND){
                m->entities[k]->stats[1] = 0;
            }
        }
    }
}

// increment the counter if there is a bomb next in the 8 tiles next to tile (y,x).
void increment_bombs_counter(int i_start, int i_end, int j_start, int j_end, int y, int x, map * m){
    #ifdef less_or_equal
    for (int i=i_start; i<i_end; i++){
        for (int j=j_start; j<j_end; j++){
    #else
    for (int i=i_start; i<=i_end; i++){
        for (int j=j_start; j<=j_end; j++){
    #endif
            int k = coord_idx(y+i,x+j);
            if (!(k>= m->height*m->width) && !(k<0) && m->entities[k] && m->entities[k]->category==SAND && m->entities[k]->stats && m->entities[k]->stats[1]==0){
                m->entities[k]->stats[2]++;
            }
        }
    }
}

// initialize the third information of each sand tile : the number of bombs next to the tile.
void count_close_bombs(map *m){
    int i_start, i_end, j_start, j_end, nb;
    for (int y=0; y<m->height; y++){
        for (int x=0; x<m->width; x++){
            int k = coord_idx(y,x);
            return_position_8(&i_start, &i_end, &j_start, &j_end, y, x, &nb);
            if (!(k>= m->height*m->width) && !(k<0) && m->entities[k] && m->entities[k]->category == SAND && m->entities[k]->stats && m->entities[k]->stats[1]==1){
                increment_bombs_counter(i_start, i_end, j_start, j_end, y, x, m);
            }
        }
    }
}

// create bombs
void init_map(map * m) {
    clear_map(m);
    #ifndef error_macro
    int ok = 0;
    while (!ok){
    #endif
        int x = m->width, y = m->height;
        int nb_bombs = (int) FILL_RATIO(x*y+1, 0.15); // number of bombs in total
        choose_index(nb_bombs, m); // set the bombs
        #ifndef error_macro
        int * visited = (int * ) malloc(sizeof(int)*x*y*5);
        int len = 0;
        #endif
        count_close_bombs(m);
        #ifndef error_macro
        if (way_exists(m, m->player_y, m->player_x, visited, &len)){ // verify if a way exists between the player and the flag
            ok = 1;
        } else{
            clear_map(m); //if there is no way : we start from the beginning.
        }
    }
    #endif
}
