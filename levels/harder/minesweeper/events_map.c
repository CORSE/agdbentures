#include "events_map.h"

extern int end_reached;

void verify_exit(void) {
    map * m = current_map();
    int i = player_idx();
    if (m->entities[i] && find_in_stack(m->player_y, m->player_x, FLAG)) {
        printf("You reached the exit!\n");
        end_reached = 1;
        level_success();
    }
}

//verify if the player walked or dug on a bomb.
void verify_bomb(void) {
    map * m = current_map();
    int i = coord_idx(m->player_y,m->player_x);
    entity * e = find_in_stack(m->player_y, m->player_x, SAND);
    if (m->entities[i] && e && e->stats[1]==1){
        printf("BOOM ! You  walked on a bomb and it exploded ! Too bad.\n");
        end_reached = 1;
        level_failed();
    }
    for (int y = 0; y< m->height; y++){
        for (int x = 0; x< m->width; x++){
            int i = coord_idx(y,x);
            if (m->entities[i] && m->entities[i]->category==SAND && m->entities[i]->stats[0] == DEACTIVATED && m->entities[i]->stats[1] == 1){
                printf("BOOM ! You  dug on a bomb and it exploded ! Too bad.\n");
                end_reached = 1;
                level_failed();
            }
        }
    }
}
