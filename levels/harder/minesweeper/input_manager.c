#include "input_manager.h"

void dig_zones_besides(int i_start, int i_end, int j_start, int j_end, int y, int x){
    map * m = current_map();
    for (int i=i_start; i<=i_end; i++){
        for (int j=j_start; j<=j_end; j++){
            int k = coord_idx(y+i,x+j);
            if (m->entities[k] && m->entities[k]->stats && m->entities[k]->stats[0]==ACTIVATED && m->entities[k]->stats[1]==0 ){
                m->entities[k]->stats[0]=DEACTIVATED;
                int nb;
                int i_start2, i_end2, j_start2, j_end2, nb_neighbours;
                return_position_8(&i_start2, &i_end2, &j_start2, &j_end2, y+i, x+j, &nb_neighbours);
                nb = m->entities[k]->stats[2];
                if (nb == 0)
                    dig_zones_besides(i_start2, i_end2, j_start2, j_end2, y+i, x+j);
            }
        }
    }
}


void dig(void) {
    map * m = current_map();
    int x = m->player_x, y = m->player_y;
    direction d = m->player_direction;
    if ((x == m->width-1 && d==RIGHT) || (x ==0 && d==LEFT) || (y == m->height-1 && d==DOWN) || (y == 0 && d==UP)){
        printf("There is nothing to dig here.\n");
        return;
    }
    int i;
    
    // calculate the position to dig.
    switch(d){
        case UP : y = y-1; break;
        case DOWN : y = y+1; break;
        case RIGHT : x = x+1; break;
        case LEFT : x = x - 1; break;
        default : break;
    }

    i = coord_idx(y,x);
    if (m->entities[i] && m->entities[i]->category==SAND && m->entities[i]->stats[0]==ACTIVATED){
        m->entities[i]->stats[0]=DEACTIVATED;
        int i_start, i_end, j_start, j_end, nb;
        return_position_8(&i_start, &i_end, &j_start, &j_end, y, x, &nb);
        int k = coord_idx(y,x);
        nb = m->entities[k]->stats[2];
        if (nb == 0)
            dig_zones_besides(i_start,i_end, j_start,j_end,y, x);
    }
    else{
        printf("There is nothing to dig here.\n");
    }
}



void c_apply_input(command * c) {
    map * m = current_map();
    if (!strcmp(c->command_buffer, "DIG")) {
        dig();
    } else {
        apply_input(c, m->player_y, m->player_x) ;
    }
}
