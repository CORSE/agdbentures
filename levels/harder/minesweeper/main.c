/** @AGDB
 * level_title: Démineur
 * program_name: main
 * engine_name:events
 * available_commands: next step continue
 * map_height: 7
 * map_width: 10
 *
 * alias: map_array mstack.maps[mstack.length - 1].floor
 * alias: exit_x    mstack.maps[mstack.length - 1].exit_x
 * alias: exit_y    mstack.maps[mstack.length - 1].exit_y
 * alias: left  4
 * alias: right 6
 * alias: up    8
 * alias: down  2
 *
 * watch: player_direction player_x player_y
 * track: load_map:mstack load_map:player_x load_map:player_y
 * 
 *
 * engine: fullgame
 * 
 */

/**
 * Entry point of the game. Manages the main loop, and apart from
 * that only calls functions.
 * 
 * Initialization:
 *  1) load the map and possible items
 *  2) load all events
 *  3) initialize the player's position and data
 *  4) start the main loop
 * 
 * Main loop:
 *  1) apply the player's input (move or action)
 *  2) observe triggers, apply each resolution function or applying triggers
 *  3) read the next input (file) or wait for the next frame and buffer an input during the wait (real time)
 * 
 * Endgame:
 *  1) receive signal from level_success or level_failure
 *  2) free all allocated memory
 *  3) exit properly
 * 
 * Game design related tip:
 *   If you want to create a game, note all your ideas, they may serve later.
 *   Video game is an art as cinema or literature: play lots of games to get a solid culture.
 *   But beside game design, several fields exist in video games creation:
 *      - level design
 *      - storytelling and writing
 *      - music and sound effets
 *      - worldbuilding
 *      - ... and many others!
*/

#include<unistd.h>
#include<signal.h>

#include "engine/agdbentures.h"
#include "events_map.h"
#include "input_manager.h"
#include "custom_map.h"

#define NB_SEC 14

int end_reached = 0;
game_instance* signal_game = NULL;

void sig_handler(int signum){
    signum++;
    printf("Too late ! You waited too long to choose a command.\n");
    level_failed(signal_game, 1);
}

int main(int argc, char ** argv) {
    printf("Manual mode. Available commands: UP, DOWN, LEFT, RIGHT and *_N variants.\nUse Ctrl+C to quit.\n");

    game_instance* game = init_game(7, 10);
    map* m = load_map("overworld", str_map);
    push_on_stack(game->map_stack, m);

    add_event((void*)verify_exit, NULL);
    add_event((void*)verify_bomb, NULL);

    // Setting signal global argument
    signal_game = game;
    signal(SIGALRM,sig_handler); // Register signal handler
    alarm(NB_SEC);
    
    show_map();

    
    command * com = get_next_command();

    // The Holy Main Loop
    while (!game->exit_main_loop) {
        c_apply_input(com);

        /* since the events may be applied if the input is FORWARD_N, 
           we need to check to avoid triggering the same event twice*/
        if (!game->exit_main_loop)
            apply_events(game);

        if(!end_reached) {
            show_map();
        }        
        else {
            show_map_bombs();
        }

        if (!game->exit_main_loop){
            signal(SIGALRM,sig_handler); // Register signal handler
            alarm(NB_SEC);
            com = get_and_free_input(com);
        }
    }

    free_command(com);
    remove_all_events(game);
    free_game(game);

    return EXIT_SUCCESS;
}
