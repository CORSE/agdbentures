#ifndef EVENTS_MAP_H
#define EVENTS_MAP_H

#include "custom_map.h"
#include "engine/agdbentures.h"

// Vérifie si le joueur se trouve sur la case de victoire
void verify_exit(void);

// Vérifie si le joueur a marché ou détérré une bombe
void verify_bomb(void);

#endif // EVENTS_MAP_H
