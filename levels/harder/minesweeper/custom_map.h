#ifndef CUSTOM_MAP_H
#define CUSTOM_MAP_H

#include <stdlib.h>
#include <time.h>

#include "engine/agdbentures.h"

#ifdef error_macro
#define FILL_RATIO(a, b) a *b
#else
#define FILL_RATIO(a, b) ((a) * (b))
#endif

extern const char *str_map;

// return true if value is not in list
int value_not_in_list(int value, int *list, int len);

void return_position_8(int *i_start, int *i_end, int *j_start, int *j_end,
                       int x, int y, int *nb_neighbours);

// return the zones next to the player's positions
int zone_next_to_player(map *m, int *zones);

// pick index to initialize the bombs.
void choose_index(int nb_bombs, map *m);

// return true if a way exists between the player and the flag.
int way_exists_rec(int i_start, int i_end, int j_start, int j_end, map *m,
                   int y, int x, int *visited, int *len);
int way_exists(map *m, int y, int x, int *visited, int *len);

// initialize the bombs.
void count_close_bombs(map *m);
void init_map(map *m);
void show_map_bombs();

#endif // CUSTOM_MAP_H
