#include "input_manager.h"
#include "string.h"

void apply_inputs(command * c) {
    map * m = current_map();
    if (!strcmp(c->command_buffer, "UP")) {
        up(m->player_y, m->player_x);
    } else if (!strcmp(c->command_buffer, "UP_N")) {
        up_n(m->player_y, m->player_x,atoi(c->args[0]));
    } else if (!strcmp(c->command_buffer, "DOWN")) {
        down(m->player_y, m->player_x);
    } else if (!strcmp(c->command_buffer, "DOWN_N")) {
        down_n(m->player_y, m->player_x,atoi(c->args[0]));
    } else if (!strcmp(c->command_buffer, "LEFT")) {
        left(m->player_y, m->player_x);
    } else if (!strcmp(c->command_buffer, "LEFT_N")) {
        left_n(m->player_y, m->player_x,atoi(c->args[0]));
    } else if (!strcmp(c->command_buffer, "RIGHT")) {
        right(m->player_y, m->player_x);
    } else if (!strcmp(c->command_buffer, "RIGHT_N")) {
        right_n(m->player_y, m->player_x,atoi(c->args[0]));
    }else {
        printf("Unknown input command.\n");
    }
}