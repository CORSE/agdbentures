#ifndef EVENTS_MAP_H
#define EVENTS_MAP_H

#include "../../engines/fullgame/agdbentures.h"
#include "custom_map.h"

// Vérifie si le joueur se trouve sur la case de victoire
void verify_exit(void);

void set_secret_door();

#endif // EVENTS_MAP_H
