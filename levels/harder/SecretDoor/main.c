/* @AGDB
 * level_title: SecretDoor
 * engine_name: fullgame
 * exec_name: secretdoor
 * engine_tags:
 * engine_antitags:
 */

#include <stdlib.h>
#include <stdio.h>

#include "../../engines/fullgame/agdbentures.h"
#include "events_map.h"
#include "custom_map.h"

#define MAX_DOOR_DIGITS 5

int execute_loop(bool show_m){

  map* m = current_map();
  command * com = get_next_command();
  exit_main_loop = false;
  

  while (!exit_main_loop) {
    if(!apply_input(com,m->player_y,m->player_x)){
      level_failed();
      return 1;
    }

    if (show_m && !exit_main_loop)
      show_map();

    /* since the events may be applied if the input is FORWARD_N, 
    we need to check to avoid triggering the same event twice  */
    if (!exit_main_loop)
      apply_events();

    if (!exit_main_loop)
      com = get_and_free_input(com);

    // plus aucun input mais on a pas encore gagné ou perdu -> défaite
    if (com == NULL && !exit_main_loop){
      level_failed();
      free_command(com);
      return EXIT_FAILURE;
      }
    }
  free_command(com);
  return EXIT_SUCCESS;
}

int main(){
  printf("========================\n");
  printf("Welcome to Secret Door !\n");
  printf("========================\n");

  bool show_m = true;

  init_map_stack();

  add_event(verify_exit,NULL);

  load_map("first floor", str_floor1);
  
  set_secret_door();
  show_map();
  
  char user_door[MAX_DOOR_DIGITS];

  printf("Only one of the three doors leads to the exit.\nWhich one do you choose ? : "); fflush(stdout);
  fgets(user_door, MAX_DOOR_DIGITS, stdin);

  char * pointer;
  pointer = strchr( user_door , '\n' );
  *pointer = '\0';

  printf("You have chosen the door %s\n",user_door);

#ifdef BUG
  switch (*user_door){
#else
  char * endPtr;
  switch(strtol( user_door, &endPtr, 10 )){
#endif
    case 49:
      init_inputs_file("inputs_door_49.txt");
      break;
    case 1:
      init_inputs_file("inputs_door_1.txt");
      break;
    case 4:
      init_inputs_file("inputs_door_4.txt");
      break;
    default:
      printf("Error : door %s does not exist !\n",user_door);
      return EXIT_FAILURE;
  }
 
  if (execute_loop(show_m)){
    printf("Door %s is apparently closed :(\n",user_door);
  }else{
    printf("Congratulation ! Door %s was the secret door ;)\n",user_door);
  }
  remove_all_events();
  free_map_stack();
  close_inputs_file();

  return EXIT_SUCCESS;
}