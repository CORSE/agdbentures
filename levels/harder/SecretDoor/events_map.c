#include "events_map.h"
#include "stdbool.h"

void verify_exit(void) {

    map * m = current_map();
    int i = player_idx();

    if (m->entities[i] && find_in_stack(m->player_y,m->player_x,FLAG)) {
        printf("You reached the exit !\n");
        level_success();
    }
}

void set_secret_door(){
    current_map()->floor[coord_idx(1, 6)]->obstacle = false;
}