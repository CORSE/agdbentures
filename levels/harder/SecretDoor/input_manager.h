#ifndef INPUT_MANAGER_H
#define INPUT_MANAGER_H

#include "../../engines/fullgame/agdbentures.h"

void apply_inputs(command * c);

#endif // INPUT_MANAGER_H