#include "custom_map.h"
#include "input_manager.h"


const char *str_map1 =
"\
+---------------+\n\
|     M        @|\n\
| M             |\n\
|       M       |\n\
|             M |\n\
| --------------|\n\
|  >|  |  |  |  |\n\
+---------------+\n\
";

//5 UP 12 RIGHT


const char *str_map2 =
"\
+---------------+\n\
|>              |\n\
|               |\n\
|               |\n\
|     M   M     |\n\
|       @       |\n\
|     M   M     |\n\
+---------------+\n\
";

// 4 DOWN    7 RIGHT

const char *str_map3 = // the monster can move two tiles for 1 movement of the player + follow him
"\
+---------------+\n\
|               |\n\
|M| >           |\n\
| |             |\n\
|               |\n\
|           | | |\n\
|           +-+ |\n\
|   @           |\n\
+---------------+\n\
";

// initialize the monsters : first stat : id, direction and coordinates
monsters* init_monsters(map * m, monsters* monsters_data){
    monsters_data->length = 0;
    for (int y = 0; y < m->height; y++)
    {
        for(int x = 0; x<m->width; x++){
            if (get_entity(m, y, x) == 'M') {;
                monsters_data->info[monsters_data->length].id = monsters_data->length;
                monsters_data->info[monsters_data->length].dir = DIR_DOWN;
                monsters_data->info[monsters_data->length].x = x;
                monsters_data->info[monsters_data->length].y = y;
                monsters_data->info[monsters_data->length].player_seen = false;
                monsters_data->length++;
            }
        }
    }
    
    for (int i = monsters_data->length; i < MAX_MONSTERS; i++)
    {
        monsters_data->info[i].id = -1;
    }
    
    return monsters_data;
}

void update_monster(monsters* mon, map* m, int fy, int fx, int ty, int tx) {
    if (get_entity(m, fy, fx) == 'M' && can_move_to(m, fy, fx, ty, tx)) {
        move_without_check(m, fy, fx, ty, tx);
        int id = search_monster(mon, fy, fx);
        if (fx < tx)
            mon->info[id].dir = DIR_RIGHT;
        else if (fx>tx)
            mon->info[id].dir = DIR_LEFT;
        else if (fy<ty)
            mon->info[id].dir = DIR_DOWN;
        else
            mon->info[id].dir = DIR_UP;

        mon->info[id].x = tx;
        mon->info[id].y = ty;        
    }
}

void verify_monster(game_instance* game) {
    map *m = current_map(game->map_stack);
    if (get_entity(m, m->player_y, m->player_x) == 'M') {
        message("A monster catches you !");
        exit(EXIT_FAILURE);
    }
}