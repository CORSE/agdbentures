/** @AGDB
 * level_title: Avoid Monster
 * program_name: avoid_monster
 * exec_name: avoid_monster
 * engine_name: map_stack
 * available_commands: edit next step continue interrupt
 *
 * interactive: True
 * verify_exit: cond exit_x == 4
 * verify_exit: cond exit_y == 7
 * verify_always: must_call init_monsters
 * verify_always: must_call move_monsters
 * verify_always: must_call verify_monster
 * verify_always: must_call is_player_seen
 * 
 * WOP: messageFR intro
 * Enfuis toi de cette prison, j'ai récupéré cette clé sur un monstre,
 * prends là et échappe toi !
 * Fais attention aux monstres, ils sont très dangereux !
 * S'ils te vois, ils te poursuivront jusqu'à ce qu'ils t'attrapent !
 * EndOfMessage
 * 
 * WOP: messageEN intro
 * Escape from this prison, I got this key off a monster, take it and escape!
 * Watch out for monsters, they're very dangerous!
 * If they see you, they'll chase you until they catch you!
 * EndOfMessage
 * 
 * arcade_maps: main first_room.tmx
 * arcade_maps: second_room second_room.tmx
 * arcade_maps: third_room third_room.tmx
 */ 

#include "engine/agdbentures.h"
#include "custom_map.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int exit_x ;
int exit_y ;

extern int win, defeat;

void init_maps(game_instance* game) {
    char* head_names[3] = {"third", "second", "first"};
    char *tail_name = "_room";
    char name[15];
    for (int i = 0; i < 3; i++) {
        strcpy(name, head_names[i]);
        strcat(name, tail_name);
        map *m = load_map(name, i == 0 ? str_map3 : i == 1 ? str_map2 : str_map1);
        push_on_stack(game->map_stack, m);
    }
}

void execute_loop(game_instance* game, monsters* mon) {
    int dest_y, dest_x;
    map* room_map = current_map(game->map_stack);
    // inputs loop
    while (!game->exit_main_loop) {
        // read command
        command * input_command = get_next_command();
        char* command = input_command->command_buffer;
        direction dir = direction_from_string(command);
        // if the direction is meaningful try to move
        if (dir != DIR_UNKNOWN) {
            // Lookup for the predicted destination
            room_map->player_direction = dir;
            player_look_forward(room_map, &dest_y, &dest_x);
        
            // If the destination is empty, go there
                if (get_entity(room_map, dest_y, dest_x) == ' ') {
                    move_player(room_map, dest_y, dest_x, dir);
                // if there's a monster, player loses
                } else if (get_entity(room_map, dest_y, dest_x) == '-' || get_entity(room_map, dest_y, dest_x) == '|' || get_entity(room_map, dest_y, dest_x) == '+') {
                    continue;
                } else if (get_entity(room_map, dest_y, dest_x) == 'M') {
                    move_player(room_map, dest_y, dest_x, dir);
                    message("The monster catches you!");
                    exit(EXIT_FAILURE);
                // if player reaches the exit, exit the loop
                } else if (get_entity(room_map, dest_y, dest_x) == '@') {
                    printf("You reached the exit!\n");
                    move_player(room_map, dest_y, dest_x, dir);
                    game->exit_main_loop = true;
                    break;
                } 
        } else if (!strcmp(command,"W") || !strcmp(command,"w") || !strcmp(command,"WAIT") || !strcmp(command,"wait")) {
            message("You stand still and wait"); // wait one turn
        } else {
            printf("Unknown command %s\n", command);
            continue;
        }
        printf("player x=%d y=%d\n", room_map->player_x, room_map->player_y);
        move_monsters(mon, room_map);
        verify_monster(game);
        show_map(room_map);
    }
}

void change_the_map(game_instance* game, int i) {
    pop_from_stack(game->map_stack);
    game->exit_main_loop = false;
    if (i == 2) {
        exit_x = 4;
        exit_y = 7;
    }
}

int main() {
    game_instance* game = init_game();
    monsters* monsters_data = malloc(sizeof(monsters));
    map *room;
    init_maps(game);
    message("You have to avoid the monsters using the directional arrows and W(ait).");
    
    // rooms loop 
    for (int i = 0; i < 3; i++) {
        room = current_map(game->map_stack);
        init_monsters(room, monsters_data);
        printf("Room n°%d\n", i+1);
        execute_loop(game, monsters_data);
        change_the_map(game, i);
    }

    free(monsters_data);
    free_map_stack(game->map_stack);

    verify_exit(room);
}
