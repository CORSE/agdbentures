#ifndef CUSTOM_MAP_H
#define CUSTOM_MAP_H

#define MAX_MONSTERS 4

#include "engine/agdbentures.h"

extern const char * str_map1;
extern const char * str_map2;
extern const char * str_map3;

typedef struct monster_s {
    int id;
    int x;
    int y;
    direction dir;
    bool player_seen;
} monster;

typedef struct monsters_s {
    monster info[MAX_MONSTERS];
    int length;
} monsters;

monsters* init_monsters(map *m, monsters* monsters_data);

void update_monster(monsters *mon, map *m, int fy, int fx, int ty, int tx);

void verify_monster(game_instance* game);

#endif // CUSTOM_MAP_H
