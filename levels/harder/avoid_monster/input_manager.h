#ifndef INPUT_MANAGER_H
#define INPUT_MANAGER_H

#include "engine/agdbentures.h"
#include "custom_map.h"
#include <unistd.h>

//return 1 if the value given in parameter is in the list.
int in_list(int value, int *list, int len);

// apply the movement from (y,x) and to direction d1 first then d2 if possible.
void make_move(monsters* mon, map *m, int y, int x, direction d1, direction d2);

// verify if a move is possible and apply it if so
int ok_move(monsters* mon, map *m, int y, int x, direction d1, direction d2);

// move the monster to the right or the left depending on the player's position
int move_up_or_down(monsters* mon, map *m, int y_monster, int x_monster, int y_player);

// move the monster to the right or the left depending on the player's position
int move_left_or_right(monsters* mon, map *m, int y_monster, int x_monster, int x_player);

// move the monster to one of the four corners
int move_to_corner(monsters* mon, map *m, int y_monster, int x_monster, int y_player, int x_player);

// follow movement (3rd monster pattern)
int follow_player(monsters* mon, map *m, int *y, int *x);

// circle movement (2nd monster pattern)
void circle(monsters* mon, map *m, int y, int x);

// come and go movement (1st monster pattern)
void come_and_go(monsters *mon, map *m, int y, int x);

// search for the monster at coordinates (x,y) and return its id or -1
int search_monster(monsters* mon, int y, int x);

// move every monster once and verify not to move twice a monster.
void move_monsters(monsters *mon, map *m);

// examine la commande c et appelle les fonctions d'action ou de déplacement correspondantes
// void parse_input(game_instance* game, monsters* mon, command * c);


#endif