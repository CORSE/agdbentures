#!/usr/bin/env python3

from level.level_abc import AbstractLevel
import graphic.constants as cst
import arcade
from level.action import Action
from language import lmessage, Lang


class Level(AbstractLevel):
    """avoid_monster"""

    def reset_skeletons(self, max):
        skeletons = []
        for i in range(max):
            skeletons.append(self.world.get_object(f"skeleton_{i}"))
        for skeleton in skeletons:
            payload = {
                "topic": "object_update",
                "action": Action("change_color"),
                "object_name": skeleton.name,
                "color": arcade.color.WHITE,
            }
            skeleton.send_update(payload)

            skeleton.visible = False
            skeleton.send_update()

    def arcade_custom_restart(self):

        self.reset_skeletons(4)

        if self.door_open:
            payload = {
                "topic": "sprites",
                "action": Action("hide"),
                "layer": "decorations",
                "locations": [(1, 5), (1, 4)],
            }
            self.send_to_gui(payload)
            self.door_open = False
        self.arcade_custom_first_start()

    def arcade_custom_first_start(self):
        player = self.world.get_player()
        wop = self.world.get_wop()
        wop.visible = True
        wop.place_at(6, 6)
        wop.direction = cst.Direction.LEFT
        wop.send_update()

        self.i = 0
        self.world.exit.place_at(-1, -1)
        self.world.exit.visible = False
        self.world.exit.send_update()

        skeletons = []
        for m in range(4):
            skeleton = self.world.get_object(f"skeleton_{m}")

            skeleton.var_x = ("monsters_data", f"->info[{m}].x")
            skeleton.var_y = ("monsters_data", f"->info[{m}].y")
            skeleton.var_dir = ("monsters_data", f"->info[{m}].dir")

            skeletons.append(skeleton)

        self.door_open = False
        self.wop_spawn = False
        self.current_map = "main"
        self.tag_map = ['f', 'i', 'r', 's', 't', '_', 'r', 'o', 'o', 'm', '\x00']

        def change_map(map_name):
            self.current_map = map_name
            payload = {
                "topic": "map_change",
                "map": map_name,
            }
            self.send_to_gui(payload)
            self.tag_map = "first_room"
            if map_name == "third_room":
                exit = self.world.get_object("exit")
                exit.place_at(4, 7)
                exit.visible = True
                exit.send_update()
                payload = {"topic": "player_update", "action": Action("rescale")}
                self.player.send_update(payload)
                self.reset_skeletons(1)
            else:
                self.reset_skeletons(4)

        def detect_map_change():
            if wop.visible:
                wop.visible = False
                wop.send_update()

            map_name = "third_room"
            if self.tag_map == [
                'f',
                'i',
                'r',
                's',
                't',
                '_',
                'r',
                'o',
                'o',
                'm',
                '\x00',
            ]:
                map_name = "second_room"

            change_map(map_name)

        def to_direction(direction):
            if direction == "DIR_UP":
                return cst.Direction.UP
            elif direction == "DIR_DOWN":
                return cst.Direction.DOWN
            elif direction == "DIR_LEFT":
                return cst.Direction.LEFT
            elif direction == "DIR_RIGHT":
                return cst.Direction.RIGHT

        def update_monster(monster):
            skeletons[monster.id].visible = True
            skeletons[monster.id].direction = to_direction(monster.dir)
            skeletons[monster.id].place_at(monster.x, monster.y)
            skeletons[monster.id].send_update()

            if monster.player_seen:
                payload = {
                    "topic": "object_update",
                    "object_name": f"skeleton_{monster.id}",
                    "action": Action("change_color"),
                    "color": arcade.color.RED,
                }
                self.send_to_gui(payload)

        def post_update(player, memory):

            if not self.wop_spawn and self.i > 1:

                wop.place_at(5, 6)
                wop.send_update()
                wop.talk("intro")

                wop.direction = cst.Direction.RIGHT
                wop.place_at(6, 6)
                wop.send_update()

                wop.direction = cst.Direction.DOWN
                wop.send_update()

                self.wop_spawn = True
            if self.i <= 2:
                self.i += 1
            if not self.door_open:
                payload = {
                    "topic": "sprites",
                    "action": Action("hide"),
                    "layer": "decorations",
                    "locations": [(1, 5), (1, 4)],
                }
                self.send_to_gui(payload)
                self.door_open = True

            monsters = self.world.get_variable("monsters_data")
            try:
                for m in monsters[0].info:
                    if self.current_map == "third_room":
                        if m.id == 0:
                            update_monster(m)
                    else:
                        if m.id != -1:
                            update_monster(m)
            except:
                pass

        player.post_update = post_update
        self.register_leave_function("change_the_map", detect_map_change)

    def pre_validation(self):
        self.checker.append_inputs(
            [
                "LEFT",
                "LEFT",
                "UP",
                "WAIT",
                "WAIT",
                "WAIT",
                "WAIT",
                "WAIT",
                "WAIT",
                "WAIT",
                "WAIT",
                "WAIT",
                "WAIT",
                "UP",
                "RIGHT",
                "RIGHT",
                "RIGHT",
                "RIGHT",
                "RIGHT",
                "RIGHT",
                "RIGHT",
                "RIGHT",
                "RIGHT",
                "RIGHT",
                "UP",
                "UP",
                "UP",
                "RIGHT",
                "RIGHT",
                "RIGHT",
                "RIGHT",
                "DOWN",
                "DOWN",
                "DOWN",
                "DOWN",
                "RIGHT",
                "RIGHT",
                "RIGHT",
                "RIGHT",
                "RIGHT",
                "RIGHT",
                "RIGHT",
                "RIGHT",
                "RIGHT",
                "RIGHT",
                "RIGHT",
                "RIGHT",
                "RIGHT",
                "RIGHT",
                "RIGHT",
                "RIGHT",
                "RIGHT",
                "RIGHT",
                "DOWN",
                "DOWN",
                "WAIT",
                "WAIT",
                "WAIT",
                "WAIT",
                "DOWN",
                "UP",
                "DOWN",
                "DOWN",
                "DOWN",
                "LEFT",
                "LEFT",
                "LEFT",
                "LEFT",
                "LEFT",
                "LEFT",
                "LEFT",
                "LEFT",
                "LEFT",
                "LEFT",
                "LEFT",
            ]
        )

    def post_validation(self):
        pass

    def test(self):
        import tests.lib_test as T

        self.recompile_bug()
        T.send_input("LEFT")
        T.send_input("LEFT")
        T.send_input("UP")
        T.send_input("UP")
        T.send_input("RIGHT")
        T.send_input("RIGHT")
        T.send_input("RIGHT")
        T.send_input("RIGHT")
        T.send_input("RIGHT")
        T.ccontinue()
        T.expect_defeat()
        self.run()
        self.recompile_answer()
        T.send_input("LEFT")
        T.send_input("LEFT")
        T.send_input("UP")
        for _ in range(10):
            T.send_input("WAIT")
        T.send_input("UP")
        for _ in range(10):
            T.send_input("RIGHT")
        T.send_input("UP")
        T.send_input("UP")
        T.send_input("UP")
        T.send_input("RIGHT")
        T.send_input("RIGHT")
        T.send_input("RIGHT")
        T.send_input("RIGHT")
        T.send_input("DOWN")
        T.send_input("DOWN")
        T.send_input("DOWN")
        T.send_input("DOWN")
        for _ in range(18):
            T.send_input("RIGHT")
        T.send_input("DOWN")
        T.send_input("DOWN")
        T.send_input("WAIT")
        T.send_input("WAIT")
        T.send_input("WAIT")
        T.send_input("WAIT")
        T.send_input("DOWN")
        T.send_input("UP")
        T.send_input("DOWN")
        T.send_input("DOWN")
        T.send_input("DOWN")
        for _ in range(11):
            T.send_input("LEFT")
        T.send_input("LEFT")
        T.unexpect_sub_strings("The monster catches you !")
        T.ccontinue()
        T.expect_victory()
        self.run()
