#include "custom_map.h"
#include "engine/action.h"

const char *str_map =
"\
+- ~~~~~~  -+\n\
   ~~~~~~    \n\
   ~~~~~~X   \n\
   ~~~~~~    \n\
   ~~~~~~    \n\
>  ~~~~~~  @ \n\
   ~~~~~~    \n\
   ~~~~~~    \n\
   ~~~~~~    \n\
 +X~~~~~~    \n\
+- ~~~~~~  -+\n\
";

// itialize map
void init_switches(map * m) {
    int n = 0; // to differentiate the switches
    for (int y = 0; y < m->height; y++) {
        for (int x = 0; x < m->width; x++) {
            entity e = get_entity(m,y,x);
            if (e && has_property(e->properties, ACTIVABLE)) {
                int* stats = (int*) malloc(sizeof(int));
                *stats = DEACTIVATED;
                e->stats = stats;
                e->id = n;
                m->floor[y][x] = create_wall();
                n++;
            }
        }
    }
}
