#include "input_manager.h"
#include "engine/entity.h"
#include "engine/map.h"

// when the switch is activated the bridge appears.
void bridge_appear(game_instance* game){
     map * m = current_map(game->map_stack);
     if (! m->tiles[m->player_y + 1][m->player_x]){
        return;
     }
     int * activate =  m->tiles[m->player_y + 1][m->player_x]->stats;
     if (*activate != ACTIVATED){
        return;
     }
    printf("The bridge appears.\n");
    int y = m->height/2;
    #ifdef BUG
    for (int x = 0; x < 5; x++ ){
    #else
    for (int x = 0; x < 6; x++ ){
    #endif
        m->floor[y][3+x].category = GRASS;
        m->floor[y][3+x].properties = 0;
        m->floor[y][3+x].display_symbol = ' ';
    }
}


void activate_switch(game_instance* game, int y, int x) {
    map * m = current_map(game->map_stack);

    if (!m->tiles[y][x] || !has_property(m->tiles[y][x]->properties, ACTIVABLE)) {
        printf("There is no switch to touch in front of you.\n");
        return;
    }
    int* stats = m->tiles[y][x]->stats;
    if (*stats == ACTIVATED) {
        printf("This switch is already activated.\n");
        return;
    } 

    *stats = ACTIVATED;
    printf("You activated the switch.\n");
}
//touch the switch
void touch(game_instance* game) {
    map* m = current_map(game->map_stack);

    if(m->player_direction == DIR_UNKNOWN) {
        printf("Player is not oriented, cannot touch\n");
        return;
    }
    
    // compute switch position
    int x, y;
    player_look_forward(m, &y, &x);
    #ifdef BUG
    activate_switch(game, x, y);
    #else
    activate_switch(game, y, x);
    #endif
}
