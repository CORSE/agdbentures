/* @AGDB
 * level_title: Bridge
 * level_name:bridge
 * exec_name: bridge
 * engine_name: events
 * bug_tags : inverse_xy loop_error
 * engine_tags:
 * engine_antitags:
 * arcade_skins: off :tiles:objects/lever_left.png
 * arcade_skins: on :tiles:objects/lever_right.png
 * arcade_maps: main main.tmx
 * available_commands: edit next step continue interrupt
 * no_verify_exit: cond map->player_x == exit_x
 * no_verify_exit: cond map->player_y == exit_y
 *
 * Bug is just an inversion between x and y in this level.
 * Maybe we can do something more interesting ...
 */

#include "engine/agdbentures.h"
#include "input_manager.h"
#include "custom_map.h"
#include <stdio.h>


int exit_x = 11;
int exit_y = 5;


direction player_direction = DIR_RIGHT;

int main() {
    printf("Available commands: UP, DOWN, LEFT, RIGHT and *_N variants; TOUCH.\nUse Ctrl+C to quit.\n");

    game_instance* game = init_game();

    map* current_map = load_map("overworld", str_map);
    push_on_stack(game->map_stack, current_map);
    current_map->player_direction = player_direction;
    init_switches(current_map);

    add_event(game, verify_exit);
    add_event(game, drown);
    add_event(game, bridge_appear);
    show_map(current_map);

    command * com;

    // The Holy Main Loop
    while (!game->exit_main_loop) {
        com = get_next_command();
        if(apply_input(current_map, com) == -1) {
            if(strcmp(com->command_buffer, "TOUCH") == 0) {
                touch(game);
            } else {
                printf("Unknown command %s\n", com->command_buffer);
            }
        }
        free_command(com);

        /* since the events may be applied if the input is FORWARD_N,
           we need to check to avoid triggering the same event twice*/
        if (!game->exit_main_loop) {
            apply_events(game);
        }

        show_map(current_map);
    }
    printf("HYHYEHYEHEYHEY\n");
    remove_all_events(game);
    free_game(game);
    
    return EXIT_SUCCESS;
}
