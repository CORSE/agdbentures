#ifndef INPUT_MANAGER_H
#define INPUT_MANAGER_H

#include "custom_map.h"
#include "engine/agdbentures.h"

// examine la commande c et appelle les fonctions d'action ou de déplacement
// correspondantes
void touch(game_instance *game);
void bridge_appear(game_instance* game);

#endif // INPUT_MANAGER_H
