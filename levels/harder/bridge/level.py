#!/usr/bin/env python3

from level.level_abc import AbstractLevel
import graphic.constants as cst
import arcade
from level.action import Action
from level.world import get_top_map



class Level(AbstractLevel):
    """bridge"""
    begin = True
    i = 0
    def arcade_custom_restart(self):
        self.arcade_custom_first_start()

    def arcade_custom_first_start(self):
        player = self.player
        payload = {
            "topic": "sprites",
            "action": Action("hide_layer"),
            "layer": "decorations_top",
        }
        self.send_to_gui(payload)
        print(self.world.exit)
        switche1 = self.world.get_object("switches1")
        switche1.visible= True
        switche1.place_at(2,9)
        switche1.send_update()
        switche2 = self.world.get_object("switches2")
        switche2.visible= True
        switche2.place_at(9,2)
        switche2.send_update()
        player.visible = True
        player.send_update()
        def player_custom_update(player, memory):
            if self.begin:
                self.begin = False
                return
            frames = memory["stack"]
            map = get_top_map(frames)
            if not map:
                return
            for i in range(6):
                print(map.floor[5][3+i])
                if map.floor[5][3+i].properties == '0':
                    payload = {
                        "topic": "sprites",
                        "action": Action("show"),
                        "layer": "decorations_top",
                        "locations": [(3+i,5)],
                    }
                    self.send_to_gui(payload)


        player.post_update = player_custom_update



    def pre_validation(self):
        self.checker.append_inputs(
            ["DOWN",
             "DOWN",
             "RIGHT",
             "DOWN",
             "TOUCH",
             "UP",
             "UP",
             "UP",
             "RIGHT",
             "RIGHT",
             "RIGHT",
             "RIGHT",
             "RIGHT",
             "RIGHT",
             "RIGHT",
             "RIGHT",
             ]
        )

    def post_validation(self):
        pass
    def test(self):
        import tests.lib_test as T
        pass



if __name__ == "__main__":
    # run_level(Level, __file__, level_type="text")
    # run_level(Level, __file__, level_type="arcade")
    test_level(Level, __file__)
