#ifndef SHORTEST_PATH_H
#define SHORTEST_PATH_H

#include "engine/agdbentures.h"
#include "maze_generation.h"

int find_shortest_path(map* m, vertex src, vertex goal);

bool check_vertex(map* m, vertex v, bool visited[MAX_MAZE_SIZE][MAX_MAZE_SIZE]);

#endif // SHORTEST_PATH_H