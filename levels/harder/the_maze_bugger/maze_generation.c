#include "maze_generation.h"
#include "engine/agdbentures.h"
#include "shortest_path.h"
#include <stdbool.h>
#include <time.h>

void add_wall(map *m, int y, int x)
{
    if (m->floor[y][x].category != F_WALL) {
        m->floor[y][x] = create_wall();
    }
}

void create_h_wall(map *m, int row)
{
    for (int x = 0; x < m->width; x++) {
        add_wall(m, row, x);
    }
}

void create_v_wall(map *m, int column)
{
    for (int y = 0; y < m->height; y++) {
        add_wall(m, y, column);
    }
}

void fill_grid(map *m)
{
    for (int y = 0; y < m->height; y++) {
        for (int x = 0; x < m->width; x++) {
            m->floor[y][x] = create_tile(F_GRASS, 0);
        }
    }
}

void init_maze(map *m)
{
    fill_grid(m);

    for (int y = 0; y < m->height; y += 2) {
        create_h_wall(m, y);
    }
    for (int x = 0; x < m->width; x += 2) {
        create_v_wall(m, x);
    }
}

// renvoie un pointeur vers un voisin pas encore visité ou NULL sinon
vertex *random_unvisited_neighbour(map *m, vertex *old,
                                   bool visited[MAX_MAZE_SIZE][MAX_MAZE_SIZE])
{
    vertex unv_neighbours[4];
    int nb_unv_neighbour = 0;

    int possible_neighbours[4][2] = {{2, 0}, {-2, 0}, {0, 2}, {0, -2}};

#ifdef BUG
    vertex *res;
#else
    vertex *res = malloc(sizeof(vertex));
#endif
    for (int a = 0; a < 4; a++) {

        vertex new = {old->x + possible_neighbours[a][0],
                      old->y + possible_neighbours[a][1]};

        if (check_vertex(m, new, visited)) {
            unv_neighbours[nb_unv_neighbour] = new;
            nb_unv_neighbour++;
        }
    }

    if (!nb_unv_neighbour) {
        free(res);
        return NULL;
    }
    *res = unv_neighbours[rand() % nb_unv_neighbour];
    return res;
}

void connectCells(map *m, vertex *v1, vertex *v2)
{
    int y = v1->y, x = v1->x;
    if (v1->x > v2->x) {
        x -= 1;
    } else if (v1->x < v2->x) {
        x += 1;
    } else if (v1->y > v2->y) {
        y -= 1;
    } else {
        y += 1;
    }
    m->floor[y][x] = create_tile(GRASS, 0);
}

void randomizedDFS(map *m, vertex *start,
                   bool visited[MAX_MAZE_SIZE][MAX_MAZE_SIZE])
{

    visited[start->y][start->x] = true;
    vertex *new = random_unvisited_neighbour(m, start, visited);
    while (new != NULL) {
        connectCells(m, start, new);
        randomizedDFS(m, new, visited);
        free(new);
        new = random_unvisited_neighbour(m, start, visited);
    }
}

void DFS(map *m)
{
    int x = 2 * (rand() % (m->height / 2)) + 1;
    int y = 2 * (rand() % (m->height / 2)) + 1;

    vertex startVertex = {x, y};
    bool visited[MAX_MAZE_SIZE][MAX_MAZE_SIZE] = {{0}};

    randomizedDFS(m, &startVertex, visited);
}

// this will generate the maze
// size must be an odd number >= 3 and the maze will be a square
map *generate_maze(int size)
{

    srand(time(NULL));

    map *maze = init_map("maze", size, size);
    init_maze(maze);

    DFS(maze);

    place_player(maze, 1, 1);
    place_entity(maze, create_entity(EXIT, 0, C_EXIT, NULL), maze->height - 2,
                 maze->width - 2);

    return maze;
}
