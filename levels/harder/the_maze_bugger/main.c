/* @AGDB
 * level_title: The Maze Bugger
 * engine_name: event_args
 * 
 * exec_name: the_maze_bugger
 * engine_tags:
 * engine_antitags:
 */

#include <stdlib.h>
#include <stdio.h>

#include "engine/agdbentures.h"
#include "maze_generation.h"
#include "events_map.h"
#include "shortest_path.h"

#define GOLD 0

#define max(a,b) (((a) > (b)) ? (a) : (b))

void show_story(){
  printf("============================\n");
  printf("Welcome to The Maze Bugger !\n");
  printf("============================\n");

  printf("You are concocting a magic potion to become invisible,\n");
  printf("but you are missing the key ingredient of the recipe: the shiny mushroom.\n\n");

  printf("As you imagine, your objective is to find this ingredient.\n");
  printf("The problem is that the only person who can sell you this ingredient is in a labyrinth.\n\
To be able to buy this ingredient you must have at least one coin.\n\
But be careful, you have a hole in your pocket and lose a coin every time you move.\n\n");

  printf("You start with as many coins as the length of the shortest path between you and the mushroom plus a small margin\n\n");
}

//main loop which will get and execute user's inputs
int execute_loop(game_instance* game){

  map* m = game->current_map;
  int *remaining_coins = &((int*)m->tiles[m->player_y][m->player_x]->stats)[GOLD];

  printf("You have %d coins left.\n", *remaining_coins);
  command * com = get_next_command();

  while (!game->exit_main_loop) {
    
    //This is just to verify that the player did move before updating the number of coins
    if(apply_input(m,com)){
      *remaining_coins = max(*remaining_coins-1,0);
    }
    
    show_map(m);
    printf("You have %d coins left.\n",*remaining_coins);

    /* since the events may be applied if the input is FORWARD_N,
    we need to check to avoid triggering the same event twice  */
    if (!game->exit_main_loop)
      apply_events(game);

    if (!game->exit_main_loop)
      com = get_and_free_input(com);

    // plus aucun input mais on a pas encore gagné ou perdu -> défaite
    if (com == NULL && !game->exit_main_loop){
      level_failed(game, game->level_exit_code);
      free_command(com);
      return EXIT_FAILURE;
      }
    }
  free_command(com);
  return EXIT_SUCCESS;
}

int main(){

  //just change this variable to adjust the size of the maze
  //must be an odd int >= 3
  int maze_size = 7;
  show_story();

  game_instance* maze_game = init_game();

  add_event(maze_game, verify_exit, maze_game);

  map* maze_map = generate_maze(maze_size);
  push_on_stack(maze_game->map_stack,maze_map);
  maze_game->current_map = maze_map;

  show_map(maze_map);

  vertex start = {1,1};
  vertex goal = {maze_size-2,maze_size-2};

  int shrt_path_len = find_shortest_path(maze_map, start,goal);
  printf("Shortest path is : %d\n",shrt_path_len);
  
  //the player's statistic will contain the number of coins he has
  maze_map->tiles[maze_map->player_y][maze_map->player_x]->stats = malloc(sizeof(int));

  ((int *)(maze_map->tiles[maze_map->player_y][maze_map->player_x]->stats))[GOLD] = shrt_path_len + 2;

  execute_loop(maze_game);

  free_game(maze_game);

  return EXIT_SUCCESS;

}
