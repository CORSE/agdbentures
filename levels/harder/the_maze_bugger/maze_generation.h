#ifndef MAZE_GENERATION_H
#define MAZE_GENERATION_H

#include "engine/agdbentures.h"

#define MAX_MAZE_SIZE 51

typedef struct Vertex
{
    int x;
    int y;
    
} vertex;


map * generate_maze(int size);

#endif // MAZE_GENERATION_H