#include "shortest_path.h"
#include <stdio.h>
#include <stdlib.h>

//check is the given vertex is valid (not visited, not a wall and within the limits of the map)
bool check_vertex(map* m, vertex v, bool visited[MAX_MAZE_SIZE][MAX_MAZE_SIZE]){
    if ((1<=v.x && v.x < m->width-1) && (1<=v.y && v.y < m->height-1)){
        if (!visited[v.y][v.x] && !is_obstacle(m,v.y,v.x))
        {
            return true;
        }
    }
    return false;
}

//check if two vertices are equal
bool are_equal(vertex a, vertex b){
    return a.x==b.x && a.y==b.y;
}

// return the length of the sortest path from src to goal or -1 if there is no such path
// This is a simple pathfinding algo using BFS
int find_shortest_path(map* m, vertex src, vertex goal){
    int queue_index = 0;
    vertex queue[MAX_MAZE_SIZE*MAX_MAZE_SIZE];
    bool visited[MAX_MAZE_SIZE][MAX_MAZE_SIZE] = {{0}};
    int dist[MAX_MAZE_SIZE][MAX_MAZE_SIZE] = {{0}};
    int possible_neighbours[4][2] = {{1,0},{-1,0},{0,1},{0,-1}};
    
    visited[src.y][src.x]=true;
    dist[src.y][src.x] = 0;
    queue[queue_index] = src;
    queue_index++;

    if(are_equal(src,goal)){
        return 0;
    }

    while (queue_index)
    {
        vertex cur_vertex = queue[queue_index-1];
        queue_index--;

        for (int a = 0;a<4;a++){

            vertex new = {cur_vertex.x+possible_neighbours[a][0],cur_vertex.y+possible_neighbours[a][1]};

            if (check_vertex(m, new,visited)){
                visited[new.y][new.x] = true;
#ifdef BUG
                dist[new.y][new.x] += 1;
#else
                dist[new.y][new.x] = dist[cur_vertex.y][cur_vertex.x] + 1;
#endif               
                queue[queue_index] = new;
                queue_index++;

                if (are_equal(new,goal)){
                    return dist[goal.y][goal.x];
                }
            }
        }
    }
    return -1;
}