#ifndef EVENTS_MAP_H
#define EVENTS_MAP_H

#include "engine/agdbentures.h"
#include "maze_generation.h"

// Vérifie si le joueur se trouve sur la case de victoire
void verify_exit(void* gm);

#endif // EVENTS_MAP_H
