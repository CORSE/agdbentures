#include "events_map.h"
#include "engine/agdbentures.h"
#include "stdbool.h"

void verify_exit(void *gm)
{

    game_instance *game = (game_instance *)gm;
    map *m = current_map(game->map_stack);

    int exit_found = 0;

    if (find_category_in_stack(m, C_EXIT, m->player_y, m->player_x)) {
        printf("\nYou found the merchant !\n");
        exit_found = 1;
        int remaining_coins = ((int *)find_category_in_stack(
                                   m, C_PLAYER, m->player_y, m->player_x)
                                   ->stats)[0];
#ifdef BUG
        if (remaining_coins & exit_found) {
#else
        if (remaining_coins && exit_found) {
#endif
            printf("Remaining coins : %d, you can buy the ingredient\n",
                   remaining_coins);
            level_success(game);
        } else {
            printf(
                "No more remaining coins, you can't buy the ingredient :(\n");
            level_failed(game, game->level_exit_code);
        }
    }
}
