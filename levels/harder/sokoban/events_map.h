#ifndef EVENTS_MAP_H
#define EVENTS_MAP_H

#include "engine/agdbentures.h"

#define EXHAUST_LIMIT 40

// Vérifie si lles caisses sont bien sur les cibles
void verify_sokoban(game_instance *game);

// défaite si le joueur fait trop de mouvements
// après EXHAUST_LIMIT inputs, même s'il bump ou que l'input n'est pas reconnu
// (pour simplifier)
void exhaust(game_instance *game);

#endif // EVENTS_MAP_H
