/** @AGDB
 * level_title: Sokoban
 * exec_name: sokoban
 * engine_name: events
 * level_number: 1108
 * available_commands: next step continue
 * map_height: 9
 * map_width: 9
 * 
 * BUG: No bug here, just to test the engine. We may add an IA which is supposed to solve it and add bugs in the IA
 * tag: sokoban
 * 
 * HINT1: Do 1 box at a time
 */ 

/**
 * Entry point of the game. Manages the main loop, and apart from
 * that only calls functions.
 * 
 * Game design related tip:
 *   Sokoban is overdone and overrated. Go play Helltaker or Baba is You instead.
*/

#include "engine/agdbentures.h"
#include "events_map.h"

const char *str_map =
"\
  +---+ \n\
+-+   | \n\
|T>B  | \n\
+-+ BT| \n\
|T+-B | \n\
| | T ++\n\
|B IBBT|\n\
|   T  |\n\
+------+\n\
";

void custom_map(game_instance* game) {
    map * m = current_map(game->map_stack);
    for (int y = 0; y < m->height; y++) {
        for (int x = 0; x < m->width; x++) {
            if (m->tiles[y][x] && m->tiles[y][x]->category == C_ITEM) {
                add_property(&m->tiles[y][x]->properties, PUSHABLE);
                m->floor[y][x].category = TARGET;
            }
        }
    }
}


int main(int argc, char ** argv) {
    printf("Manual mode. Available commands: UP, DOWN, LEFT, RIGHT and *_N variants.\nUse Ctrl+C to quit.\n");

    game_instance *game = init_game();
    map* m = load_map("deepdarkdungeon", str_map);
    push_on_stack(game->map_stack, m);
    custom_map(game);
    add_event(game, verify_sokoban);
    add_event(game, exhaust);

    //show_map();

    command * com = get_next_command();

    // The Holy Main Loop
    while (!game->exit_main_loop) {
        apply_input(m, com);

        /* since the events may be applied if the input is FORWARD_N, 
           we need to check to avoid triggering the same event twice*/
        if (!game->exit_main_loop) {
            apply_events(game);
            com = get_and_free_input(com);
        }
        
        //show_map();
    }

    free_command(com);
    remove_all_events(game);
    free_game(game);
    
    return EXIT_SUCCESS;
}
