#include "events_map.h"

unsigned exhaust_limit = EXHAUST_LIMIT;
unsigned old_n_done = 0;

void verify_sokoban(game_instance* game) {
    map * m = current_map(game->map_stack);
    unsigned n_done = 0;
    unsigned n_targets = 0;
    for (int y = 0; y < m->height; y++) {
        for (int x = 0; x < m->width; x++) {
            if (m->floor[y][x].category == TARGET) {
                n_targets++;
                if (m->tiles[y][x] && has_property(m->tiles[y][x]->properties, PUSHABLE))
                    n_done++;
            }
        }
    }
    if (n_done == n_targets) {
        printf("All the boxes are on the targets!\n");
        level_success(game);
    } else if (old_n_done != n_done) {
        printf("%u/%u boxes are on the targets\n", n_done, n_targets);
    }
    old_n_done = n_done;
}

void exhaust(game_instance* game) {
    if (exhaust_limit > 0) {
        exhaust_limit--;
        if (exhaust_limit % 10 == 0)
            printf("You have %u movements left!\n", exhaust_limit);
    } else {
        printf("You are exhausted and die. Too bad.\n");
        level_failed(game, 1);
    }
}
