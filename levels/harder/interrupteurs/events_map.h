#ifndef EVENTS_MAP_H
#define EVENTS_MAP_H

#include "custom_map.h"
#include "engine/agdbentures.h"

/* Vérifie si les N_SWITCHES interrupteurs ont été allumés,
et active le piège ou ouvre la porte selon l'ordre d'allumage. */
void verify_switch_order(game_instance *game);

#endif // EVENTS_MAP_H
