#ifndef CUSTOM_MAP_H
#define CUSTOM_MAP_H

#include "engine/agdbentures.h"

#define N_SWITCHES 4

extern const char *str_map;

// the order in which the switches are activated and the expected order
#ifdef string_comparison
// @AGDB see common.c for explanation about this bug
extern char played_order[N_SWITCHES];
extern const char correct_order[N_SWITCHES];
#else
extern char played_order[N_SWITCHES + 1];
extern const char correct_order[N_SWITCHES + 1];
#endif

// initializes the switches
void init_switches(map *m);

#endif // CUSTOM_MAP_H
