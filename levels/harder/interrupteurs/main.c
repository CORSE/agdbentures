/** @AGDB
 * level_title: Interrupteurs
 * exec_name: interrupteurs
 * engine_name: events
 * available_commands: edit next step continue interrupt
 * arcade_maps: main interrupteurs.tmx
 * arcade_skins: off :tiles:objects/lever_left.png
 * arcade_skins: on :tiles:objects/lever_right.png
 *
 * player_mode: map_stack
 *
 * alias: map_array mstack.maps[mstack.length - 1].floor
 * alias: exit_x    mstack.maps[mstack.length - 1].exit_x
 * alias: exit_y    mstack.maps[mstack.length - 1].exit_y
 * alias: left  4
 * alias: right 6
 * alias: up    8
 * alias: down  2
 *
 * no_verify_exit: cond map->player_x == exit_x
 * no_verify_exit: cond map->player_y == exit_y
 * no_verify player_x
 * watch: player_direction player_x player_y
 * track: load_map:mstack load_map:player_x load_map:player_y
 *
 * BUG: several bugs. One is a string comparison forgetting the \0, one is a template used instead of malloc, one is a stack alloc, the last one is an inversion of y and x
 *
 * tag: inverse_xy stack_alloc template string_comparison
 *
 * HINT1: What is the value of current_map()->entities for switches?
 */

/**
 * Entry point of the game. Manages the main loop, and apart from
 * that only calls functions.
 *
 * Initialization:
 *  1) load the map and possible items
 *  2) load all events
 *  3) initialize the player's position and data
 *  4) start the main loop
 *
 * Main loop:
 *  1) apply the player's input (move or action)
 *  2) observe triggers, apply each resolution function or applying triggers
 *  3) read the next input (file) or wait for the next frame and buffer an input during the wait (real time)
 *
 * Endgame:
 *  1) receive signal from level_success or level_failure
 *  2) free all allocated memory
 *  3) exit properly
 *
 * Game design related tip:
 *   If you want to create a game, note all your ideas, they may serve later.
 *   Video game is an art as cinema or literature: play lots of games to get a solid culture.
 *   But beside game design, several fields exist in video games creation:
 *      - level design
 *      - storytelling and writing
 *      - music and sound effets
 *      - worldbuilding
 *      - ... and many others!
*/

#include "engine/agdbentures.h"
#include "events_map.h"
#include "input_manager.h"
#include "custom_map.h"

int switches0_x = 1;
int switches0_y = 4;

int switches1_x = 13;
int switches1_y = 4;

int switches2_x = 1;
int switches2_y = 8;

int switches3_x = 13;
int switches3_y = 8;

int door_x = 7;
int door_y = 3;

int exit_x = 7;
int exit_y = 1;


int main() {
    printf("Manual mode. Available commands: UP, DOWN, LEFT, RIGHT and *_N variants; TOUCH.\nUse Ctrl+C to quit.\n");

    game_instance* game = init_game();

    map* m = load_map("overworld", str_map);
    push_on_stack(game->map_stack, m);
    game->current_map = m;
    init_switches(m);

    add_event(game, verify_exit);
    add_event(game, verify_switch_order);
    add_event(game, drown);

    show_map(current_map(game->map_stack));
    
    command * com;

    // The Holy Main Loop
    while (!game->exit_main_loop) {
        com = get_next_command();
        if (apply_input(m, com) <= 0) {

            if(strcmp(com->command_buffer, "TOUCH") == 0) {
                touch(game);
            } else {
                printf("Unknown command %s\n", com->command_buffer);
            }
        }

        /* since the events may be applied if the input is FORWARD_N, 
           we need to check to avoid triggering the same event twice*/
        apply_events(game);
        free_command(com);
        show_map(current_map(game->map_stack));
    }
    free_game(game);
    return EXIT_SUCCESS;
}
