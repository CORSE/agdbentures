#include "custom_map.h"

const char *str_map =
"\
+-----+-+-----+\n\
|~~~~~|@|~~~~~|\n\
|~~~~~| |~~~~~|\n\
+-----+D+-----+\n\
|X           X|\n\
|             |\n\
|             |\n\
|             |\n\
|X     >     X|\n\
+-------------+\n\
";

#ifdef BUG2
// @AGDB in the exercise we forget the +1 so the string doesn't end in \0 and played_order becomes corrupted
const char correct_order[N_SWITCHES] = "2103";
char played_order[N_SWITCHES] = "";
#else
const char correct_order[N_SWITCHES+1] = "2103";
char played_order[N_SWITCHES+1] = "";
#endif


#ifdef BUG
/* @AGDB returning a local variable address has undefined results
 which depend of the compiler. With GCC, this returns NULL and the switch is not existent */
entity create_switch(int n) {
    struct entity_s swi;
    swi.properties = OBSTACLE | ACTIVABLE;
    swi.id = n;
    int* states = malloc(sizeof(int));
    *states = DEACTIVATED;
    swi.stats = states;
    swi.display_symbol = 'X';
    return &swi;
}
#else // stack_alloc
entity create_switch(int n) {
    entity swi = malloc(sizeof(entity));
    swi->properties = ACTIVABLE | OBSTACLE;
    swi->id = n;
    // a switch has only one stat: its state
    int* states = malloc(sizeof(int));
    *states = DEACTIVATED;
    swi->stats = states;
    swi->display_symbol = 'X';
    swi->next = NULL;
    return swi;
}
#endif // stack_alloc


void init_switches(map * m) {
    int nth_switch = 0; // to differentiate the switches
    for (int y = 0; y < m->height; y++) {
        for (int x = 0; x < m->width; x++) {
            entity e = m->tiles[y][x];
            if (e && has_property(e->properties, ACTIVABLE)) {
                m->tiles[y][x] = create_switch(nth_switch);
                m->floor[y][x] = create_wall();
                nth_switch++;
            }
        }
    }

    assert(nth_switch == N_SWITCHES);
}
