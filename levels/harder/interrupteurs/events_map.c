#include "events_map.h"

int once = 1;

void verify_switch_order(game_instance* game) {
    // not all switches are on or this event has already been done
    if (!once || strlen(played_order) < N_SWITCHES) {
        return;
    }

    int trig;

    #ifdef string_comparison
    /* @AGDB use strcmp instead of strncmp
     and set the size of these strings to N_SWITCHES instead of N_SWITCHES+1
     see common.c for more explanations
     in the end the comparaison will never work even if the order is correct */
    if (strcmp(played_order, correct_order)) {
    #else
    if (strncmp(played_order, correct_order, N_SWITCHES)) {
    #endif
        trig = -1;
    } else {
        trig = 1;
    }

    map * m = current_map(game->map_stack);

    for (int y = 0; y < m->height; y++) {
        for(int x = 0; x < m->width; x++) {
            if (m->floor[y][x].category == GRASS && trig == -1) {
                m->floor[y][x].category = WATER;
            } else if (m->floor[y][x].category == DOOR && trig == 1) {
                m->floor[y][x].category = GRASS;
                m->floor[y][x].properties = 0;
                m->floor[y][x].display_symbol = ' ';
            }
        }
    }

    if (trig == -1){
        printf("You activated the switches in a wrong order! A trap triggers and a fireball go on you!\n");
        exit(EXIT_FAILURE);
        }
    else if (trig == 1)
        printf("You activated the switches in the correct order!\nThe door opens!\n");

    once = 0;
}
