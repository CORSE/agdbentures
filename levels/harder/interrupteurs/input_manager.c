#include "input_manager.h"

void activate_switch(map * m, int y, int x) {

    if (!m->tiles[y][x] || !has_property(m->tiles[y][x]->properties, ACTIVABLE)) {
        printf("There is no switch to touch in front of you.\n");
        return;
    } 
    int* stats = m->tiles[y][x]->stats;
    if (*stats == ACTIVATED) {
        printf("This switch is already activated.\n");
        return;
    } 

    *stats = ACTIVATED;
    printf("You activate the %dth switch.\n", m->tiles[y][x]->id);

    char buffer[2];
    sprintf(buffer, "%d", m->tiles[y][x]->id);
    strcat(played_order, buffer);
}

void touch(game_instance* game) {
    map* m = current_map(game->map_stack);

    if(m->player_direction == DIR_UNKNOWN) {
        printf("Player is not oriented, cannot touch\n");
        return;
    }
    
    // compute switch position
    int x, y;
    player_look_forward(m, &y, &x);
    activate_switch(m, y, x);
}
