from level.level_abc import AbstractLevel
from language import lmessage, Lang
from level.action import Action
import graphic.constants as cst



class Level(AbstractLevel):
    good_order = [2, 1, 0, 3]
    switches : list = []
    switches_order: list = []
    dico_pos: dict = {
        (1, 7, cst.Direction.DOWN): 2, (2, 8, cst.Direction.LEFT): 2, # lever 2
        (1, 5, cst.Direction.UP): 0, (2, 4, cst.Direction.LEFT): 0,  # lever 0
        (13, 5, cst.Direction.UP): 1, (12, 4, cst.Direction.RIGHT): 1, # lever 1
        (13, 7, cst.Direction.DOWN): 3, (12, 8, cst.Direction.RIGHT): 3 # lever 3
    }
    switches_order_validation = []
    already_done : bool = False

    def arcade_custom_restart(self):
        self.impact = self.world.get_object("impact")
        self.fire_on_player = self.world.get_object("fire_on_player")
        self.fire_on_player.visible = False
        self.fire_on_player.send_update()
        self.impact.visible = False
        self.arcade_custom_first_start()
    def arcade_custom_first_start(self):
        self.over = False
        self.actions = {}
        pl = {
            "topic": "sprites",
            "action": Action("hide_layer"),
            "layer": "decorations_top",
        }
        self.send_to_gui(pl)
        self.door = self.world.get_object("door")
        self.door.visible = True
        self.door.send_update()

        self.impact = self.world.get_object("impact")
        self.fire_on_player = self.world.get_object("fire_on_player")
        self.fire_on_player.visible = False
        self.fire_on_player.send_update()
        self.impact.visible = False
        pl = self.impact.payload()
        pl['change/speed'] = 0.8
        self.impact.send_update()
        self.impact.send_update(pl)
        self.switches = []
        for i in range(4):
            self.switches.append(self.world.get_object(f"switches{i}"))
            pl = self.switches[i].payload()
            pl['action'] = Action('change_skin')
            pl['skin'] = "on"
            self.switches[i].send_update(pl)

        self.player = self.world.player
        self.switches_order = []
        self.already_done = False
        self.register_breakpoint("activate_switch",self.switches_touch)
        self.register_breakpoint("verify_switch_order", self.open_door)

    def switches_touch(self):
        #for i in range(5):
            #self.fire.update()
        pos_actu = (self.player.coord_x,self.player.coord_y,self.player.direction)
        if pos_actu not in self.dico_pos.keys():
            return
        num_switche = self.dico_pos[pos_actu]
        if num_switche in self.switches_order:
            return
        y = (self.tracker.get_variable_value("y", as_raw_python_objects=True).value)
        x = (self.tracker.get_variable_value("x", as_raw_python_objects=True).value)
        map = (self.tracker.get_variable_value("m", as_raw_python_objects=True).value)
        if type(map[0].tiles[y][x]) == tuple:
            return
        self.switches_order.append(num_switche)
        pl = self.switches[num_switche].payload()
        self.actions["levers_activate"] = Action('change_skin')
        pl['action'] = self.actions["levers_activate"]
        pl['skin'] = "off"
        self.switches[num_switche].send_update(pl)

    def open_door(self):
        if len(self.switches_order) != 4 or self.already_done:
            return
        self.already_done = True
        if self.good_order == self.switches_order:
            self.door.visible = False
            self.door.send_update()
        else:
            self.actions["fire_ball_disapear"] = Action("disappear")
            self.actions["kill_player"] = Action("disappear")
            self.impact.visible = True
            self.impact.send_update()
            self.impact.place_at(self.player.coord_x, self.player.coord_y)
            pl = self.impact.payload()
            self.actions["move_ball"] = Action("move")
            self.actions["move_ball"].callback = self.touch_player
            pl["action"] = Action('change/far_move', [self.actions["levers_activate"]])
            pl["value"] = "crowflies"
            self.impact.send_update(pl)
            pl["action"] = self.actions["move_ball"]
            self.impact.send_update(pl)
            self.set_dependencies(self.actions["kill_player"])

    def explode(self):
        self.fire_on_player.place_at(self.player.coord_x, self.player.coord_y)
        self.fire_on_player.visible = True
        self.fire_on_player.send_update()
        self.fire_on_player.tl_offset_y = - 200

        pl = self.player.payload()
        pl['action'] = Action('hit')
        self.player.send_update(pl)
        self.player.visible = False
        self.player.send_update()
        pl = {
            "topic": "player_update",
            "action": self.actions["kill_player"],
        }
        self.send_to_gui(pl)
        self.over = True

    def touch_player(self):
        pl = self.impact.payload()
        self.actions["fire_ball_disapear"].callback = self.explode
        pl['action'] = self.actions["fire_ball_disapear"]
        self.impact.send_update(pl)



    def switches_touch_validation(self):
        chk = self.checker.tracker

        player_x = (chk.get_variable_value_as_str('m->player_x', "int"))
        player_y = (chk.get_variable_value_as_str('m->player_y', "int"))
        player_dest = (chk.get_variable_value_as_str('m->player_direction', "int"))
        player_dest = (cst.Direction(player_dest))
        pos_actu=(player_x,player_y,player_dest)
        if pos_actu not in self.dico_pos.keys():
            self.checker.failed("A switches will be activated but player is not in front of it")
            return

        num_switche = self.dico_pos[pos_actu]
        if num_switche in self.switches_order_validation:
            return

        y = (chk.get_variable_value_as_str('y', "int"))
        x = (chk.get_variable_value_as_str('x', "int"))
        pos_switch_in_map = (chk.get_variable_value_as_str(f'm->tiles[{y}][{x}]', "int"))

        if type(pos_switch_in_map) == tuple:
            self.checker.failed("It seems switches can't be activated ...")
        self.switches_order_validation.append(num_switche)

    def open_door_validation(self):
        if self.good_order != self.switches_order_validation:
            self.checker.failed(f"Player need to be kill if order is wrong.")
    def pre_validation(self):
        self.checker.register_breakpoint("activate_switch", self.switches_touch_validation)
        self.checker.append_inputs(
            [
                'LEFT',
                'LEFT',
                'LEFT',
                'LEFT',
                'LEFT',
                'TOUCH',
                'UP',
                'UP',
                'UP',
                'RIGHT',
                'RIGHT',
                'RIGHT',
                'RIGHT',
                'RIGHT',
                'RIGHT',
                'RIGHT',
                'RIGHT',
                'RIGHT',
                'RIGHT',
                'RIGHT',
                'UP',
                'TOUCH',
                'LEFT',
                'LEFT',
                'LEFT',
                'LEFT',
                'LEFT',
                'LEFT',
                'LEFT',
                'LEFT',
                'LEFT',
                'LEFT',
                'LEFT',
                'LEFT',
                'UP',
                'TOUCH',
                'RIGHT',
                'RIGHT',
                'RIGHT',
                'RIGHT',
                'RIGHT',
                'RIGHT',
                'RIGHT',
                'RIGHT',
                'RIGHT',
                'RIGHT',
                'RIGHT',
                'RIGHT',
                'DOWN',
                'DOWN',
                'TOUCH',
                'LEFT',
                'LEFT',
                'LEFT',
                'LEFT',
                'LEFT',
                'LEFT',
                'UP',
                'UP',
                'UP',
                'UP',
                'UP',
                'UP',
            ]
        )


    def post_validation(self):
        self.open_door_validation()
        if len(self.switches_order_validation) != 4:
            self.checker.failed("4th switch need to be activated")


    def create_scenario(self, T, die = False):
        T.left(5)
        T.send_input('TOUCH')
        T.up(3)
        if die:
            T.up()
            T.left()
            T.send_input("TOUCH")
            T.down()
        T.right(11)
        T.up()
        T.send_input("TOUCH")
        T.left(12)
        T.up()
        T.send_input('TOUCH')
        T.right(12)
        T.down(2)
        T.send_input('TOUCH')
        T.left(6)
        T.up(6)

    def test(self):
        import tests.lib_test as T
        self.recompile_answer()
        self.create_scenario(T)
        T.ccontinue()
        T.expect_victory()
        self.run()
        self.recompile_bug()
        self.create_scenario(T)
        T.ccontinue()
        T.expect_infinite_loop()
        self.run()
        self.recompile_answer()
        self.create_scenario(T, die = True)
        T.ccontinue()
        T.expect_defeat()
        self.run()

