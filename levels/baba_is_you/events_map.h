#ifndef EVENTS_MAP_H
#define EVENTS_MAP_H

#include "../../engines/fullgame/agdbentures.h"
#include "custom_map.h"

typedef enum {
    BABA_r,
    FLAG_r,
    WALL_r,
} who;

typedef enum {
    YOU,
    STOP,
    WIN
} what;

// There is only one 'you' and one 'win' for each move (you can't move 2 different entities at a time).

// Vérifie si le joueur se trouve sur la case de victoire
void verify_exit(void);

void add_rule(int c, int y, int x);
// initializes the rules
void verify_rules(void);



#endif // EVENTS_MAP_H
