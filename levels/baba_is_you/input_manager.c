#include "input_manager.h"

extern int rules[3][3];

void input(int y, int x, command * c){
    map * m = current_map();
    int i = coord_idx(y,x);
    if (i>=m->height * m->width || i<0){
        return;
    }
    if (m->entities[i] && find_in_stack(y, x, m->role)){
        if (!strcmp(c->command_buffer, "UP")) {
            up(y,x);
        } else if (!strcmp(c->command_buffer, "UP_N")) {
            up_n(y,x,atoi(c->args[0]));
        } else if (!strcmp(c->command_buffer, "DOWN")) {
            down(y,x);
        } else if (!strcmp(c->command_buffer, "DOWN_N")) {
            down_n(y,x,atoi(c->args[0]));
        } else if (!strcmp(c->command_buffer, "LEFT")) {
            left(y,x);
        } else if (!strcmp(c->command_buffer, "LEFT_N")) {
            left_n(y,x,atoi(c->args[0]));
        } else if (!strcmp(c->command_buffer, "RIGHT")) {
            right(y,x);
        } else if (!strcmp(c->command_buffer, "RIGHT_N")) {
            right_n(y,x,atoi(c->args[0]));
        } else{
            printf("Unknown input command.\n");
        }
    }
}

void c_apply_input(command * c) {
    map * m = current_map();
    if (m->role == NONETYPE){
        return;
    }
    #ifdef loops_error
    for (int y = 0; y < m->height; y++){
        for(int x = 0; x < m->width; x++){
            input(y,x,c);
        }
    }
    #else
    //two different loops so that if we move an entity, we only see it once (moved to an index already seen by the loop).
    if (!strcmp(c->command_buffer, "UP") || !strcmp(c->command_buffer, "UP_N") || !strcmp(c->command_buffer, "LEFT") || !strcmp(c->command_buffer, "LEFT_N")){
        for (int y = 0; y < m->height; y++){
            for(int x = 0; x < m->width; x++){
                input(y,x,c);
            }
        }
    }
    else if (!strcmp(c->command_buffer, "DOWN") || !strcmp(c->command_buffer, "DOWN_N") || !strcmp(c->command_buffer, "RIGHT") || !strcmp(c->command_buffer, "RIGHT_N")){
        #ifdef index_error
        for (int y = m->height-1; y > 0; y--){
            for(int x = m->width-1; x > 0; x--){
        #else 
        for (int y = m->height-1; y >= 0; y--){
            for(int x = m->width-1; x >= 0; x--){
        #endif
                input(y,x,c);
            }
        }
    }
    else {
        printf("Unknown input command.\n");
    }
    #endif

}
