#!/usr/bin/env python3

# Pour le moment level dans être dans PYTHONPATH
from level.utils import generate_dict
from level.level import level


if __name__ == '__main__':
    metadata = generate_dict("main.c")
    g = level(metadata, type="text")
    g.run()
