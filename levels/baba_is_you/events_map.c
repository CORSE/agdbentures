#include "events_map.h"

int once = 1;
int rules[3][3];

//verify if the rule win is set
int exist_win(){
    if(rules[BABA_r][WIN]){
        return BABA;
    }
    if(rules[FLAG_r][WIN]){
        return FLAG;
    }
    if (rules[WALL_r][WIN]){
        return WALL;
    }
    return NONETYPE; // you can't win 
}

// verify if an entity role is (on) an entity marked as win
void ok_win(entitytype role, entitytype win){
    map * m = current_map();
    if (role == win){
        level_success();
        return;
    }
    for (int y = 0; y<m->height; y++){
        for (int x = 0; x<m->width; x++){
            if (find_in_stack(y,x,role) && find_in_stack(y,x,win))
                level_success();
        }
    }
}


void verify_exit(void) {
    map * m = current_map();
    entitytype role = m->role;
    entitytype win = exist_win();
    if (win==NONETYPE){
        return;
    }
    ok_win(role,win);
}


void clear_rules(){
    for (int i = 0; i<3; i++){
        for (int j = 0; j<3; j++){
            rules[i][j]=false;
        }
    }
}

// apply STOP role 
void apply_stop(void){
    map * m = current_map();
    for(int y = 0; y < m->height; y++){
        for(int x = 0; x < m->width; x++){
            int i = coord_idx(y,x);
            m->floor[i]->obstacle = false;
            if (m->entities[i] && m->entities[i]->category == FLAG && rules[FLAG_r][STOP])
                m->floor[i]->obstacle = true;
            if (m->entities[i] && m->entities[i]->category == BABA && rules[BABA_r][STOP])
                m->floor[i]->obstacle = true;
            if (m->entities[i] && m->entities[i]->category == WALL && rules[WALL_r][STOP])
                m->floor[i]->obstacle = true;
        }
    } 
}

// return 1 if there is an entity player on the map
int player_exist(){
    map * m = current_map();
    for (int y = 0; y < m->height; y++){
        for (int x = 0; x < m->width; x++){
            int i = coord_idx(y,x);
            if (m->entities[i] && m->entities[i]->category == PLAYER){
                return 1;
            }
        }
    }  
    return 0;
}

// apply the YOU rule (changes the role on the map)
void apply_you(void){
    map * m = current_map();
    if(rules[BABA_r][YOU]){
        m->role = BABA;
    } else if (rules[WALL_r][YOU]){
        m->role = WALL;
    } else if(rules[FLAG_r][YOU]){
        m->role = FLAG;
    } else {
        if (player_exist()){
            m->role = PLAYER;
        }
        else    
            m->role = NONETYPE;
    }
}

// set the rules array to 1 if a rule is found : a rule is b/f/g i w/y/x from left to right or up to down.
void add_rule(int c, int y, int x){
    map * m = current_map();
    int index_who;
    if (c=='b')
        index_who = BABA_r;
    else if (c=='f')
        index_who = FLAG_r;
    else if (c=='m')
        index_who = WALL_r;
    else return;

    int i = coord_idx(y,x);
    int width = m->width;
    if (x+2 < m->width && m->entities[i+1] && m->entities[i+1]->category == TEXT && m->entities[i+1]->stats[0] == 'i'){
        if (m->entities[i+2] && m->entities[i+2]->category == TEXT){
            int index_what;
            if (m->entities[i+2]->stats[0] == 'w')
                index_what = WIN;
            else if (m->entities[i+2]->stats[0] == 'y')
                index_what = YOU;
            else if (m->entities[i+2]->stats[0] == 'x'){
                index_what = STOP;
            }
            rules[index_who][index_what] =  1;
        }
    }
    #ifdef double_writing_error // you have to check from left to right AND up to down else a rule can be skipped.
    else 
    #endif
    if (y+2 < m->height && m->entities[i+width] && m->entities[i+width]->category == TEXT && m->entities[i+width]->stats[0] == 'i'){
        if (m->entities[i+width*2] && m->entities[i+width*2]->category == TEXT){
            int index_what;
            if (m->entities[i+width*2]->stats[0] == 'w')
                index_what = WIN;
            else if (m->entities[i+width*2]->stats[0] == 'y')
                index_what = YOU;
            else if (m->entities[i+width*2]->stats[0] == 'x'){
                index_what = STOP;
            }
            rules[index_who][index_what]=1;
        }
    }    
}


// browse all the map and if a text entity is encountered check rules.
void verify_rules(void) {
    map * m = current_map();
    clear_rules();
    for (int y = 0; y < m->height; y++) {
        for (int x = 0; x < m->width; x++) {
            int i = coord_idx(y, x);
            if (m->entities[i] && m->entities[i]->category == TEXT) {
                add_rule(m->entities[i]->stats[0], y, x);
            }
        }
    }
    apply_stop();
    apply_you();
}

