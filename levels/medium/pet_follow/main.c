/* @AGDB
 * level_title: pet_follow
 * exec_name: pet_follow
 * engine_name: command_args
 * interactive: True
 * available_commands: next step edit continue
 *
 * arcade_maps: main pet_follow.tmx
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "engine/action.h"
#include "engine/agdbentures.h"
#include "engine/game.h"
#include "engine/read_input.h"

#define CIRCULAR_BUFFER_SIZE 10
#define FOLLOW_DISTANCE \
  5  // The distance at which the pet will follow the character

int exit_x = 18;
int exit_y = 4;

int pet_exit_x = 19;
int pet_exit_y = 2;

int pet_x = -1, pet_y = -1;

// Circular buffer definition to store the command history
typedef struct {
  command * internal[CIRCULAR_BUFFER_SIZE];
  int read_index;  // The next command to read
} circular_buffer;

char str_map[] =
"\
+------------------------+\n\
|~~~~~~###############~~~|\n\
|~~~~~~#   #         #~~~|\n\
|~###### # ##### ### #~~~|\n\
|~#P    >#     # #@  #~~~|\n\
|~############   #####~~~|\n\
|~~~~~~~~~~~~#####~~~~~~~|\n\
+------------------------+\n\
";

command right_command = {"RIGHT", 5, 0, NULL};
command right_command_simple = {"RIGHT", 1, 0, NULL};
command left_command_simple = {"LEFT", 1, 0, NULL};
command up_command_simple = {"UP", 1, 0, NULL};
command down_command_simple = {"DOWN", 1, 0, NULL};

circular_buffer command_buffer;


command * transform_command(char * move){
    if(strcmp(move, "LEFT") == 0  || strcmp(move, "LEFT_N") == 0 ) {
        return &left_command_simple;
    } else if (strcmp(move, "RIGHT") == 0 || strcmp(move, "RIGHT_N") == 0) {
        return &right_command_simple;
    } else if (strcmp(move, "UP") == 0 || strcmp(move, "UP_N") == 0) {
        return &up_command_simple;
    } else if (strcmp(move, "DOWN") == 0 || strcmp(move, "DOWN_N") == 0) {
        return &down_command_simple;
    } else {
        return NULL;
    }
}


void buffer_write(circular_buffer * buffer, command * input_player) {
  command * to_write = transform_command(input_player->command_buffer);
  for (int i=0; i< input_player->n_args +1 ; i++){
    buffer->internal[(buffer->read_index + FOLLOW_DISTANCE) % CIRCULAR_BUFFER_SIZE ] = to_write;
  }
}

command * buffer_read(circular_buffer * buffer) {
  command * command_read = buffer->internal[buffer->read_index % (CIRCULAR_BUFFER_SIZE - 1)];
#ifdef BUG
  buffer->read_index = buffer->read_index % ( CIRCULAR_BUFFER_SIZE - 1 ) + 1;
#else
  buffer->read_index = (buffer->read_index + 1)  %  CIRCULAR_BUFFER_SIZE;
#endif
  return command_read;
}

void init_buffer(circular_buffer *buffer) {
  for (int i = 0; i < FOLLOW_DISTANCE; i++) {
    buffer->internal[i] = &right_command;
  }
  buffer->read_index = 0;
}

void verify_exit_pet(game_instance* game) {
  if (pet_x == pet_exit_x && pet_y == pet_exit_y) {
    verify_exit(game->map);
    exit(EXIT_SUCCESS);
  } else{
    printf("DEFAITE the pet should be at x=%d y=%d but is at x=%d y=%d\n", pet_exit_x, pet_exit_y, pet_x, pet_y);
    exit(EXIT_FAILURE);
  }
}

int main() {
  game_instance* game = init_game(20, 8);
  game->map = load_map(str_map);
  map* current_map = game->map;
  // init pet coordinate
  pet_x = 3;
  pet_y = 4;

  init_buffer(&command_buffer);

  command * input_command;
  command * buffered_command;
  while (!game->exit_main_loop) {
    input_command = get_next_command();

    // write the command in the buffer for the pet to follow
    // Split DIR N into N DIR commands

    int borne_sup = apply_input(game->map, input_command);
    for (int i = 0; i < borne_sup; i++) {
      buffer_write(&command_buffer, input_command);
      // reads the next command to do
      buffered_command = buffer_read(&command_buffer);

      // moves the pet accordingly, don't go into walls
      int next_y;
      int next_x;
      direction pet_direction = direction_from_string(buffered_command->command_buffer);
      look_forward( pet_y, pet_x,pet_direction, &next_y, &next_x);
      // The pet moves if the destination is empty, otherwise it does nothing but consume the command
      if (get_entity(game->map, next_y, next_x) == ' ') {
        pet_y = next_y;
        pet_x = next_x;
      }
      if (game->map->player_y == exit_y && game->map->player_x == exit_x){
        game->exit_main_loop = 1;
      }
    }
    free_command(input_command);
  }
  verify_exit_pet(game);
}
