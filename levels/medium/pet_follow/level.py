from level.level_abc import AbstractLevel
from language import lmessage, Lang

import graphic.constants as cst


class Level(AbstractLevel):
    pet_exit_x = 0
    pet_exit_y = 0
    pet_x = 0
    pet_y = 0
    pet_position = []

    def arcade_custom_restart(self):
        self.arcade_custom_first_start()

    def arcade_custom_first_start(self):
        pet = self.world.get_object("pet")
        self.pet_position = [3, 4]
        pet.direction = cst.Direction.RIGHT
        self.register_breakpoint('get_next_command', self.update_pet_direction)

    def update_pet_direction(self):
        pet = self.world.get_object("pet")
        pet_x, pet_y = self.pet_position
        self.pet_position = [pet.coord_x, pet.coord_y]
        if pet.coord_x > pet_x:
            pet.direction = cst.Direction.RIGHT
        if pet.coord_x < pet_x:
            pet.direction = cst.Direction.LEFT
        if pet.coord_y > pet_y:
            pet.direction = cst.Direction.DOWN
        if pet.coord_y < pet_y:
            pet.direction = cst.Direction.UP
        pet.send_update()

    def check_move(self):
        player = self.world.get_player()
        self.player_position.append([player.coord_x, player.coord_y])

    def update_pet(self):
        chk = self.checker.tracker
        self.pet_x = chk.get_variable_value_as_str('pet_x', "int")
        self.pet_y = chk.get_variable_value_as_str('pet_y', "int")

    def pre_validation(self):
        self.checker.append_inputs(
            [
                'UP',
                'UP',
                'RIGHT',
                'RIGHT',
                'DOWN',
                'DOWN',
                'RIGHT',
                'RIGHT',
                'RIGHT',
                'RIGHT',
                'DOWN',
                'RIGHT',
                'RIGHT',
                'UP',
                'UP',
                'UP',
                'RIGHT',
                'RIGHT',
                'RIGHT',
                'RIGHT',
                'DOWN',
                'DOWN',
                'LEFT',
                'LEFT',
            ]
        )
        chk = self.checker.tracker
        self.pet_exit_x = chk.get_variable_value_as_str('pet_exit_x', "int")
        self.pet_exit_y = chk.get_variable_value_as_str('pet_exit_y', "int")
        self.checker.register_watch('pet_x', self.update_pet)
        self.checker.register_watch('pet_y', self.update_pet)

    def post_validation(self):
        if self.pet_exit_x != 19 or self.pet_exit_y != 2:
            self.checker.failed(
                lmessage(
                    {
                        Lang.EN: "You don't have to move the exit.",
                        Lang.FR: "Vous n'avez pas à bouger la sortie.",
                    }
                )
            )
        if self.pet_y != self.pet_exit_y or self.pet_x != self.pet_exit_x:
            self.checker.failed(
                lmessage(
                    {
                        Lang.EN: f"DEFEAT the pet should be at x={self.pet_exit_x} y={self.pet_exit_y} but is at x={self.pet_x} y={self.pet_y}",
                        Lang.FR: f"DEFAITE l'animal doit être pour x en {self.pet_exit_x} et en y en{self.pet_exit_y} mais il est sur ces coordonnées pour x {self.pet_x} et voici pour y {self.pet_y}",
                    }
                )
            )

    def test(self):
        import tests.lib_test as T

        self.recompile_bug()
        T.send_input("UP_N 2")
        T.send_input("RIGHT_N 2")
        T.send_input("DOWN_N 2")
        T.send_input("RIGHT_N 4")
        T.send_input("DOWN_N 1")
        T.send_input("RIGHT_N 2")
        T.send_input("UP_N 3")
        T.send_input("RIGHT_N 4")
        T.send_input("DOWN_N 2")
        T.send_input("LEFT_N 2")
        T.ccontinue()
        T.expect_defeat()
        self.run()
        self.recompile_answer()
        T.send_input("UP_N 2")
        T.send_input("RIGHT_N 2")
        T.send_input("DOWN_N 2")
        T.send_input("RIGHT_N 4")
        T.send_input("DOWN_N 1")
        T.send_input("RIGHT_N 2")
        T.send_input("UP_N 3")
        T.send_input("RIGHT_N 4")
        T.send_input("DOWN_N 2")
        T.send_input("LEFT_N 2")
        T.ccontinue()
        T.expect_victory()
        self.run()
