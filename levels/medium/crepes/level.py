#!/usr/bin/env python3

import re

from level.level_abc import AbstractLevel
from level.world import get_top_map, map_linear_to_matrix
from logs import lvl
import graphic.constants as cst
from level.action import Action

class Level(AbstractLevel):
    """crepes"""

    def arcade_custom_first_start(self):
        self.ingredient_visible = [True, True]
        self.ingredient_donne = [False, False]

        self.cook = self.world.maps["crepe_shop"].named_objects["cook"]
        # print(f"named objects: {self.level_maps['crepe_shop'].named_objects}")

        # egg1 = self.world.get_object("egg1")
        # egg2 = self.world.get_object("egg2")
        # flour1 = self.world.get_object("flour1")
        # flour2 = self.world.get_object("flour2")
        # to make sure we hide the ingredients the first time the player visits the crepes shop
        self.first_call_hide = True

        def show_player():
            self.player.visible = True
            pl = self.player.payload()
            self.player.send_update(pl)

        def hide_player():
            self.player.visible = False
            pl = self.player.payload()
            self.player.send_update(pl)

        def change_map(map_name):
            lvl.debug(f"Map change")
            self.current_map = map_name
            self.world.current_map = self.current_map
            # by default sprites are visible and we hide them if needed
            self.ingredient_visible = [True, True]
            payload = {
                "topic": "map_change",
                "map": map_name,
            }
            self.player.send_update(payload)
            payload = {"topic": "player_update", "action": Action("rescale")}
            self.player.send_update(payload)
            show_player()
            if self.current_map == "crepe_shop":
                self.egg1 = self.world.get_object("egg1")
                self.egg2 = self.world.get_object("egg2")
                self.flour1 = self.world.get_object("flour1")
                self.flour2 = self.world.get_object("flour2")
                self.cook.visible = True
                if self.cook.direction != cst.Direction.DOWN:
                    self.cook.direction = cst.Direction.DOWN
                self.cook.send_update()
            if self.current_map == "egg_shop":
                egg = self.world.get_object("egg")

            if self.current_map == "flour_shop":
                flour = self.world.get_object("flour")

        def hide_ingredient(pos_x, pos_y):
            payload = {
                "topic": "sprites",
                "action": Action("hide"),
                "layer": "objects",
                "locations": [
                    (pos_x, pos_y),
                ],
            }
            self.send_to_gui(payload)

        def cook_action():
            self.cook.direction = cst.Direction.RIGHT
            self.cook.send_update()

        def display_ingredient(pos_x, pos_y, name):
            if name == "egg" and pos_x == 3:
                self.egg1.visible = True
                pl = self.egg1.payload()
                self.egg1.send_update(pl)
            elif name == "egg" and pos_x == 4:
                self.egg2.visible = True
                pl = self.egg2.payload()
                self.egg2.send_update(pl)
            elif name == "flour" and pos_x == 3:
                self.flour1.visible = True
                pl = self.flour1.payload()
                self.flour1.send_update(pl)
            elif name == "flour" and pos_x == 4:
                self.flour2.visible = True
                pl = self.flour2.payload()
                self.flour2.send_update(pl)
            else:
                lvl.debug(f"ERROR : wrong ingredient name, therefore cannot display it")

        def player_map_detection(player, memory):
            frames = memory["stack"]
            top_map = get_top_map(frames)
            if not top_map:
                return
            map_name = "".join(top_map.name[:-1])

            if self.current_map != map_name:
                # we entered a new map
                change_map(map_name)

        def ingredient_display_update(player, memory):
            # does nothing on main map
            if self.current_map == "main":
                return

            frames = memory["stack"]
            map = get_top_map(frames)
            map_linear_to_matrix(map)
            lvl.debug(f"Map tiles")
            for line in map.tiles:
                lvl.debug(f"{line}")
            if self.current_map == "crepe_shop":
                ingredients = map.tiles[3][3:5]
                lvl.debug(f"Ingredients : {ingredients}")
                if self.first_call_hide:
                    lvl.debug(f"Hiding all items")
                    hide_ingredient(3, 3)
                    hide_ingredient(4, 3)
                    self.first_call_hide = False

                for i in range(len(ingredients)):
                    ingredient = map.tiles[3][3 + i].split("'")[1]
                    lvl.debug(f"Enum result")
                    lvl.debug(f"i : {i}, ingredient : {ingredient}")
                    if ingredient == "E" and not self.ingredient_donne[i]:
                        lvl.debug(f"Displaying the egg sprite")
                        # display the egg sprite
                        self.ingredient_donne[i] = True
                        display_ingredient(3 + i, 3, name="egg")
                    elif ingredient == "F" and not self.ingredient_donne[i]:
                        lvl.debug(f"Displaying the flour sprite")
                        # display the flour sprite
                        self.ingredient_donne[i] = True
                        display_ingredient(3 + i, 3, name="flour")
                    elif ingredient == "=" and self.ingredient_donne[i]:
                        lvl.debug(f"Hiding he ingredient sprite")
                        # hide the ingredient sprite
                        hide_ingredient(3 + i, 3)

            else:
                ingredient = map.tiles[3][3].split("'")[1]
                if ingredient == " " and self.ingredient_visible[0]:
                    # make the sprite disappear
                    self.ingredient_visible[0] = False
                    hide_ingredient(3, 3)

        def post_update(player, memory):
            player_map_detection(self.player, memory)
            ingredient_display_update(self.player, memory)

        self.register_breakpoint("enter_building", hide_player)
        self.register_leave_function("enter_building", hide_player)
        self.register_breakpoint("cook_crepe", cook_action)
        self.player.post_update = post_update

    def arcade_custom_restart(self):
        self.arcade_custom_first_start()

    def check_on_inventory(self):
        chk = self.checker.tracker
        inventory = chk.get_variable_value_as_str('player_ingredient_inventory',"char")
        lvl.debug("Checking if player has an empty inventory after giving all ingredients")
        if inventory != '_':
            self.checker.failed("Player isn't ready to receive the crepe after giving the ingredients")

    def check_ingredients(self):
        chk = self.checker.tracker

        shop_map = chk.get_variable_value("m", as_raw_python_objects = True).value[0]
        shop_map_tiles = list(map(lambda x: re.sub(r"(^\d+\s')|'$",'',x), shop_map.tiles))

        left_ingredient = shop_map_tiles[24]
        right_ingredient = shop_map_tiles[25]
        lvl.debug(f"Checking if the ingredients given to the cook are correct : {left_ingredient} & {right_ingredient}")

        if left_ingredient != 'E':
            if left_ingredient == 'F':
                self.checker.failed("Flour found where eggs should be in the crepe shop")
            else:
                self.checker.failed(f"Unexpected ingredient '{left_ingredient}'")
        elif right_ingredient != 'F':
            if left_ingredient == 'E':
                self.checker.failed("Eggs found where flour should be in the crepe shop")
            else:
                self.checker.failed(f"Unexpected ingredient '{left_ingredient}'")
        else:
            self.checker.has_crepe = True

    crepe_input = [
                'RIGHT',
                'RIGHT',
                'RIGHT',
                'RIGHT',
                'RIGHT',
                'RIGHT',
                'RIGHT',
                'RIGHT',
                'RIGHT',
                'RIGHT',
                'RIGHT',
                'RIGHT',
                'RIGHT',
                'UP',
                'UP',
                'UP',
                'TAKE',
                'EXIT',
                'DOWN',
                'DOWN',
                'LEFT',
                'LEFT',
                'LEFT',
                'LEFT',
                'LEFT',
                'LEFT',
                'LEFT',
                'LEFT',
                'UP',
                'UP',
                'UP',
                'GIVE',
                'EXIT',
                'DOWN',
                'DOWN',
                'RIGHT',
                'RIGHT',
                'RIGHT',
                'RIGHT',
                'RIGHT',
                'RIGHT',
                'RIGHT',
                'RIGHT',
                'RIGHT',
                'RIGHT',
                'RIGHT',
                'RIGHT',
                'RIGHT',
                'RIGHT',
                'UP',
                'UP',
                'UP',
                'TAKE',
                'EXIT',
                'DOWN',
                'DOWN',
                'LEFT',
                'LEFT',
                'LEFT',
                'LEFT',
                'LEFT',
                'LEFT',
                'LEFT',
                'LEFT',
                'LEFT',
                'LEFT',
                'LEFT',
                'LEFT',
                'LEFT',
                'LEFT',
                'UP',
                'UP',
                'UP',
                'GIVE',
                'COOK',
                'EXIT',
                'DOWN',
                'DOWN',
                'RIGHT',
                'RIGHT',
                'RIGHT',
                'RIGHT',
                'RIGHT',
                'DOWN',
                'DOWN',
                'DOWN'
            ]

    def pre_validation(self):
        self.checker.has_crepe = False
        self.checker.register_watch("ready_to_cook", self.check_on_inventory)
        self.checker.register_breakpoint("cook_crepe", self.check_ingredients)
        self.checker.append_inputs(self.crepe_input)

    def post_validation(self):
        if not self.checker.has_crepe:
            self.checker.failed("Player didn't receive the crepe from the cook")

    def test_input(self, T):
        for i in self.crepe_input:
            T.send_input(i)

    def test(self):
        import tests.lib_test as T

        self.recompile_answer()
        self.test_input(T)
        T.ccontinue()
        T.expect_victory()
        self.run()
        self.check_validation(should_validate=True)
        
        self.recompile_bug()
        self.test_input(T)
        T.ccontinue()
        T.expect_defeat()
        self.run()

        self.recompile_fix('static_main_map')
        self.test_input(T)
        T.ccontinue()
        T.expect_victory()
        self.run()
        self.check_validation(should_validate=True)

        fixes = ['player_inventory',
                'cooking_conditions',
                'ingredients_already_in_shop',
                'no_verify_crepes',
                'always_ready']
        for fix in fixes:
            self.recompile_fix(fix)
            self.test_input(T)
            T.ccontinue()
            T.expect_victory()
            self.run
            self.check_validation(should_validate=False)
