/* @AGDB
 * level_title: crepes
 * exec_name: crepes
 * engine_name: map_stack
 * available_commands: edit next step continue interrupt
 *
 * interactive: True
 *
 *
 * arcade_maps: main main.tmx
 * arcade_maps: crepe_shop crepe_shop.tmx
 * arcade_maps: flour_shop flour_shop.tmx
 * arcade_maps: egg_shop egg_shop.tmx
 *
 * verify_init: cond player_ingredient_inventory == '_'
 * verify_init: cond ready_to_cook == 0
 * verify_always: change_in_function ready_to_cook check_ready_to_cook
 * verify_always: must_call check_ready_to_cook
 * verify_always: must_call cook_crepe
 * verify_always: must_call give_ingredient
 * verify_always: must_call verify_crepes
 * verify_exit: cond ready_to_cook == 1
 *
 *
 */

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "engine/agdbentures.h"
#include "engine/game.h"
#include "engine/map.h"

#define INSIDE_DOOR_X 3
#define INSIDE_DOOR_Y 5

// Ingredient is above the door in buildings
#define INGREDIENT_X INSIDE_DOOR_X
#define INGREDIENT_Y 3

// Coordinates of doors of buildings
#define MAIN_DOOR_X 9
#define EGG_DOOR_X 17
#define FLOUR_DOOR_X 23

// The cook is on the left of ingredients
#define COOK_X INGREDIENT_X - 1
#define COOK_Y INGREDIENT_Y

// exit coordinates
int exit_x = 14;
int exit_y = 10;

char str_map[] = "\
                             \n\
                             \n\
                             \n\
       +---+   +---+ +---+   \n\
#######+-D-+###+-D-+#+-D-+###\n\
        # #       ##    ##   \n\
              P   ##    ##   \n\
    >                        \n\
                     ##      \n\
          #          ##      \n\
              @              \n\
";

#ifndef FIX_INGREDIENTS_ALREADY_IN_SHOP
char crepe_shop_map[] = "\
+-----+\n\
|     |\n\
|     |\n\
| C== |\n\
|     |\n\
+--D--+\n\
";
#else
char crepe_shop_map[] = "\
+-----+\n\
|     |\n\
|     |\n\
| CEF |\n\
|     |\n\
+--D--+\n\
";
#endif

char flour_map[] = "\
+-----+\n\
|     |\n\
|     |\n\
|  F  |\n\
|     |\n\
+--D--+\n\
";

char egg_map[] = "\
+-----+\n\
|     |\n\
|     |\n\
|  E  |\n\
|     |\n\
+--D--+\n\
";

// Global map array for shop maps
// Crepe shop is 0, egg shop is 1 and flour shop is 2
map *maps[3] = {NULL, NULL, NULL};

// Global inventory that is empty at the beginning
#ifdef FIX_PLAYER_INVENTORY  // cheat : player has the crepe from the start
char player_ingredient_inventory = 'C';
#else
char player_ingredient_inventory = '_';
#endif

// Bool that tracks whether the cook can make a crepe or not
#ifdef FIX_COOKING_CONDITIONS  // cheat : player has already given the
                               // ingredients from the start
bool ready_to_cook = true;
#else
bool ready_to_cook = false;
#endif

void verify_crepes()
{
    if (player_ingredient_inventory != 'C') {
        printf("DEFEAT!\nYou need to have a crepe in your inventory\n");
        exit(EXIT_FAILURE);
    }
}

map *create_building_map(game_instance *game, char *building_name,
                         char *building_map, int map_number)
{
#ifdef BUG
    map *m = load_map(building_name, building_map);
    maps[map_number] = m;
#else
    map *m = maps[map_number];
    if (m == NULL) {
        m = load_map(building_name, building_map);
        maps[map_number] = m;
    }
#endif
    // Place the player in front of the door
    place_player(m, INSIDE_DOOR_Y - 1, INSIDE_DOOR_X);
    m->player_direction = DIR_UP;
    push_on_stack(game->map_stack, m);

    return m;
}

void enter_ingredient_building(game_instance *game, char *building_name)
{
    map *m;
    bool egg_shop = !strcmp(building_name, "egg_shop");
    char *strmap;
    int map_number;
    const char welcome_format[] =
        "Entering the *%s SHOP*.\nType TAKE (UP ARROW) to take the "
        "ingredient,\nor EXIT (DOWN ARROW) in the terminal.";
    char welcome_message[150];

    if (egg_shop) {
        strmap = egg_map;
        map_number = 1;
        sprintf(welcome_message, welcome_format, "EGG");
    } else {
        assert(!strcmp(building_name, "flour_shop"));
        strmap = flour_map;
        map_number = 2;
        sprintf(welcome_message, welcome_format, "FLOUR");
    }

    m = create_building_map(game, building_name, strmap, map_number);
    message(welcome_message);

    // infinite command loop until exit
    int must_exit = 0;
    while (must_exit == 0) {
        show_map(m);
        printf("Shop building command:\n");
        const command *command = get_next_command();
        if (!command)
            break;

        if (!strcmp(command->command_buffer, "TAKE") ||
            !strcmp(command->command_buffer, "UP")) {
            // if the ingredient is still there we can take it
            if (player_ingredient_inventory == '_') {
                entity ent = get_entity(m, INGREDIENT_Y, INGREDIENT_X);
                switch(ent) {
                case 'E':
                case 'F':
                    player_ingredient_inventory = ent;
                    remove_entity(m, INGREDIENT_Y, INGREDIENT_X);
                    printf("Taken %c in inventory\n",
                           player_ingredient_inventory);
                    if (ent == 'E') {
                        message("You take the egg ingredient.");
                    } else {
                        message("You take the flour ingredient.");
                    }
                    break;
                default:
                    printf("No ingredient to take\n");
                    message("There is no ingredient to take!");
                }
            } else {
                // if the inventory is full we don't take it
                printf("Inventory is full\n");
                message(
                    "You cannot take an ingredient : your inventory is full!");
            }

        } else if (!strcmp(command->command_buffer, "GIVE")) {
            message("Keep your ingredient for the crepe shop !");
        } else if (!strcmp(command->command_buffer, "COOK")) {
            message("You cannot cook here !");
        } else if (!strcmp(command->command_buffer, "EXIT") ||
                   !strcmp(command->command_buffer, "DOWN")) {
            must_exit = 1;
        } else {
            printf("Invalid building command\n");
        }
    }
    printf("Exiting building %s\n", building_name);
}

// Give the player inventory and place it at the given X position
void give_ingredient(map *m, int ingredient_x)
{
    place_entity(m, player_ingredient_inventory, COOK_Y, ingredient_x);
    player_ingredient_inventory = '_';
    printf("Ingredients: '%c' '%c'\n",
            get_entity(m, COOK_Y, COOK_X + 1),
            get_entity(m, COOK_Y, COOK_X + 2));
}

/**
 * Verify if the necessary ingredients are available.
 * If so, activates the \p ready_to_cook global boolean.
 */
void check_ready_to_cook(map *m)
{
    assert(!strcmp(m->name, "crepe_shop"));
    // Check which ingredients are already available
#ifndef FIX_ALWAYS_READY
    char first_ingredient = get_entity(m, COOK_Y, COOK_X + 1);
    char second_ingredient = get_entity(m, COOK_Y, COOK_X + 2);

    assert(player_ingredient_inventory == 'E' ||
           player_ingredient_inventory == 'F');

    if (first_ingredient == '=') {
        give_ingredient(m, COOK_X + 1);
    } else if (second_ingredient == '=' &&
               player_ingredient_inventory != first_ingredient) {
        give_ingredient(m, COOK_X + 2);
        // Crepe can be made now
        ready_to_cook = true;
    } else {
        printf("The ingredient %c is not interesting\n",
               player_ingredient_inventory);
    }
#else
    ready_to_cook = true;
#endif
}

void cook_crepe(map *m)
{
    // "Delete" the ingredients from the map
    place_entity(m, '=', COOK_Y, COOK_X + 1);
    place_entity(m, '=', COOK_Y, COOK_X + 2);
    // Give a crepe to the player
    player_ingredient_inventory = 'C';
}

void enter_main_building(game_instance *game)
{
#ifndef FIX_STATIC_MAIN_MAP
    map *m = create_building_map(game, "crepe_shop", crepe_shop_map, 0);
#else
    // try using a static map here (alternative answer)
    static map *m = NULL;
    if (!m) {
        m = create_building_map(game, "crepe_shop", crepe_shop_map, 0);
    } else {
        push_on_stack(game->map_stack, m);
    }
#endif

    message("Welcome to the *CREPES SHOP*.\nType GIVE (UP ARROW) to give an "
            "ingredient,\nCOOK (A) to make a crepe,\nor EXIT (DOWN ARROW) in "
            "the terminal.");

    // infinite command loop until exit
    int must_exit = 0;
    while (must_exit == 0) {
        show_map(m);
        printf("Crepe shop building command:\n");
        const command *command = get_next_command();
        if (!command)
            break;

        if (!strcmp(command->command_buffer, "GIVE") ||
            !strcmp(command->command_buffer, "UP")) {
            // give an ingredient if this is in the inventory
            switch (player_ingredient_inventory) {
            case '_':
                message("You cannot give an ingredient, since you don't have "
                        "any in your inventory!");
                break;
            case 'E':
                message("Thank you for the egg!");
                // check if we have given enough ingredients
                check_ready_to_cook(m);
                break;
            case 'F':
                message("Thank you for the flour!");
                // check if we have given enough ingredients
                check_ready_to_cook(m);
                break;
            case 'C':
                message("No, keep your crepe!\nYou need it to validate the "
                        "level !");
                break;
            default:
                printf(
                    "ERROR : unknown ingredient in the player's inventory\n");
            }
        } else if (!strcmp(command->command_buffer, "TAKE")) {
            message("You cannot take any ingredient in the crepes shop !");
        } else if (!strcmp(command->command_buffer, "COOK") ||
                   !strcmp(command->command_buffer, "C")) {
            if (ready_to_cook) {
                cook_crepe(m);
                message("Alright, here is your crepe !\nYou can now validate "
                        "the level.");
            } else {
                message("I can't cook a crepe yet !\nI need the two "
                        "ingredients !\nYou will find them in the ingredients "
                        "shops next to mine.");
            }
        } else if (!strcmp(command->command_buffer, "EXIT") ||
                   !strcmp(command->command_buffer, "DOWN")) {
            must_exit = 1;
        } else {
            printf("Invalid building command\n");
        }
    }

    printf("Exiting crepes shop building\n");
}

void enter_building(game_instance *game, int door_x)
{
    if (door_x == MAIN_DOOR_X) {
        enter_main_building(game);
    } else if (door_x == EGG_DOOR_X) {
        enter_ingredient_building(game, "egg_shop");
    } else if (door_x == FLOUR_DOOR_X) {
        enter_ingredient_building(game, "flour_shop");
    } else {
        printf("Invalid door position for enter_building\n");
        exit(EXIT_FAILURE);
    }
    // We exit the functions when the character exits the building.
    pop_from_stack(game->map_stack);

    // Orient the character toward the outside when exiting.
    current_map(game->map_stack)->player_direction = DIR_DOWN;
}

int main()
{
    game_instance *game = init_game();
    map *main_map = load_map("main", str_map);

    push_on_stack(game->map_stack, main_map);

    // main game loop
    while (!game->exit_main_loop) {
        // read command
        command *cmd;
        cmd = get_next_command();
        if (!cmd) break;

        // Check for doors & exit
        entity next = next_tile(main_map, cmd);
        printf("NEXT: %c\n", next);
        if(next == 'D') {
            enter_building(game, main_map->player_x);
        }else if(next == '@'){
            game->exit_main_loop = true;
        }
        
        // Move player
        apply_input(main_map, cmd);
        show_map(main_map);
    }

#ifndef FIX_NO_VERIFY_CREPES
    verify_crepes();
#endif
    verify_exit(main_map);
}
