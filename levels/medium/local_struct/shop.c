#include "shop.h"

const int weapon_price[WEAPONS_N] = {0, 1, 2, 5};
const int armor_price[ARMORS_N] = {0, 2, 4, 8};

int gold = 0;

weapon_type buy_weapon(weapon_type weapon)
{
    printf("You try to buy a weapon...\n");
    weapon_type widx = WEAPONS_N - 1;
    while (widx > 0 && gold < weapon_price[widx]) {
        widx--;
    }
    if (widx > weapon) {
        printf("You bought a new weapon!\n");
        gold -= weapon_price[widx];
    } else {
        printf("You already have a better weapon!\n");
    }

    return (weapon_type)widx > weapon ? (weapon_type)widx : (weapon_type)weapon;
}

armor_type buy_armor(armor_type armor)
{
    printf("You try to buy armor...\n");
    armor_type aidx = ARMORS_N - 1;
    while (aidx > 0 && gold < armor_price[aidx]) {
        aidx--;
    }
    if (aidx > armor) {
        printf("You bought a new armor!\n");
        gold -= armor_price[aidx];
    } else {
        printf("You already have a better armor!\n");
    }

    return (armor_type)aidx > armor ? (armor_type)aidx : (armor_type)armor;
}
