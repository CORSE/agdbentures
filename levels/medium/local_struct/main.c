/* @AGDB
 * level_title: Local_structure
 * exec_name: local_struct
 * engine_name: map_stack
 * available_commands: edit next step continue interrupt
 *
 * arcade_maps: main main.tmx
 * arcade_maps: shop shop.tmx
 * arcade_skins: guard_sword :characters:Soldier/Soldier 02-1.png
 * arcade_skins: guard_shield :characters:Soldier/Soldier 03-1.png
 *
 *
 * Bug idea... a local structure representing the character is passed without
 * pointer in a function call. An armor & sword are "bought" in the "weapon
 * store" (function), but "disappear" as the character leaves the shop (exit).
 *
 * Tags: #pointer, #local, #structure
 *
 *
 * OBJenemy: messageFR defeat
 * Tu m'as vaincu, je te laisse passer...
 * EndOfMessage
 * OBJenemy: messageEN defeat
 * You defeated me, you can pass...
 * EndOfMessage
 *
 * no_verify: change_in_function map->player_y move_player
 * no_verify: change_in_function map->player_x move_player
 *
 * verify_always: cond exit_x == 34
 * verify_init: cond player_life == 100
 * verify_init: cond enemy_life == 100
 *
 * Engine
 * - player_x, player_y, direction are globals
 * - reading basic maps
 * - stack of maps (entering / leaving)
 * - non-moving hardcoded enemys
 * - hardcoded discussion with Shopkeeper
 * - gold as global variable
 * - basic buy system (buy highest possible)
 * - verify level based on position at exit
 *
 * Checks
 * - buys are done in the shop (i.e., in function enter_shop)
 *   · watch buy_weapon and buy_armor
 * - equipment is not modify outside the shop
 *   · watch memory for equipment
 * - exit position is not changed
 * - verify_exit is called
 * - troll is defeated in combat and there is no new path to the exit
 *   · difficult to check
 */


// maybe remove some test in metadata
#include "engine/agdbentures.h"
#include "engine/game.h"
#include "engine/map.h"
#include "shop.h"
#include <stdlib.h>
#include <string.h>

/*************************************************************
 * Level objectives:
 * Buy armor and weapon, and defeat the Troll on the brige.
 *************************************************************/

#define MAIN_DOOR_X 19
#define MAIN_DOOR_Y 2

int exit_x = 34;
int exit_y = 4;

int player_life = 100;
int enemy_life = 100;

/**
 * In this map, a lot of gold, a shop with a door D, and a troll T guarding the
 * bridge leading to the exit.
 * */
char str_map[] = "\
         $    $         $  ~~~~~        \n\
  $     $       +-----+    ~~~~~        \n\
$   $           +--D--+ -- ~~~~~        \n\
                     +  -  ~~~~~        \n\
  ----------------        $  T     @    \n\
 >    $     $              ~~~~~        \n\
$                 $    $   ~~~~~        \n\
";

/**
 * The map for the shop. A door at the bottom, and a shopkeeper S behind its
 * counter, and items of weaponry behind him/her.
 */
char str_shop[] = "\
+-------+\n\
|[[[/)))|\n\
|   S   |\n\
+-------+\n\
|       |\n\
|       |\n\
+---D---+\n\
";

void player_attack(weapon_type weapon) {
  int damage = 10;
  if (weapon == DAGGER) {
    damage = 15;
  } else if (weapon == SHORT_SWORD) {
    damage = 25;
  } else if (weapon == LONG_SWORD) {
    damage = 25;
  }

  enemy_life -= damage;
}

void enemy_attack(armor_type armor) {
  int damage = 45;
  if (armor == LEATHER_JACKET) {
    damage = 40;
  } else if (armor == CHAINMAIL) {
    damage = 35;
  } else if (armor == FULLPLATE) {
    damage = 30;
  }

  player_life -= damage;  
}

void fight_enemy(char enemy, equipment eq) {
  if(enemy == 'S') {
    printf("You can't attack the shopkeeper\n");
    exit(EXIT_FAILURE);
  } else {
    while (player_life > 0 && enemy_life > 0) {
      printf("You attack the enemy\n");
      player_attack(eq.weapon);
      printf("Player life: %d\nenemy life: %d\n", player_life < 0 ? 0 : player_life, enemy_life < 0 ? 0 : enemy_life);
      if (enemy_life > 0) {
        printf("The enemy attacks you\n");
        enemy_attack(eq.armor);
        printf("Player life: %d\nenemy life: %d\n", player_life < 0 ? 0 : player_life, enemy_life < 0 ? 0 : enemy_life);
      }
    }

    if (player_life <= 0) {
      printf("You died\n");
      exit(EXIT_FAILURE);
    } else {
      printf("You defeated the enemy\n");
    }
  }
}

void add_coin() {
  gold++;
}

void print_equipement(equipment eq) {
  printf("Your weapon: ");
  switch (eq.weapon) {
    case NO_WEAPON:
      printf("\033[1;31mNone\033[0m\n");
      break;
    case DAGGER:
      printf("\033[1;31mDagger\033[0m\n");
      break;
    case SHORT_SWORD:
      printf("\033[1;31mShort sword\033[0m\n");
      break;
    case LONG_SWORD:
      printf("\033[1;31mLong sword\033[0m\n");
      break;
    default:
      printf("Unknown\n");
      break;
  }

  printf("Your armor: ");
  switch (eq.armor) {
    case NO_ARMOR:
      printf("\033[1;31mNone\033[0m\n");
      break;
    case LEATHER_JACKET:
      printf("\033[1;31mLeather jacket\033[0m\n");
      break;
    case CHAINMAIL:
      printf("\033[1;31mChainmail\033[0m\n");
      break;
    case FULLPLATE:
      printf("\033[1;31mFull plate\033[0m\n");
      break;
    default:
      printf("Unknown\n");
      break;
  }
}

/**
 * Function called when entering the shop to buy equipment. Parameter is the
 * equipement to update when buying.
 */
#ifdef BUG
void enter_shop(game_instance* game, equipment eq)
#else
void enter_shop(game_instance* game, equipment* eq)
#endif
{
  printf("You enter a shop.\n");
  map* m = load_map("shop", str_shop);
  push_on_stack(game->map_stack, m);

  /* place player above the door */
  place_player(m, 5, 4);

  bool leave_shop = false;

  message("Welcome to my shop!\nYou can say BUY to buy equipment or EXIT to leave.\n");
  message("Here is what I have:\nWEAPONS:\nDagger: 1 gold\nShort sword: 2 gold\nLong sword: 5 gold\n\nARMORS:\nLeather jacket: 2 gold\nChainmail: 4 gold\nFull plate: 8 gold\n");
  while (!leave_shop) {
#ifdef BUG
    print_equipement(eq);
#else
    print_equipement(*eq);
#endif
    printf("Gold: \033[1;33m%d\033[0m\n", gold);
    /* apply_command(); */
    command* cmd = get_next_command();

    if (strcmp(cmd->command_buffer, "EXIT") == 0 ||
        strcmp(cmd->command_buffer, "DOWN") == 0) {
      leave_shop = true;
    } else if (strcmp(cmd->command_buffer, "BUY") == 0 ||
               strcmp(cmd->command_buffer, "UP") == 0) {
/* buy a weapon */
#ifdef BUG
        eq.weapon = buy_weapon((int)eq.weapon);
        eq.armor = buy_armor((int)eq.armor);
#else
        eq->weapon = buy_weapon((int)eq->weapon);
        eq->armor = buy_armor((int)eq->armor);
#endif
    } else if (strcmp(cmd->command_buffer, "FIGHT") == 0) {
      printf("You will regret this!\n");
#ifdef BUG
        fight_enemy('S', eq);
#else
        fight_enemy('S', *eq);
#endif
    } else {
        fprintf(stderr, "Cannot handle this command!\n");
    }

    if (m->player_x == 4 && m->player_y == 5 &&
        m->player_direction == DIR_DOWN) {
      /* leaving the shop */
      leave_shop = true;
    }
  }

  /* forward(); */
  /* talk(); */

  pop_from_stack(game->map_stack);
  /* BUGIDEA: in a previous level, introduce the idea of stack of maps and
   * forgetting to unload */
  printf("You leave the shop.\n");
  return;
}

int main(void) {
  game_instance* game = init_game();
  push_on_stack(game->map_stack, load_map("main", str_map));
  map* m = current_map(game->map_stack);

  /* We started the level without weapons */
  equipment my_eq;
  my_eq.weapon = NO_WEAPON;
  my_eq.armor = NO_ARMOR;

  /* Initialize the first map */

  printf("Enter your commands (UP, DOWN, LEFT, RIGHT...):\n");
  while (m->player_x != exit_x || m->player_y != exit_y) {
    print_equipement(my_eq);
    printf("Gold: \033[1;33m%d\033[0m\n", gold);
    /* apply_command(); */
    command* cmd = get_next_command();
    if (strcmp(cmd->command_buffer, "EXIT") == 0) {
      break;
    } else {
      char dest = next_tile(m, cmd);
      apply_input(m, cmd);
      if (dest == '$') {
        add_coin();
      } else if (dest == 'T') {
        /* now is time to fight the troll! */
        fight_enemy('T', my_eq);
      } else if (dest == 'D'){
        /* entering the shop */
        /* Now buy some armor + weapon so we can defeat the troll. */
        #ifdef BUG
          enter_shop(game, my_eq);
        #else
          enter_shop(game, &my_eq);
        #endif

      }
    }
  }
  /* hopefully we are now at the exit */
  /* forward_n(10); */
  map* map = current_map(game->map_stack);
  verify_exit(map);
}
