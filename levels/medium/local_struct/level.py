#!/usr/bin/env python3

from level.level_abc import AbstractLevel
from level.action import Action
import graphic.constants as cst
from language import lmessage, Lang

import time

from level.world import find_var_in_frame


class Level(AbstractLevel):
    """local struct"""

    number_of_coins: int
    previous_x: int
    previous_y: int
    amount_of_gold: int
    player_life: int
    monster_life: int
    first_time_attack: bool

    def arcade_custom_restart(self):
        self.arcade_custom_first_start()

    def arcade_custom_first_start(self):
        self.number_of_coins = 0
        self.previous_x = -5
        self.previous_y = -5
        self.amount_of_gold = 0
        self.player_life = 100
        self.monster_life = 100
        self.first_time_attack = True

        self.previous_number_of_coins = -1
        self.previous_weapon = None
        self.previous_armor = None
        self.current_map = "main"
        player = self.world.get_player()
        payload = {
            "topic": "player_update",
            "action": Action("change_skin"),
            "skin": "player_main",
        }
        player.send_update(payload)
        enemy = self.world.get_object("enemy")
        enemy.place_at(29, 4)
        enemy.direction = cst.Direction.LEFT
        enemy.visible = True
        enemy.send_update()
        player_equipments = ["No Weapon", "No Armor"]
        self.current_weapon = "No Weapon"
        self.current_armor = "No Armor"
        self.price_of_items = {
            "No Weapon": 0,
            "No Armor": 0,
            "Dagger": 1,
            "Short Sword": 2,
            "Long Sword": 5,
            "Leather Jacket": 2,
            "Chainmail": 4,
            "Fullplate": 8,
        }

        def change_map(map_name):
            self.current_map = map_name
            # by default sprites are visible and we hide them if needed
            payload = {
                "topic": "map_change",
                "map": map_name,
            }
            if map_name == "main":
                player.direction = cst.Direction.DOWN
                player.send_update()
            player.send_update(payload)
            payload = {"topic": "player_update", "action": Action("rescale")}
            self.player.send_update(payload)

        def hide_coins():
            self.update_all()
            pos_x, pos_y = player.coord_x, player.coord_y
            if self.previous_x != pos_x or self.previous_y != pos_y:
                self.number_of_coins += 1
            payload = {
                "topic": "sprites",
                "action": Action("hide"),
                "layer": "decorations",
                "locations": [
                    (pos_x, pos_y),
                ],
            }
            self.previous_x = pos_x
            self.previous_y = pos_y
            self.send_to_gui(payload)

        def weapon_and_armor_text(type, eq):
            if eq == "DAGGER":
                return "Dagger"
            elif eq == "SHORT_SWORD":
                return "Short Sword"
            elif eq == "LONG_SWORD":
                return "Long Sword"
            elif eq == "LEATHER_JACKET":
                return "Leather Jacket"
            elif eq == "CHAINMAIL":
                return "Chainmail"
            elif eq == "FULLPLATE":
                return "Fullplate"
            else:
                if type == "weapon":
                    return "No Weapon"
                else:
                    return "No Armor"

        def player_map_detection(player, memory):
            frames = memory["stack"]
            building = None
            equipments = None
            for frame in frames:
                if frame.name == "enter_shop":
                    if self.world.get_variable("eq"):
                        equi = self.world.get_variable("eq")
                        if type(equi) == tuple:
                            equipments = equi[0].__dict__
                        else:
                            equipments = equi.__dict__
                        building = "shop"
                else:
                    if self.world.get_variable("my_eq"):
                        equipments = self.world.get_variable("my_eq").__dict__
            if equipments:
                for type_eq, eq in equipments.items():
                    if type_eq == "weapon":
                        weapon = weapon_and_armor_text(type_eq, eq)
                        player_equipments[0] = weapon

                        # Change the skin of the player if he has a new weapon
                        if self.current_weapon != weapon:
                            self.current_weapon = weapon
                            payload = {
                                "topic": "player_update",
                                "action": Action("change_skin"),
                                "skin": "guard_sword",
                            }
                            player.send_update(payload)
                            self.number_of_coins -= self.price_of_items[weapon]
                    if type_eq == "armor":
                        armor = weapon_and_armor_text(type_eq, eq)
                        player_equipments[1] = armor

                        # Change the skin of the player if he has a new armor
                        if self.current_armor != armor:
                            self.current_armor = armor
                            payload = {
                                "topic": "player_update",
                                "action": Action("change_skin"),
                                "skin": "guard_shield",
                            }
                            player.send_update(payload)
                            self.number_of_coins -= self.price_of_items[armor]

            if building:
                if self.current_map != building:
                    # we entered a new building
                    change_map(building)
            else:
                if self.current_map != "main":
                    # we exited the building
                    change_map("main")

        def show_player_life():
            player_life = self.tracker.get_variable_value_as_str("player_life", "int")
            payload = {
                "topic": "gui_change",
                "action": "change_health_bar",
                "who": "player",
                "life": player_life if player_life > 0 else 0,
            }
            self.send_to_gui(payload)

        def show_enemy_life():
            enemy_life = self.tracker.get_variable_value_as_str("enemy_life", "int")
            payload = {
                "topic": "gui_change",
                "action": "change_health_bar",
                "who": "enemy",
                "life": enemy_life if enemy_life > 0 else 0,
            }
            self.send_to_gui(payload)
            if enemy_life == 0:
                pl = {
                    "topic": "object_update",
                    "object_name": "enemy",
                    "action": Action("fall"),
                }
                self.send_to_gui(pl)
                enemy.talks("defeat")

        def post_update(player, memory):
            player_map_detection(player, memory)
            if self.number_of_coins != self.previous_number_of_coins:
                payload = {
                    "topic": "gui_change",
                    "action": "show_text",
                    "text": f"Gold: {self.number_of_coins}",
                    "index": 0,
                }
                self.send_to_gui(payload)
                self.previous_number_of_coins = self.number_of_coins

            if self.previous_weapon != player_equipments[0]:
                payload = {
                    "topic": "gui_change",
                    "action": "show_text",
                    "text": f"Weapon: {player_equipments[0]}",
                    "index": 1,
                }
                self.send_to_gui(payload)

            if self.previous_armor != player_equipments[1]:
                payload = {
                    "topic": "gui_change",
                    "action": "show_text",
                    "text": f"Armor: {player_equipments[1]}",
                    "index": 2,
                }
                self.send_to_gui(payload)

        def enemy_attack():
            enemy.coord_x -= 1
            enemy.send_update()
            enemy.coord_x += 1
            enemy.send_update()
            pl = player.payload()
            pl['action'] = Action('hit')
            player.send_update(pl)
            show_player_life()

            # TODO: change this to an action when the branch dev/tutoriel_dev is merged into main
            time.sleep(0.1)

        def player_attack():
            if self.first_time_attack:
                player.coord_x -= 1
                player.send_update()
                self.first_time_attack = False
            player.coord_x += 1
            player.send_update()
            player.coord_x -= 1
            player.send_update()
            pl = enemy.payload()
            pl['action'] = Action('hit')
            enemy.send_update(pl)

            show_enemy_life()

            # TODO: change this to an action when the branch dev/tutoriel_dev is merged into main
            time.sleep(0.1)

        show_player_life()
        show_enemy_life()
        player.post_update = post_update
        self.register_breakpoint("add_coin", hide_coins)
        self.register_leave_function("enemy_attack", enemy_attack)
        self.register_leave_function("player_attack", player_attack)

    def pre_validation(self):
        self.checker.append_inputs(
            [
                'LEFT',
                'DOWN',
                'UP',
                'UP',
                'UP',
                'UP',
                'RIGHT',
                'RIGHT',
                'UP',
                'DOWN',
                'RIGHT',
                'RIGHT',
                'RIGHT',
                'RIGHT',
                'RIGHT',
                'RIGHT',
                'UP',
                'UP',
                'RIGHT',
                'RIGHT',
                'RIGHT',
                'RIGHT',
                'RIGHT',
                'RIGHT',
                'RIGHT',
                'RIGHT',
                'RIGHT',
                'RIGHT',
                'RIGHT',
                'RIGHT',
                'RIGHT',
                'RIGHT',
                'RIGHT',
                'RIGHT',
                'RIGHT',
                'RIGHT',
                'DOWN',
                'DOWN',
                'DOWN',
                'DOWN',
                'DOWN',
                'DOWN',
                'LEFT',
                'LEFT',
                'LEFT',
                'LEFT',
                'LEFT',
                'LEFT',
                'LEFT',
                'LEFT',
                'UP',
                'LEFT',
                'LEFT',
                'LEFT',
                'LEFT',
                'LEFT',
                'LEFT',
                'LEFT',
                'LEFT',
                'LEFT',
                'LEFT',
                'LEFT',
                'LEFT',
                'RIGHT',
                'RIGHT',
                'RIGHT',
                'RIGHT',
                'RIGHT',
                'RIGHT',
                'RIGHT',
                'RIGHT',
                'RIGHT',
                'RIGHT',
                'RIGHT',
                'RIGHT',
                'RIGHT',
                'UP',
                'UP',
                'UP',
                'BUY',
                'DOWN',
                'DOWN',
                'RIGHT',
                'RIGHT',
                'RIGHT',
                'RIGHT',
                'RIGHT',
                'RIGHT',
                'RIGHT',
                'RIGHT',
                'RIGHT',
                'RIGHT',
                'RIGHT',
                'RIGHT',
                'RIGHT',
                'RIGHT',
                'RIGHT',
            ]
        )
        self.checker.register_breakpoint('add_coin', self.add_gold)
        self.checker.register_watch("my_eq", self.check_equipement)
        self.checker.register_watch("enemy_life", self.check_weapon)
        self.checker.register_watch("player_life", self.check_armor)

    def check_armor(self):
        armor = self.checker.tracker.get_variable_value_as_str("armor", "int")
        life = self.checker.tracker.get_variable_value_as_str("player_life", "int")
        match armor:
            case 0:
                if self.player_life - 45 == life:
                    self.player_life = life
                else:
                    self.print_checker()
                return
            case 1:
                if self.player_life - 40 == life:
                    self.player_life = life
                else:
                    self.print_checker()
                return
            case 2:
                if self.player_life - 35 == life:
                    self.player_life = life
                else:
                    self.print_checker()
                return
            case 3:
                if self.player_life - 30 == life:
                    self.player_life = life
                else:
                    self.print_checker()
                return

    def check_weapon(self):
        weapon = self.checker.tracker.get_variable_value_as_str("weapon", "int")
        life = self.checker.tracker.get_variable_value_as_str("enemy_life", "int")
        match weapon:
            case 0:
                if self.monster_life - 10 == life:
                    self.monster_life = life
                else:
                    self.print_checker()
                return
            case 1:
                if self.monster_life - 15 == life:
                    self.monster_life = life
                else:
                    self.print_checker()
                return
            case 2:
                if self.monster_life - 25 == life:
                    self.monster_life = life
                else:
                    self.print_checker()
                return
            case 3:
                if self.monster_life - 25 == life:
                    self.monster_life = life
                else:
                    self.print_checker()
                return

    def print_checker(self):
        self.checker.failed(
            lmessage(
                {
                    Lang.EN: "You cheat during the fight.",
                    Lang.FR: "Vous avez triché durant le combat.",
                }
            )
        )

    def check_equipement(self):
        possible_value = [0, 1, 2, 3, 4]
        if not (self.checker.tracker_in_function("enter_shop")):
            weapon = self.checker.tracker.get_variable_value_as_str(
                "my_eq.weapon", "int"
            )
            armor = self.checker.tracker.get_variable_value_as_str("my_eq.armor", "int")
            if weapon not in possible_value or armor not in possible_value:
                weapon = self.checker.tracker.get_variable_value_as_str(
                    "my_eq->weapon", "int"
                )
                armor = self.checker.tracker.get_variable_value_as_str(
                    "my_eq->armor", "int"
                )
            if weapon not in possible_value or armor not in possible_value:
                weapon = self.checker.tracker.get_variable_value_as_str(
                    "eq->weapon", "int"
                )
                armor = self.checker.tracker.get_variable_value_as_str(
                    "eq->armor", "int"
                )
            if weapon not in possible_value or armor not in possible_value:
                weapon = self.checker.tracker.get_variable_value_as_str(
                    "eq.weapon", "int"
                )
                armor = self.checker.tracker.get_variable_value_as_str(
                    "eq.armor", "int"
                )

            if weapon != 0 or armor != 0:
                self.checker.failed(
                    lmessage(
                        {
                            Lang.EN: "You can change your stuff only if you buy it.",
                            Lang.FR: "Vous ne pouvez changer votre équipement qu'en l'achetant.",
                        }
                    )
                )

    def add_gold(self):
        valeur = self.checker.tracker.get_variable_value_as_str("gold", "int")
        if valeur - self.amount_of_gold > 1:
            if valeur != 0:
                self.checker.failed(
                    lmessage(
                        {
                            Lang.EN: "Amount of gold need change only in add_coin function.",
                            Lang.FR: "Le montant d'argent ne doit changer que dans la fonction add_coin .",
                        }
                    )
                )
        self.amount_of_gold = valeur

    def post_validation(self):
        pass

    def test(self):
        import tests.lib_test as T

        self.recompile_answer()
        T.send_input('LEFT')
        T.send_input('DOWN')
        for _ in range(4):
            T.send_input('UP')
        T.send_input('RIGHT')
        T.send_input('RIGHT')
        T.send_input('UP')
        T.send_input('DOWN')
        for _ in range(6):
            T.send_input('RIGHT')
        T.send_input('UP')
        T.send_input('UP')
        for _ in range(18):
            T.send_input('RIGHT')
        for _ in range(6):
            T.send_input('DOWN')
        for _ in range(8):
            T.send_input('LEFT')
        T.send_input('UP')
        for _ in range(12):
            T.send_input('LEFT')
        for _ in range(13):
            T.send_input('RIGHT')
        T.send_input('UP')
        T.send_input('UP')
        T.send_input('UP')
        T.send_input('BUY')
        T.send_input('DOWN')
        T.send_input('DOWN')

        for _ in range(27):
            T.send_input('RIGHT')
        T.ccontinue()
        T.expect_victory()
        self.run()
        self.recompile_bug()
        T.send_input('LEFT')
        T.send_input('DOWN')
        for _ in range(4):
            T.send_input('UP')
        T.send_input('RIGHT')
        T.send_input('RIGHT')
        T.send_input('UP')
        T.send_input('DOWN')
        for _ in range(6):
            T.send_input('RIGHT')
        T.send_input('UP')
        T.send_input('UP')
        for _ in range(18):
            T.send_input('RIGHT')
        for _ in range(6):
            T.send_input('DOWN')
        for _ in range(8):
            T.send_input('LEFT')
        T.send_input('UP')
        for _ in range(12):
            T.send_input('LEFT')
        for _ in range(13):
            T.send_input('RIGHT')
        T.send_input('UP')
        T.send_input('UP')
        T.send_input('UP')
        T.send_input('BUY')
        T.send_input('DOWN')
        T.send_input('DOWN')
        for _ in range(27):
            T.send_input('RIGHT')
        T.ccontinue()
        T.expect_defeat()
        self.run()
