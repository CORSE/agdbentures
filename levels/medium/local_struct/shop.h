#ifndef SHOP_H
#define SHOP_H

#include <stdio.h>

extern int gold;

/**
 * Different types of weapons
 */
typedef enum {
  NO_WEAPON,
  DAGGER,
  SHORT_SWORD,
  LONG_SWORD,
  WEAPONS_N
} weapon_type;

/**
 * Different types of armor
 */
typedef enum {
  NO_ARMOR,
  LEATHER_JACKET,
  CHAINMAIL,
  FULLPLATE,
  ARMORS_N
} armor_type;

typedef struct {
  weapon_type weapon;
  armor_type armor;
} equipment;

/**
 * Buy the most expensive weapon possible.
 */
weapon_type buy_weapon(weapon_type weapon);

/**
 * Buy the most expensive armor possible.
 */
armor_type buy_armor(armor_type armor);

#endif  // SHOP_H
