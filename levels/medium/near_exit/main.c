/* @AGDB
 * level_title: Near exit
 * exec_name: near_exit
 * engine_name: events
 * engine_bug : True
 * engine_tags:
 * engine_antitags:
 * no_verify player_x
 * available_commands: next step edit continue interrupt
 *
 * no_verify_exit: cond map->player_x == exit_x
 * no_verify_exit: cond map->player_y == exit_y
 * player_mode: simple_map
 * arcade_maps: main near_exit.tmx
 * coordinates: wop_init wop_end
 * WOP: messageEN intro
 *  I made some change in the engine, come with me to the end
 * EndOfMessage
 * WOP: messageFR intro
 * J'ai fait un peu changement dans l'engine, viens avec moi à la sortie
 * EndOfMessage

 */

#include "engine/agdbentures.h"
#include <stdlib.h>

int exit_x = 16;
int exit_y = 5;

char str_map[] = "\
                             \n\
-----------------------------\n\
                             \n\
                             \n\
                             \n\
            >   @            \n\
                             \n\
                             \n\
                             \n\
-----------------------------\n\
                             \n\
";


int main() {
    game_instance *game = init_game();
    map *current_map = load_map("main", str_map);

    push_on_stack(game->map_stack, current_map);
    game->current_map = current_map;
    // The engine change ;)
#ifdef BUG
    add_event(game,verify_exit(game));
#else
    add_event(game,verify_exit);
#endif
    command *command;
    while (!game->exit_main_loop) {
        command = get_next_command();
        apply_input(current_map, command);
        free_command(command);
        apply_events(game);
    }
    free_game(game);
    exit(EXIT_SUCCESS);
}