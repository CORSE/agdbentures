#!/usr/bin/env python3

from level.level_abc import AbstractLevel
from logs import lvl
import graphic.constants as cst
from level.action import Action


class Level(AbstractLevel):
    """near_exit"""

    i: int = 0

    def arcade_custom_restart(self):
        self.arcade_custom_first_start()

    def arcade_custom_first_start(self):
        self.wop = self.world.get_wop()
        init = self.world.get_coordinates("wop_init")
        self.end = self.world.get_coordinates("wop_end")
        self.wop.place_on(init)
        self.wop.visible = True
        self.wop.send_update()
        self.wop.talk("intro")
        self.wop.place_on(self.end)
        self.wop.direction = cst.Direction.DOWN
        self.wop.send_update()

    def pre_validation(self):
        self.checker.append_inputs(
            [
                'RIGHT',
                'RIGHT',
                'RIGHT',
                'RIGHT',
            ]
        )

    def post_validation(self):
        pass

    def test(self):
        import tests.lib_test as T

        self.recompile_answer()
        T.send_input("RIGHT")
        T.send_input("RIGHT")
        T.send_input("RIGHT")
        T.send_input("RIGHT")
        T.ccontinue()
        T.expect_victory()
        self.run()
        self.recompile_bug()
        T.send_input("RIGHT")
        T.ccontinue()
        T.expect_segfault()
        self.run()
