#!/usr/bin/env python3

from level.level_abc import AbstractLevel
from logs import cus
import graphic.constants as cst


class Level(AbstractLevel):

    def hide_door(self):
        self.door_closed = False
        self.door.visible = False
        self.door.send_update()

    def hide_prisoner(self):
        self.prisoner.visible = False
        self.prisoner.send_update()

    def arcade_custom_restart(self):
        self.hide_door()
        self.hide_prisoner()

    def arcade_custom_first_start(self):
        # initialize player
        player = self.world.get_player()

        # initialize the wop
        self.wop = self.world.get_wop()
        self.wop.visible = True
        self.wop.place_on(self.world.get_coordinates("wop_rescue_init"))
        self.wop.talks("intro")
        self.wop.talks("warning")
        self.wop.talks("wish_luck")

        # initialize the prisoner
        self.prisoner = self.world.get_object("prisoner")
        self.prisoner.visible = False
        self.prisoner.send_update()
        self.prisoner_exit = self.world.get_object("prisoner_exit")
        self.prisoner_exit.visible = False
        self.prisoner_exit.send_update()

        # initialize the door
        self.door = self.world.get_object("door")
        self.door.visible = False
        self.door_closed = False
        self.door.send_update()

        # initialize the patrolmens
        self.patrolmen_circle = self.world.get_object("patrolmen_circle")
        self.patrolmen_short_hall = self.world.get_object("patrolmen_short_hall")

        def change_position(direction, object):
            if direction == "UP":
                object.direction = cst.Direction.UP
            elif direction == "DOWN":
                object.direction = cst.Direction.DOWN
            elif direction == "LEFT":
                object.direction = cst.Direction.LEFT
            elif direction == "RIGHT":
                object.direction = cst.Direction.RIGHT

        def break_the_cage():
            payload = {
                "topic": "sprites",
                "action": "hide",
                "layer": "decorations_top",
                "locations": [(33, 2)],
            }
            self.send_to_gui(payload)

        def post_update(player, memory):
            if player.coord_x == 33 and player.coord_y == 2:
                self.prisoner_exit.appear = True
                self.prisoner_exit.visible = True
                break_the_cage()
                self.prisoner.visible = True

            if (
                player.coord_x == self.door.coord_x
                and player.coord_y == self.door.coord_y - 1
                and not self.door_closed
            ):
                self.door_closed = True
                self.door.visible = True
                self.door.send_update()
                # prisoner.visible = True

            frames = memory["stack"]
            for frame in frames:
                try:
                    change_position(
                        "".join(
                            frame.variables["buffered_command_circle"]
                            .value[0]
                            .__dict__["command_buffer"][:-1]
                        ),
                        self.patrolmen_circle,
                    )
                    change_position(
                        "".join(
                            frame.variables["buffered_command_short_hall"]
                            .value[0]
                            .__dict__["command_buffer"][:-1]
                        ),
                        self.patrolmen_short_hall,
                    )
                    change_position(
                        "".join(
                            frame.variables["buffered_command_prisoner"]
                            .value[0]
                            .__dict__["command_buffer"][:-1]
                        ),
                        self.prisoner,
                    )
                except:
                    pass

        player.post_update = post_update

        # def pre_send_jail():
        # wop.talks("escaper_catched")
        # pass
        # self.register_breakpoint("resolve_jailbreak", pre_send_jail)

        def post_esc_catched():
            catched = self.tracker.get_variable_value_as_str("catched", "int")
            if catched:
                self.wop.talks("escaper_catched")
                payload = {
                    "topic": "sprites",
                    "action": "create",
                    "type": "skill",
                    "x": None,
                    "y": None,
                }
                if player.coord_x in [x for x in range(30, 34)] and player.coord_y in [
                    y for y in range(6, 10)
                ]:
                    payload["x"] = self.patrolmen_circle.coord_x
                    payload["y"] = self.patrolmen_circle.coord_y
                elif player.coord_x in [
                    x for x in range(17, 30)
                ] and player.coord_y in [y for y in range(6, 10)]:
                    payload["x"] = self.patrolmen_short_hall.coord_x
                    payload["y"] = self.patrolmen_short_hall.coord_y

                self.send_to_gui(payload)

        self.register_leave_function("catch_the_escaper", post_esc_catched)

    def pre_validation(self):
        self.checker.append_inputs(
            [
                "RIGHT",
                "RIGHT",
                "RIGHT",
                "RIGHT",
                "RIGHT",
                "RIGHT",
                "RIGHT",
                "RIGHT",
                "RIGHT",
                "RIGHT",
                "DOWN",
                "RIGHT",
                "RIGHT",
                "UP",
                "UP",
                "UP",
                "UP",
                "UP",
                "UP",
                "UP",
                "UP",
                "UP",
                "UP",
                "RIGHT",
                "RIGHT",
                "RIGHT",
                "RIGHT",
                "RIGHT",
                "RIGHT",
                "RIGHT",
                "RIGHT",
                "RIGHT",
                "RIGHT",
                "RIGHT",
                "RIGHT",
                "RIGHT",
                "RIGHT",
                "RIGHT",
                "RIGHT",
                "RIGHT",
                "RIGHT",
                "DOWN",
                "RIGHT",
                "RIGHT",
                "UP",
                "DOWN",
                "DOWN",
                "DOWN",
                "WAIT",
                "WAIT",
                "WAIT",
                "WAIT",
                "DOWN",
                "DOWN",
                "DOWN",
                "DOWN",
                "LEFT",
                "LEFT",
                "LEFT",
                "LEFT",
                "LEFT",
                "UP",
                "UP",
                "UP",
                "UP",
                "WAIT",
                "WAIT",
                "WAIT",
                "WAIT",
                "WAIT",
                "WAIT",
                "WAIT",
                "UP",
                "LEFT",
                "LEFT",
                "LEFT",
                "LEFT",
                "DOWN",
                "DOWN",
                "RIGHT",
                "RIGHT",
                "DOWN",
                "DOWN",
                "DOWN",
                "LEFT",
                "LEFT",
                "LEFT",
                "LEFT",
                "LEFT",
                "LEFT",
                "LEFT",
                "LEFT",
                "LEFT",
                "LEFT",
                "LEFT",
                "LEFT",
                "UP",
                "UP",
                "UP",
                "UP",
                "UP",
                "GOOD",
            ]
        )

    # def post_validation(self):
    #     pass

    def test(self):
        pass
