/* @AGDB
 * level_title: rescue
 * exec_name: rescue
 * engine_name: read_map_str
 *
 * available_commands: next step edit continue
 *
 * OBJprisoner: var_x prisoner_x
 * OBJprisoner: var_y prisoner_y
 * OBJprisoner: char_rep P
 * OBJprisoner_exit: var_x prisoner_exit_x
 * OBJprisoner_exit: var_y prisoner_exit_y
 * OBJprisoner_exit: char_rep &
 * OBJdoor: char_rep D
 * OBJdoor: var_x door_x
 * OBJdoor: var_y door_y
 * OBJexit: var_x exit_x
 * OBJexit: var_y exit_y
 * OBJpatrolmen_circle: char_rep C
 * OBJpatrolmen_circle: var_x patrolmen_circle_x
 * OBJpatrolmen_circle: var_y patrolmen_circle_y
 * OBJpatrolmen_short_hall: char_rep S
 * OBJpatrolmen_short_hall: var_x patrolmen_short_hall_x
 * OBJpatrolmen_short_hall: var_y patrolmen_short_hall_y
 *
 * # Info from deprecated branch
 * #  "prisoner": ":characters:Male/Male 04-3.png",
 * #  "patrolmen_circle": ":characters:Other/pipo-charachip_soldier01.png",
 * #  "patrolmen_short_hall": ":characters:Enemy/Enemy 02-1.png",
 *
 * arcade_maps: main rescue.tmx
 *
 * coordinates: wop_rescue_init
 *
 * verify_always: cond prisoner_exit_x == 3
 * verify_always: cond prisoner_exit_y == 6
 * verify_always: cond exit_x == 2
 * verify_always: cond exit_y == 6
 * verify_always: cond cage_x == 33
 * verify_always: cond cage_y == 2
 * verify_always: cond door_x == 13
 * verify_always: cond door_y == 9
 * verify_always: cond tp_x == 15
 * verify_always: cond tp_y == 4
 *
 * verify_init: cond patrolmen_circle_x == 30
 * verify_init: cond patrolmen_circle_y == 6
 * verify_init: cond patrolmen_short_hall_x == 17
 * verify_init: cond patrolmen_short_hall_y == 4
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "engine/action.h"
#include "engine/agdbentures.h"
#include "engine/game.h"
#include "engine/read_input.h"

/** @AGDB
 *
 * Wise person
 * WOP: message intro
 * There is a villager who has been kidnapped by goblins.
 * EndOfMessage
 * WOP: message warning
 * This is A-rank rescue mission and it's dangerous.
 *
 *
 * Once you go through that way, the door will close, the last rescuer
 * he had to sacrify because he didn't know it.
 *
 *
 * Fortunately, he was able to set up a teleport for us.
 *
 * EndOfMessage
 *
 * WOP: message wish_luck
 * If you are ready, go through that door and save him.
 *
 *
 * Be careful with patrolman and come back alive, kid.
 * EndOfMessage
 *
 * WOP: message escaper_catched
 *
 * WHERE..ARE..YOU..GOING..!
 *
 *
 * HAHAHA!
 *
 * EndOfMessage
 *
 */

#define CIRCULAR_BUFFER_SIZE 20
// The distance at which the prisoner will follow the character
#define FOLLOW_DISTANCE 4
#define LAST_WORD 30

// Initialize the door's position
int door_x = 13, door_y = 9;
bool door_closed = false;

// Initialize the player's exit
int exit_x = 2, exit_y = 6;
int tp_x = 15, tp_y = 4;

// Initialize the prisoner's exit
int prisoner_exit_x = 3, prisoner_exit_y = 6;

// Initialize the cage which have prisoner
int prisoner_x, prisoner_y;
int cage_x = 33, cage_y = 2;

// Initialize the patrolmen's position
int patrolmen_circle_x = 30, patrolmen_circle_y = 6;
int patrolmen_short_hall_x = 17, patrolmen_short_hall_y = 4;

// Circular buffer definition to store the command history
typedef struct {
    command *internal[CIRCULAR_BUFFER_SIZE];
    int read_index;  // The next command to read
} circular_buffer;

// prisoner\ and Player are initially in the same place
char str_map[] = "\
+----------------------------------+\n\
|##         #######################|\n\
|##    #    #                   #P#|\n\
|        #  # #################   #|\n\
|           # #&#             ### #|\n\
|         ### # ######## ### #### #|\n\
|         ### # # # # #    # #    #|\n\
|           # # #       ## # # ## #|\n\
|           # # ########## # # ## #|\n\
|>          # #            #      #|\n\
|    ##       #####################|\n\
+----------------------------------+\n\
";

// Pile for player and prisoner
circular_buffer command_buffer;

// Pile for patrolmen
command right_command = {"RIGHT", 5, 0, NULL};
command up_command = {"UP", 2, 0, NULL};
command left_command = {"LEFT", 4, 0, NULL};
command down_command = {"DOWN", 4, 0, NULL};
circular_buffer short_hall_buffer;
circular_buffer circle_buffer;

// Prisoner's function
void init_buffer(circular_buffer *com) { com->read_index = -1; }

void buffer_write(circular_buffer *buffer, command *to_write)
{
    buffer->read_index++;
    buffer->internal[buffer->read_index % CIRCULAR_BUFFER_SIZE] = to_write;
}

int verify_sortie_prisoner()
{
    if (prisoner_x == prisoner_exit_x && prisoner_y == prisoner_exit_y) {
        return 1;
    } else {
        printf(
            "DEFAITE the prisoner should be at x=%d y=%d but is at x=%d y=%d\n",
            prisoner_exit_x, prisoner_exit_y, prisoner_x, prisoner_y);
        return 0;
    }
}

// Patrolmen's functions
void init_patrol_buffer(circular_buffer *circle, circular_buffer *short_hall)
{
    /* Patrolmen_circle*/
    for (int i = 0; i < 12; i++) {
        if (i < 3) {
            circle->internal[i] = &right_command;
        } else if (i < 6) {
            circle->internal[i] = &down_command;
        } else if (i < 9) {
            circle->internal[i] = &left_command;
        } else {
            circle->internal[i] = &up_command;
        }
        circle->read_index = 0;

        /* Patrolmen_short_hall */
        for (int i = 0; i < 20; i++) {
            if (i < 10) {
                short_hall->internal[i] = &right_command;
            } else {
                short_hall->internal[i] = &left_command;
            }
        }
        short_hall->read_index = 0;
    }
}

void resolve_jailbreak()
{
    char last_word[LAST_WORD];
    printf("WHERE..ARE..YOU..GOING..!\nHAHAHA!\n");
    prompt("Any...last..words ??\n", last_word, LAST_WORD);
    printf("OK...GOOD..LUCK NEXT.!");
}

int catch_the_escaper(map *map, direction direction, int x, int y, int esc_x,
                      int esc_y)
{
    bool catched = false;
    switch (direction) {
    case DIR_UP:
        if (x == esc_x) {
            if ((esc_y - y == -2 && map->tiles[y - 1][x] != WALL) ||
                (esc_y - y == -1 || esc_y == y)) {
                catched = true;
            }
        }
        break;

    case DIR_DOWN:
        if (x == esc_x) {
            if ((esc_y - y == 2 && map->tiles[y + 1][x] != WALL) ||
                (esc_y - y == 1 || esc_y == y)) {
                catched = true;
            }
        }
        break;

    case DIR_LEFT:
        if (y == esc_y) {
            if ((esc_x - x == -2 && map->tiles[y][x - 1] != WALL) ||
                (esc_x - x == -1 || esc_x == x)) {
                catched = true;
            }
        }
        break;

    case DIR_RIGHT:
        if (y == esc_y) {
            if ((esc_x - x == 2 && map->tiles[y][x + 1] != WALL) ||
                (esc_x - x == 1 || esc_x == x)) {
                catched = true;
            }
        }
        break;

    default:
        printf("Unknown direction!\n");
        exit(EXIT_FAILURE);
    }
    return catched;
}

void patrolmen_movement(command **buffered_command,
                        direction *patrolmen_direction, circular_buffer *buffer,
                        int *index, int *x, int *y)
{
    int patrolmen_next_x, patrolmen_next_y;
    *buffered_command = buffer->internal[*index];
    (*index)++;
    (*patrolmen_direction) =
        direction_from_string((*buffered_command)->command_buffer);
    look_forward(*y, *x, (*patrolmen_direction), &patrolmen_next_y,
                 &patrolmen_next_x);
    *x = patrolmen_next_x;
    *y = patrolmen_next_y;
}

// Map's function
void close_door(map *map)
{
    map->tiles[door_y][door_x] = DOOR;
    door_closed = true;
    message("The door is closed now! \nYou can't get out anymore!");
}

void get_out_of_castle(map *map)
{
    char last_word[LAST_WORD];
    prisoner_x = 3;
    prisoner_y = 6;
    map->player_x = 2;
    map->player_y = 6;
    prompt("OMG! You're back!\nGood Experience?", last_word, LAST_WORD);
}

int main()
{
    game_instance *game = init_game(36, 12);
    game->map = load_map(str_map);

    // init coordinate and index
    int index_player = 0;
    int index_prisoner = 0;
    int index_circle = 0;
    int index_short_hall = 0;

    // Distance between and human && Follow's condition
    bool rescued = false;
    bool prisoner_follow = false;
    int distance;

    command *input_command;
    command *buffered_command_prisoner, *buffered_command_short_hall,
        *buffered_command_circle;
    init_buffer(&command_buffer);
    init_patrol_buffer(&circle_buffer, &short_hall_buffer);

    while (!game->exit_main_loop) {
        input_command = get_next_command();

        while (!isDirection(input_command->command_buffer)) {
            printf("Unknown Command! Use these command: RIGHT, LEFT, UP, DOWN, "
                   "WAIT\n");
            input_command = get_next_command();
        }

        // Patrolmen's movement
        direction circle_direction, short_hall_direction;
        // Circle's movement
        patrolmen_movement(&buffered_command_circle, &circle_direction,
                           &circle_buffer, &index_circle, &patrolmen_circle_x,
                           &patrolmen_circle_y);
        if (index_circle >= 12) {
            index_circle = 0;
        }

        // Short_hall's movement
        patrolmen_movement(&buffered_command_short_hall, &short_hall_direction,
                           &short_hall_buffer, &index_short_hall,
                           &patrolmen_short_hall_x, &patrolmen_short_hall_y);
        if (index_short_hall >= 20) {
            index_short_hall = 0;
        }

        // Before saving the prisoner, the command_buffer isn't activated
        if (!rescued) {
            apply_input(game->map, input_command);

            if (game->map->player_x == cage_x &&
                game->map->player_y == cage_y) {
                rescued = true;
                // Initialize prisoner's position
                prisoner_x = 33;
                prisoner_y = 2;
            }
            if (game->map->player_x == 13 && game->map->player_y == 8 &&
                !door_closed) {
                close_door(game->map);
                printf("\nTHE DOOR IS CLOSED\n");
            }
        } else {
            buffer_write(&command_buffer, input_command);
            if (!prisoner_follow) {
                apply_input(game->map, input_command);
                index_player++;
                distance++;

                if (distance == FOLLOW_DISTANCE) {
                    prisoner_follow = true;
                    // The prisoner start in the next order
                    index_prisoner++;
                }
            } else {
                if (!(index_player <= CIRCULAR_BUFFER_SIZE - 1)) {
                    index_player = index_player % CIRCULAR_BUFFER_SIZE + 1;
                } else {
                    index_player++;
                }
                apply_input(game->map,
                            command_buffer.internal[index_player - 1]);

                // The prisoner should consume the command before increasing the
                // index
                buffered_command_prisoner =
                    command_buffer.internal[index_prisoner - 1];

                int next_y, next_x;
                direction prisoner_direction = direction_from_string(
                    buffered_command_prisoner->command_buffer);
                look_forward(prisoner_y, prisoner_x, prisoner_direction,
                             &next_y, &next_x);

                // The prisoner moves if the destination is empty or player
                // place, otherwise it does nothing but consume the command
#ifdef BUG
                if (game->map->tiles[next_y][next_x] == ' ') {
#else
                if (game->map->tiles[next_y][next_x] == ' ' ||
                    game->map->tiles[next_y][next_x] == '>') {
#endif
                    prisoner_y = next_y;
                    prisoner_x = next_x;
                }

                if (!(index_prisoner <= CIRCULAR_BUFFER_SIZE - 1)) {
#ifdef BUG
                    index_prisoner =
                        index_prisoner % (CIRCULAR_BUFFER_SIZE - 1) + 1;
                }
#else
                    index_prisoner = index_prisoner % CIRCULAR_BUFFER_SIZE + 1;
                } else {
                    index_prisoner++;
                }
#endif
            }
        }
        // Catch the escaper by patrolmen
        if (catch_the_escaper(game->map, circle_direction, patrolmen_circle_x,
                              patrolmen_circle_y, prisoner_x, prisoner_y) ||
            catch_the_escaper(game->map, short_hall_direction,
                              patrolmen_short_hall_x, patrolmen_short_hall_y,
                              prisoner_x, prisoner_y) ||
            catch_the_escaper(game->map, circle_direction, patrolmen_circle_x,
                              patrolmen_circle_y, game->map->player_x,
                              game->map->player_y) ||
            catch_the_escaper(game->map, short_hall_direction,
                              patrolmen_short_hall_x, patrolmen_short_hall_y,
                              game->map->player_x, game->map->player_y)) {
            resolve_jailbreak();
            game->exit_main_loop = true;
        }

        // Exit Loop's condition
        if (game->map->player_x == tp_x && game->map->player_y == tp_y) {
            get_out_of_castle(game->map);
            game->exit_main_loop = true;
        }
    }
    if (verify_sortie_prisoner()) {
        /* Verify the prisoner's finish place*/
        verify_exit(game->map);
    } else {
        exit(EXIT_FAILURE);
    }

    return 0;
}
