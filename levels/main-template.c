/* @AGDB
 * level_title: MANDATORY
 * engine_name: MANDATORY
 * exec_name: MANDATORY
 * engine_tags:
 * engine_antitags:
 */

#include <stdlib.h>

#include "agdbentures.h"

int main() {
  init_map_stack();

  command * command;
  while (!exit_main_loop) {
    command = get_next_command();

    apply_input(command);

    if (!exit_main_loop) {
        apply_events();
    }

    free_command(command);
}

  return 0;
}