/* @AGDB
 * level_title: Holy Grail
 * exec_name: holy_grail
 * engine_name: input_command
 * engine_tags:
 * engine_antitags:
 *
 * available_commands: next step edit continue
 * interactive: True
 *
 * arcade_maps: main holy_grail.tmx
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <unistd.h>
#include <ctype.h>

#include "engine/agdbentures.h"
#include "custom_map.h"

#define MAX_PASSWORD_LENGTH 30

int exit_x = 12;
int exit_y = 4;

#define MAP_WIDTH 14
#define MAP_HEIGHT 9

// return true if the player is on the exit
bool player_on_exit(map *map)
{
    return map->player_x == exit_x && map->player_y == exit_y;
}

// return true if the word 'it' is in the string given in parameter.
// It should not be inside a word so we check that letters around are
// not alphanumeric values.
bool check_word(char *input)
{
    int i = 0;
    while (input[i] != '\0')
    {
        if (!isalnum(input[i]) &&
            input[i + 1] == 'i' &&
            input[i + 2] == 't' &&
#ifdef BUG
            !isalnum(input[i + 2]))
        {
#else
            !isalnum(input[i + 3]))
        {
#endif
            return true;
        }
#ifndef BUG
        i++;
#endif
    }
    return false;
}

// FR Les chevalier crient 'Ni!' ce qui fait reculer le joueur
// EN The knights shout 'Ni!' at the player, which make it go backwards
void angry_knights(map *map)
{
    message("The knights say : \"Ni!\". Their scream is so loud it makes you go backward.");
    knock_back(map);
    show_map(map);
    message("\"Ni!\"");
    knock_back(map);
    show_map(map);
    message("\"Ni!\"");
    knock_back(map);
    show_map(map);
    message("\"Ni!\"");
    knock_back(map);
    show_map(map);
}

int main(void)
{

    char input_player[MAX_PASSWORD_LENGTH];
    bool scared_knights = false;
    // creation de la map
    map *current_map = create_map(MAP_HEIGHT, MAP_WIDTH);
    command *command;

    // main loop
    while (!player_on_exit(current_map))
    {
        show_map(current_map);
        printf("\n");

        printf("Enter move command (RIGHT, LEFT, UP, DOWN)\n");
        command = get_next_command();
#ifdef _DEBUG
        printf("Your command is %s\n", command->command_buffer);
        /* printf("Your command is %s\n", command); */ /*TODO: understand why
                                                         this makes agdbentures hang...
                                                         (pb in easytracker ?)*/
#endif
        apply_input(current_map, command);

        if (scared_knights)
        {
            // FR Les chevaliers ne cherchent plus à gêner le joueur
            // EN The knights leave the player alone
            continue;
        }

        int dist = knight_distance(current_map);
#ifdef _DEBUG
        printf("knight distance is %d\n", dist);
#endif

        switch (knight_distance(current_map))
        {
        case 2:
            message("Six knights are guarding the exit.");
            message("The knights say: \"'We are the knights who say 'Ni!' and we want a shruberry!'\"");
            break;
        case 1:
            if (current_map->player_y == 4)
            {
                prompt("What do you say to the knights?", input_player, MAX_PASSWORD_LENGTH);
#ifdef _DEBUG
                printf("You said: '%s'\n", input_player);
#endif
                if (check_word(input_player))
                {
                    scared_knights = true;
                    scatter_knights(current_map);
                }
                else
                {
                    angry_knights(current_map);
                }
            }

            break;
        default:
            // FR Ne rien faire
            // EN Do nothing
            break;
        }
    }
    show_map(current_map);

    verify_exit(current_map);

    return EXIT_FAILURE;
}
