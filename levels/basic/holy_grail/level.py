#!/usr/bin/env python3

from level.level_abc import AbstractLevel
from level.action import Action
from logs import cus
import graphic.constants as cst


class Level(AbstractLevel):

    def arcade_custom_first_start(self):

        knights = [self.world.get_object(f"knight_{i}") for i in range(1,7)]
        self.last_knight_update = False
        self.scattered_knights = False
        self.restart = False 
        for i in range(6):
            knights[i].visible = True
            knights[i].direction = cst.Direction.LEFT
            knights[i].send_update()

        cus.debug(f"knights objects: {knights}")

        def get_knight_position(i):
            j = 0
            map = self.world.maps[self.world.current_map]
            for x in range(map.height):
                for y in range(map.width):
                    if map.tiles[x][y] == 'V':
                        if (j == i):
                            return y,x
                        j+=1
            return -1, -1

        def set_knight_position(i):
            def set_knight_position_internal(a, b):
                knight = knights[i]
                old_x, old_y = knight.coord_x, knight.coord_y
                (new_x, new_y) = get_knight_position(i)
                if (new_x == -1 or new_y == -1):
                    return
                knight.coord_x = new_x
                knight.coord_y = new_y

                
                if knight.coord_x != old_x or knight.coord_y != old_y or self.last_knight_update:
                    cus.debug(f"New position for {knight} : {knight.coord_x, knight.coord_y}")
                    knight._far_move = 'pathfind'
                    # handle the knights movements after a restart 
                    if self.restart:
                        knight.direction = cst.Direction.LEFT
                        knight.send_update()
                    else:
                        self.scattered_knights = True
                        pl = knight.payload()
                        pl['action'] = Action('hit')
                        knight.send_update(pl)  # send "hit" payload 
                        knight.send_update()    # send "move" payload
                        if i < 3:
                            knight.direction = cst.Direction.DOWN
                        else:
                            knight.direction = cst.Direction.UP
                        knight.send_update()
                        # flag update so the knights don't keep getting hit after they have moved away
                    if self.last_knight_update:
                        self.last_knight_update = False
                        self.restart = False
                    if i == 4:
                        # we're updating the 5th knight here so we need the 6th next
                        # (and the 6th doesn't move in the process so it's the only way to update it !)
                        self.last_knight_update = True 
                

            return set_knight_position_internal

        for i in range(6):
            # set_knight_position(i) is a closure. Overwrite default method in object.py
            knights[i].set_position = set_knight_position(i)
            pl = knights[i].payload()
            pl['change/speed'] = 0.3
            knights[i].send_update(pl)

        player = self.world.get_player()

        def player_knock_back():
            self.update_all()
            # we change position ourselves, even if inferior will later change that
            # so that direction will not be updated toward direction of
            # movement
            player.coord_x -= 1

            pl = player.payload()
            pl['action'] = Action('hit')
            pl['change/speed'] = 2
            player.send_update(pl)
        
        def player_knock_back_end():
            pl = player.payload()
            pl['change/speed'] = 1
            player.send_update(pl)

        self.register_breakpoint("angry_knights", player_knock_back)
        self.register_leave_function("angry_knights", player_knock_back_end)
        
    def arcade_custom_restart(self):
        tmp = self.scattered_knights
        self.arcade_custom_first_start()
        self.restart = tmp

    def pre_validation(self):
        self.checker.append_inputs(
            [
                'RIGHT',
                'RIGHT',
                'RIGHT',
                'RIGHT',
                'RIGHT',
                'RIGHT',
                'RIGHT',
                'RIGHT',
                'RIGHT',
                "Please let me pass",  # will be knocked back 4 times
                'RIGHT',
                'RIGHT',
                'RIGHT',
                'RIGHT',
                "Leave it be!",
                'RIGHT',
                'RIGHT',
            ]
        )

    def test_input(self,T):
        for _ in range(9):
            T.send_input("RIGHT")
        T.send_input("Please let me pass")
        for _ in range(4):
            T.send_input("RIGHT")
        T.send_input("Leave it be!")
        for _ in range(2):
            T.send_input("RIGHT")

    def test_wrong_input(self,T):
        for _ in range(9):
            T.send_input("RIGHT")
        for _ in range(2):
            T.send_input("Leave its be!")
            for _ in range(4):
                T.send_input("RIGHT")


    def test(self):
        import tests.lib_test as T

        self.recompile_answer()
        self.test_input(T)
        T.ccontinue()
        T.unexpect_infinite_loop()
        T.expect_victory()
        self.run()
        self.check_validation(should_validate=True)

        self.recompile_answer()
        self.test_wrong_input(T)
        T.ccontinue(timeout=0.1)
        T.expect_infinite_loop()
        self.run()

        self.recompile_bug()
        self.test_input(T)
        T.ccontinue(timeout=0.1)
        T.expect_infinite_loop()
        self.run()
