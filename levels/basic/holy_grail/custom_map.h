#ifndef CUSTOM_MAP_H
#define CUSTOM_MAP_H

#include "engine/agdbentures.h"

map *create_map(int height, int width);

int knight_distance(map* map);
void scatter_knights(map * m);
void knock_back(map * m);

#endif // CUSTOM_MAP_H
