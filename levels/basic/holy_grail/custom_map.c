#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include "custom_map.h"
#include "engine/agdbentures.h"

#define PLAYER_START_X 1
#define PLAYER_START_Y 4

#define KNIGHT 'V'
#define KNIGHT_X 11

// place wall on the map's border.
void place_walls(map* map)
{
    for (int y = 0; y < map->height; y++) {
        for (int x = 0; x < map->width; x++) {
            if (x == 0 || x == map->width-1 || y == 0 || y == map->height-1)
                place_entity(map, WALL, y, x);
        }
    }
}

map* create_map(int height, int width) {
    map* custom_map = init_map(height,width);

    // placement of the walls
    place_walls(custom_map);

    // placement of the knights
    for (int i = 1; i < 8; i++){
        if (i==4)
            continue;
        place_entity(custom_map, KNIGHT, i, KNIGHT_X);   
    }

    // placement of the player
    place_player(custom_map, PLAYER_START_Y, PLAYER_START_X);
    // placement of the exit
    place_entity(custom_map, EXIT, 4, 12);

    return custom_map;
}

// returns the distance to the column of knights
int knight_distance(map* map)
{
    return KNIGHT_X - map->player_x;
}

// scatter the knights away from the exit
void scatter_knights(map * m)
{
    message("\"Owwwwww !\"\nThe knights howl in pain and retreat, you can now reach the exit.");
    show_map(m);
    left(m,6,11);
    left(m,2,11);
    show_map(m);
    up(m,2,10);
    down(m,6,10);
    show_map(m);
    left(m,3,11);
    left(m,5,11);
    show_map(m);
    down(m,5,10);
    up(m,3,10);
    show_map(m);
    left(m,6,10);
    left(m,2,10);
    show_map(m);
    down(m,6,9);
    up(m,2,9);
}

void knock_back(map * map)
{
    //FR Ne pas utiliser player_left ou la direction sera changée vers LEFT
    //EN Cannot use player_left or it would get LEFT direction
    move_player(map, map->player_y, map->player_x - 1, DIR_RIGHT);
}