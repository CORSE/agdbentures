/* @AGDB
 * level_title: Keys
 * exec_name: keys
 * engine_name: none
 *
 *
 * BUG: comparison of key is actually an assignment
 * Tag: #lang
 *
 * available_commands: next step edit continue
 *
 * arcade_maps: main keys.tmx
 *
 * GUI: only 'next/step/edit' buttons + click-break
 *
 * coordinates: wop_init
 * 
 * OBJkey1: char_rep K
 * OBJkey2: char_rep P
 * 
 * OBJupdoor_front_open: char_rep O
 * OBJupdoor_back_open: char_rep O
 * 
 * OBJupdoor_front_closed: char_rep C
 * OBJlowdoor_front_closed: char_rep C
 * OBJupdoor_back_closed: char_rep C
 * OBJlowdoor_back_closed: char_rep C
 *
 * no_verify: change_in_function player_x forward
 * no_verify: change_in_function player_y forward
 * verify_always: change_in_function key_count grab_key
 * 
 * WOP: messageEN intro
 * 
 * Alright, no more interaction !
 * Back to the good old movement programs !
 * You will have to collect two keys in order to be able to open the two doors straight ahead.
 * Good luck !
 * 
 * EndOfMessage
 * 
 * WOP: messageFR intro
 * 
 * Très bien, ici pas d'interaction, on retourne aux bons vieux programmes de déplacement !
 * Tu dois ramasser deux clés pour pouvoir ouvrir les deux portes au bout du chemin.
 * Bonne chance !
 * 
 * EndOfMessage
 * 
 * A vertical wall bars the passage to the exit. There are two locked door 
 * on the wall. Two keys are generated at a random position. The goal is to 
 * search for the keys and then open the doors to get to the exit.
 */

#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <assert.h>
#include <time.h>

enum direction {
    UP,
    DOWN,
    LEFT,
    RIGHT
};

/* Variables for the player */
int player_x=3, player_y=5;         /* coordinates */
int player_direction = RIGHT;   /* direction of movement */
int key_count = 0;      /* the number of keys found by the player */
int key_used  = 0;      /* the number of keys used  by the player */

/* Coordinates for the exit */
int exit_x=27;
int exit_y= 5;

/* x Position of a full vertical wall, of width 2, with 2 doors at the y position */
int wall_x=22;
int wall_width=2;
int door_y= 5;

/* Coordinates for the keys */
#define NUM_KEYS 2
int key_x[NUM_KEYS] = {-1, -1};
int key_y[NUM_KEYS] = {-1, -1};
// Coordinates of the keys once set on the map (for verifying purposes, do not modify)
int initial_key_x[NUM_KEYS];
int initial_key_y[NUM_KEYS];

void turn_left (void)
{
    if (player_direction == UP) {
        player_direction = LEFT;
    } else if (player_direction == DOWN) {
        player_direction = RIGHT;
    } else if (player_direction == LEFT) {
        player_direction = DOWN;
    } else if (player_direction == RIGHT) {
        player_direction = UP;
    } else {
        fprintf(stderr, "Error: unknown direction!\n");
    }
}

void turn_right (void)
{
    if (player_direction == UP) {
        player_direction = RIGHT;
    } else if (player_direction == DOWN) {
        player_direction = LEFT;
    } else if (player_direction == LEFT) {
        player_direction = UP;
    } else if (player_direction == RIGHT) {
        player_direction = DOWN;
    } else {
        fprintf(stderr, "Error: unknown direction!\n");
    }
}

void turn_around(void)
{
    if (player_direction == UP) {
        player_direction = DOWN;
    } else if (player_direction == DOWN) {
        player_direction = UP;
    } else if (player_direction == LEFT) {
        player_direction = RIGHT;
    } else if (player_direction == RIGHT) {
        player_direction = LEFT;
    } else {
        fprintf(stderr, "Error: unknown direction!\n");
    }
}

void try_open_door(void)
{
#ifdef BUG
    if (key_count = key_used) {
#else
    if (key_count == key_used) {
#endif
        printf ("You do not have enough keys!\n");
    } else {
        assert (key_count > key_used) ; /* it should never be possible to have used more keys than we have */
        printf ("You open the door with one of your keys.\n");
        player_x++;
        key_used++;
    }
    }

// Try and grab the key at the player position.
void grab_key(int key) {
    // the key disappears if its coordinates are (-1,-1)
    if (key_x[key] == -1 && key_y[key] == -1) {
        key_count++; /* increase the number of keys found */
        if (key_count == 1) {
            printf("You found the first key !\n");
        } else if (key_count == 2) {
            printf("You found the second key !\n");
        }
    }
}

// Check if there is a key at the player position, and pick it up if any.
void check_key(void)
{
    for(int key=0; key<NUM_KEYS; key++) {
        if (player_x == key_x[key] && player_y == key_y[key]) {
            initial_key_x[key] = key_x[key];
            initial_key_y[key] = key_y[key];
#ifdef BUG
            key_x[key]-=1;
            key_y[key]-=1;
#else
            key_x[key]=-1; 
            key_y[key]=-1;
#endif
            grab_key(key);
        }
    }
}

// Returns if the player is near the wall
bool near_wall(void)
{
    if (player_x+1 >= wall_x && player_x+1 <= wall_x+wall_width-1) {
        return true;
    } else {
        return false;
    }
}

void forward(void)
{
    if (player_direction == UP) {
        player_y--;
    } else if (player_direction == DOWN) {
        player_y++;
    } else if (player_direction == LEFT) {
        player_x--;
    } else if (player_direction == RIGHT) {
        if (near_wall()) {
            if (player_y != door_y) {
                printf ("You bump into a wall.\n");
            } else {
                try_open_door();
            }
        } else {
            player_x++;
        }
    } else {
        fprintf(stderr, "Error: unknown direction!\n");
    }

    check_key();
}

void forward_n(int steps)
{
    for (int i=0; i<steps; i++) {
        forward();
    }
}

/**
 * Choose coordinates of keys at random.
 */
void keys_random_position(void)
{
    srand(time(NULL)); /* seeds the random generator */;

    for(int key=0; key<NUM_KEYS; key++) {
        key_x[key] = 7*(key+1)+rand() % 5;   /* sets x position between 7 and 11 for key1 and 14 and 18 for key2 */
        key_y[key] = 1 + rand() % 6;
        while (key_y[key] == player_y) {
            key_y[key] = 1 + rand() % 7; /* sets y position between 0 and 6 but not on row 5 */
        }
        printf("Key n°%d position : %dx%d\n", key, key_x[key], key_y[key]);
    }
}

void search_key(void)
{
    int init_key = key_count;

    turn_right();
    forward();
    turn_left();

    /* do the search in the area until we find the key */
    while (init_key == key_count) {
        /* search first line */
        forward_n(5);
        /* then go back */
        turn_around();
        forward_n(5);
        turn_around();
        /* move up one line and search */
        turn_left();
        forward();
        turn_right();
    }
    /* do not forget to go back to middle line */
    if (player_y > exit_y) {
        turn_left();
        forward_n(player_y - exit_y);
        turn_right();
    }
    if (player_y < exit_y) {
        turn_right();
        forward_n(exit_y - player_y);
        turn_left();
    }
}

void verify_exit(void)
{
    if (player_x != exit_x) {
        printf("DEFAITE!\n");
        exit (EXIT_FAILURE);
    } else {
        printf("VICTOIRE!\n");
        exit (EXIT_SUCCESS);
    }
}

int main(void)
{
    keys_random_position(); /* choose a random position for the keys */

    forward_n(4);
    search_key();

    forward_n(7);
    search_key();

    forward_n (wall_x - player_x - 1);
    // try and cross the first door
    forward();
    // try and cross the second door
    forward();
    forward_n(exit_x - player_x);

    verify_exit();
}
