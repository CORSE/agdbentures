#!/usr/bin/env python3

from level.level_abc import AbstractLevel
from logs import cus
from level.action import Action


class Level(AbstractLevel):

    def arcade_custom_first_start(self):
        # TODO WOP or panel

        player = self.world.get_player()
        player.has_talked = False
        wop = self.world.get_wop()
        wop_init = self.world.get_coordinates("wop_init")
        wop.visible = True
        wop.place_on(wop_init)
        wop.send_update()
        memory = self.tracker.get_program_memory(as_raw_python_objects=True)
        keys = [self.world.get_object(f"key{i}") for i in (1, 2)]
        # cus.debug("key objects", keys)
        self.first_call_hide = True
        # we only use the open UPDOORS as objects because we need to manipulate open LOWDOORS as top decorations
        # so the player walks between open lowdoors and updoors (and not above open lowdoors)
        front_door_open = self.world.get_object("updoor_front_open")
        back_door_open = self.world.get_object("updoor_back_open")
        front_doors_closed = [
            self.world.get_object(f"{position}_front_closed")
            for position in ("updoor", "lowdoor")
        ]
        back_doors_closed = [
            self.world.get_object(f"{position}_back_closed")
            for position in ("updoor", "lowdoor")
        ]

        def hide_keys():
            for i in (0, 1):
                keys[i].visible = False
                pl = keys[i].payload()
                keys[i].send_update(pl)

        def hide_front_doors_open():
            front_door_open.visible = False
            pl = front_door_open.payload()
            front_door_open.send_update(pl)
            pl = {
                "topic": "sprites",
                "layer": "decorations_top",
                "action": Action("hide"),
                "locations": [(22, 5)],
            }
            self.send_to_gui(pl)

        def hide_back_doors_open():
            back_door_open.visible = False
            pl = back_door_open.payload()
            back_door_open.send_update(pl)
            pl = {
                "topic": "sprites",
                "layer": "decorations_top",
                "action": Action("hide"),
                "locations": [(23, 5)],
            }
            self.send_to_gui(pl)

        def hide_front_doors_closed():
            for i in (0, 1):
                front_doors_closed[i].visible = False
                pl = front_doors_closed[i].payload()
                front_doors_closed[i].send_update(pl)

        def hide_back_doors_closed():
            for i in (0, 1):
                back_doors_closed[i].visible = False
                pl = back_doors_closed[i].payload()
                back_doors_closed[i].send_update(pl)

        def show_front_doors_open():
            front_door_open.visible = True
            pl = front_door_open.payload()
            front_door_open.send_update(pl)
            pl = {
                "topic": "sprites",
                "layer": "decorations_top",
                "action": Action("show"),
                "locations": [(22, 5)],
            }
            self.send_to_gui(pl)

        def show_back_doors_open():
            back_door_open.visible = True
            pl = back_door_open.payload()
            back_door_open.send_update(pl)
            pl = {
                "topic": "sprites",
                "layer": "decorations_top",
                "action": Action("show"),
                "locations": [(23, 5)],
            }
            self.send_to_gui(pl)

        def door_display_update(player, memory):

            global_variables = memory["global_variables"]
            key_used = global_variables["key_used"].value
            cus.debug(f"Key used : {key_used}")

            if key_used == 0:
                pass
                # hide_front_doors_open()
                # hide_back_doors_open()

            elif key_used == 1:
                hide_front_doors_closed()
                show_front_doors_open()
                payload = {
                    "topic": "player_update",
                    "action": Action("talk"),
                    "message": "You open the first door with one of your keys.",
                }
                player.send_update(payload)

            elif key_used == 2:
                hide_back_doors_closed()
                show_back_doors_open()
                if not player.has_talked:
                    payload = {
                        "topic": "player_update",
                        "action": Action("talk"),
                        "message": "You open the second door with one of your keys.",
                    }
                    player.send_update(payload)
                    player.has_talked = True

        player.post_update = door_display_update

        # def set_key_position(i):
            # def set_key_position_internal(memory):
                # global_variables = memory["global_variables"]
#
                # keys[i].coord_x = global_variables["key_x"].value[i]
                # keys[i].coord_y = global_variables["key_y"].value[i]
                # cus.debug(f"Key {i+1} 
                            # : ({keys[i].coord_x},{keys[i].coord_y})")
#
                # if keys[i].coord_x == -1 or keys[i].coord_y == -1:
                    # keys[i].visible = False
#
                # else:
                    # keys[i].visible = True
#
                # pl = keys[i].payload()
                # keys[i].send_update(pl)
#
            # return set_key_position_internal

        if self.first_call_hide:
            hide_keys()
            hide_front_doors_open()
            hide_back_doors_open()
            self.first_call_hide = False

        wop.talk('intro')
        for k in range(2):
            keys[k].var_x = ("key_x", k)  # array access index k
            keys[k].var_y = ("key_y", k)  # array access index k

    def arcade_custom_restart(self):
        front_door_open = self.world.get_object("updoor_front_open")
        back_door_open = self.world.get_object("updoor_back_open")
        front_doors_closed = [
            self.world.get_object(f"{position}_front_closed")
            for position in ("updoor", "lowdoor")
        ]
        back_doors_closed = [
            self.world.get_object(f"{position}_back_closed")
            for position in ("updoor", "lowdoor")
        ]

        def show_doors_closed():
            for i in (0, 1):
                back_doors_closed[i].visible = True
                pl = back_doors_closed[i].payload()
                back_doors_closed[i].send_update(pl)
                front_doors_closed[i].visible = True
                pl = front_doors_closed[i].payload()
                front_doors_closed[i].send_update(pl)

        def hide_front_doors_open():
            front_door_open.visible = False
            pl = front_door_open.payload()
            front_door_open.send_update(pl)
            pl = {
                "topic": "sprites",
                "layer": "decorations_top",
                "action": Action("hide"),
                "locations": [(22, 5)],
            }
            self.send_to_gui(pl)

        def hide_back_doors_open():
            back_door_open.visible = False
            pl = back_door_open.payload()
            back_door_open.send_update(pl)
            pl = {
                "topic": "sprites",
                "layer": "decorations_top",
                "action": Action("hide"),
                "locations": [(23, 5)],
            }
            self.send_to_gui(pl)

        hide_front_doors_open()
        hide_back_doors_open()
        show_doors_closed()

    def check_on_key(self):
        chk = self.checker.tracker
        key_count = chk.get_variable_value_as_str('key_count', "int")
        plx = chk.get_variable_value_as_str('player_x', "int")
        ply = chk.get_variable_value_as_str('player_y', "int")
        if key_count == 1:
            key_x = chk.get_variable_value_as_str(f'initial_key_x[0]', "int")
            key_y = chk.get_variable_value_as_str(f'initial_key_y[0]', "int")
            cus.debug(
                f"Checking if player really is on first key : {plx, ply} vs {key_x, key_y}"
            )
            if plx != key_x or ply != key_y:
                self.checker.failed(
                    f"Player is not on first key when picking it up, key is in {key_x},{key_y} and player is in {plx},{ply}"
                )

        elif key_count == 2:
            key_x = chk.get_variable_value_as_str(f'initial_key_x[1]', "int")
            key_y = chk.get_variable_value_as_str(f'initial_key_y[1]', "int")
            cus.debug(
                f"Checking if player really is on second key : {plx, ply} vs {key_x, key_y}"
            )
            if plx != key_x or ply != key_y:
                self.checker.failed(
                    f"Player is not on second key when picking it up, key is in {key_x},{key_y} and player is in {plx},{ply}"
                )

        else:
            self.checker.failed("Player is trying to take a key that doesn't exist")

    def check_open_doors(self):
        chk = self.checker.tracker
        plx = chk.get_variable_value_as_str('player_x', "int")
        ply = chk.get_variable_value_as_str('player_y', "int")
        doorx = chk.get_variable_value_as_str('wall_x', "int")
        doory = chk.get_variable_value_as_str('door_y', "int")
        cus.debug(
            f"Checking if player really is on first door {plx, ply} vs {doorx, doory}"
        )
        if plx != doorx - 1 and plx != doorx or ply != doory:
            self.checker.failed("Player is not on door when opening it")

        key_count = chk.get_variable_value_as_str('key_count', "int")
        if key_count != 2:
            self.checker.failed("Player opens the doors but doesn't have the 2 keys")
        else:
            self.checker.has_opened_doors = True

    def pre_validation(self):
        # We need to check that key_count is incremented only
        # if player is on key, and that player passes through the door
        self.checker.has_opened_doors = False
        self.checker.register_watch('key_count', self.check_on_key)
        self.checker.register_breakpoint('try_open_door', self.check_open_doors)

    def post_validation(self):
        if not self.checker.has_opened_doors:
            self.checker.failed("Player has not opened the doors")

    def test(self):
        import tests.lib_test as T

        self.recompile_bug()
        T.ccontinue(timeout=0.1)
        T.expect_infinite_loop()
        self.run()

        self.recompile_answer()
        T.ccontinue(timeout=0.1)
        T.unexpect_infinite_loop()
        T.expect_victory()
        self.run()
