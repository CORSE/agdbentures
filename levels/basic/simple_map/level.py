#!/usr/bin/env python3

from level.level_abc import AbstractLevel
from level.action import Action
from logs import lvl


class Level(AbstractLevel):

    # TODO: make it generic: always hide all walls
    def arcade_custom_restart(self):
        pl = {
            "topic": "sprites",
            "action": Action("hide_layer"),
            "layer": "walls",
        }
        self.send_to_gui(pl)


    def arcade_custom_first_start(self):
        wop = self.world.get_outside_object("wop")
        wop.visible = True
        wop.place_at(2,3)
        wop.send_update()

        wop.talks("intro")

        def check_wop_talk():
            x = self.tracker.get_variable_value_as_str("x", "int")
            y = self.tracker.get_variable_value_as_str("y", "int")
            if x == 4 and y == 3: # first corner of the wall
                wop.talks('cheeky')

        self.register_breakpoint("place_entity", check_wop_talk)


    def test(self):
        import tests.lib_test as T

        self.recompile_answer()
        T.ccontinue()
        T.expect_victory()
        self.run()
        self.check_validation(should_validate=True)

        self.recompile_bug()
        T.ccontinue()
        T.expect_defeat()
        self.run()
