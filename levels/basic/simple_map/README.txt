Le dossier engine/ contient la premiere version du moteur.

Le fichier map.h contient la description de la structure de la carte en deux dimensions.
On se souvient de la position du joueur et on place des entites sous forme de caracteres sur la carte.

Dans le fichier action.h on peut voir les fonctions de deplacement.
Certaines verifications sur le deplacement du joueur sont deja ecrites dans le moteur.
Si on n'en veut pas il faut ecrire soi meme les fonctions equivalentes avec les verifications qu'on veut.
Il est toujours possible de modifier manuellement l'etat de la carte sans passer par les fonctions de action.h

Les fonctions avec un nom a base de look_forward dans action.h permettent de regarder autour d'une case donnee pour voir si il y a une entite.

Le mieux reste de lire les fichiers et les commentaires associes.