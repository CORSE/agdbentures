/* @AGDB
 *
 * Simple map to test agdbenture features
 *
 * level_title: Simple Map
 * exec_name: simple_map
 * engine_name: simple_map
 * engine_tags:
 * engine_antitags:
 *
 * available_commands: next step continue edit
 *
 * arcade_maps: main simple_map.tmx
 *
 * verify_always: cond exit_x == 8
 * verify_always: cond exit_y == 4
 *# verify_always: cond current_map->player_y == 4
 *
 *
 * WOP: message intro
 * Tu sais ce qui manque à ce jeu ?
 * Un MOTEUR DE JEU ! Et surtout une vraie CARTE !
 *
 *
 * Parce que tout ce monde autour de toi, jusqu'à présent c'était de la déco...
 * Mais ça y est, j'ai tout restructuré, maintenant il y a une vraie gestion de
 * carte et de déplacement du joueur...
 *
 *
 *
 * À ce propos, excuse-moi, j'en oublie mes bonnes manières...
 *
 * C'est impoli de parler à quelqu'un alors qu'il n'est pas là.
 * J'attends ton arrivée.
 * EndOfMessage
 *
 *
 *
 * WOP: message welcome
 * Ah ! Te voici !
 *
 *
 * Pour une fois, voici un niveau reposant, je n'y ai pas mis de bug, il suffit
 * juste d'observer les nouvelles fonctionnalités du code.
 *
 *
 *
 * Tu peux également aller lire le fichier README.txt qui contient des
 * explications plus détaillés.
 *
 *
 * Le fichier se trouve dans le même dossier que le 'main.c' de ce niveau.
 * EndOfMessage
 *
 * WOP: message cheeky
 * Bon, j'avoue, je n'ai pas pu résister à cette petite plaisanterie...
 *
 *
 * Ho ho ho ! Je suis un vrai boute-en-train !
 *
 *
 * Pour valider ce niveau, tu n'as pas le droit de supprimer la double boucle
 * qui place les murs autour de toi. Il te faut trouver dans le moteur la ou
 * les fonctions qui pourront t'aider.
 * EndOfMessage
 */

#include <stdlib.h>
#include "engine/agdbentures.h"

int exit_x = 8;
int exit_y = 4;

int main(void)
{
    map *current_map = init_map(9, 12);
    place_player(current_map, exit_y, 5);
    place_entity(current_map, EXIT, exit_y, exit_x);
    for (int y = exit_y - 1; y <= exit_y + 1; y++) {
        for (int x = 4; x <= 6; x++) {
            if (!(x == 5 && y == exit_y)) {
                place_entity(current_map, WALL, y, x);
            }
        }
    }
#ifndef BUG
    remove_entity(current_map, exit_y, 6);
#endif
#ifdef FIX_CHANGE_WIDTH
    current_map->width -= 2;
#endif

    player_right(current_map);
    player_right(current_map);
    player_right(current_map);

    verify_exit(current_map);
}
