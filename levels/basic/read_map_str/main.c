/* @AGDB
 * level_title: read_map_str
 * exec_name: read_map_str
 * engine_name: read_map_str
 * engine_bug : True
 * engine_tags:
 * engine_antitags:
 * 
 * available_commands: next step continue edit
 *
 * arcade_maps: main read_map_str_bugged.tmx
 * arcade_maps: correct read_map_str_correct.tmx
 */

#include "engine/agdbentures.h"
#include <stdlib.h>

int exit_y = 3;
int exit_x = 5;

static const char str_map[] = "\
########\n\
#      #\n\
#      #\n\
#  > @ #\n\
########\n\
";

int main() {
    map *current_map = load_map(str_map);

    player_right(current_map);
    player_right(current_map);

    verify_exit(current_map);
}