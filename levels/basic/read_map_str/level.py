#!/usr/bin/env python3

from level.level_abc import AbstractLevel


class Level(AbstractLevel):
    def arcade_custom_first_start(self):
        self.map_changed = False

        # map change should work automatically now
        # def post_update(player, memory):
            # frames = memory["stack"]
            # map = find_map(frames)
#
            # if map is not None and map.width == 8 and map.height == 5:
                # if not self.map_changed:
                    # payload = {
                        # "topic": "map_change",
                        # "map": "correct",
                    # }
                    # player.send_update(payload)
                    # self.map_changed = True
#
        # self.world.get_player().post_update = post_update

    def test(self):
        import tests.lib_test as T

        self.recompile_answer()
        T.ccontinue()
        T.expect_victory()
        self.run()
        self.check_validation(should_validate=True)

        self.recompile_bug()
        T.ccontinue()
        T.expect_defeat()
        self.run()
