/* @AGDB
 * level_title: Input Command
 * exec_name: input_command
 * engine_name: input_command
 * engine_bug : True
 * engine_tags:
 * engine_antitags:
 * available_commands: next step edit continue
 * new_commands_during_level: interrupt
 *
 * # TODO: are there any error in this level? Seems not.
 * # We could add an engine_bug: to be decided
 *
 * interactive: True
 * arcade_maps: main input_command.tmx
 *
 * coordinates: wop_init
 *
 * WOP: message intro
 * Ca y est, on va faire des vraies cartes, mais surtout on apporte de 
 * l'interaction dans le jeu !
 *
 *
 * Pour t'aider à faire la différence entre l'attente d'une prochaine commande, 
 * et l'attente d'une input dans le jeu, j'ai fait un écran de pause.
 *
 *
 *
 * PS: regarde également dans la console pour savoir si le programme attend une 
 * entrée de ta part.
 *
 *
 * Dans ce niveau, tu peux déplacer le personnage en entrant RIGHT dans la 
 * console, ou en utilisant les flêches du clavier.
 *
 * EndOfMessage
 *
 * WOP: message trash_down
 * Et bien, qu'est-ce que tu attends pour finir le niveau ?
 *
 * Insiste un peu !
 * EndOfMessage
 *
 * WOP: message help_down
 * Bon, tu as l'air coincé...
 *
 *
 * C'est le moment idéal pour apprendre à mettre en pause un niveau.
 *
 *
 *
 * Pour cela, utilise ce nouveau bouton "INTERRUPT", ou bien fait Ctrl-C dans 
 * cette fenêtre ou la console.
 * EndOfMessage
 *
 *
 * WOP: message interrupt
 * Le niveau est mis en pause.
 * Regarde dans la console, on peut voir que le programme a été interrompu 
 * pendant qu'il exécutait la fonction __GI___libc_read !
 *
 *
 * Hum, cette fonction n'est pas à nous.
 * Observe la pile des appels de fonctions pour voir d'où vient cet appel.
 *
 *
 *
 * Pour voir la pile des appels, tape "backtrace" (ou "bt") dans la console.
 * EndOfMessage
 *
 * WOP: message backtrace
 * Ah, on voit que `main` a appelé `get_next_command`, qui elle même a appelé 
 * des fonctions internes de la libc.
 *
 *
 * Pour sortir de ces fonctions, il faut les exécuter jusqu'au bout avec
 * la commande "finish" (ou "fin").
 * EndOfMessage
 *
 *
 * WOP: message finish
 * Le niveau s'est relancé !
 *
 *
 * C'est parce que la fonction attend une entrée de ta part.
 * Utilise une des flêches du clavier pour envoyer l'entrée à la fonction.
 * EndOfMessage
 *
 *
 *
 * OLD Message
 * Quand le symbole 'pause' apparaît en haut, et que la carte est grisée, le 
 * programme est en pause et on peut l'inspecter (print, next, etc.).
 *
 *
 * Quand le programme 'tourne', le symbole 'play' apparaît
 *
 * EndOfMessage
 *
 */

#include "engine/agdbentures.h"
#include <stdlib.h>

int exit_x = 8;
int exit_y = 3;

void place_walls(map* map)
{
    for (int x = 2; x <= 9; x++) {
        place_entity(map, WALL, 4, x);
    }
    place_entity(map, WALL, 3, 2);
    place_entity(map, WALL, 3, 5);
    place_entity(map, WALL, 3, 9);
    for (int x = 2; x <= 9; x++) {
        if (x < 4 || x > 6) {
            place_entity(map, WALL, 2, x);
        }
    }
    for(int x = 3; x <= 7; x++) {
        place_entity(map, WALL, 1, x);
    }
}

int main(void)
{
    map *current_map = init_map(6, 12);
    place_entity(current_map, EXIT, 3,8);
    place_walls(current_map);
    place_player(current_map, 3, 3);
    // main loop
    command* command;
    while (current_map->player_x != exit_x || current_map->player_y != exit_y) {
        printf("Enter move command (RIGHT, LEFT, UP, DOWN)\n");
        command = get_next_command();
        printf("Commande lue: '%s'\n", command->command_buffer);
        apply_input(current_map, command);
        free_command(command);
    }

    verify_exit(current_map);
}
