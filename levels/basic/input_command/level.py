#!/usr/bin/env python3

from level.level_abc import AbstractLevel
from level.action import Action
from logs import lvl


class Level(AbstractLevel):
    in_ok_sequence = ['RIGHT', 'UP', 'RIGHT', 'RIGHT', 'DOWN', 'RIGHT', 'RIGHT']


    def custom_first_start(self):
        self.wop = self.world.get_wop()
        entry = self.world.get_coordinates("wop_init")
        self.wop.place_on(entry)
        def add_wall():
            ent = self.tracker.get_variable_value_as_str("entity", "char")
            if ent == '#':
                lvl.debug(f"Adding a wall to the map")
                x = self.tracker.get_variable_value_as_str("x", "int")
                y = self.tracker.get_variable_value_as_str("y", "int")
                pl = {
                    "topic": "sprites",
                    "action": Action("create"),
                    "x": x,
                    "y": y,
                    "type": "wall",
                }
                self.send_to_gui(pl)

        def check_input():
            cmd = self.tracker.get_variable_value_as_str("c->command_buffer", "char*")
            lvl.debug(f"Apply input on {cmd}")
            if cmd == "DOWN":
                self.down_counter += 1
                lvl.debug(f"Number of down: {self.down_counter}")
            if self.down_counter > 1 and self.wop_down_state == 0:
                self.wop_down_state = 1
                self.wop.talk("trash_down")
            if self.down_counter > 4 and self.wop_down_state == 1:
                self.wop_down_state = 2
                self.wop.talk("help_down")

                pl = {
                    "topic": "gui_change",
                    "action": "show_button",
                    "command": "interrupt",
                }
                self.wop.send_update(pl)


        self.register_breakpoint("place_entity", add_wall)
        self.register_breakpoint("apply_input", check_input)

        self.wop.visible = True
        player = self.world.get_player()
        self.wop.send_update()
        self.wop.talk("intro")

        self.my_set_reset()

        pl = {
            "topic": "sprites",
            "action": Action("hide_layer"),
            "layer": "walls",
        }
        self.send_to_gui(pl)


    def my_set_reset(self):
        self.down_counter = 0
        self.wop_down_state = 0
        self.wop_interrupt = False
        self.wop_bt = False
        self.wop_finish = False


    def interrupt_inferior(self):
        """
        Override original method to trigger message from wop
        """
        if not self.wop_interrupt:
            # only talk once
            self.wop_interrupt = True
            self.wop.talk("interrupt")
        super().interrupt_inferior()


    def parse_and_eval(self, cmdline:str):
        """
        Catch the backtrace to give more information
        """
        if not self.wop_bt and len(cmdline) >= 2 and (cmdline == "bt" or "backtrace".startswith(cmdline)):
            self.wop_bt = True
            self.wop.talk("backtrace")

        if not self.wop_finish and len(cmdline) >= 3 and "finish".startswith(cmdline):
            self.wop_finish = True
            self.wop.talk("finish")

        return super().parse_and_eval(cmdline)

    def pre_validation(self):
        self.checker.append_inputs(
            self.in_ok_sequence
        )

    def test_input(self, T):
        for i in self.in_ok_sequence:
            T.send_input(i)

    def test_walls_input(self, T):
        T.send_input('RIGHT')
        T.send_input('RIGHT')
        T.send_input('RIGHT')
        T.send_input('UP')
        T.send_input('UP')
        T.send_input('RIGHT')
        T.send_input('RIGHT')
        T.send_input('RIGHT')
        T.send_input('DOWN')
        T.send_input('DOWN')
        T.send_input('RIGHT')
        T.send_input('RIGHT')


    def test(self):
        import tests.lib_test as T

        self.recompile_bug()

        T.payload(action='talk', message_substr='vraies cartes')
        T.payload(action='hide_layer')

        self.test_walls_input(T)
        for _ in range(3):
            T.send_input('DOWN')
        T.ccontinue(timeout=0.3)
        T.expect_infinite_loop()
        T.payload(action='talk', message_substr='que tu attends')
        T.payload(action='talk', message_substr='INTERRUPT')
        T.payload(topic='gui_change')
        # finally, the interrupt of the timeout should have triggered the next 
        # message
        T.payload(action='talk', message_substr='backtrace')
        T.command("backtrace")
        T.expect_sub_strings("__GI___libc_read", "sysv/linux/read.c")
        T.payload(action='talk', message_substr='get_next_command')

        # Giving additional input so we can 'finish' the current read
        T.send_input('DOWN')
        T.command("finish")
        T.expect_sub_strings("Run till exit", '_IO_new_file_underflow')
        T.payload(action='talk', message_substr='attend une entrée')
        T.command("finish")
        T.command("finish")
        # We should be back from the getline call
        T.expect_sub_strings("Run till exit", 'linesize = getline')
        self.run()

        self.recompile_bug()
        self.test_input(T)
        T.ccontinue(timeout=0.1)
        T.expect_infinite_loop()
        self.run()
        self.check_validation(should_validate=False)


        self.recompile_answer()
        self.test_input(T)
        T.ccontinue()
        T.expect_victory()
        self.run()
        self.check_validation(should_validate=True)


        self.reset()
        self.test_walls_input(T)
        T.ccontinue()
        T.expect_victory()
        self.run()
        self.check_validation(should_validate=True)
