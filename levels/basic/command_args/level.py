#!/usr/bin/env python3

from level.level_abc import AbstractLevel
from logs import cus
from language import lmessage, Lang

import graphic.constants as cst


class Level(AbstractLevel):

    def arcade_custom_first_start(self):
        wop = self.world.get_wop()
        player = self.world.get_player()
        monster = self.world.get_object("monster")

        init = self.world.get_coordinates("wop_init")
        talk = self.world.get_coordinates("wop_talk")
        end = self.world.get_coordinates("wop_end")
        player_talk = self.world.get_coordinates("player_talk")
        congrats = self.world.get_coordinates("wop_congrats")

        wop.visible = True
        wop.place_on(init)
        wop.send_update()
        wop.place_on(talk)
        wop.send_update()

        self.talk_done = False

        def monster_direction_update():
            directions = {
                (16,14) : cst.Direction.UP,
                (16,12) : cst.Direction.LEFT,
                (3,12) : cst.Direction.UP,
                (3,6) : cst.Direction.RIGHT,
                (16,6) : cst.Direction.UP
            }
            coords = (monster.coord_x, monster.coord_y)
            if coords in directions:
                monster.direction = directions[coords]
                monster.send_update()

        def player_custom_update(player, m):
            frames = m["stack"]
            x, y = player.coord_x, player.coord_y

            if player.is_on(player_talk) and not self.talk_done:
                wop.talk("intro")
                wop.talk("monster")
                wop.place_on(end)
                wop.direction = cst.Direction.RIGHT
                wop.send_update()

                monster.visible = True
                monster.direction = cst.Direction.LEFT
                monster.send_update()

                self.talk_done = True

            if player.is_on(congrats):
                wop.talk("congrats")

            monster_direction_update()


        player.post_update = player_custom_update

    def pre_validation(self):
        self.checker.append_inputs(
            ['LEFT_N 1','UP_N 6','RIGHT_N 13','UP_N 3','LEFT_N 15']
        )

        self.exit_x = self.checker.tracker.get_variable_value_as_str('exit_x', "int")
        self.exit_y = self.checker.tracker.get_variable_value_as_str('exit_y', "int")
        self.checker.count_calls('apply_input')

    def post_validation(self):
        
        if self.exit_x != 1 or self.exit_y != 3:
            self.checker.failed(
                lmessage(
                    {
                        Lang.EN: "The exit shouldn't move",
                        Lang.FR: "La sortie ne devrait pas bouger"
                    }
                )
            )

        if self.checker.stats['count_calls']['apply_input'] <= 4:
            self.checker.failed(
                lmessage(
                    {
                        Lang.EN: "The player must only move with the commands",
                        Lang.FR: "Le joueur doit seulement se déplacer avec les commandes"
                    }
                )
            )

    def test_input(self,T):
        T.send_input("LEFT")
        T.send_input("UP_N 6")
        T.send_input("RIGHT_N 13")
        T.send_input("UP_N 3")
        T.send_input("LEFT_N 15")

    def test_input_death(self,T):
        T.send_input("LEFT")
        T.send_input("UP")
        T.send_input("UP")

    def test(self):

        import tests.lib_test as T

        self.recompile_answer()
        self.test_input(T)
        T.ccontinue()
        T.expect_victory()
        self.run()
        self.check_validation(should_validate=True)

        self.recompile_bug()
        self.test_input(T)
        T.ccontinue()
        T.expect_defeat()
        self.run()

        self.recompile_answer()
        self.test_input_death(T)
        T.ccontinue()
        T.expect_defeat()
        self.run()


        self.recompile_fix("without_input")
        self.test_input(T)
        T.ccontinue()
        T.expect_victory()
        self.run()
        self.check_validation(should_validate=False)
