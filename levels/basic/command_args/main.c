/* @AGDB
 * level_title: Command args
 * exec_name: command_args
 * engine_name: command_args
 * engine_bug : True
 * engine_tags:
 * engine_antitags:
 * 
 * available_commands: next step edit continue
 * arcade_maps: main command_args.tmx
 * 
 *
 * coordinates: wop_init wop_end wop_congrats
 *
 * WOP: messageEN intro
 * Phew ! I thought I had lost you !
 * Follow this path so we can meet on the other side.
 * EndOfMessage
 *
 * WOP: messageFR intro
 * Ouf ! Je pensais t'avoir perdu !
 * Suit ce chemin et nous pourrons nous retrouver de l'autre côté.
 * EndOfMessage
 *
 * WOP: messageEN monster
 * Oh no ! Behind you ! A goblin !
 * Find a command to run faster !
 * EndOfMessage
 *
 * WOP: messageFR monster
 * Oh non ! Derrière toi ! Un goblin !
 * Trouve une commande pour courir plus vite !
 * EndOfMessage
 *
 * WOP: messageEN congrats
 * Great ! Now cross the bridge to be safe.
 * He won't follow you anymore.
 * EndOfMessage
 *
 * WOP: messageFR congrats
 * Bien vu ! Maintenant traverse le pont pour être en sécurité.
 * Il ne te suivra plus.
 * EndOfMessage
 */

#include "engine/agdbentures.h"
#include "engine/map.h"
#include <stdlib.h>
#include <stdbool.h>

int exit_x = 1;
int exit_y = 3;

int monster_x = 16;
int monster_y = 14;
entity monster = 'M';
int monster_destinations[][2] = {{16,12}, {3,12}, {3,6}, {16,6}, {-1,-1}};
int next_dest = 0;

static const char str_map[] = "\
                    \n\
                    \n\
  ################  \n\
 @               #  \n\
  ############## #  \n\
~ ~~~~~~~~~~~~~~ ~  \n\
  ~              ~  \n\
  ~ #############~  \n\
  ~ #               \n\
  ~ #           #~  \n\
  ~ #           #~  \n\
  ~ #############~~~\n\
  ~ >            # ~\n\
  ~############# #  \n\
  ~            # #  \n\
";

// Check if the monster kills the player
void verify_monster(map* m){
  if (monster_x == m->player_x && monster_y == m->player_y) {
    place_entity(m, monster, monster_y, monster_x);
    show_map(m);
    printf("The monster killed you !\n");
    exit(EXIT_FAILURE);
  }
}

// Give the next coordinates of the monster
void get_monster_destination(int* dest_x, int* dest_y){
  int* dest = monster_destinations[next_dest];
  *dest_x = dest[0];
  *dest_y = dest[1];
  if (*dest_x != -1 && *dest_y != -1){
    next_dest++;
  }
}

void move_monster(map* m){
  int dest_x, dest_y;
  get_monster_destination(&dest_x, &dest_y);
  if (dest_x == -1 || dest_y == -1){
    return;
  }

  int* coord_to_change;
  int dest;
  if( dest_y != monster_y ){ // x doesn't change
    coord_to_change = &monster_y;
    dest = dest_y;
  }else if( dest_x != monster_x ){ // y doesn't change 
    coord_to_change = &monster_x;
    dest = dest_x;
  }else{ // the monster doesn't move
    verify_monster(m);
    return;
  }

  remove_entity(m, monster_y, monster_x);

  // Moving the monster to it's next destination
  while(*coord_to_change != dest){
    if(*coord_to_change < dest){
      (*coord_to_change)++;
    }else{
      (*coord_to_change)--;
    }
    verify_monster(m);
  }

  place_entity(m, monster, monster_y, monster_x);
}

int main() {
  map *current_map = load_map(str_map);
  place_entity(current_map, monster, monster_y, monster_x);
  show_map(current_map);

#ifndef FIX_WITHOUT_INPUT
  command* command;
  while(true)  {
      printf("Enter move command (RIGHT, LEFT, UP, DOWN ...)\n");
      command = get_next_command();
      apply_input(current_map, command); // moving the player
      move_monster(current_map);
      show_map(current_map);
      free_command(command);

      //leave loop if player won
      if ( current_map->player_x == exit_x && current_map->player_y == exit_y ){
        break; 
      }
  }
#else
  player_left(current_map);
  player_up_n(current_map, 6);
  player_right_n(current_map, 13);
  player_up_n(current_map, 3);
  player_left_n(current_map, 15);
#endif

  verify_exit(current_map);
}
