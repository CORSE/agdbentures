#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <assert.h>

#include "engine/map.h"
#include "custom_map.h"

bool in_map(int y, int x)
{
    return x >= 0 && x < MAP_WIDTH &&
           y >= 0 && y < MAP_HEIGHT;
}

void place_walls(map* map)
{
    for (int y = 0; y < map->height; y++)
    {
        place_entity(map, WALL, y, (map->width/2));
    }
}

bool is_guard(map* map, int y, int x)
{
    if (in_map(y, x) && get_entity(map, y, x) == GUARD) {
        return true;
    }
    return false;
}

void move_guard(map* map)
{
    /*FR vérification que le garde est bien à droite du joueur */
    /*EN verifying the guard is on the right of the player */
    assert(is_guard(map, map->player_y, map->player_x+1));

    right(map, map->player_y, map->player_x+1);
    up(map, map->player_y, map->player_x+2);
}

map* create_custom_map(int height, int width)
{
    map* custom_map = init_map(height, width);

    // placement des murs
    place_walls(custom_map);

    // placement du garde
    place_entity(custom_map, GUARD, custom_map->height/2,
                 custom_map->width/2);

    // placement du joueur
    place_player(custom_map, (custom_map->height/2), 0);

    // placement de la sortie
    place_entity(custom_map, EXIT, (custom_map->height/2), custom_map->width-1);

    return custom_map;
}
