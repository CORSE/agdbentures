#!/usr/bin/env python3

from level.level_abc import AbstractLevel
import graphic.constants as cst
from logs import cus
from language import lmessage, Lang


class Level(AbstractLevel):
    def arcade_custom_first_start(self):
        player = self.world.get_player()
        wop = self.world.get_outside_object("wop")
        guard = self.world.get_object("guard")

        wop.visible = True
        wop.place_at(3, 3)
        wop.send_update()

        def player_custom_update(player, m):
            if player.is_on_left(guard):
                guard.direction = cst.Direction.LEFT
                guard.send_update()
                # guard.has_changed = True
                # guard.notify_update()

        player.post_update = player_custom_update
        self.register_breakpoint("place_player", lambda: wop.talks('intro'))

        # self.my_set_reset()

    #
    #
    # def my_set_reset(self):
    # guard = self.world.get_object("guard")
    # guard.visible = True
    #
    # def arcade_custom_restart(self):
    # self.my_set_reset()

    def pre_validation(self):
        self.checker.append_inputs(["Swordfis", "Swordfishlong", "Swordfish"])

        num_messages = 0

        def check_messages():
            nonlocal num_messages
            msg = self.checker.tracker.get_variable_value_as_str("msg", "char*")

            num_messages += 1

            if num_messages == 1:
                if not msg.startswith("Halte !"):
                    self.checker.failed(
                        lmessage(
                            {
                                Lang.EN: "First guard message is wrong.",
                                Lang.FR: "Le premier message du garde est incorrect.",
                            }
                        )
                    )
            elif num_messages == 2:
                if msg.startswith("Correct !"):
                    self.checker.failed(
                        lmessage(
                            {
                                Lang.EN: "Too short password accepted as valid.",
                                Lang.FR: "Mot de passe trop court accepté comme correct.",
                            }
                        )
                    )
            elif num_messages == 3:
                if msg.startswith("Correct !"):
                    self.checker.failed(
                        lmessage(
                            {
                                Lang.EN: "False password accepted as valid.",
                                Lang.FR: "Mauvais mot de passe accepté comme correct.",
                            }
                        )
                    )
            elif num_messages == 4:
                if not msg.startswith("Correct !"):
                    self.checker.failed(
                        lmessage(
                            {
                                Lang.EN: "Correct password not accepted.",
                                Lang.FR: "Mot de passe correct non accepté.",
                            }
                        )
                    )

        self.checker.register_breakpoint('message', check_messages)

    def test(self):
        import tests.lib_test as T

        self.recompile_bug()
        T.send_input("Swordfish")
        T.send_input("Lobster")
        T.send_input("Eel")
        T.ccontinue()
        T.expect_sub_strings("Faux")
        T.expect_defeat()
        self.run()

        self.recompile_answer()
        T.send_input("Salmon")
        T.send_input("Swordfish")
        T.ccontinue()
        T.expect_sub_strings("Correct")
        T.expect_victory()
        self.run()
        self.check_validation(should_validate=True)

        self.recompile_answer()
        T.send_input("Lobster")
        T.send_input("Whale")
        T.send_input("Squid")
        T.ccontinue()
        T.expect_sub_strings("Faux")
        T.expect_defeat()
        self.run()
