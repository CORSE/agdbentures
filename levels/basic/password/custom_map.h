#ifndef CUSTOM_MAP_H
#define CUSTOM_MAP_H

#include "engine/agdbentures.h"

//FR Caractère ASCII représentant le garde
//EN ASCII character representing the guard
#define GUARD 'G'

#define MAP_WIDTH 14
#define MAP_HEIGHT 9

//FR Renvoie True si le point (x,y) est à l'intérieur de la carte
//EN Returns True if point (x,y) is inside of the map
bool in_map(int y, int x);

//FR Renvoie True si le garde est à la position (x,y)
//EN Returns True if the guard is at position (x,y)
bool is_guard(map* map, int y, int x);

//FR Déplace le garde d'une case à droite puis en haut
//EN Moves the guard one step to the right, then one up
void move_guard(map *map);

//FR Crée la carte pour ce niveau
//EN Generates the map for this level
map *create_custom_map(int height, int width);

#endif // CUSTOM_MAP_H
