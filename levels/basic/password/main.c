/* @AGDB
 * level_title: password
 * exec_name: password
 * engine_name: simple_map
 * engine_tags:
 * engine_antitags:
 *
 * available_commands: next step edit continue
 *
 * arcade_maps: main password.tmx
 *
 * HINT1 how is the password verified ?
 *
 * WOP: message intro
 * Tu sais ce qui manque à ce jeu ?
 *
 * ...
 *
 *
 * Non, le moteur de jeu et la carte c'est fait depuis belle lurette...
 *
 *
 * Ce qu'il manque c'est de l'INTERACTION !
 *
 *
 *
 * Depuis le temps qu'on se connait, je parle, je parle et jamais tu ne peux
 * répondre ! Mais ça va changer. Tiens, va discuter avec ce charmant
 * personnage, je suis sûr que vous allez vous entendre.
 * EndOfMessage
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

#include "engine/agdbentures.h"
#include "custom_map.h"

#define MAX_PASSWORD_LENGTH 30

int exit_x = MAP_WIDTH-1;
int exit_y = (MAP_HEIGHT/2);

const char password[MAX_PASSWORD_LENGTH] = "Swordfish";

bool check_password(char* user_input)
{
#ifdef FIX_ALL_OK
    return true; // Accepts all inputs as valid passwords
#endif
#ifdef FIX_TOO_SHORT
    int len = strlen(password);
    return ! strncmp(password, user_input, len-1);
#endif
#ifdef BUG
    if (password==user_input) return true;
    return false;
#elif BUG_DIFFICILE
    int i = 0;
    while (password[i]!='\0' && user_input[i]!='\0'){
        if (password[i]!=user_input[i]){
            break;
        }
        i++;
    }
    if (password[i]==user_input[i]) return true;
    return false;
#else
    int len = strlen(password);
    int ulen = strlen(user_input);
    user_input[ulen] = '\0';  // remove trailing '\n'
    ulen--;
    if (len != ulen) return false;
    return ! strncmp(password, user_input, len);
#endif
}

int main(void)
{
    char user_password[MAX_PASSWORD_LENGTH];
    int attempts = 3; // Trois essais max pour le mot de passe

    // Création de la carte de ce niveau (cf. code source dans custom_map.c)
    map* current_map = create_custom_map(MAP_HEIGHT, MAP_WIDTH);

    // Tentative d'atteindre la sortie à droite
    while (current_map->player_x < current_map->width-1) {
        if (! is_guard(current_map,
                       current_map->player_y,
                       current_map->player_x+1)) {
            // on peut aller à droite si on n'est pas devant le garde
            player_right(current_map);
            continue;
        }
        // Arrivés devant le garde
        if (attempts == 0) {
            message("Vous avez echoue trois fois ! Vous ne pouvez pas passer !");
            break;
        }
        if (attempts == 3) {
            // Premier essai: message d'introduction
            message("Halte ! Si vous voulez passer, il faut donner le mot de passe !");
        }
        prompt("Quel est le mot de passe ?", user_password, MAX_PASSWORD_LENGTH);

        printf("Vous avez dit: '%s'\n", user_password);
        attempts--;

        if (! check_password(user_password)) {
            message("Faux !");
            continue;
        }

        // Mot de passe correct !
        message("Correct ! Vous pouvez passer...");
        move_guard(current_map);
   }

    verify_exit(current_map);
}
