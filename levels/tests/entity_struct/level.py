#!/usr/bin/env python3

from level.level_abc import AbstractLevel
from logs import cus
import tests.lib_test as T


class Level(AbstractLevel):
    """01_intro"""

    def arcade_custom_first_start(self):
        pass

    def test(self):

        self.recompile_bug()
        # T.skip()

        T.cnext(2)
        T.variable("current_map->width", 12)

        T.cnext(6)

        # map change here
        T.variable("current_map->width", 9)

        T.variable("exit_y", 6)
        T.hook(self.check_world_exit_y, 6)

        T.cnext(2)

        T.variable("exit_y", 4)
        T.hook(self.check_world_exit_y, 4)
        T.ccontinue()

        T.expect_defeat()
        self.run()
