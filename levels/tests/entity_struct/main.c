/* @AGDB
 *
 * Test entity_struct engine here.
 * exec_name:    test
 * engine_name:  entity_struct
 *
 * available_commands: next step edit
 *
 * no_verify: must_call verify_exit
 * no_verify_exit: cond player_y == exit_y
 *
 * arcade_maps: main test.tmx
 **/
#include "custom_map.h"
#include "engine/agdbentures.h"
#include <stdio.h>
#include <stdlib.h>

void run_loop(game_instance *game)
{
    map *map = current_map(game->map_stack);
    command *command;

    while (!game->exit_main_loop) {
        command = get_next_command();
        apply_input(map, command);
        free_command(command);
    }
}

int main(void)
{
    game_instance *game = init_game();
    map *map = load_map("main", main_map);
    push_on_stack(game->map_stack, map);

    run_loop(game);
    /* Check whether we are on the right spot */
    /* verify_exit(current_map); */
}
