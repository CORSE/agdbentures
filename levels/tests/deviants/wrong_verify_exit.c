/* @AGDB
 * Calls verify exit, but does not really check player position
 *
 * verify_init: player_x 0
 * verify_always: exit_x 1
 */

#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>

int player_x=0;
int exit_x=1;

void forward(void)
{
    player_x = player_x + 1;
}

void verify_exit(void)
{
    if (false) {
        printf("DEFAITE!\n");
        exit (EXIT_FAILURE);
    } else {
        printf("VICTOIRE!\n");
        exit (EXIT_SUCCESS);
    }
}

int main(void)
{
    verify_exit();
}
