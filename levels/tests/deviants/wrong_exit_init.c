/* @AGDB
 * Places the exit on the player initially.
 *
 * verify_init: player_x 0
 * verify_always: exit_x 1
 */

#include <stdlib.h>
#include <stdio.h>

int player_x=0;
int exit_x=0;

void forward(void)
{
    player_x = player_x + 1;
}

void verify_exit(void)
{
    if (player_x != exit_x) {
        printf("DEFAITE!\n");
        exit (EXIT_FAILURE);
    } else {
        printf("VICTOIRE!\n");
        exit (EXIT_SUCCESS);
    }
}

int main(void)
{
    verify_exit();
}
