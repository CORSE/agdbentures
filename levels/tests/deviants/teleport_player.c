/* @AGDB
 * Changes the player position in forward but with too much difference.
 *
 * verify_init: player_x  0
 * verify_always: exit_x 42
 */

#include <stdlib.h>
#include <stdio.h>

int player_x=0;
int exit_x=42;

void forward(void)
{
    player_x = exit_x;
}

void verify_exit(void)
{
    if (player_x != exit_x) {
        printf("DEFAITE!\n");
        exit (EXIT_FAILURE);
    } else {
        printf("VICTOIRE!\n");
        exit (EXIT_SUCCESS);
    }
}

int main(void)
{
    forward();
    verify_exit();
}
