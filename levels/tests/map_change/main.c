/* @AGDB
 *
 * Test basic agdbentures features here.
 * Features that could be used in tutorial (no engine).
 *
 *
 * level_title:   ?-? Test
 * exec_name:    test
 * engine_name:  simple_map
 *
 * available_commands: next step edit
 *
 * no_verify: must_call verify_exit
 * no_verify_exit: cond player_y == exit_y
 *
 * arcade_maps: main test.tmx
 * arcade_maps: tele test2.tmx
 * # arcade_maps: tele_offset 100 0
 *
 * OBJteleport:
 * coordinates: entry
 **/
#include <stdlib.h>
#include "engine/agdbentures.h"
#include <stdio.h>

/** @AGDB
 * Player status
 * player_y: 2
 *
 * Map goal
 * exit_y: 4
 *
 */

#define INIT_PLAYER_X 4
#define INIT_PLAYER_Y 5

int exit_x = 6;
int exit_y = 6;

#define TELE_X 7

map *current_map;  // points to either main or tele
map *main_map;     // starting map
map *tele_map;     // map where to teleport to


void forward(map *map)
{
    player_right(map);

    if (map == tele_map) {
        // Try to move exit to see what happens
        move_entity(tele_map, exit_y, exit_x, exit_y-1, exit_x);
        exit_y--;
    }
    if (map->player_x == TELE_X) {
        // Teleport player to tele_map
        place_player(tele_map, exit_y, exit_x-4);
        current_map = tele_map;
    }


    show_map(map);
}

int main(void)
{
    main_map = init_map(9, 12);
    current_map = main_map;
    place_player(current_map, INIT_PLAYER_Y, INIT_PLAYER_X);

    tele_map = init_map(9, 9);
    place_entity(tele_map, EXIT, exit_y, exit_x);

    /* Number of times do we have to move to reach the exit */
    forward(current_map);
    forward(current_map);
    forward(current_map); // should teleport here

    forward(current_map);


    forward(current_map);

    /* Check whether we are on the right spot */
    verify_exit(current_map);
}
