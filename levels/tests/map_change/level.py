#!/usr/bin/env python3

from level.level_abc import AbstractLevel
from logs import cus
import tests.lib_test as T


class Level(AbstractLevel):
    """01_intro"""

    def arcade_custom_first_start(self):

        player = self.world.get_player()

        main_addr = None
        tele_addr = None
        prev_current_addr = None

        def player_custom_update(player, *args):
            nonlocal main_addr, tele_addr, prev_current_addr
            # When player starts moving, maps should be allocated

            if main_addr is None:
                main_addr = self.world.get_pointer_address("main_map")
                tele_addr = self.world.get_pointer_address("tele_map")
                prev_current_addr = self.world.get_pointer_address("current_map")

                cus.info(f"Addresses of main 0x{main_addr:02x} and tele 0x{tele_addr:02x}")

                assert prev_current_addr == main_addr

            current_addr = self.world.get_pointer_address("current_map")

            if current_addr != prev_current_addr:
                cus.info(f"Current address changed from 0x{prev_current_addr:02x} to 0x{current_addr:02x}")

                if tele_addr == 0:  # TODO: fix this problem
                    cus.error("Adress of `tele_map` is NULL but should not")

                    tele_addr = self.world.get_pointer_address("tele_map")
                    cus.info(f"Tele address is 0x{tele_addr:02x}")

                if current_addr == tele_addr:
                    payload = {
                        "topic": "map_change",
                        "map": "tele",
                    }
                    self.world.send_update(payload)
                else:
                    cus.warning(f"Do not know where the current map is")

            prev_current_addr = current_addr

        player.post_update = player_custom_update


    def check_world_exit_y(self, y):
        ex = self.world.exit
        T.assert_eq(y, ex.coord_y)

    def test(self):

        self.recompile_bug()
        # T.skip()

        T.cnext(2)
        T.variable("current_map->width", 12)

        T.cnext(6)

        # map change here
        T.variable("current_map->width", 9)

        T.variable("exit_y", 6)
        T.hook(self.check_world_exit_y, 6)

        T.cnext(2)

        T.variable("exit_y", 4)
        T.hook(self.check_world_exit_y, 4)
        T.ccontinue()

        T.expect_defeat()
        self.run()
