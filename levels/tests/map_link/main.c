/** @AGDB
 * level_title: test_graph
 * exec_name: test_graph
 * engine_name: map_graph
 * available_commands: edit next step continue interrupt
 *
 * interactive: True
 * verify_exit: cond exit_x == 4
 * verify_exit: cond exit_y == 7
 *
 * arcade_maps: main first_room.tmx
 * arcade_maps: second_room second_room.tmx
 * arcade_maps: third_room third_room.tmx
 */

#include "custom_map.h"
#include "engine/agdbentures.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int exit_x = 4;
int exit_y = 7;

void init_maps(game_instance *game)
{
    char *head_names[3] = {"third", "second", "first"};
    char *tail_name = "_room";
    char name[15];
    for (int i = 0; i < 3; i++) {
        strcpy(name, head_names[i]);
        strcat(name, tail_name);
        map *m = load_map(name, i == 0   ? str_map3
                                : i == 1 ? str_map2
                                         : str_map1);
        push_on_stack(game->map_stack, m);
    }
    game->current_map = game->map_stack->maps[0];
}

void check_exit(game_instance *game)
{
    if (strcmp(game->current_map->name, "third_room") == 0) {
        verify_exit(game->current_map);
    }
}

void execute_loop(game_instance *game)
{
    /* int dest_y, dest_x; */
    map *room_map = current_map(game->map_stack);
    place_player(room_map, 6, 5);
    // inputs loop
    while (!game->exit_main_loop) {
        // read command
        command *input_command = get_next_command();
        printf("ça c'est le retour de apply_input : %d\n",
               apply_input(room_map, input_command));
        free_command(input_command);
        printf("coord x du jouer : %d et pour y : %d\n", room_map->player_x,
               room_map->player_y);
        show_map(room_map);
        map *dest = take_map_link(game, room_map);
        if (dest != NULL) {
            pop_from_stack(game->map_stack);
            room_map = current_map(game->map_stack);
            printf("le nom de ma map : %s \n", room_map->name);
        }
        check_exit(game);
    }
}

int main()
{
    game_instance *game = init_game();
    /* map *room; */
    init_maps(game);
    add_link_to_map(game->map_stack->maps[2], 5, 5, game->map_stack->maps[1], 2,
                    2);
    add_link_to_map(game->map_stack->maps[1], 6, 6, game->map_stack->maps[0], 5,
                    5);

    // rooms loop
    execute_loop(game);
}
