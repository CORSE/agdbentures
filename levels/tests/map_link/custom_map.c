#include "custom_map.h"

const char *str_map1 =
"\
+---------------+\n\
|              @|\n\
|               |\n\
|               |\n\
|               |\n\
|               |\n\
|  >            |\n\
+---------------+\n\
";

//5 UP 12 RIGHT


const char *str_map2 =
"\
+---------------+\n\
|               |\n\
|               |\n\
|               |\n\
|               |\n\
|       @       |\n\
|               |\n\
+---------------+\n\
";

// 4 DOWN    7 RIGHT

const char *str_map3 = // the monster can move two tiles for 1 movement of the player + follow him
"\
+---------------+\n\
|               |\n\
|               |\n\
|   @           |\n\
|               |\n\
|               |\n\
|               |\n\
|               |\n\
+---------------+\n\
";

// initialize the monsters : first stat : id, direction and coordinates
