#!/usr/bin/env python3

from level.level_abc import AbstractLevel
import graphic.constants as cst
import arcade
from level.action import Action
from language import lmessage, Lang


class Level(AbstractLevel):
    """test map_link"""

    def arcade_custom_restart(self):
        self.arcade_custom_first_start()

    def arcade_custom_first_start(self):
        # exit = self.world.get_object("exit")
        # exit.place_at(-1, -1)
        # exit.visible = False
        # exit.send_update()
        player = self.world.get_player()
        self.current_map = "main"
        self.tag_map = ['f', 'i', 'r', 's', 't', '_', 'r', 'o', 'o', 'm', '\x00']

        def change_map(map_name):
            self.current_map = map_name
            print(map_name)

            payload = {
                "topic": "map_change",
                "map": map_name,
            }
            self.send_to_gui(payload)
            if map_name == "third_room":
                # exit = self.world.get_object("exit")
                # exit.place_at(4, 7)
                # exit.send_update()
                payload = {"topic": "player_update", "action": Action("rescale")}
                self.player.send_update(payload)

        def detect_map_change():
            map_name = "third_room"
            if self.tag_map == [
                's',
                'e',
                'c',
                'o',
                'n',
                'd',
                '_',
                'r',
                'o',
                'o',
                'm',
                '\x00',
            ]:
                map_name = "second_room"

            change_map(map_name)

        # def post_update(player, memory):
            # frames = memory["stack"]
            # map = get_top_map(frames)
            # if map and map.name != self.tag_map:
                # print(map.name)
                # self.tag_map = map.name
                # detect_map_change()
#
        # player.post_update = post_update

    def pre_validation(self):
        self.checker.append_inputs(
            [
                "UP",
                "RIGHT",
                "DOWN",
                "RIGHT",
                "DOWN",
                "RIGHT",
                "DOWN",
                "RIGHT",
                "DOWN",
                "LEFT",
                "DOWN",
                "DOWN",
            ]
        )

    def post_validation(self):
        pass

    def test(self):
        import tests.lib_test as T

        self.recompile_answer()
        T.send_input("UP")
        T.send_input("RIGHT")
        T.send_input("DOWN")
        T.send_input("RIGHT")
        T.send_input("DOWN")
        T.send_input("RIGHT")
        T.send_input("DOWN")
        T.send_input("RIGHT")
        T.send_input("DOWN")
        T.send_input("LEFT")
        T.send_input("DOWN")
        T.send_input("DOWN")
        T.ccontinue()
        T.expect_victory()
        self.run()
