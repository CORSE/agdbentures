/* @AGDB
 *
 * Test what happens when some important variables are left out.
 * (E.g., deleted by mistake by user)
 *
 * Here, there is no engine, so we should have player_y and player_direction.
 *
 * available_commands: next edit step
 * engine_name: none
 *
 * no_verify: change_in_function player_x forward
 * no_verify: change_in_function player_y forward
 * no_verify: must_call verify_exit
 *
 * arcade_maps: main ../test.tmx
 */

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int player_x = 5;
int player_y = 3;
int exit_x = 5;
int exit_y = 3;
int player_direction = 0;

void infinite_loop(void)
{
    int count = 0;
    while (true) {
        sleep(1);
        printf("I'm in my infinite loop %d\n", ++count);
    }
}

void sleep_loop(void)
{
    sleep(1);
    return;
}

void no_loop(void) { return; }

int main()
{
#ifdef BUG
#if FIX_SLEEP
    sleep_loop();
#else
    infinite_loop();
#endif
#else
    no_loop();
#endif

    exit(EXIT_SUCCESS);
}
