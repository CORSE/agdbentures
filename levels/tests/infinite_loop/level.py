#!/usr/bin/env python3

from level.level_abc import AbstractLevel
import graphic.constants as cst
from logs import lvl


class Level(AbstractLevel):
    """Infinite loops"""

    def pre_validation(self):
        # We need to check that has_key is changed to true only
        # if player is on key, and that player passes through the door
        pass

    def post_validation(self):
        pass

    def test(self):
        import tests.lib_test as T

        self.recompile_bug()

        T.ccontinue(timeout=0.1)
        T.expect_infinite_loop()
        self.run()

        self.reset()
        T.cnext(
            timeout=0.1
        )  # Testing again, the next command should trigger the infinite loop
        T.expect_infinite_loop()
        self.run()

        # Uncomment to following to use default timeout of tests
        # Disabled since it adds 5 seconds to the whole tests
        # self.reset()
        # T.ccontinue()
        # T.expect_infinite_loop()
        # T.expect_string("toto")  # cannot do tests after infinite loops
        # self.run()

        self.check_validation(
            should_validate=False,
            substr_reasons="interrupted",
        )

        self.recompile_answer()

        T.ccontinue()
        T.unexpect_infinite_loop()
        self.run()

        self.check_validation(should_validate=True)

        # Was to test whether a timeout on previous test could interrupt the
        # next test. This does not seem possible since we stop the timeout when
        # progam exits, and the tracker are actually different.
        # self.recompile_fix("sleep")
        # T.sleep(1)
        # T.ccontinue(timeout=3.2)
        # T.unexpect_infinite_loop()
        # self.run()
