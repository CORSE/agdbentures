/* @AGDB
 * level_title: Reverse string
 * exec_name: rev_string
 * engine_name: none
 * engine_tags:
 * engine_antitags:
 * 
 */
 #include <stdio.h>
 #include <stdlib.h>
#include <string.h>

char* reverse(char* word) {
    int i = 0;
    while (word[i] != '\0') {
        i++;
    }
    // Allocate destination buffer
    char* dest = malloc((i+1)*sizeof(char));
    dest[i] = '\0';

    int k = 0;
#ifdef BUG
    i--;
#endif
    // go back to the beginning of the string while copying
#ifdef BUG
    while (word[i] != 0) {
#else
    while (i >= 0) {
#endif
        dest[k] = word[i];
        i--;
        k++;
    }

    return dest;
}

int main() {
    char word[] = "Hello World!";
    char* dest;

    dest = reverse(word);

    if(strcmp(dest, "!dlroW olleH") == 0) {
        printf("SUCCESS\n");
        return EXIT_SUCCESS;
    } else {
        printf("FAILURE %s should be \"!dlroW olleH\"\n", dest);
        return EXIT_FAILURE;
    }
}
