/* @AGDB
 * level_title: Transfer
 * exec_name: transfer
 * engine_name: none
 * engine_tags:
 * engine_antitags:
 * 
 */
#include <stdio.h>
#include <stdlib.h>

#ifdef BUG
// transfer the given quantity from the "from" variable to the "to" variable in place
void transfer(int from, int to, int quantity) {
    from -= quantity;
    to += quantity;
}
#else
// transfer the given quantity from the "from" variable to the "to" variable in place
void transfer(int *from, int *to, int quantity) {
    *from -= quantity;
    *to += quantity;
}
#endif

int main() {
    int account_a = 5;
    int account_b = 6;

    // transfer 3 from account_a to account_b
#ifdef BUG
    transfer(account_a, account_b, 3);
#else
    transfer(&account_a, &account_b, 3);
#endif

    if (account_a == 2 && account_b == 9) {
        printf("SUCCESS\n");
        return EXIT_SUCCESS;
    } else {
        printf("FAILURE account_a has %d and should be 2 and account_b has %d and should be 9\n", account_a, account_b);
        return EXIT_FAILURE;
    }
}
