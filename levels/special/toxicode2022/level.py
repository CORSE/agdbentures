#!/usr/bin/env python3

from level.level_abc import AbstractLevel
from level.objects import find_map


class Level(AbstractLevel):

    def arcade_custom_restart(self):
        self.arcade_custom_first_start()
    def arcade_custom_first_start(self):

        def next_color():
            pl = {
                "topic": "bubble_player",
                "action": "next_color",
            }
            self.send_to_gui(pl)

        def prev_color():
            pl = {
                "topic": "bubble_player",
                "action": "prev_color",
            }
            self.send_to_gui(pl)

        self.register_breakpoint("g", next_color)
        self.register_leave_function("g", prev_color)


    def pre_validation(self):
        pass

    def post_validation(self):
        pass

    def test(self):
        pass
