
/* @AGDB
 * level_title: toxicode
 * exec_name: toxicode
 * engine_name: map_entity_stacking
 * available_commands: next step continue interrupt
 *
 * test: true
 * player_mode: simple_map
 * exit_x: 200
 * exit_y: 100
 *
 * player_bubble: True
 *
 * arcade_maps: main toxicode.tmx
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "engine/agdbentures.h"


int exit_x=4;
int exit_y=3;

int initx = 5;
int inity = 5;

map *themap;

#define FaireNfois(x) for (int _i=0; _i<x; _i++)
/* #define GRIS 'o' */
/* #define BLANC '.' */
/* #define HACHURE 'x' */
#define GRIS AREA_GRAY
#define BLANC AREA_WHITE
#define HACHURE AREA_HASHED

#if 0
#define gauche() player_left(themap)
#define droite() player_right(themap)
#define haut() player_up(themap)
#define bas() player_down(themap)
#define couleur()  \
    themap->floor[themap->player_y][themap->player_x].category
#else


#define gauche() \
    printf("going left\n"); \
    player_left(themap)

#define droite() \
    printf("going right\n"); \
    player_right(themap) 

#define haut() \
    printf("going up\n"); \
    player_up(themap)

#define bas() \
    printf("going down\n"); \
    player_down(themap)

char *COULEUR[FLOORTYPE_N] = {
    "",
    "",
    "",
    "",
    "",
    "",
    "blanc",
    "gris",
    "hachuré",
};

#define couleur()  \
    ({ floor _c = themap->floor[themap->player_y][themap->player_x].category; \
      printf("Couleur: %s\n", COULEUR[_c]); \
      _c; })
#endif

void show_position(void)
{
    printf("Player position: %dx%d\n", themap->player_x, themap->player_y);
}
void verify_exit(map* map)
{
    if (map->player_x != exit_x || map->player_y != exit_y) {
        printf("DEFAITE!\nYou haven't reached the exit\n");
        free_map(themap);
        exit (EXIT_FAILURE);
    } else {
        printf("VICTOIRE!\nYou've reached the exit\n");
        free_map(themap);
        exit (EXIT_SUCCESS);
    }
}
char str_map[] =
"\
               \n\
               \n\
               \n\
   o.x.x.oxo   \n\
   ..x.ox.o.   \n\
   .x..x.xo.   \n\
   .....o...   \n\
   o.xxo.x.x   \n\
               \n\
               \n\
               \n\
";


void g(int x)
{
    if (x == 2) return;
    FaireNfois(x) {
        droite();
    }
    if (couleur() == GRIS) {
        x = x+1;
    }
    haut();
    g(x);
    FaireNfois(x) {
        gauche();
    }
}


void question15()
{
    g(1);
}


int main(void)
{
    map *current_map = load_map("main", str_map);
    themap = current_map;

    place_player(themap, initx, inity);
    question15();
    show_position();
    verify_exit(themap);
}
