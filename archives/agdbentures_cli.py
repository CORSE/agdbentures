""" A minimalist agdbentures version with just CLI to test levels"""

from level_controller import level_controller

from easytracker import init_tracker

tracker = init_tracker("GDB")

level_controller().load_level()

while True:
    print(tracker.get_console_output())  # type: ignore

    command = input("Command: ")
    if command == "quit":
        tracker.force_program_stop()
        break
    if command == "unlock":
        # check if current level is a success and load next level otherwise do nothing
        if level_controller().current_level.success:
            level_controller().unlock_next_level()
            level_controller().load_level()

            continue
    pause_reason = tracker.send_direct_command(command)

    print(pause_reason)

    level_controller().on_tracker_breaked(command, pause_reason)
