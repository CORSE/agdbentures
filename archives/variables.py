"""Variables monitoring/expansion tools."""

import ast
from typing import Any, Optional


# pylint: disable=eval-used
def value(var: str, env: dict[str, Any]) -> Any:
    """
    Evaluate 'var' expression with local environment 'env'.
    """
    assert var != ""
    try:
        return eval(var, {}, env)
    except (KeyError, AttributeError, TypeError, NameError):
        return None


def expand_expr(expr: str, alias: dict[str, str]) -> str:
    """
    Substitute aliased variables in 'expr'.

    :param expr: The expression to resolve.
    :param alias: Dictionnary of aliases.
    :return: The resolved expression.
    """
    # TODO: Florent: normally should not be passed smth != than string...
    # but the objects are not well implemented it seems
    if type(expr) != str:
        return
    for var, inferior_var in alias.items():
        expr = expr.replace(var, inferior_var)
    return expr


def expand_condition(cond: str, obj_x, obj_y) -> str:
    """
    Transform a condition based on keywords to condition
    based on coordinates vs. the player.
    """
    cond = cond.strip()
    if cond == "always":
        return "True"
    elif cond == "ontop":
        return (
            f"player_x == {obj_x}"
            f" and player_y == {obj_y}"
        )
    elif cond == "near":
        return (
            f"abs(player_x - {obj_x}) <= 1"
            f" and abs(player_y - {obj_y}) <= 1"
        )
    elif cond == "below":
        return (
            f"player_x == {obj_x}"
            f" and player_y == {obj_y} + 1"
        )
    return cond



# pylint: disable=invalid-name
class AstVarFinder(ast.NodeVisitor):
    """Ast walker that finds variables."""

    def __init__(self):
        """AstVarFinder initialization."""
        self.variables = set()

    def visit_Call(self, node):
        """Visits a Call node. Skips the function name"""
        for arg in node.args:
            self.visit(arg)
        for keyword in node.keywords:
            self.visit(keyword)

    def visit_Name(self, node):
        """Visits a Name node. Adds the ids to a list"""
        self.variables.add(node.id)

    def find_variables(self, node):
        """Finds all the variables from a given node."""
        self.variables = set()
        self.visit(node)


# We only need one walker
_walker = AstVarFinder()


def list_variables(expr: str, alias: Optional[dict[str, str]] = None) -> set[str]:
    """
    Returns the variables that are used in a given expression.

    :param expr: The expression to parse.
    :return: The variables that are used in the expression.
    """
    if type(expr) != str:
        return set([])
    if alias is not None:
        expr = expand_expr(expr, alias)

    tree = ast.parse(expr, mode="eval")
    _walker.find_variables(tree)

    return _walker.variables
