"""
Events implementation for the python motor.
"""

from collections import defaultdict
from inspect import signature, Parameter
from typing import Callable, Optional, Any


class EventList:
    """
    Event list class.

    Stores events in a dictionnary of events.
    The value of this dictionnary are callback functions.
    """

    def __init__(self, variables):
        """
        Event list initialization.

        :param variables: Dictionnary of variables.
        """
        self.events = defaultdict(set)
        self.variables = variables

    def connect(self, event: str, function: Callable) -> None:
        """
        Connect a function to an event.

        :param event: The name of the event to listen.
        :param function: The function to call on update.
        If the function arity is 2, then on update,
        it will be called with the variable name and its
        new value. If the function arity is 1, it will be
        called with the updated value.
        """
        self.events[event].add(function)

    def disconnect(self, function: Callable, event: Optional[str] = None) -> None:
        """
        Disconnect a function from an event.

        :param function: The function to disconnect.
        :param event: The event to work on. If not given, disconnects the
                      function from all the events.
        """
        if event is not None:
            self.events[event].remove(function)
        else:
            for function_set in self.events.values():
                function_set.discard(function)

    def on_value_change(self, variables: set[str]) -> None:
        """
        Trigger the update of list of variables.

        :param variable_list: List of variables that have been updated.
        """

        # Should we store the arity somewhere instead of checking
        # every time we call the function?
        def arity(function: Callable) -> int:
            """Get the arity of a function."""
            count = 0
            for param in signature(function).parameters.values():
                if param.default is Parameter.empty:
                    count += 1
            return count

        for variable in variables:
            for function in self.events[f"value_change:{variable}"]:
                # Checking the function arity
                function_arity = arity(function)
                if function_arity not in [1, 2]:
                    raise RuntimeError(
                        f"Wrong arity for event callback funtion {function.__name__}"
                    )
                if function_arity == 1:
                    function(self.variables[variable])
                else:
                    function(variable, self.variables[variable])

    def trigger(self, event: str, *args: Optional[list[Any]]) -> None:
        """
        Trigger an event.

        :param event: The event to trigger.
        :param args: The arguments to give to the functions connected to the event.
        """
        for function in self.events[event]:
            function(*args)
