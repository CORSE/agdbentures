"""
Module that controls the pygame window.
"""
# disabling pylint for no-member from pygame
# pylint: disable=no-member
# disabling pylint wrong import position because the environnement variable has to be set
# before pygame import
# pylint: disable=wrong-import-position

# Standard imports
import os
from typing import Callable, Tuple

# Non standard imports
# silent pygame launch
# this has to be done before pygame import
os.environ["PYGAME_HIDE_SUPPORT_PROMPT"] = "hide"
import pygame

# Own imports
from level_controller import level_controller
from easytracker.init_tracker import init_tracker
from easytracker import PauseReason
from visualprimitives.pygame.window import (
    BasicWindow,
    TOP_RIGHT_ANCHORS,
    BOTTOM_RIGHT_ANCHORS,
)
from visualprimitives.pygame.gui.debugger_gui import BUTTON_WIDTH, BUTTON_HEIGHT
from visualprimitives.pygame.gui import source_code_gui, console, debugger_gui
from visualprimitives.pygame.game import Game

RIGHT_COLUMN_WIDTH = 2 * BUTTON_WIDTH + 50

NEXT_LEVEL_TEXT = "Next Level"
LOAD_LEVEL_TEXT = "Load level"


class MainWindow(BasicWindow):
    """
    Provides a graphical window using pygame.
    """

    def __init__(self):
        """
        Initialisation of the graphical windows (pygame),
        and some attributes.
        """
        super().__init__()
        pygame.display.set_caption("AGDBENTURES")

        self.tracker = init_tracker("GDB")

        self.init_debugger_gui()
        self.init_console()
        self.init_source_code_gui()
        self.init_game_view()

        # unlock the first level
        level_controller().unlock_next_level()

        self.main_loop_callback = lambda: self.game.draw(self.window_surface)

    def init_debugger_gui(self):
        """Initialize the graphical debugger"""
        # positioning debugger gui
        debugger_gui_height = 6 * BUTTON_HEIGHT
        pos = (-RIGHT_COLUMN_WIDTH, 0)
        size = (RIGHT_COLUMN_WIDTH, debugger_gui_height)
        debugger_gui.get_instance().init_gui(
            self.ui_manager, (pos, size), TOP_RIGHT_ANCHORS
        )

        debugger_gui.get_instance().add_callback_button(
            LOAD_LEVEL_TEXT, (0, 0), callback=load_next_level
        )
        debugger_gui.get_instance().add_callback_button(
            "Edit", (0, 50), callback=self.tracker.send_direct_command("edit")
        )
        debugger_gui.get_instance().add_callback_button(
            "Make", (0, 100), callback=self.tracker.send_direct_command("make")
        )
        debugger_gui.get_instance().add_callback_button(
            NEXT_LEVEL_TEXT, (0, 150), callback=level_controller().unlock_next_level
        )

        MainWindow.add_button_with_forward(
            "Run",
            (BUTTON_WIDTH + 20, 0),
            lambda: console.get_instance().trigger_command("run"),
        )
        self.add_button_with_forward(
            "Start",
            (BUTTON_WIDTH + 20, 50),
            lambda: console.get_instance().trigger_command("start"),
        )
        self.add_button_with_forward(
            "Next",
            (BUTTON_WIDTH + 20, 100),
            lambda: console.get_instance().trigger_command("next"),
        )
        self.add_button_with_forward(
            "Step",
            (BUTTON_WIDTH + 20, 150),
            lambda: console.get_instance().trigger_command("step"),
        )

    def init_console(self):
        """Initialize graphical console"""
        # positioning console gui
        console_height = self.height - 6 * BUTTON_HEIGHT - 50
        pos = (-RIGHT_COLUMN_WIDTH, -console_height - 50)
        size = (RIGHT_COLUMN_WIDTH, console_height)
        console.get_instance().init_gui(
            self.ui_manager, (pos, size), BOTTOM_RIGHT_ANCHORS
        )

        console.get_instance().triggered_command_callback = self.tracker_breaked

    def init_source_code_gui(self):
        """Initialize the source code visualisation"""
        # positioning source code display
        pos = (-400 - RIGHT_COLUMN_WIDTH, 0)
        size = (400, self.height - 50)
        source_code_gui.get_instance().init_gui(
            self.ui_manager, (pos, size), anchors=TOP_RIGHT_ANCHORS
        )

        # right_column_width = 4*debugger_gui.BUTTON_WIDTH+30

    def init_game_view(self):
        """Initialize the 2D view of the game"""
        # load game tiles
        self.game = Game((50, -50), self.tracker)
        assets_dir = os.path.join(level_controller().agdbenture_dir, "assets")
        self.game.preload_tiles(assets_dir)

    @staticmethod
    def add_button_with_forward(
        button_name: str, pos: Tuple[int, int], callback: Callable[[], PauseReason]
    ):
        """Registers a new button in the debugger gui instance
        with a callback decorated with event forwarding"""
        debugger_gui.get_instance().add_callback_button(
            button_name,
            pos,
            # callback=self.forward_button_event(button_name, callback))
            callback=callback,
        )

    def forward_button_event(
        self, button_name: str, callback=Callable[[], PauseReason]
    ):
        """Forward the button pressed event to the current level"""

        def inner():
            pause_reason = callback()  # pylint:disable=abstract-class-instantiated
            self.tracker_breaked(
                button_name.lower(), pause_reason
            )  # FIXME get real command instead of hack

        return inner

    def tracker_breaked(self, command: str, pause_reason: PauseReason):
        """Notify the current level that the tracker breaked"""
        if level_controller().current_level is not None:
            level_controller().current_level.on_tracker_breaked(command, pause_reason)
        self.game.update()


def load_next_level():
    """show the next level button and trigger the next level loading"""
    debugger_gui.get_instance().get_button_by_text(NEXT_LEVEL_TEXT).show()
    level_controller().load_current_level_into_tracker()
