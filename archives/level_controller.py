"""Level controller."""

from subprocess import run
import os
import shutil
from importlib import import_module

from easytracker import tracker_interface, PauseReason

DEBUG_LEVEL = -1  # level number to debug, -1 to use progress file as usual
DEBUG_EVENTS = 0  # 0 1

# load level list from file and append level number for alphabetical order
LEVEL_LIST = ["first_level", "move", "snake", "enter_shop", "last_level"]


def create_folder_if_not_exist(folder):
    """creates the given folder if it doesn't exist yet"""
    try:
        os.mkdir(folder)
    except FileExistsError:
        pass


# pylint: disable=too-many-instance-attributes
class LevelController:
    """Level controller.

    This class is responsible to load the python code
    for the current level, and to do the level switch
    when the user goes to the next level.
    """

    def __init__(self):
        # creates the saves directory if it doesn't exist
        create_folder_if_not_exist("saves")

        # init common directories
        self.agdbenture_dir = os.path.join(os.path.dirname(__file__))
        self.saves_dir = os.path.join(self.agdbenture_dir, "saves")
        self.levels_dir = os.path.join(self.agdbenture_dir, "levels")
        self.levels_source_dir = os.path.join(self.levels_dir, "source")
        self.levels_script_dir = os.path.join(self.levels_dir, "scripts")

        # load past progress from file
        self.progress = []
        self.__load_progress()

        # compute next level to do
        self.current_level_number = 0
        if self.progress:
            self.current_level_number = LEVEL_LIST.index(self.progress[-1]) + 1
        # Debug option to go to a specific level
        if DEBUG_LEVEL != -1:
            self.current_level_number = DEBUG_LEVEL
        self.current_level_name = LEVEL_LIST[self.current_level_number]

        # prepare empty level data
        self.current_level = None
        self.working_directory = None
        self.script_directory = None

    # Progress management
    def __load_progress(self):
        """Loads all the levels played by the user from saves/progress.txt"""
        try:
            with open(
                os.path.join(self.saves_dir, "progress.txt"), "r", encoding="utf-8"
            ) as progress_file:
                self.progress = [name.strip() for name in progress_file.readlines()]
        except FileNotFoundError:
            # We do nothing if no progress file is found
            pass

    def __save_progress(self):
        """Saves the current progress into the progress file"""
        with open(
            os.path.join(self.saves_dir, "progress.txt"), "w", encoding="utf-8"
        ) as progress_file:
            for name in self.progress:
                print(name, file=progress_file)

    def clear_progress(self):
        """Clears the current progress and saves it into the progress file"""
        self.progress = []
        self.__save_progress()

    # level loading and success
    def load_level(self):
        """Prepare the level code and loads it into easytracker.

        It loads the level control script.
        For that, python dynamicity is used to instantiate
        the next level class from its name.
        Thank you python :)"""
        self.working_directory = os.path.join(self.saves_dir, self.current_level_name)

        self.__prepare_level_source_code()
        self.__prepare_level_script()

        tracker_interface().send_direct_command(f"cd {self.working_directory}")
        tracker_interface().load_program(f"{self.current_level.executable_name}")

    def __prepare_level_source_code(self):
        """copy or load the source code and compiles it"""
        try:
            os.mkdir(self.working_directory)

            src_dir = os.path.join(self.levels_source_dir, self.current_level_name)

            # if the folder doesn't exist, copy the source code in it
            shutil.copytree(src_dir, self.working_directory, dirs_exist_ok=True)
        except FileExistsError:
            pass

        # Compile once
        run(["make", "-C", self.working_directory, "-s"], check=False)

    def __prepare_level_script(self):
        """dynamicaly loads the current level module"""
        self.script_directory = os.path.join(
            self.levels_script_dir, self.current_level_name
        )

        # Import level class and instatiate level
        level_class = import_module(
            f"levels.scripts.{self.current_level_name}.level"
        ).Level
        self.current_level = level_class(self.working_directory, self.script_directory)

    def unlock_next_level(self):
        """Updates progress and saves it"""
        # add the current level to the progress
        self.progress.append(self.current_level_name)
        # load the next level
        self.current_level_number += 1
        self.current_level_name = LEVEL_LIST[self.current_level_number]
        self.__save_progress()

    # event forwarding
    def on_tracker_breaked(self, command: str, pause_reason: PauseReason):
        """Sends the GDB event to the current level to be processed"""
        if DEBUG_EVENTS:
            print(pause_reason)
        if self.current_level is not None:
            self.current_level.on_tracker_breaked(command, pause_reason)


instance = LevelController()


def level_controller():
    """returns a global instance to the level controller"""
    return instance
