"""Generic template for all levels"""

import os

from main_window import NEXT_LEVEL_TEXT, LOAD_LEVEL_TEXT

from visualprimitives.pygame.gui import debugger_gui, source_code_gui
from easytracker import PauseReason


class TemplateLevel:
    """Generic level template.

    Overwrite methods by inheritance to in your
    level implementation class.
    """

    def __init__(self, level_directory):
        self.level_directory = level_directory
        self.succeeded = False

        # disable everything before loading
        for button in debugger_gui.get_instance().get_all_buttons():
            # by default buttons are hidden and enabled at level start
            button.hide()
            button.enable()
        debugger_gui.get_instance().get_button_by_text(NEXT_LEVEL_TEXT).disable()
        debugger_gui.get_instance().get_button_by_text(LOAD_LEVEL_TEXT).show()

        self.program_name = "prog"
        self.source_path = os.path.join(level_directory, "level.c")

    def success(self):
        """Set the level as succeeded"""
        debugger_gui.get_instance().get_button_by_text(NEXT_LEVEL_TEXT).enable()
        self.succeeded = True

    def create_popup_from_file(
        self,
        filename: str,
        title: str,
        dismiss_button_text,
        ui_object_id="#popup_window",
    ):
        """Reads a file and put the text into the main popup"""
        with open(
            os.path.join(self.level_directory, filename), "r", encoding="utf-8"
        ) as f_d:
            file_content = f_d.read()
        debugger_gui.get_instance().create_text_popup(
            title, file_content, dismiss_button_text, ui_object_id
        )

    def on_popup_closed(self, popup_id):
        """Has to be overriden by levels to process each window exit"""

    def on_tracker_breaked(
        self, command: str, pause_reason: PauseReason
    ):  # pylint: disable=unused-argument, no-self-use
        """Has to be overriden by levels. This runs when the tracker breaked.
        This updates the source code gui with line number and the game visualization"""
        source_code_gui.get_instance().update()
