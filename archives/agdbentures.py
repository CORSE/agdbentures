#!/usr/bin/env python

"""
Starting point of the game.

This program will launch the graphical window
and will show a console.
"""

import logging

from main_window import MainWindow

if __name__ == "__main__":
    # silent logs
    # change the logging level if you are debugging
    logging.basicConfig(level=logging.INFO)

    app = MainWindow()

    app.start()
